<!DOCTYPE html>
<html lang="en">
<head>
  <title>Cars 24</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="styles.css"> 
</head>
<body>
<div class="container-fluid clickCall" onclick="location.href='tel:9811140959';">    
    <section class="bannerSection">            
        <div class="container">
            <div class="row bannerContentNew">
                <div class="col-md-12 col-sm-12 bannerContentSection">
                    <h2>Car Bechni Ho, Toh CARS24!</h2>
                    <br>
                    <h2 class="clickCall">
                        <span class="clickCall"><i class="fa fa-phone" style="font-size:38px; color: #0267bc; "></i></span> 
                        <span class="mobileBannerCall">
                            <a class="clickCall">9811140959</a> 
                        </span>
                    </h2>

                    <p class="missCallClass">Give a Miss Call</p>
                </div>
            </div>
        </div>

    </section>
    
    <div class="clearfix"></div>

    <br>    

    <section class="secondbanner">
              &nbsp;
    </section>

    <div class="clearfix"></div>

    <br>   

    <section>
        <div class="col-sm-12">
            <h1>Sell Cars in 3 Easy Steps</h1>
            <br>
            <br>

            <div class="col-md-12">
                <img src="./img/firstEnquiry.png" alt="">
            </div>

            <br>
            <br>

            <div class="col-md-12">
                <img src="./img/second_image.png" alt="">
            </div>

            <br>
            <br>
            <div class="col-md-12">
                <img src="./img/third_image.png" alt="">
            </div>
        </div>
    </section>

    <div class="clearfix"></div>

    <br>   
    <br>
    <br>


    <section>
        <div class="col-sm-12 thirdSecImageItemsContainer">
            <h1>Why Choose Us ?</h1>            
            <div class="thirdSecImageItems">
                <img src="./img/sell_car.png" alt="">
            </div>

            <div class="thirdSecImageItems">
                <img src="./img/best_car.png" alt="">
                
            </div>

            <div class="thirdSecImageItems">
                <img src="./img/instant_payment.png" alt="">
            </div>

            <div class="thirdSecImageItems">
                <img src="./img/free_rc.png" alt="">
                
            </div>


        </div>


    </section>

    <br>

    <div class="clearfix"></div>

    <br>
    <section>
        <div class="col-sm-12">
            <div class="single">
                <div>
                    <img src="./img/testimonial_one.JPG" alt="">
                </div>
                <div>
                    <img src="./img/testimonial_two.JPG" alt="">
                </div>
                <div>
                    <img src="./img/testimonial_three.JPG" alt="">
                </div>
                <div>
                    <img src="./img/testimonial_four.JPG" alt="">
                </div>
            </div>
        </div>
    </section>

    <div class="clearfix"></div>
    <br>
    <br>
    <br>
    <br>
    <br>    <br>    <br> 
    <footer>
        <div class="footerContentSection">
            <span class="">
                <a class="clickCall">
                    <img class="clickCall" src="./img/modal_Call_Img.png" alt="" width="100%">
                </a>
            </span>
        </div>
    </footer>
    <div class="clearfix"></div>
    <br>
    <section>
            <p>&nbsp; &nbsp; </p>
    </section> 
    <br> 
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="script.js"></script>

</body>
</html>