<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function validate_fields($form = ""){
		
	//get instance of main codeigniter object
	$ci_instance =& get_instance();

	if($ci_instance->form_validation->run($form) == false){
		//validtion failed
		$validation_error = validation_errors();
		$data['error'] = $ci_instance->form_validation->error_array();

		$message = explode("\n", $validation_error)[0];
		$message = str_replace("<p>","",$message);
		$message = str_replace("</p>","",$message);
		setResponse(false, $data, $message);
		return false;
	}

	$ci_instance->config->set_item('language', 'english');
	return true;
}

function setResponse($status, $data = array(), $message = ""){
	//Get CI Instance
	$ci = & get_instance();

	$response['status'] = $status;
	$response['data'] = isset($data) ? $data : new stdClass();
	$response['message'] = $message;

	$ci->OutputResult = $response;

	header('Content-Type: application/json');
	echo json_encode($response);
}
?>