<?php

if (!function_exists('api_hoh_hiranandani')) {
    function api_hoh_hiranandani($data){
        $api_url = 'https://lead.anarock.com/api/v0/adcanopus/sync-lead'; // as mentioned on left under HTTP Request Heading

        $key = '711032e021af85fb'; 
        $current_time = time();
        $message = (string)$current_time;
        // to lowercase hexits
        $hash = hash_hmac('sha256', $message, $key);
        // you can use libraries like https://github.com/brick/phonenumber to format numbers before sending
        $number =  '+' . $data['country'] . ' ' . $data['phone'];//PhoneNumber::parse('+447123456789'); // phone number of the lead with +
        $country_code = $data['country'];//$number->getRegionCode(); // GB
        $phone = $data['phone'];//$number->getNationalNumber(); // 7123456789
        $campaign_id = "hoh-bannerghatta-adcanopus"; // String to Attribute the lead to specific project. contact ANAROCK team for this.
        $postFields  = "";
        $postFields .= "&name=".$data['name'];
        $postFields .= "&email=".$data['email']; 
        $postFields .= "&purpose=buy";// refer to query parameters section on left section
        $postFields .= "&current_time=".$current_time;
        $postFields .= "&phone=".$phone; // Phone number without country code
        $postFields .= "&country_code=".$country_code; // Standard ISO3166-1 alpha-2 code for a country.
        $postFields .= "&hash=".$hash; // its mandatory to create hash using same timestamp as sent in current_time parameter
        $postFields .= "&campaign_id=".$campaign_id;
        //print_r($postFields);exit;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$api_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    $postFields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        return $server_output;
        
    }
}


if (!function_exists('api_sterlingascentia')) {
	function api_sterlingascentia($data){
        $api_url = 'https://lead.anarock.com/api/v0/adcanopus/sync-lead'; // as mentioned on left under HTTP Request Heading

        $key = '711032e021af85fb'; 
        $current_time = time();
        $message = (string)$current_time;
        // to lowercase hexits
        $hash = hash_hmac('sha256', $message, $key);
        // you can use libraries like https://github.com/brick/phonenumber to format numbers before sending
        $number =  '+' . $data['country'] . ' ' . $data['phone'];//PhoneNumber::parse('+447123456789'); // phone number of the lead with +
        $country_code = $data['country'];//$number->getRegionCode(); // GB
        $phone = $data['phone'];//$number->getNationalNumber(); // 7123456789
        $campaign_id = "adcanopus_sterling_ascentia"; // String to Attribute the lead to specific project. contact ANAROCK team for this.
        $postFields  = "";
        $postFields .= "&name=".$data['name'];
        $postFields .= "&email=".$data['email']; 
        $postFields .= "&purpose=buy";// refer to query parameters section on left section
        $postFields .= "&current_time=".$current_time;
        $postFields .= "&phone=".$phone; // Phone number without country code
        $postFields .= "&country_code=".$country_code; // Standard ISO3166-1 alpha-2 code for a country.
        $postFields .= "&hash=".$hash; // its mandatory to create hash using same timestamp as sent in current_time parameter
        $postFields .= "&campaign_id=".$campaign_id;
        //print_r($postFields);exit;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$api_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    $postFields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        return $server_output;
        
    }
}
if (!function_exists('api_gcorptheicon')) {
	 function api_gcorptheicon($data){
        $api_url = 'https://lead.anarock.com/api/v0/adcanopus/sync-lead'; // as mentioned on left under HTTP Request Heading

        $key = '711032e021af85fb'; 
        $current_time = time();
        $message = (string)$current_time;
        // to lowercase hexits
        $hash = hash_hmac('sha256', $message, $key);
        // you can use libraries like https://github.com/brick/phonenumber to format numbers before sending
        $number =  '+' . $data['country'] . ' ' . $data['phone'];//PhoneNumber::parse('+447123456789'); // phone number of the lead with +
        $country_code = $data['country'];//$number->getRegionCode(); // GB
        $phone = $data['phone'];//$number->getNationalNumber(); // 7123456789
        $campaign_id = "gcorp-theicon-adcanopus"; // String to Attribute the lead to specific project. contact ANAROCK team for this.
        $postFields  = "";
        $postFields .= "&name=".$data['name'];
        $postFields .= "&email=".$data['email']; 
        $postFields .= "&purpose=buy";// refer to query parameters section on left section
        $postFields .= "&current_time=".$current_time;
        $postFields .= "&phone=".$phone; // Phone number without country code
        $postFields .= "&country_code=".$country_code; // Standard ISO3166-1 alpha-2 code for a country.
        $postFields .= "&hash=".$hash; // its mandatory to create hash using same timestamp as sent in current_time parameter
        $postFields .= "&campaign_id=".$campaign_id;
        //print_r($postFields);exit;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$api_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    $postFields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        return $server_output;
       

    }
}
if(!function_exists('api_mittalelanza')){
	 function api_mittalelanza($data){
        $api_url = 'https://lead.anarock.com/api/v0/adcanopus/sync-lead'; // as mentioned on left under HTTP Request Heading

        $key = '711032e021af85fb'; 
        $current_time = time();
        $message = (string)$current_time;
        // to lowercase hexits
        $hash = hash_hmac('sha256', $message, $key);
        // you can use libraries like https://github.com/brick/phonenumber to format numbers before sending
        $number =  '+' . $data['country'] . ' ' . $data['phone'];//PhoneNumber::parse('+447123456789'); // phone number of the lead with +
        $country_code = $data['country'];//$number->getRegionCode(); // GB
        $phone = $data['phone'];//$number->getNationalNumber(); // 7123456789
        $campaign_id = "mittal_elanza_adcanopus"; // String to Attribute the lead to specific project. contact ANAROCK team for this.
        $postFields  = "";
        $postFields .= "&name=".$data['name'];
        $postFields .= "&email=".$data['email']; 
        $postFields .= "&purpose=buy";// refer to query parameters section on left section
        $postFields .= "&current_time=".$current_time;
        $postFields .= "&phone=".$phone; // Phone number without country code
        $postFields .= "&country_code=".$country_code; // Standard ISO3166-1 alpha-2 code for a country.
        $postFields .= "&hash=".$hash; // its mandatory to create hash using same timestamp as sent in current_time parameter
        $postFields .= "&campaign_id=".$campaign_id;
       
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$api_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    $postFields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        return $server_output;
        
    }
}

if (!function_exists('api_maya_indradhanush')) {
	function api_maya_indradhanush($data){
        $api_url = 'https://lead.anarock.com/api/v0/adcanopus/sync-lead'; // as mentioned on left under HTTP Request Heading

        $key = '711032e021af85fb'; 
        $current_time = time();
        $message = (string)$current_time;
        // to lowercase hexits
        $hash = hash_hmac('sha256', $message, $key);
        // you can use libraries like https://github.com/brick/phonenumber to format numbers before sending
        $number =  '+' . $data['country'] . ' ' . $data['phone'];//PhoneNumber::parse('+447123456789'); // phone number of the lead with +
        $country_code = $data['country'];//$number->getRegionCode(); // GB
        $phone = $data['phone'];//$number->getNationalNumber(); // 7123456789
        $campaign_id = "maya_indradhanush_adcanopus"; // String to Attribute the lead to specific project. contact ANAROCK team for this.
        $postFields  = "";
        $postFields .= "&name=".$data['name'];
        $postFields .= "&email=".$data['email']; 
        $postFields .= "&purpose=buy";// refer to query parameters section on left section
        $postFields .= "&current_time=".$current_time;
        $postFields .= "&phone=".$phone; // Phone number without country code
        $postFields .= "&country_code=".$country_code; // Standard ISO3166-1 alpha-2 code for a country.
        $postFields .= "&hash=".$hash; // its mandatory to create hash using same timestamp as sent in current_time parameter
        $postFields .= "&campaign_id=".$campaign_id;
       
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$api_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    $postFields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        return $server_output;
        
    }
}


if (!function_exists('api_tata')) {
    function api_tata($data){
        $api_url = 'https://lead.anarock.com/api/v0/adcanopus/sync-lead'; // as mentioned on left under HTTP Request Heading

        $key = '711032e021af85fb'; 
        $current_time = time();
        $message = (string)$current_time;
        // to lowercase hexits
        $hash = hash_hmac('sha256', $message, $key);
        // you can use libraries like https://github.com/brick/phonenumber to format numbers before sending
        $number =  '+' . $data['country'] . ' ' . $data['phone'];//PhoneNumber::parse('+447123456789'); // phone number of the lead with +
        $country_code = $data['country'];//$number->getRegionCode(); // GB
        $phone = $data['phone'];//$number->getNationalNumber(); // 7123456789
        $campaign_id = "tata_newhaven_adcanopus"; // String to Attribute the lead to specific project. contact ANAROCK team for this.
        $postFields  = "";
        $postFields .= "&name=".$data['name'];
        $postFields .= "&email=".$data['email']; 
        $postFields .= "&purpose=buy";// refer to query parameters section on left section
        $postFields .= "&current_time=".$current_time;
        $postFields .= "&phone=".$phone; // Phone number without country code
        $postFields .= "&country_code=".$country_code; // Standard ISO3166-1 alpha-2 code for a country.
        $postFields .= "&hash=".$hash; // its mandatory to create hash using same timestamp as sent in current_time parameter
        $postFields .= "&campaign_id=".$campaign_id;
       
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$api_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    $postFields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        return $server_output;
        
    }
}



if (!function_exists('api_tatapromont')) {
    function api_tatapromont($data){
        $api_url = 'https://lead.anarock.com/api/v0/adcanopus/sync-lead'; // as mentioned on left under HTTP Request Heading

        $key = '711032e021af85fb'; 
        $current_time = time();
        $message = (string)$current_time;
        // to lowercase hexits
        $hash = hash_hmac('sha256', $message, $key);
        // you can use libraries like https://github.com/brick/phonenumber to format numbers before sending
        $number =  '+' . $data['country'] . ' ' . $data['phone'];//PhoneNumber::parse('+447123456789'); // phone number of the lead with +
        $country_code = $data['country'];//$number->getRegionCode(); // GB
        $phone = $data['phone'];//$number->getNationalNumber(); // 7123456789
        $campaign_id = "tata-promont-adcanopus"; // String to Attribute the lead to specific project. contact ANAROCK team for this.
        $postFields  = "";
        $postFields .= "&name=".$data['name'];
        $postFields .= "&email=".$data['email']; 
        $postFields .= "&purpose=buy";// refer to query parameters section on left section
        $postFields .= "&current_time=".$current_time;
        $postFields .= "&phone=".$phone; // Phone number without country code
        $postFields .= "&country_code=".$country_code; // Standard ISO3166-1 alpha-2 code for a country.
        $postFields .= "&hash=".$hash; // its mandatory to create hash using same timestamp as sent in current_time parameter
        $postFields .= "&campaign_id=".$campaign_id;
       
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$api_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    $postFields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        return $server_output;
        
    }
} 

?>