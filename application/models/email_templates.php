<?php
class email_templates extends CI_MODEL
{
	protected $setting_phone_no;
	protected $setting_admin_name;
	protected $setting_site_name;
	protected $all_array;
	
	function __construct(){
		parent::__construct();	
		$config = array('mailtype' => "html");
		$this->load->library('email', $config);
		$this->load->library('smtp');
		$this->load->library('phpmailer');
		
	}
	
	
	function mlkt_mail_send($from_email="", $from_name="", $to="", $subject="", $message="", $to1="") {
		global $dm_name;		
		global $dm_name;
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		
			
		$mail = new PHPMailer();
		$mail->Host = "set29.adcanopus.com"; // Connect to this GreenArrow server
		$mail->SMTPAuth = true; // enables SMTP authentication. Set to false for IP-based authentication
		$mail->Port = 587; // SMTP submission port to inject mail into. Usually port 587 or 25
		$mail->Username = "mailclass-adc_medlife@trns.adcanopus.com"; // SMTP username
		$mail->Password = 'hdgf8sdfgibskd'; // SMTP password
		 
		// Timezone
		date_default_timezone_set('America/Chicago');
		
		// Campaign Settings
		$mail_class = "adcan"; // Mail Class to use
		
		// Create the SMTP session
		$mail->IsSMTP(); // Use SMTP
		$mail->SMTPKeepAlive = true; // prevent the SMTP session from being closed after each message
		$mail->SmtpConnect();
		
		// Set headers that are constant for every message outside of the foreach loop
		$mail->SetFrom($from_email, $from_name);
		$mail->Subject = $subject;
		$mail->addCustomHeader("X-GreenArrow-MailClass: $mail_class");
	

		// Generate headers that are unique for each message
		$mail->ClearAllRecipients();
		$mail->AddAddress($to);
		//$mail->AddAddress('atul@adcanopus.com');
		if($to1 != ""){
			$mail->AddAddress($to1);
		}
		
	
		// Generate the message
		$mail->MsgHTML($message);
		
		if($mail->Send()) {
			return true;
		} else {
			return false;
		}

		// Close the SMTP session
		$mail->SmtpClose();
		
		return true;
		
	}


	function mlkt_mail_send_new($from_email="", $from_name="", $to="", $subject="", $message="", $to1="", $to2="", $to3="", $to4="", $to5="", $to6="", $to7="", $to8="", $to9="", $to10="") {
		global $dm_name;		
		global $dm_name;
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		
			
		$mail = new PHPMailer();
		$mail->Host = "set29.adcanopus.com"; // Connect to this GreenArrow server
		$mail->SMTPAuth = true; // enables SMTP authentication. Set to false for IP-based authentication
		$mail->Port = 587; // SMTP submission port to inject mail into. Usually port 587 or 25
		$mail->Username = "mailclass-adc_medlife@trns.adcanopus.com"; // SMTP username
		$mail->Password = 'hdgf8sdfgibskd'; // SMTP password
		 
		// Timezone
		date_default_timezone_set('America/Chicago');
		
		// Campaign Settings
		$mail_class = "adcan"; // Mail Class to use
		
		// Create the SMTP session
		$mail->IsSMTP(); // Use SMTP
		$mail->SMTPKeepAlive = true; // prevent the SMTP session from being closed after each message
		$mail->SmtpConnect();
		
		// Set headers that are constant for every message outside of the foreach loop
		$mail->SetFrom($from_email, $from_name);
		$mail->Subject = $subject;
		$mail->addCustomHeader("X-GreenArrow-MailClass: $mail_class");
	

		// Generate headers that are unique for each message
		$mail->ClearAllRecipients();
		$mail->AddAddress($to);
		//$mail->AddAddress('atul@adcanopus.com');
		if($to1 != ""){
			$mail->AddAddress($to1);
		}

		if($to2 != ""){
			$mail->AddAddress($to2);
		}

		if($to3 != ""){
			$mail->AddAddress($to3);
		}

		if($to3 != ""){
			$mail->AddAddress($to3);
		}

		if($to4 != ""){
			$mail->AddAddress($to4);
		}

		if($to5 != ""){
			$mail->AddAddress($to5);
		}

		if($to6 != ""){
			$mail->AddAddress($to6);
		}

		if($to7 != ""){
			$mail->AddAddress($to7);
		}

		if($to8 != ""){
			$mail->AddAddress($to8);
		}

		if($to9 != ""){
			$mail->AddAddress($to9);
		}

		if($to10 != ""){
			$mail->AddAddress($to10);
		}
		
	
		// Generate the message
		$mail->MsgHTML($message);
		
		if($mail->Send()) {
			return true;
		} else {
			return false;
		}

		// Close the SMTP session
		$mail->SmtpClose();
		
		return true;
		
	}
	
	
	function mail_send_attachment($from_email="", $from_name="", $to="", $subject="", $message="", $path_name="",$filename="", $headers = "", $to1="", $to2="", $to3="", $to4="", $to5="", $to6="", $to7="") {
		
		if($headers=="local"){
			
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		
		
			$mail = new PHPMailer();
			$mail->IsSMTP();
			$mail->Mailer = "smtp";
			$mail->Host = "set29.adcanopus.com"; // Connect to this GreenArrow server
			$mail->SMTPAuth = true; // enables SMTP authentication. Set to false for IP-based authentication
			$mail->Port = 587; // SMTP submission port to inject mail into. Usually port 587 or 25
			$mail->Username = "mailclass-adc_medlife@trns.adcanopus.com"; // SMTP username
			$mail->Password = 'hdgf8sdfgibskd'; // SMTP password
			
				// Timezone
			date_default_timezone_set('America/Chicago');
			
			// Campaign Settings
			$mail_class = "adcan"; // Mail Class to use
			
			// Create the SMTP session
			$mail->IsSMTP(); // Use SMTP
			$mail->SMTPKeepAlive = true; // prevent the SMTP session from being closed after each message
			$mail->SmtpConnect();
			
			$full_path = $path_name;
			
			$mail->AddAttachment($full_path, $filename );
			
			// Set headers that are constant for every message outside of the foreach loop
			$mail->SetFrom($from_email, $from_name);
			$mail->Subject = $subject;
			$mail->addCustomHeader("X-GreenArrow-MailClass: $mail_class");
		
			// Generate headers that are unique for each message
			$mail->ClearAllRecipients();
			$mail->AddAddress($to);
			//$mail->AddAddress('atul@adcanopus.com');
			if($to1 != ""){
				$mail->AddAddress($to1);
			}
			
			if($to2 != ""){
				$mail->AddAddress($to2);
			}
			if($to3 != ""){
				$mail->AddAddress($to3);
			}
			if($to4 != ""){
				$mail->AddAddress($to4);
			}
			if($to5 != ""){
				$mail->AddAddress($to5);
			}
			if($to6 != ""){
				$mail->AddAddress($to6);
			}
			if($to7 != ""){
				$mail->AddAddress($to7);
			}

			// Generate the message
			$mail->MsgHTML($message);
			
			if($mail->Send()) {
				
				return true;
			} else {
				
				return false;
			}
				
			
			
			
		}
	}


	

	function mlkt_mail_send_medlife($from_email="", $from_name="", $to="", $subject="", $message="", $to1="") {
		global $dm_name;		
		global $dm_name;
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		
		//$from_email = "update@whats-upp.in"; 
			
		$mail = new PHPMailer();
		$mail->Host = "set29.adcanopus.com"; // Connect to this GreenArrow server
		$mail->SMTPAuth = true; // enables SMTP authentication. Set to false for IP-based authentication
		$mail->Port = 587; // SMTP submission port to inject mail into. Usually port 587 or 25
		$mail->Username = "mailclass-adc_medlife@trns.adcanopus.com"; // SMTP username
		$mail->Password = 'hdgf8sdfgibskd'; // SMTP password		 


		// Timezone
		date_default_timezone_set('America/Chicago');
		
		// Campaign Settings
		$mail_class = "medlife"; // Mail Class to use
		
		// Create the SMTP session
		$mail->IsSMTP(); // Use SMTP
		$mail->SMTPKeepAlive = true; // prevent the SMTP session from being closed after each message
		$mail->SmtpConnect();
		
		// Set headers that are constant for every message outside of the foreach loop
		$mail->SetFrom('update@theistorm.com', $from_name);
		$mail->Subject = $subject;
		$mail->addCustomHeader("X-GreenArrow-MailClass: $mail_class");
	

		// Generate headers that are unique for each message
		$mail->ClearAllRecipients();
		$mail->AddAddress($to);
		//$mail->AddAddress('atul@adcanopus.com');
		if($to1 != ""){
			$mail->AddAddress($to1);
		}
		
	
		// Generate the message
		$mail->MsgHTML($message);
		
		if($mail->Send()) {
			return true;
		} else {
			return false;
		}

		// Close the SMTP session
		$mail->SmtpClose();
		
		return true;
		
	}

	
	
	public function email_signup($user_email = "" , $user_name="") {
		if($user_name != "" && $user_email != "" ) {
			
			// GET THE DETAILS OF USER
			$from_email = SUPPORT_EMAIL;
			$from_name  = SITE_NAME;
			$to         = $user_email; 
			
			$subject = "Welcome To ".SITE_NAME;
			
			$message = '<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#f8f7f5">
			<tr align="center">
				<td><a href="'.SITE_URL.'" target="_blank" style="text-decoration:none"> 
						<table width="600" cellpadding="0" cellspacing="0" align="center" style=" margin:30px 0 30px 0px; border:1px solid #CCC;">
						<tr style="margin-top:20px;">
							<td  bgcolor="#006699" width="550px "align="center" style="padding:10px 0 10px 0px; "><img src="'.S3_URL.'/site/images/logo.png"></td>
						</tr>               	
						<tr style="margin-top:20px;">
							<td bgcolor="#ddd "align="center" style="padding:10px 0 10px 0px;  border-bottom:1px dashed #FFFFFF;">
							<span  style="font-size:18px; text-decoration:none; color:#006699; font-weight:normal; text-transform:capitalize;">
							Welcome To Paisafatafat </span></td>
						</tr>
		
						 <tr style="margin-top:20px;">
							<td align="left" style="padding:20px 0 0 20px;">Dear '.$user_name.',<br><span style="padding:30px 0 0 0; color:#999; line-height:46px;"></span></br><p>You have successfully completed the registration process . we will contact you soon.</p></td>
						</tr>
						<tr>
							<td style=" padding:20px 0 10px 20px ;display:table">
							<span style="background-color:#006699;color:#fff;display:inline-block; font-size:13px;min-height:26px;line-height:30px;min-width:50px;padding:1px 10px;text-align:left;text-decoration:none; font-weight:400; font-size:14px;">Go to paisafatafat.com</span>
							</td>
						</tr>
						<tr>
								<td style="padding:10px 0 10px 20px;"><div style="max-width:100%; font-size:12px; line-height:18px; color:#000;"><span style="color:#000;  text-decoration:none; ">Thanks,<br>Paisafatafat Team</span></div></td>
							  </tr>
					 </table>
					</a> 
				   </td>
			   </tr>
			</table>';
					
			if($this->mail_send($from_email, $from_name, $to, $subject, $message)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	
	public function email_sheltrex($user_email = "", $fn, $to1="", $to2="", $to3="", $to4="", $to5="") {
		
		$to6 = "";
		$to7 = "";
		
		if($user_email != "" ) {
			$from_email = SUPPORT_EMAIL;
			$from_name  = SITE_NAME;
			$to         = $user_email; 
			$filename   = "sheltrex_".$fn.".xls";
			$filename1  = $fn.".xls";
			$path_name	= "./cdn/downloads/".$filename;
			
			$subject = "Sheltrex Leads.";	
			$message   = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
							<html xmlns='http://www.w3.org/1999/xhtml'>
		                 <head>
		                    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
		                    <title></title>
		                 </head>
		                 <body style='font-family: calibri; font-size:16px; margin:20px 0px; color:rgba(59,71,82,1.00); line-height:25px' bgcolor='#efefef' leftmargin='0'topmargin='0' marginwidth='0' marginheight='0'  >
		                     <div style='text-align:left;'>
							 Hi Team,<br>
							 Please find the attachment of Sheltrex Yesterday leads.
							 </div>
					     </body>
						 </html>
		
		                ";
						$header="local";			
			
			if($this->mail_send_attachment($from_email, $from_name, $to, $subject, $message,$path_name,$filename1, $header, $to1, $to2, $to3, $to4, $to5, $to6, $to7)) {
				return true;
			} else {
				return false;
			}
		}
		
	}
	
	
	public function email_srirama($user_email = "", $fn, $to1="", $to2="", $to3="", $to4="", $to5="", $to6="", $to7="") {
		if($user_email != "" ) {
			$from_email = SUPPORT_EMAIL;
			$from_name  = SITE_NAME;
			$to         = $user_email; 
			$filename   = "srirama_".$fn.".xls";
			$filename1  = $fn.".xls";
			$path_name	= "./cdn/srirama_downloads/".$filename;
			//echo $path_name;exit;
			$subject   = "Adcanopus Source - Codetakeiteasy.";	
			$message   = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
							<html xmlns='http://www.w3.org/1999/xhtml'>
		                 <head>
		                    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
		                    <title></title>
		                 </head>
		                 <body style='font-family: calibri; font-size:16px; margin:20px 0px; color:rgba(59,71,82,1.00); line-height:25px' bgcolor='#efefef' leftmargin='0'topmargin='0' marginwidth='0' marginheight='0'  >
		                     <div style='text-align:left;'>
							   Hi Team,
							   Please find the attachment of Sriram Properties  of Yesterday leads.
							 </div>
					     </body>
						 </html>
		
		                ";
						$header="local";			
			
			if($this->mail_send_attachment($from_email, $from_name, $to, $subject, $message,$path_name,$filename1, $header, $to1, $to2, $to3, $to4, $to5 , $to6, $to7)) {
				return true;
			} else {
				return false;
			}
		}			
	}



	public function email_religare($user_email = "", $fn, $to1="", $to2="", $to3="", $to4="", $to5="", $to6="") {
		if($user_email != "" ) {
			$to7		= "";
			$from_email = SUPPORT_EMAIL;
			$from_name  = SITE_NAME;
			$to         = $user_email; 
			$filename   = "religare_".$fn.".xls";
			$filename1  = $fn.".xls";
			$path_name	= "./cdn/religare_downloads/".$filename;
			//echo $path_name;exit;
			$subject   = "Adcanopus Source - Religare.";	
			$message   = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
							<html xmlns='http://www.w3.org/1999/xhtml'>
		                 <head>
		                    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
		                    <title></title>
		                 </head>
		                 <body style='font-family: calibri; font-size:16px; margin:20px 0px; color:rgba(59,71,82,1.00); line-height:25px' bgcolor='#efefef' leftmargin='0'topmargin='0' marginwidth='0' marginheight='0'  >
		                     <div style='text-align:left;'>
							   Hi Team,
							   Please find the attachment of Religare  of Yesterday leads.
							 </div>
					     </body>
						 </html>
		
		                ";
						$header="local";			
			
			if($this->mail_send_attachment($from_email, $from_name, $to, $subject, $message,$path_name,$filename1, $header, $to1, $to2, $to3, $to4, $to5 , $to6, $to7)) {
				return true;
			} else {
				return false;
			}
		}			
	}


	public function email_purvankara($user_email = "", $lpname, $fn, $to1="", $to2="", $to3="", $to4="", $to5="", $to6="") {
		if($user_email != "" ) {
			$from_email = SUPPORT_EMAIL;
			$from_name  = SITE_NAME;
			$to7		= "";
			$to         = $user_email; 
			$filename   = $lpname."_".$fn.".xls";
			$filename1  = $fn.".xls";
			$path_name	= "./cdn/purvankara_downloads/".$filename;
			$lp ="";
			$lp = ucfirst($lpname);
			//echo $path_name;exit;
			$subject = 	$lp." Leads.";	
			$message   = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
							<html xmlns='http://www.w3.org/1999/xhtml'>
		                 <head>
		                    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
		                    <title></title>
		                 </head>
		                 <body style='font-family: calibri; font-size:16px; margin:20px 0px; color:rgba(59,71,82,1.00); line-height:25px' bgcolor='#efefef' leftmargin='0'topmargin='0' marginwidth='0' marginheight='0'  >
		                     <div style='text-align:left;'>
							 Hi Sir/Madam
							   Please find the attachment of purvankara ".$lp." Yesterday leads.
							 </div>
					     </body>
						 </html>
		
		                ";
						$header="local";			
			
			if($this->mail_send_attachment($from_email, $from_name, $to, $subject, $message,$path_name,$filename1, $header, $to1, $to2, $to3, $to4, $to5 , $to6, $to7)) {
				return true;
			} else {
				return false;
			}
		}			
	 }




	 public function email_mantri($user_email = "", $fn, $lpname, $to1="", $to2="", $to3="", $to4="", $to5="", $to6="") {
		if($user_email != "" ) {
			$from_email = SUPPORT_EMAIL;
			$from_name  = SITE_NAME;
			$to7         = ""; 
			$to         = $user_email; 
			$filename   = "mantri_".$fn.".xls";
			$filename1  = $fn.".xls";
			$path_name	= "./cdn/mantri_downloads/".$filename;
			//echo $path_name;exit;
			$subject   = "Adcanopus Source - Mantri.";	
			$message   = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
							<html xmlns='http://www.w3.org/1999/xhtml'>
		                 <head>
		                    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
		                    <title></title>
		                 </head>
		                 <body style='font-family: calibri; font-size:16px; margin:20px 0px; color:rgba(59,71,82,1.00); line-height:25px' bgcolor='#efefef' leftmargin='0'topmargin='0' marginwidth='0' marginheight='0'  >
		                     <div style='text-align:left;'>
							   Hi Team,
							   Please find the attachment of ".$lpname."  of Yesterday leads.
							 </div>
					     </body> 
						 </html>";
						$header="local";			
			
			if($this->mail_send_attachment($from_email, $from_name, $to, $subject, $message,$path_name,$filename1, $header, $to1, $to2, $to3, $to4, $to5 , $to6, $to7)) {
				return true;
			} else {
				return false;
			}
		}			
	}
	 

	public function email_chaipoint($user_email = "", $fn, $lpname, $to1="", $to2="", $to3="", $to4="", $to5="", $to6="") {
		if($user_email != "" ) {
			$from_email = SUPPORT_EMAIL;
			$from_name  = SITE_NAME;
			$to7         = ""; 
			$to         = $user_email; 
			$filename   = "chaipoint_".$fn.".xls";
			$filename1  = $fn.".xls";
			$path_name	= "./cdn/mantri_downloads/".$filename;
			//echo $path_name;exit;
			$subject   = "Adcanopus Source - ".$lpname;	
			$message   = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
							<html xmlns='http://www.w3.org/1999/xhtml'>
		                 <head>
		                    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
		                    <title></title>
		                 </head>
		                 <body style='font-family: calibri; font-size:16px; margin:20px 0px; color:rgba(59,71,82,1.00); line-height:25px' bgcolor='#efefef' leftmargin='0'topmargin='0' marginwidth='0' marginheight='0'  >
		                     <div style='text-align:left;'>
							   Hi Team,
							   Please find the attachment of ".$lpname."  of Yesterday leads.
							 </div>
					     </body> 
						 </html>";
						$header="local";			
			
			if($this->mail_send_attachment($from_email, $from_name, $to, $subject, $message,$path_name,$filename1, $header, $to1, $to2, $to3, $to4, $to5 , $to6, $to7)) {
				return true;
			} else {
				return false;
			}
		}			
	} 


	public function email_winmore($user_email = "", $fn, $to1="", $to2="", $to3="", $to4="", $to5="") {
		
		$to6 = "";
		$to7 = "";
		
		if($user_email != "" ) {
			$from_email = SUPPORT_EMAIL;
			$from_name  = SITE_NAME;
			$to         = $user_email; 
			$filename   = "winmore_".$fn.".xls";
			$filename1  = $fn.".xls";
			$path_name	= "./cdn/downloads/".$filename;
			
			$subject = "Winmore Leads.";	
			$message   = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
							<html xmlns='http://www.w3.org/1999/xhtml'>
		                 <head>
		                    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
		                    <title></title>
		                 </head>
		                 <body style='font-family: calibri; font-size:16px; margin:20px 0px; color:rgba(59,71,82,1.00); line-height:25px' bgcolor='#efefef' leftmargin='0'topmargin='0' marginwidth='0' marginheight='0'  >
		                     <div style='text-align:left;'>
							 Hi Team,<br>
							 Please find the attachment of Winmore Yesterday leads.
							 </div>
					     </body>
						 </html>
		
		                ";
						$header="local";			
			
			if($this->mail_send_attachment($from_email, $from_name, $to, $subject, $message,$path_name,$filename1, $header, $to1, $to2, $to3, $to4, $to5, $to6, $to7)) {
				return true;
			} else {
				return false;
			}
		}
		
	}


	public function email_common($user_email = "", $fn, $lpname, $to1="", $to2="", $to3="", $to4="", $to5="", $to6="") {
		if($user_email != "" ) {
			$from_email = SUPPORT_EMAIL;
			$from_name  = SITE_NAME;
			$to7         = ""; 
			$to         = $user_email; 
			$filename   = $fn.".xls";
			$filename1  = $fn.".xls";
			$path_name	= "./cdn/downloads/".$filename;
			//echo $path_name;exit;
			$subject   = "Adcanopus Source - ". $lpname;	
			$message   = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
							<html xmlns='http://www.w3.org/1999/xhtml'>
		                 <head>
		                    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
		                    <title></title>
		                 </head>
		                 <body style='font-family: calibri; font-size:16px; margin:20px 0px; color:rgba(59,71,82,1.00); line-height:25px' bgcolor='#efefef' leftmargin='0'topmargin='0' marginwidth='0' marginheight='0'  >
		                     <div style='text-align:left;'>
							   Hi Team,
							   Please find the attachment of Yesterday's lead.
							 </div>
					     </body>
						 </html>
		
		                ";
						$header="local";			
			
			if($this->mail_send_attachment($from_email, $from_name, $to, $subject, $message,$path_name,$filename1, $header, $to1, $to2, $to3, $to4, $to5 , $to6, $to7)) {
				return true;
			} else {
				return false;
			}
		}			
	}


	public function email_lgcl($user_email = "", $fn, $to1="", $to2="", $to3="", $to4="", $to5="", $to6="") {
		if($user_email != "" ) {
			$from_email = SUPPORT_EMAIL;
			$from_name  = SITE_NAME;
			$to7         = ""; 
			$to         = $user_email; 
			$filename   = $fn.".xls";
			$filename1  = $fn.".xls";
			$path_name	= "./cdn/downloads/".$filename;
			//echo $path_name;exit;
			$subject   = "Adcanopus Source - LGCL .";	
			$message   = "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
							<html xmlns='http://www.w3.org/1999/xhtml'>
		                 <head>
		                    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
		                    <title></title>
		                 </head>
		                 <body style='font-family: calibri; font-size:16px; margin:20px 0px; color:rgba(59,71,82,1.00); line-height:25px' bgcolor='#efefef' leftmargin='0'topmargin='0' marginwidth='0' marginheight='0'  >
		                     <div style='text-align:left;'>
							   Hi Team,
							   Please find the attachment of LGCL Yesterday's leads.
							 </div>
					     </body>
						 </html>
		
		                ";
						$header="local";			
			
			if($this->mail_send_attachment($from_email, $from_name, $to, $subject, $message,$path_name,$filename1, $header, $to1, $to2, $to3, $to4, $to5 , $to6, $to7)) {
				return true;
			} else {
				return false;
			}
		}			
	}


	public function medlife_registeration_mail($user_email, $user_name, $user_token){

		if($user_name != "" && $user_email != "" ) {
			
			// GET THE DETAILS OF USER
			$from_email = "alerts@ticketabhi.com";//SUPPORT_EMAIL;
			$from_name  = "Medlife Labs";
			$to         = $user_email; 
			$to1 		= "";

			$subject = "Welcome. Confirmation on MeDLife Lab Test";
			
			$message = '<!doctype html>
						<html>
						<head>
						<meta charset="utf-8">
						<title>Lab Tests | Welcome</title>
						</head>
						<body bgcolor="#fff" style="padding: 15px 0px; font-size: 16px; font-family: arial">
						<table width="600" cellspacing="0" cellpadding="0" align="center">
						<tbody>
						<tr>
						<td>
						<table style="border: 2px solid #ccc;" cellpadding="5" bgcolor="#ffffff">
						<tbody>
						<tr>
						<td style="padding: 15px 5px;" align="right" width="395">'.$user_name.'</td>
						</tr>
						<tr>
						<td style="border-top: 1px solid #ccc;" colspan="3">&#160;</td>
						</tr>
						<tr>
						<td>
						<p>Thankyou for registering.<br /><br /> Your first step has begun towards healthy living. Assuring you 100% accurate reports.</p>
						<p style="line-height: 22px;">Click the below activation link to confirm your registration and communication details</p>
						</td>
						</tr>
						<tr>
						<td style="padding-bottom: 30px;" colspan="3" align="center"><a href="'.SITE_URL.'/medlife/confirmation/'.$user_token.'" style="background: #f26122; display: inline-block; padding: 9px 15px; border-radius: 5px; text-decoration: none; color: #fff;">Click Here</a></td>
						</tr>
						</tbody>
						</table>
						</td>
						</tr>
						</tbody>
						</table>
						</body>
						</html>';
					
			if($this->mlkt_mail_send_medlife($from_email, $from_name, $to, $subject, $message, $to1)) {
				return true;
			} else {
				return false;
			}
		}


	} 

	
}
?> 