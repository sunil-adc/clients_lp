<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class lead_check extends CI_Model
{
	protected $data;
    public function __construct(){
        parent::__construct();
		$this->load->library('memcached');
    }
		

	public function _check_fire_status($net,$pub){
	     
		 if (!($this->session->userdata('fire_status')) && $this->session->userdata('fire_status') != '1') {
				if ($this->session->userdata('utm_medium') != '') {
					return $this->_check_inside_utm_medium($this->session->userdata('utm_medium'), $this->session->userdata('utm_source') );
				} else if ($this->session->userdata('utm_source') != "") {
					return $this->_check_inside_utm_source($this->session->userdata('utm_source'));
				}
		}
		return '0';
	   	
	}
	
	public function _check_inside_utm_medium($utm_medium = "", $utm_source = "" ) {
				
		if($utm_medium != "") {
			// CACHE OFF AND CHECK WHETHER PUBLISHER IS REALLY AVAILABLE
			$this->db->cache_off();
			$is_medium_available = 0;
   
			$res = $this->db->query("select count(m.id) total_medium from ".MEDIUM_PIXEL." m inner join ".SOURCE_PIXEL." s on (m.utm_source = s.id) where lower(m.utm_medium) = '".strtolower($utm_medium)."' and lower(s.utm_source) = '".strtolower($utm_source)."'");
			   if ($res->num_rows() > 0) {
				foreach ($res->result() as $val_pub) {
				 $is_medium_available = $val_pub->total_medium;
				}
			 }
			
			// IF AVAILABLE THEN CHECK FOR PINCODE
			if($is_medium_available > 0) {
				$this->db->cache_off();
				
				$sel_q = $this->db->query("select m.id, m.utm_source  from ".MEDIUM_PIXEL." m inner join ".SOURCE_PIXEL." s on (m.utm_source = s.id) where lower(m.utm_medium) = '".strtolower($utm_medium)."' and lower(s.utm_source) = '".strtolower($utm_source)."'");
				
				if($sel_q->num_rows() > 0) {
					foreach ($sel_q->result() as $row) {
						return true;
					}
				}
			} else if ($utm_source != "") {
				
				return $this->_check_inside_utm_source($utm_source);
			}
		}
		return false;
	}
	
	public function _check_inside_utm_source($utm_source="") {
		// CACHE OFF AND CHECK WHETHER PUBLISHER IS REALLY AVAILABLE
		$this->db->cache_off();
		$cond = array('lower(utm_source)' => $utm_source);
		$this->db->where($cond);
		$this->db->from(SOURCE_PIXEL);
		$is_source_available =  $this->db->count_all_results();
		
		
		// IF AVAILABLE THEN CHECK FOR PINCODE
		if($is_source_available > 0 ) {
			$this->db->cache_off();
			
			$sel_q = $this->db->query("select id, utm_source  from ".SOURCE_PIXEL." where lower(utm_source) = '".strtolower($utm_source)."'");
				
				if($sel_q->num_rows() > 0) {
					foreach ($sel_q->result() as $row) {
						return true;
					}
				}
		
		}
		return false;
	}

    	
		
	
	public function set_pixel($usr_id = "0", $table) {
		$arr = array();
		
		$arr['source_medium_script'] = "";
		
		// PRE INITIALIZE VARIABLE
		$arr['net_pub_script'] =  "";
		
		if($usr_id > 0 && is_numeric($usr_id)) {
			$usr_id = $usr_id;
		} else {
			if ($_SESSION['user_id'] > 0) {
				$usr_id = $_SESSION['user_id'];
			} else {
				return false;
			}
		}
		$this->db->cache_off();
		$res = $this->db->query("select  utm_source from ".$table." where id = ".$usr_id);
		
		$phone = ($this->session->userdata('phone') != "" ? $this->session->userdata('phone') : "") ;

		if($res->num_rows() > 0) {
			
			foreach ($res->result() as $val) {
				//if($val->fire_status == 1) {
					if(isset($val->utm_source) && $val->utm_source != '0' && trim($val->utm_source) != ""){
						$this->db->cache_off();
						
						$res = $this->db->query("SELECT s.id, s.utm_source, s.pixel, s.status FROM ".SOURCE_PIXEL." s WHERE LOWER(s.utm_source) =  '".$val->utm_source."'");
						if ($res->num_rows() > 0){
							foreach ($res->result() as $net_val) {
								$arr['source_medium_script'] = str_replace("{USERID}", $usr_id, str_replace("{PHONE}", $phone, $net_val->pixel));
							}
						}
						
					}
                    
					return $arr['source_medium_script'];
				//}
			}
		}
		return false;
	}

	
}
?>