<?php
class cache_model extends CI_MODEL {
	
	public function __construct(){
		parent::__construct();	
		$this->load->library('memcached');
	}
	
	public function get_cache($table_cache, $param, $table_name, $order_by, $where) {
		$cache = $this->memcached->get_cache($table_cache);
		if ($cache != false) {
			return object_to_array(__unserialize__($cache));
		} else {
			$cache = $this->buildqa_cache($table_cache , $param , $table_name, $order_by, $where);
			return ($cache != false) ? object_to_array($cache) : 'N/A';
		}
		return false;
	}
	
	public function buildqa_cache($table_cache = '', $param = '', $table_name= '', $order_by, $where= '' ){   
        
		$order_by_con = $order_by != "" ? ' order by '.$order_by.' asc' : '' ;
		
		$whr = $where != "" ? ' and '.$where : '' ; 
		
		$res = $this->db->query('select '.$param.' from '.$table_name.' where status =1 '.$whr.$order_by_con );
		if ($res->num_rows() > 0) { 
			foreach ($res->result() as $val) {
				$data_val[]  = $val;
				
				$this->memcached->set_cache ($table_cache, __serialize__($data_val), 604800, true);
			}
			if ($table_cache != '') {
				return $data_val;
			} else {
				return true;
			}
		} 
		return false;
	}
	
	
	
	public function get_cache_withstatus($table_cache, $param, $table_name, $order_by, $where) {
		$cache = $this->memcached->get_cache($table_cache);
		if ($cache != false) {
			return object_to_array(__unserialize__($cache));
		} else {
			$cache = $this->buildqa_cache_withstatus($table_cache , $param , $table_name, $order_by, $where);
			return ($cache != false) ? object_to_array($cache) : 'N/A';
		}
		return false;
	}
	
	public function buildqa_cache_withstatus($table_cache = '', $param = '', $table_name= '', $order_by, $where= '' ){   
        $whr = " ";
		$order_by_con = $order_by != "" ? ' order by '.$order_by.' asc' : '' ;
		
		if( $where != ""){
			$whr = " where ".$where ; 
		}
		
		$res = $this->db->query('select '.$param.' from '.$table_name.$whr.$order_by_con );
		if ($res->num_rows() > 0) { 
			foreach ($res->result() as $val) {
				$data_val[]  = $val;
				
				$this->memcached->set_cache ($table_cache, __serialize__($data_val), 604800, true);
			}
			if ($table_cache != '') {
				return $data_val;
			} else {
				return true;
			}
		} 
		return false;
	}
	
	//FOR NOW IFSC
	public function get_cache_withstatus_new($table_cache, $param, $table_name, $order_by, $where) {
		$cache = $this->memcached->get_cache($table_cache);
		if ($cache != false) {
			return object_to_array(__unserialize__($cache));
		} else {
			$cache = $this->buildqa_cache_withstatus_new($table_cache , $param , $table_name, $order_by, $where);
			return ($cache != false) ? object_to_array($cache) : 'N/A';
		}
		return false;
	}
	
	public function buildqa_cache_withstatus_new($table_cache = '', $param = '', $table_name= '', $order_by, $where= '' ){   
        $whr = " ";
		$order_by_con = $order_by != "" ? ' order by '.$order_by.' asc' : '' ;
		
		if( $where != ""){
			$whr = " where ".$where ; 
		}
		
		$res = $this->db->query('select '.$param.' from '.$table_name.$whr.$order_by_con );
		if ($res->num_rows() > 0) { 
			foreach ($res->result() as $val) {
				$data_val[]  = $val;
				
			}
			
			$this->memcached->set_cache ($table_cache, __serialize__($data_val), 604800, true);
			
			if ($table_cache != '') {
				return $data_val;
			} else {
				return true;
			}
		} 
		return false;
	}
	
}
?>