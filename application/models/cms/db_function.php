<?php
class db_function extends CI_MODEL
{
	function __construct(){
		parent:: __construct();
		$this->load->library('pagination');
	}
	
	public function get_single_value($table, $field, $whr = "1=1", $echo = false, $cache_off = false){
		
		// TURN OFF THE CACHE FOR THIS QUERY
		if($cache_off == true) {
			$this->db->cache_off();
		}
		
		$query = $this->db->query("select ".$field." from ".$table." where ".$whr);
		
		if($echo == true)
			echo $this->db->last_query();
			
		if ($query->num_rows() > 0) {
			$row = $query->row_array(); 
			return $row[$field];
		}else{
			return false;
		}
	}
	
	public function get_single_row($table, $field="*", $whr = "1=1", $echo = false, $cache_off = false){
		
		// TURN OFF THE CACHE FOR THIS QUERY
		if($cache_off == true) {
			$this->db->cache_off();
		}
		
		$query = $this->db->query("select ".$field." from ".$table." where ".$whr);
		
		if($echo == true)
			echo $this->db->last_query();
			
		if ($query->num_rows() > 0) {
			return $row = $query->row_array(); 
		}else{
			return false;
		}
	}
	
	// FUNCTION WILL PROCESS QUERY AND GIVE OUTPUT ARRAY
	public function get_data($sql, $cache_off = false, $replica_db = "", $is_replica = false) {
		
		if($cache_off == true) {
			if($is_replica == true) {
				$replica_db->cache_off();
			} else {
				$this->db->cache_off();
			}
		}
		
		if($is_replica == true) {
			$query = $replica_db->query($sql);
		} else {
			$query = $this->db->query($sql);
		}	
		
		if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        } else {
			return false;
		}
	}
	
	public function count_record($sql = '', $echo = false, $cache_off = false, $replica_db = "", $is_replica = false) {
		
		if($sql == '')
			return false;
			
		if($echo == TRUE){
			echo $sql; exit;
		}
		
		if($cache_off == true) {
			if($is_replica == true) {
				$replica_db->cache_off();
			} else {
				$this->db->cache_off();
			}
		}
		
		if($is_replica == true) {
			$query = $replica_db->query($sql);
		} else {
			$query = $this->db->query($sql);
		}		
		
		return $query->num_rows();
    }
	
	public function count_record_using_count($sql = '', $column_name='total', $echo = false, $cache_off = false, $replica_obj = "", $replica_db = false) {
		
		if($sql == '')
			return false;
			
		if($echo == TRUE){
			echo $sql; exit;
		}
		
		if($cache_off == true) {
			if($replica_db == true) {
				$replica_obj->cache_off();
			} else {
				$this->db->cache_off();
			}
		}
		
		if($replica_db == true) {
			$query = $replica_obj->query($sql);
		} else {
			$query = $this->db->query($sql);
		}
		
		foreach ($query->result() as $row) {
			return $row->$column_name;
		}
		return false;
    }
	 
	public function is_db_duplicate_field($tablename, $id, $primary_field, $field, $value) {
		
		// OFF THE CACHE FOR THIS QUERY
		$this->db->cache_off();
		
		if($primary_field != '' && $id != "") {
			$query = $this->db->query('SELECT '.$primary_field.' FROM '.$tablename.' where '.$primary_field.' != '.$id.' and '.$field.' = "'.$value.'"');
		} else {
			$query = $this->db->query('SELECT '.$primary_field.' FROM '.$tablename.' where '.$field.' = "'.$value.'"');
		}
		// CHECK FOR DUPLICATE RECORDS
		if($query->num_rows() == 0){
			return true;
		}
		return false;
	}
}