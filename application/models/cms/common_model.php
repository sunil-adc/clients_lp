<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class common_model extends CI_Model
{
	protected $data;
    public function __construct(){
        parent::__construct();
    }


	public function Menu_Array(){ 
		
		
		/*$offers       = array(
						     array("Manage Offers", "manage_offers", "manage_offers", 1),
						);
		*/
		
		$cc_offers = array(
					    	array("Manage Bank", "manage_banklist", "manage_banklist", 1),
							array("Manage Card", "manage_cc", "manage_cc",  2),
						    
						);
		
		
		$user_details = array(
						     array("User details", "user_details", "user_details", 1),
							 array("Email Enquiry", "email_enq", "email_enq", 2),
							 array("Sobha User Details", "user_details_sobha", "user_details_sobha", 3),
							 array("Religare User details", "user_details_religare", "user_details_religare", 4),
							 array("Mfine User details", "user_details_mfine", "user_details_mfine", 5),
							 array("Shelterx User details", "user_details_sheltrex", "user_details_sheltrex", 6),
							 array("Polcyx health User details", "user_details_policyx_health", "user_details_policyx_health", 7),
							 array("Polcyx Life User details", "user_details_policyx_life", "user_details_policyx_life", 8),
							 array("cc Leads", "user_details_pp_cc", "user_details_pp_cc", 9),
							 array("Purvankara User details", "user_details_purvankara", "user_details_purvankara", 10),
							 array("Srirama User details", "user_details_srirama", "user_details_srirama", 11),
							 array("Mantri User details", "user_details_mantri", "user_details_mantri", 12),							 
							 array("Hiranandani User details", "user_details_hiranandani", "user_details_hiranandani", 13),
							 array("LSBF User details", "user_details_lsbf", "user_details_lsbf", 14),
							 array("Ozone User details", "user_details_ozone", "user_details_ozone", 15),							 
							 array("Nmims User details", "user_details_nmims", "user_details_nmims", 16),
							 array("Winmore User details", "user_details_winmore", "user_details_winmore", 17),
							 array("Nutratimes User details", "user_details_nutratimes", "user_details_nutratimes", 18),							 
							 array("OxygenClub User details", "user_details_oxygenclub", "user_details_oxygenclub", 19),
							 array("Vaswani User details", "user_details_vaswani", "user_details_vaswani", 20),
							 array("Realestate Properties User details", "user_details_realestate", "user_details_realestate", 21),
							 array("Insurance User details", "user_details_insurance", "user_details_insurance", 22),
							 array("Chaipoint User details", "user_details_chaipoint", "user_details_chaipoint", 23),

		               );
		
		$medlife 	= array(
							array("Medlife User details", "user_details_medlife", "user_details_medlife", 1),
							array("Medlife Report", "medlife_report", "medlife_report", 2)
						);

		$admin_user  = array(
					    	array("Manage user", "manage_admin", "manage_admin_user",  1),
						    array("Manage role", "manage_role", "manage_role", 2),
						);
						
		$utm_detail  = array(
					    	array("Manage Source", "manage_utm_source", "manage_utm_source",  1),
						    array("Manage Medium", "manage_utm_medium", "manage_utm_medium", 2),
							array("Manage Affiliate", "manage_utm_aff", "manage_utm_aff", 3), 
						);				
		
		
		$menu      = array(
							//array("Offers",			"Offers",		   $offers,		      1,     'offers'),	
							array("Credit Card",	"Credit Card",	   $cc_offers,		  1,     'offers'),	
							array("Medlife",		"Medlife",	   	   $medlife,		  2,     'users'),	
							array("User details",	"User details",	   $user_details,	  3,     'users'),
							array("Admin  users",	"admin users",	   $admin_user ,	  4,     'users'),	
					        array("Utm  Details",	"utm details",	   $utm_detail ,	  5,      'utm')	
					);
		
		return $menu;
	}
	
	public function get_today_count(){
		//$sql="select count(id) as cnt from survey_data where DATE_FORMAT(participated_on,'%Y-%m-%d')=CURDATE()";
		$result=array();
		$sql="SELECT name, email, net, pub, bnr, fire_status, participated_on FROM survey_data WHERE DATE_FORMAT(participated_on,'%Y-%m-%d')=CURDATE() GROUP BY email ORDER BY id DESC";
		$query=$this->db->query($sql);
		$count=0;
		$details=array();
		if($query->num_rows > 0){
		   foreach($query->result() as $row){
			//return $row->cnt;
			$details[]=$row;
			$count++;
			}
		}
		$result['details']=$details;
		$result['count']=$count;
		return $result;
    }
	
	public function status_dropdown($sel_status=''){
		
		$status_dropdown="<option value=''>select status</option>";
		$a_status="";
		$i_status="";
		if($sel_status){
		   $a_status="selected=selected";
		}else{
		   $i_status="selected=selected";
		}
		$status_dropdown.="<option value='1' ".$a_status.">active</option>";
		$status_dropdown.="<option value='0' ".$i_status.">inactive</option>";
		return $status_dropdown;
    }
	
	public function send_mail($email_elements=''){
		
		$from =  $email_elements['from_email']; // change it to yours
		$to = $email_elements['to_email'];// change it to yours
		$subject = $email_elements['subject'];
		$message = $email_elements['message'];
		
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: '.$from.' <'.$from.'>' . "\r\n";
		
		@mail($to, $subject, $message, $headers);
		 
		}
	
	
	 

}

?>