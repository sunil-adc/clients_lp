<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class general extends CI_Model
{
	
    public function __construct()
    {
        parent::__construct();
    }
	public function checkNetworkPubId($utm_source = "", $utm_sub="", $utm_campaign="", $utm_bnr = "", $utm_medium = "", $zoneid = "", $mobile_track = "", $click_id = "") {
	  
	 	// NETWORK - START
		if (trim($utm_source) != "") {
			if (isset($_COOKIE['utm_source'])) {
				if (trim($_COOKIE['utm_source']) != trim($utm_source)) {
					// SET COOKIE IF REQUESTED OR ALREADY SET
					$this->session->unset_userdata('utm_medium');
					$this->session->set_userdata('utm_source',$utm_source);
				}
			}else {
				// SET COOKIE IF REQUESTED OR ALREADY SET
				$this->session->unset_userdata('utm_medium');
				$this->session->set_userdata('utm_source',$utm_source);  
			}
		}
	  	// NETWORK - END
	  
		// PUBID - START
		if (trim($utm_medium) != "") {
			$this->session->set_userdata('utm_medium',$utm_medium);  
		}
		// PUBID - END
	  
		if (trim($utm_bnr) != "") {
			$this->session->set_userdata('utm_bnr', $utm_bnr); 
		}
		
		if (trim($zoneid) != "") {
			$this->session->set_userdata('zoneid', $zoneid); 
		}
		
		if (trim($mobile_track) != "") {
			$this->session->set_userdata('mobile_track', $mobile_track); 
		}
		
		if (trim($click_id) != "") {
			$this->session->set_userdata('click_id', $click_id); 
		}
		
		if (trim($utm_sub) != "") {
			$this->session->set_userdata('utm_sub',$utm_sub);  
		}
		
	  	return false;
	}
	
	
	public function email_verify($email_token = ""){
	      
	   if($email_token != ""){
		  
		  $this->db->where("email_valid_token", $email_token);
		   
		  $user_verify=array();
		  $user_verify['optin'] = 1;
		   
		  $this->db->update(USER,$user_verify);   
	   }
	   return true;
	}
		
	
	public function is_duplicate_add ($tablename="", $field="", $value="", $cache_off=true, $echo=false) {
		
		if($cache_off == true) {
			$this->db->cache_off();
		}
			
		$query = $this->db->query("select ".$field." from ".$tablename." where ".$field." = '".$value."'");
		
		if($echo == true)
			echo $this->db->last_query();
		
		if ($query->num_rows() > 0) {
			return false;
		}else{
			return true;
		}
	}


	// general
	public function add_user($tablename,$insert_arr){

		$result    = $this->db->insert($tablename, $insert_arr);
        $insert_id = $this->db->insert_id($result);
        if($insert_id != ""){
        	return $insert_id;
        }else{
        	return false;
        }
        exit;

	}
    //general
	public function is_duplicate($tablename,$column,$where){
		$query = $this->db->query("select ".$column." from ".$tablename." where ".$where);
		if($query->num_rows()>0){
			return true;
		}else{
			return false;
		}
        exit; 
	}


	public function update_lead($tablename,$user_id,$lead_id){
		
		$lead_array =array(
			'lead_id'=> $lead_id,
		);
        $this->db->where('id', $user_id);
        $this->db->update($tablename, $lead_array);
        return true;
        exit;
	}
	
	
	
}
?>