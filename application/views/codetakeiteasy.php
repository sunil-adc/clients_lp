<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>The Best Life Whitefield has to offer – Codename Take It Easy | Shriram Properties - Home</title>
      <meta name="description" content="A project that gives you the best of both worlds; a perfect work location and a resort-like weekend lifestyle, at a price that keeps your wallet heavy and your heart light.">
      <meta name="keywords" content="Take It Easy, Shriram Properties, Real Estate, KR Puram, New launch, Whitefield, Shriram Blue">
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/favicon.ico" type="image/x-icon" />
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/css/vendor.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/css/main.css">    
      <script src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/js/jquery-2.2.3.min.js"></script>
      
      <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <script src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/js/bootstrap.js"></script>
      <link href="//fonts.googleapis.com/css?family=Righteous" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Mukta+Mahee:200,300,400,500,600,700,800" rel="stylesheet">
   
		<!-- Global site tag (gtag.js) - AdWords: 794656122 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-794656122"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'AW-794656122');
	</script>


   </head>
   <body> 
    <?php $all_array = all_arrays(); ?>
    <!--/banner-bottom-->
    <div class="w3_agilits_banner_bootm">
        <div class="w3_agilits_inner_bottom">
            <div class="wthree_agile_login">
                <ul>
                    <li>
                        <i class="fa fa-podcast" aria-hidden="true"></i> 
                        RERA Regn No - PR/180728/001966 | It's time to take it easy, and book
                    </li>
                   
                </ul>
            </div>

        </div>
    </div>
    <!--//banner-bottom-->
    <!--/banner-section--> 
    <div id="demo-1" data-zs-src='["<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/b1.jpg","<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/b2.jpg","<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/b3.jpg"]' data-zs-overlay="dots" data-zs-speed="10000">
        <div class="demo-inner-content">
            <!--/banner-info-->
            <div class="baner-info">
                <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="srirama_jsfrm('<?php echo SITE_URL?>codetakeiteasy/submit_frm','1')">
                    <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/shriram_properties.png" />
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/logo.png" />
                                </a> 
                            </div> 
                            <h4> Literally, the best life Whitefield has to offer! </h4>
                    <div class="group">
                        <div class="form-group">
                       
                            <input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid_puravankara('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err1"></span>
                        </div>
              
                    </div>  
                    <div class="group">
                    <div class="form-group">
                   
                    <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid_puravankara('email', 'Please enter correct email')" data-attr="Please enter correct email">
                    <span class="help-block" id="email_err1" ></span>
                    </div>
                    <div class="form-group">
                    <input type="hidden" class="hiddenCountry" name="CountryCode" id="CountryCode1" value="91">
                    <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*" maxlength="10" placeholder="Mobile Number" onkeyup="chck_valid_puravankara('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                    <span class="help-block" id="phone_err1"> </span>
                    </div>
           
                    </div>      
                
                   
                    <div class="submitbtncontainer">
                    <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit" class="frm-sbm">
                    </div>
                </form>
            </div>
            <!--/banner-info-->
        </div>
    </div>
    <div class="agileits-banner-grids text-center">
                <div class="banner-bottom-girds">
                    <div class="services_agile" id="services">
                        <div class="services-top">
                            <div class="col-md-3 col-sm-3 col-xs-6 service_grid text-center">
                                <div class="serviceinfo_agile ih-item circle colored effect17">
                                    <div class="img">
                                        <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/a1.jpg" class="img-responsive" alt="img">
                                    </div>
                                    <div class="info">
                                        <p>water zone</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6 service_grid text-center">
                                <div class="serviceinfo_agile ih-item circle colored effect17">
                                    <div class="img">
                                        <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/a2.jpg" class="img-responsive" alt="img">
                                    </div>
                                    <div class="info">
                                        <p>Skating Rink</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6 service_grid text-center">
                                <div class="serviceinfo_agile ih-item circle colored effect17">
                                    <div class="img">
                                        <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/a3.jpg" class="img-responsive" alt="img">
                                    </div>
                                    <div class="info">
                                        <p>Club House</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-6 service_grid text-center">
                                <div class="serviceinfo_agile ih-item circle colored effect17">
                                    <div class="img">
                                        <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/a4.jpg" class="img-responsive" alt="img">
                                    </div>
                                    <div class="info">
                                        <p>Cricket Practice </p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
    <!--/banner-section-->
            <!-- stats -->
            <div class="wthree-stats">
        <div class="col-md-6">
        <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/ClubPool.jpg" class="img-responsive" alt="" />
        </div>
        <div class="col-md-6 stats-info agileits-w3layouts">
            <div class="col-sm-6 col-xs-3 stats-grid">
                <!-- <div class='numscroller numscroller-big-bottom'>Pre-Launch</div> -->
                <div class="stats-img stat2">
                    <p>1ST CHOICE OF BEST INVENTORY</p>
                </div>
            </div>
            <div class="col-sm-6 col-xs-3 stats-grid">
                <!-- <div class='numscroller numscroller-big-bottom'>1 BHK, 2 BHK, 3 BHK</div> -->
                <div class="stats-img stat2">
                    <p>EXCLUSIVE DISCOUNT ON PRE-LAUNCH PRICE</p>
                </div>
            </div>
            <div class="col-sm-6 col-xs-3 stats-grid">
                <!-- <div class='numscroller numscroller-big-bottom'>Pre-Launch</div> -->
                <div class="stats-img stat2">
                    <p>AN EASILY REFUNDABLE AMOUNT OF ₹36000/-</p>
                </div>
            </div>
            <div class="col-sm-6 col-xs-3 stats-grid">
                <!-- <div class='numscroller numscroller-big-bottom'>1 BHK, 2 BHK, 3 BHK</div> -->
                <div class="stats-img stat2">
                    <p>EASY ONLINE PROCESS</p>
                </div>
            </div>
            <div class="col-sm-6 col-xs-3 stats-grid stat1">
                <!-- <div class='numscroller numscroller-big-bottom'>September 2022</div> -->
                <div class="stats-img stat2">
                    <p>ONLY 499 PRE-BOOKING SLOTS ON OFFER</p>
                </div>
            </div>
            <div class="col-sm-6 col-xs-3 stats-grid stat1">
                <!-- <div class='numscroller numscroller-big-bottom'>Rs.31,00,000 Onwards*</div> -->
                <div class="stats-img stat2">
                    <p>SECURE PAYMENTS</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- //stats --> 
    <!--about -->
    <div id="about" class="wthree-about section-w3ls">
        <div class="container">
      
            <h3 class="w3ls-title">
                <span>o</span>VERVIEW</h3>
            <h5>Welcome to world-class vacation homes!</h5>
            <div class="col-md-7  embed-container">
            <iframe autoplay class="fancybox-iframe" src="//player.vimeo.com/video/282298679?hd=1&show_title=1&show_byline=1&show_portrait=0&fullscreen=1&autoplay=0"></iframe>
            </div>
            <div class="col-md-5 w3ls-row">
                <div class="col-md-6 col-sm-6 wthree-about-left">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/a2.jpg" class="img-responsive" alt="" />
                </div>
                <div class="col-md-6 col-sm-6 w3ls-row alert wthree-about-right">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/overview-1.png" class="img-responsive" alt="" />
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>

        </div>
    </div>
    <!-- //about -->   

        <!-- menu -->
        <div class="w3ls-services section-w3ls">
        <div class="container">
            <h3 class="w3ls-title">
                <span>F</span>EATURES</h3>
            <div class="grid">
                <figure class="col-md-3 col-xs-6 w3l-service-hover">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/icon-cycle-track.jpg" alt="t1" class="img-responsive" />
                    <figcaption>
                        <h4>Cycling</h4>
                    </figcaption>
                </figure>
                <figure class="col-md-3 col-xs-6  w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/shopping.jpg" alt="t2" class="img-responsive" />
                    <figcaption>
                        <h4>SHOPPING</h4>
                    </figcaption>
                </figure>
    
                
                <figure class="col-md-3 col-xs-6  w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/icon-park.jpg" alt="t2" class="img-responsive" />
                    <figcaption>
                        <h4>Gardens</h4>
                    </figcaption>
                </figure>
                <figure class="col-md-3 col-xs-6 w3l-service-hover s4">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/feature_jogging.jpg" alt="t3" class="img-responsive" />
                    <figcaption>
                        <h4>Jogging Track</h4>
                    </figcaption>
                </figure>
                <div class="clearfix"></div>
        
     
            </div>
        </div>
    </div>
    <div>
    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/price1.jpg" alt="t1" class="img-responsive" />
    </div>
    <!-- //menu -->
            <!-- menu -->
            <div class="w3ls-services section-w3ls">
        <div class="container amenities-block">
            <h3 class="w3ls-title">
                <span>A</span>MENITIES</h3>
            <div class="grid">
     


                <figure class="col-md-3 col-xs-6 w3l-service-hover s3">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/f8.png" alt="t1" class="img-responsive" />
                    <figcaption>
                        <h4>Swimming Pool </h4>
                    </figcaption>
                </figure>
                <figure class="col-md-3 col-xs-6 w3l-service-hover s4">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/f9.png" alt="t1" class="img-responsive" />
                    <figcaption>
                        <h4> Kid's Pool</h4>
                    </figcaption>
                </figure>
             
                <figure class="col-md-3 col-xs-6  w3l-service-hover s1">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/f10.png" alt="t2" class="img-responsive" />
                    <figcaption>
                        <h4>Jogging Track</h4>
                    </figcaption>
                </figure>
   
                <figure class="col-md-3 col-xs-6 w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/f11.png" alt="t3" class="img-responsive" />
                    <figcaption>
                        <h4>Badminton Court</h4>
                    </figcaption>
                </figure>
                <div class="clearfix"></div>
       

                 <figure class="col-md-3 col-xs-6  w3l-service-hover s1">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/f14.png" alt="t2" class="img-responsive" />
                    <figcaption>
                        <h4>Pets corner</h4>
                    </figcaption>
                </figure>
                <figure class="col-md-3 col-xs-6 w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/f15.png" alt="t3" class="img-responsive" />
                    <figcaption>
                        <h4>Well-equipped Gymnasium</h4>
                    </figcaption>
                </figure>

          
                <figure class="col-md-3 col-xs-6 w3l-service-hover s4">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/f16.png" alt="t1" class="img-responsive" />
                    <figcaption>
                        <h4> Table Tennis</h4>
                    </figcaption>
                </figure>
             
                 <figure class="col-md-3 col-xs-6  w3l-service-hover s1">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/f17.png" alt="t2" class="img-responsive" />
                    <figcaption>
                        <h4>Meditation Room</h4>
                    </figcaption>
                </figure>
                <div class="clearfix"></div>

                <figure class="col-md-3 col-xs-6 w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/f18.png" alt="t3" class="img-responsive" />
                    <figcaption>
                        <h4>Aerobics Room</h4>
                    </figcaption>
                </figure>

                <figure class="col-md-3 col-xs-6 w3l-service-hover s3">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/f19.png" alt="t1" class="img-responsive" />
                    <figcaption>
                        <h4>Party Hall</h4>
                    </figcaption>
                </figure>
                <figure class="col-md-3 col-xs-6 w3l-service-hover s4">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/f21.png" alt="t1" class="img-responsive" />
                    <figcaption>
                        <h4> Steam Room & Sauna</h4>
                    </figcaption>
                </figure>
              
      

                <figure class="col-md-3 col-xs-6 w3l-service-hover s3">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/f24.png" alt="t1" class="img-responsive" />
                    <figcaption>
                        <h4>Provision for Shops</h4>
                    </figcaption>
                </figure>
            </div>
        </div>
    </div>
    <div>
    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/extra.jpg" alt="t1" class="img-responsive" />
    </div>
    <!-- //menu -->
    <!-- services -->
    <div class="panel-sec section-w3ls">
        <div class="container">
            <h3 class="agileits-title">HIGHLIGHTS</h3>
            <!-- timings -->
            <div class="w3l-about timing">
                <div class="col-md-4 w3_service_bottom_grid1">
                    <div class="about-w3left">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h5 class="panel-title asd">
                                        <a class="pa_italic collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false"
                                            aria-controls="collapseOne">
                                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                            <i class="glyphicon glyphicon-minus" aria-hidden="true"></i>Highlights
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false"
                                    style="height: 0px;">
                                    <div class="panel-body panel_text">
                                        <ul class="b-list">
                                        <li>Resort Like Living</li>
                  <li> Close to Hoodi, Whitefield</li>
                  <li>Low Density Project</li>
                  <li>Unhindered Lake View</li>
                  <li>Loaded with Amenities available only in Highend Resorts</li>
                  <li>20 to 25% Price Less than similar top Builder Projects</li>
   
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h5 class="panel-title asd">
                                        <a class="pa_italic collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                                            aria-controls="collapseTwo">
                                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                            <i class="glyphicon glyphicon-minus" aria-hidden="true"></i>About 
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false"
                                    style="height: 0px;">
                                    <div class="panel-body panel_text">
                                        <ul class="b-list">
                                        <li>Code Name : Take It Easy</li>
                                        <li>1, 2 & 3 Bed Condominiums and Duplexes</li>
                                        <li>645 Sqft. to 2150 Sqft.</li>
                                        <li>9 Acres</li>
                                        <li>No. of Apartments - 471</li>
                                        <li>Location : Whitefield Extension, 10 Mins to Hoodi</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                      
                        
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="col-md-7 col-sm-12 agile_timing_right">
                        
                        <ul>
                            <li>
                                <span>24Hr Backup</span></li>
                            <li>
                                <span>Club House </span></li>
                                <li>
                                <span>Security </span></li>
                            <li>
                                <span>Tennis Court </span></li>
                                <li>
                                <span>Badminton Court  </span></li>
                                <li>
                                <span>Community Hall  </span></li>
                                <li>
                                <span>Swimming Pool  </span></li>
                                <li>
                                <span>Wifi </span></li>
                                <li>
                                <span>Bank/Atm  </span></li>
                                <li>
                                <span>Garden  </span></li>
                        </ul>
                    </div>
                    <div class="col-md-5 menu-bg"></div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <!-- //timings -->
    </div>
    <!-- services -->
    <div class="w3ls-services section-w3ls">
        <div class="container">
            <h3 class="w3ls-title">
                <span>G</span>ALLERY</h3>
            <div class="grid">
                <figure class="col-md-3 col-xs-6 w3l-service-hover">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/Aerial-01.jpg" alt="t1" class="img-responsive" />
                  
                </figure>
                <figure class="col-md-3 col-xs-6  w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/Aerial-02.jpg" alt="t2" class="img-responsive" />
                    
                </figure>
    
                
                <figure class="col-md-3 col-xs-6  w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/b2.jpg" alt="t2" class="img-responsive" />
                
                </figure>
                <figure class="col-md-3 col-xs-6 w3l-service-hover s4">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/a3.jpg" alt="t3" class="img-responsive" />
                 
                </figure>
                <div class="clearfix"></div>
                <figure class="col-md-3 col-xs-6 w3l-service-hover">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/Bed-Room.jpg" alt="t1" class="img-responsive" />
                  
                </figure>
                <figure class="col-md-3 col-xs-6  w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/Club_Lobby.png" alt="t2" class="img-responsive" />
                    
                </figure>
    
                
                <figure class="col-md-3 col-xs-6  w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/Club-Cafe.jpg" alt="t2" class="img-responsive" />
                
                </figure>
                <figure class="col-md-3 col-xs-6 w3l-service-hover s4">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/Sky-Deck.jpg" alt="t3" class="img-responsive" />
                 
                </figure>
                <div class="clearfix"></div>
                <figure class="col-md-3 col-xs-6 w3l-service-hover">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/LivingDining.jpg" alt="t1" class="img-responsive" />
                  
                </figure>
                <figure class="col-md-3 col-xs-6  w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/Lscp-01.jpg" alt="t2" class="img-responsive" />
                    
                </figure>
    
                
                <figure class="col-md-3 col-xs-6  w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/Lscp-02.jpg" alt="t2" class="img-responsive" />
                
                </figure>
                <figure class="col-md-3 col-xs-6 w3l-service-hover s4">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/Lscp-03.jpg" alt="t3" class="img-responsive" />
                 
                </figure>
            </div>
        </div>
    </div>
    <!-- services bottom -->
    <div class="section-w3ls services">
        <div class="container">
            <div class="services-right">
                <div class="services-grid">
                  
                    <div class="col-md-12 col-sm-12 col-xs-12 sr-txt">
                        
                    <h5>Overview</h5>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="services-grid">                  
                    <div class="col-md-12 col-sm-12 col-xs-12 sr-txt">
                       
                        <p>“TAKE IT EASY” when it comes to buying from Shriram Properties since you would hardly have any doubt on the quality. Code Take It Easy is one such fantastic project located near Whitefield, off Hoodi Circle. Code Take It Easy spans 9 acres of land on which there are 471 apartments built with a futuristic design. The location of Code Take It Easy is as vibrant as any other prominent one in Bangalore with malls, schools and eateries within earshot.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="services-left">
                <h4 class="title">SPECIFICATIONS</h4>
                
                <p class="data">The project has 1, 2 and 3 bhk apartments starting from 645 sq ft and stretching up to 1650 sq ft. There is also a 3 bhk duplex sized about 2150 sq ft designed to immerse you in joy. There are plenty of amenities inside the community to make your life as enjoyable as possible.</p>
              
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!--//services bottom-->
    <div>
    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/location-map-01.jpg" alt="t1" class="img-responsive" />
    </div>
    <!-- testimonials -->
    <div class="testimonials section-w3ls" id="testimonials">
        <div class="container">
            <h3 class="w3ls-title">
                <span>A </span>SITE VISIT FOR FREE</h3>
            <div class="w3_testimonials_grids w3_testimonials_grids">
                <div class="sreen-gallery-cursual">
                    <div id="owl-demo" class="owl-carousel">
                        <div class="item-owl">
             
                            <div class="col-md-12 col-sm-12 col-xs-12 test-review test-tooltip1">
                            
                                <p>
                                    <i class="fa fa-quote-left" aria-hidden="true"></i> 
                                    Make a free site visit to Codename: Take It Easy by Shriram Properties along with special assistance from our sales personnel to get complete in-depth information about the property.
                                    <i class="fa fa-quote-right" aria-hidden="true"></i>
                                </p>
                               
                            </div>
                        </div>
              
                    </div>
                    <!--//screen-gallery-->
                </div>
            </div>
        </div>
    </div>
    <!-- //testimonials -->
       <!-- contact -->
       <div class="contact-bottom section-w3ls main-pos" id="contact">
        <div class="container">
            <h3 class="w3ls-title">
                <span>c</span>ontact us</h3>
         
            <div class="clearfix"></div>
            <div class="contact-main">
                <div class="col-md-6 col-sm-6 col-xs-6 contact-left-w3ls">
                    <h3 class="h3-w3l">contact info</h3>
        
                    <div class="visit contact-grid-agileinfo">
                        <div class="contact-icon-wthree">
                            <span class="fa fa-home" aria-hidden="true"></span>
                        </div>
                        <div class="contact-text-agileinfo">
                            <h4>CodeName Take It Easy</h4>
                            <p>Whitefield Extension,</p>
                            <p>Bangalore,</p>
                            <p>Karnataka</p>
                        
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 map">
                   
                    <iframe class="mapBorder" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7774.571361339026!2d77.72830188876777!3d13.017470881817513!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae11330a1a1177%3A0x35c3ef1de69db7e7!2sShriram+Code+Take+It+Easy!5e0!3m2!1sen!2sin!4v1533294043929" width="100%"  frameborder="0" allowfullscreen=""></iframe>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- //contact -->
       <!-- footer -->
       <div class="agile-footer w3ls-section">
        <div class="container">
       
            <div class="agileits_w3layouts-copyright">
                <p>© 2018  Shriram Properties. All rights reserved 
                    <!-- <a href="#" > Privacy Policy </a> | <a href="#" > Disclaimer </a> -->
                </p>
            </div>
        </div>
    </div>
    <!-- //footer -->
    <!-- contact form start -->
    <div class="floating-form visiable" id="contact_form">
    <div class="contact-opener">Enquire Now</div>
   <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="srirama_jsfrm('<?php echo SITE_URL?>codetakeiteasy/submit_frm','2')">
                    <div class="form-logo">
                                <a href="#">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/shriram_properties.png" />
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/logo.png" />
                                  
                                </a> 
                            </div> 
                            <h4> Literally, the best life Whitefield has to offer! </h4>
                    <div class="group">
                        <div class="form-group">
                       
                            <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid_puravankara('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err2"></span>
                        </div>
              
                    </div>  
                    <div class="group">
                    <div class="form-group">
                   
                    <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid_puravankara('email', 'Please enter correct email')" data-attr="Please enter correct email">
                    <span class="help-block" id="email_err2" ></span>
                    </div>
                    <div class="form-group">
                    <input type="hidden" class="hiddenCountry" name="CountryCode" id="CountryCode2" value="91">
                    <input type="text" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*" maxlength="10" placeholder="Mobile Number" onkeyup="chck_valid_puravankara('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                    <span class="help-block" id="phone_err2"> </span>
                    </div>
                
                    </div>      
                
                    
                    <div class="submitbtncontainer">
                    <input type="submit" id="frm-sbmtbtn2" value="Submit" name="submit" class="frm-sbm">
                    </div>
                </form>
    <div>
    <div class="popup-enquiry-form mfp-hide" id="popupForm">
    <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="srirama_jsfrm('<?php echo SITE_URL?>codetakeiteasy/submit_frm','3')">
                    <div class="form-logo">
                        <a href="#">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/shriram_properties.png" />
                            <img src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/images/logo.png" />

                        </a> 
                    </div> 
                            <h4> Literally, the best life Whitefield has to offer! </h4>
                    <div class="group">
                        <div class="form-group">
                       
                            <input type="text" class="form-control" id="name3" name="name" placeholder="Name" onkeyup="chck_valid_puravankara('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err3"></span>
                        </div>
                        <div class="form-group">
                   
                   <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid_puravankara('email', 'Please enter correct email')" data-attr="Please enter correct email">
                   <span class="help-block" id="email_err3" ></span>
                   </div>
                   <div class="form-group">
                    <input type="hidden" class="hiddenCountry" name="CountryCode" id="CountryCode3" value="91">
                    <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*" maxlength="10" placeholder="Mobile Number" onkeyup="chck_valid_puravankara('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                    <span class="help-block" id="phone_err3"> </span>
                    </div>
                    </div>  
                
                
                    
                    <div class="submitbtncontainer">
                    <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit" class="frm-sbm">
                    </div>
                </form>
    </div>    
    <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
    <input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
	<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
	<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
	<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">

    <!-- banner slider -->
    <script src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/js/modernizr-2.6.2.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/js/jquery.zoomslider.min.js"></script>
    <!-- //banner slider -->
    <!-- //gallery -->
    <script src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/js/jquery.tools.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/js/jquery.mobile.custom.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/js/jquery.cm-overlay.js"></script>
	<script src="<?php echo S3_URL?>/site/scripts/default.js"></script>
    <script src="<?php echo S3_URL?>/site/scripts/realesate.js"></script>
    <!-- //gallery -->
    <!-- testimonials -->
    <!-- required-js-files-->
	<!-- Magnific Popup core JS file -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/js/owl.carousel.js"></script>
   
    <!--//required-js-files-->
    <!-- start-smooth-scrolling -->
    <script src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/js/move-top.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/js/easing.js"></script>

    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
  
    <script src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/js/SmoothScroll.min.js"></script>
    <!-- //smooth-scrolling-of-move-up -->
    <script src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/js/SmoothScroll.min.js"></script>
    <!-- navigation  -->
    <script src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/js/intlTelInput.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/codetakeiteasy/js/main.js"></script>

   </body>
</html>