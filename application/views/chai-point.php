<!DOCTYPE html>
<?php define('S3_URL_NEW', 'https://clpcdn.s3-ap-southeast-1.amazonaws.com/cdn'); ?>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>boxC</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="google-site-verification" content="0dbmPCmO4667-u3ow1vZI8ma61UpYpOFJIso_aqvUzs" />
<link rel="shortcut icon" href="<?php echo S3_URL?>/site/chai-point-assets/images/favicon.png" type="image/x-icon" />
<link rel="stylesheet" href="<?php echo S3_URL?>/site/chai-point-assets/css/vendor.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
<link rel="stylesheet" href="<?php echo S3_URL?>/site/chai-point-assets/css/main.css">
<link rel="stylesheet" href="<?php echo S3_URL?>/site/chai-point-assets/css/slider.css">
<link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
<!-- fonts -->
<link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
            rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

<!-- Taboola Pixel Code -->
<script type='text/javascript'>
  window._tfa = window._tfa || [];
  window._tfa.push({notify: 'event', name: 'page_view', id: 1155066});
  !function (t, f, a, x) {
         if (!document.getElementById(x)) {
            t.async = 1;t.src = a;t.id=x;f.parentNode.insertBefore(t, f);
         }
  }(document.createElement('script'),
  document.getElementsByTagName('script')[0],
  '//cdn.taboola.com/libtrc/unip/1155066/tfa.js',
  'tb_tfa_script');
</script>
<noscript>
  <img src='//trc.taboola.com/1155066/log/3/unip?en=page_view'
      width='0' height='0' style='display:none'/>
</noscript>
<!-- End of Taboola Pixel Code -->

<!-- Global site tag (gtag.js) - Google Ads: 790151098 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-790151098"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-790151098');
</script>

<body onunload="openPopup()">
<?php $all_array = all_arrays(); ?>
<!-- banner -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <div class="container">
        <div class="carousel-caption baner-mob">
          <form role="form" id="feedbackForm" class="feedbackForm form_hide" action="JavaScript:void(0)" onsubmit="chaipoint_jsfrm('<?php echo SITE_URL?>chaipoint/frm_submit','1','<?php echo SITE_URL?>chaipoint')">
            <div class="">
              <div class="col">
                <div class="form-logo"> <a href="#"> <img src="<?php echo S3_URL?>/site/chai-point-assets/images/boxc.png" /> </a> </div>
              </div>
              <div class="col">
                <div class="group">
                  <div class="form-group">
                    <input type="text" class="form-control" id="name1" name="name" placeholder="Name" autofocus="autofocus" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                    <span class="help-block" id="name_err1"></span> </div>
                </div>
                <div class="group">
                  <div class="form-group">
                    <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                    <span class="help-block" id="email_err1" ></span> </div>
                  <div class="form-group">
                    <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                    <span class="help-block" id="phone_err1"> </span>  </div>
                </div>

                <div class="group">
                  <div class="form-group">
                    <input type="text" class="form-control" id="company1" name="company" placeholder="Company Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                    <span class="help-block" id="company_err1"></span>
                  </div>
                </div>

                <div class="group">
                  <div class="form-group">
                    <select name="count" id="employee1" class="form-control">
                      <option value="" >Select Employee Count</option>
                      <option value="upto30">1 - 30</option>
                      <option value="30plus">31 - 50</option>
                      <option value="50plus">51 - 100</option>
                      <option value="100plus">101 - 200</option>
                      <option value="200Plus">More Than 200</option>
                    </select>
                    <span class="help-block" id="employee_err1"></span> </div>
                </div>

                <div class="group">
                  <div class="form-group">
                    <!-- <input type="text" class="form-control" id="city1" name="city" placeholder="Enter City" onkeyup="chck_valid('name', 'Please enter correct City')" data-attr="Please enter correct City"> -->
                    <select name="city" id="city1" class="form-control">
                      <option value="" >Select City</option>
                      <option value="ahmedabad">Ahmedabad</option>
                      <option value="bangalore">Bangalore</option>
                      <option value="chennai">Chennai</option>
                      <option value="delhi">Delhi</option>
                      <option value="gurgaon">Gurgaon</option>
                      <option value="hyderabad">Hyderabad</option>
                      <option value="indore">Indore</option>
                      <option value="kolkata">Kolkata</option>
                      <option value="mumbai">Mumbai</option>
                      <option value="pune">Pune</option>
                      <option value="vadodara">Vadodara</option>
                      <option value="others">Others</option>
                    </select>
                    <span class="help-block" id="city_err1"></span> </div>
                </div>
                
                
                 <div class="group">
                  <div style="display:flex;font-size:10px;line-height:1.2;padding-bottom: 10px">
                <input type="checkbox" checked="{agree}">
                <div style="padding-left: 5px; color:#000">I agree and authorize team to contact me. This will override the registry with DNC / NDNC</div>
              </div>
                </div>
                
                <!--<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
					<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
					<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">-->
                <div class="submitbtncontainer">
                  <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
                </div>
              </div>
            </div>
          </form>
          
          <form role="form" id="feedbackForm" class="feedbackForm mobile_form" action="JavaScript:void(0)" onsubmit="chaipoint_jsfrm('<?php echo SITE_URL?>chaipoint/frm_submit','4','<?php echo SITE_URL?>chaipoint')">
            <div class="">
              <div class="col">
                <div class="form-logo"> <a href="#"> <img src="<?php echo S3_URL?>/site/chai-point-assets/images/boxc.png" /> </a> </div>
              </div>
              <div class="col">
                <div class="group">
                  <div class="form-group">
                    <input type="text" class="form-control" id="name4" name="name" placeholder="Name" autofocus="autofocus" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                    <span class="help-block" id="name_err4"></span> </div>
                </div>
                <div class="group">
                  <div class="form-group">
                    <input type="email" class="form-control" id="email4" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                    <span class="help-block" id="email_err4" ></span> </div>
                  <div class="form-group">
                    <input type="text" class="form-control only_numeric phone" id="phone4" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                    <span class="help-block" id="phone_err4"> </span> </span> </div>
                    
                    <div class="group">
                    <div class="form-group">
                    <input type="text" class="form-control" id="company4" name="company" placeholder="Company Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                    <span class="help-block" id="company_err4"></span>
                    </div>
                  </div>

                  <div class="group">
                    <div class="form-group">
                      <select name="count" id="employee4" class="form-control">
                        <option value="" >Select Employee Count</option>
                        <option value="upto30">1 - 30</option>
                        <option value="30plus">31 - 50</option>
                        <option value="50plus">51 - 100</option>
                        <option value="100plus">101 - 200</option>
                        <option value="200Plus">More Than 200</option>
                      </select>
                      <span class="help-block" id="employee_err4"></span>
                    </div>
                  </div>

                  <div class="group">
                        <div class="form-group">
                            <!-- <input type="text" class="form-control" id="city4" name="city" placeholder="Enter City" onkeyup="chck_valid('name', 'Please enter correct City')" data-attr="Please enter correct City"> -->
                            <select name="city" id="city4" class="form-control">
                              <option value="" >Select City</option>
                              <option value="ahmedabad">Ahmedabad</option>
                              <option value="bangalore">Bangalore</option>
                              <option value="chennai">Chennai</option>
                              <option value="delhi">Delhi</option>
                              <option value="gurgaon">Gurgaon</option>
                              <option value="hyderabad">Hyderabad</option>
                              <option value="indore">Indore</option>
                              <option value="kolkata">Kolkata</option>
                              <option value="mumbai">Mumbai</option>
                              <option value="pune">Pune</option>
                              <option value="vadodara">Vadodara</option>
                              <option value="others">Others</option>
                            </select>
                            <span class="help-block" id="city_err4"></span> 
                        </div>
                    </div>

                    <div class="group">
                        <div class="form-group">
                            <input type="text" class="form-control" id="city4" name="city" placeholder="Enter City" onkeyup="chck_valid('name', 'Please enter correct City')" data-attr="Please enter correct City">
                            <span class="help-block" id="city_err4"></span> 
                        </div>
                    </div>
                
                </div>
                
                 <div class="group">
                  <div style="display:flex;font-size:10px;line-height:1.2;padding-bottom: 10px">
                <input type="checkbox" checked="{agree}">
                <div style="padding-left: 5px; color:#000">I agree and authorize team to contact me. This will override the registry with DNC / NDNC </div>
              </div>
                </div>
                
                <!--<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
					<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
					<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">-->
                <div class="submitbtncontainer">
                  <input type="submit" id="frm-sbmtbtn4" value="Submit" name="submit">
                </div>
              </div>
            </div>
          </form>
        
          
        </div>
      </div>
    </div>
  </div>
  
  <!-- The Modal --> 
</div>
<!--//banner --> 

<!--Overview --> 

<div class="about" id="about">
    <div class="container">
        <div class="col-md-12">
            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/natural_ingredients.png" alt="group" >
                <h3 class="marginTop30 greenTxt">100% Natural Ingredients</h3>
                <p class="marginTop10">Fresh Milk & Fresh Brewed</p>
            </div>
            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/billing_transparency.png" alt="group" >
                <h3 class="marginTop30 greenTxt">Billing Tranparency</h3>
                <p class="marginTop10">With a Cloud Based Platform</p>
            </div>
            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/world_class_service.png" alt="group" >
                <h3 class="marginTop30 greenTxt">World Class Service</h3>
                <p class="marginTop10">Through IoT Enabled Dispensers</p>
            </div>
            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/right_pricing.png" alt="group" >
                <h3 class="marginTop30 greenTxt">Right Pricing</h3>
                <p class="marginTop10">Great Pricing per Cup</p>
            </div>
        </div>
    </div>
</div>

<!--//Overview --> 


<!-- about -->
<div class="about" id="about" style="background-color: #edf2f8;">
  <div class="container">
    <div class="col-md-6 about_right marginTop50 aboutRightBorder">  
        <img src="<?php echo S3_URL?>/site/chai-point-assets/images/chai_dispenser.jpg" alt="group" width="100%">
    </div>
    <div class="col-md-6 about-left marginTop50 aboutParagraph">
    <div class="col-md-8 col-sm-12 marginTo3m">

        <h2 class="leftText headStyles">Fresh Milk Based Authentic Chai and Coffee Dispensers</h2>

        <ul class="marginTo1m machineFeatures">
          <li>100% natural, freshly brewed</li>
          <li> Password protected settings </li>
          <li> Repair services within 24 hours </li>
          <li> 100% process compliant & certified </li>
          <li> Smart billing & invoicing solution </li>
        </ul>

    </div>
        

    </div>
    <div class="clearfix"> </div>
  </div>
</div>


<div class="about" id="about">
  <div class="container">
    <div class="col-md-6 about-left marginTop50 aboutParagraph">

    <div class="col-md-9 col-sm-12 pullRight marginTo3m">

        <h2 class="leftText headStyles">Integrated Fresh Milk Chai and Coffee Dispensers </h2>

        <ul class="marginTo1m machineFeatures">
          <li> 100% natural, freshly brewed </li>
          <li> Android powered, cloud connected </li>
          <li> Self-indicating preventive maintenance </li>
          <li> Live consumption tracking </li>
          <li> Smart billing & invoicing solution </li>
        </ul>
        
    </div>

        
    </div>

    <div class="col-md-6 about_right marginTop50 aboutLeftBorder">  
        <img src="<?php echo S3_URL?>/site/chai-point-assets/images/integrated_dispenser.jpg" alt="group" width="100%">
    </div>
    
    <div class="clearfix"> </div>
  </div>
</div>

<!-- //about --> 
<!-- popular -->

<!-- //popular --> 


<div class=""  style="padding:50px 0px; background-color: #E9F0F8;">
    <div class="container">

        <div class="col-md-12">
            
            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/hot_beverage.jpg" alt="group">
            </div>

            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/chai.jpg" alt="group">
            </div>

            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/filter-coffee.jpg" alt="group">
            </div>

            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/hot_beverage_faded.jpg" alt="group">
            </div>
            
        </div>

        <div class="col-md-12 marginTop20 mobileDisplay">
            
        <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/tea_bag.jpg" alt="group">
            </div>

            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/black-tea.jpg" alt="group">
            </div>

            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/green-tea.jpg" alt="group">
            </div>

            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/tea.jpg" alt="group">
            </div>
            
            
        </div>

        <div class="col-md-12 marginTop20 webDisplay">
            
            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/tea.jpg" alt="group">
            </div>

            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/black-tea.jpg" alt="group">
            </div>

            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/green-tea.jpg" alt="group">
            </div>

            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/tea_bag.jpg" alt="group">
            </div>
            
        </div>

        <div class="col-md-12 marginTop20">
            
            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/malt.jpg" alt="group">
            </div>

            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/hot-chocolate.jpg" alt="group">
            </div>

            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/badam-turmeric.jpg" alt="group">
            </div>

            <div class="col-md-3 col-sm-12">
                <img src="<?php echo S3_URL?>/site/chai-point-assets/images/malt_faded.jpg" alt="group">
            </div>

            <span id="makeNewEnquiry"></span>
            
        </div>

    </div>
</div>
	


<footer class="bg-bark">

  <div class="container">
    <div class="col-md-12 copy-right-grids marginTop20">
      
      <div class="col-md-12 footer-about http://panelss.in/Dropbox/trunk/clients_lp/nmims/chaipoint">
          <h3>About boxC.in</h3>
          <p>
              For millions of working professionals in India, finding a good cup of Chai is a major challenge. Chai Point has introduced India’s first cloud based platform for beverage services.
          </p>
          <br>          
      </div>      
    </div>
  </div>
</footer>



<div class="floating-form" id="contact_form">
<div class="contact-opener">Enquire Now</div>
<form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="chaipoint_jsfrm('<?php echo SITE_URL?>chaipoint/frm_submit','2','<?php echo SITE_URL?>chaipoint')">
  <div class="">
    <div class="col text-center">
      <div class="form-logo"> <a href="#"> <img src="<?php echo S3_URL?>/site/chai-point-assets/images/boxc.png" /> </a> </div>
    </div>
    <div class="col">
      <div class="groups">
        <div class="form-group">
          <input type="text" class="form-control" id="name2" name="name" placeholder="Name" autofocus="autofocus" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
          <span class="help-block" id="name_err2"></span> </div>
      </div>
      <div class="groups">
        <div class="form-group">
          <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
          <span class="help-block" id="email_err2" ></span> </div>
        <div class="form-group">
          
          <input type="text" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
          <span class="help-block" id="phone_err2"> </span>  </div>

                  <div class="group">
          <div class="form-group">
          <input type="text" class="form-control" id="company2" name="company" placeholder="Company Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
          <span class="help-block" id="company_err2"></span>
          </div>
        </div>

        <div class="group">
          <div class="form-group">
            <select name="count" id="employee2" class="form-control">
              <option value="" >Select Employee Count</option>
              <option value="upto30">1 - 30</option>
              <option value="30plus">31 - 50</option>
              <option value="50plus">51 - 100</option>
              <option value="100plus">101 - 200</option>
              <option value="200Plus">More Than 200</option>
            </select>
            <span class="help-block" id="employee_err2"></span>
          </div>
        </div>

        <div class="group">
            <div class="form-group">
            <!-- <input type="text" class="form-control" id="city2" name="city" placeholder="Enter City" onkeyup="chck_valid('name', 'Please enter correct City')" data-attr="Please enter correct City"> -->
            <select name="city" id="city2" class="form-control">
              <option value="" >Select City</option>
              <option value="ahmedabad">Ahmedabad</option>
              <option value="bangalore">Bangalore</option>
              <option value="chennai">Chennai</option>
              <option value="delhi">Delhi</option>
              <option value="gurgaon">Gurgaon</option>
              <option value="hyderabad">Hyderabad</option>
              <option value="indore">Indore</option>
              <option value="kolkata">Kolkata</option>
              <option value="mumbai">Mumbai</option>
              <option value="pune">Pune</option>
              <option value="vadodara">Vadodara</option>
              <option value="others">Others</option>
            </select>
            <span class="help-block" id="city_err2"></span> </div>
        </div>


      </div>
      <div class="submitbtncontainer">
        <input type="submit" id="frm-sbmtbtn2" value="Submit" name="submit">
      </div>
    </div>
  </div>
</form>
<div>
<div class="popup-enquiry-form mfp-hide" id="popupForm">
  <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="chaipoint_jsfrm('<?php echo SITE_URL?>chaipoint/frm_submit','3','<?php echo SITE_URL?>chaipoint')">
    <div class="">
      <div class="col">
        <div class="form-logo"> <a href="#"> <img src="<?php echo S3_URL?>/site/chai-point-assets/images/boxc.png" /> </a> </div>
      </div>
      <div class="col">
        <div class="groups">
          <div class="form-group">
            <input type="text" class="form-control" id="name3" name="name" placeholder="Name" autofocus="autofocus" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
            <span class="help-block" id="name_err3"></span> </div>
          <div class="form-group">
            <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
            <span class="help-block" id="email_err3" ></span> </div>
        </div>
        <div class="groups">
          <div class="form-group">
            
            <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
            <span class="help-block" id="phone_err3"> </span></div>
        </div>


        <div class="group">
          <div class="form-group">
          <input type="text" class="form-control" id="company3" name="company" placeholder="Company Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
          <span class="help-block" id="company_err3"></span>
          </div>
        </div>

        <div class="group">
          <div class="form-group">
            <select name="count" id="employee3" class="form-control">
              <option value="" >Select Employee Count</option>
              <option value="upto30">1 - 30</option>
              <option value="30plus">31 - 50</option>
              <option value="50plus">51 - 100</option>
              <option value="100plus">101 - 200</option>
              <option value="200Plus">More Than 200</option>
            </select>
            <span class="help-block" id="employee_err3"></span>
          </div>
        </div>

        <div class="group">
            <div class="form-group">
            <!-- <input type="text" class="form-control" id="city3" name="city" placeholder="Enter City" onkeyup="chck_valid('name', 'Please enter correct City')" data-attr="Please enter correct City"> -->
            <select name="city" id="city3" class="form-control">
              <option value="" >Select City</option>
              <option value="ahmedabad">Ahmedabad</option>
              <option value="bangalore">Bangalore</option>
              <option value="chennai">Chennai</option>
              <option value="delhi">Delhi</option>
              <option value="gurgaon">Gurgaon</option>
              <option value="hyderabad">Hyderabad</option>
              <option value="indore">Indore</option>
              <option value="kolkata">Kolkata</option>
              <option value="mumbai">Mumbai</option>
              <option value="pune">Pune</option>
              <option value="vadodara">Vadodara</option>
              <option value="others">Others</option>
            </select>
            <span class="help-block" id="city_err3"></span> </div>
        </div>
        

        <div class="submitbtncontainer"> 
          <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
        </div>
      </div>
    </div>
  </form>
</div>
<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
<script src="<?php echo S3_URL?>/site/chai-point-assets/js/vendor.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script> 
<script src="<?php echo S3_URL?>/site/chai-point-assets/js/main.js"></script> 
   
<script>

/*var inFormOrLink;
$('a').on('click', function() { inFormOrLink = true; });
$('form').on('submit', function() { inFormOrLink = true; });

$(window).on("beforeunload", function() { 
    return inFormOrLink ? "Do you really want to close?" : null; 
})*/

</script>

    
    
</body>
</html>