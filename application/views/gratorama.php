<?php $all_array = all_arrays(); 

?>
<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Gratorama - Wizard Fortune</title>
      
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/gratorama/images/favicon.png" type="image/x-icon" />

	  <link rel="stylesheet" href="<?php echo S3_URL?>/site/gratorama/css/vendor.css">
	  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/gratorama/css/main.css">    
      <!-- fonts -->
      <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
            rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
   </head>
   <body>

    <?php $all_array = all_arrays(); ?>      

	<!-- banner -->
    <div class="side-bar-image">
      
            <img src="<?php echo S3_URL?>/site/gratorama/images/sidebanner.png" />
       
    </div> 
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="">
          <div id ="message_show" style="color: red;">
       </div>
					<div class="carousel-caption" id="form_show">
                    <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="gratorama_jsfrm('<?php echo SITE_URL?>gratorama/submit_frm')" >
                   
                    <div class="">       
                    <div class="col">
                           <!-- <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/gratorama/images/logo-small.png" />
                                </a> 
                            </div>  -->
                                                     
                            </div> 
                        <div class="col">   
                        <div class="form-group">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct firstname')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err"></span>
                        </div>
                   
                        <div class="form-group">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                            <span class="help-block" id="email_err" ></span>
                        </div>
                        
                        <div class="form-group">
                            <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode">
                            <input type="text" class="form-control only_numeric phone" id="phone" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                            <span class="help-block" id="phone_err"> </span>
                            
                        </div>
                        <div class="form-group">
                            <input type="text" name="city" placeholder="City" data-attr="Please enter correct city" id="city" class="form-control .only_numeric" onkeyup="chck_valid('city', 'Please enter correct city')">
							<span class="help-block" id="city_err"> </span>
                            
                        </div>
                 
                       
                      
                    <div class="submitbtncontainer">
                         <button type="submit" id="frm-sbmtbtn1" name="submit" class="btn-img">
                            <img src="<?php echo S3_URL?>/site/gratorama/images/button.gif" />
                         </button>

                    </div>
                    </div>
                    </div>
                </form>
					</div>
				</div>
			</div>
		</div>

		<!-- The Modal -->
	</div>
    <!--//banner -->    
	

	<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
	<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
	<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
	<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
	<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
	
	
	<script src="<?php echo S3_URL?>/site/gratorama/js/vendor.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>  
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
    
    <script src="<?php echo S3_URL?>/site/gratorama/js/main.js"></script>
	<script src="<?php echo S3_URL?>/site/scripts/gratorama.js"></script>
   
	 
   </body>
</html>