<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Instant Save Bills</title>
    <link rel="shortcut icon" href="<?php echo S3_URL?>/site/handdybook_assets/images/fav.png" type="image/x-icon">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/handdybook_assets/css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
</head>
<style>
    .error_class{
        color: #ff5454;
        text-align: left;
        margin-left: 0.5em;
    }
</style>
<body>
    <?php 
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url_components = parse_url($actual_link); 
        if(isset($url_components['query'])){
            parse_str($url_components['query'], $params); 
        }
        //echo $params['id']; exit;
        ?>
    <section id="banner">
        <div class="contain">
            <div class="col-md-12">
                <div class="col-md-7 mobilePaddingZero">
                    <nav class="navbar">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <a class="navbar-brand" href="<?php echo base_url()."index.php/Handdybook/";?>"><img src="<?php echo S3_URL?>/site/handdybook_assets/images/Logo.png" alt="" srcset=""></a>
                            </div>
                        </div>
                    </nav>
                    <div class="content">
                        <h1 >We care about people... <br>and their bills!</h1>
                        <p class="description">Chill while your bill-pal brings all the great deals for you. You are about to change your life forever…</p>
                        <p class="description">Enjoy this <span ><b>FREE, painless</b></span>  assistance for your <span><b>LIFETIME</b></span></p>

                        <div class="work">
                            <div class="col-md-12">
                                <div class="col-md-8">
                                    <div class="howWework">
                                        <h3>How We Work</h3>
                                        <p><span><img src="<?php echo S3_URL?>/site/handdybook_assets/images/checkbok.svg" alt="" srcset="">Register with 3 basic details only, to get started!</span></p>
                                        <p><span><img src="<?php echo S3_URL?>/site/handdybook_assets/images/checkbok.svg" alt="" srcset="">Just sip on your coffee & relax. We get the deals for you.</span></p>
                                        <p><span><img src="<?php echo S3_URL?>/site/handdybook_assets/images/checkbok.svg" alt="" srcset="">We tell you what the deal is & switch you to it if you like it.</span></p>
                                        <p><span><img src="<?php echo S3_URL?>/site/handdybook_assets/images/checkbok.svg" alt="" srcset="">Deal ends, but over service wont. So, relax once again!</span></p>
                                    </div>
                                    
                                </div>
                                <div class="col-md-4">
                                    <!-- <img src="../assets/images/Group 84.png"  class="img-responsive" alt=""> -->
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-5 inputFormSec mobilePaddingZero">
                    <div class="panel">
                        <h1>Join over 200,000 members</h1>
                        <div class="formset">
                            <form method="POST" id="join_form">
                                <?php if(isset($params['utm_source'])){ ?>
                                <input type="hidden" id="utm_source" name="utm_source" value="<?php echo $params['utm_source']; ?>">
                                <?php } 
                                if(isset($params['utm_medium'])){ 
                                ?>
                                <input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo $params['utm_medium']; ?>">
                                <?php } 
                                if(isset($params['utm_sub'])){ 
                                ?>
                                <input type="hidden" id="utm_sub" name="utm_sub" value="<?php echo $params['utm_sub']; ?>">
                                <?php } 
                                if(isset($params['utm_campaign'])){ 
                                ?>
                                <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo $params['utm_campaign']; ?>">
                                <?php } ?>
                                <div class="form-group">
                                    <lable class="form-label">Name</lable>
                                    <input type="text" class="form-control" name="name" id="name" autofocus>
                                </div>
                                <div id="name-error" class="error_class"></div>
                                <div class="form-group">
                                    <lable class="form-label">Email Id</lable>
                                    <input type="text" class="form-control" name="email" id="email" autofocus>
                                </div>
                                <div id="email-error" class="error_class"></div>
                                <div class="form-group">
                                    <lable class="form-label">Phone Number</lable>
                                    <input class="form-control" type="tel" id="contact_no" name="contact_no" />
                                </div>
                                <div id="contact_no-error" class="error_class"></div>
                                <div class="form-group">
                                    <lable class="form-label">Pincode</lable>
                                    <input class="form-control" type="text" id="pincode" name="pincode" />
                                </div>
                                <div id="pincode-error" class="error_class"></div>

                                <div class="checkbox">
                                    <label><input type="checkbox" id="tips" name="tips" checked disabled>By ticking here, you agree we can send you occasional emails with incredibly helpful money saving tips.</label>
                                </div>
                                <button class="btn" type="submit" id="getQuote">Get your quote in 30 seconds</button>
                            </form>

                            <p class="policy">
                                Our <span> Privacy Policy </span>outlines how we use your data and where it goes. Please make sure you read it before moving ahead with the enquiry.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>       
    </section>
</body>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="<?php echo S3_URL?>/site/handdybook_assets/js/script.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script>

////////////____Input Focus___//////////////////

    $(document).ready(function () {
        $('input[type=text]').val('');
        $('input[type=tel]').val('');
    });

    $('.form-control').focusout(function() {
        $('.form-group').removeClass('focus');
    });
    $('.form-control').focus(function() {
        $(this).closest('.form-group').addClass('focus');
    });

/// Input Kepress Filled  Focus

        $('.form-control').keyup(function() {
            if($(this).val().length > 0){
                $(this).closest('.form-group').addClass('filled');
            }

            else{
                $(this).closest('.form-group').removeClass('filled');
            }
        });                     

/// Input Check Filled Focus

    var $formControl = $('.form-control');
    var values = {};
    var validate =    $formControl.each(function() {
        if($(this).val().length > 0){
            $(this).closest('.form-group').addClass('filled');
        }
        else{
            $(this).closest('.form-group').removeClass('filled');
        }
    });

// Button switching

    // $('.caption').click(function(){
    // $(this).closest('.log-form').addClass('open');
    // });


    // $('.close').click(function(){
    // $(this).closest('.log-form').removeClass('open');
    // });
   

</script>

<script>
    $(document).ready(function(){
 
        //submit join form
        $('#join_form').submit(function(event){
            event.preventDefault();
            //get the input data
            input = $('#join_form').serialize();
            base_url = '<?php echo base_url(); ?>';
            $('#join_form .error_class').empty();
            //submit
            $.ajax({
                type: 'POST',
                url: base_url+"/index.php/Handdybook/submit_join_form",
                data: input,
                success : function(response){ 
                    if(response.status == false){
                        $.each(response.data['error'], function(key, value){
                            $('#join_form #'+key+'-error').html(value);
                        });
                    }else if(response.status == true){
                        $('#getQuote').text('Getting Your Quote …').button('refresh');
                        $.alert({
                            title: 'Thank You',
                            btnClass: 'ok',
                            content: 'We will get back to you soon',
                            buttons: {
                                ok: {
                                    btnClass: 'btn-blue',
                                    action: function(){
                                        window.location.href = "questions";
                                    }
                                },
                            }
                        });
                    }
                } 
            });
        });
    });
</script>
</html>