
<!doctype html>
<html amp>
<head>
<meta charset=UTF-8>
<meta name=viewport content="width=device-width, initial-scale=1">
<link rel=icon type="image/png" id=favicon href="https://dg0qqklufr26k.cloudfront.net/wp-content/uploads/2018/06/cropped-fav-icon1-32x32.png"/>
<title>New Title</title>

<link rel=stylesheet href="<?php echo SITE_URL?>cdn/site/mfine-assets/css/all.css">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style>
<script async src="<?php echo SITE_URL?>cdn/site/mfine-assets/js/v0.js"></script>
<script src="http://www.amazepromos.com/cdn/site/scripts/jquery-1.11.3.min.js"></script>
<script async src="<?php echo SITE_URL?>cdn/site/scripts/default.js"></script>



<noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<link rel="stylesheet" href="<?php echo SITE_URL?>cdn/site/mfine-assets/css//style.css">

</head>
<body class="page-template page-template-template-generic-lp page-template-template-generic-lp-php page page-id-53856 page-parent et_pb_button_helper_class et_fixed_nav et_show_nav et_cover_background et_pb_gutter linux et_pb_gutters3 et_primary_nav_dropdown_animation_fade et_secondary_nav_dropdown_animation_fade et_pb_footer_columns4 et_header_style_left et_right_sidebar et_divi_theme et_minified_js et_minified_css">
<section class=banner>
<amp-img src="<?php echo SITE_URL?>cdn/site/mfine-assets/images/mfine_banner.jpg" alt="" width=1366 height=650 layout=responsive class="d-none d-lg-block"></amp-img>
<amp-img src="<?php echo SITE_URL?>cdn/site/mfine-assets/images/mfine_mobile_banner.jpg" alt="" width=480 height=327 layout=responsive class="d-block d-lg-none"></amp-img>
<div class=banner-caption>
<div class=container>
<div class=row>
<div class="col-12 col-lg-4 offset-lg-8">
<div class=form-holder id=form>
<h2 class="proxi-semi mt-0 mb-0 text-uppercase fs-24 text-center position-relative">Thank you </h2>
<h3 class="proxi-semi mt-10 mb-0 fs-21 text-center position-relative">for your interest in Mfine Healthcare</h3>



</div>
</div>
</div>
</div>
</div>
</section>
<div class="d-none d-lg-block">
<section class="checkup-section pt-40 pb-40">
<div class=container>
<div class=row>
<div class=col-12>
<h2 class="text-center fs-21 mb-30 proxi-bold text-uppercase">Available Wellness Packages</h2>
</div>
<div class=col-4>
<div class=white-box>
<h3 class="text-center mt-0 mb-10 section-title fs-24 text-uppercase proxi-bold">Full Body check</h3><br>
<p class="fs-18 text-center mb-30">51 Parameters</p>
<ul class="has-and-more mb-0 text-center list-unstyled">
<li>Complete Blood Count</li>
<li>Liver Profile</li>
<li>Diabetes Profile</li>
<li>Thyroid Profile</li>
<li>Lipid Profile</li>
<li>Kidney Profile</li>
<li>Iron Deficiency Profile</li>
<li>Homocysteine</li>
<li>and more</li>
</ul>
<p class="fs-21 color-blue mt-30 proxi-bold text-center">Tests starting at Rs.799</p>
<p class="mb-0 text-center"><a on="tap:form.scrollTo(duration=1000),AMP.setState({optionone:true,optiontwo:false,optionthree:false,optionfour:false,className:'d-block',otpName:'d-none',OTP:''})" tabindex=0 class="btn btn-secondary text-uppercase">Book the Test</a></p>
</div>
</div>
<div class=col-4>
<div class=white-box>
<h3 class="text-center mt-0 mb-10 section-title fs-24 text-uppercase proxi-bold">Advanced Full Body Check - Men</h3>
<p class="fs-18 text-center mb-30">75 Parameters</p>
<ul class="mb-0 list-unstyled text-center">
<li>Liver Profile</li>
<li>Kidney Profile</li>
<li>Diabetic Profile</li>
<li>Lipid Profile</li>
<li>Thyroid Profile</li>
<li>Urine Analysis</li>
<li class=no-bullet>&nbsp;</li>
<li class=no-bullet>&nbsp;</li>
<li class=no-bullet>&nbsp;</li>
</ul>
<p class="fs-21 color-blue mt-30 proxi-bold text-center">Tests starting at Rs.1299</p>
<p class="mb-0 text-center"><a on="tap:form.scrollTo(duration=1000),AMP.setState({optiontwo:true,optionone:false,optionthree:false,optionfour:false,className:'d-block',otpName:'d-none',OTP:''})" role=button tabindex=0 class="btn btn-secondary text-uppercase">Book the Test</a></p>
</div>
</div>
<div class=col-4>
<div class=white-box>
<h3 class="text-center mt-0 mb-10 section-title fs-24 text-uppercase proxi-bold">Advanced Full Body Check - Women</h3>
<p class="fs-18 text-center mb-30">80 Parameters</p>
<ul class="has-and-more list-unstyled mb-0 text-center">
<li>Complete Blood Count</li>
<li>Inflammation Marker</li>
<li>Liver Profile</li>
<li>Kidney Profile</li>
<li>Diabetic Profile</li>
<li>Lipid Profile</li>
<li>Thyroid Profile</li>
<li>Urine Analysis</li>
<li class=no-bullet>and more</li>
</ul>
<p class="fs-21 color-blue mt-30 proxi-bold text-center">Tests starting at Rs.1599</p>
<p class="mb-0 text-center"><a on="tap:form.scrollTo(duration=1000),AMP.setState({optionthree:true,optiontwo:false,optionone:false,optionfour:false,className:'d-block',otpName:'d-none',OTP:''})" role=button tabindex=0 class="btn btn-secondary text-uppercase">Book the Test</a></p>
</div>
</div>
<div class="col-4 offset-4 mt-30">
<div class=white-box>
<h3 class="text-center mt-0 mb-10 section-title fs-24 text-uppercase proxi-bold">Alcohol Risk Assessment</h3>
<p class="fs-18 text-center mb-30">51 Parameters</p>
<ul class="has-and-more mb-0 list-unstyled text-center">
<li>Liver Profile</li>
<li>Pancreatic Profile</li>
<li>Lipid Profile</li>
<li>Kidney Profile</li>
<li>Diabetic Profile</li>
<li class=no-bullet>&nbsp;</li>
<li class=no-bullet>&nbsp;</li>
<li class=no-bullet>&nbsp;</li>
<li class=no-bullet>&nbsp;</li>
</ul>
<p class="fs-21 color-blue mt-30 proxi-bold text-center">Tests starting at Rs.1249</p>
<p class="mb-0 text-center"><a on="tap:form.scrollTo(duration=1000),AMP.setState({optionfour:true,optiontwo:false,optionthree:false,optionone:false,className:'d-block',otpName:'d-none',OTP:''})" role=button tabindex=0 class="btn btn-secondary text-uppercase">Book the Test</a></p>
</div>
</div>
</div>
</div>
</section>
</div>
<div class="d-block d-lg-none">
<section class="checkup-section pt-40 pb-40">
<div class=container>
<div class=row>
<div class=col-12>
<h3 class="text-center mt-0 mb-10 section-title fs-24 text-uppercase proxi-bold">Full Body check</h3>
<p class="fs-18 text-center mb-30">51 Parameters</p>
</div>
<div class="col-8 offset-2 col-lg-3 offset-lg-3">
<ul class=mb-0>
<li>Complete Blood Count</li>
<li>Liver Profile</li>
<li>Diabetes Profile</li>
<li>Thyroid Profile</li>
<li>Lipid Profile</li>
</ul>
</div>
<div class="col-8 offset-2 col-lg-3 offset-lg-0">
<ul class="has-and-more mb-0">
<li>Kidney Profile</li>
<li>Iron Deficiency Profile</li>
<li>Homocysteine</li>
<li>and more</li>
</ul>
</div>
<div class="col-12 text-center">
<p class="fs-21 color-blue mt-30 proxi-bold">Tests starting at Rs.799</p>
<p class=mb-0><a on="tap:form.scrollTo(duration=1000),AMP.setState({optionone:true,optiontwo:false,optionthree:false,optionfour:false,className:'d-block',otpName:'d-none',OTP:''})" role=button tabindex=0 class="btn btn-secondary text-uppercase">Book the Test</a></p>
</div>
</div>
</div>
</section>
<section class="checkup-section pt-40 pb-40">
<div class=container>
<div class=row>
<div class=col-12>
<h3 class="text-center mt-0 mb-10 section-title fs-24 text-uppercase proxi-bold">Advanced Full Body Check - Men</h3>
<p class="fs-18 text-center mb-30">75 Parameters</p>
</div>
<div class="col-8 offset-2 col-lg-4 offset-lg-4">
<ul class=mb-0>
<li>Liver Profile</li>
<li>Kidney Profile</li>
<li>Diabetic Profile</li>
<li>Lipid Profile</li>
<li>Thyroid Profile</li>
<li>Urine Analysis</li>
</ul>
</div>
<div class="col-12 text-center">
<p class="fs-21 color-blue mt-30 proxi-bold">Tests starting at Rs.1299</p>
<p class=mb-0><a on="tap:form.scrollTo(duration=1000),AMP.setState({optiontwo:true,optionone:false,optionthree:false,optionfour:false,className:'d-block',otpName:'d-none',OTP:''})" role=button tabindex=0 class="btn btn-secondary text-uppercase">Book the Test</a></p>
</div>
</div>
</div>
</section>
<section class="checkup-section pt-40 pb-40">
<div class=container>
<div class=row>
<div class=col-12>
<h3 class="text-center mt-0 mb-10 section-title fs-24 text-uppercase proxi-bold">Advanced Full Body Check - Women</h3>
<p class="fs-18 text-center mb-30">80 Parameters</p>
</div>
<div class="col-8 offset-2 col-lg-3 offset-lg-3">
<ul class=mb-0>
<li>Complete Blood Count</li>
<li>Inflammation Marker</li>
<li>Liver Profile</li>
<li>Kidney Profile</li>
</ul>
</div>
<div class="col-8 offset-2 col-lg-3 offset-lg-0">
<ul class="has-and-more mb-0">
<li>Diabetic Profile</li>
<li>Lipid Profile</li>
<li>Thyroid Profile</li>
<li>Urine Analysis</li>
</ul>
</div>
<div class="col-12 text-center">
<p class="fs-21 color-blue mt-30 proxi-bold">Tests starting at Rs.1599</p>
<p class=mb-0><a on="tap:form.scrollTo(duration=1000),AMP.setState({optionthree:true,optiontwo:false,optionone:false,optionfour:false,className:'d-block',otpName:'d-none',OTP:''})" role=button tabindex=0 class="btn btn-secondary text-uppercase">Book the Test</a></p>
</div>
</div>
</div>
</section>
<section class="checkup-section pt-40 pb-40">
<div class=container>
<div class=row>
<div class=col-12>
<h3 class="text-center mt-0 mb-10 section-title fs-24 text-uppercase proxi-bold">Alcohol Risk Assessment</h3>
<p class="fs-18 text-center mb-30">51 Parameters</p>
</div>
<div class="col-8 offset-2 col-lg-3 offset-lg-3">
<ul class=mb-0>
<li>Liver Profile</li>
<li>Pancreatic Profile</li>
<li>Lipid Profile</li>
</ul>
</div>
<div class="col-8 offset-2 col-lg-3 offset-lg-0">
<ul class="has-and-more mb-0">
<li>Kidney Profile</li>
<li>Diabetic Profile</li>
</ul>
</div>
<div class="col-12 text-center">
<p class="fs-21 color-blue mt-30 proxi-bold">Tests starting at Rs.1249</p>
<p class=mb-0><a on="tap:form.scrollTo(duration=1000),AMP.setState({optionfour:true,optiontwo:false,optionthree:false,optionone:false,className:'d-block',otpName:'d-none',OTP:''})" role=button tabindex=0 class="btn btn-secondary text-uppercase">Book the Test</a></p>
</div>
</div>
</div>
</section>
</div>
<section class="lp-content pt-40 pb-40">
<div class=container>
<div class=row>
<div class=col-12>
<p class="proxi-bold fs-21 mb-30 text-center">1,00,000 People Have Trusted Us.</p>
</div>
<div class="col-6 col-lg-3 text-center mb-30">
<amp-img src="<?php echo SITE_URL?>cdn/site/mfine-assets/images/new-1.png" alt="" width=90 height=87 layout=fixed></amp-img>
<h5 class=fs-16>FREE SAMPLE PICKUP</h5>
<p class="fs-14 mb-0">Anywhere, anytime in your city</p>
</div>
<div class="col-6 col-lg-3 text-center mb-30">
<amp-img src="<?php echo SITE_URL?>cdn/site/mfine-assets/images/new-2.png" alt="" width=90 height=87 layout=fixed></amp-img>
<h5 class=fs-16>ACCURATE RESULTS</h5>
<p class="fs-14 mb-0">NEBL accredited labs</p>
</div>
<div class="col-6 col-lg-3 text-center mb-30">
<amp-img src="<?php echo SITE_URL?>cdn/site/mfine-assets/images/new-3.png" alt="" width=90 height=87 layout=fixed></amp-img>
<h5 class=fs-16>HASSLE FREE SERVICES</h5>
<p class="fs-14 mb-0">On booking appointments, sample pickup and payments</p>
</div>
<div class="col-6 col-lg-3 text-center mb-30">
<amp-img src="<?php echo SITE_URL?>cdn/site/mfine-assets/images/new-4.png" alt="" width=90 height=87 layout=fixed></amp-img>
<h5 class=fs-16>BEST PRICE</h5>
<p class="fs-14 mb-0">Pay only offer price. No hidden costs</p>
</div>
<div class="col-12 text-center">
<p class=mb-0><a on="tap:form.scrollTo(duration=1000)" role=button tabindex=0 class="btn btn-secondary text-uppercase">Book your test today</a></p>
</div>
</div>
</div>
</section>
<section class="about-content pt-40 pb-40">
<div class=container>
<div class=row>
<div class=col-12>
<h3 class="text-center mt-0 mb-10 section-title fs-24 text-uppercase proxi-bold">About Mfine</h3>
<p class="text-center fs-18 mb-30">On mfine, we provide instant access to high-quality and reliable healthcare. Join 332,000+ patients like you who have consulted with the city's top doctors on mfine.</p>
</div>
</div>
</div>
</section>
<p class=metakey_glp>Health Check up, health packages, total health check up package, online health check up, executive health checkup, full health check,complete health checkup, master health check up, medical health check up, Full Body Check up, Full Body health Check up, full health checkup, online Full body check up, full body test, Body Check Up, Body Check Up, complete body checkup, Whole body checkup</p>
<div class="sticky-button-holder d-block d-lg-none">
<button on="tap:form.scrollTo(duration=1000)" class="btn btn-secondary w-100 text-uppercase">Book my Test</button>
</div>
<div class="dummydiv d-block d-lg-none"></div>

<div id="pi">

<?php

$vr = $this->session->userdata('mfine_id');
$ut = $this->session->userdata('utm_source');
if( isset($vr) &&  $vr > 0 && $ut != ""){
   echo $this->lead_check->set_pixel($vr, MFINE_USER);
   
}

?>
</div>

</body>
</html>
