<?php 
$city_arr_list  		= array(
								"Ahmedabad" 	=> "Ahmedabad",
		                    	"Chennai" 		=> "Chennai",
								"Delhi" 		=> "Delhi",
								"Hyderabad" 	=> "Hyderabad",
		                    	"Kolkata" 		=> "Kolkata",
								"Mumbai" 		=> "Mumbai",
								"Pune" 			=> "Pune",
		                    	"Chandigarh"	=> "Chandigarh",
								"Coimbatore" 	=> "Coimbatore",
								"Guwahati" 		=> "Guwahati",
		                    	"Jaipur" 		=> "Jaipur",
								"Kochi" 		=> "Kochi",
								"Nagpur" 		=> "Nagpur",
		                    	"Vishakapatnam" => "Vishakapatnam",
								"Bhubaneshwar" 	=> "Bhubaneshwar",
								"Cuttack" 		=> "Cuttack",
		                    	"Vijaywada" 	=> "Vijaywada",
								"Mysore" 		=> "Mysore",
								"Gurgaon" 		=> "Gurgaon",
								"Others" 		=> "Others"
								
							);

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Insurance - Compare Insurance Quotes Online - Health | Term | Car Insurance</title>
    <meta name="description" content="Compare insurance quotes online and buy insurance plans like, Health insurance, Car Insurance, Travel and Life Insurance. Instant policy comparison and save upto 50%.">
    <meta name="keywords" content="Compare Insurance, Buy Insurance, Online Insurance, insurance company India, Health Insurance, Term Insurance, Car, Insurance Comparison, Compare, Policies, Compare Insurance Plans, Insurance, Insurance Online, Insurance Plans Online">
    <link rel="shortcut icon" href="<?php echo S3_URL?>/site/health-assets/images/favicon.ico" type="image/x-icon" />

    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/health-assets/css/vendor.css">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/health-assets/css/main.css">
     <!-- fonts -->
      <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Ropa+Sans:400,400i&amp;subset=latin-ext" rel="stylesheet">
    <!-- //fonts -->
  </head>
  <body>

    <div class="banner-main jarallax">
			<div class="container">
				<div class="banner-inner">
					<div class="col-md-5 banner-left">
           
            
            <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="ploicyx_health_jsfrm('<?php echo SITE_URL?>health/healthfrm')">
            <div class="mobile">
            <div class="top-logo text-center">
               <img src="<?php echo S3_URL?>/site/health-assets/images/policyxcom.png" />
            </div>  
					  	<h1 class="mobile-title">Health Insurance</h1>
            </div>  
						<h3>Get Instant Quotes</h3>
              <input type="text"  id="name" name="name" placeholder="Name" onkeyup="chck_valid_health('name', 'Please enter correct name')" data-attr="Please enter correct name">
              <span class="help-block" id="name_err"></span>
			  
              <input type="email" id="email" name="email" placeholder="Email Address" onkeyup="chck_valid_health('email', 'Please enter correct email')" data-attr="Please enter correct email">
              <span class="help-block" id="email_err" ></span>
			  
              <input type="text" class="only_numeric" id="phone" name="phone" pattern="\d*" maxlength="10" placeholder="Mobile Number" onkeyup="chck_valid_health('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
              <span class="help-block" id="phone_err"> </span>
			  
              <input type="text" id="datetimepicker" name="dob" placeholder="DOB"  data-attr="Please enter correct Date">
              <span class="help-block" id="dob_err" ></span>
     
              

              <select class="" id="city" name="city" onchange="chck_valid_health('city', 'Please select correct city')"   data-attr="Please select city">
                  <option value="" >Select city</option>
                  <?php foreach($city_arr_list as $k=>$v){?>
                    <option value="<?php echo $k;?>" ><?php echo $v;?></option>
                  <?php }?>   
                </select>
                <span class="help-block" id="city_err"></span>
				
				
                <input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : "d_sr"); ?>">
                <input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "d_ut"); ?>">
                <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : "d_ut"); ?>">
							<div class="submit">
								<input type="submit" id="frm-sbmtbtn" value="Proceed" name="submit">
							</div>
						</form>
					</div>
					<div class="col-md-7 banner-right">
            <div class="top-logo text-center desktop">
               <img src="<?php echo S3_URL?>/site/health-assets/images/policyxcom.png" />
            </div>  
						<h1 class="desktop">Health Insurance</h1>
						
					<span style="border: dotted 1px #fff; color: #fff; border-radius: 50px; padding: 5px 10px; margin: auto; width: 100%; display: block; text-align: center; background: #7aa9deb5;">Health insurance premium starting from <strong>19Rs/day</strong>*.</span>
						<br>
				
						
						<h4>India's Best Insurance Portal</h4>
							<div class="banner-right-text">
								<div class="main-icon">
									<i class="fa fa-share" aria-hidden="true"></i>
								</div>
								<p>Our customers saved on an average Rs 3000 per policy on online purchase.</p>
							<div class="clearfix"></div>
							</div>
							<div class="banner-right-text">
								<div class="main-icon">
									<i class="fa fa-share" aria-hidden="true"></i>
								</div>
								<p>It takes less than 10 Minutes to get your policy delivered to your Inbox.</p>
							<div class="clearfix"></div>
							</div>
							<div class="banner-right-text">
								<div class="main-icon">
									<i class="fa fa-share" aria-hidden="true"></i>
								</div>
								<p>Medical care through a network of 6000+ hospitals</p>
							<div class="clearfix"></div>
							</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
    <!-- about -->
    <section class="about" id="about">
      <div class="container">
      <div class="about-heading">
        <h2>Welcome to PolicyX.com</h2>
      </div>
        <div class="about-grids">
        <div class="col-md-6 about-left">
          <img src="<?php echo S3_URL?>/site/health-assets/images/about.jpg" alt="" />
        </div>
        <div class="col-md-6 about-right">
          <p><strong>PolicyX.com</strong> is one of the fastest growing IRDA approved insurance 
            comparison portals in India. We help customers in comparing different 
            insurance policy quotes and products like life, car, health, travel, 
            investment, pension and business among others. We have tie-ups with some 
            of the leading online insurance companies in India and provide a step by 
            step guidance to our customers in purchasing insurance plans by means of 
            infographics, videos, charts and instant quotes, based on the latest research 
            and data that helps them in making an informed choice. At PolicyX.com we offer 
            the best insurance services for free of cost with the help of unique features 
            like plan specific cash flow projection charts, videos etc to help you understand 
            policy features and costs better. It is a one stop portal for all your insurance needs.</p>
        </div>
        <div class="clearfix"></div>
        </div>
        </div>
    </section>
    <!-- //about -->
        <!-- about -->
        <section class="Compare" id="Compare">
      <div class="container">
      <div class="about-heading">
        <h2>Why To Compare Insurance?</h2>
      </div>
        <div class="about-grids">
        <div class="col-md-12 about-right">
          <p>It is important to compare & check insurance quotes before purchasing any policy as it can assist in saving a considerable amount of money along with a better coverage. For many people it may be a formidable task. But at PolicyX.com consumer can do the same with ease. By comparing insurance quotes at PolicyX.com a person can save up to 60 percent on premium of many different policies and can get many add on benefits as well. The chief objective of our organization is to assist consumers in making the right decision before any purchase and it can be done by comparing insurance quotes online at this portal. There are top insurers, which are offering the similar types of products but getting the best one out of them can only be done by verifying their policies and various features whereby you increase your chance of selecting the best insurance policies as per your requirement by checking the features, benefits, premium, and many additional features of different plans on a single page.</p>
        </div>
        <div class="clearfix"></div>
        </div>
        </div>
    </section>
    <!-- //about -->
            <!-- about -->
            <section class="Compare" id="help">
      <div class="container">
      <div class="about-heading">
        <h2>How We Help You To Compare Insurance Policies ?</h2>
      </div>
        <div class="about-grids">
        <div class="col-md-12 about-right">
          <p>
          At PolicyX.com, you can choose from various General Insurance policies like health, car & two wheeler, travel and life insurance policies like term, child, pension and retirement plans. Our vision is to help each and every insurance buyer in choosing the most suitable policy that perfectly fits their requirements and needs. PolicyX.com has also been nominated for the Best Website Of The Year 2014 & 2015 and nominated for Red Herring Top 100, 2015 Awards. We use the best technology for making the entire comparing and buying process easier for you.
          </p>
          <p>
          We strongly recommend you to compare insurance plans before buying. Every time you visit our web site and take advantage of the free quotes based on various parameters, you get a better understanding of available products thereby helping you to choose the best plan for yourself. There are many sites which are providing the same services, but the benefit of choosing us are many such as lower premium costs, absence of mediators, better customer service and much more. We assist consumers in comparing and buying insurance policies online, renewing policies and much more. We have partners from diverse insurance segments, including health insurance, travel insurance, term insurance, car insurance etc. These partnerships help us in improving services and presenting the prices and plan's details straight from the insurance company.
                  </p>  
          <p>
          Rest assured that we bring you an unbiased and transparent insurance comparison service for different types of policies at your doorstep. We aim to provide the best online platform that maximizes your comfort and savings while zeroing in on a policy. For any kind of assistance, feel free to contact us.
                  </p>        
        </div>
        <div class="clearfix"></div>
        </div>
        </div>
    </section>
    <!-- //about -->
    <!--Partners-->
    <section class="partner-section" id="partners">
      <div class="container">
        <div class="services-heading">
          <h3>Our Partners</h3>
        </div>     
      </div>
    <div class="container testimonial-container">
      <div class="partners-list">
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/icici.jpg" alt=""> 
          </div>  
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/edelweiss-tokio.jpg" alt=""> 
          </div>  
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/apollo-munich.png" alt=""> 
          </div>  
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/Bharti.jpg" alt=""> 
          </div>  
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/national-insurance.png" alt=""> 
          </div> 
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/tata.jpg" alt=""> 
          </div> 
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/aditya.jpg" alt=""> 
          </div> 
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/cigna.jpg" alt=""> 
          </div>  
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/Star-health.jpg" alt=""> 
          </div>  
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/fg.jpg" alt=""> 
          </div>  
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/HDFC-ERGO.jpg" alt=""> 
          </div>  
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/hdfc-life-logo.jpg" alt=""> 
          </div> 
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/us.png" alt=""> 
          </div> 
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/canara-hsbc.gif" alt=""> 
          </div> 
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/Max.jpg" alt=""> 
          </div> 
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/max-li.jpg" alt=""> 
          </div> 
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/bajaj-allianz.png" alt=""> 
          </div> 
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/Religare_Enterprises_Limited.jpg" alt=""> 
          </div> 
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/pnb-metlife.jpg" alt=""> 
          </div> 
          <div class="partners-list-item">
            <img src="<?php echo S3_URL?>/site/health-assets/images/AEGON.png" alt=""> 
          </div> 
         
      </div>  
    </div>   
    </section>
    <!--Partners-->


    <!--Testimonial -->
    <section class="testimonial-section" id="testimonials">
      <div class="container">
        <div class="services-heading">
          <h3>Customer Review</h3>
        </div>     
      </div>
    <div class="container testimonial-container"> 
    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <div id="testimonial-slider" class="owl-carousel">

                <div class="testimonial">
                    <p class="description">
                        "I bought 3 policies from PolicyX.com. The customer executive gave me a better solution in the policy port time and I got policy at regional price along with lots of benefits associated with it which was lacking in my earlier policy. The communication of customer care team was very nice and they guided me well."
                    </p>
                    <div class="pic">
                        <img src="<?php echo S3_URL?>/site/health-assets/images/review1.jpg" alt="">
                    </div>
                    <h3 class="testimonial-title">
                    Rajiv Bhaskar
                        <small>Delhi</small>
                    </h3>
                </div>

                   <div class="testimonial">
                    <p class="description">
                        "I wanted a Health Plan and while searching on Google came across PolicyX.com. I got the call instantly from their agent who guided me well and contacted on time. I would recommend PolicyX.com to anyone interested in purchasing a policy."
                    </p>
                    <div class="pic">
                        <img src="<?php echo S3_URL?>/site/health-assets/images/review2.jpg" alt="">
                    </div>
                    <h3 class="testimonial-title">
                    Khodubha Sarvaiya
                        <small>Rajkot, Gujarat</small>
                    </h3>
                </div>     

             <div class="testimonial">
                    <p class="description">
                        " Good platform to buy health insurance and customer care executive was very good in guiding me throughout the process."
                    </p>
                    <div class="pic">
                        <img src="<?php echo S3_URL?>/site/health-assets/images/review3.jpg" alt="">
                    </div>
                    <h3 class="testimonial-title">
                    Sanat Boro
                        <small>Guwahati</small>
                    </h3>
                </div>  

                <div class="testimonial">
                    <p class="description">
                        "Good job! Keep on doing the good work. The process of buying the policy is very smooth. Recommend to anyone who wants to buy an insurance policy."
                    </p>
                    <div class="pic">
                        <img src="<?php echo S3_URL?>/site/health-assets/images/review4.jpg" alt="">
                    </div>
                    <h3 class="testimonial-title">
                    Trilok Singh Sengar
                        <small>Gwalior</small>
                    </h3>
                </div>  

                 <div class="testimonial">
                    <p class="description">
                        "Came across this site online and contacted their toll free no. All my queries were resolve in one call by their agent."
                    </p>
                    <div class="pic">
                        <img src="<?php echo S3_URL?>/site/health-assets/images/review5.jpg" alt="">
                    </div>
                    <h3 class="testimonial-title">
                    Ranjan De
                        <small>Delhi</small>
                    </h3>
                </div>  


                 <div class="testimonial">
                    <p class="description">
                        "I selected PolicyX.com because they gave the best policy at lowest premium rates. A good site to compare and buy insurance."
                    </p>
                    <div class="pic">
                        <img src="<?php echo S3_URL?>/site/health-assets/images/review6.jpg" alt="">
                    </div>
                    <h3 class="testimonial-title">
                    Ma Murtoza
                        <small>Gurgaon</small>
                    </h3>
                </div>  

                <div class="testimonial">
                    <p class="description">
                        " I thought buying an online policy will create a lot of mess. I would miss many important details. But thanks to PolicyX, buying a policy online is so easy now."
                    </p>
                    <div class="pic">
                        <img src="<?php echo S3_URL?>/site/health-assets/images/user.png" alt="">
                    </div>
                    <h3 class="testimonial-title">
                    Bhawna Sinha
                        <small>Gurgaon</small>
                    </h3>
                </div>  

                <div class="testimonial">
                    <p class="description">
                        "I got the exact health plan I was looking for, that too at a well compared and reasonable price. I like PolicyX.com. Keep up the good work."
                    </p>
                    <div class="pic">
                        <img src="<?php echo S3_URL?>/site/health-assets/images/user.png" alt="">
                    </div>
                    <h3 class="testimonial-title">
                    Sameer Jain
                        <small>Mumbai</small>
                    </h3>
                </div>  

            </div>
        </div>
    </div>
</div>
                  </section>
    <!--//Testimonial -->


    <!-- services -->
    <section class="services" id="services">
      <div class="container">
        <div class="services-heading">
          <h3>Why Compare Insurance Quotes with PolicyX.Com</h3>
        </div>     
      </div>
    <!-- //services -->
    <div class="container serviceBox-container">
      <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="serviceBox">
                  <div class="service-icon">
                      <i class="fa fa-pencil-square-o"></i>
                  </div>
                  <h3>Transparent and Unbiased</h3>
                  <p>
                  We are not a seller; we are an advisor who guides you towards choosing the best plans through our unbiased and transparent reviews and suggestions.
                  </p>
              </div>
          </div>

          <div class="col-md-3 col-sm-6">
            <div class="serviceBox">
                  <div class="service-icon">
                      <i class="fa fa fa-video-camera"></i>
                  </div>
                  <h3>Product Specific Videos</h3>
                  <p>
                  Along with providing all plan specific information, we help you understand the plan better by providing plan specific short videos.
                  </p>
              </div>
          </div>

          <div class="col-md-3 col-sm-6">
            <div class="serviceBox">
                  <div class="service-icon">
                      <i class="fa fa-university"></i>
                  </div>
                  <h3>IRDA Approved Portal</h3>
                  <p>
                  PolicyX.com is approved by the IRDA (Insurance Regulatory and Development Authority) to provide insurance comparison and offer plans from various companies.
                  </p>
              </div>
          </div>

          <div class="col-md-3 col-sm-6">
            <div class="serviceBox">
                  <div class="service-icon">
                      <i class="fa fa-bar-chart "></i>
                  </div>
                  <h3>Dynamic Cash Flow Charts</h3>
                  <p>
                  Since insurance is a complex product, we want you to be sure about the policy you purchase. We have designed complex algorithms that show you dynamic yearly cash flows under various scenarios.
                  </p>
              </div>
          </div>
      </div>
  </div>    
</section>

<footer>
    <div class="footer-bottom">
        <div class="container">
            <a class="pull-left"> <img src="<?php echo S3_URL?>/site/health-assets/images/policyxcom.png" /></a>
            <span><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:1800-4200-269">1800-4200-269</a></span>
            <span class="">|</span>
            <span><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:helpdesk@policyx.com">helpdesk@policyx.com</a></span>            
            <span class="">|</span>
            <p>India's Best Insurance Portal</p>
            
        </div>
    </div>
	
	 <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
</footer>
<div id="copyright_wrapper">
	<div class="container">
		<div class="row">
            <div class="col-sm-12">
            	<p class="small-font text-left "> Copyright <a href="https://www.policyx.com">PolicyX.com /</a><strong class="font-skyblue"> Certified :</strong> IRDA Approved Number - <a href="https://www.policyx.com/license.php"> IRDA/WBA17/14 </a> Insurance is the subject matter of solicitation</p> 
            </div>
			<div class="col-sm-12">
				<p class="small-font text-left">Disclaimer: The information displayed on this website is of the insurers with whom our company has an agreement. The prospect's particulars could be shared with insurers.</p>   
 			</div> 
 		</div>
	</div>
</div>
    <script src="<?php echo S3_URL?>/site/health-assets/js/vendor.js"></script>
    <script src="<?php echo S3_URL?>/site/health-assets/js/main.js"></script>
	<script src="<?php echo S3_URL?>/site/scripts/default.js"></script> 
  </body>
</html>