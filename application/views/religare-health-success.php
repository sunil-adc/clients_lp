
<!DOCTYPE html>
<html lang="en"><head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Religare Health Insurance</title>
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/religare-health/religare-images/favicon.ico" type="image/x-icon" />
<script src="<?php echo S3_URL?>/site/scripts/jquery-1.11.3.min.js"></script>
      <!-- Bootstrap -->
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/religare-health/religare-css/bootstrap.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/religare-health/religare-css/new-age.min.css"> 
      <link href="https://fonts.googleapis.com/css?family=Sunflower:300,500,700" rel="stylesheet">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      
      <!-- DO NOT MODIFY -->
      <!-- Quora Pixel Code (JS Helper) -->
      <script>
      !function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
      qp('init', '7cc31ea88ae24531ba00b6301258883a');
      qp('track', 'ViewContent');
      </script>
      <noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/7cc31ea88ae24531ba00b6301258883a/pixel?tag=ViewContent&noscript=1"/></noscript>
      <!-- End of Quora Pixel Code --> 
       
      <script>qp('track', 'GenerateLead');</script>
 
   
       
   <!-- DO NOT MODIFY -->
   <!-- Quora Pixel Code (JS Helper) -->
   <script>
   !function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
   qp('init', '7cc31ea88ae24531ba00b6301258883a');
   qp('track', 'ViewContent');
   </script>
   <noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/7cc31ea88ae24531ba00b6301258883a/pixel?tag=ViewContent&noscript=1"/></noscript>
   <!-- End of Quora Pixel Code -->

  <script>qp('track', 'GenerateLead');</script>

  <!-- Global site tag (gtag.js) - Google Ads: 798077325 -->
   <script async src="https://www.googletagmanager.com/gtag/js?id=AW-798077325"></script>
   <script>
     window.dataLayer = window.dataLayer || [];
     function gtag(){dataLayer.push(arguments);}
     gtag('js', new Date());

     gtag('config', 'AW-798077325');
   </script>

   <!-- Event snippet for Religare_lead conversion page -->
   <script>
     gtag('event', 'conversion', {'send_to': 'AW-798077325/L0onCKDLiLYBEI3jxvwC'});
   </script>   

   </head>
   <body id="page-top">
      <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
         <div class="container text-center">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">                
               <a class="navbar-brand page-scroll" href="#page-top">
               <img src="<?php echo S3_URL?>/site/religare-health/religare-images/logo.png" class="logo"/>
               </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1 ">
               <h4 style="color: #fff;text-shadow: 1px 1px 5px #2562bb;font-size: 22px;
                  margin: 40px 0px;"></h4>
            </div>
            <!-- /.navbar-collapse -->
         </div>
         <!-- /.container-fluid -->
      </nav>
	  
	  
	  	<header>
      	<?php $all_array = all_arrays(); ?>
         <div class="container form_margin">
            <div class="row">
				<div class="col-md-6 form_box">
				<br/><br/> 
					<h2>
					It'll Only Take A Few Seconds To Take A Responsible Decision For You And Your Family
					</h2>
                </div>
                <div class="col-md-2"> 
				</div>
               <div class="col-md-4"> 
                  <div class="jumbotron form_box">
                     <div class="row text-center" id="f_div" >
                        <div class="text-center  col-md-12 col-lg-12">
<h4 class="form-title">Thank you for sharing your contact information with us.</h4><br/>

<h4 class="form-title">Our representative will get in touch with you shortly.</h4><br/>
                        </div>
                         
                     </div>
                    
                  </div>
               </div>
          
            </div>
         </div>
      </header>
      
      
      
      
      <section id="overview" class="overview">   
         <div class="container">
         <div class="row">
         	<h1 class="text-center spacing">Overview</h1>
         	<div class="col-md-6 spacing">
         		<h3>Our Philosophy</h3>
         		<p>Life is about experiencing every good bit of it throughout one's lifetime; be it a walk in the park with your parents or building memories of playing with your little one. At Religare Health Insurance, we understand that these experiences can be truly rejoiced when one lives a healthy life without having to worry about any unforeseen medical issues.</p>
         	</div>
         	<div class="col-md-6 spacing">
         		<h3>Our Solutions</h3>
         		<p>In line with our objective of ensuring good health…hamesha! Our Comprehensive Health Insurance Plan – 'Care'; helps safeguard you & your family against financial risks arising out of a medical emergency. With 'Care' by your side you can be assured that while you're unwell; we'll take up all the hassles related to your treatment so that you can stay worry free and focus only on your recovery.</p>
         	</div>
         </div> 
		</div>
      </section>
       
       
     <section id="coverage" class="coverage">    
	 <div class="container service"> 
	 <h1 class="text-center">Care Coverage</h1>
		<p class="text-center">A Comprehensive  <strong>Health Insurance Plan</strong> to meet everyone's ( individual and family) healthcare needs.</p>
		<div class="row text-center spacing-1">
		
			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					<img class="img-circle" src="<?php echo S3_URL?>/site/religare-health/religare-images/in-Patient.png" data-holder-rendered="true">

					</div>
					<h4 class="title">In-Patient Care</h4>

				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					<img class="img-circle"   src="<?php echo S3_URL?>/site/religare-health/religare-images/pre-post.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Pre & Post Hospitalization</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/individual-cover.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Individual & Floater Cover</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/hospitalization.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Domiciliary Hospitalization</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/allowance.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Daily Allowance</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/ambulance.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Ambulance Cover</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/organ.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Organ Donar Cover</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/lifelong.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Lifelong Renewability</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/health-checkup.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Annual Health-Checkup</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/bonus.png" data-holder-rendered="true">

					</div>
					<h4 class="title">No Claim Bonus</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/opinion.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Second Opinion</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/tax.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Tax Benifit</h4>             
				</div>
			</div>
		</div>
	</div>
	</section>
         
         <div class="container">
        		 <div class="row spacing-1">         
         	<div class="col-md-6 bg-dark">
         		<ul>
         		  <li>A Comprehensive  Health Insurance Plan to meet everyone's (individual and family) healthcare needs.</li>
                  <li>From Diagnosis to Recovery - "Care" at every step!</li>
                  <li>30 Day Pre-Hospitalization Coverage</li>
                  <li>Ambulance Cover</li>
                  <li>In-Patient Hospitalization - Hospitalization for at least 24 hours</li>
                  <li>ICU Charges</li>
         		</ul>
         	</div>
         	<div class="col-md-6">
         		<ul>
         		  <li>60-Day Post – Hospitalization Coverage</li>
                  <li>Annual Health Check-up</li>
                  <li>Automatic Recharge of Sum Insured</li>
                  <li>No-Claim Bonus</li>
                  <li>No Upper Age Limit of Enrollment</li>
                  <li>Lifelong Renewability</li> 
         		</ul>
         	</div>
         	</div>
		</div>
        
         <section id="covers" class="covers">          
         <div class="container">
         <div class="row">
         	<h1 class="text-center spacing-1">Add On Covers</h1>
         	<div class="col-md-6">
         		<h3>No Claim Bonus Super</h3>
         		<p>By choosing an optional cover – NCB Super – you can further increase your sum insured amount by 50% for every claim free year up to a maximum of 100%. Hence – in total with both NCB & NCB Super one can increase their sum insured up to 150% in 5 years. </p>
         	</div>
         	<div class="col-md-6">
         		<h3>Everyday Care</h3>
         		<p>Your family may avoid hospitalization for many years at a stretch. However, you have to be really fortunate to avoid doctor consultation for even a few months. For all the visits for all routine visits to the hospital - we give you 1% of your Sum Insured for doctor consultation & diagnostic tests each. This can be availed via a cashless health card and additional premium paid to avail this add- on cover is also applicable for tax benefit under section 80D of IT act. </p>
         	</div>
         		<div class="col-md-6">
         		<h3>Unlimited Automatic Recharge</h3>
         		<p>There should never be a time when you run out of coverage, which is why by selecting this add – on cover your sum insured amount if re-instated in your policy every time your sum insured exhausts. And this can be availed unlimited number of times.</p>
         	</div>
         	<div class="col-md-6">
         		<h3>Air Ambulance Cover</h3>
         		<p>The right cure may be miles away but never out of your reach with Air Ambulance Cover. For all those times when you might be recommended a treatment which is not in your city of stay. </p>
         	</div>
         		<div class="col-md-6">
         		<h3>Personal Accident Cover</h3>
         		<p>Forget the hassles of maintaining two separate policies, one for a health insurance cover & second for personal accident cover. By selecting this add- on cover you get coverage for Accidental Death and Permanent Total Disability for up to 10 times of the Sum Insured opted. </p>
         	</div>
         	<div class="col-md-6">
         		<h3>International Second Opinion</h3>
         		<p>We believe that sometimes reassurance works better than the cure itself. Hence before going ahead with the treatment recommended, we give you an option to consult doctors and take a second opinion.</p>
         	</div>
         </div>

			</div>
			</section>
    
	 
    <div id="pi">
       <?php
     $vr = $this->session->userdata('religare_id');
    
     $ut = $this->session->userdata('utm_source');
     if( isset($vr) &&  $vr > 0 && $ut != ""){
       echo $this->lead_check->set_pixel($vr,RELIGARE_USER);
       
     }
     ?>
      </div> 
    
     
      <footer>
      	© 2019 - Religare Health Insurance. All Rights Reserved
      </footer>
      <!-- jQuery -->
      <
      <!-- Bootstrap Core JavaScript -->
      <script src="<?php echo S3_URL?>/site/scripts/bootstrap.js"></script>
      <!-- Plugin JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
      <!-- Theme JavaScript -->
      <script src="<?php echo S3_URL?>/site/scripts/flatpickr.min.js"></script>
      <script src="<?php echo S3_URL?>/site/scripts/default.js"></script> 
      
       
   </body>
</html>