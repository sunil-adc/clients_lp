<!DOCTYPE html>
<html>

<head>
    <title>Adcanopus</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="refresh" content="3;url=<?php echo SITE_URL ?>nutratimes/result" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/nutratimes-quiz-assets/lib/bootstrap-4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/nutratimes-quiz-assets/lib/rangeslider.js-2.3.0/rangeslider.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
    <!--Custom Css-->
    <link rel="stylesheet" type="text/css" href="<?php echo S3_URL?>/site/nutratimes-quiz-assets/css/style.css">

</head>

<body>
    <div class="indicator">
        <svg width="16px" height="12px">
            <polyline id="back" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>
            <polyline id="front" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>
        </svg>
    </div>
    <div class="loader">Calculating</div>


    <!-- jQuery 3 -->
    <script src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/lib/jquery/jquery-3.3.1.min.js"></script>
    <script src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/lib/jquery-validation-1.17.0/jquery.validate.min.js"></script>
    <!-- Range Slider -->
    <script src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/lib/rangeslider.js-2.3.0/rangeslider.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/lib/bootstrap-4.0.0/js/bootstrap.min.js"></script>

    <!-- Customjs -->
    <script src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/js/vendor.js"></script>
    <script src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/js/custom.js"></script>
</body>

</html>