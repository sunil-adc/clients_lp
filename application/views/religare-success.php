<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>OPEN A FREE* ONLINE TRADING ACCOUNT</title>
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/religare-images/favicon.ico" type="image/x-icon" />

      <!-- Bootstrap -->
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/religare-css/bootstrap.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/religare-css/new-age.min.css">
      <link href="https://fonts.googleapis.com/css?family=Sunflower:300,500,700" rel="stylesheet">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
	
   
   </head>
   <body id="page-top">
      <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
         <div class="container text-center">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">                
               <a class="navbar-brand page-scroll" href="#page-top">
               <img src="<?php echo S3_URL?>/site/religare-images/logo.png" class="logo"/>
               </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1 ">
               <h4 style="color: #fff;text-shadow: 1px 1px 5px #2562bb;font-size: 22px;
                  margin: 40px 0px;"></h4>
            </div>
            <!-- /.navbar-collapse -->
         </div>
         <!-- /.container-fluid -->
      </nav>
	  
	  
	  <header>
      <?php $all_array = all_arrays(); ?>
         <div class="container form_margin">
            <div class="row">
				<div class="col-md-8 form_box">
					<h1>OPEN A <span style="color: orange">FREE*</span> ONLINE TRADING ACCOUNT</h1>
					<h5>Enjoy Rs. 500 brokerage credit* offer | Zero* A/C Opening Charges | Zero* AMC</h5>
            <br> 
            <div class="benefits-block desktop-view">
                <h3>WHY TRADE WITH RELIGARE ONLINE</h3>
              
                <ul class="benefits">
                  <li>
                    <div class="logo"><img src="<?php echo S3_URL?>/site/religare-images/1.png"></div>
                    <div class="name1">SIMPLE TRADING PLATFORM</div>
                  </li> 
                    <li>
                    <div class="logo"><img src="<?php echo S3_URL?>/site/religare-images/2.png"></div>
                    <div class="name1">FLEXI BROKERAGE PLANS</div>
                  </li>
                  <li>
                    <div class="logo"><img src="<?php echo S3_URL?>/site/religare-images/3.png"></div>
                    <div class="name1">TRADE ANYTIME ANYWHERE</div>
                  </li>
                    <li>
                    <div class="logo"><img src="<?php echo S3_URL?>/site/religare-images/4.png"></div>
                    <div class="name1">REAL TIME INFORMATION ACCESS</div>
                  </li>
                  <li>
                    <div class="logo"><img src="<?php echo S3_URL?>/site/religare-images/5.png"></div>
                    <div class="name1">QUALITY EXPERT RESEARCH REPORTS</div>
                  </li>
                    <li>
                    <div class="logo"><img src="<?php echo S3_URL?>/site/religare-images/6.png"></div>
                    <div class="name1">INTEGRATED TRADING</div>
                  </li>
                </ul>
               </div>
                </div>
               <div class="col-md-4 "> 
                  <div class="jumbotron form_box">
                     
					 <div class="row text-center" id="success_verify">  
							
                           <h3 style="color:#3c4850; margin-top:50px;font-size: 20px;line-height: 30px;">Thank you for your interest in  Religare Broking Limited. </h3>
                     </div>
					 
                  </div>
               </div>
               <div class="col-md-12 benefits-block mobile-view">
                 <div class="form_box">
                <h3>WHY TRADE WITH RELIGARE ONLINE</h3>
              
                <ul class="benefits">
                  <li>
                    <div class="logo"><img src="<?php echo S3_URL?>/site/religare-images/1.png"></div>
                    <div class="name1">SIMPLE TRADING <br>PLATFORM</div>
                  </li> 
                    <li>
                    <div class="logo"><img src="<?php echo S3_URL?>/site/religare-images/2.png"></div>
                    <div class="name1">FLEXI BROKERAGE<br> PLANS</div>
                  </li>
                  <li>
                    <div class="logo"><img src="<?php echo S3_URL?>/site/religare-images/3.png"></div>
                    <div class="name1">TRADE ANYTIME ANYWHERE</div>
                  </li>
                    <li>
                    <div class="logo"><img src="<?php echo S3_URL?>/site/religare-images/4.png"></div>
                    <div class="name1">REAL TIME INFORMATION ACCESS</div>
                  </li>
                  <li>
                    <div class="logo"><img src="<?php echo S3_URL?>/site/religare-images/5.png"></div>
                    <div class="name1">QUALITY EXPERT RESEARCH REPORTS</div>
                  </li>
                    <li>
                    <div class="logo"><img src="<?php echo S3_URL?>/site/religare-images/6.png"></div>
                    <div class="name1">INTEGRATED <br>TRADING</div>
                  </li>
                </ul>
                 </div>
               </div>
            </div>
         </div>
      </header>
	  <?php 
				$name  =$this->session->userdata('name');
				$email =$this->session->userdata('email');
				$phone =$this->session->userdata('phone');
				$city  =$this->session->userdata('city');
		
		echo '<img src="https://insta.religareonline.com/campaigns/weblead.aspx?phonenumber='.$phone.'&Name='.$name.'&email='.$email.'&city='.$city.'&utm_source=ADCANOPUS">';
	  
	 
	  //echo '<img src="http://panelss.in/Dropbox/trunk/clients_lp/religare/add_data?phonenumber='.$phone.'&Name='.$name.'&email='.$email.'&city='.$city.'&utm_source=ADCANOPUS" width="1px"    height="1px">';
	  ?>
      <section id="features" class="features" >
         <div class="container">
<br>
<br>
		<br> 
<br>
		
	Copyright 2010 Religare. Trademarks are the property of their respective owners. All rights reserved.<br>
Please note that by submitting the above mentioned details, you are authorizing us to Call/SMS you even though you may be registered under DNC. We shall Call/SMS you for a period of 12 months.<br>

Investment in securities market are subject to market risks, read all the related documents carefully before investing.<br>
Brokerage will not exceed the SEBI prescribed limit. 



<br>
<br>
			    Religare Broking Limited, Registered Office:2nd Floor, Rajlok Building, 24, Nehru Place, New Delhi-110019. Board line number 011-46272400, Fax No. 011-46272447
                <br>Religare is a registered trademark of RHC Holding Private Limited used under license by Religare Enterprises Limited and its subsidiaries.<br>
Registration No. NSE: SEBI Regn. No: INB 230653732, INF 230653732 &amp; INE 230653732 TM Co: 06537 Clearing Member (F&amp;O) No. M50235 BSE:SEBI Regn. No: INB 010653732 &amp; INF 010653732 | Clearing No: 3004 | MSEI SEBI Regn. No: INB260653739, INF260653739 &amp; INE260653732 TM Co: 1051 Clearing Member (F&amp;O) No. 51 | NSDL: DP ID: IN 301774 | SEBI Regn. No: IN-DP-NSDL-150-2000 | CDSL DP ID: 30200 SEBI Regn. No: IN-DP-CDSL-202-2003 || Religare Commodities Limited: NCDEX: Member ID- 00109 | MCX: Member ID- 10575 | SEBI Registration Number- INZ000022334| NSEL Member ID- 10180 | NCDEX Emarket Member ID- 10042 | Research Analyst SEBI Regi.No.: INH100001831 <a target="_blank" href="http://www.religareonline.com/disclaimer">Disclaimer</a> <span> <a href="http://www.religareonline.com/MediaGalary/religare_images/201701051513011693899-Insta Flexi Margin Plan- Equity &amp; Currency- V.1.docx.pdf" target="_blank">*T&amp;C </a>|  
<a href="http://www.religareonline.com/mediagalary/religare_images/201804100958089406853-terms_n_conditions.pdf" target="_blank">#T&amp;C </a> 
</span></div>
      </section>
       
       <div id="pi">
       <?php
	   $vr = $this->session->userdata('religare_id');
	   $ut = $this->session->userdata('utm_source');
	   if( isset($vr) &&  $vr > 0 && $ut != ""){
		   echo $this->lead_check->set_pixel($vr, RELIGARE_USER);
		   
	   }
	   ?>
      </div> 
	   
      <footer>
      	© 2018 -  Religare Broking Limited. All Rights Reserved
      </footer>
      <!-- jQuery -->
      <script src="<?php echo S3_URL?>/site/scripts/jquery-1.11.3.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="<?php echo S3_URL?>/site/scripts/bootstrap.js"></script>
      <!-- Plugin JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
      <!-- Theme JavaScript -->
      <script src="<?php echo S3_URL?>/site/religare-css/new-age.min.css"></script>
      <script src="<?php echo S3_URL?>/site/scripts/flatpickr.min.js"></script>
      <script src="<?php echo S3_URL?>/site/scripts/default.js"></script>
      <link href="<?php echo S3_URL?>/site/religare-css/flatpickr.min.css" rel="stylesheet">
      
      <!--  <script>
		$(document).ready(function(e) {    
		$(document).on("click", ".book_app", function(e) {
			var option=($(this).attr("data-attr"));
			// $("#religare_test option[value='"+option+"']").select();
			 $('#religare_test option:eq('+option+')').prop('selected', true)
		});
		});
		
		

      </script>-->
	  
		<!-- Google Code for Remarketing Tag -->
		<!--------------------------------------------------
		Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
		--------------------------------------------------->
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 798087424;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/798087424/?guid=ON&amp;script=0"/>
		</div>
		</noscript>
		
	  
	    <!-- Google Code for Religare Conversion Conversion Page -->
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 798087424;
		var google_conversion_label = "j4QMCLOqpIUBEICyx_wC";
		var google_remarketing_only = false;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/798087424/?label=j4QMCLOqpIUBEICyx_wC&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
	
	  
   </body>
</html>