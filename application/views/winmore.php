
 <?php $all_array = all_arrays(); ?>
<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Winmore School</title>
      <meta name="description" content="Winmore School">
      <meta name="keywords" content="Winmore School">
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/winmore-assets/images/favicon.png" type="image/x-icon" />
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

      <link rel="stylesheet" href="<?php echo S3_URL?>/site/winmore-assets/css/vendor.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/winmore-assets/css/main.css">    
      <script src="<?php echo S3_URL?>/site/winmore-assets/js/jquery-2.2.3.min.js"></script>
      
      <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <script src="<?php echo S3_URL?>/site/winmore-assets/js/bootstrap.js"></script>
      <link href="//fonts.googleapis.com/css?family=Righteous" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Mukta+Mahee:200,300,400,500,600,700,800" rel="stylesheet">
   </head>
   <body> 
    <?php $all_array = all_arrays(); ?>
    <!--/banner-bottom-->
    <div class="w3_agilits_banner_bootm">
        <div class="w3_agilits_inner_bottom">
            <div class="wthree_agile_login">
                <ul>
                    <li> 
                    Empowering generations to win the right way
                    </li>
                   
                </ul>
            </div>

        </div>
    </div>
    <!--//banner-bottom-->
    <!--/banner-section--> 
    <div id="demo-1" data-zs-src='["<?php echo S3_URL?>/site/winmore-assets/images/b1.jpg","<?php echo S3_URL?>/site/winmore-assets/images/b2.jpg","<?php echo S3_URL?>/site/winmore-assets/images/b3.jpg"]' data-zs-overlay="dots" data-zs-speed="10000">
        <div class="demo-inner-content">
            <!--/banner-info-->
            <div class="baner-info">
                <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" 
                onsubmit="winmore_jsfrm('<?php echo SITE_URL?>winmore/submit_frm','1')">
                    <div class="form-logo">
                        <a href="#">
                            <img src="<?php echo S3_URL?>/site/winmore-assets/images/logo.jpg" /> 
                        </a> 
                    </div>  
                    <div class="group">
                        <div class="form-group"> 
                            <input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err1"></span>
                        </div>
              
                    </div>  
                    <div class="group">

                    <div class="form-group">                   
                    <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                    <span class="help-block" id="email_err1" ></span>
                    </div>

					 <div class="form-group">
                              <select  name="campus" id="campus1" class="form-control campus" onkeyup="chck_valid('campus', 'Please Select Preferred campus')" data-attr="Please Select Preferred campus" onchange="gradechange('1')">
                                 <option value="" selected disabled>Preferred Campus</option>
								 <option value="whitefield">Whitefield</option>
                                  <option value="jakkur ">Jakkur</option>
                              </select>
							   <span class="help-block" id="campus_err1"> </span> 
                           </div>
                     <div class="form-group">
                              <select  name="grade" id="grade1" class="form-control grade" onkeyup="chck_valid('grade', 'Please Select Preferred Grade')" data-attr="Please Select Preferred Grade">
                                 <option value="" selected disabled>Preferred Grade</option>
								 <?php foreach($all_array['WINMORE_GRADE'] as $k=>$v){ ?>
                                 <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                 <?php }?>
                                 
                              </select>
							   <span class="help-block" id="grade_err1"> </span> 
                           </div>

                    <div class="form-group"> 
					 <input type="hidden" class="hiddenCountry" name="CountryCode" id="CountryCode1" value="91">
                    <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*"  
                    placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" 
                    data-attr="Please enter correct Mobile number">
                    <span class="help-block" id="phone_err1"> </span>
                    </div> 
                    </div>      
                
                   
                    <div class="submitbtncontainer">
                    <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit" class="frm-sbm">
                    </div>

                </form>
            </div>
            <!--/banner-info-->
        </div>
    </div>
     
    
     
    <!-- //stats --> 
    <!--about -->
    <div id="about" class="wthree-about section-w3ls">
        <div class="container">
      
            <h3 class="w3ls-title">
                <span>o</span>VERVIEW</h3>
            <h5>Welcome to Winmore Academy!</h5>
            <div class="col-md-6  embed-container">
            <iframe class="fancybox-iframe" autoplay src="https://www.youtube.com/embed/49f_PwIbsBo" frameborder="0" allowfullscreen></iframe>
            
            </div>
            <div class="col-md-6  agileits-w3layouts">
            <h5>About Us</h5>
                <p> Winmore Academy, the co-educational CBSE School is the new face of innovative, progressive 
            and holistic learning, under the banner of St. Andrews School. A well-reputed institution 
            with over three decades of experience in the field of education, St. Andrews School now extends 
            its legacy on a national scale to a wider and diversified community through Winmore.</p>
                <p>A vibrant learning culture, a strong academic standard and an array of opportunities for skill development – 
            Winmore Academy provides an ideal platform for the bright future of our children. Pushing the limits of academics
            and non-academics, Winmore crafts an atmosphere of curiosity for knowledge and living, developing the potential of
            the makers of tomorrow.</p>
               
            </div>
            <div class="clearfix"> </div>

        </div>
    </div>
    <!-- //about -->   
   <!-- services -->
   <div class="panel-sec section-w3ls">
        <div class="container">
            <h3 class="agileits-title">HIGHLIGHTS</h3>
            <!-- timings -->
            <div class="w3l-about timing">
                <div class="col-md-4 w3_service_bottom_grid1">
                    <div class="about-w3left">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h5 class="panel-title asd">
                                        <a class="pa_italic" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                            <i class="glyphicon glyphicon-minus" aria-hidden="true"></i>SCHOOLS
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true"
                                    >
                                    <div class="panel-body panel_text">
                                        <ul class="b-list">
                                        <li>The 10-acre Winmore Academy, Whitefield,
                                            (previously known as St. Andrews School,
                                            Whitefield), currently in its second year, will
                                            offer admissions from LKG to grade VII. </li>
                                                            <li>The upcoming 2-acre campus at Jakkur,
                                            opening in the next academic year, will offer
                                            admissions from playschool to grade V. </li>
                                                            <li>Both the schools will be upgraded in a
                                            phased-manner to fully fledged K-12 and K10
                                            schools respectively. </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                      
                        
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="col-md-7 col-sm-12 agile_timing_right">
                        
                        <ul>
                            <li>
                                <span>Spacious digital classrooms</span></li>
                            <li>
                                <span>Open courtyards</span></li>
                                <li>
                                <span>Up-to-date composite science laboratories</span></li>
                            <li>
                                <span>State-of-the-art ICT suites</span></li>
                                <li>
                                <span>Well-stocked libraries</span></li>
                                <li>
                                <span>Dedicated language and music rooms</span></li>
                                <li>
                                <span>Art and craft rooms</span></li>
                                <li>
                                <span>Play areas</span></li>
                                <li>
                                <span>Cricket nets</span></li>
                                <li>
                                <span>Skating rinks
                                </span></li>
                        </ul>
                    </div>
                    <div class="col-md-5 menu-bg"></div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <!-- //timings -->
    </div>
   	<!-- projects -->

	<div class="gallery" id="projects">
		<div class="container">
			<h3 class="w3ls-title">
                <span>G</span>ALLERY</h3>
			<div class="agile_gallery_grids w3-agile demo">
				
				<div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g2.jpg">
                        <div class="stack twisted">    
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g2.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
                    </div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g3.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g3.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g4.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g4.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g5.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g5.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g6.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g6.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g7.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g7.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g8.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g8.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g10.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g10.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g12.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g12.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g13.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g13.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g14.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g14.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g15.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g15.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g16.jpg">
                        <div class="stack twisted">
                             <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g16.jpg" alt=" " class="img-responsive" />							
						</div>
						</a>
					</div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g17.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g17.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g18.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g18.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g19.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g19.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g20.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g20.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g21.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g21.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g22.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g22.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g23.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g23.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g24.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g24.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g25.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g25.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Winmore Academy" href="<?php echo S3_URL?>/site/winmore-assets/images/g26.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/winmore-assets/images/g26.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
                </div>
                
			</div>
		</div>
	</div>


           <!-- services bottom -->
    <div class="section-w3ls services">
        <div class="container">
            <div class="services-right">
                <div class="services-grid">
                  
                    <div class="col-md-12 col-sm-12 col-xs-12 sr-txt">
                        
                    <h5>Campus</h5>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="services-grid">                  
                    <div class="col-md-12 col-sm-12 col-xs-12 sr-txt">
                        <p>The school campus influences the well-being and growth of a child. Winmore offers state-of-the-art campuses and top-notch infrastructure to maximise the learning opportunities for our students.</p>
                        <p>The ten-acre campus in Whitefield, located in tranquil settings with lush green surroundings and open spaces, creates an ideal learning environment. The school is currently in its second year and offers admissions from kindergarten to grade VII.</p>
                        <p>The two-acre campus situated in the easily accessible suburb of Jakkur, opening in the next academic year, will offer admissions from kindergarten to grade V.</p>
                        <p>Both the schools cater to the needs of the students by providing world-class amenities and will be upgraded in a phased-manner to fully fledged K-12 and K-10 schools respectively.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="services-left">
                <h4 class="title">Curriculum</h4>
                
                <p class="data">Academics at Winmore is engaging and exciting – young learners are encouraged to be inquisitive and to think critically. The CBSE curriculum offered is broad, innovative and supplemented by a variety of hands-on activities. The educational programme focuses not only on academic development, but also on the creative, physical, cultural and spiritual growth of children.</p>
                <p class="data">Celebrating the uniqueness of each child, we offer a wide spectrum of opportunities to develop their skills and unleash their inherent potential. ‘The School of Life’ houses world-class amenities to develop their talents in music, drama, visual arts and sports. Students of Winmore are given the scope for creativity and imagination, helping with their over-all development.</p>
                <p class="data">Taking care of building healthy bodies along with healthy minds, students are provided with a variety of sporting activities to choose from. The school offers after-school training in Cricket, Basketball and Football under the able guidance of accredited coaches. Our top-class sporting infrastructure complements and aids in developing the talents under our roof.</p>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!--//services bottom-->
        <div class="w3ls-services section-w3ls">
        <div class="container">
            <h3 class="w3ls-title">
                <span>F</span>EATURES</h3>
            <div class="grid">
                <figure class="col-md-6 col-xs-12 w3l-service-hover">
                    <img src="<?php echo S3_URL?>/site/winmore-assets/images/f1.jpg" alt="t1" class="img-responsive" />
               
                </figure>
                <figure class="col-md-6 col-xs-12  w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/winmore-assets/images/f2.jpg" alt="t2" class="img-responsive" />
                   
                </figure>
    
                <div class="clearfix"></div>
                <figure class="col-md-6 col-xs-12  w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/winmore-assets/images/f3.jpg" alt="t2" class="img-responsive" />
         
                </figure>
                <figure class="col-md-6 col-xs-12 w3l-service-hover">
                    <img src="<?php echo S3_URL?>/site/winmore-assets/images/f4.jpg" alt="t1" class="img-responsive" />
               
                </figure>
                <div class="clearfix"></div>
                <figure class="col-md-6 col-xs-12  w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/winmore-assets/images/f5.jpg" alt="t2" class="img-responsive" />
                   
                </figure>
    
                
                <figure class="col-md-6 col-xs-12  w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/winmore-assets/images/f6.jpg" alt="t2" class="img-responsive" />
         
                </figure>
                <div class="clearfix"></div>
                <figure class="col-md-6 col-xs-12  w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/winmore-assets/images/f7.jpg" alt="t2" class="img-responsive" />
                   
                </figure>
    
                
                <figure class="col-md-6 col-xs-12  w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/winmore-assets/images/f8.jpg" alt="t2" class="img-responsive" />
         
                </figure>
                <div class="clearfix"></div>
                
                <figure class="col-md-6 col-xs-12  w3l-service-hover s2">
                    <img src="<?php echo S3_URL?>/site/winmore-assets/images/f9.jpg" alt="t2" class="img-responsive" />
         
                </figure>
                <div class="clearfix"></div>
        
     
            </div>
        </div>
    </div>
   

    <!-- //testimonials -->
       <!-- contact -->
       <div class="contact-bottom section-w3ls main-pos" id="contact">
        <div class="container">
            <h3 class="w3ls-title">
                <span>F</span>ind us</h3>
         
            <div class="clearfix"></div>
            <div class="contact-main">
                <div class="col-md-6 col-sm-6 col-xs-6 map">
                    <iframe class="mapBorder" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d497574.5163066006!2d77.794868!3d13.0165!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe766aa6158ee518e!2sWinmore+Academy%2C+Bengaluru!5e0!3m2!1sen!2sin!4v1542696923006" width="100%"  frameborder="0"  allowfullscreen></iframe>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 map">
                    <iframe class="mapBorder" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7772.364479030663!2d77.602832!3d13.087634!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x34c73c21ee374209!2sWinmore+Academy%2C+Jakkur%2C+Bengaluru!5e0!3m2!1sen!2sin!4v1542697003885" width="100%"  frameborder="0" allowfullscreen=""></iframe>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- //contact -->
       <!-- footer -->
       <div class="agile-footer w3ls-section">
        <div class="container">
       
            <div class="agileits_w3layouts-copyright">
                <p>Copyright 2018 All Rights Reserved Winmore Academy
                    <!-- <a href="#" > Privacy Policy </a> | <a href="#" > Disclaimer </a> -->
                </p>
            </div>
        </div>
    </div>
    <!-- //footer -->
    <!-- contact form start -->
    <div class="floating-form visiable" id="contact_form">
    <div class="contact-opener">Enquire Now</div>
   <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="winmore_jsfrm('<?php echo SITE_URL?>winmore/submit_frm','2')">
                    <div class="form-logo">
                                <a href="#">
                                <img src="<?php echo S3_URL?>/site/winmore-assets/images/logo.jpg" />                                   
                                </a> 
                            </div>  
                    <div class="group">
                        <div class="form-group">                       
                            <input type="text" class="form-control" id="name2" name="name" placeholder="Name" 
                            onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err2"></span>
                        </div>              
                    </div>  

                    <div class="group">
                        <div class="form-group">                   
                            <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                            <span class="help-block" id="email_err2" ></span>
                        </div>
						
								<div class="form-group">
                              <select  name="campus" id="campus2" class="form-control campus" onkeyup="chck_valid('campus', 'Please Select Preferred campus')" data-attr="Please Select Preferred campus" onchange="gradechange('2')">
                                 <option value="" selected disabled>Preferred Campus</option>
								 <option value="whitefield">Whitefield</option>
                                  <option value="jakkur ">Jakkur</option>
                              </select>
							   <span class="help-block" id="campus_err2"> </span> 
                           </div>

                        <div class="form-group">
                              <select  name="grade" id="grade2" class="form-control grade" onkeyup="chck_valid('grade', 'Please Select Preferred Grade')" data-attr="Please Select Preferred Grade">
                                 <option value="" selected disabled>Preferred Grade</option>
								 <?php foreach($all_array['WINMORE_GRADE'] as $k=>$v){ ?>
                                 <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                 <?php }?>
                                 
                              </select>
							   <span class="help-block" id="grade_err2"> </span> 
                           </div>

                        <div class="form-group">
                            <input type="hidden" class="hiddenCountry" name="CountryCode" id="CountryCode2" value="91">						
                            <input type="text" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*"  
                            placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" 
                            data-attr="Please enter correct Mobile number">
                            <span class="help-block" id="phone_err2"> </span>
                        </div> 
                    </div>      
                
                    
                    <div class="submitbtncontainer">
                     <input type="submit" id="frm-sbmtbtn2" value="Submit" name="submit" class="frm-sbm">
                    </div>
                </form>
    <div>
   <div class="popup-enquiry-form mfp-hide" id="popupForm">
                <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" 
                onsubmit="winmore_jsfrm('<?php echo SITE_URL?>winmore/submit_frm','3')">
                    <div class="form-logo">
                        <a href="#">
                        <img src="<?php echo S3_URL?>/site/winmore-assets/images/logo.jpg" /> 

                        </a> 
                    </div>  
                    <div class="group">
                    <div class="form-group">                   
                            <input type="text" class="form-control" id="name3" name="name" placeholder="Name" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                            <span class="help-block" id="name_err3" ></span>
                        </div>
                        <div class="form-group">                   
                            <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                            <span class="help-block" id="email_err3" ></span>
                        </div>

						<div class="form-group">
                              <select  name="campus" id="campus3" class="form-control campus" onkeyup="chck_valid('campus', 'Please Select Preferred campus')" data-attr="Please Select Preferred campus" onchange="gradechange('3')">
                                 <option value="" selected disabled>Preferred Campus</option>
								 <option value="whitefield">Whitefield</option>
                                  <option value="jakkur ">Jakkur</option>
                              </select>
							   <span class="help-block" id="campus_err3"> </span> 
                           </div>
                        <div class="form-group">
                              <select  name="grade" id="grade3" class="form-control grade" onkeyup="chck_valid('grade', 'Please Select Preferred Grade')" data-attr="Please Select Preferred Grade">
                                 <option value="" selected disabled>Preferred Grade</option>
								 <?php foreach($all_array['WINMORE_GRADE'] as $k=>$v){ ?>
                                 <option value="<?php echo $k;?>"><?php echo $v;?></option>
                                 <?php }?>
                                 
                              </select>
							   <span class="help-block" id="grade_err3"> </span> 
                           </div>

                    <div class="form-group"> 
					 <input type="hidden" class="hiddenCountry" name="CountryCode" id="CountryCode3" value="91">
                        <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*"  
                        placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" 
                        data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="phone_err3"> </span>
                    </div> 
                    </div>  
                
                    
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit" class="frm-sbm">
                    </div>
                </form>
    </div>   
    <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
    <input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
	<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
	<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
	<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">

    <!-- banner slider -->
    <script src="<?php echo S3_URL?>/site/winmore-assets/js/modernizr-2.6.2.min.js"></script>
    <script src="<?php echo S3_URL?>/site/winmore-assets/js/jquery.zoomslider.min.js"></script>
    <!-- //banner slider -->
    <!-- //gallery -->
	
    <script src="<?php echo S3_URL?>/site/winmore-assets/js/jquery.tools.min.js"></script>
    <script src="<?php echo S3_URL?>/site/winmore-assets/js/jquery.mobile.custom.min.js"></script>
    <script src="<?php echo S3_URL?>/site/winmore-assets/js/jquery.cm-overlay.js"></script>
	<script src="<?php echo S3_URL?>/site/scripts/winmore.js"></script>
   
    <!-- //gallery -->
    <!-- testimonials -->
    <!-- required-js-files-->
	<!-- Magnific Popup core JS file -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo S3_URL?>/site/winmore-assets/js/owl.carousel.js"></script>
   
    <!--//required-js-files-->
    <!-- start-smooth-scrolling -->
    <script src="<?php echo S3_URL?>/site/winmore-assets/js/move-top.js"></script>
    <script src="<?php echo S3_URL?>/site/winmore-assets/js/easing.js"></script>

    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
  
    <script src="<?php echo S3_URL?>/site/winmore-assets/js/SmoothScroll.min.js"></script>
    <!-- //smooth-scrolling-of-move-up -->
    <script src="<?php echo S3_URL?>/site/winmore-assets/js/SmoothScroll.min.js"></script>
    <!-- navigation  -->
    <script src="<?php echo S3_URL?>/site/winmore-assets/js/intlTelInput.min.js"></script>
    <script src="<?php echo S3_URL?>/site/winmore-assets/js/main.js"></script>

   </body>
</html>