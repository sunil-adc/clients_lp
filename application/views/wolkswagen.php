<!doctype html>
<html>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131460563-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-131460563-1');
</script>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="">
<title>Volkswagen</title>
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo S3_URL?>/site/wolkswagen-assets/images/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo S3_URL?>/site/wolkswagen-assets/images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo S3_URL?>/site/wolkswagen-assets/images/favicon-16x16.png">
<link rel="manifest" href="http://www.volkswagenfinancialservices.co.in/vwsecure/images/icon/site.webmanifest">
<link rel="mask-icon" href="<?php echo S3_URL?>/site/wolkswagen-assets/images/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Ubuntu:400,500,700" rel="stylesheet">

<meta name="robots" content="noindex, nofollow">
<link rel="stylesheet" href="<?php echo S3_URL?>/site/wolkswagen-assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo S3_URL?>/site/wolkswagen-assets/css/font-awesomesome.min.css" />
<link rel="stylesheet" href="<?php echo S3_URL?>/site/wolkswagen-assets/css/bootstrap-select.min.css" /> 

<link rel="stylesheet" href="<?php echo S3_URL?>/site/wolkswagen-assets/css/style.css" />
<link rel="stylesheet" href="<?php echo S3_URL?>/site/wolkswagen-assets/css/style1.css" />
<link rel="stylesheet" href="<?php echo S3_URL?>/site/wolkswagen-assets/css/basictable.css" />
<link rel="stylesheet" href="<?php echo S3_URL?>/site/wolkswagen-assets/css/responsive.css" /> 
<link href="<?php echo S3_URL?>/site/wolkswagen-assets/css/owl.carousel.css" rel="stylesheet" type="text/css" />
<link href="<?php echo S3_URL?>/site/wolkswagen-assets/css/owl.theme.css" rel="stylesheet" type="text/css" />
<style type="text/css">
.parsley-required,.parsley-type {
  color: red !important;
  font-size: 14px; 
  list-style: none;
  margin-left:0px; 

}
.parsley-errors-list {
  color: red !important;
  font-size: 14px; 
  list-style: none;
  margin-left:0px; 
  padding-left:0px;
}  
</style>
</head>

<body class="">
<header class="header header-2 "  style="    padding-top: 0px;">
  <nav class="navbar navbar-default header-navigation stricky">
    <div class="container" >
    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12 navbar-header"> <a class="navbar-brand" href="#"> 
          <img src="<?php echo S3_URL?>/site/wolkswagen-assets/images/logo-left.png" alt="" class="img-responsive" /> </a> </div>
      <div class="col-md-6 col-sm-6 col-xs-12  navbar-header navbar-header-right pull-right" > 
          <a class="navbar-brand" href="#" > <img src="<?php echo S3_URL?>/site/wolkswagen-assets/images/logo-right.png" alt="" class="img-responsive" /> </a> </div>
      </div>
    </div>
  </nav>
</header>
<section>
  <div id="slideshow" class="owl-carousel">
    <div class="item">
      <div class="container">
        <h2><span class="black_strip">Adapts to your Adventure. </span> <span class="white_strip">And your Finance Options.</span> </h2>
      </div>
      <img class="img-responsive" alt="banner" src="<?php echo S3_URL?>/site/wolkswagen-assets/images/banner-6.jpg"  /> </div>
    <div class="item">
      <div class="container">
        <h2><span class="black_strip">High on Adventure. </span> <span class="white_strip">Low on EMI!</span> </h2>
      </div>
      <img class="img-responsive" alt="banner" src="<?php echo S3_URL?>/site/wolkswagen-assets/images/banner-2.png" /> </div>
    <div class="item">
      <div class="container">
        <h2><span class="black_strip">Your kind of Adventure. </span> <span class="white_strip">Your kind of Assured Returns!</span> </h2>
      </div>
      <img class="img-responsive" alt="banner" src="<?php echo S3_URL?>/site/wolkswagen-assets/images/banner-3.png" /> </div>
    <!-- <div class="item">
      <div class="container">
        <h2><span class="black_strip">Adapts to your Adventure. </span> <span class="white_strip">And your Finance Options.</span> </h2>
      </div>
      <img class="img-responsive" alt="banner" src="images/banner-6.jpg" /> </div>
    <div class="item">
      <div class="container">
        <h2><span class="black_strip">Adapts to your Adventure. </span> <span class="white_strip">And your Finance Options.</span> </h2>
      </div>
      <img class="img-responsive" alt="banner" src="images/banner-6.jpg" /> </div>
    <div class="item">
      <div class="container">
        <h2><span class="black_strip">Adapts to your Adventure. </span> <span class="white_strip">And your Finance Options.</span> </h2>
      </div>
      <img class="img-responsive" alt="banner" src="images/banner-6.jpg" /> </div> -->
  </div>
</section>
<section class="book-now-section" id="contact">
  <form role="form" method="post" data-parsley-validate id="frmSendAudiChoiceFeedback" action="JavaScript:void(0)" onsubmit="wolkswagen_jsfrm('<?php echo SITE_URL?>volkswagen/submit_frm')">
  <!-- <form role="form" action="" method="post" id="frmSendAudiChoiceFeedback"> -->
    <input type="hidden" name="txtAudiChoiceSubject" id="txtAudiChoiceSubject" value="VW Secure Lead">
    <div class="container">
      <h2 class="dark_title_style">Request a call back</h2>
      <div class="form_row_style">
        <div class="form-grp">
          <input class="form-control"  name="name"  id="name"  placeholder="Name *" type="text" />
           <span class="help-block" id="name_err"> </span>
        </div>
        <div class="form-grp">
          <input  name="email"  id="email"  placeholder="Email *" type="email" class="form-control" />
          <span class="help-block" id="email_err"> </span>
        </div>
        <div class="form-grp">
          <!-- <input required name="textAudiChoicemobile" placeholder="Mobile *" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" pattern="[0-9]*" inputmode="numeric" type="text" class="form-control" /> -->
          <input  name="mobile"  id="mobile" maxlength="15" placeholder="Mobile *"  inputmode="numeric" type="text" class="form-control" />
           <span class="help-block" id="mobile_err"> </span>
        </div>
        <div class="form-grp slect-lg-box">
          <select class="selectpicker" id="dealer" name="dealer" >
            <option value="">Select Nearest Dealer *</option>
            <!-- <option value="Audi Ahmedabad">Volkswagen Ahmedabad</option>
            <option value="Audi Bengaluru">Volkswagen Bengaluru</option>
            <option value="Audi Bengaluru Central">Volkswagen Bengaluru Central</option>
            <option value="Audi Navi Mumbai">Volkswagen Navi Mumbai</option>
            <option value="Audi Pune">Volkswagen Pune</option>
            <option value="Audi Raipur">Volkswagen Raipur</option>
            <option value="Audi Rajkot">Volkswagen Rajkot</option>
            <option value="Audi Ranchi">Volkswagen Ranchi</option>
            <option value="Audi Surat">Volkswagen Surat</option>
            <option value="Audi Thane">Volkswagen Thane</option>
            <option value="Audi Udaipur">Volkswagen Udaipur</option>
            <option value="Audi Vadodara">Volkswagen Vadodara</option>
            <option value="Audi Vizag">Volkswagen Vizag</option> -->
            <option value="VW Cochin">Cochin Volkswagen</option>
            <option value="VW Kottayam">Kottayam Volkswagen</option>
            <option value="VW Perumbavoor">Perumbavoor Volkswagen</option>
            <option value="VW Thiruvananthapuram">Thiruvananthapuram Volkswagen</option>
            <option value="VW Kollam">Kollam Volkswagen</option>
          </select>
           <span class="help-block" id="dealer_err"> </span>
        </div>
        <div class="form-grp slect-lg-box">
          <select class="selectpicker"  name="car" id="car">
            <option value="">Select Your Car  *</option>
            <!-- <option value="audi-a3">Volkswagen A3</option>
            <option value="audi-a4">Volkswagen A4</option>
            <option value="audi-a6">Volkswagen A6</option>
            <option value="audi-q3">Volkswagen Q3</option>
            <option value="audi-q5">Volkswagen Q5</option> -->
            <option value="VW Tiguan">Volkswagen Tiguan</option>
          </select>
           <span class="help-block" id="car_err"> </span>
        </div>
        <div class="form-grp  interested_btn_style">
          <input type="hidden" id="formAction" name="formAction" value="" />

           <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
   <input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
   <input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
  <input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
  <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
 
          <!-- <input value="I am Interested" class="thm-btn" width="100%"  type="submit" id="btnSubmitSendAudiChoiceFeedback" name="btnSubmitSendAudiChoiceFeedback" onclick="return submitSendAudiChoiceFeedback();"> -->
          <input value="I am Interested" class="thm-btn" width="100%"  type="submit" id="submit" name="submit" >

        </div>
      </div>
    </div>
  </form>
  <div id="thankyoubox" style="display: none;">
    <h2>Thank You !</h2>
    <p>Our customer care executive will contact you shortly.</p>
  </div>



</section>
<section class="welcome-section sec-pad home_id_link">
  <div class="container">
    <div class="row">
      <div class="col-md-12"> <a href="#intro">
        <button class="thm-btnas">Introduction</button>
        </a> <a href="#plan">
        <button class="thm-btnas">End of Term Options</button>
        </a> <a href="#upgrade">
        <button class="thm-btnas">Upgrade</button>
        </a> <a href="#benefits">
        <button class="thm-btnas">Features</button>
        </a> <a href="#faq">
        <button class="thm-btnas">FAQ</button>
        </a> <a href="#emi">
        <button class="thm-btnas">Volkswagen Secure Advantage</button>
        </a> </div>
    </div>
  </div>
</section>
<section class="welcome-section sec-pad pd80-B lighten_bg" id="intro">
  <div class="container">
    <div class="row">
      <div class="sec-title">
        <h2 class="main_title_style mrg0-T">Introduction</h2> 
      </div> 
      <div class="col-md-6"> <div class="intro_img_style"><img src="<?php echo S3_URL?>/site/wolkswagen-assets/images/Introduction_left_img.jpg" alt="" /> </div> </div>
      <div class="col-md-6">
        <div class="intro_text_style">
          <h3>What is Volkswagen Secure? </h3>
          <p class="title_btm_line"></p>
          <p>The time is right to drive a Volkswagen that best suits your lifestyle and ambitions. Volkswagen has always been known for its care, especially when it comes to your financial security.</p>
          <p> Introducing Volkswagen Secure, an exclusive finance solution from Volkswagen Financial Services, specially crafted to meet your financial needs, thus securing your peace of mind.</p>
          <p>A finance solution, which offers you, Assured Future Value of your Volkswagen, and provides you with secure options to select from at the end of the term.</p>
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="welcome-section sec-pad benefits_box_style" id="intro">
  <div class="container">
    <div class="row">
      <div class="col-md-12" >
       <div class="sec-title">
        <h2 class="main_title_style">Benefits of Volkswagen Secure</h2>
        </div> 
		  <ul>
		  	<li><i class="fa fa-angle-right"></i> 40%* lower EMI for 3 years</li>
		  	<li><i class="fa fa-angle-right"></i>Assured Future Value - Determined at the time of purchase</li>
        <li><i class="fa fa-angle-right"></i>Eligible Models - Volkswagen Tiguan</li>
		  </ul>
      </div>
    </div>
  </div>
</section>
<section class="our-news sec-pad" id="upgrade">
  <div class="container">
    <div class="sec-title" >
      <h2 class="main_title_style">Upgrade</h2>   
    </div>
     <p class="dark_text_style sm-text">A new Volkswagen every three years.</p>

    <div class="row light-bg">
      <div class="col-md-6 pd-0"><div class="Upgrade_left_box"> <img src="<?php echo S3_URL?>/site/wolkswagen-assets/images/upgread.jpg" > </div> </div>
      <div class="col-md-6">
        <div class="Upgrade_right_box">
          <h3> Wish to upgrade to a new Volkswagen?</h3>
          <p>Do it with ease, as Volkswagen Secure assures the future value of your Volkswagen and puts you in a great position for an upgrade at the end of your term.</p>
        </div>
      </div>
    </div>
  </div>
</section>
<section  id="plan" class="sec-pad-btm-none">
  <div class="sec-title">
    <h2 class="main_title_style">End of Term Options</h2> 
  </div>
  <div class="container term-option-maindiv">
    <div class="term-option">
      <div class="row">
      <div class="col-xs-3 col-md-4"></div>
      <div class="col-xs-6 col-md-4 rounded-circle-div">
        <div class="rounded-circle">
          <h4>Return <span class="bg_rotate"></span></h4>
          <p class="bigger-text-style cnt1"> All affairs come to an end.</p>
          <p class="smaller-text-style">Got the full experience of your Volkswagen? Now, just return it back to the dealership.</p>
        </div>
      </div>
      <div class="col-xs-3 col-md-4"></div>
    </div>
    <div class="row">
      <div class="col-xs-6 col-md-4 rounded-circle-div">
        <div class="rounded-circle circle2">
          <h4>Retain <span class="bg_rotate"></span></h4>
          <p class="bigger-text-style cnt2"> Make your Volkswagen yours forever.</p>
          <p  class="smaller-text-style">Just pay the remaining amount at the end of the tenure.</p>
        </div>
      </div>
      <div class="col-xs-0 col-md-4"></div>
      <div class="col-xs-6 col-md-4 rounded-circle-div">
        <div class="rounded-circle circle2">
          <h4>Refinance <span class="bg_rotate"></span></h4>
          <p class="bigger-text-style cnt3"> Can’t let go of  your Volkswagen?</p>
          <p   class="smaller-text-style">Just continue your tenure for another contract term.</p>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>

<section class="our-news sec-pad light-bg" id="emi"> 
  <div class="container">
    <div class="sec-title">
      <h2 class="main_title_style">Volkswagen Secure advantage</h2>  
    </div>
       <p class="dark_text_style lg-text" >Comparison - Classic Credit  Vs Volkswagen Secure</p>

    <div class="row">
      <div class="col-md-12">
        <table class="table table-bordered table-style table-style-width table-breakpoint">
          <thead>
            <tr>
              <th>Variable</th>
              <th>Classic Credit</th>
              <th>Volkswagen Secure*</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Car Price<br>
                (Ex Showroom)</td>
              <td> 30,87,611</td>
              <td> 30,87,611</td>
            </tr>
            <tr>
              <td>Finance Amount</td>
              <td> 27,78,850</td>
              <td> 27,78,850</td>
            </tr>
            <tr>
              <td>Tenure (months)</td>
              <td> 36</td>
              <td> 36</td>
            </tr>
            <tr>
              <td>EMI (1 - 36) </td>
              <td> 88,263</td>
              <td> 52,297</td>
            </tr>
            <tr>
              <td>Assured Future Value </td>
              <td> N.A.</td>
              <td> 16,98,186</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
<section class="our-news sec-pad-top-none" id="benefits" >
  <div class="container">
    <div class="sec-title">
      <h2 class="main_title_style">Features</h2>  
    </div>
	<div class="features-content">
		<ul>
			<li> <a class="option-heading">
        <p class="border-field-style">Assured Future Value <span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p class="text-justify">Get an Assured Future Value for your Volkswagen, determined at the time purchase.</p>
        </div></li>
			<li><a   class="option-heading">
        <p class="border-field-style">Lower EMI<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p  class="text-justify">Volkswagen  Secure offers you 40%* lower EMI as compared to the standard EMIs payable.</p>
        </div></li>
			<li><a class="option-heading">
        <p class="border-field-style">Security<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p  class="text-justify">A finance solution tailored for potential Volkswagen owners who are looking for complete security at the end of loan term. You have the secure options to Retain, Return, Upgrade or Refinance your car at the end of the tenure. Low EMI (almost 40% lower than std. loan) & Assured Future Value are the biggest advantages that Volkswagen Secure offers you</p>
        </div></li>
			<li><a  class="option-heading">
        <p class="border-field-style">Volkswagen  Support<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p class="text-justify">Volkswagen Secure extends to you every support you need – pre as well as post sales, wherever and whenever.</p>
        </div></li>
		</ul>
	</div>
    
  </div>
</section>
<hr class="mrg-0">
<!-- <section class="our-news sec-pad accordian-style" id="faq" >
  <div class="container">
    <div class="sec-title">
      <h2 class="main_title_style">Frequently Asked Questions</h2>  
    </div>
    <div class="row">
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>What is Volkswagen  Secure? <span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>An assured buyback program catered for your customers offering a complete peace of mind.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a  class="option-heading">
        <p>What is Residual Value? <span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Residual Value is a defined percentage of net ex-showroom price, which is assured at the end of loan term to customer as per the prescribed conditions.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>Which Volkswagen models are eligible for the program?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>All variants of the Volkswagen Tiguan are eligible for Volkswagen Secure.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>Can a customer choose his/her own motor insurance provider? <span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Customer is required to purchase comprehensive motor insurance coverage recommended by the respective Volkswagen Dealer.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>Is extended warranty part of the offer? <span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div class="option-content">
          <p>Manufacture warranty is standard for the first two years; however in the program additional one year / two year extended warranty depending on the opted loan tenure is mandatory at customers cost.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>Who is eligible for Volkswagen Secure?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>All salaried and non- salaried applicants are eligible as per credit underwriting policy of Volkswagen Financial Services.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>What is the maximum finance amount offered under the program?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Eligible customer can be funded up to 100% of the net ex-showroom price meeting all the necessary conditions as per the Volkswagen Finance credit policy.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>Can the customer get the finance amount for Vehicle Registration, Motor Insurance and Extended Warranty?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>In the current product structure this RTO funding is not available but the add-on products like EW and MI funding will be included in our product update. SVP is compulsory, but funding is not available for the same.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>What is the maximum allowed tenure under this program?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>The tenure offered currently is only for 36 and 48 months.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>How the customer is assured of the residual value of his/her vehicle?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>During the execution of the contract, a repurchase agreement will be signed between the customer and the dealer indicating the assured residual value.
          </p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a  class="option-heading">
        <p>Does Volkswagen Secure impose a capping on the mileage that should be clocked in during the tenure?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Yes, customer is only allowed to clock in maximum kilometers during the entire tenure agreed in repurchase agreement. Customer will be required to pay for each additional kilometre exceeding the threshold based on a prescribed amount per kilometre. The prescribed amount will be reflected in the repurchase agreement.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a  class="option-heading">
        <p>Can a customer foreclose the loan before the end of the loan tenure?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Yes, the customer has the right to foreclose the account at any time, however the repurchase agreement will be considered null and void.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a  style="cursor:pointer" class="option-heading">
        <p>What are the conditions for Car return at the end of loan tenure?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Customer is required to adhere to the prescribed wear and tear guidelines and should maintain the vehicle through authorized Volkswagen service centres. </p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>When is a customer required to inform of his options?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Ideally the customer is required to get in touch with his/her dealer 75 days prior to the end of the loan tenure.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>What is the process involved for returning the vehicle?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Customer is required to get in touch with the Volkswagen dealership 1 month prior to the end of tenure to confirm his final options and complete the final valuation of his/her vehicle in accordance with the fair wear and tear terms.</p>
        </div>
      </div>
      <div class="col-md-12 load_more_btn"><a href="#"  id="loadMore">View more <i class="fa fa-chevron-down" aria-hidden="true"></i></a></div>
    </div>
  </div>
</section> -->
<section class="our-news sec-pad accordian-style" id="faq" >
  <div class="container">
    <div class="sec-title">
      <h2 class="main_title_style">Frequently Asked Questions</h2>  
    </div>
    <div class="row">
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>What is Volkswagen  Secure? <span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>An assured buyback program catered for your customers offering a complete peace of mind.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a  class="option-heading">
        <p>What is Residual Value? <span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Residual Value is a defined percentage of net ex-showroom price, which is assured at the end of loan term to customer as per the prescribed conditions.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>Which Volkswagen models are eligible for the program?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>All variants of the Volkswagen Tiguan are eligible for Volkswagen Secure.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>Can a customer choose his/her own motor insurance provider? <span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Customer is required to purchase comprehensive motor insurance coverage recommended by the respective Volkswagen Dealer.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>Is extended warranty part of the offer? <span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div class="option-content">
          <p>Manufacture warranty is standard for the first two years; however in the program additional one year / two year extended warranty depending on the opted loan tenure is mandatory at customers cost.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>Who is eligible for Volkswagen Secure?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>All salaried and non- salaried applicants are eligible as per credit underwriting policy of Volkswagen Financial Services.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>What is the maximum finance amount offered under the program?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Eligible customer can be funded up to 100% of the net ex-showroom price meeting all the necessary conditions as per the Volkswagen Finance credit policy.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>Can the customer get the finance amount for Vehicle Registration, Motor Insurance and Extended Warranty?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>In the current product structure this RTO funding is not available but the add-on products like EW and MI funding will be included in our product update. SVP is compulsory, but funding is not available for the same.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>What is the maximum allowed tenure under this program?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>The tenure offered currently is only for 36 and 48 months.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>How the customer is assured of the residual value of his/her vehicle?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>During the execution of the contract, a repurchase agreement will be signed between the customer and the dealer indicating the assured residual value.
          </p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a  class="option-heading">
        <p>Does Volkswagen Secure impose a capping on the mileage that should be clocked in during the tenure?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Yes, customer is only allowed to clock in maximum kilometers during the entire tenure agreed in repurchase agreement. Customer will be required to pay for each additional kilometre exceeding the threshold based on a prescribed amount per kilometre. The prescribed amount will be reflected in the repurchase agreement.</p>
        </div>
      </div>
       <div class="col-md-12 q-box"> <a  class="option-heading">
        <p>What are the options available at the end tenure?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Customer has 4 options to consider before the end of  tenure:<br>
Retain – The customer will retain the car at the end of loan tenure and pay the outstanding balance to close the loan<br>
Refinance – Customer can opt for refinance facility and payback in equated monthly instalments<br>
Return – The customer can return the vehicle and pay the difference if any due to the wear and tear as defined in the repurchase agreement and dealer will pay the outstanding balance and close the account.<br>
Upgrade – The customer opt to return the vehicle and upgrade to buy a higher variant / model of the brand.</p>
  </div>
      </div>
       <div class="col-md-12 q-box"> <a  class="option-heading">
        <p>Can a customer foreclose the loan before the end of the loan tenure?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Yes, the customer has the right to foreclose the account at any time, however the repurchase agreement will be considered null and void.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a  style="cursor:pointer" class="option-heading">
        <p>What are the conditions for Car return at the end of loan tenure?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Customer is required to adhere to the prescribed wear and tear guidelines and should maintain the vehicle through authorized Volkswagen service centres. </p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>When is a customer required to inform of his options?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Ideally the customer is required to get in touch with his/her dealer 75 days prior to the end of the loan tenure.</p>
        </div>
      </div>
      <div class="col-md-12 q-box"> <a class="option-heading">
        <p>What is the process involved for returning the vehicle?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Customer is required to get in touch with the Volkswagen dealership 1 month prior to the end of tenure to confirm his final options and complete the final valuation of his/her vehicle in accordance with the fair wear and tear terms.</p>
        </div>
      </div>
                   <div class="col-md-12 q-box"> <a class="option-heading">
        <p>Where shall the customer return the vehicle?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>The customer shall return the vehicle to the Dealer at his workshop or any other location specified by the Dealer.</p>
        </div>
      </div>
                   <div class="col-md-12 q-box"> <a class="option-heading">
        <p>Who shall foreclose the loan account with Volkswagen Finance in the event customer decides to return the vehicle?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>The Dealer will foreclose the customers’ loan account with Volkswagen Finance based on the agreed buy back value. Any additional charges to be borne by the customer. </p>
        </div>
      </div>
                   <div class="col-md-12 q-box"> <a class="option-heading">
        <p>Can the foreclosure amount be paid to the customer by the Dealer?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>The foreclosed amount will get transferred to Volkswagen Finance by the Dealer, under no circumstance will the foreclosed amount be transferred to the customer.</p>
        </div>
      </div>
                  <div class="col-md-12 q-box"> <a class="option-heading">
        <p>Who will be responsible to conduct the valuation of the vehicle?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>The vehicle shall be subject to a inspection by Volkswagen India and repairs if any shall be undertaken (costs will be borne by the Customer) and completed prior to the physical handover of the car to Dealer. However we strongly recommend to take a superior insurance coverage from in-house insurance companies.</p>
        </div>
      </div>
                <div class="col-md-12 q-box"> <a class="option-heading">
        <p>If there is a dispute on the final valuation proposed by the dealer, how will this be resolved?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>The customer has the option to sell the car in open market if he is getting a better value for his car, but needs to pay the balance amount to Volkswagen Finance to close the loan.</p>
        </div>
      </div>
                  <div class="col-md-12 q-box"> <a class="option-heading">
        <p>Can a customer do any retrofitting on the car?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>There shall be no after-market (grey market) retro-fitments on the cars. Retro-fitments if any shall only be those as carried by the Dealer as per the terms & condition of repurchase agreement.</p>
        </div>
      </div>
                <div class="col-md-12 q-box"> <a class="option-heading">
        <p>What if the terms and conditions stipulated in the repurchase agreement is not met?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>If any or all of the conditions stipulated in the repurchase agreement are not met, Dealer at his sole discretion has the right to reduce the value of the car as per the violations from the conditions and the Customer shall have no right or remedy against the Dealer for such action but has the option to sell the car in open market if he is getting a better value for his car and needs to pay the balance outstanding amount in time to Volkswagen Finance to close the loan.</p>
        </div>
      </div>
                <div class="col-md-12 q-box"> <a class="option-heading">
        <p>When does the customer surrender his vehicle to the dealer assuming he wants to return the vehicle at the end of the tenure?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>Customer to surrender( if option to surrender is exercised) his/her vehicle 15 days prior to end of the Buy Back Period once the final surrender value has been agreed and confirmed with Dealer.</p>
        </div>
      </div>
                  <div class="col-md-12 q-box"> <a class="option-heading">
        <p>What is the Minimum loan to value?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>The Minimum loan to value of the car in this product is 70% of net Ex-showroom price of the car.</p>
        </div>
      </div>
                 <div class="col-md-12 q-box"> <a class="option-heading">
        <p>Are fleet cars eligible for Volkswagen Secure?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>No, As per VWFS credit policy we don’t finance any kind of fleet cars.</p>
        </div>
      </div>
                  <div class="col-md-12 q-box"> <a class="option-heading">
        <p>Can the car be returned to different dealer/location other than selling dealer?<span class="arrow-up"><i class="fa fa-chevron-up" aria-hidden="true"></i></span><span class="arrow-down"><i class="fa fa-chevron-down" aria-hidden="true"></i></span></p>
        </a>
        <div  class="option-content">
          <p>As of now this facility is restricted to Kochi dealer only but after pan India launch customer can return the car at end of the tenor to  any VW dealer in order to provide homogenous service levels pan India.</p>
        </div>
      </div>
                 
      <div class="col-md-12 load_more_btn"><a href="#"  id="loadMore">View more <i class="fa fa-chevron-down" aria-hidden="true"></i></a></div>
    </div>
  </div>
</section>

<hr class="mrg-0">
<section class="welcome-section sec-pad"  >
  <div class="container">
    <div class="row">
      <div class="col-md-12" style="text-align:center"> <a href="#contact">
        <button class="thm-btnas contact_btn">Contact Us</button>
        </a> </div>
    </div>
  </div>
</section>
<footer class="bottom-footer">
  <div class="container">
    <div class="copy-text pull-left">
      <p>© 2017-18. Volkswagen  Financial Services. All rights reserved. Designed & Developed by<a href="http://collateral.co.in/"  target="_blank"> Collateral.</a><br>
        *Terms and Conditions apply. Under the joint trade name “Volkswagen Financial Services” the subsidiaries of Volkswagen Financial Services AG provides the aforesaid services. Financial Services are offered by Volkswagen Finance Private Limited. Leasing, Insurance & Mobility Services are offered by Volkswagen Finance Private Limited in cooperation with third parties. Offer applicable on select variants and models. The scheme cannot be clubbed with any other offer. Calculations shown are approximate and for budgetary purposes only. Prices are subject to vary for different models, cities, etc. The ex-showroom price and standard EMI mentioned are for representative purposes only. Offer valid for limited period and liable to change without prior intimation. Finance at sole discretion of Volkswagen Finance Private Limited. Assured Future Value is solely governed by the terms and conditions of wear & tear shared while signing of the contract. Full terms and conditions are available upon application. Available only to approved applicants of Volkswagen  Financial Services. Subject to credit assessment. </p>
      <div class="portlet ui-sortable" id="yw0">
        <div class="portlet-content">
         <ul>
           <li><a href="https://www.facebook.com/vwfsIndia/" target="_blank"><i class="fa fa-facebook"></i></a></li>
           <li><a  href="https://twitter.com/vwfsIndia" target="_blank"><i class="fa fa-twitter"></i></a></li>
           <li><a href="https://www.linkedin.com/company/volkswagen-finance-private-limited" target="_blank"><i class="fa fa-linkedin"></i></a></li>
           <li><a  href="https://www.youtube.com/channel/UCZKlxC2JvabZBzoqhfSBNfQ" target="_blank"><i class="fa fa-youtube"></i></a></li>
         </ul>  
        </div>
      </div>
    </div>
  </div>
</footer>
<script src="<?php echo S3_URL?>/site/wolkswagen-assets/js/jquery.min.js"></script> 
<script src="<?php echo S3_URL?>/site/wolkswagen-assets/js/bootstrap.min.js"></script> 
<script src="<?php echo S3_URL?>/site/wolkswagen-assets/js/bootstrap-select.min.js"></script> 
<script src="<?php echo S3_URL?>/site/wolkswagen-assets/js/slideshow.js" type="text/javascript"></script> 
<script src="<?php echo S3_URL?>/site/wolkswagen-assets/js/owl.carousel.min.js" type="text/javascript"></script> 
<!-- <script src="<?php echo S3_URL?>/site/wolkswagen-assets/js/parsley-min.js" type="text/javascript"></script> -->
<script>
	$(document).ready(function() {
		$(".option-content").hide();
		$(".arrow-up").hide();
		$(".option-heading").click(function(){
				$(this).next(".option-content").slideToggle(500);
				$(this).find(".arrow-up, .arrow-down").toggle();
		});
	});
$(function () {
    $(".accordian-style .q-box").slice(0, 4).show();
    $("#loadMore").on('click', function (e) {
        e.preventDefault();
        $(".accordian-style .q-box:hidden").slice(0, 4).slideDown();
        if ($(".accordian-style .q-box:hidden").length == 0) {
            $("#load").fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
});

$('a[href=#top]').click(function () {
    $('body,html').animate({
        scrollTop: 0
    }, 600);
    return false;
});

$(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        $('.totop a').fadeIn();
    } else {
        $('.totop a').fadeOut();
    }
});
</script>

  <script type="text/javascript" src="<?php echo S3_URL?>/site/wolkswagen-assets/js/jquery.basictable.min"></script>

  <script type="text/javascript">
    $(document).ready(function() {  
      $('.table-breakpoint').basictable({
        breakpoint: 767
      }); 
       
    });
  </script>

<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->


</body>
</html>

<script src="<?php echo S3_URL?>/site/scripts/wolkswagen.js"></script>
<script type="text/javascript">
  /*
$(document).ready(function() {

    // process the form
    $('form').submit(function(event) {
        // get the form data
        // there are many ways to get this data using jQuery (you can use the class or id also)
        var subject = $('input[name=txtAudiChoiceSubject]').val();
        var name = $('input[name=textAudiChoicename]').val();
        var email = $('input[name=textAudiChoiceemail]').val();
        var mobile = $('input[name=textAudiChoicemobile]').val();
        var dealer_city = $('select[name=textAudiChoicecity]').val();
        var model = $('select[name=textAudiChoicemodel]').val();

        // process the form
        $.ajax({
            type        : 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url         : 'get_response.php', // the url where we want to POST
            data        : {'txtAudiChoiceSubject':subject,'textAudiChoicename':name,'textEmail':email,'textAudiChoicemobile':mobile,'textAudiChoicecity':dealer_city,'textAudiChoicemodel':model}, // our data object
            dataType    : 'json', // what type of data do we expect back from the server
            success:function(resp)
            { 
              if (resp.status==1) {
                $('input[name=txtAudiChoiceSubject]').val('');
                $('input[name=textAudiChoicename]').val('');
                $('input[name=textAudiChoiceemail]').val('');
                $('input[name=textAudiChoicemobile]').val('');
                $('select[name=textAudiChoicecity]').val('');
                $('select[name=textAudiChoicemodel]').val('');  
                
                $('#frmSendAudiChoiceFeedback').hide();  
                // $('#thankyoubox').show();  
                window.location.href="thankyou-VM-secure.html";
              }else{
                alert(resp.msg);
              }
              // console.log(resp);
            }
        });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });

});  
*/
</script>