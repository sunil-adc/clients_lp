<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>SRL Diagnostics Home Collect Service - Blood & Urine Sample Collection</title>
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/srl-images/favicon.ico" type="image/x-icon" />

      <!-- Bootstrap -->
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/srl-assets/css/vendor.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/srl-assets/css/main.css">    
      <!-- online-fonts -->
      <link href="//fonts.googleapis.com/css?family=Convergence" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=ABeeZee:400,400i" rel="stylesheet">
      <!-- //online-fonts -->

   </head>
   <body>
      <div class="Main-agile">
        <!-- banner -->
        <div class="banner">
        
          <div class="container-fluid">
           
            <div class="banner-content-block">
            <div class="col-sm-8">
               <div class="baner-contents">
               <!-- <h1 class="title">BODY CHECKUP</h1> -->
                <h3 class="description">Hygienic collection of blood and urine samples from the comfort of your home.</h3>
                <h1 class="title text-center">PACKAGE STARTING FROM ₹999/-</h1>
                  <div class="package-list">
                      <a href="#fittness" class="package-btn smoothScroll">FITNESS CARE <i class="fa fa-arrow-down blink"></i></a>
                      <a href="#evergreen" class="package-btn smoothScroll">EVERGREEN CARE <i class="fa fa-arrow-down blink"></i></a>
                      <a href="#completecare" class="package-btn smoothScroll">COMPLETE CARE <i class="fa fa-arrow-down blink"></i></a>
                      <a href="#activecare" class="package-btn smoothScroll">ACTIVE CARE <i class="fa fa-arrow-down blink"></i></a>
                      <a href="#vitalcare" class="package-btn smoothScroll">VITAL CARE <i class="fa fa-arrow-down blink"></i></a>
                  </div>
               </div>      
            </div>  
            <div class="col-sm-3 app-w3">
            <div class="logo">
                <a href="index.html">
                <img src="<?php echo S3_URL?>/site/srl-images/logo.jpg" class="logo-img"/>
                </a>
            </div>
            <h4>Book Your Appointment Now</h4>
            <div class="">
            <form role="form" id="feedbackForm" class="srl_form" action="JavaScript:void(0)" onsubmit="srl_jsfrm('<?php echo SITE_URL?>srlworld/srlfrm')">
              <div class="form-group">
                <div class="input-group wow fadeInUp animated" data-wow-delay=".5s">
                  <span class="input-group-addon" id="basic-addon1">
                    <i class="fa fa-user" aria-hidden="true"></i>
                </span>
                  <input type="text" class="form-control" id="name" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                </div>                   
                  <span class="help-block" id="name_err"></span>
              </div>

              <div class="form-group">
                <div class="input-group wow fadeInUp animated" data-wow-delay=".5s">
                  <span class="input-group-addon" id="basic-addon1">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                </span>
                  <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                </div>                                
                <span class="help-block" id="email_err" ></span>
              </div>

              <div class="form-group">
                <div class="input-group wow fadeInUp animated" data-wow-delay=".5s">
                  <span class="input-group-addon" id="basic-addon1">
                    <i class="fa fa-phone" aria-hidden="true"></i>
                </span>
                <input type="text" class="form-control only_numeric" id="phone" name="phone" pattern="\d*" maxlength="10"placeholder="Phone Number" onkeyup="chck_valid('phone', 'Please enter correct phone')" data-attr="Please enter correct phone number"> 
                 </div> 
                  
                  <span class="help-block" id="phone_err"> </span>
              </div>
                             
              <div class="form-group">
                <div class="input-group wow fadeInUp animated" data-wow-delay=".5s">
                  <span class="input-group-addon" id="basic-addon1">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                  </span>
                  <select class="form-control selectSearch" id="city" name="city" onchange="chck_valid('city', 'Please select correct city')" 
                      data-attr="Please select city">
                      <option value="" >Select city</option>
                      <option value="Delhi" >Delhi</option>
                      <option value="Mumbai" >Mumbai</option>
                      <option value="Pune" >Pune</option>
                      <option value="Chandigarh" >Chandigarh</option>
                      <option value="Gurgaon" >Gurgaon</option>
                      <option value="Faridabad" >Faridabad</option>
                      <option value="Noida" >Noida</option>
                      <option value="Lucknow" >Lucknow</option>
                      <option value="Bangalore" >Bangalore</option>
                      <option value="Chennai" >Chennai</option>
                      <option value="Hyderabad" >Hyderabad</option>
                      <option value="Kolkata" >Kolkata</option>
                      <option value="other" >Other</option>
                </select>
                 </div> 
                
                <span class="help-block" id="city_err"></span>
            </div>

            <div class="form-group"> 
            <div class="input-group wow fadeInUp animated" data-wow-delay=".5s">
                  <span class="input-group-addon" id="basic-addon1">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                </span>
                   <input type="text" class="form-control datepicker" id="get_date" name="get_date" placeholder="Appointment Date" onkeyup="chck_valid('get_date', 'Please select Date slot')" data-attr="Please select Date slot">
                 </div>                                 
             
                <span class="help-block" id="get_date_err"></span>
            </div>

            <div class="form-group">
            <div class="input-group wow fadeInUp animated" data-wow-delay=".5s">
                  <span class="input-group-addon" id="basic-addon1">
                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                </span>
                <select class="form-control selectSearch" id="srl_time" name="srl_time" onchange="chck_valid('srl_time', 'Please select time slot')" data-attr="Please select time slot">
                  <option value="">Select Time Slot</option>
                  <?php foreach($all_array['SRL_TIME'] as $k=>$l){ 
                  
                        echo "<option value='".$k."'>".$l."</option>";
                    } ?>
                </select>
                 </div>  
               
                <span class="help-block" id="srl_time_err"></span>
            </div>

            <div class="form-group">
            <div class="input-group wow fadeInUp animated" data-wow-delay=".5s">
                  <span class="input-group-addon" id="basic-addon1">
                    <i class="fa fa-medkit" aria-hidden="true"></i>
                </span>
                <select class="form-control selectSearch" name="srl_test" id="srl_test" onchange="chck_valid('srl_test', 'Please select one Test')" data-attr="Please select one Test"> 
                  <option value="">Select Test</option>
                    <?php foreach($all_array['SRL_TEST'] as $k=>$l){ 
                  
                        echo "<option value='".$k."'>".$l[0]."</option>";
                    } ?>
                </select>
                 </div>  
                
                <span class="help-block" id="srl_test_err"></span>
            </div>
                           
							  
            <input type="hidden" id="pub" name="pub" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : "d_pb"); ?>">
            <input type="hidden" id="utm" name="utm" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "d_ut"); ?>">
							  
            <input type="submit" id="frm-sbmtbtn" class="btn btn-primary btn-lg" value="SUBMIT" onclick=" window.dotq = window.dotq || []; window.dotq.push( { 'projectId': ‘10000’, 'properties': {  'pixelId': '10033092', 'qstrings': { 'et': 'custom', 'el': 'lead', 'ea': 'submit' } } } ); ">
							  
                           </form>
            </div>
          </div>
        </div>  
          </div>
 
        </div>
      </div>
      <!-- //banner -->
      <div class="container service"> 
    <div class="row">
        <div class="col-md-4 col-sm-4">
            <div class="serviceBox">
                <div class="service-icon">
                <img class="img-circle" src="<?php echo S3_URL?>/site/srl-assets/images/appoinment.png" data-holder-rendered="true">
                    <span class="number">1</span>
                </div>
                <h3 class="title"> Arrange an Appointment</h3>
               
            </div>
        </div>
 
        <div class="col-md-4 col-sm-4">
            <div class="serviceBox">
                <div class="service-icon">
                <img class="img-circle"   src="<?php echo S3_URL?>/site/srl-assets/images/home.png" data-holder-rendered="true">
                    <span class="number">2</span>
                </div>
                <h3 class="title">Sample collection from your home</h3>             
            </div>
        </div>

        <div class="col-md-4 col-sm-4">
            <div class="serviceBox">
                <div class="service-icon">
                   <img class="img-circle"  src="<?php echo S3_URL?>/site/srl-assets/images/dowmload.png" data-holder-rendered="true">
                    <span class="number">3</span>
                </div>
                <h3 class="title">Download report online</h3>             
            </div>
        </div>
    </div>
</div>
</div>
<!-- Packages 1 -->
<div class="jarallax team fittness" id="fittness">
		<div class="container">
			<div class="team-heading">
        <h3>Fitness Care <span></span></h3>
        <p>Ideal for those who want to improve their workout performance & endurance</p>
			</div>
			<div class="agileits-team-grids">
      <div class="row">
        <div class="accordian-conainer">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#fitness1" aria-expanded="true" aria-controls="collapseOne">
                            <span> Basic (46 TESTS)</span> <span class="price">₹1399</span>
                            </a>
                        </h4>
                    </div>
                    <div id="fitness1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <p class="description">
                            IDEAL FOR Ideal for people who are embarking on a new fitness regime
                            </p>
                            <p class="description">
                            Embarking on a new fitness program? Understanding key biomarkers in your blood with SRL Fitness Care- Basic test can help make sure you're in good shape before you start.
                            This package can give you vital information about your current health and fitness
                            </p>
                            <div class="table-responsive">
          <table class="table table-bordered">

              <tbody><tr class="">
                <td>General Health</td>
                <td>CBC, Urinalysis</td>
              </tr>
              <tr>
                <td>Diabetes</td>
                <td>Glucose (F)</td>
              </tr>
              <tr>
                <td>Thyriod</td>
                <td>TSH</td>
              </tr>
              <tr>
                <td>Lipid</td>
                <td>Triglycerides, Cholesterol Total, HDL, LDL, VLDL, LDL/HDL Ratio, Cholesterol Total / HDL Ratio</td>
              </tr>
              <tr>
                <td>Kidney</td>
                <td>Uric Acid, BUN, Creatinine </td>
              </tr>
              
              <tr>
                <td>Liver</td>
                <td>SGOT, SGPT, Bilirubin Total </td>
              </tr>            
          </tbody></table>
        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#fitness2" aria-expanded="false" aria-controls="collapseTwo">
                            <span> Advance (59 TESTS)</span> <span class="price">₹1999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="fitness2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                        <p class="description">
                        IDEAL FOR Ideal for People who want to track their & measure their improved body stats
                            </p>
                            <p class="description">
                            This comprehensive package is designed to help you track the result of your fitness regime over time. We recommend repeating the advance Check 2 times a year to understand your baseline (or normal) marker range, as well as how your markers change throughout your training
                            </p>
                            <div class="table-responsive" style="padding:0; width: 100%">
          <table class="table table-bordered">

              <tbody><!-- ngIf: subTestDetail[0].general_health != '' --><tr ng-if="subTestDetail[0].general_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">General Health</td>
                <td ng-bind="subTestDetail[0].general_health" class="ng-binding">CBC, urinalysis</td>
              </tr><!-- end ngIf: subTestDetail[0].general_health != '' -->
              <!-- ngIf: subTestDetail[0].diabetes != '' --><tr ng-if="subTestDetail[0].diabetes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Diabetes</td>
                <td class="ng-binding">Glucose(F)</td>
              </tr><!-- end ngIf: subTestDetail[0].diabetes != '' -->
              <!-- ngIf: subTestDetail[0].thyroid != '' --><tr ng-if="subTestDetail[0].thyroid != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Thyriod</td>
                <td class="ng-binding">TSH</td>
              </tr><!-- end ngIf: subTestDetail[0].thyroid != '' -->
              <!-- ngIf: subTestDetail[0].lipid_profile != '' --><tr ng-if="subTestDetail[0].lipid_profile != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Lipid</td>
                <td class="ng-binding">Triglycerides,Cholesterol Total,HDL,VLDL,LDL/HDL Ratio,Cholesterol Total/HDL Ratio</td>
              </tr><!-- end ngIf: subTestDetail[0].lipid_profile != '' -->
              <!-- ngIf: subTestDetail[0].kidney != '' --><tr ng-if="subTestDetail[0].kidney != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Kidney</td>
                <td class="ng-binding">Uric Acid,BUN,Creatinine, BUN/Creatinine Ratio, Total Protein, Albumin, Globulin</td>
              </tr><!-- end ngIf: subTestDetail[0].kidney != '' -->
              <!-- ngIf: subTestDetail[0].bone != '' --><tr ng-if="subTestDetail[0].bone != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Bone</td>
                <td class="ng-binding">Vitamin D, Calcium,Phosphorous</td>
              </tr><!-- end ngIf: subTestDetail[0].bone != '' -->
              <!-- ngIf: subTestDetail[0].liver != '' --><tr ng-if="subTestDetail[0].liver != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Liver</td>
                <td class="ng-binding">SGOT,SGPT,Bilirubin Total</td>
              </tr><!-- end ngIf: subTestDetail[0].liver != '' -->
              <!-- ngIf: subTestDetail[0].stress != '' -->
              <!-- ngIf: subTestDetail[0].fatigue != '' --><tr ng-if="subTestDetail[0].fatigue != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Fatigue</td>
                <td class="ng-binding">Iron,TIBC,%Saturation</td>
              </tr><!-- end ngIf: subTestDetail[0].fatigue != '' -->
              <!-- ngIf: subTestDetail[0].pancreas != '' -->
              <!-- ngIf: subTestDetail[0].heart != '' -->
              <!-- ngIf: subTestDetail[0].muscle != '' --><tr ng-if="subTestDetail[0].muscle != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Muscle</td>
                <td class="ng-binding">Sodium, Potassium, Chloride</td>
              </tr><!-- end ngIf: subTestDetail[0].muscle != '' -->
              <!-- ngIf: subTestDetail[0].performance_hormone != '' -->
              <!-- ngIf: subTestDetail[0].allergy != '' --> 
              <!-- ngIf: subTestDetail[0].cancer_screening != '' -->
               <!-- ngIf: subTestDetail[0].electrolytes != '' -->
               <!-- ngIf: subTestDetail[0].nerve_health != '' -->
              <!-- ngIf: subTestDetail[0].anemia != '' -->

          </tbody></table>
        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#fitness3" aria-expanded="false" aria-controls="collapseThree">
                            <span> Performance (48 TESTS)</span> <span class="price">₹2999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="fitness3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                        <p class="description">
                        IDEAL FOR Ideal for people who want to improve their workout performance and endurance
                            </p>
                            <p class="description">
                            This package includes exclusive tests that can monitor key biomarkers in blood, to get information that can improve motivation, performance and recovery. Optimise your performance by analysing and fine tuning these key biomarkers
                            </p>
                            <div class="table-responsive" style="padding:0; width: 100%">
          <table class="table table-bordered">

              <tbody><!-- ngIf: subTestDetail[0].general_health != '' --><tr ng-if="subTestDetail[0].general_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">General Health</td>
                <td ng-bind="subTestDetail[0].general_health" class="ng-binding">Urinalysis</td>
              </tr><!-- end ngIf: subTestDetail[0].general_health != '' -->
              <!-- ngIf: subTestDetail[0].diabetes != '' -->
              <!-- ngIf: subTestDetail[0].thyroid != '' -->
              <!-- ngIf: subTestDetail[0].lipid_profile != '' --><tr ng-if="subTestDetail[0].lipid_profile != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Lipid</td>
                <td class="ng-binding">Triglycerides, Cholesterol Total, HDL, LDL, VLDL, LDL/HDL Ratio, Cholesterol Total / HDL Ratio</td>
              </tr><!-- end ngIf: subTestDetail[0].lipid_profile != '' -->
              <!-- ngIf: subTestDetail[0].kidney != '' --><tr ng-if="subTestDetail[0].kidney != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Kidney</td>
                <td class="ng-binding">Uric Acid, BUN, Creatinine, BUN/Creatinine Ratio, Total Protein</td>
              </tr><!-- end ngIf: subTestDetail[0].kidney != '' -->
              <!-- ngIf: subTestDetail[0].bone != '' --><tr ng-if="subTestDetail[0].bone != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Bone</td>
                <td class="ng-binding">Vitamin D, Calcium</td>
              </tr><!-- end ngIf: subTestDetail[0].bone != '' -->
              <!-- ngIf: subTestDetail[0].liver != '' --><tr ng-if="subTestDetail[0].liver != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Liver</td>
                <td class="ng-binding">SGOT, SGPT, Bilirubin Total &amp; Direct, Alkaline Phosphatase, LDH, albumin, Globulin, A:G Ratio, Serum Protein</td>
              </tr><!-- end ngIf: subTestDetail[0].liver != '' -->
              <!-- ngIf: subTestDetail[0].stress != '' -->
              <!-- ngIf: subTestDetail[0].fatigue != '' -->
              <!-- ngIf: subTestDetail[0].pancreas != '' -->
              <!-- ngIf: subTestDetail[0].heart != '' --><tr ng-if="subTestDetail[0].heart != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Heart</td>
                <td class="ng-binding">Apo lipoprotein - B</td>
              </tr><!-- end ngIf: subTestDetail[0].heart != '' -->
              <!-- ngIf: subTestDetail[0].muscle != '' -->
              <!-- ngIf: subTestDetail[0].performance_hormone != '' --><tr ng-if="subTestDetail[0].performance_hormone != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Performance Hormone</td>
                <td class="ng-binding">DHEAS,Testosterone</td>
              </tr><!-- end ngIf: subTestDetail[0].performance_hormone != '' -->
              <!-- ngIf: subTestDetail[0].allergy != '' --> 
              <!-- ngIf: subTestDetail[0].cancer_screening != '' -->
               <!-- ngIf: subTestDetail[0].electrolytes != '' -->
               <!-- ngIf: subTestDetail[0].nerve_health != '' -->
              <!-- ngIf: subTestDetail[0].anemia != '' --><tr ng-if="subTestDetail[0].anemia != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Anemia</td>
                <td class="ng-binding">Vitamin B12, TIBC, Iron, % Saturation</td>
              </tr><!-- end ngIf: subTestDetail[0].anemia != '' -->

          </tbody></table>
        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
			</div>
		</div>
	</div>
<!-- //Packages -->
<!-- Packages 2 -->
<div class="jarallax team evergreen" id="evergreen">
		<div class="container">
			<div class="team-heading">
        <h3>Evergreen Care <span></span></h3>
        <p>Ideal for those above 40 years of age who might at the risk of various chronic diseases</p>
			</div>
			<div class="agileits-team-grids">
      <div class="row">
        <div class="accordian-conainer">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#evergreen1" aria-expanded="true" aria-controls="collapseOne">
                            <span> 60+ Male (56 TESTS)</span> <span class="price">₹1999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="evergreen1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                        <p class="description">
                        IDEAL FOR Ideal for men above the age of 60 years, who want to get a comprehensive health check up done
                            </p>
                            <div class="table-responsive" style="padding:0; width: 100%">
          <table class="table table-bordered">

              <tbody><!-- ngIf: subTestDetail[0].general_health != '' --><tr ng-if="subTestDetail[0].general_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">General Health</td>
                <td ng-bind="subTestDetail[0].general_health" class="ng-binding">CBC, Urinalysis</td>
              </tr><!-- end ngIf: subTestDetail[0].general_health != '' -->
              <!-- ngIf: subTestDetail[0].diabetes != '' --><tr ng-if="subTestDetail[0].diabetes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Diabetes</td>
                <td class="ng-binding">Glucose (F)</td>
              </tr><!-- end ngIf: subTestDetail[0].diabetes != '' -->
              <!-- ngIf: subTestDetail[0].thyroid != '' --><tr ng-if="subTestDetail[0].thyroid != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Thyriod</td>
                <td class="ng-binding">TSH</td>
              </tr><!-- end ngIf: subTestDetail[0].thyroid != '' -->
              <!-- ngIf: subTestDetail[0].lipid_profile != '' --><tr ng-if="subTestDetail[0].lipid_profile != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Lipid</td>
                <td class="ng-binding">Triglycerides, Cholesterol Total, HDL, LDL, VLDL, LDL/HDL Ratio, Cholesterol Total / HDL Ratio</td>
              </tr><!-- end ngIf: subTestDetail[0].lipid_profile != '' -->
              <!-- ngIf: subTestDetail[0].kidney != '' --><tr ng-if="subTestDetail[0].kidney != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Kidney</td>
                <td class="ng-binding">Uric Acid, BUN, Creatinine, BUN/Creatinine Ratio, Total Protein, 
Albumin, Globulin</td>
              </tr><!-- end ngIf: subTestDetail[0].kidney != '' -->
              <!-- ngIf: subTestDetail[0].bone != '' --><tr ng-if="subTestDetail[0].bone != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Bone</td>
                <td class="ng-binding">Calcium, Phosphorous</td>
              </tr><!-- end ngIf: subTestDetail[0].bone != '' -->
              <!-- ngIf: subTestDetail[0].liver != '' --><tr ng-if="subTestDetail[0].liver != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Liver</td>
                <td class="ng-binding">SGOT, SGPT, Bilirubin Total</td>
              </tr><!-- end ngIf: subTestDetail[0].liver != '' -->
              <!-- ngIf: subTestDetail[0].stress != '' -->
              <!-- ngIf: subTestDetail[0].fatigue != '' -->
              <!-- ngIf: subTestDetail[0].pancreas != '' -->
              <!-- ngIf: subTestDetail[0].heart != '' -->
              <!-- ngIf: subTestDetail[0].muscle != '' --><tr ng-if="subTestDetail[0].muscle != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Muscle</td>
                <td class="ng-binding">Sodium, Potassium, Chloride</td>
              </tr><!-- end ngIf: subTestDetail[0].muscle != '' -->
              <!-- ngIf: subTestDetail[0].performance_hormone != '' -->
              <!-- ngIf: subTestDetail[0].allergy != '' --> 
              <!-- ngIf: subTestDetail[0].cancer_screening != '' --><tr ng-if="subTestDetail[0].cancer_screening != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Cancer Screening</td>
                <td class="ng-binding">PSA (Prostate Specific Antigen</td>
              </tr><!-- end ngIf: subTestDetail[0].cancer_screening != '' -->
               <!-- ngIf: subTestDetail[0].electrolytes != '' -->
               <!-- ngIf: subTestDetail[0].nerve_health != '' -->
              <!-- ngIf: subTestDetail[0].anemia != '' -->

          </tbody></table>
        </div>                    
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#evergreen2" aria-expanded="false" aria-controls="collapseTwo">
                            <span> 60+ Female (57 TESTS)</span> <span class="price">₹1999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="evergreen2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                        <p class="description">
                        IDEAL FOR Ideal for men above the age of 60 years, who want to get a comprehensive health check up done
                            </p>
                            <div class="table-responsive" style="padding:0; width: 100%">
          <table class="table table-bordered">

              <tbody><!-- ngIf: subTestDetail[0].general_health != '' --><tr ng-if="subTestDetail[0].general_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">General Health</td>
                <td ng-bind="subTestDetail[0].general_health" class="ng-binding">CBC, Urinalysis</td>
              </tr><!-- end ngIf: subTestDetail[0].general_health != '' -->
              <!-- ngIf: subTestDetail[0].diabetes != '' --><tr ng-if="subTestDetail[0].diabetes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Diabetes</td>
                <td class="ng-binding">Glucose (F)</td>
              </tr><!-- end ngIf: subTestDetail[0].diabetes != '' -->
              <!-- ngIf: subTestDetail[0].thyroid != '' --><tr ng-if="subTestDetail[0].thyroid != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Thyriod</td>
                <td class="ng-binding">TSH</td>
              </tr><!-- end ngIf: subTestDetail[0].thyroid != '' -->
              <!-- ngIf: subTestDetail[0].lipid_profile != '' --><tr ng-if="subTestDetail[0].lipid_profile != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Lipid</td>
                <td class="ng-binding">Triglycerides, Cholesterol Total, HDL, LDL, VLDL, LDL/HDL Ratio, Cholesterol Total / HDL Ratio</td>
              </tr><!-- end ngIf: subTestDetail[0].lipid_profile != '' -->
              <!-- ngIf: subTestDetail[0].kidney != '' --><tr ng-if="subTestDetail[0].kidney != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Kidney</td>
                <td class="ng-binding">Uric Acid, BUN, Creatinine, BUN/Creatinine Ratio, Total Protein, Albumin, Globulin</td>
              </tr><!-- end ngIf: subTestDetail[0].kidney != '' -->
              <!-- ngIf: subTestDetail[0].bone != '' --><tr ng-if="subTestDetail[0].bone != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Bone</td>
                <td class="ng-binding">Calcium, Phosphorous, Alkaline Phosphatase</td>
              </tr><!-- end ngIf: subTestDetail[0].bone != '' -->
              <!-- ngIf: subTestDetail[0].liver != '' --><tr ng-if="subTestDetail[0].liver != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Liver</td>
                <td class="ng-binding">SGOT, SGPT, Bilirubin Total</td>
              </tr><!-- end ngIf: subTestDetail[0].liver != '' -->
              <!-- ngIf: subTestDetail[0].stress != '' -->
              <!-- ngIf: subTestDetail[0].fatigue != '' -->
              <!-- ngIf: subTestDetail[0].pancreas != '' -->
              <!-- ngIf: subTestDetail[0].heart != '' -->
              <!-- ngIf: subTestDetail[0].muscle != '' -->
              <!-- ngIf: subTestDetail[0].performance_hormone != '' -->
              <!-- ngIf: subTestDetail[0].allergy != '' --> 
              <!-- ngIf: subTestDetail[0].cancer_screening != '' -->
               <!-- ngIf: subTestDetail[0].electrolytes != '' --><tr ng-if="subTestDetail[0].electrolytes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Electrolytes</td>
                <td class="ng-binding">Sodium, Potassium, Chloride</td>
              </tr><!-- end ngIf: subTestDetail[0].electrolytes != '' -->
               <!-- ngIf: subTestDetail[0].nerve_health != '' --><tr ng-if="subTestDetail[0].nerve_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Nerve Health</td>
                <td class="ng-binding">Vitamin B12</td>
              </tr><!-- end ngIf: subTestDetail[0].nerve_health != '' -->
              <!-- ngIf: subTestDetail[0].anemia != '' -->

          </tbody></table>
        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#evergreen3" aria-expanded="false" aria-controls="collapseThree">
                            <span> 50+ Male (54 TESTS)</span> <span class="price">₹1999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="evergreen3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>IDEAL FOR Ideal for men between the ages 51- 60 years, who want to get a comprehensive health check up done</p>
                            <div class="table-responsive" style="padding:0; width: 100%">
          <table class="table table-bordered">

              <tbody><!-- ngIf: subTestDetail[0].general_health != '' --><tr ng-if="subTestDetail[0].general_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">General Health</td>
                <td ng-bind="subTestDetail[0].general_health" class="ng-binding">CBC, Urinalysis</td>
              </tr><!-- end ngIf: subTestDetail[0].general_health != '' -->
              <!-- ngIf: subTestDetail[0].diabetes != '' --><tr ng-if="subTestDetail[0].diabetes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Diabetes</td>
                <td class="ng-binding">HbA1c</td>
              </tr><!-- end ngIf: subTestDetail[0].diabetes != '' -->
              <!-- ngIf: subTestDetail[0].thyroid != '' -->
              <!-- ngIf: subTestDetail[0].lipid_profile != '' --><tr ng-if="subTestDetail[0].lipid_profile != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Lipid</td>
                <td class="ng-binding">Triglycerides, Cholesterol Total, HDL, 
LDL, VLDL, LDL/HDL Ratio, Cholesterol Total / HDL Ratio</td>
              </tr><!-- end ngIf: subTestDetail[0].lipid_profile != '' -->
              <!-- ngIf: subTestDetail[0].kidney != '' --><tr ng-if="subTestDetail[0].kidney != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Kidney</td>
                <td class="ng-binding">Uric Acid, BUN, Creatinine, BUN/Creatinine Ratio, Total Protein, 
Albumin, Globulin</td>
              </tr><!-- end ngIf: subTestDetail[0].kidney != '' -->
              <!-- ngIf: subTestDetail[0].bone != '' --><tr ng-if="subTestDetail[0].bone != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Bone</td>
                <td class="ng-binding">Vitamin D</td>
              </tr><!-- end ngIf: subTestDetail[0].bone != '' -->
              <!-- ngIf: subTestDetail[0].liver != '' --><tr ng-if="subTestDetail[0].liver != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Liver</td>
                <td class="ng-binding">SGOT, SGPT, Bilirubin Total</td>
              </tr><!-- end ngIf: subTestDetail[0].liver != '' -->
              <!-- ngIf: subTestDetail[0].stress != '' -->
              <!-- ngIf: subTestDetail[0].fatigue != '' -->
              <!-- ngIf: subTestDetail[0].pancreas != '' -->
              <!-- ngIf: subTestDetail[0].heart != '' --><tr ng-if="subTestDetail[0].heart != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Heart</td>
                <td class="ng-binding">hS-CRP</td>
              </tr><!-- end ngIf: subTestDetail[0].heart != '' -->
              <!-- ngIf: subTestDetail[0].muscle != '' --><tr ng-if="subTestDetail[0].muscle != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Muscle</td>
                <td class="ng-binding">Sodium, Potassium, Chloride</td>
              </tr><!-- end ngIf: subTestDetail[0].muscle != '' -->
              <!-- ngIf: subTestDetail[0].performance_hormone != '' -->
              <!-- ngIf: subTestDetail[0].allergy != '' --> 
              <!-- ngIf: subTestDetail[0].cancer_screening != '' -->
               <!-- ngIf: subTestDetail[0].electrolytes != '' -->
               <!-- ngIf: subTestDetail[0].nerve_health != '' -->
              <!-- ngIf: subTestDetail[0].anemia != '' -->

          </tbody></table>
        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#evergreen4" aria-expanded="false" aria-controls="collapseThree">
                            <span> 50+ Female (68 TESTS)</span> <span class="price">₹1999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="evergreen4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>IDEAL FOR Ideal for women between the ages 51- 60 years, who want to get a comprehensive health check up done</p>
                            <div class="table-responsive" style="padding:0; width: 100%">
          <table class="table table-bordered">

              <tbody><!-- ngIf: subTestDetail[0].general_health != '' --><tr ng-if="subTestDetail[0].general_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">General Health</td>
                <td ng-bind="subTestDetail[0].general_health" class="ng-binding">CBC, Urinalysis</td>
              </tr><!-- end ngIf: subTestDetail[0].general_health != '' -->
              <!-- ngIf: subTestDetail[0].diabetes != '' --><tr ng-if="subTestDetail[0].diabetes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Diabetes</td>
                <td class="ng-binding">HbA1c</td>
              </tr><!-- end ngIf: subTestDetail[0].diabetes != '' -->
              <!-- ngIf: subTestDetail[0].thyroid != '' -->
              <!-- ngIf: subTestDetail[0].lipid_profile != '' --><tr ng-if="subTestDetail[0].lipid_profile != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Lipid</td>
                <td class="ng-binding">Triglycerides, Cholesterol Total, HDL, 
LDL, VLDL, LDL/HDL Ratio, Cholesterol Total / HDL Ratio</td>
              </tr><!-- end ngIf: subTestDetail[0].lipid_profile != '' -->
              <!-- ngIf: subTestDetail[0].kidney != '' --><tr ng-if="subTestDetail[0].kidney != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Kidney</td>
                <td class="ng-binding">Uric Acid, BUN, Creatinine, BUN/Creatinine Ratio, Total Protein, 
Albumin, Globulin</td>
              </tr><!-- end ngIf: subTestDetail[0].kidney != '' -->
              <!-- ngIf: subTestDetail[0].bone != '' --><tr ng-if="subTestDetail[0].bone != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Bone</td>
                <td class="ng-binding">Vitamin D, Rheumatoid Factor, CRP</td>
              </tr><!-- end ngIf: subTestDetail[0].bone != '' -->
              <!-- ngIf: subTestDetail[0].liver != '' --><tr ng-if="subTestDetail[0].liver != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Liver</td>
                <td class="ng-binding">SGOT, SGPT, Bilirubin Total</td>
              </tr><!-- end ngIf: subTestDetail[0].liver != '' -->
              <!-- ngIf: subTestDetail[0].stress != '' -->
              <!-- ngIf: subTestDetail[0].fatigue != '' -->
              <!-- ngIf: subTestDetail[0].pancreas != '' -->
              <!-- ngIf: subTestDetail[0].heart != '' -->
              <!-- ngIf: subTestDetail[0].muscle != '' -->
              <!-- ngIf: subTestDetail[0].performance_hormone != '' -->
              <!-- ngIf: subTestDetail[0].allergy != '' --> 
              <!-- ngIf: subTestDetail[0].cancer_screening != '' -->
               <!-- ngIf: subTestDetail[0].electrolytes != '' --><tr ng-if="subTestDetail[0].electrolytes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Electrolytes</td>
                <td class="ng-binding">Sodium, Potassium, Chloride</td>
              </tr><!-- end ngIf: subTestDetail[0].electrolytes != '' -->
               <!-- ngIf: subTestDetail[0].nerve_health != '' -->
              <!-- ngIf: subTestDetail[0].anemia != '' -->

          </tbody></table>
        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#evergreen5" aria-expanded="false" aria-controls="collapseThree">
                            <span> 40+ Male (62 TESTS)</span> <span class="price">₹1999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="evergreen5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>IDEAL FOR Ideal for men between the ages 41 - 50 years, who want to get a comprehensive health check up done</p>
                            <div class="table-responsive" style="padding:0; width: 100%">
          <table class="table table-bordered">

              <tbody><!-- ngIf: subTestDetail[0].general_health != '' --><tr ng-if="subTestDetail[0].general_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">General Health</td>
                <td ng-bind="subTestDetail[0].general_health" class="ng-binding">CBC, Urinalysis</td>
              </tr><!-- end ngIf: subTestDetail[0].general_health != '' -->
              <!-- ngIf: subTestDetail[0].diabetes != '' --><tr ng-if="subTestDetail[0].diabetes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Diabetes</td>
                <td class="ng-binding">HbA1c</td>
              </tr><!-- end ngIf: subTestDetail[0].diabetes != '' -->
              <!-- ngIf: subTestDetail[0].thyroid != '' --><tr ng-if="subTestDetail[0].thyroid != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Thyriod</td>
                <td class="ng-binding">TSH</td>
              </tr><!-- end ngIf: subTestDetail[0].thyroid != '' -->
              <!-- ngIf: subTestDetail[0].lipid_profile != '' --><tr ng-if="subTestDetail[0].lipid_profile != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Lipid</td>
                <td class="ng-binding">Triglycerides, Cholesterol Total, HDL, LDL, VLDL, LDL/HDL Ratio, Cholesterol Total / HDL Ratio</td>
              </tr><!-- end ngIf: subTestDetail[0].lipid_profile != '' -->
              <!-- ngIf: subTestDetail[0].kidney != '' --><tr ng-if="subTestDetail[0].kidney != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Kidney</td>
                <td class="ng-binding">Uric Acid, BUN, Creatinine, BUN/Creatinine Ratio, Total Protein</td>
              </tr><!-- end ngIf: subTestDetail[0].kidney != '' -->
              <!-- ngIf: subTestDetail[0].bone != '' --><tr ng-if="subTestDetail[0].bone != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Bone</td>
                <td class="ng-binding">Vitamin D</td>
              </tr><!-- end ngIf: subTestDetail[0].bone != '' -->
              <!-- ngIf: subTestDetail[0].liver != '' --><tr ng-if="subTestDetail[0].liver != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Liver</td>
                <td class="ng-binding">SGOT, SGPT, Bilirubin Total &amp; Direct, 
Alkaline Phosphatase, LDH, Albumin, 
Globulin, A:G Ratio, Serum Protein, GG</td>
              </tr><!-- end ngIf: subTestDetail[0].liver != '' -->
              <!-- ngIf: subTestDetail[0].stress != '' -->
              <!-- ngIf: subTestDetail[0].fatigue != '' -->
              <!-- ngIf: subTestDetail[0].pancreas != '' -->
              <!-- ngIf: subTestDetail[0].heart != '' -->
              <!-- ngIf: subTestDetail[0].muscle != '' --><tr ng-if="subTestDetail[0].muscle != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Muscle</td>
                <td class="ng-binding">Sodium, Potassium, Chloride</td>
              </tr><!-- end ngIf: subTestDetail[0].muscle != '' -->
              <!-- ngIf: subTestDetail[0].performance_hormone != '' -->
              <!-- ngIf: subTestDetail[0].allergy != '' --> 
              <!-- ngIf: subTestDetail[0].cancer_screening != '' -->
               <!-- ngIf: subTestDetail[0].electrolytes != '' -->
               <!-- ngIf: subTestDetail[0].nerve_health != '' -->
              <!-- ngIf: subTestDetail[0].anemia != '' -->

          </tbody></table>
        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#evergreen6" aria-expanded="false" aria-controls="collapseThree">
                            <span> 40+ Female (60 TESTS)</span> <span class="price">₹1999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="evergreen6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>IDEAL FOR Ideal for women between the ages 41 - 50 years, who want to get a comprehensive health check up done</p>
                            <div class="table-responsive" style="padding:0; width: 100%">
          <table class="table table-bordered">

              <tbody><!-- ngIf: subTestDetail[0].general_health != '' --><tr ng-if="subTestDetail[0].general_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">General Health</td>
                <td ng-bind="subTestDetail[0].general_health" class="ng-binding">CBC, Urinalysis</td>
              </tr><!-- end ngIf: subTestDetail[0].general_health != '' -->
              <!-- ngIf: subTestDetail[0].diabetes != '' --><tr ng-if="subTestDetail[0].diabetes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Diabetes</td>
                <td class="ng-binding">HbA1c</td>
              </tr><!-- end ngIf: subTestDetail[0].diabetes != '' -->
              <!-- ngIf: subTestDetail[0].thyroid != '' --><tr ng-if="subTestDetail[0].thyroid != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Thyriod</td>
                <td class="ng-binding">TSH, T3, T4</td>
              </tr><!-- end ngIf: subTestDetail[0].thyroid != '' -->
              <!-- ngIf: subTestDetail[0].lipid_profile != '' --><tr ng-if="subTestDetail[0].lipid_profile != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Lipid</td>
                <td class="ng-binding">Triglycerides, Cholesterol Total, HDL, LDL, VLDL, LDL/HDL Ratio, Cholesterol Total / HDL Ratio</td>
              </tr><!-- end ngIf: subTestDetail[0].lipid_profile != '' -->
              <!-- ngIf: subTestDetail[0].kidney != '' --><tr ng-if="subTestDetail[0].kidney != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Kidney</td>
                <td class="ng-binding">Uric Acid, BUN, Creatinine, BUN/Creatinine Ratio, Total Protein, Albumin, Globulin</td>
              </tr><!-- end ngIf: subTestDetail[0].kidney != '' -->
              <!-- ngIf: subTestDetail[0].bone != '' --><tr ng-if="subTestDetail[0].bone != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Bone</td>
                <td class="ng-binding">Vitamin D</td>
              </tr><!-- end ngIf: subTestDetail[0].bone != '' -->
              <!-- ngIf: subTestDetail[0].liver != '' --><tr ng-if="subTestDetail[0].liver != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Liver</td>
                <td class="ng-binding">SGOT, SGPT, Bilirubin Total</td>
              </tr><!-- end ngIf: subTestDetail[0].liver != '' -->
              <!-- ngIf: subTestDetail[0].stress != '' -->
              <!-- ngIf: subTestDetail[0].fatigue != '' --><tr ng-if="subTestDetail[0].fatigue != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Fatigue</td>
                <td class="ng-binding">Iron, TIBC, % Saturation, Ferritin </td>
              </tr><!-- end ngIf: subTestDetail[0].fatigue != '' -->
              <!-- ngIf: subTestDetail[0].pancreas != '' -->
              <!-- ngIf: subTestDetail[0].heart != '' -->
              <!-- ngIf: subTestDetail[0].muscle != '' --><tr ng-if="subTestDetail[0].muscle != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Muscle</td>
                <td class="ng-binding">Sodium, Potassium, Chloride</td>
              </tr><!-- end ngIf: subTestDetail[0].muscle != '' -->
              <!-- ngIf: subTestDetail[0].performance_hormone != '' -->
              <!-- ngIf: subTestDetail[0].allergy != '' --> 
              <!-- ngIf: subTestDetail[0].cancer_screening != '' -->
               <!-- ngIf: subTestDetail[0].electrolytes != '' -->
               <!-- ngIf: subTestDetail[0].nerve_health != '' -->
              <!-- ngIf: subTestDetail[0].anemia != '' -->

          </tbody></table>
        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
			</div>
		</div>
	</div>
<!-- //Packages2 -->

<!-- Packages 3 -->
<div class="jarallax team completecare" id="completecare">
		<div class="container">
			<div class="team-heading">
        <h3>Complete Care<span></span></h3>
        <p>Ideal for people of all age groups who want a comprehensive health check</p>
			</div>
			<div class="agileits-team-grids">
      <div class="row">
        <div class="accordian-conainer">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#completecare1" aria-expanded="true" aria-controls="collapseOne">
                            <span>Basic (27 TESTS)</span> <span class="price">₹999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="completecare1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                           <p>IDEAL FOR Ideal for people who are getting their health check up done for the first time</p>
                           <p>Basic Screening package with Lipid, Thyroid, Liver,Kidney & Sugar tests</p>
                           <div class="table-responsive" style="padding:0; width: 100%">
          <table class="table table-bordered">

              <tbody><!-- ngIf: subTestDetail[0].general_health != '' --><tr ng-if="subTestDetail[0].general_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">General Health</td>
                <td ng-bind="subTestDetail[0].general_health" class="ng-binding">Hb, Urianlysis</td>
              </tr><!-- end ngIf: subTestDetail[0].general_health != '' -->
              <!-- ngIf: subTestDetail[0].diabetes != '' --><tr ng-if="subTestDetail[0].diabetes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Diabetes</td>
                <td class="ng-binding">Glucose (F)</td>
              </tr><!-- end ngIf: subTestDetail[0].diabetes != '' -->
              <!-- ngIf: subTestDetail[0].thyroid != '' --><tr ng-if="subTestDetail[0].thyroid != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Thyriod</td>
                <td class="ng-binding">TSH</td>
              </tr><!-- end ngIf: subTestDetail[0].thyroid != '' -->
              <!-- ngIf: subTestDetail[0].lipid_profile != '' --><tr ng-if="subTestDetail[0].lipid_profile != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Lipid</td>
                <td class="ng-binding">Triglycerides, Cholesterol Total</td>
              </tr><!-- end ngIf: subTestDetail[0].lipid_profile != '' -->
              <!-- ngIf: subTestDetail[0].kidney != '' --><tr ng-if="subTestDetail[0].kidney != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Kidney</td>
                <td class="ng-binding">Uric Acid, BUN, Creatinine</td>
              </tr><!-- end ngIf: subTestDetail[0].kidney != '' -->
              <!-- ngIf: subTestDetail[0].bone != '' -->
              <!-- ngIf: subTestDetail[0].liver != '' --><tr ng-if="subTestDetail[0].liver != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Liver</td>
                <td class="ng-binding">SGOT, SGPT, Bilirubin Total</td>
              </tr><!-- end ngIf: subTestDetail[0].liver != '' -->
              <!-- ngIf: subTestDetail[0].stress != '' -->
              <!-- ngIf: subTestDetail[0].fatigue != '' -->
              <!-- ngIf: subTestDetail[0].pancreas != '' -->
              <!-- ngIf: subTestDetail[0].heart != '' -->
              <!-- ngIf: subTestDetail[0].muscle != '' -->
              <!-- ngIf: subTestDetail[0].performance_hormone != '' -->
              <!-- ngIf: subTestDetail[0].allergy != '' --> 
              <!-- ngIf: subTestDetail[0].cancer_screening != '' -->
               <!-- ngIf: subTestDetail[0].electrolytes != '' -->
               <!-- ngIf: subTestDetail[0].nerve_health != '' -->
              <!-- ngIf: subTestDetail[0].anemia != '' -->

          </tbody></table>
        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#completecare2" aria-expanded="false" aria-controls="collapseTwo">
                            <span>Essential (46 TESTS)</span> <span class="price">₹1399</span>
                            </a>
                        </h4>
                    </div>
                    <div id="completecare2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <p>IDEAL FOR Ideal for people who are getting their health check up done for the first time with the advantage of advanced Lipid Testing</p>
                            <p>Essential Screening Package to check the basic functioning of Thyroid, Liver, Kidney along with Sugar & Advanced Lipid testing.</p>
                            <div class="table-responsive" style="padding:0; width: 100%">
          <table class="table table-bordered">

              <tbody><!-- ngIf: subTestDetail[0].general_health != '' --><tr ng-if="subTestDetail[0].general_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">General Health</td>
                <td ng-bind="subTestDetail[0].general_health" class="ng-binding">CBC,Urinalysis</td>
              </tr><!-- end ngIf: subTestDetail[0].general_health != '' -->
              <!-- ngIf: subTestDetail[0].diabetes != '' --><tr ng-if="subTestDetail[0].diabetes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Diabetes</td>
                <td class="ng-binding">Glucose(F)</td>
              </tr><!-- end ngIf: subTestDetail[0].diabetes != '' -->
              <!-- ngIf: subTestDetail[0].thyroid != '' --><tr ng-if="subTestDetail[0].thyroid != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Thyriod</td>
                <td class="ng-binding">TSH</td>
              </tr><!-- end ngIf: subTestDetail[0].thyroid != '' -->
              <!-- ngIf: subTestDetail[0].lipid_profile != '' --><tr ng-if="subTestDetail[0].lipid_profile != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Lipid</td>
                <td class="ng-binding">Triglycerides, Cholesterol Total, HDL, 
LDL, VLDL, LDL/HDL Ratio, 
Cholesterol Total / HDL Ratio</td>
              </tr><!-- end ngIf: subTestDetail[0].lipid_profile != '' -->
              <!-- ngIf: subTestDetail[0].kidney != '' --><tr ng-if="subTestDetail[0].kidney != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Kidney</td>
                <td class="ng-binding">Uric Acid,BUN,Creatinine</td>
              </tr><!-- end ngIf: subTestDetail[0].kidney != '' -->
              <!-- ngIf: subTestDetail[0].bone != '' -->
              <!-- ngIf: subTestDetail[0].liver != '' --><tr ng-if="subTestDetail[0].liver != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Liver</td>
                <td class="ng-binding">SGOT,SGPT,Bilirubin Total</td>
              </tr><!-- end ngIf: subTestDetail[0].liver != '' -->
              <!-- ngIf: subTestDetail[0].stress != '' -->
              <!-- ngIf: subTestDetail[0].fatigue != '' -->
              <!-- ngIf: subTestDetail[0].pancreas != '' -->
              <!-- ngIf: subTestDetail[0].heart != '' -->
              <!-- ngIf: subTestDetail[0].muscle != '' -->
              <!-- ngIf: subTestDetail[0].performance_hormone != '' -->
              <!-- ngIf: subTestDetail[0].allergy != '' --> 
              <!-- ngIf: subTestDetail[0].cancer_screening != '' -->
               <!-- ngIf: subTestDetail[0].electrolytes != '' -->
               <!-- ngIf: subTestDetail[0].nerve_health != '' -->
              <!-- ngIf: subTestDetail[0].anemia != '' -->

          </tbody></table>
        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#completecare3" aria-expanded="false" aria-controls="collapseThree">
                            <span>Advance (63 TESTS)</span> <span class="price">₹1999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="completecare3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>IDEAL FOR Ideal for people who lead a fast life and often complain of fatigue, tiredness and joint pain</p>
                            <p>This test is optimal for people falling in the age bracket of 22- 35 years, who are working professionals and lead a stressful life with little work and personal life balance. Such a scenario can take a toll on health and thus, it is essential that regular check-ups be done to ensure you are leading a healthy life</p>
                            <div class="table-responsive" style="padding:0; width: 100%">
          <table class="table table-bordered">

              <tbody><!-- ngIf: subTestDetail[0].general_health != '' --><tr ng-if="subTestDetail[0].general_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">General Health</td>
                <td ng-bind="subTestDetail[0].general_health" class="ng-binding">CBC,Urinalysis</td>
              </tr><!-- end ngIf: subTestDetail[0].general_health != '' -->
              <!-- ngIf: subTestDetail[0].diabetes != '' --><tr ng-if="subTestDetail[0].diabetes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Diabetes</td>
                <td class="ng-binding">Glucose(F)</td>
              </tr><!-- end ngIf: subTestDetail[0].diabetes != '' -->
              <!-- ngIf: subTestDetail[0].thyroid != '' --><tr ng-if="subTestDetail[0].thyroid != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Thyriod</td>
                <td class="ng-binding">TSH</td>
              </tr><!-- end ngIf: subTestDetail[0].thyroid != '' -->
              <!-- ngIf: subTestDetail[0].lipid_profile != '' --><tr ng-if="subTestDetail[0].lipid_profile != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Lipid</td>
                <td class="ng-binding">Triglycerides, Cholesterol Total, HDL, LDL, VLDL, LDL/HDL Ratio, 
Cholesterol Total / HDL Ratio</td>
              </tr><!-- end ngIf: subTestDetail[0].lipid_profile != '' -->
              <!-- ngIf: subTestDetail[0].kidney != '' --><tr ng-if="subTestDetail[0].kidney != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Kidney</td>
                <td class="ng-binding">Uric Acid,BUN,Creatinine, BUN/Creatinine Ratio, Total Protein</td>
              </tr><!-- end ngIf: subTestDetail[0].kidney != '' -->
              <!-- ngIf: subTestDetail[0].bone != '' --><tr ng-if="subTestDetail[0].bone != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Bone</td>
                <td class="ng-binding">Vitamin D, Rheumatoid Factor</td>
              </tr><!-- end ngIf: subTestDetail[0].bone != '' -->
              <!-- ngIf: subTestDetail[0].liver != '' --><tr ng-if="subTestDetail[0].liver != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Liver</td>
                <td class="ng-binding">SGOT, SGPT, Bilirubin Total &amp; Direct, 
Alkaline Phosphatase,LDH, albumin, 
Globulin, A:G Ratio, Serum Protein</td>
              </tr><!-- end ngIf: subTestDetail[0].liver != '' -->
              <!-- ngIf: subTestDetail[0].stress != '' -->
              <!-- ngIf: subTestDetail[0].fatigue != '' -->
              <!-- ngIf: subTestDetail[0].pancreas != '' -->
              <!-- ngIf: subTestDetail[0].heart != '' -->
              <!-- ngIf: subTestDetail[0].muscle != '' --><tr ng-if="subTestDetail[0].muscle != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Muscle</td>
                <td class="ng-binding">Sodium, Potassium, Chloride</td>
              </tr><!-- end ngIf: subTestDetail[0].muscle != '' -->
              <!-- ngIf: subTestDetail[0].performance_hormone != '' -->
              <!-- ngIf: subTestDetail[0].allergy != '' --> 
              <!-- ngIf: subTestDetail[0].cancer_screening != '' -->
               <!-- ngIf: subTestDetail[0].electrolytes != '' -->
               <!-- ngIf: subTestDetail[0].nerve_health != '' -->
              <!-- ngIf: subTestDetail[0].anemia != '' -->

          </tbody></table>
        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#completecare4" aria-expanded="false" aria-controls="collapseThree">
                            <span>Total (72 TESTS)</span> <span class="price">₹2999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="completecare4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>IDEAL FOR Ideal for people who suspect having lifestyle disorders such as anemia, anxiety etc</p>
                            <p>Lifestyle has become so hectic these days that we hardly find any time for physical activities and this can do more harm to you than you can imagine! With SRL Diagnostics' Complete Care-Total package, you can identify illness in the early stages and keep your health in check.</p>
                            <div class="table-responsive" style="padding:0; width: 100%">
          <table class="table table-bordered">

              <tbody><!-- ngIf: subTestDetail[0].general_health != '' --><tr ng-if="subTestDetail[0].general_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">General Health</td>
                <td ng-bind="subTestDetail[0].general_health" class="ng-binding">CBC,Urinalysis</td>
              </tr><!-- end ngIf: subTestDetail[0].general_health != '' -->
              <!-- ngIf: subTestDetail[0].diabetes != '' --><tr ng-if="subTestDetail[0].diabetes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Diabetes</td>
                <td class="ng-binding">Glucose(F),HbA1c</td>
              </tr><!-- end ngIf: subTestDetail[0].diabetes != '' -->
              <!-- ngIf: subTestDetail[0].thyroid != '' --><tr ng-if="subTestDetail[0].thyroid != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Thyriod</td>
                <td class="ng-binding">TSH,T3,T4</td>
              </tr><!-- end ngIf: subTestDetail[0].thyroid != '' -->
              <!-- ngIf: subTestDetail[0].lipid_profile != '' --><tr ng-if="subTestDetail[0].lipid_profile != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Lipid</td>
                <td class="ng-binding">Triglycerides, Cholesterol Total, HDL, 
LDL, VLDL, LDL/HDL Ratio, 
Cholesterol Total / HDL Ratio</td>
              </tr><!-- end ngIf: subTestDetail[0].lipid_profile != '' -->
              <!-- ngIf: subTestDetail[0].kidney != '' --><tr ng-if="subTestDetail[0].kidney != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Kidney</td>
                <td class="ng-binding">Uric Acid,BUN,Creatinine, BUN/Creatinine Ratio, Total Protein</td>
              </tr><!-- end ngIf: subTestDetail[0].kidney != '' -->
              <!-- ngIf: subTestDetail[0].bone != '' --><tr ng-if="subTestDetail[0].bone != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Bone</td>
                <td class="ng-binding">Vitamin D, Calcium</td>
              </tr><!-- end ngIf: subTestDetail[0].bone != '' -->
              <!-- ngIf: subTestDetail[0].liver != '' --><tr ng-if="subTestDetail[0].liver != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Liver</td>
                <td class="ng-binding">SGOT, SGPT, Bilirubin Total &amp; Direct, 
Alkaline Phosphatase,LDH, albumin, 
Globulin, A:G Ratio, Serum Protein</td>
              </tr><!-- end ngIf: subTestDetail[0].liver != '' -->
              <!-- ngIf: subTestDetail[0].stress != '' -->
              <!-- ngIf: subTestDetail[0].fatigue != '' --><tr ng-if="subTestDetail[0].fatigue != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Fatigue</td>
                <td class="ng-binding">Iron, TIBC, % Saturation, Vitamin B12</td>
              </tr><!-- end ngIf: subTestDetail[0].fatigue != '' -->
              <!-- ngIf: subTestDetail[0].pancreas != '' -->
              <!-- ngIf: subTestDetail[0].heart != '' -->
              <!-- ngIf: subTestDetail[0].muscle != '' --><tr ng-if="subTestDetail[0].muscle != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Muscle</td>
                <td class="ng-binding">Sodium, Potassium, Chloride, Magnesium
</td>
              </tr><!-- end ngIf: subTestDetail[0].muscle != '' -->
              <!-- ngIf: subTestDetail[0].performance_hormone != '' -->
              <!-- ngIf: subTestDetail[0].allergy != '' --> 
              <!-- ngIf: subTestDetail[0].cancer_screening != '' -->
               <!-- ngIf: subTestDetail[0].electrolytes != '' -->
               <!-- ngIf: subTestDetail[0].nerve_health != '' -->
              <!-- ngIf: subTestDetail[0].anemia != '' -->

          </tbody></table>
        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#completecare5" aria-expanded="false" aria-controls="collapseThree">
                            <span>Premium (78 TESTS)</span> <span class="price">₹4999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="completecare5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>IDEAL FOR Ideal for people who lead a stressful life, who suffer from diabetes, complain of allergy etc</p>
                            <p>The Complete Care-Premium package is suitable for people who lead busy lives and are suffering from diabetes or usually complain about recurring allergies. Take advantage of this health check-up package and go worry free</p>
                            <div class="table-responsive" style="padding:0; width: 100%">
          <table class="table table-bordered">

              <tbody><!-- ngIf: subTestDetail[0].general_health != '' --><tr ng-if="subTestDetail[0].general_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">General Health</td>
                <td ng-bind="subTestDetail[0].general_health" class="ng-binding">CBC,Urinalysis</td>
              </tr><!-- end ngIf: subTestDetail[0].general_health != '' -->
              <!-- ngIf: subTestDetail[0].diabetes != '' --><tr ng-if="subTestDetail[0].diabetes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Diabetes</td>
                <td class="ng-binding">Glucose (F),HbA1c, Urine Microalbuminuria</td>
              </tr><!-- end ngIf: subTestDetail[0].diabetes != '' -->
              <!-- ngIf: subTestDetail[0].thyroid != '' --><tr ng-if="subTestDetail[0].thyroid != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Thyriod</td>
                <td class="ng-binding">TSH,FT3, FT4</td>
              </tr><!-- end ngIf: subTestDetail[0].thyroid != '' -->
              <!-- ngIf: subTestDetail[0].lipid_profile != '' --><tr ng-if="subTestDetail[0].lipid_profile != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Lipid</td>
                <td class="ng-binding">Triglycerides, Cholesterol Total, HDL, LDL, VLDL, 
LDL/HDL Ratio, Cholesterol Total / HDL Ratio</td>
              </tr><!-- end ngIf: subTestDetail[0].lipid_profile != '' -->
              <!-- ngIf: subTestDetail[0].kidney != '' --><tr ng-if="subTestDetail[0].kidney != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Kidney</td>
                <td class="ng-binding">Uric Acid, BUN, Creatinine, BUN/Creatinine Ratio, Total Protein</td>
              </tr><!-- end ngIf: subTestDetail[0].kidney != '' -->
              <!-- ngIf: subTestDetail[0].bone != '' --><tr ng-if="subTestDetail[0].bone != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Bone</td>
                <td class="ng-binding">Vitamin D, Calcium</td>
              </tr><!-- end ngIf: subTestDetail[0].bone != '' -->
              <!-- ngIf: subTestDetail[0].liver != '' --><tr ng-if="subTestDetail[0].liver != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Liver</td>
                <td class="ng-binding">SGOT,SGPT,Bilirubin Total &amp; Direct, Alkaline Phosphatase,LDH,Albumin, Globulin,A:G Ratio, Serum Protein ,GGT</td>
              </tr><!-- end ngIf: subTestDetail[0].liver != '' -->
              <!-- ngIf: subTestDetail[0].stress != '' --><tr ng-if="subTestDetail[0].stress != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Stress</td>
                <td class="ng-binding">Cortisol</td>
              </tr><!-- end ngIf: subTestDetail[0].stress != '' -->
              <!-- ngIf: subTestDetail[0].fatigue != '' --><tr ng-if="subTestDetail[0].fatigue != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Fatigue</td>
                <td class="ng-binding">Iron, TIBC, Ferritin , % Saturation,
Vitamin B12, Folic Acid</td>
              </tr><!-- end ngIf: subTestDetail[0].fatigue != '' -->
              <!-- ngIf: subTestDetail[0].pancreas != '' -->
              <!-- ngIf: subTestDetail[0].heart != '' --><tr ng-if="subTestDetail[0].heart != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Heart</td>
                <td class="ng-binding">Hs-CRP, Apo lipoprotein- B</td>
              </tr><!-- end ngIf: subTestDetail[0].heart != '' -->
              <!-- ngIf: subTestDetail[0].muscle != '' --><tr ng-if="subTestDetail[0].muscle != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Muscle</td>
                <td class="ng-binding">Sodium, Potassium, Chloride</td>
              </tr><!-- end ngIf: subTestDetail[0].muscle != '' -->
              <!-- ngIf: subTestDetail[0].performance_hormone != '' -->
              <!-- ngIf: subTestDetail[0].allergy != '' --><tr ng-if="subTestDetail[0].allergy != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Allergy</td>
                <td class="ng-binding">Total IgE</td>
              </tr><!-- end ngIf: subTestDetail[0].allergy != '' --> 
              <!-- ngIf: subTestDetail[0].cancer_screening != '' -->
               <!-- ngIf: subTestDetail[0].electrolytes != '' -->
               <!-- ngIf: subTestDetail[0].nerve_health != '' -->
              <!-- ngIf: subTestDetail[0].anemia != '' -->

          </tbody></table>
        </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
			</div>
		</div>
	</div>
<!-- //Packages3 -->

<!-- Packages 4 -->
<div class="jarallax team activecare" id="activecare">
		<div class="container">
			<div class="team-heading">
        <h3>Active Care<span></span></h3>
        <p>Ideal for those who follow a sedentary lifestyle, hectic work schedules with no time for physical activity</p>
			</div>
			<div class="agileits-team-grids">
      <div class="row">
        <div class="accordian-conainer">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#activecare1" aria-expanded="true" aria-controls="collapseOne">
                            <span>Sedentary Lifestyle (46 TESTS)</span> <span class="price">₹1899</span>
                            </a>
                        </h4>
                    </div>
                    <div id="activecare1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                           <p>IDEAL FOR Ideal for  those suffering from 'Sitting Disease'</p>
                           <p>Driving during your morning commute to an 8-hour-a-day desk job, and then unwinding on the couch in front of the television all evening. What’s more, Do you depend on email, cell phone apps, direct-deposit paychecks, and online shopping to accomplish tasks that 10 or 20 years ago would have required you to get up and run errands? If so, then you may have "sitting disease,” & might be putting your health at riskof various Lifestyle disorders. Take SRL Active Care for Sedentary Lifestyle test & find out how you can prevent these becoming part of your future</p>
                           <div class="table-responsive" style="padding:0; width: 100%">
          <table class="table table-bordered">

              <tbody><!-- ngIf: subTestDetail[0].general_health != '' --><tr ng-if="subTestDetail[0].general_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">General Health</td>
                <td ng-bind="subTestDetail[0].general_health" class="ng-binding">CBC, Urinalysis</td>
              </tr><!-- end ngIf: subTestDetail[0].general_health != '' -->
              <!-- ngIf: subTestDetail[0].diabetes != '' --><tr ng-if="subTestDetail[0].diabetes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Diabetes</td>
                <td class="ng-binding">HbA1c</td>
              </tr><!-- end ngIf: subTestDetail[0].diabetes != '' -->
              <!-- ngIf: subTestDetail[0].thyroid != '' --><tr ng-if="subTestDetail[0].thyroid != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Thyriod</td>
                <td class="ng-binding">TSH</td>
              </tr><!-- end ngIf: subTestDetail[0].thyroid != '' -->
              <!-- ngIf: subTestDetail[0].lipid_profile != '' --><tr ng-if="subTestDetail[0].lipid_profile != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Lipid</td>
                <td class="ng-binding">Triglycerides, Cholesterol Total, HDL, LDL, VLDL, LDL/HDL Ratio, Cholesterol Total / HDL Ratio</td>
              </tr><!-- end ngIf: subTestDetail[0].lipid_profile != '' -->
              <!-- ngIf: subTestDetail[0].kidney != '' --><tr ng-if="subTestDetail[0].kidney != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Kidney</td>
                <td class="ng-binding">BUN, Creatinine</td>
              </tr><!-- end ngIf: subTestDetail[0].kidney != '' -->
              <!-- ngIf: subTestDetail[0].bone != '' --><tr ng-if="subTestDetail[0].bone != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Bone</td>
                <td class="ng-binding">Calcium</td>
              </tr><!-- end ngIf: subTestDetail[0].bone != '' -->
              <!-- ngIf: subTestDetail[0].liver != '' -->
              <!-- ngIf: subTestDetail[0].stress != '' -->
              <!-- ngIf: subTestDetail[0].fatigue != '' -->
              <!-- ngIf: subTestDetail[0].pancreas != '' -->
              <!-- ngIf: subTestDetail[0].heart != '' -->
              <!-- ngIf: subTestDetail[0].muscle != '' --><tr ng-if="subTestDetail[0].muscle != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Muscle</td>
                <td class="ng-binding">Sodium, Potassium, Chloride</td>
              </tr><!-- end ngIf: subTestDetail[0].muscle != '' -->
              <!-- ngIf: subTestDetail[0].performance_hormone != '' -->
              <!-- ngIf: subTestDetail[0].allergy != '' --> 
              <!-- ngIf: subTestDetail[0].cancer_screening != '' -->
               <!-- ngIf: subTestDetail[0].electrolytes != '' -->
               <!-- ngIf: subTestDetail[0].nerve_health != '' -->
              <!-- ngIf: subTestDetail[0].anemia != '' -->

          </tbody></table>
        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#activecare2" aria-expanded="false" aria-controls="collapseTwo">
                            <span>Professionals on the go (47 TESTS)</span> <span class="price">₹1399</span>
                            </a>
                        </h4>
                    </div>
                    <div id="activecare2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <p>IDEAL FOR Professionals who tend to ignore their health in lieu of professional tasks at hand</p>
                            <p>Over time, extended periods at the computer can take a toll on your health & results in Weight gain, fatigue, musculoskeletal problems, anxiety & stress.Take SRL Active Care for Professionals on the Go test & find out how you can prevent these becoming part of your future</p>
                            <div class="table-responsive" style="padding:0; width: 100%">
          <table class="table table-bordered">

              <tbody><!-- ngIf: subTestDetail[0].general_health != '' --><tr ng-if="subTestDetail[0].general_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">General Health</td>
                <td ng-bind="subTestDetail[0].general_health" class="ng-binding">CBC, Urinalysis</td>
              </tr><!-- end ngIf: subTestDetail[0].general_health != '' -->
              <!-- ngIf: subTestDetail[0].diabetes != '' --><tr ng-if="subTestDetail[0].diabetes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Diabetes</td>
                <td class="ng-binding">HbA1c</td>
              </tr><!-- end ngIf: subTestDetail[0].diabetes != '' -->
              <!-- ngIf: subTestDetail[0].thyroid != '' --><tr ng-if="subTestDetail[0].thyroid != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Thyriod</td>
                <td class="ng-binding">TSH</td>
              </tr><!-- end ngIf: subTestDetail[0].thyroid != '' -->
              <!-- ngIf: subTestDetail[0].lipid_profile != '' --><tr ng-if="subTestDetail[0].lipid_profile != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Lipid</td>
                <td class="ng-binding">Triglycerides, Cholesterol Total, HDL, LDL, VLDL, LDL/HDL Ratio, Cholesterol Total / HDL Ratio</td>
              </tr><!-- end ngIf: subTestDetail[0].lipid_profile != '' -->
              <!-- ngIf: subTestDetail[0].kidney != '' --><tr ng-if="subTestDetail[0].kidney != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Kidney</td>
                <td class="ng-binding">BUN, Creatinine </td>
              </tr><!-- end ngIf: subTestDetail[0].kidney != '' -->
              <!-- ngIf: subTestDetail[0].bone != '' -->
              <!-- ngIf: subTestDetail[0].liver != '' --><tr ng-if="subTestDetail[0].liver != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Liver</td>
                <td class="ng-binding">SGOT, SGPT, Bilirubin Total </td>
              </tr><!-- end ngIf: subTestDetail[0].liver != '' -->
              <!-- ngIf: subTestDetail[0].stress != '' --><tr ng-if="subTestDetail[0].stress != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Stress</td>
                <td class="ng-binding">Cortisol</td>
              </tr><!-- end ngIf: subTestDetail[0].stress != '' -->
              <!-- ngIf: subTestDetail[0].fatigue != '' --><tr ng-if="subTestDetail[0].fatigue != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Fatigue</td>
                <td class="ng-binding">Vitamin B12</td>
              </tr><!-- end ngIf: subTestDetail[0].fatigue != '' -->
              <!-- ngIf: subTestDetail[0].pancreas != '' -->
              <!-- ngIf: subTestDetail[0].heart != '' -->
              <!-- ngIf: subTestDetail[0].muscle != '' -->
              <!-- ngIf: subTestDetail[0].performance_hormone != '' -->
              <!-- ngIf: subTestDetail[0].allergy != '' --> 
              <!-- ngIf: subTestDetail[0].cancer_screening != '' -->
               <!-- ngIf: subTestDetail[0].electrolytes != '' -->
               <!-- ngIf: subTestDetail[0].nerve_health != '' -->
              <!-- ngIf: subTestDetail[0].anemia != '' -->

          </tbody></table>
        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#activecare3" aria-expanded="false" aria-controls="collapseThree">
                            <span>Homemakers (48 TESTS)</span> <span class="price">₹1999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="activecare3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>IDEAL FOR Ideal for Homemakers who take full care of their family but sometimes ignore their health & complain of fatigue & joint pain</p>
                            <p>Feeling exhausted all the time but you’re not sure why, this is the chronic fatigue test for you. Take out the guesswork and understand the simple changes you can make to improve your energy levels.Take test & find out how you can prevent these becoming part of your future</p>
                            <div class="table-responsive" style="padding:0; width: 100%">
          <table class="table table-bordered">

              <tbody><!-- ngIf: subTestDetail[0].general_health != '' --><tr ng-if="subTestDetail[0].general_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">General Health</td>
                <td ng-bind="subTestDetail[0].general_health" class="ng-binding">CBC, Urinalysis</td>
              </tr><!-- end ngIf: subTestDetail[0].general_health != '' -->
              <!-- ngIf: subTestDetail[0].diabetes != '' --><tr ng-if="subTestDetail[0].diabetes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Diabetes</td>
                <td class="ng-binding">Glucose(F)</td>
              </tr><!-- end ngIf: subTestDetail[0].diabetes != '' -->
              <!-- ngIf: subTestDetail[0].thyroid != '' --><tr ng-if="subTestDetail[0].thyroid != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Thyriod</td>
                <td class="ng-binding">TSH</td>
              </tr><!-- end ngIf: subTestDetail[0].thyroid != '' -->
              <!-- ngIf: subTestDetail[0].lipid_profile != '' --><tr ng-if="subTestDetail[0].lipid_profile != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Lipid</td>
                <td class="ng-binding">Triglycerides, Cholesterol Total, HDL, LDL, VLDL, LDL/HDL Ratio,Cholesterol Total / HDL Ratio</td>
              </tr><!-- end ngIf: subTestDetail[0].lipid_profile != '' -->
              <!-- ngIf: subTestDetail[0].kidney != '' --><tr ng-if="subTestDetail[0].kidney != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Kidney</td>
                <td class="ng-binding">BUN, Creatinine </td>
              </tr><!-- end ngIf: subTestDetail[0].kidney != '' -->
              <!-- ngIf: subTestDetail[0].bone != '' --><tr ng-if="subTestDetail[0].bone != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Bone</td>
                <td class="ng-binding">Calcium, Phosphorous, Alkaline Phosphatase</td>
              </tr><!-- end ngIf: subTestDetail[0].bone != '' -->
              <!-- ngIf: subTestDetail[0].liver != '' -->
              <!-- ngIf: subTestDetail[0].stress != '' -->
              <!-- ngIf: subTestDetail[0].fatigue != '' --><tr ng-if="subTestDetail[0].fatigue != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Fatigue</td>
                <td class="ng-binding">Iron ,TIBC, % Saturation, Ferritin ,Vitamin B12</td>
              </tr><!-- end ngIf: subTestDetail[0].fatigue != '' -->
              <!-- ngIf: subTestDetail[0].pancreas != '' -->
              <!-- ngIf: subTestDetail[0].heart != '' -->
              <!-- ngIf: subTestDetail[0].muscle != '' -->
              <!-- ngIf: subTestDetail[0].performance_hormone != '' -->
              <!-- ngIf: subTestDetail[0].allergy != '' --> 
              <!-- ngIf: subTestDetail[0].cancer_screening != '' -->
               <!-- ngIf: subTestDetail[0].electrolytes != '' -->
               <!-- ngIf: subTestDetail[0].nerve_health != '' -->
              <!-- ngIf: subTestDetail[0].anemia != '' -->

          </tbody></table>
        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#activecare4" aria-expanded="false" aria-controls="collapseThree">
                            <span>Socializers (38 TESTS)</span> <span class="price">₹1899</span>
                            </a>
                        </h4>
                    </div>
                    <div id="activecare4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>IDEAL FOR Ideal for people who like to work hard and party harder</p>
                            <p>Sometimes the junk food & drinks can take toll on your health especially liver.Take test & find out how you can party harder & yet be healthy</p>
                            <div class="table-responsive" style="padding:0; width: 100%">
          <table class="table table-bordered">

              <tbody><!-- ngIf: subTestDetail[0].general_health != '' --><tr ng-if="subTestDetail[0].general_health != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">General Health</td>
                <td ng-bind="subTestDetail[0].general_health" class="ng-binding">CBC</td>
              </tr><!-- end ngIf: subTestDetail[0].general_health != '' -->
              <!-- ngIf: subTestDetail[0].diabetes != '' --><tr ng-if="subTestDetail[0].diabetes != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Diabetes</td>
                <td class="ng-binding">HbA1c</td>
              </tr><!-- end ngIf: subTestDetail[0].diabetes != '' -->
              <!-- ngIf: subTestDetail[0].thyroid != '' -->
              <!-- ngIf: subTestDetail[0].lipid_profile != '' --><tr ng-if="subTestDetail[0].lipid_profile != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Lipid</td>
                <td class="ng-binding">Triglycerides, Cholesterol Total, HDL, LDL, VLDL, LDL/HDL Ratio,Cholesterol Total / HDL Ratio</td>
              </tr><!-- end ngIf: subTestDetail[0].lipid_profile != '' -->
              <!-- ngIf: subTestDetail[0].kidney != '' --><tr ng-if="subTestDetail[0].kidney != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Kidney</td>
                <td class="ng-binding">BUN, Creatinine </td>
              </tr><!-- end ngIf: subTestDetail[0].kidney != '' -->
              <!-- ngIf: subTestDetail[0].bone != '' -->
              <!-- ngIf: subTestDetail[0].liver != '' --><tr ng-if="subTestDetail[0].liver != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Liver</td>
                <td class="ng-binding">SGOT, SGPT, Bilirubin Total &amp; Direct, 
Alkaline Phosphatase, LDH, albumin, 
Globulin, A:G Ratio, Serum Protein, GGT</td>
              </tr><!-- end ngIf: subTestDetail[0].liver != '' -->
              <!-- ngIf: subTestDetail[0].stress != '' -->
              <!-- ngIf: subTestDetail[0].fatigue != '' -->
              <!-- ngIf: subTestDetail[0].pancreas != '' --><tr ng-if="subTestDetail[0].pancreas != ''" class="ng-scope">
                <td class="sedentaryCare-tr01">Pancreas</td>
                <td class="ng-binding">Amylase, Lipase</td>
              </tr><!-- end ngIf: subTestDetail[0].pancreas != '' -->
              <!-- ngIf: subTestDetail[0].heart != '' -->
              <!-- ngIf: subTestDetail[0].muscle != '' -->
              <!-- ngIf: subTestDetail[0].performance_hormone != '' -->
              <!-- ngIf: subTestDetail[0].allergy != '' --> 
              <!-- ngIf: subTestDetail[0].cancer_screening != '' -->
               <!-- ngIf: subTestDetail[0].electrolytes != '' -->
               <!-- ngIf: subTestDetail[0].nerve_health != '' -->
              <!-- ngIf: subTestDetail[0].anemia != '' -->

          </tbody></table>
        </div>
                        </div>
                    </div>
                </div>
            

            </div>
        </div>
    </div>
			</div>
		</div>
	</div>
<!-- //Packages4 -->

<!-- Packages 5 -->
<div class="jarallax team vitalcare" id="vitalcare">
		<div class="container">
			<div class="team-heading">
        <h3>Vital Care<span></span></h3>
        <p>Ideal for testing the health of various organs and identifying specific issues</p>
			</div>
			<div class="agileits-team-grids">
      <div class="row">
        <div class="accordian-conainer">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#vitalcare1" aria-expanded="true" aria-controls="collapseOne">
                            <span>Heart</span> <span class="price">₹1999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="vitalcare1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                           <p>IDEAL FOR Advance Testing for Heart ailments</p>
                           <div class="table-responsive">
        <table class="table table-bordered ng-binding" ng-bind-html="subTestDetail[0].general_health">              
              <tbody><tr>
                <td>Triglycerides, Cholesterol Total, HDL, LDL, VLDL, LDL/HDL Ratio, Cholesterol Total / HDL Ratio</td>
              </tr>
              <tr>
                <td>hs-CRP, Apolipoprotein-A1, Apo lipoprotein-B, APO A1/B Ratio</td>
              </tr>
              <tr>
                <td>HbA1c</td>
              </tr>
              <tr>
                <td>BUN, Creatinine, Urinalysis</td>
              </tr>
              <tr>
                <td>Electrolytes (Sodium, Potassium, Chloride)</td>
              </tr>
              </tbody>
</table>
        
               
       

        </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#vitalcare2" aria-expanded="false" aria-controls="collapseTwo">
                            <span>Liver</span> <span class="price">₹1999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="vitalcare2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <p>IDEAL FOR Advance Testing for Liver Functioning</p>
                            <table class="table table-bordered ng-binding" ng-bind-html="subTestDetail[0].general_health">              
                            <tbody><tr>
                <td>SGOT, SGPT,LDH, Alkaline Phosphatase, Albumin, Globulin, A/G Ratio, 
Total Protein, Bilirubin Total , Bilirubin Direct, Indirect</td>
              </tr>
              <tr>
                <td>GGT</td>
              </tr>
              <tr>
                <td>HbsAg</td>
              </tr>
              <tr>
                <td>HbA1c</td>
              </tr>
              <tr>
                <td>Triglycerides, Cholesterol total</td>
              </tr>

              </tbody>
</table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#vitalcare3" aria-expanded="false" aria-controls="collapseThree">
                            <span>Kidney</span> <span class="price">₹1999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="vitalcare3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>IDEAL FOR Advance Testing for Kidney Functioning</p>
                            <table class="table table-bordered ng-binding" ng-bind-html="subTestDetail[0].general_health">              
                         <tbody><tr>
                <td>BUN , Creatinine, Uric Acid, Urinalysis, Total Protein, Albumin, Sodium, Potassium, Chloride</td>
              </tr>
              <tr>
                <td>Calcium, Phosphorous, Alkaline Phosphatase</td>
              </tr>
              <tr>
                <td>Glucose(F) , HbA1c, CBC</td>
              </tr>
              <tr>
                <td>Cholesterol total</td>
              </tr>
      

              </tbody>
</table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#vitalcare4" aria-expanded="false" aria-controls="collapseThree">
                            <span>Thyroid</span> <span class="price">₹1999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="vitalcare4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>IDEAL FOR Advance Testing for Thyroid Functioning</p>
                            <table class="table table-bordered ng-binding" ng-bind-html="subTestDetail[0].general_health">              
              
              <tbody><tr>
                <td>FT3, FT4, TSH</td>
              </tr>
              <tr>
                <td>Triglycerides, Cholesterol Total, HDL, LDL, VLDL, LDL/HDL Ratio, Cholesterol Total / HDL Rati</td>
              </tr>
              <tr>
                <td>HbA1c</td>
              </tr>
              <tr>
                <td>TPO Antibody Test</td>
              </tr>
              
              
      

              </tbody>
</table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#vitalcare5" aria-expanded="false" aria-controls="collapseThree">
                            <span>Bone</span> <span class="price">₹1999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="vitalcare5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>IDEAL FOR Advance Testing for Bone & Joint Pain</p>
                            <table class="table table-bordered ng-binding" ng-bind-html="subTestDetail[0].general_health">              
                            <tbody><tr>
                <td>Rheumatoid Factor, Alkaline Phosphatase , Calcium, Phosphorous, Uric Acid</td>
              </tr>
              <tr>
                <td>CBC, CRP</td>
              </tr>
              <tr>
                <td>Vitamin D, Rheumatoid Factor</td>
              </tr>
              </tbody>
</table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#vitalcare6" aria-expanded="false" aria-controls="collapseThree">
                            <span>Anemia</span> <span class="price">₹1999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="vitalcare6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>IDEAL FOR Advance Testing for Chronic Fatigue</p>
                            <table class="table table-bordered ng-binding" ng-bind-html="subTestDetail[0].general_health">              
               
              <tbody><tr>
                <td>CBC, CRP</td>
              </tr>
              <tr>
                <td>Iron, TIBC, % saturation, Ferritin</td>
              </tr>
              <tr>
                <td>Vitamin B12, Folic Acid</td>
              </tr>
             
             
              </tbody>
</table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#vitalcare7" aria-expanded="false" aria-controls="collapseThree">
                            <span>Menopause</span> <span class="price">₹1999</span>
                            </a>
                        </h4>
                    </div>
                    <div id="vitalcare7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <p>IDEAL FOR Advance Testing for Women approaching Menopause</p>
                            <table class="table table-bordered ng-binding" ng-bind-html="subTestDetail[0].general_health">              
        
              <tbody><tr>
                <td>FSH, FT4, TSH, Estradiol</td>
              </tr>
              <tr>
                <td>Calcium, Phosphorous</td>
              </tr>
              <tr>
                <td>Vitamin D</td>
              </tr>
             
             
              </tbody>
</table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
			</div>
		</div>
	</div>
<!-- //Packages5 -->
<footer class="footer">
      	© 2016 - SRL Diagnostics. All Rights Reserved
      </footer>
     <script src="<?php echo S3_URL?>/site/srl-assets/js/vendor.js"></script>    
    <script src="<?php echo S3_URL?>/site/srl-assets/js/main.js"></script>
   </body>
</html>