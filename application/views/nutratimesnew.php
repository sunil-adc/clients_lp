
<?php $all_array = all_arrays(); ?><!DOCTYPE html>
<html lang="en">
<head>
  <title>Nutratimes</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>


  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="<?php echo S3_URL?>/site/nutratimes1-assets/lib/jquery-validation-1.17.0/jquery.validate.min.js"></script>
  <link rel="stylesheet" href="<?php echo S3_URL?>/site/nutratimes1-assets/css/custom.css">
  <script src="<?php echo S3_URL?>/site/nutratimes1-assets/js/custom.js"></script>
  
</head>
<body>
 
<div class="container">
  <div class="row">
    <div class="col-md-6 text-left">
      <img src="<?php echo S3_URL?>/site/nutratimes1-assets/images/nutratimes_logo.png" class="logo">
    </div>
    <div class="col-md-6 text-right">
      <img src="<?php echo S3_URL?>/site/nutratimes1-assets/images/rating.png" class="logo">
    </div>
  </div>

  <div class="row cod">
    <div class="col-sm-12 text-center">
      <h3>You have been Approved for Cash On Delivery </h3>
        <h5>Free Overnight Shipping within India</h5>
    </div>
  </div>

  <div class="row guarantee">
    <div class="col-sm-2 text-right">
      <img src="<?php echo S3_URL?>/site/nutratimes1-assets/images/moneyback-guarantee.png" style="width: 80px;">
    </div>
    <div class="col-sm-10 text-left">
      <h3>100% Money Back Guarantee:</h3>
      <h5>Our Promise: Easy Refund and Pick up - If you don't lose weight, simply call us and we will give you a full 100% hassle-free refund. We stand behind our product. Try it, Risk Free.</h5>
    </div>
  </div>


  <div class="row">
    <div class="col-sm-12">
      <form id="productform" name="productform">
      <div class="products" id="product1" data-pid="1"  >
        <input type="radio" name="image_radio" value="1" style="display: none;" id="p1"  />
           <img id="myImage2" src="<?php echo S3_URL?>/site/nutratimes1-assets/images/three-bottle.png" class="img-fluid">
           <img id="myImage2h" src="<?php echo S3_URL?>/site/nutratimes1-assets/images/three-bottle-hover.png" class="img-fluid" style="display: none;" >
          
      </div> 
      
      <div class="products" id="product2" data-pid="2">
        <input type="radio" name="image_radio" value="2" style="display: none;" id="p2" />
            <img id="myImage1" src="<?php echo S3_URL?>/site/nutratimes1-assets/images/two-bottle.png" class="img-fluid">
            <img id="myImage1h" src="<?php echo S3_URL?>/site/nutratimes1-assets/images/two-bottle-hover.png" class="img-fluid" style="display: none;">
      </div> 
      
      <div class="products" id="product3" data-pid="3">
           <input type="radio" name="image_radio" value="3" style="display: none;" id="p3" checked="checked" />
           <img id="myImage" src="<?php echo S3_URL?>/site/nutratimes1-assets/images/single-bottle.png" class="img-fluid"  style="display: none;"> 
           <img id="myImageh" src="<?php echo S3_URL?>/site/nutratimes1-assets/images/single-bottle-hover.png" class="img-fluid"> 
         
      </div>
       </form>
    </div>
  </div>
  <div id ="hr"> 
 <div class="row" id="formtop">
         <div  class="col-md-12 form-header text-center">
            <b class="text-white text-uppercase">WHERE DO WE SHIP YOUR BOTTLE</b>
         </div>
         <div id="formbottom" class="col-md-12 border p-3">
           <form class="needs-validation" novalidate="" method="POST" id="detailsform">
               <input type="hidden" name="prod" id="prod" value=""> 
               <div class="form-row">
                  <div class="col-md-6 mb-3">
                     <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First name" >
                     <div class="invalid-feedback">
                        Please enter your first name.
                     </div>
                  </div>
                  <div class="col-md-6 mb-3">
                     <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last name" >
                     <div class="invalid-feedback">
                        Please enter your last name.
                     </div>
                  </div>
               </div>
               <div class="form-row">
                  <div class="col-md-12 mb-3">
                     <input type="tel" class="form-control" name="zip_code"  id="zip_code" placeholder="Pincode" >
                     <div class="invalid-feedback">
                        Please provide a valid pincode.
                     </div>
                  </div>
                  <div class="col-md-12 mb-3">
                     <input type="text" class="form-control" name="address_1"  id="address_1" placeholder="Your Address" >
                     <div class="invalid-feedback">
                        Please provide a valid address.
                     </div>
                  </div>
                  <!--<div class="col-md-12 mb-3">
                     <input type="text" class="form-control" name="address_2"  id="address_2" placeholder="Landmark" >
                     <div class="invalid-feedback">
                        Please provide a valid address.
                     </div>
                  </div>-->
                  <div class="col-md-6 mb-3">
                     <input type="text" class="form-control" name="city" id="city" placeholder="City" >
                     <div class="invalid-feedback">
                        Please provide a valid city.
                     </div>
                  </div>
                  <div class="col-md-6 mb-3">
                     <select id="state" class="form-control" name="state" >
                        <option value='' >Select State</option>
                          <?php 
                          foreach ($all_array['NUTRATIMES_STATE_ARR'] as $k => $v) {
                              
                              echo "<option value='".$k."' >".$v."</option>";
                          }
                          ?>
                        </select>
                     <div class="invalid-feedback">
                        Please provide a valid state.
                     </div>
                  </div>
                  <div class="col-md-6 mb-3">
                     <input type="email"  name="email" class="form-control" id="email" placeholder="Email Address" >
                     <div class="invalid-feedback">
                        Please provide a valid email address.
                     </div>
                  </div>
                  <div class="col-md-6 mb-3">
                            <input type="tel" inputmode="numeric"  name="phone_number" class="form-control" id="phone_number" placeholder="Mobile Number" >
                   
                  </div>
                  <div class="otp">
                  <div class="col-md-12 mb-6" >
                        <input type="tel" inputmode="numeric"  name="otp" class="form-control" id="otp" placeholder="OTP" maxlength="4" style="width:100%" >
                          <span id="otp_error" class="otp_error"></span>


                  </div>
                  <div class="col-md-12 mb-6"  id="show_resend">
                       <span id="otp_sent" class="otp_sent"></span>
                      <span><a href="javascript:otpresend('<?php echo SITE_URL?>nutratimes/otp_resend')">Click Here</a> To Resend OTP</span>
                  </div>
                </div>
               </div>

                  <!--<input type="hidden" name="country" value="IN">
                  <input id="product_id" type="hidden" name="product_id" value="1166">-->

               <div class="form-row">
                  <div class="col-12 text-center">
                     <button id="btn-form-submit" type="submit" class="btn-placeorder">
                        PLACE ORDER
                     </button>
                     <br><span>256 bit secure form</span>
                  </div>
               </div>
               <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
               <input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
               <input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
               <input type="hidden" id="utm_sub" name="utm_sub" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
               <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
               
            </form>
         </div>
      </div>
    </div>

   <div class="row">
      <div class="col-md-12 text-center" style="padding:60px;">
        <img src="<?php echo S3_URL?>/site/nutratimes1-assets/images/verified.png" class="img-fluid"><br>
         <img src="<?php echo S3_URL?>/site/nutratimes1-assets/images/banner02.jpg" class="img-fluid">
      </div>
   </div>
   
</div>

</body>
</html>
