
<!DOCTYPE html>
<html lang="en"><head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Religare Health Insurance</title>
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/religare-health/religare-images/favicon.ico" type="image/x-icon" />
<script src="<?php echo S3_URL?>/site/scripts/jquery-1.11.3.min.js"></script>
      <!-- Bootstrap -->
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/religare-health/religare-css/bootstrap.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/religare-health/religare-css/new-age.min.css"> 
      <link href="https://fonts.googleapis.com/css?family=Sunflower:300,500,700" rel="stylesheet">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      <script>
      	$(document).ready(function() {
      		$("#children1").hide();
      		$("#children2").hide();
      	});

       
       function show_mem(i){
       	var val = $("#family_member"+i).val();
       	if(val == 1){
       		$("#children"+i).hide();
       	}
       	
       		
       		if(val == 2){
       			$("#children"+i).show();
       			$("#children"+i).val("0");
       		}
       		if(val == 3){
       			$("#children"+i).show();
       			$("#children"+i).val("1");
       		}
       		if(val == 4){
       			$("#children"+i).show();
       			$("#children"+i).val("2");
       		}
       		if(val == 5){
       			$("#children"+i).show();
       			$("#children"+i).val("3");
       		}
       		if(val == 6){
       			$("#children"+i).show();
       			$("#children"+i).val("4");
       		}
       
       }
    
      </script>

      <!-- Global site tag (gtag.js) - Google Ads: 798077325 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-798077325"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'AW-798077325');
	</script>


   </head>
   <body id="page-top">
      <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
         <div class="container text-center">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">                
               <a class="navbar-brand page-scroll" href="#page-top">
               <img src="<?php echo S3_URL?>/site/religare-health/religare-images/logo.png" class="logo"/>
               </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1 ">
               <h4 style="color: #fff;text-shadow: 1px 1px 5px #2562bb;font-size: 22px;
                  margin: 40px 0px;"></h4>
            </div>
            <!-- /.navbar-collapse -->
         </div>
         <!-- /.container-fluid -->
      </nav>
	  
	  
	  	<header>
      	<?php $all_array = all_arrays(); ?>
         <div class="container form_margin">
            <div class="row">
				<div class="col-md-6 form_box">
				<br/><br/> 
					<h2>
					It'll Only Take A Few Seconds To Take A Responsible Decision For You And Your Family
					</h2>
                </div>
                <div class="col-md-2"> 
				</div>
               <div class="col-md-4"> 
                  <div class="jumbotron form_box">
                     <div class="row text-center" id="f_div" >
                        <div class="text-center  col-md-12 col-lg-12">
                           <h4 class="form-title">Get a Free Quote in Seconds</h4><br/>
                        </div>
                        <div class="text-left col-lg-12">
                           <!-- CONTACT FORM https://github.com/jonmbake/bootstrap3-contact-form -->
                           <form role="form" id="feedbackForm1" class="religare_form" action="JavaScript:void(0)" onsubmit="religarehealth_jsfrm('<?php echo SITE_URL?>religare/religarefrm','health',1)">
                              <div class="form-group">
                                 <input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid_religare('name', 'Please enter correct name')" data-attr="Please enter correct name">
                                 <span class="help-block" id="name_err1"></span>
                              </div>
                               <div class="form-group">
                                 <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid_religare('email', 'Please enter correct email')" data-attr="Please enter correct email">
                                 <span class="help-block" id="email_err1" ></span>
                              </div>

										<div class="form-group">
											<div class=" family_div">											   
												<select class="form-control family" name="family_member" id="family_member1" 
												onchange="show_mem(1)">
												<option value="">Family Members</option>
												<option value="1">1 </option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												<option value="6">6</option>
                              					</select>														
												<span class="help-block" id="family_member_err1"></span>
											</div>
											<div class="family_div">
											<input type="number" class="form-control clildren" id="children1" name="children"  
											placeholder="Children" min="0" max="4" >												
												<span class="help-block" id="children_err1"></span>
											</div>
										</div> 

									   <div class="form-group"> 
										<select class="form-control" name="eldes_member" id="eldes_member1">
												<option value="">Age of Eldest Member</option>
												<option value="18 - 24">18-24 Years</option>
												<option value="25 - 35">25-35 Years</option>
												<option value="36 - 40">36-40 Years</option>
												<option value="41 - 45">41-45 Years</option>
												<option value="46 - 50">46-50 Years</option>
												<option value="51 - 55">51-55 Years</option>
												<option value="56 - 60">56-60 Years</option>
												<option class="ageLimitSelect" value="61 - 65">61-65 Years</option>
												<option class="ageLimitSelect" value="66 - 70">66-70 Years</option>
												<option class="ageLimitSelect" value="71 - 75">71-75 Years</option>
												<option class="ageLimitSelect" value="76 - 99">&gt;75 Years</option>
                              	</select>
                              	<span class="help-block" id="eldes_member_err1"></span>
										</div>


                              <div class="form-group">
                                 <input type="text" class="form-control only_numeric" id="phone1" name="phone" pattern="\d*" maxlength="10" placeholder="Mobile Number" onkeyup="chck_valid_religare('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                                 <span class="help-block" id="phone_err1"> </span>
                              </div> 
                            
                              <div class="form-group">
                                 
								 <select class="form-control" id="city1" name="city" onchange="chck_valid_religare('city', 'Please select correct city')" 
								 data-attr="Please select city">
								 <option value="" >Select city</option>
								 <?php foreach($city_arr as $city){?>
                                       
                                       <option value="<?php echo $city['city'];?>" ><?php echo $city['city'];?></option>
								 <?php }?>   
                                 </select>
                                 <span class="help-block" id="city_err1"></span>
                                 </div>
							  
                            
                              
                              <input type="submit" id="frm-sbmtbtn1" class="btn btn-primary btn-lg" value="CALL ME BACK">
                           </form>
                           <!-- END CONTACT FORM --> 


                        </div>
                     </div>

					 <div class="row text-center hidden_div" id="otpForm_div1">  
							<form role="form" id="otpForm" class="medlife_form" action="JavaScript:void(0)" onsubmit="religarehealth_otp('<?php echo SITE_URL?>religare/religare_otpverification', '<?php echo SITE_URL?>', 1)" >
							  <input type="text" class="form-control" id="otp1" name="otp" placeholder="Enter OTP" maxlength="4" pattern="\d*">
							  <input type="hidden"  id="otp_phone1" name="otp_phone" >
							  <span class="help-block" id="otp_err1"></span>
							  <input type="submit" id="otpfrm-sbmtbtn1" style="width: 30%; float: left; font-size: 13px" class="btn btn-primary btn-lg" value="VERIFY">
							  <!--<input type="submit" id="res-sbmtbtn" style="width: 25%; float: right; margin-top: 12px; background: none; border: 0; color: #073e84; font-size: 12px; text-align: right; text-decoration: underline;" class="btn btn-info btn-sm" value="RE-SEND">-->
							</form>
                     </div>

					 <div class="row text-center hidden_div resendFormSec" id="res_otp_div1">  
							<form role="form" id="resendForm" class="medlife_form" action="JavaScript:void(0)" onsubmit="religarehealth_otp_resend('<?php echo SITE_URL?>religare/religare_reset', 1)" >
								 <input type="hidden"  id="res_phone1" name="res_phone1" >
								 <span class="help-block" id="otp_res_err1"></span>
								 <input type="submit" id="res-sbmtbtn1" style="width: 100%; float: right; margin-top: 12px; background: none; border: 0; color: #073e84; font-size: 12px; text-align: right; text-decoration: underline;" class="btn btn-info btn-sm" value="RE-SEND">
							  
							</form>  
                           
                     </div>

					 
                    
                  </div>
               </div>
          
            </div>
         </div>
      </header>
      
      
      
      
      <section id="overview" class="overview">   
         <div class="container">
         <div class="row">
         	<h1 class="text-center spacing">Overview</h1>
         	<div class="col-md-6 spacing">
         		<h3>Our Philosophy</h3>
         		<p>Life is about experiencing every good bit of it throughout one's lifetime; be it a walk in the park with your parents or building memories of playing with your little one. At Religare Health Insurance, we understand that these experiences can be truly rejoiced when one lives a healthy life without having to worry about any unforeseen medical issues.</p>
         	</div>
         	<div class="col-md-6 spacing">
         		<h3>Our Solutions</h3>
         		<p>In line with our objective of ensuring good health…hamesha! Our Comprehensive Health Insurance Plan – 'Care'; helps safeguard you & your family against financial risks arising out of a medical emergency. With 'Care' by your side you can be assured that while you're unwell; we'll take up all the hassles related to your treatment so that you can stay worry free and focus only on your recovery.</p>
         	</div>
         </div> 
		</div>
      </section>
       
       
     <section id="coverage" class="coverage">    
	 <div class="container service"> 
	 <h1 class="text-center">Care Coverage</h1>
		<p class="text-center">A Comprehensive  <strong>Health Insurance Plan</strong> to meet everyone's ( individual and family) healthcare needs.</p>
		<div class="row text-center spacing-1">
		
			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					<img class="img-circle" src="<?php echo S3_URL?>/site/religare-health/religare-images/in-Patient.png" data-holder-rendered="true">

					</div>
					<h4 class="title">In-Patient Care</h4>

				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					<img class="img-circle"   src="<?php echo S3_URL?>/site/religare-health/religare-images/pre-post.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Pre & Post Hospitalization</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/individual-cover.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Individual & Floater Cover</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/hospitalization.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Domiciliary Hospitalization</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/allowance.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Daily Allowance</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/ambulance.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Ambulance Cover</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/organ.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Organ Donar Cover</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/lifelong.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Lifelong Renewability</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/health-checkup.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Annual Health-Checkup</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/bonus.png" data-holder-rendered="true">

					</div>
					<h4 class="title">No Claim Bonus</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/opinion.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Second Opinion</h4>             
				</div>
			</div>

			<div class="col-md-3 col-sm-3">
				<div class="serviceBox">
					<div class="service-icon">
					   <img class="img-circle"  src="<?php echo S3_URL?>/site/religare-health/religare-images/tax.png" data-holder-rendered="true">

					</div>
					<h4 class="title">Tax Benifit</h4>             
				</div>
			</div>
		</div>
	</div>
	</section>
         
         <div class="container">
        		 <div class="row spacing-1">         
         	<div class="col-md-6 bg-dark">
         		<ul>
         		  <li>A Comprehensive  Health Insurance Plan to meet everyone's (individual and family) healthcare needs.</li>
                  <li>From Diagnosis to Recovery - "Care" at every step!</li>
                  <li>30 Day Pre-Hospitalization Coverage</li>
                  <li>Ambulance Cover</li>
                  <li>In-Patient Hospitalization - Hospitalization for at least 24 hours</li>
                  <li>ICU Charges</li>
         		</ul>
         	</div>
         	<div class="col-md-6">
         		<ul>
         		  <li>60-Day Post – Hospitalization Coverage</li>
                  <li>Annual Health Check-up</li>
                  <li>Automatic Recharge of Sum Insured</li>
                  <li>No-Claim Bonus</li>
                  <li>No Upper Age Limit of Enrollment</li>
                  <li>Lifelong Renewability</li> 
         		</ul>
         	</div>
         	</div>
		</div>
        
         <section id="covers" class="covers">          
         <div class="container">
         <div class="row">
         	<h1 class="text-center spacing-1">Add On Covers</h1>
         	<div class="col-md-6">
         		<h3>No Claim Bonus Super</h3>
         		<p>By choosing an optional cover – NCB Super – you can further increase your sum insured amount by 50% for every claim free year up to a maximum of 100%. Hence – in total with both NCB & NCB Super one can increase their sum insured up to 150% in 5 years. </p>
         	</div>
         	<div class="col-md-6">
         		<h3>Everyday Care</h3>
         		<p>Your family may avoid hospitalization for many years at a stretch. However, you have to be really fortunate to avoid doctor consultation for even a few months. For all the visits for all routine visits to the hospital - we give you 1% of your Sum Insured for doctor consultation & diagnostic tests each. This can be availed via a cashless health card and additional premium paid to avail this add- on cover is also applicable for tax benefit under section 80D of IT act. </p>
         	</div>
         		<div class="col-md-6">
         		<h3>Unlimited Automatic Recharge</h3>
         		<p>There should never be a time when you run out of coverage, which is why by selecting this add – on cover your sum insured amount if re-instated in your policy every time your sum insured exhausts. And this can be availed unlimited number of times.</p>
         	</div>
         	<div class="col-md-6">
         		<h3>Air Ambulance Cover</h3>
         		<p>The right cure may be miles away but never out of your reach with Air Ambulance Cover. For all those times when you might be recommended a treatment which is not in your city of stay. </p>
         	</div>
         		<div class="col-md-6">
         		<h3>Personal Accident Cover</h3>
         		<p>Forget the hassles of maintaining two separate policies, one for a health insurance cover & second for personal accident cover. By selecting this add- on cover you get coverage for Accidental Death and Permanent Total Disability for up to 10 times of the Sum Insured opted. </p>
         	</div>
         	<div class="col-md-6">
         		<h3>International Second Opinion</h3>
         		<p>We believe that sometimes reassurance works better than the cure itself. Hence before going ahead with the treatment recommended, we give you an option to consult doctors and take a second opinion.</p>
         	</div>
         </div>

			</div>
			</section>
    
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title" id="myModalLabel">Get a Free Quote in Seconds</h4>
						</div>
						<div class="modal-body">
						<form role="form" id="feedbackForm2" class="religare_form" action="JavaScript:void(0)" 
						onsubmit="religarehealth_jsfrm('<?php echo SITE_URL?>religare/religarefrm','health',2)">
                              <div class="form-group">
                                 <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid_religare('name', 'Please enter correct name')" data-attr="Please enter correct name">
                                 <span class="help-block" id="name_err2"></span>
                              </div>
                               <div class="form-group">
                                 <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid_religare('email', 'Please enter correct email')" data-attr="Please enter correct email">
                                 <span class="help-block" id="email_err2" ></span>
                              </div>

										<div class="form-group" style="margin-bottom:5px">
											<div class="family_div" style="margin-right: 2.5%;">
											   <select class="form-control family" name="family_member" id="family_member2" onchange="show_mem(2)">
												<option value="">Family Members</option>
												<option value="1">1 </option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												<option value="6">6</option>
                              	</select>														
												<span class="help-block" id="family_member_err2"></span>
											</div>
											<div class=" family_div">
											<input type="number" class="form-control clildren" id="children2" name="children" min="0" max="5" placeholder="Children">												
												<span class="help-block" id="children_err2"></span>
											</div>
										</div> 

									   <div class="form-group"> 
											<select class="form-control" name="eldes_member" id="eldes_member2">
												<option value="">Age of Eldest Member</option>
												<option value="18 - 24">18-24 Years</option>
												<option value="25 - 35">25-35 Years</option>
												<option value="36 - 40">36-40 Years</option>
												<option value="41 - 45">41-45 Years</option>
												<option value="46 - 50">46-50 Years</option>
												<option value="51 - 55">51-55 Years</option>
												<option value="56 - 60">56-60 Years</option>
												<option class="ageLimitSelect" value="61 - 65">61-65 Years</option>
												<option class="ageLimitSelect" value="66 - 70">66-70 Years</option>
												<option class="ageLimitSelect" value="71 - 75">71-75 Years</option>
												<option class="ageLimitSelect" value="76 - 99">&gt;75 Years</option>
                              	</select>
                              	<span class="help-block" id="eldes_member_err2"></span>
										</div>


                              <div class="form-group">
                                 <input type="text" class="form-control only_numeric" id="phone2" name="phone" 
											pattern="\d*" maxlength="10" placeholder="Mobile Number" 
											onkeyup="chck_valid_religare('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                                 <span class="help-block" id="phone_err2"> </span>
                              </div> 
                            
                              <div class="form-group"> 
								 <select class="form-control" id="city2" name="city" onchange="chck_valid_religare('city', 'Please select correct city')" 
								 data-attr="Please select city">
								 <option value="" >Select city</option>
								 <?php foreach($city_arr as $city){?>
                                       
                                       <option value="<?php echo $city['city'];?>" ><?php echo $city['city'];?></option>
								 <?php }?>   
                                 </select>
                                 <span class="help-block" id="city_err2"></span>
                                 </div>
							  
                            
                              
                              
                              <input type="submit" id="frm-sbmtbtn2" class="btn btn-primary btn-lg" value="CALL ME BACK">
                           </form>
                           <!-- END CONTACT FORM --> 

                           <div class="row text-center hidden_div" id="otpForm_div2">  
							<form role="form" id="otpForm" class="medlife_form" action="JavaScript:void(0)" onsubmit="religarehealth_otp('<?php echo SITE_URL?>religare/religare_otpverification', '<?php echo SITE_URL?>', 2)" >
							  <input type="text" class="form-control" id="otp2" name="otp" placeholder="Enter OTP" maxlength="4" pattern="\d*">
							  <input type="hidden"  id="otp_phone2" name="otp_phone" >
							  <span class="help-block" id="otp_err2"></span>
							  <input type="submit" id="otpfrm-sbmtbtn2" style="width: 30%; float: left; font-size: 13px" class="btn btn-primary btn-lg" value="VERIFY">
							  <!--<input type="submit" id="res-sbmtbtn" style="width: 25%; float: right; margin-top: 12px; background: none; border: 0; color: #073e84; font-size: 12px; text-align: right; text-decoration: underline;" class="btn btn-info btn-sm" value="RE-SEND">-->
							</form>
                     </div>

					 <div class="row text-center hidden_div resendFormSec" id="res_otp_div2" >  
							<form role="form" id="resendForm" class="medlife_form" action="JavaScript:void(0)" onsubmit="religarehealth_otp_resend('<?php echo SITE_URL?>religare/religare_reset', 2)" >
								 <input type="hidden"  id="res_phone2" name="res_phone2" >
								 <span class="help-block" id="otp_res_err2"></span>
								 <input type="submit" id="res-sbmtbtn2" style="width: 100%; float: right; margin-top: 12px; background: none; border: 0; color: #073e84; font-size: 12px; text-align: right; text-decoration: underline;" class="btn btn-info btn-sm" value="RE-SEND">
							  
							</form>  
                           
                     </div>

                     </div>
					</div>

					 
                     
				</div>
			</div>
    
     <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
            <input type="hidden" id="utm_source" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "
                "); ?>">
            <input type="hidden" id="utm_medium" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : "
                "); ?>">
            <input type="hidden" id="utm_sub" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : "
                "); ?>">
            <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : "
                "); ?>">
    
     
      <footer>
      	© 2019 - Religare Health Insurance. All Rights Reserved
      </footer>
      <!-- jQuery -->
      <!-- Bootstrap Core JavaScript -->
      <script src="<?php echo S3_URL?>/site/scripts/bootstrap.js"></script>
      <!-- Plugin JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
      <!-- Theme JavaScript -->
      <script src="<?php echo S3_URL?>/site/scripts/flatpickr.min.js"></script>
      <script src="<?php echo S3_URL?>/site/scripts/default.js"></script> 
      
      <script>
			setTimeout(function() {
				$('#myModal').modal();
			}, 4000);
		</script>

   </body>
</html>