<?php $all_array = all_arrays(); 

?>
<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Get In Touch |  NMIMS Distance Education</title>      
<link rel="shortcut icon" href="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/favicon.png" type="image/x-icon" />


<link rel="stylesheet" href="<?php echo S3_URL?>/site/nmims-executivemba-assets/css/vendor.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel="stylesheet" href="<?php echo S3_URL?>/site/nmims-executivemba-assets/css/slick.css">    
<link href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo S3_URL?>/site/nmims-executivemba-assets/css/main.css">    
<!-- fonts -->
<link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

<!-- DO NOT MODIFY -->
<!-- Quora Pixel Code (JS Helper) -->
<script>
!function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
qp('init', '0250ea612ac748c6b5516e1e84caa786');
qp('track', 'ViewContent');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/0250ea612ac748c6b5516e1e84caa786/pixel?tag=ViewContent&noscript=1"/></noscript>
<!-- End of Quora Pixel Code --> 
<script>qp('track', 'GenerateLead');</script>


   </head>
   <body> 
	
   <header class="">
         <div class="logo">
            <a href="JavaScript:Void(0);" target=""><img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/p-logo.png"></a>
         </div>
         <div class="clearB"></div>
	  </header>
	  
   <?php $all_array = all_arrays(); ?>   
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="">
					<div class="carousel-caption">
                    <!-- <form role="form" id="feedbackForm" class="first_form feedbackForm" action="JavaScript:void(0)" onsubmit="nmims_jsfrm_executive_mba('<?php echo SITE_URL?>nmims/submit_frm','1')">
                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Make Your Course Enquiry</h4>                           
                            </div> 
                        <div class="col">   
                        <div class="form-group">
                            <input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                            <span class="help-block" id="name_err1"></span>
                        </div>
                   
                        <div class="form-group">
                            <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                            <span class="help-block" id="email_err1" ></span>
                        </div>
                        
                        <div class="form-group">
                            <input type="hidden" class="hiddenCountry" name="CountryCode1" value="91" id="CountryCode1">
                            <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                            <span class="help-block" id="phone_err1"> </span>
                            
                        </div>
                        <div class="form-group">
                           <select name="city" id="city1" class="form-control">
                                                      
                            </select>
							<span class="help-block" id="city_err1"> </span>
                            
                        </div>

                        <div class="form-group">
                           <select name="center" id="center1" class="form-control">
						        <option value="" > Regional Center</option>                                              
                            </select>
							<span class="help-block" id="center_err1"> </span>
                            
                        </div>
                       
                        <div class="form-group radio-form">
                            <div class="custom-radio">
                                <p>
                                    <input type="checkbox" id="agree3" name="agree3" value="agree"  required checked disabled>
                                    <label for="agree3">I authorize NMIMS representative to contact me via phone and/or email. This will override registry on DND/NDNC.</label>
                                </p>
                            </div>    
                        
                                                
                        </div>
                       
                    
                        <div class="submitbtncontainer">
                            <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
                        </div>

                        <p class="disclaimer">All fields are mandatory. T&C and Privacy Policy applies.</p>

                    </div>
                    </div>
                    <div class="otp-section" id ="otp_section1" style="display:none;">
                        <form  action="JavaScript:void(0)" onsubmit="">

                `       <input type="hidden"  id="otp_email1" name="otp_email" value="">
                        <div class="form-group">
                            <input type="text" class="form-control" id="otp1" name="otp" placeholder="OTP"
                                onkeyup="chck_valid('otp', 'Please enter correct otp')"
                                data-attr="Please enter correct otp">
                            <span class="help-block" id="otp_err3"></span>
                        </div>
                        <div class="submitbtncontainer">
                            <input type="submit" id="sbmtbtn3" value="Enter OTP" name="submit">
                        </div>
                        
                        </form>
                        <form  action="JavaScript:void(0)" onsubmit="">
                                <p class="disclaimer">OTP Received to the Entered Mobile Number If Not Received <input type="submit" id="res-sbmtbtn3"  value="Click-Here"></p>
                        </form>
                    </div>
                </form> -->

                
                    </div>
                    
				</div>
			</div>
		</div>

		<!-- The Modal -->
	</div>
    <!--//banner -->    
    
   
    <div class="course-block">
        <div class="container">    
            <div class="row">
                <div class="col-md-6 text-left inn-mba-block">
                    <h1 class="title" class="text-center">MBA (WX)</h1><br>
                    <ul style="list-style: none;">
                        <li class="check1">
                            The Master of Business Administration for Working Executives - MBA (WX) - brings the <span>rigor of a campus</span> executive MBA  program to a power packed <span>online format</span>
                        </li>
                        <li class="check2">
                            <span>120 students per batch: the tailor-made program</span> for ambitious professionals preparing for the next stage of their career encompassing specialised training with intensive online delivery mechanisms 
                        </li>
                        <li class="check2">
                            Allows a student to <span>acquire essential skills</span>, develop a strong professional network, &amp; accelerate their career progression
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 text-right">
                <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/programme_banner.png" alt="" class="img-responsive">   
                </div>
            </div>
        </div>
    </div> 



    <section class="ph-section">
        <div class="ph-wrap">
            <div class="ph-container">
                <h2 class="heading-01 text-center">Program Highlights</h2>
                <ul class="ph-circles">
                    <li>
                        <div class="ph-circle">
                            <div class="ph-text">
                                <h1>15</h1>
                                <h5>MONTHS</h5>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="ph-circle">
                            <div class="ph-text">
                                <h1>5</h1>
                                <h5>TERMS</h5>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="ph-circle">
                            <div class="ph-text">
                                <h1>3</h1>
                                <h5>MONTHS PER TERM</h5>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="ph-circle">
                            <div class="ph-text">
                                <h1>5</h1>
                                <h5>MODULES PER TERM</h5>
                            </div>
                        </div>
                    </li>
                </ul>

                <ul class="ph-2col">
                    <li>
                        <div class="ph-2col-text">
                            <div>
                                <h1>Harvard Business Publishing Certificate</h1>
                                <p>Foundation Course - MBA (WX) includes a Certificate from Harvard Business Publishing in addition to an MBA degree from NMIMS Online University</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="ph-2col-text">
                            <div>
                                <h1>Capstone Project</h1>
                                <p>Final Semester - Includes a Capstone Business Simulation Project that enables the student to synthesise concepts </p>
                            </div>
                        </div>
                    </li>
                </ul>

                <ul class="ph-terms">
                    <li>
                        <div class="ph-th">TERM l</div>
                        <div class="ph-term">Business Management</div>
                    </li>
                    <li>
                        <div class="ph-th">TERM ll</div>
                        <div class="ph-term">Business Management</div>
                    </li>
                    <li>
                        <div class="ph-th">TERM lll</div>
                        <div class="ph-term">Specialisation</div>
                    </li>
                    <li>
                        <div class="ph-th">TERM lV</div>
                        <div class="ph-term">Specialisation</div>
                    </li>
                    <li>
                        <div class="ph-th">TERM V</div>
                        <div class="ph-term">Capstone<br> Project</div>
                    </li>
                </ul>
            </div>
        </div>
    </section>



    <section>
        <div class="programme_structure_section" id="Specialisations">
            <div class="container">					
                <div class="specialisation_block">
                    <h2 class="heading-01">Specialisations</h2>
                    <p>MBA (WX) is the only program of its kind to offer learner's a chance to choose between core &amp; dual specialisations</p>
                    <div class="programme_section">
                        <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                            <ul class="resp-tabs-list">
                                <li class="resp-tab-item1 resp-tab-active" aria-controls="tab_item-0" role="tab">Core Specialisation</li>
                                <li class="resp-tab-item2" aria-controls="tab_item-1" role="tab">Dual Specialisations</li>
                            </ul>
                            <div class="resp-tabs-container">
                                <h2 class="resp-accordion resp-accordion1 resp-tab-active" role="tab" aria-controls="tab_item-0"><span class="resp-arrow"></span>Core Specialisation</h2><div class="resp-tab-content resp-tab-content1 resp-tab-content-active" style="display:block" aria-labelledby="tab_item-0">
                                    <div class="sem01-T">
                                        <div class="cs01">
                                            <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/cs-icon2.png">
                                            <span>A student will choose 6 electives in both Terms III &amp; IV (3 in each Term)</span>
                                        </div>
                                        <div class="cs01">
                                            <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/cs-icon2.png">
                                            <span>The remaining 4 electives (2 in each Term) can be chosen from any track/s</span>
                                        </div>
                                        <i class="clearB"></i>
                                    </div>
                                    <i class="clearB"></i>
                                </div>
                                <h2 class="resp-accordion resp-accordion2" role="tab" aria-controls="tab_item-1"><span class="resp-arrow"></span>Dual Specialisations</h2><div class="resp-tab-content resp-tab-content2" aria-labelledby="tab_item-1">
                                    <div class="sem01-T sem02-T">
                                        <div class="cs01">
                                            <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/cs-icon2.png">
                                            <span>A student will choose 5 electives each from two tracks: Track 1 &amp; Track 2 (across Terms III &amp; IV)</span>
                                        </div>
                                        <div class="cs01">
                                            <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/cs-icon2.png">
                                            <span>A student will choose 3 electives from Track 1 and 2 in Term III </span>
                                        </div>
                                        <div class="cs01">
                                            <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/cs-icon2.png">
                                            <!-- <span>A student will 2 electives from Track 1 and 3 from Track 2 in Term IV</span> -->
                                            <span>A student will choose 2 electives from Track 1 and 3 electives from Track 2 in Term IV</span>
                                        </div>
                                        <i class="clearB"></i>							
                                    </div>
                                    <i class="clearB"></i>
                                </div>
                            </div>
                        </div>
                        <i class="clearB"></i>
                    </div>
                </div><!--Specialization-->
                <div class="track">
                    <h2 class="heading-01">Specialisation Tracks</h2>
                    <p>The third and fourth terms are specialisation terms. The student can tailor his/her MBA by
choosing one of four specialisations</p>
                    <div class="special_block">
                        <ul>
                            <li>
                                <div>
                                    <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/mbam_icon.png">
                                    <span>Marketing</span>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/mba_leadership_icon.png">
                                    <span>Leadership &amp; Strategy</span>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/mba_supply_chain.png">
                                    <span>Operations &amp; Supply Chain</span>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/mba_applied.png">
                                    <span>Applied Finance</span>
                                </div>
                            </li>
                        </ul>
                        <i class="clearB"></i>
                    </div>
                </div>
            </div>
            <div class="ST-bg">
                <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/team-bg.png">
            </div>				
        </div><!--Programme Structure-->
    </section>


	<section class="section04 webfaculty">			
			<div class="faculty_structure" id="Faculty">
				<div class="container">
					<div class="faculty_block">
						<h2 class="heading-01">Faculty</h2>
						<p>Leading business thinkers from academia and industry, to give you dynamic mix of theory and business practices*</p>
						<div style="margin-top: 50px;">
						<ul class="bxslider">
							<li>
								<div class="faculty-info">
									<div class="fc_block">
										<div class="fc_social_link">
											<a href="https://in.linkedin.com/in/dimple-grover-6993462" target="_blank" tabindex="-1"><img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/linkedin.png"></a>
										</div>
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/dimple-grover.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="faculty_info">
										<h1>Dr. Dimple Grover</h1>
										<p>Ph. D. (IIT Delhi), M. Phil. (Delhi University)</p>
									</div>
								</div>


								<div class="faculty-info">
									<div class="fc_block">
										<div class="fc_social_link">
											<a href="https://in.linkedin.com/in/amal-roy-685bb347" target="_blank" tabindex="-1"><img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/linkedin.png"></a>
										</div>
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/amal-kumar-roy.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="faculty_info">
										<h1>Prof. Amal Sankar Roy</h1>
										<p>M Tech. (IIT Kharagpur) &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp; &nbsp;  &nbsp;  &nbsp;  &nbsp; </p>
									</div>
								</div>

								<div class="faculty-info">
									<div class="fc_block">
										<div class="fc_social_link">
											<a href="javascript:void(0)" tabindex="-1"><img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/linkedin.png"></a>
										</div>
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/mukesh-chaturvedi.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="faculty_info">
										<h1>Dr. Mukesh Chaturvedi </h1>
										<p>Ph.D. (BITS, Pilani), M.M.S (BITS, Pilani)</p>
									</div>
								 </div>


							</li>

							<li>
								<div class="faculty-info">
									<div class="fc_block">
										<div class="fc_social_link">
											<a href="javascript:void(0)" tabindex="-1"><img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/linkedin.png"></a>
										</div>
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/rohit-kumar.png">
									    </div>										
										<i class="clearB"></i>
									</div>
									<div class="faculty_info">
										<h1>Prof. Rohit Kumar Gautam</h1>
										<p>PGDM (IIM Ahmedabad),  B. Tech. (IIT Bombay)</p>
									</div>
								 </div>

								 <div class="faculty-info">
									<div class="fc_block">
										<div class="fc_social_link">
											<a href="https://www.linkedin.com/in/tanmoy-adhikary-8132002" target="_blank" tabindex="-1"><img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/linkedin.png"></a>
										</div>
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/tanmoy-adhikary.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="faculty_info">
										<h1>Prof. Tanmoy Adhikary</h1>
										<p>PGDM (IIM Ahmedabad), M.Sc. (IIT Kharagpur)</p>
									</div>
								</div>

								<div class="faculty-info">
									<div class="fc_block">
										<div class="fc_social_link">
											<a href="https://in.linkedin.com/in/nora-bhatia-11851491" target="_blank" tabindex="-1"><img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/linkedin.png"></a>
										</div>
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/nora-bhatia.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="faculty_info">
										<h1>Prof. Nora Bhatia</h1>
										<p>MBA (NMIMS Mumbai). Chief HR - Mahindra Intertrade</p>
									</div>
								</div>

							</li>

					

							</ul>
						</div>

					</div>

				</div>

			</div>


	</section>



	
	<section class="section04 mobilefaculty">			
			<div class="faculty_structure" id="Faculty">
				<div class="container">
					<div class="faculty_block">
						<h2 class="heading-01">Faculty</h2>
						<p>Leading business thinkers from academia and industry, to give you dynamic mix of theory and business practices*</p>
						<div>
						
								<div class="mobile-faculty-info">
									<div class="fc_block">
										<div class="fc_social_link">
											<a href="https://in.linkedin.com/in/dimple-grover-6993462" target="_blank" tabindex="-1"><img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/linkedin.png"></a>
										</div>
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/dimple-grover.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="mobile-faculty_info">
										<h1>Dr. Dimple Grover</h1>
										<p>Ph. D. (IIT Delhi), M. Phil. (Delhi University)</p>
									</div>
								</div>


								<div class="mobile-faculty-info">
									<div class="fc_block">
										<div class="fc_social_link">
											<a href="https://in.linkedin.com/in/amal-roy-685bb347" target="_blank" tabindex="-1"><img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/linkedin.png"></a>
										</div>
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/amal-kumar-roy.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="mobile-faculty_info">
										<h1>Prof. Amal Sankar Roy</h1>
										<p>M Tech. (IIT Kharagpur)</p>
									</div>
								</div>

								<div class="mobile-faculty-info">
									<div class="fc_block">
										<div class="fc_social_link">
											<a href="javascript:void(0)" tabindex="-1"><img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/linkedin.png"></a>
										</div>
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/mukesh-chaturvedi.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="mobile-faculty_info">
										<h1>Dr. Mukesh Chaturvedi </h1>
										<p>Ph.D. (BITS, Pilani), M.M.S (BITS, Pilani)</p>
									</div>
								 </div>


							
								<div class="mobile-faculty-info">
									<div class="fc_block">
										<div class="fc_social_link">
											<a href="javascript:void(0)" tabindex="-1"><img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/linkedin.png"></a>
										</div>
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/rohit-kumar.png">
									    </div>										
										<i class="clearB"></i>
									</div>
									<div class="mobile-faculty_info">
										<h1>Prof. Rohit Kumar Gautam</h1>
										<p>PGDM (IIM Ahmedabad),  B. Tech. (IIT Bombay)</p>
									</div>
								 </div>

								 <div class="mobile-faculty-info">
									<div class="fc_block">
										<div class="fc_social_link">
											<a href="https://www.linkedin.com/in/tanmoy-adhikary-8132002" target="_blank" tabindex="-1"><img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/linkedin.png"></a>
										</div>
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/tanmoy-adhikary.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="mobile-faculty_info">
										<h1>Prof. Tanmoy Adhikary</h1>
										<p>PGDM (IIM Ahmedabad), M.Sc. (IIT Kharagpur)</p>
									</div>
								</div>

								<div class="mobile-faculty-info">
									<div class="fc_block">
										<div class="fc_social_link">
											<a href="https://in.linkedin.com/in/nora-bhatia-11851491" target="_blank" tabindex="-1"><img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/linkedin.png"></a>
										</div>
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/nora-bhatia.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="mobile-faculty_info">
										<h1>Prof. Nora Bhatia</h1>
										<p>MBA (NMIMS Mumbai). Chief HR - Mahindra Intertrade</p>
									</div>
								</div>

						</div>

					</div>

				</div>

			</div>


	</section>


        <section class="section02">
                <div class="market_strategies_section">			
                    <div class="gm-section">
                        <div class="gm-mid-block wow zoomIn">
                            <div class="midd-img-sec">
                                <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/speacial_feature_bg.png">
                                <div class="ygj_title"><h3 class="heading-01">Why<span class="brown-color">MBA (WX)</span></h3></div>
                            </div>
                        </div>
                        <div class="gm-left-block">
                            <ul>
                                <li class="wow zoomIn">
                                    <div class="ygj_circle">
                                        <div class="inn_circle align-right">
                                            <p><span class="red-color">Each batch to comprise of select 120 students:</span><br>
                                            <span class="bulet"></span> Encourage meaningful interactions with fellow students<br>
                                            <span class="bulet"></span> Allows faculty to effectively optimise interactivity during live sessions</p> 
                                        </div>
                                    </div>
                                </li>
                                <li class="wow zoomIn">
                                    <div class="ygj_circle">
                                        <div class="inn_circle align-right">
                                            <p><span class="red-color">Choose from core &amp; dual specialisation</span> <br>
                                            <span class="bulet"></span> 4 specialisation to tackle business requirements of emerging markets<br>
                                            <span class="bulet"> </span> Option to master two separate fields of study</p>
                                        </div>
                                    </div>
                                </li>

                                <li class="wow zoomIn"> 
                                    <div class="ygj_circle">
                                        <div class="inn_circle">
                                            <p><span class="red-color">Harvard Business Publishing Certification</span><br>
                                            <span class="bulet"> </span> Access entire range of global and India centric content<br>
                                            <span class="bulet"> </span> 6 Online Certificates &amp; 1 Final Certificate</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="wow zoomIn">
                                    <div class="ygj_circle">
                                        <div class="inn_circle">
                                            <p><span class="red-color">Knowledge-Skill-Attitude (KSA) framework</span><br>
                                            <span class="bulet"></span> Designed to enhance the student's capability to face
                                            Volatility Uncertainty-Complexity-Ambiguousness
                                            (VUCA) in their business set-up</p>
                                        </div>
                                    </div>
                                </li>
                                
                                <i class="clearB"></i>
                            </ul>
                            <i class="clearB"></i>
                        </div>
                    </div>
                    <div class="ygj-bg">
                        <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/growth_bg.png">
                    </div>
                </div>
            </section>




			





               
    <section class="Section05" id="Admissions">
			<div class="admission_section">
				<div class="container">
					<div class="admission_block">
						<h2 class="heading-01">Admissions</h2>
						<div class="main_admission">
							<div class="Left_admission_block">
								<div class="programme_duration_block">
									<ul>
										<li class="program_duration">
											<span>Duration of the Program:</span>
											<h3>15 Months</h3>
										</li>
									</ul>
								</div>
								<!--<div class="programme_validity_block">
									<ul>
										<li class="program_validity">
											<span>Program Validity:</span>
											<h3>24 Months</h3>
										</li>
									</ul>
								</div>-->
								<div class="student_eligibility_section">
									<h3>Student Eligibility</h3>
									<ul style="list-style-type:square;">
										<li>Bachelor’s Degree (10+2+3) in any discipline from a recognised university with a minimum 50% score and 2 years of work experience

										</li><li>Post Graduation / Professional Degree with a minimum 50% score and 1 year of work experience</li>
										<!--<li>Bachelor’s Degree in any discipline from recognised Universities with a minimum of 50% and 2 years of work experience.</li>
										<li>Post Graduation / Professional Degree with a minimum of 50% and 1 year of work experience.</li>-->
										<!--<li>Bachelor’s Degree in any discipline from a recognised University with minimum 50% marks and preferably 2 years of work experience.</li>
										<li>Post-Graduation / Professional Degree from a recognised University with minimum 50% marks and preferably 1 year of Work Experience.</li>
										<li>Bachelor’s Degree in any discipline from a recognised university with minimum 50% and preferably 2 years of work experience.</li>
										<li>Bachelor’s Degree in any discipline from a recognised university with minimum 50% and post-graduation/professional degree with minimum 50% and preferably 1 year of work experience.</li>
										<li>Bachelor’s Degree in any discipline from recognized Universities with minimum 50% marks and preferably 2 years of Work Experience.</li>
										<li>Post-Graduation / Professional Degree with minimum 50% marks and preferably 1 year of Work Experience.</li>
										<li>Founder/Co-founder of a company or startup with graduation from a recognised university</li>-->
									</ul>
								</div>
							</div>
							<div class="Right_admission_block">
								<img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/admission_right_img.jpg">
							</div>
						 <i class="clearB"></i>
						</div>
					</div>
				</div><!--Container-->
				 <i class="clearB"></i>
				 
				 <div class="admission-bg">
					<img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/admission_bg.png">
				</div>
			</div>
		</section>




        <section class="section03">
			<div class="feature_section">
				<div class="feature_block">
					<div class="feature_img">
						<img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/feature_bg.png">
						<div class="feature_title">
							<h3 class="heading-01">Added <span class="brown-color">Advantage</span></h3>						
						</div>
					</div>	
					<div class="feature_information">
						<ul>
							<li class="FL1">
								<div class="feature_box fb_L sm-feature_block L1">
									<div class="featurebox_img" style="float:right"><img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/live_classroom_icon.png"></div>
									<div class="feature_text a-right">
										<h3>Live Online Classroom</h3>
										Lectures are delivered live online through the university's virtual learning environment where students have access to teaching materials and can communicate with faculty members and fellow students. Moreover, live sessions are recorded &amp; made available for the ease of students
									</div>									
								</div>
							</li>
							
							<li class="FR1">
								<div class="feature_box fb_R sm-feature_block R1">
									<div class="featurebox_img"><img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/online_library_icon.png"></div>
									<div class="feature_text a-left">
										<h3>Assessment System</h3> 									
										Computer-based assignments, 70 marks:projects, MCQs, presentation. 30 marks: descriptive questions. Convenient exam schedules
									</div>								
								</div>
							</li>	
							<i class="clearB"></i>
						</ul>
						<ul>
							<li class="FL2">
								<div class="feature_box fb_L L2">
									<div class="featurebox_img" style="float:right"><img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/peer_learning_icon.png"></div>
									<div class="feature_text text a-right">
										<h3>Peer Learning &amp; Interactions</h3>
										The Learning management system is designed for the desktop and mobile facilitate opportunities to learn from the cohort &amp; faculty. Students' can interact with their peers who bring a diverse range of skills and professional experience to the classroom
									</div>								
								</div>
							</li>
							<li class="FR2">
								<div class="feature_box fb_R R2">
									<div class="featurebox_img"><img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/continious_learning_icon.png"></div>
									<div class="feature_text a-left">
										<h3>Continuous Learning <!--& Assessment--></h3>
										This approach allows tracking of progress. It provides a platform to offer students support, guidance, &amp; opportunities to improve during the course or program
									</div>									
								</div>								
							</li>
							<i class="clearB"></i>
						</ul>
						<i class="clearB"></i>
					</div>					
				</div>
				<i class="clearB"></i>
				<div class="feature_icon-bg">
					<img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/feature_icon_bg.png">
				</div>
			</div>
			
		</section>








    <div class="nmims">
        <div class="container">
        <h2 class="title">About NMIMS</h2>
        <p>
        Founded from the iconic SVKM's NMIMS (deemed-to-be-university), we are NMIMS Global Access School for Continuing Education, India's #1 online management university. From being the country's premiere distance-learning establishment of the 20th century to crafting a 21st century ready online education platform, our journey has been dynamic and gratifying. Thousands of students have graduated from NGA-SCE and are now fulfilling their hopes and dreams
        </p>
         <br><br><br>
        <h2 class="title">NMIMS Edge</h2>
                <div class="col-md-6 text-left ">
                    <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/edge_img.jpg" alt="" class="img-responsive">   
                </div>
                <div class="col-md-6 Right_edge_block">
                <ul>
								<li>
									<div class="edge_icon">
                                    <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/accredited_naac_img.png">
									</div>
									<div class="edge_text">
										Accredited by NAAC with Grade A+ 
									</div>
								</li>
								<li>
									<div class="edge_icon">
                                    <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/granted_autonomy_icon.png">
									</div>
									<div class="edge_text">
										Granted Autonomy Category 1 by UGC
									</div>
								</li>
								<li>
									<div class="edge_icon">
                                    <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/regional_centre_icon.png">
									</div>
									<div class="edge_text">
										9 regional offices across India
									</div>
								</li>
								<li>
									<div class="edge_icon">
                                    <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/flexible_fee_icon.png">
									</div>
									<div class="edge_text">
										Flexible Fee Payment options with EMI facility
									</div>
								</li>
								
								<li>
									<div class="edge_icon">
                                    <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/help_desk_img.png">
									</div>
									<div class="edge_text">
										Dedicated student support team to manage all queries
									</div>
								</li>
							</ul>
                </div>
                 
        </div>
    </div>

 


    <footer>
        <!-- <div class="disclaimerSection" style="background-color: #161d2d;">
            <div class="container disclaimer">
                <p>
                    
                </p>
            </div>
        </div> -->

		<div class="copy-right-grids">
            <div class="container">
                <p class="footer-gd"><b>Disclaimer: </b> This page is a property of Adcanopus, a marketing affiliate of NMIMS Online University. All activity on this page is limited to providing information on Master of Business Administration 
                        for Working Executives program.
                </p>
            </div>
			
		</div>

	</footer>    
	<div class="floating-form visiable" id="contact_form">
	<div class="contact-opener">Enquire Now</div>
        <form role="form" id="feedbackForm" class="second_form feedbackForm" action="JavaScript:void(0)" onsubmit="nmims_jsfrm_executive_mba('<?php echo SITE_URL?>nmims/submit_frm','2')">
                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Make Your Course Enquiry</h4>                           
                            </div> 
                        <div class="col">   
                        <div class="form-group">
                            <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                            <span class="help-block" id="name_err2"></span>
                        </div>
                        
                    <div class="form-group">
                        <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                        <span class="help-block" id="email_err2" ></span>
                    </div>
                    
                    <div class="form-group">
                        <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode2">
                        <input type="text" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="phone_err2"> </span>
                        
                    </div>
                    <div class="form-group">
                        <select name="city" id="city2" class="form-control">                                                                                                  
                            
                            <option value="" > City</option>
                                                        
                        </select>
                        <span class="help-block" id="city_err2"> </span>
                        
                    </div>

                    <div class="form-group">
                        <select name="city" id="center2" class="form-control">
                            <option value="" >  Regional Center</option>                          
                        </select>
                        <span class="help-block" id="regional_center_err3"> </span>
                    </div>

                    <div class="form-group radio-form">
                        <div class="custom-radio">
                            <p>
                                <input type="checkbox" id="agree3" name="agree3" value="agree"  required checked disabled>
                                <label for="agree3">I authorize NMIMS representative to contact me via phone and/or email. This will override registry on DND/NDNC.</label>
                            </p>
                        </div>                        
                    </div>

                  
                                         
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>

                    <p class="disclaimer">All fields are mandatory. T&C and Privacy Policy applies.</p>

                </div>
            </div>

            <!--<div class="otp-section" id ="otp_section2" style="display:none;">
                <form  action="JavaScript:void(0)" onsubmit="">

                <input type="hidden"  id="otp_email2" name="otp_email" value="">
                <div class="form-group">
                    <input type="text" class="form-control" id="otp2" name="otp" placeholder="OTP"
                        onkeyup="chck_valid('otp', 'Please enter correct otp')"
                        data-attr="Please enter correct otp">
                    <span class="help-block" id="otp_err3"></span>
                </div>
                <div class="submitbtncontainer">
                    <input type="submit" id="sbmtbtn3" value="Enter OTP" name="submit">
                </div>
                
                </form>
                <form  action="JavaScript:void(0)" onsubmit="">
                        <p class="disclaimer">OTP Received to the Entered Mobile Number If Not Received <input type="submit" id="res-sbmtbtn3"  value="Click-Here"></p>
                </form>
            </div>-->

        </form>	
    </div>

    <div class="popup-enquiry-form mfp-hide" id="popupForm">
        <form role="form" id="feedbackForm" class="third_form feedbackForm" action="JavaScript:void(0)" onsubmit="nmims_jsfrm_executive_mba('<?php echo SITE_URL?>nmims/submit_frm','3')">                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/nmims-executivemba-assets/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Make Your Course Enquiry</h4>                           
                            </div> 
                        <div class="col">   
                        <div class="form-group">
                            <input type="text" class="form-control" id="name3" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                            <span class="help-block" id="name_err3"></span>
                        </div>
              
                    <div class="form-group">
                        <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                        <span class="help-block" id="email_err3" ></span>
                    </div>
                    
                    <div class="form-group">
                        <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode3">
                        <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="phone_err3"> </span>
                       
                    </div>
                    <div class="form-group">
                        <select name="city" id="city3" class="form-control">
                            <option value="" > City</option>                          
                        </select>
                        <span class="help-block" id="city_err3"> </span>
                    </div>

                    <div class="form-group">
                        <select name="center" id="center3" class="form-control">
                            <option value="" >  Regional Center</option>                          
                        </select>
                        <span class="help-block" id="regional_center_err3"> </span>
                    </div>

                    <div class="form-group radio-form">
                        <div class="custom-radio">
                            <p>
                                <input type="checkbox" id="agree3" name="agree3" value="agree"  required checked disabled>
                                <label for="agree3">I authorize NMIMS representative to contact me via phone and/or email. This will override registry on DND/NDNC.</label>
                            </p>
                        </div>    
                    
                        <span class="help-block" id="cource_err3"> </span>                           
                    </div>
                                          
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>

                    <p class="disclaimer">All fields are mandatory. T&C and Privacy Policy applies.</p>

                </div>
            </div>
            <!--<div class="otp-section" id ="otp_section3" style="display:none;">
                <form  action="JavaScript:void(0)" onsubmit="">

                <input type="hidden"  id="otp_email3" name="otp_email" value="">
                <div class="form-group">
                    <input type="text" class="form-control" id="otp3" name="otp" placeholder="OTP"
                        onkeyup="chck_valid('otp', 'Please enter correct otp')"
                        data-attr="Please enter correct otp">
                    <span class="help-block" id="otp_err3"></span>
                </div>
                <div class="submitbtncontainer">
                    <input type="submit" id="sbmtbtn3" value="Enter OTP" name="submit">
                </div>
                
                </form>
                <form  action="JavaScript:void(0)" onsubmit="">
                        <p class="disclaimer">OTP Received to the Entered Mobile Number If Not Received <input type="submit" id="res-sbmtbtn3"  value="Click-Here"></p>
                </form>
            </div>-->
        </form>
    </div>
                                    



	<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
	<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
	<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
	<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
	<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
	
	
	<script src="<?php echo S3_URL?>/site/nmims-executivemba-assets/js/vendor.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script> 
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>

    <script src="<?php echo S3_URL?>/site/nmims-executivemba-assets/js/slick.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.js"></script>

    <script src="<?php echo S3_URL?>/site/nmims-executivemba-assets/js/main.js"></script>
	
	 <script src="<?php echo S3_URL?>/site/scripts/nmims.js"></script>
<script>


$(document).ready(function(){
	var second_statearr = []; var second_sstate = '' ; var second_data ; 
	setTimeout(function(){
		
		$.ajax({
			url:'<?php echo S3_URL?>/site/nmims-executivemba-assets/json/cities.json?v=0.1',
			success:function(res){
                second_data = res ;
			                 console.log('Respose');
                
			   for(i=0; i<second_data.length; i++){
                second_statearr.push(second_data[i].state);
			   }
			   $('.second_form').find('#city2').append('<option value="">Select city</option>');
			   second_statearr.sort();
			   second_statearr = $.unique( second_statearr );

			   $.each(second_statearr, function(key, state){
				  $('.second_form').find('#city2').append('<option value="'+state+'">'+state+'</option>');
			   });
               second_sstate = second_statearr[0];
             
			   change_second_city(second_sstate);
			}
			
		 });
	},100);
});

var second_statearr = []; var second_sstate = '' ; var second_data ; 

$('#city2').on('change', function () {
    $.ajax({
			url:'<?php echo S3_URL?>/site/nmims-executivemba-assets/json/cities.json?v=0.1',
			success:function(res){
                second_data = res ;
			    console.log('Respose');
                
			   for(i=0; i<second_data.length; i++){
                second_statearr.push(second_data[i].state);
			   }
			   $('.second_form').find('#city2').append('<option value="">Select city</option>');
			   second_statearr.sort();
			   second_statearr = $.unique( second_statearr );

			   $.each(second_statearr, function(key, state){
				  $('.second_form').find('#city2').append('<option value="'+state+'">'+state+'</option>');
			   });
               second_sstate = second_statearr[0];
             
			   change_second_city(second_sstate);
			}
			
		 });
});

function change_second_city(state){
    var citys =$('#city2').val();
 
	if(citys.trim()==''){
		$('.second_form').find('#center2').append('<option value="">Select center</option>');
	}else{
		$('.second_form').find('#center2').html('');
		$('.second_form').find('#center2').append('<option value="">Select center</option>');
	for(i=0; i<second_data.length; i++){
    	if(citys == second_data[i].state){
    		$('.second_form').find('#center2').append('<option value="'+second_data[i].name+'">'+second_data[i].name+'</option>');
    	}
	}}
}

</script>



<script>


$(document).ready(function(){
	var statearr = []; var sstate = '' ; var data ; 
	setTimeout(function(){
		
		$.ajax({
			url:'<?php echo S3_URL?>/site/nmims-executivemba-assets/json/cities.json?v=0.1',
			success:function(res){
			   data = res ;
			                 console.log('Respose');
                
			   for(i=0; i<data.length; i++){
				  statearr.push(data[i].state);
			   }
			   $('.first_form').find('#city1').append('<option value="">Select city</option>');
			   statearr.sort();
			   statearr = $.unique( statearr );

			   $.each(statearr, function(key, state){
				  $('.first_form').find('#city1').append('<option value="'+state+'">'+state+'</option>');
			   });
               sstate = statearr[0];
             
			   changecity(sstate);
			}
			
		 });
	},100);
});

var statearr = []; var sstate = '' ; var data ; 

$('#city1').on('change', function () {


        $.ajax({
            url:'<?php echo S3_URL?>/site/nmims-executivemba-assets/json/cities.json?v=0.1',
            success:function(res){
               data = res ;
               console.log('Respose');
                console.log(data);

               for(i=0; i<data.length; i++){
                  statearr.push(data[i].state);
               }
              
               statearr.sort();
               statearr = $.unique( statearr );
               
               sstate = statearr;
             
               changecity(sstate);
            }
            
         });
});

function changecity(state){
    var citys =$('#city1').val();
 
	if(citys.trim()==''){
		$('.first_form').find('#center1').append('<option value="">Select center</option>');
	}else{
		$('.first_form').find('#center1').html('');
		$('.first_form').find('#center1').append('<option value="">Select center</option>');
	for(i=0; i<data.length; i++){
    	if(citys == data[i].state){
    		$('.first_form').find('#center1').append('<option value="'+data[i].name+'">'+data[i].name+'</option>');
    	}
	}}
}

</script>






<script>


$(document).ready(function(){
	var statearr_third = []; var sstate_third = '' ; var data_third ; 
	setTimeout(function(){
		
		$.ajax({
			url:'<?php echo S3_URL?>/site/nmims-executivemba-assets/json/cities.json?v=0.1',
			success:function(res){
                data_third = res ;
			                 console.log('Respose');
                
			   for(i=0; i<data_third.length; i++){
                statearr_third.push(data_third[i].state);
			   }
			   $('.third_form').find('#city3').append('<option value="">Select city</option>');
			   statearr_third.sort();
			   statearr_third = $.unique( statearr_third );

			   $.each(statearr_third, function(key, state){
				  $('.third_form').find('#city3').append('<option value="'+state+'">'+state+'</option>');
			   });
               sstate_third = statearr_third[0];
             
			   changethird_city(sstate_third);
			}
			
		 });
	},100);
});

var statearr_third = []; var sstate_third = '' ; var data_third ;  

$('#city3').on('change', function () {


    $.ajax({
			url:'<?php echo S3_URL?>/site/nmims-executivemba-assets/json/cities.json?v=0.1',
			success:function(res){
                data_third = res ;
			                 console.log('Respose');
                
			   for(i=0; i<data_third.length; i++){
                statearr_third.push(data_third[i].state);
			   }
			   $('.third_form').find('#city3').append('<option value="">Select city</option>');
			   statearr_third.sort();
			   statearr_third = $.unique( statearr_third );

			   $.each(statearr_third, function(key, state){
				  $('.third_form').find('#city3').append('<option value="'+state+'">'+state+'</option>');
			   });
               sstate_third = statearr_third[0];
             
			   changethird_city(sstate_third);
			}
			
		 });
});

function changethird_city(state){
    var citys =$('#city3').val();
 
	if(citys.trim()==''){
		$('.third_form').find('#center3').append('<option value="">Select center</option>');
	}else{
		$('.third_form').find('#center3').html('');
		$('.third_form').find('#center3').append('<option value="">Select center</option>');
	for(i=0; i<data_third.length; i++){
    	if(citys == data_third[i].state){
    		$('.third_form').find('#center3').append('<option value="'+data_third[i].name+'">'+data_third[i].name+'</option>');
    	}
	}}
}

</script>
<script>

$(document).ready(function(){
  $('.bxslider').bxSlider();
});


</script>

<script>
	$(".resp-tab-item2").click(function(){
		$(this).addClass("resp-tab-active");
		$(".resp-tab-item1").removeClass("resp-tab-active");

		$(".resp-tab-content2").addClass("resp-tab-content-active");
		$(".resp-tab-content2").show();

		$(".resp-tab-content1").removeClass("resp-tab-content-active");
		$(".resp-tab-content1").hide();
		
	});

	$(".resp-tab-item1").click(function(){
		$(this).addClass("resp-tab-active");
		$(".resp-tab-item2").removeClass("resp-tab-active");
		
		$(".resp-tab-content1").addClass("resp-tab-content-active");
		$(".resp-tab-content1").show();

		$(".resp-tab-content2").removeClass("resp-tab-content-active");
		$(".resp-tab-content2").hide();
	});


</script>




    </body>
</html>