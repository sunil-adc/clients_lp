<?php $all_array = all_arrays(); 

?>
<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Get In Touch |  NMIMS Distance Education</title>      
<link rel="shortcut icon" href="<?php echo S3_URL?>/site/nmims-assets/images/favicon.png" type="image/x-icon" />


<link rel="stylesheet" href="<?php echo S3_URL?>/site/nmims-assets/css/vendor.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel="stylesheet" href="<?php echo S3_URL?>/site/nmims-assets/css/main.css">    
<!-- fonts -->
<link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

<!-- DO NOT MODIFY -->
<!-- Quora Pixel Code (JS Helper) -->
<script>
!function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
qp('init', '0250ea612ac748c6b5516e1e84caa786');
qp('track', 'ViewContent');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/0250ea612ac748c6b5516e1e84caa786/pixel?tag=ViewContent&noscript=1"/></noscript>
<!-- End of Quora Pixel Code --> 
<script>qp('track', 'GenerateLead');</script>


   </head>
   <body> 
   <?php $all_array = all_arrays(); ?>   
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="">
					<div class="carousel-caption">
                    <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="nmims_jsfrm('<?php echo SITE_URL?>nmims/submit_frm','1')">
                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Make Your Course Enquiry</h4>                           
                            </div> 
                        <div class="col">   
                        <div class="form-group">
                            <input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                            <span class="help-block" id="name_err1"></span>
                        </div>
                   
                        <div class="form-group">
                            <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                            <span class="help-block" id="email_err1" ></span>
                        </div>
                        
                        <div class="form-group">
                            <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode1">
                            <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                            <span class="help-block" id="phone_err1"> </span>
                            
                        </div>
                        <div class="form-group">
                           <select name="city" id="city1" class="form-control">
                                                                                                     
                                
						        <option value="" > City</option>
				                <?php foreach($all_array['NMIMS_CITY'] as $k=>$v){ ?>
										<option value="<?php echo $k;?>" > <?php echo $v;?></option>
				               <?php }?>                                                                      
                            </select>
							<span class="help-block" id="city_err1"> </span>
                            
                        </div>
                       
                        <div class="form-group radio-form">
                            <label class="title-label">Course</label>
                            <div class="custom-radio">
                                 <p>
                                    <input type="radio" id="c11" name="cource1" value="Post Graduate Diploma - 2 yr "  checked>
                                    <label for="c11">Post Graduate Diploma (2 yr)</label>
                                </p>
                                <p>
                                    <input type="radio" id="c12" name="cource1" value="Diploma - 1 yr">
                                    <label for="c12"> Diploma (1 yr)</label>
                                </p>
                                <p>
                                    <input type="radio" id="c13" name="cource1" value="Professional Programs - 1 year">
                                    <label for="c13">Professional Programs (1 year)</label>
                                </p>
                                <p>
                                    <input type="radio" id="c14" name="cource1" value="Certificate - 6 months">
                                    <label for="c14">Certificate (6 months)</label>
                                </p>
                            </div>    
                         
							<span class="help-block" id="cource_err1"> </span>                           
                        </div>
                       
                      
                    <div class="submitbtncontainer">
                    <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
                    </div>
                    </div>
                    </div>
                </form>
					</div>
				</div>
			</div>
		</div>

		<!-- The Modal -->
	</div>
    <!--//banner -->    
    
   
    <div class="course-block">
        <div class="container">    
            <div class="row">
                <div class="col-md-6 text-left">
                    <h3 class="title" class="text-center">Programs that propel</h3><br>
                    <p>Take the leap towards the next stage of your career with programs that have been thoughtfully designed for working professionals. They are structured and scheduled to allow for a fulfilling work life & study balance. Depending on the need and depth of upskilling, students have an option to choose from specialisations within following: Certificate programs, Diploma Programs, Professional Programs, PG Diploma Programs and Master Programs.</p>
                    <br>
                </div>
                <div class="col-md-6 text-right">
                <img src="<?php echo S3_URL?>/site/nmims-assets/images/programme_banner.jpg" alt="" class="img-responsive">   
                </div>
            </div>
        </div>
    </div> 



    <div class="course-block">
    <div class="container">
    <h3 class="title">Programs</h3>
    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="pricingTable">
                <h3 class="title">Post Graduate Diploma Programs</h3>
                <div class="price-value"><i class="fa fa-graduation-cap" aria-hidden="true"></i></div>
                <ul class="pricing-content"> 
                        <li>Post Graduate Diploma in Business Management <span>(PGDBM)</span></li>
                        <li>PGDBM <span>(Marketing Management)</span></li>
                        <li>PGDBM <span>(Financial Management)</span></li>
                        <li>PGDBM <span>(Human Resource Management)</span></li>
                        <li>PGDBM <span>(International Trade Management)</span></li>
                        <li>PGDBM <span>(Banking and Finance Management)</span></li>
                        <li>PGDBM <span>(Supply Chain Management)</span></li>
                        <li>PGDBM <span>(Operations Management)</span></li>
                        <li>PGDBM <span>(Retail Management)</span></li>
                        <li>PGDBM <span>(Information Technology and Systems Management)</span></li> 
                </ul>
                <a href="#" class="pricingTable-signup">Enquire Now</a>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="pricingTable green">
                <h3 class="title">Diploma Programs</h3>
                <div class="price-value"><i class="fa fa-file" aria-hidden="true"></i></div>
                <ul class="pricing-content">
                        <li>Business Management </li>
						<li>Marketing Management </li>
						<li>Financial Management </li>
						<li>Human Resource Management</li>
						<li>International Trade Management</li>
						<li>Banking and Finance Management</li>
						<li>Supply Chain Management</li>
                        <li>Retail Management</li>
                        <li>Operations Management </li> 

                </ul>
                <a href="#" class="pricingTable-signup">Enquire Now</a>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="pricingTable blue">
                <h3 class="title">Certificate Programs</h3>
                <div class="price-value"><i class="fa fa-certificate" aria-hidden="true"></i></div>
                <ul class="pricing-content"> 
                    <li>IT Management</li>
                    <li>Operations Management</li>
                    <li>Project Management</li>
                    <li>Corporate Communication</li> 
                    <li>Business Management</li> 
                </ul>
                
                <a href="#" class="pricingTable-signup">Enquire Now</a>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="pricingTable red">
                <h3 class="title">Professional Programs</h3>
                <div class="price-value"><i class="fa fa-certificate" aria-hidden="true"></i></div>
                <ul class="pricing-content"> 
                    <li>Digital Marketing</li> 
                </ul>
                
                <a href="#" class="pricingTable-signup">Enquire Now</a>
            </div>
        </div>
    </div>
    </div>


   
    <div class="course-block">
    <div class="container"> 
        <div class="row"> 
            <h3 class="title">Features</h3>
            <div class="col-md-12 features">&nbsp;</div>
        </div>
    </div>
    </div>


    </div>
	<!-- about -->
		<!--<div class="about" id="about">
		<div class="container">
			
			<div class="col-md-6 about_right"> 
				<h3>ABOUT</h3>
				<h3 class="bold">NMIMS UNIVERSITY</h3>
				
                 <P>
                 All NMIMS schools have state-of-the-art infrastructure. NMIMS promotes global thinking consistent with national interest and promotes values, professionalism, social sensitivity and dynamic entrepreneurship. Our programs have been recognized and accredited across India and internationally for giving students the opportunity to develop a career at a holistic level. With its large number of international tie-ups, the University is expanding at a phenomenal pace, working towards the concept of establishing a ‘Global Village’.
                 </P>
                 <P>
                 <i>
                 Today, more than 12000 students and 450 full time faculty members are part of India's most sought after academic community, NMIMS!
                 </i>
                 </P>
            </div>
            <div class="col-md-6 about-left">
                <div class="col-xs-12 aboutimg-w3l aboutimg-w3l2">
                    <div class="rwd-media">
                         <iframe  src="https://www.youtube.com/embed/ncQLAb_jJOc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>    
                    </div>
				</div>
				<div class="clearfix"> </div>
			</div>
            <div class="clearfix"> </div>
		</div>
	</div>-->
	<!-- //about -->
    <div class="nmims">
        <div class="container">
        <h2 class="title">About NMIMS</h2>
        <p>We are NMIMS Global Access School for Continuing Education, #1 Distance Management University. From being the country's premiere distance-learning establishment of the 20th century to crafting a 21st century education platform, our journey has been dynamic and gratifying. Thousands of students have graduated from NGA-SCE and are now fulfilling their hopes and dreams.
         </p>
         <br><br><br>
        <h2 class="title">NMIMS Edge</h2>
                <div class="col-md-6 text-left ">
                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/edge_img.jpg" alt="" class="img-responsive">   
                </div>
                <div class="col-md-6 Right_edge_block">
                <ul>
								<li>
									<div class="edge_icon">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/accredited_naac_img.png">
									</div>
									<div class="edge_text">
										Accredited by NAAC with Grade A+ 
									</div>
								</li>
								<li>
									<div class="edge_icon">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/granted_autonomy_icon.png">
									</div>
									<div class="edge_text">
										Granted Autonomy Category 1 by UGC
									</div>
								</li>
								<li>
									<div class="edge_icon">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/regional_centre_icon.png">
									</div>
									<div class="edge_text">
										9 regional offices across India
									</div>
								</li>
								<li>
									<div class="edge_icon">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/flexible_fee_icon.png">
									</div>
									<div class="edge_text">
										Flexible Fee Payment options with EMI facility
									</div>
								</li>
								
								<li>
									<div class="edge_icon">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/help_desk_img.png">
									</div>
									<div class="edge_text">
										Dedicated student support team to manage all queries
									</div>
								</li>
							</ul>
                </div>
                 
        </div>
    </div>

    <!-- projects -->
	<div class="gallery" id="projects">
        <div class="container"> 
            <h3 class="title">Hiring Partners</h3>
        
            <div class="row">
                <div class="col-md-12">
                    <!-- begin panel group -->
                    <div  class="panel-group">
                         <div class="panel panel-default"> 
                           <span class="side-tab" data-toggle="tab" role="tab" aria-expanded="false"> 
                                <div class="panel-heading title-header-block" role="tab" id="" data-toggle="collapse" data-parent="#" href="#" aria-expanded="" aria-controls=""> 
                                    <h4 class="panel-title">NMIMS has partnered with leading corporates to provide job assistance to all students enrolling in the NMIMS Learning Programs. Post successful program completing, the hiring partners have agreed to interview meritorious students based on their requirements.</h4> 
                                    <h4></h4> 
                                    <h4></h4> 
                                    <span class="btn"></span> 
                                </div> 
                           </span> 
                        </div>   
                    </div>
                    
                </div> <!-- /col-md-12 -->
            </div> <!--/ .row -->
            <div class="row">
                <div class="col-md-12 clearfix">
                    <div class="col-sm-offset-1 col-sm-10 hiring-slider">
                        <div id="hiring-slider" class="owl-carousel">
                            <div class="testimonial">
                            <h4 class="testimonial-title">
                                        Mindteck India
                                    </h4>
                                <p class="description">
                                Established in 1991, Mindteck provides product engineering and IT services to top-tier Fortune 50-1000 clientele, start-ups, leading universities and government entities around the globe. They offer holistic Analytics and Business Intelligence service offerings to companies enabling data driven decision making. http://www.mindteck.com/career/job-openings.html
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                        <img src="<?php echo S3_URL?>/site/nmims-assets/images/mindteck.png" alt="">
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="testimonial">
                            <h4 class="testimonial-title">
                                    SHOPX
                                       
                                    </h4>
                                <p class="description">
                                    To create a difference in the world of small retailers, SHOPX challenges conventional e-commerce and retail to transform the way consumers shop online. Their mission is to empower 1 million retailers and thereby enable 400 million middle income Indians to reap the benefits of commerce.http://shopx.in/careers/
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/shopx-logo-white.png" />
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="testimonial">
                                
                            <h4 class="testimonial-title">
                                    Essel Propack Ltd. </h4>
                                <p class="description">
                                    Essel Propack is a global tube-packaging company headquartered in Mumbai in India. It is part of the Essel Group, and is a specialty packaging manufacturer of laminated plastic tubes for the FMCG and Pharma companies. With over 2,600 employees in 12 countries working through 25 state-of-the-art facilities, Essel is the largest global specialty packaging company in the world. www.esselpropack.com
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/Essel-Propack.jpg" />
                                    </div>
                                </div>
                            </div>
                            <div class="testimonial">
                                <h4 class="testimonial-title">
                                    Eminent Land Realty
                                    </h4>
                                <p class="description">
                                    With 5000+ satisfied customers and over 5L active users, Eminent Land is a leading real estate company based in Gurgaon. The company puts forth their skills and expertise to help property seekers and investor with the upcoming opportunities in the real estate sector. http://www.eminentland.com/careers.php
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/Eminent-Land.png" />
                                    </div>
                                    
                                    
                                </div>
                            </div>
                            <div class="testimonial">
                                <h4 class="testimonial-title">
                                    Lake Shore India Management
                                       
                                    </h4>
                                <p class="description">
                                    Lake Shore is a facility management company that is into investing, developing and managing shopping centres across India.
                                </p>                   
                                <div class="testimonial-review">
                                    <!-- <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/shopx-logo-white.png" />
                                    </div> -->
                                    
                                </div>
                            </div>
                            <div class="testimonial">
                                 <h4 class="testimonial-title">
                                    Control Print India Ltd </h4>
                                <p class="description">
                                    Control Print Limited is India’s first Coding and Marking solutions manufacturers. With over two decades of industry experience, CPL is also one of the fastest growing in its industry. CPI believes considers their eompoyees their biggest assets. https://www.controlprint.com/contact/careers
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/Control-Print.jpg" />
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="testimonial">
                                <h4 class="testimonial-title">
                                    Associate Group 
                                    </h4>
                                <p class="description">
                                    The ‘Associate Group’ is a diversified conglomerate with business footprints in industries ranging from timber and panel products to infrastructure and high-pressure gas cylinders for CNG (Compressed Natural Gas) application. The Group’s diverse businesses primarily cater to requirements of real estate, construction, infrastructure development and automobile industry domains. https://www.associatedecor.com/company-culture
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/Associate-Group.png" />
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="testimonial">
                                <h4 class="testimonial-title">
                                    Yoptima
                                       
                                    </h4>
                                <p class="description">
                                The YOptima Audience Platform helps businesses engage customers through desktop, mobile and video ads across all stages of the funnel. Marketers use YOptima to prospect new customers at the top of the funnel by creating highly targeted, large scale look-alike audience of their existing customers. http://www.yoptima.com
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/yoptima-logo.jpg" />
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="testimonial">
                                 <h4 class="testimonial-title">
                                    Zalaris Consulting
                                 </h4>
                                <p class="description">
                                Zalaris provides full outsourcing of business functions for all payroll and personnel services and related consultancy services to enterprises in Europe. Their solutions allow customers to operate with common payroll- and HR-processes across geographies. https://zalaris.in/
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/Zalaris.jpg" />
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="testimonial">
                                    <h4 class="testimonial-title">
                                    Maxstack Lab Technologies
                                     </h4>
                                <p class="description">
                                    Max stack labs is an IT service/solutions provider founded in 2016, specializing in the areas of Application testing, Artificial Intelligence, Business intelligence, Block Chain, Web/Mobile application development, CRM and providing high-end solutions around the quality management products. http://www.maxstacklabs.com/
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/Max-Stack-Labs.jpg" />
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="testimonial">
                                <h4 class="testimonial-title">
                                    Café Coffee Day </h4>
                                <p class="description">
                                Café Coffee Day (abbreviated as CCD) is an Indian café chain owned by Coffee Day Global Limited, a subsidiary of Coffee Day Enterprises Limited. Present in 6 countries, there have 1500+ outlets across 28 states of India. http://www.cafecoffeeday.com/
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/ccd.jpg" />
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="testimonial">
                                <h4 class="testimonial-title">
                                    VIVO India  
                                    </h4>
                                <p class="description">
                                Vivo India is a global smartphone company that creates smart phones with excellent appearance, professional acoustic fidelity, extreme video display and joyful experience for the vivacious, young and fashionable urban mainstream consumers. VIVO dares to pursue perfection and constantly create surprises! http://www.vivo.com/in/about-vivo/career
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/Vivo-India.jpg" />
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="testimonial">
                                <h4 class="testimonial-title">
                                     Leel Electricals Ltd 
                                    </h4>
                                <p class="description">
                                    Leel Electricals is the largest producer of Coils / Heat Exchangers (Fin & Tube type) in India, serving the entire spectrum of HVAC & R industry in the country as well as OEM’s in North America, Europe, Middle East and Australia. The company also manufactures Air conditioners for the Indian Railways, Metro Rail and Buses. http://www.leelelectric.com
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/leel-logo.jpg" />
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="testimonial">
                             <h4 class="testimonial-title">
                                    Milkbasket
                                    </h4>
                                <p class="description">
                                 Milkbasket is India's first subscription based micro-delivery service fulfilling daily grocery and household needs of customers every morning. Headquartered in Gurgaon, MilkBasket has raised $7 million in a Series A round of funding led by Kalaari Capital, along with BeeNext and existing investors Unilever Ventures and Blume Ventures. To enable frequent and friction- less buying, Milkbasket has innovated flexi ordering and contactless delivery - both a first in the ecommerce industry - and favourites of Milkbasket customers.
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/milk-basket.png" />
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="testimonial">
                            <h4 class="testimonial-title">
                                    ToneTag
                                    </h4>
                                <p class="description">
                                ToneTag is a global technology solutions provider. It offers sound-based contactless payments, location based services, and proximity based customer engagement services, amongst others. ToneTag has filed 7 global patents for a variety of exciting use cases in the payments ecosystem and the offline retail space and has touched the lives of its over 50 million global consumers. ToneTag is the first company to have enabled contactless payment acceptance on Electronic Data Capture (EDC) machines using sound.
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/tone-tag.png" />
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="testimonial">
                                <h4 class="testimonial-title">
                                    Bajaj Electrical Ltd
                                    </h4>
                                <p class="description">
                                Bajaj Electricals Limited (BEL), an 80 year old trusted company with a turnover of above Rs. 4700 Crores, is a part of the US $ 7 billion (over Rs 30,000 crores) "Bajaj Group". They have 3 business segments - Engineering & Projects, Consumer Products and Illumination. With 19 branch offices spread across India they are supported by a chain of about 1000 distributors, 4000 authorized dealers, over 4,00,000 retail outlets and over 282 Customer Care centers.
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/bajaj-electricals.png" />
                                    </div>
                                   
                                </div>
                            </div>
                            <div class="testimonial">
                                 <h4 class="testimonial-title">
                                    Helix Tech Pvt. Ltd.
                                    </h4>
                                <p class="description">
                                Helix Tech is an award winning mobile first technology partner to big and small entities, providing them end-to-end turnkey software solutions. In last 5 years of inception, they have consulted close to 500+ Apps for over 100 clients across 6 continents. Helix Tech has been voted as the company of the year by CIO review & has ranked among the top 30 companies to watch out for by Silicon Review.
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/helix-tech.png" />
                                    </div>
                                  
                                </div>
                            </div>
                            <div class="testimonial">
                            <h4 class="testimonial-title">
                                    Michelin India Pvt. Ltd
                                    </h4>
                                <p class="description">
                                    Michelin, the leading tire company, is dedicated to sustainably improving the mobility of goods and people by manufacturing and marketing tires and services for every type of vehicle, including airplanes, automobiles, bicycles/motorcycles, earthmovers, farm equipment and trucks. Headquartered in Clermont-Ferrand, France, Michelin is present in more than 170 countries, has 111,200 employees and operates 67 production plants in 17 different countries. Michelin has a Technology Center in charge of research and development, with operations in Europe, North America and Asia.
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/milchelin.png" />
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="testimonial">
                                <h4 class="testimonial-title">
                                    Teleperformance  
                                    </h4>
                                <p class="description">
                                Teleperformance is a strategic partner to the world’s leading companies, bringing solutions and enhancing customer experience during each interaction. They are the largest interactions experts team in the market: multicultural, highly skilled, and deeply knowledgeable, with a wide range of integrated omnichannel solutions, technology, and the highest security standards. https://www.teleperformance.com/en-us/who-we-are/about-us
                                </p>                   
                                <div class="testimonial-review">
                                    <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/teleperformance.jpg" />
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="testimonial">
                            <h4 class="testimonial-title">
                                    Dr. Remedies Healthcare India Pvt Ltd
                                    </h4>
                                <p class="description">
                                Leading chain of diagnostic centeres and Pathalogy labs in Bangalore and Hyderabad. Company formed in 2012, has a share capital is Rs. 7,500,000 and its paid up capital is Rs. 7,200,000.It is inolved in Manufacture of other chemical products
                                </p>                   
                                <div class="testimonial-review">
                                    <!-- <div class="pic">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/Vivo-India.jpg" />
                                    </div> -->
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
	</div>
    <!-- //projects -->

  
    <footer>
		<div class="copy-right-grids">
        <div class="container">
            <p class="footer-gd"><b>Disclaimer: </b> This page is a property of Adcanopus, a marketing affiliate of NMIMS University.
                 All activity on this page is limited to providing information on various NMIMS program.</p>
        </div>
			
		</div>
	</footer>    
	<div class="floating-form visiable" id="contact_form">
	<div class="contact-opener">Enquire Now</div>
        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="nmims_jsfrm('<?php echo SITE_URL?>nmims/submit_frm','2')">
                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Make Your Course Enquiry</h4>                           
                            </div> 
                        <div class="col">   
                        <div class="form-group">
                            <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                            <span class="help-block" id="name_err2"></span>
                        </div>
                        
                    <div class="form-group">
                        <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                        <span class="help-block" id="email_err2" ></span>
                    </div>
                    
                    <div class="form-group">
                        <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode2">
                        <input type="text" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="phone_err2"> </span>
                        
                    </div>
                    <div class="form-group">
                           <select name="city" id="city2" class="form-control">                                                                                                  
                                
						        <option value="" > City</option>
				                 <?php foreach($all_array['NMIMS_CITY'] as $k=>$v){ ?>
										<option value="<?php echo $k;?>" > <?php echo $v;?></option>
				               <?php }?>                            
                            </select>
							<span class="help-block" id="city_err2"> </span>
                            
                        </div>
                        <div class="form-group radio-form">
                            <label class="title-label">Course</label>
                            <div class="custom-radio">
                                 <p>
                                    <input type="radio" id="c21" name="cource1" value="Post Graduate Diploma - 2 yr "  checked>
                                    <label for="c21">Post Graduate Diploma (2 yr)</label>
                                </p>
                                <p>
                                    <input type="radio" id="c22" name="cource1" value="Diploma - 1 yr">
                                    <label for="c22"> Diploma (1 yr)</label>
                                </p>
                                <p>
                                    <input type="radio" id="c23" name="cource1" value="Professional Programs - 1 year">
                                    <label for="c23">Professional Programs (1 year)</label>
                                </p>
                                <p>
                                    <input type="radio" id="c24" name="cource1" value="Certificate - 6 months">
                                    <label for="c24">Certificate (6 months)</label>
                                </p>
                            </div>   




                            
                             
                         
							<span class="help-block" id="cource_err2"> </span>                           
                        </div>                  
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>
                </div>
            </div>
        </form>	
	<div>
        <!-- 
    <div class="popup-enquiry-form mfp-hide" id="popupForm">
        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="nmims_jsfrm('<?php echo SITE_URL?>nmims/submit_frm','3')">                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/nmims-assets/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Make Your Course Enquiry</h4>                           
                            </div> 
                        <div class="col">   
                        <div class="form-group">
                            <input type="text" class="form-control" id="name3" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                            <span class="help-block" id="name_err3"></span>
                        </div>
              
                    <div class="form-group">
                        <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                        <span class="help-block" id="email_err3" ></span>
                    </div>
                    
                    <div class="form-group">
                        <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode3">
                        <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="phone_err3"> </span>
                       
                    </div>
                    <div class="form-group">
                           <select name="city" id="city3" class="form-control">
                                                                                                  
                                
						        <option value="" > City</option>
				                    <?php foreach($all_array['NMIMS_CITY'] as $k=>$v){ ?>
										<option value="<?php echo $k;?>" > <?php echo $v;?></option>
				               <?php }?>                        
                            </select>
							<span class="help-block" id="city_err3"> </span>
                            
                        </div>
                        <div class="form-group radio-form">
                            <label class="title-label">Course</label>
                            <div class="custom-radio">
                                 <p>
                                    <input type="radio" id="c31" name="cource1" value="Post Graduate Diploma - 2 yr "  checked>
                                    <label for="c31">Post Graduate Diploma (2 yr)</label>
                                </p>
                                <p>
                                    <input type="radio" id="c32" name="cource1" value="Diploma - 1 yr">
                                    <label for="c32"> Diploma (1 yr)</label>
                                </p>
                                <p>
                                    <input type="radio" id="c33" name="cource1" value="Professional Programs - 1 year">
                                    <label for="c33">Professional Programs (1 year)</label>
                                </p>
                                <p>
                                    <input type="radio" id="c34" name="cource1" value="Certificate - 6 months">
                                    <label for="c34">Certificate (6 months)</label>
                                </p>
                            </div>   
							<span class="help-block" id="cource_err3"> </span>                           
                        </div>                   
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>
                </div>
            </div>
        </form>
    </div>
-->
	<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
	<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
	<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
	<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
	<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
	
	
	<script src="<?php echo S3_URL?>/site/nmims-assets/js/vendor.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script> --> 
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
    
    <script src="<?php echo S3_URL?>/site/nmims-assets/js/main.js"></script>
	
	 <script src="<?php echo S3_URL?>/site/scripts/nmims.js"></script>
   </body>
</html>