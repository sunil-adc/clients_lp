<?php $all_array = all_arrays(); 

?>
<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Max Life Insurance</title>
	  <link rel="shortcut icon" href="<?php echo S3_URL?>/site/insurance-assets/maxlife/images/fevicon.png" type="image/x-icon" />
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/insurance-assets/maxlife/css/vendor.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/insurance-assets/maxlife/css/bootstrap.min.css">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/insurance-assets/maxlife/css/main.css">    
      <!-- fonts -->
      <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
            rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

        <!-- DO NOT MODIFY -->
        <!-- Quora Pixel Code (JS Helper) --> 
        <script>
        !function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
        qp('init', '0250ea612ac748c6b5516e1e84caa786');
        qp('track', 'ViewContent');
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/0250ea612ac748c6b5516e1e84caa786/pixel?tag=ViewContent&noscript=1"/></noscript>
        <!-- End of Quora Pixel Code -->

   </head>
   <body>

 <?php $all_array = all_arrays(); ?>      
 

    <div class="slide-wrapper">

   <div id="homepage-feature" class="carousel slide" data-ride="carousel" data-interval="3000">
      <ol class="carousel-indicators">
         <li data-target="#homepage-feature" data-slide-to="0" class="active"></li>
         <li data-target="#homepage-feature" data-slide-to="1"></li>
         <li data-target="#homepage-feature" data-slide-to="2"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner">

         <div class="item active">
            <img src="<?php echo S3_URL?>/site/insurance-assets/maxlife/images/desktop_banner_1.jpg" />
         </div>

         <div class="item">
            <img src="<?php echo S3_URL?>/site/insurance-assets/maxlife/images/desktop_banner_2.jpg" />
         </div>

         <div class="item">
            <img src="<?php echo S3_URL?>/site/insurance-assets/maxlife/images/desktop_banner_3.jpg" />
         </div>

      </div>
      <!-- /.carousel-inner -->
      <!-- Controls -->
      <a class="left carousel-control" href="#homepage-feature" data-slide="prev">
        <span class="fa fa-angle-left"></span>
      </a>
      <a class="right carousel-control" href="#homepage-feature" data-slide="next">
        <span class="fa fa-angle-right"></span>
      </a>
   </div>
   <!-- /#homepage-feature.carousel -->
   <div class="red">

     <div class="red-content">
        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="insurance_jsfrm('<?php echo SITE_URL?>policy/insurance/submit_frm','maxlife','1')">
                   
            <div class="">       
            <div class="col">
                    <div class="form-logo">
                        <a href="#">
                            <img src="<?php echo S3_URL?>/site/insurance-assets/maxlife/images/logo-small.png" />
                        </a> 
                    </div> 
                    <h4>Thank you for expressing interest on our Max Life Insurance Our expert will get in touch with you shortly.</h4>                           
                    </div> 
            </div>
        </form>

     </div>


   </div>

</div>
<!-- /.container.slide-wrapper -->


    
    <!--//banner -->    

    <div class="about" id="about">
        <div class="container">
            
            <div class="col-md-12"> 
                <h3 class="title">5 powerful reasons to buy Max Life Term Insurance</h3>                              
            </div>
			<div class="row">            
			<div class="col-md-4">
					<div class="pricingTable text-center">
						<img src="<?php echo S3_URL?>/site/insurance-assets/maxlife/images/icon-one.jpg" alt="" class="img-responsive">
						<h3 class="title">40 Major Critical Illnesses Covered^ </h3> 
						<a href="#" class="pricingTable-signup">Enquiry</a>
					</div>
				</div>

				<div class="col-md-4">
                    <div class="pricingTable text-center">
						<img src="<?php echo S3_URL?>/site/insurance-assets/maxlife/images/icon-2.jpg" alt="" class="img-responsive">
						<h3 class="title">Save upto 40% with Limited Premium Payment**</h3> 
						<a href="#" class="pricingTable-signup">Enquiry</a>
					</div>
				</div>

				<div class="col-md-4">
                    <div class="pricingTable text-center">
						<img src="<?php echo S3_URL?>/site/insurance-assets/maxlife/images/icon-3.jpg" alt="" class="img-responsive">
						<h3 class="title">Return of Premium*^</h3> 
						<a href="#" class="pricingTable-signup">Enquiry</a>
					</div>
				</div>            
			</div> 
       
            <div class="row">            
			<div class="col-md-4">
                <div class="pricingTable text-center">
						<img src="<?php echo S3_URL?>/site/insurance-assets/maxlife/images/icon-4.jpg" alt="" class="img-responsive">
						<h3 class="title">Choice of 7 Payout Options^</h3> 
						<a href="#" class="pricingTable-signup">Enquiry</a>
					</div>
				</div>

				<div class="col-md-4">
                    <div class="pricingTable text-center">
						<img src="<?php echo S3_URL?>/site/insurance-assets/maxlife/images/icon-5.jpg" alt="" class="img-responsive">
						<h3 class="title">Enhance Cover with Your Changing Needs^</h3> 
						<a href="#" class="pricingTable-signup">Enquiry</a>
					</div>
				</div>           
			</div>         
        </div>
    </div> 



    <div class="about" id="about">
        <div class="container">
            
            <div class="col-md-12"> 
                <h3 class="title"> Why Max Life ?</h3>                              
            </div>
			<div class="row">            
			<div class="col-md-4">
					<div class="pricingTable text-center">
						<img src="<?php echo S3_URL?>/site/insurance-assets/maxlife/images/cliam-icon.jpg" alt="" class="img-responsive">
						<h3 class="title">98.74% Claim Settlement Ratio</h3> 
						<p>Source: Max Life Public Disclosure FY18-19</p>
					</div>
				</div>

				<div class="col-md-4">
                    <div class="pricingTable text-center">
						<img src="<?php echo S3_URL?>/site/insurance-assets/maxlife/images/best-life-icon.jpg" alt="" class="img-responsive">
						<h3 class="title">Life Insurer Of The Year.
                            <br>
                            &nbsp;
                        </h3> 
						<p>Outlook Money Award, 2018
                        <br>
                            &nbsp;
                        </p>
					</div>
				</div>

				<div class="col-md-4">
                    <div class="pricingTable text-center">
						<img src="<?php echo S3_URL?>/site/insurance-assets/maxlife/images/swift-icon.jpg" alt="" class="img-responsive">
						<h3 class="title">Swift & Prompt Insurer Award</h3> 
						<p>The Economic Times, Life Insurance-Large Category, 2018</p>
					</div>
				</div>            
			</div>       
        </div>
    </div> 

    
    <div class="course-block">
    <div class="container">
    <div class="serviceBox">
    <h3 style="font-size: 18px; text-align: left;"> <strong>BEWARE OF SPURIOUS/FRAUD PHONE CALLS!

      IRDAI CLARIFIES TO PUBLIC THAT :</strong></h3>
        <div class="row">
            <div class="col-md-12">
               <ul>
               	<li>IRDAI is not involved in activities like selling insurance policies, announcing bonus or investment of premiums.</li>
               	<li>Public receiving such phone calls are requested to lodge a police complaint.</li>
               </ul>
              
            </div>            
        </div>
    </div> 
    </div>  
	   </div>
	   
    <!-- //about -->
    <div class="services">
        <div class="container">
            <div class="row">
            <div class="col-md-12">
        	 <p style="font-size: 10px;"><strong> Disclaimer :</strong></p>
            </div>
			</div>
			  <div class="row">
			  <div class="col-md-12">
             <p style="font-size: 10px;"> Max Life Smart Term Plan. A Non-Linked, Non-Participating, Term Insurance Plan (UIN: 104N113V01).<br>* Standard Premium For 24-Year Old Male, Non-Smoker, 25 Years Policy Term, 25 Year Premium Payment Term (exclusive of GST). ^On payment of additional premiums.</p>
             <br />
             <p style="font-size: 10px;"> Life insurance cover is available in this product. Insurance is the subject matter of solicitation. For more details on the risk factors, terms and conditions, please read the product sales prospectus on <a style="color: blue;" href="https://www.maxlifeinsurance.com/" class="disc-clr" target="_blank">www.maxlifeinsurance.com</a> carefully before concluding a sale. For details on the riders- Max Life Waiver of Premium Plus Rider (UIN104B029V02) &amp; Max Life Comprehensive Accident Benefit Rider (UIN104B025V02) please refer to the Rider prospectus on <a style="color: blue;" href="https://www.maxlifeinsurance.com/" class="disc-clr" target="_blank">www.maxlifeinsurance.com</a>.</p>
            <br/>
            <br />
            <p style="font-size: 10px;">Copyright @2013 Max Life insurance Co. Ltd. All Rights Reserved. An ISO 9001:2008 Certified Company.
                Max Life Insurance Co. Ltd. is a Joint Venture between Max Financial Services Ltd. and Mitsui Sumitomo Insurance Co. Ltd.
                Trade logos displayed above belong to Max Financial Services Limited and Mitsui Sumitomo Insurance Co. Ltd. respectively and are used by Max Life Insurance Co Ltd under a license.
                Registered Office: 419, Bhai Mohan Singh Nagar, Railmajra, Tehsil Balachaur, District Nawanshahr, Punjab -144 533.Corporate Office: Max Life Insurance Co. Ltd., 3rd, 11th and 12th Floor, DLF Square Building, Jacaranda Marg, DLF City Phase II, Gurugram (Haryana) - 122002.
                Helpline: 1860 120 5577 (9:00 A.M to 6:00 P.M Monday to Saturday, except National Holidays; call charges apply). Online Term Plan Helpline: 1800 200 3383.
                Fax Number:0124-4159397. Email Ids: <a style="color: blue;" href="mailto:service.helpdesk@maxlifeinsurance.com" class="disc-clr" target="_blank">service.helpdesk@maxlifeinsurance.com</a>; <a style="color: blue;" href="mailto:online@maxlifeinsurance.com" class="disc-clr" target="_blank">online@maxlifeinsurance.com</a>
                IRDAI - Registration No. 104. ARN/Web/01. Corporate Identity Number (CIN):U74899PB2000PLC045626.
            </p>
            </div>
			</div>
       
       		<!-- <div class="row">
        	 <div class="col-md-6" style="font-size: 9px;">ARN: Web/Aff/OTP+V03IRDAI</div>
        	 <div class="col-md-6 text-right" style="font-size: 10px;">Regn. No. 104</div>
            </div> -->
            <!-- <div class="col-md-12" style="font-size: 9px; padding: 0px;">
        	**On payment of additional premium
            </div> -->
        </div>
    </div>
    <br><br><br><br>

    <footer>
      <div id="pi">
       <?php
       $vr = $this->session->userdata('user_id');
       $ut = $this->session->userdata('utm_source');
       if( isset($vr) &&  $vr > 0 && $ut != ""){
           echo $this->lead_check->set_pixel($vr, INSURANCE_USER);
           
       }
       ?>
      </div>
        <div class="text-center">
            <img src="<?php echo S3_URL?>/site/insurance-assets/maxlife/images/footer.jpg" />
        </div>
    </footer>    
    
    
    <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
    <input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
    <input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
    <input type="hidden" id="utm_sub" name="utm_sub" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
    <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
    
    <!-- <script>
    $("#homepage-feature").carousel();
    </script> -->
    
    <script src="<?php echo S3_URL?>/site/insurance-assets/maxlife/js/vendor.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>  
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
    
    <script src="<?php echo S3_URL?>/site/insurance-assets/maxlife/js/main.js"></script>
    
    <script src="<?php echo S3_URL?>/site/insurance-assets/maxlife/js/bootstrap-datepicker.js"></script>
     <script src="<?php echo S3_URL?>/site/scripts/insurance.js"></script>
   </body>
</html>