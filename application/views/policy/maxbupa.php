<?php $all_array = all_arrays(); 

?>
<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Maxbupa</title>
	  <link rel="shortcut icon" href="<?php echo S3_URL?>/site/insurance-assets/maxbupa/images/icon.png" type="image/x-icon" />
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/insurance-assets/maxbupa/css/vendor.css">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/insurance-assets/maxbupa/css/main.css">    
      <!-- fonts -->
      <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
            rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

        <!-- DO NOT MODIFY -->
        <!-- Quora Pixel Code (JS Helper) -->
        <script>
        !function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
        qp('init', 'ff0dc2036cfb4f2d9ef0b464754ce3bb');
        qp('track', 'ViewContent');
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/ff0dc2036cfb4f2d9ef0b464754ce3bb/pixel?tag=ViewContent&noscript=1"/></noscript>
        <!-- End of Quora Pixel Code -->

   </head>
   <body>

 <?php $all_array = all_arrays(); ?>      
        <!-- <div class="header-top">
            <p>
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                Chokkanahalli Thanisandra Main Road, Yelahanka Hobli, Bengaluru-560064
            </p>
        </div> -->
    <!-- banner -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
              <div class="container">
                <div class="row">                
                    <div class="col-md-3 text-center">
                        
                        <img src="<?php echo S3_URL?>/site/insurance-assets/maxbupa/images/30_minute-mob.gif" class="head-clock" />
                    </div>
                  
                        
                   
                    <div class="col-md-6">
                        <h1 class="head-text header">
                            MAKE EVERY SECOND COUNT DURING AN EMERGENCY
                        </h1>
                    
                    </div>
                    <div class="col-md-3 text-center">
                         <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="insurance_jsfrm('<?php echo SITE_URL?>policy/insurance/submit_frm','maxbupa','1')">
                                <div class="">       
                                        <div class="col">
                                            <div class="form-logo form-logo-right">
                                                <a href="#">
                                                    <img src="<?php echo S3_URL?>/site/insurance-assets/maxbupa/images/logo-small.png" />
                                                </a> 
                                            </div> 
                                            <h4>Get your Life Insurance</h4>                           
                                            </div> 
                                        <div class="col">   
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                                            <span class="help-block" id="name_err1"></span>
                                        </div>
                                    
                                        <div class="form-group">
                                            <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                                            <span class="help-block" id="email_err1" ></span>
                                        </div>
                                        
                                        <div class="form-group">
                                            <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode1">
                                            <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                                            <span class="help-block" id="phone_err1"> </span>
                                            
                                        </div>
                                        
                                    <div class="submitbtncontainer">
                                    <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
                                </div>
                                </div>
                                </div>
                        </form>
                    </div>
                </div>             	 
              </div> 
            </div>
        </div>

        <!-- The Modal -->
    </div>
    <!--//banner -->    

    <div class="about" id="about">
        <div class="container">
            
            <div class="col-md-12"> 
                <h3 class="title">Why choose Max Bupa?</h3>
      
                <br><br>
                
            </div>
            <div class="row">
                            <div class="col-md-4 text-center spacing-4 serviceBox">
                                <div class="feature">
                                    <img src="<?php echo S3_URL?>/site/insurance-assets/maxbupa/images/crcl_1.png" alt="">
                                    <br> <br> 
                                   
                                    <p><strong>Maternity and coverage for new born from day one**</strong></p>
                                </div>
                            </div>
                            <div class="col-md-4 text-center spacing-4 serviceBox">
                                <div class="feature">
                                    <img src="<?php echo S3_URL?>/site/insurance-assets/maxbupa/images/crcl_2.png" alt="">
                                    <br> <br>
                                  
                                    <p><strong>Covers 60 days pre & 90 days post hospitalisation</strong></p>
                                </div>
                            </div> 
                            <div class="col-md-4 text-center spacing-4 serviceBox">
                                <div class="feature">
                                    <img src="<?php echo S3_URL?>/site/insurance-assets/maxbupa/images/crcl_3.png" alt="">
                                    <br> <br>  
                             
                                    <p><strong>No capping on room rent***</strong></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 text-center spacing-4 serviceBox">
                                <div class="feature">
                                    <img src="<?php echo S3_URL?>/site/insurance-assets/maxbupa/images/crcl_4.png" alt="">
                                    <br> <br> 
                                  
                                    <p><strong>Coverage from Rs. 5 lakh to Rs. 1 crore</strong></p>
                                </div>
                            </div>
                            <div class="col-md-4 text-center spacing-4 serviceBox">
                                <div class="feature">
                                    <img src="<?php echo S3_URL?>/site/insurance-assets/maxbupa/images/crcl_5.png" alt="">
                                    <br> <br>
                                 
                                    <p><strong>All day-care procedures are covered</strong></p>
                                </div>
                            </div> 
                            
                        </div>
        </div>
    </div> 

    
    <div class="course-block">
    <div class="container">

        <div class="row">
            
        <div class="col-md-12">
                <div class="pricingTable">
                    
               
                    <p style="font-size: 9px; text-align: justify;">Insurance is the subject matter of solicitation | Max Bupa Health Insurance Co. Ltd.. 'Max', 'Max logo' and 'Bupa' logo are trademarks of their respective owners and are being used by Max Bupa Health Insurance Company Limited under license. UIN No: MB/SS/CA/2018-19/195 Reg. Ofc.: Max House, 1 Dr. Jha Marg, Okhla, New Delhi – 110020. IRDAI Registration No. 145.CIN No. is U66000DL2008PLC182918. Fax No.: 1800 3070 3333. Help line No.: 1860-3010-3333. www.maxbupa.com.*Max Bupa processes pre-auth requests within 30 minutes for all active policies, subject to receiving all documents and information(s) up to Max Bupa’s satisfaction. The above commitment does not include pre-authorization settlement at the time of discharge or system outrage **Maternity claim would be payable after we have received at least 3 continuous annual premiums for the Insured Person in respect of whom a claim is made. Coverage for New Born child from Day 1 is applicable where maternity claim is admissible. ***Applicable for platinum plan. For Silver and Gold plans covered up to sum Insured except suite or above room category. Please read sales brochure carefully before concluding a sale. Product Name: Heartbeat Product UIN No.: IRDA/NL-HLT/MBHI/P-H/V.III/19/16-17 </p>
                   
                </div>
            </div>
        </div>
    </div> 
    </div> 
 

    <br><br><br><br>

    <footer>
        <div class="copy-right-grids">
            <p class="footer-gd">Copyright 2017 Duelist Technolgies pvt.ltdINC. Designed by SeemaWebGalaxy. All rights reserved.</p>
        </div>
    </footer>    
    <div class="floating-form visiable" id="contact_form">
    <div class="contact-opener">Enquire Now</div>
        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="insurance_jsfrm('<?php echo SITE_URL?>policy/insurance/submit_frm','maxbupa','2')">
                   
                    <div class="">       
                    <div class="col popup-form">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/insurance-assets/maxbupa/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Get your Life Insurance</h4>                           
                            </div> 
                        <div class="col">   
                        <div class="form-group">
                            <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                            <span class="help-block" id="name_err2"></span>
                        </div>
                        
                    <div class="form-group">
                        <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                        <span class="help-block" id="email_err2" ></span>
                    </div>
                    
                    <div class="form-group">
                        <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode2">
                        <input type="text" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="phone_err2"> </span>
                        
                    </div>
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>
                </div>
            </div>
        </form> 
    <div>
    <div class="popup-enquiry-form mfp-hide" id="popupForm">
        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="insurance_jsfrm('<?php echo SITE_URL?>policy/insurance/submit_frm','maxbupa','3')">
                   
                    <div class="">       
                    <div class="col popup-form">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/insurance-assets/maxbupa/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Get your Life Insurance</h4>                           
                            </div> 
                        <div class="col">   
                        <div class="form-group">
                            <input type="text" class="form-control" id="name3" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                            <span class="help-block" id="name_err3"></span>
                        </div>
              
                    <div class="form-group">
                        <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                        <span class="help-block" id="email_err3" ></span>
                    </div>
                    
                    <div class="form-group">
                        <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode3">
                        <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="phone_err3"> </span>
                       
                    </div> 
                                         
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>
                </div>
            </div>
        </form>
    </div>

    <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
    <input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
    <input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
    <input type="hidden" id="utm_sub" name="utm_sub" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
    <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
    
    
    <script src="<?php echo S3_URL?>/site/insurance-assets/maxbupa/js/vendor.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>  
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
    
    <script src="<?php echo S3_URL?>/site/insurance-assets/maxbupa/js/main.js"></script>
    
     <script src="<?php echo S3_URL?>/site/scripts/insurance.js"></script>
   </body>
</html>