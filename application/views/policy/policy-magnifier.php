<?php $all_array = all_arrays(); 

?>
<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Policy Magnifier-Insurance</title>

	  <link rel="stylesheet" href="<?php echo S3_URL?>/site/insurance-assets/policy_magnifier/css/vendor.css">
	  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/insurance-assets/policy_magnifier/css/main.css">    
      <!-- fonts -->
      <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
            rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
   </head>
   <body>

 <?php $all_array = all_arrays(); ?>      
        <!-- <div class="header-top">
            <p>
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                Chokkanahalli Thanisandra Main Road, Yelahanka Hobli, Bengaluru-560064
            </p>
        </div> -->
	<!-- banner -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="">
					<div class="carousel-caption">
                    <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="insurance_jsfrm('<?php echo SITE_URL?>policy/insurance/submit_frm','policy_magnifier','1')">
                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/insurance-assets/policy_magnifier/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Get your Life Insurance</h4>                           
                            </div> 
                        <div class="col">   
                        <div class="form-group">
                            <input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                            <span class="help-block" id="name_err1"></span>
                        </div>
                   
                        <div class="form-group">
                            <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                            <span class="help-block" id="email_err1" ></span>
                        </div>
                        
                        <div class="form-group">
                            <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode1">
                            <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                            <span class="help-block" id="phone_err1"> </span>
                            
                        </div>
                        <div class="form-group">
                           <select name="city" id="city1" class="form-control">
                                                                                                     
                                
						        <option value="" >Select City</option>
				                <?php foreach($all_array['POLICYMAFNIFER_CITIES'] as $k=>$v){ ?>
										<option value="<?php echo $k;?>" > <?php echo $v;?></option>
				               <?php }?>                                                                      
                            </select>
							<span class="help-block" id="city_err1"> </span>
                            
                        </div>
                       
                    
                      
                    <div class="submitbtncontainer">
                    <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
                    </div>
                    </div>
                    </div>
                </form>
					</div>
				</div>
			</div>
		</div>

		<!-- The Modal -->
	</div>
    <!--//banner -->    

    <div class="about" id="about">
		<div class="container">
			
			<div class="col-md-12"> 
				<h3 class="title">Welcome to Policy Magnifier</h3>
				<h3 class="bold text-center">As per your needs get the best insurance quotes within a minute from best company</h3>
                <br><br>
                
            </div>
            <div class="row">
							<div class="col-md-4 text-center">
								<div class="feature">
                                    <img src="<?php echo S3_URL?>/site/insurance-assets/policy_magnifier/images/icon-1.jpg" alt="">
                                    <br> <br> 
                                    <h3 class="feature-title">360 Quick Search</h3>
									<p>The online service helps you to search the best Life Insurance plan as per the requirement from the best Insurer.</p>
								</div>
							</div>
							<div class="col-md-4 text-center">
								<div class="feature">
                                    <img src="<?php echo S3_URL?>/site/insurance-assets/policy_magnifier/images/icon-2.jpg" alt="">
                                    <br> <br>
									<h3 class="feature-title">Products</h3>
									<p>We help you to get best Health Plans, Term Plans, Investment Plans &amp; Pension Plans for your life &amp; family security.</p>
								</div>
							</div> 
							<div class="col-md-4 text-center">
								<div class="feature">
                                    <img src="<?php echo S3_URL?>/site/insurance-assets/policy_magnifier/images/icon-3.jpg" alt="">
                                    <br> <br>  
								<h3 class="feature-title">Best Insurance &amp; Investment Plans</h3>
									<p>100% Financial Security of your loved one's future.Get the Best Quotes for Term plan&amp; Health planright now!</p>
								</div>
							</div>
						</div>
            
		</div>
	</div> 

    
    <div class="course-block">
    <div class="container">
    <h3 class="title">Insurance Plans<br><br></h3>
    <h3 class="bold text-center">Get the Investment, Insurance, Term, Health & Pension plans for Life Policies.</h3>
        <div class="row">
            
        <div class="col-md-6">
                <div class="pricingTable">
                    <img src="<?php echo S3_URL?>/site/insurance-assets/policy_magnifier/images/investment_planning.jpg" alt="" class="img-responsive">
                    <h3 class="title">Investment Planning</h3> 
                    <p>Investment plans, ULIPs & savings plans offered by Insurance companies provide the 
assurance of lump sum funds to best meet from your savings at the maturity 
of your policy & Flexibility to choose your policy Term too.</p>
                    <a href="#" class="pricingTable-signup">Get Quote</a>
                </div>
            </div>

            <div class="col-md-6">
                <div class="pricingTable">
                    <img src="<?php echo S3_URL?>/site/insurance-assets/policy_magnifier/images/healthandmedical.jpg" alt="" class="img-responsive">
                    <h3 class="title">Health & Medical Plans</h3> 
                    <p>Health insurance helps pay for medical expenses. Health insurance covers cost of an insured individual's medical and surgical expenses.

</p>
                    <a href="#" class="pricingTable-signup">Get Quote</a>
                </div>
            </div>

            <div class="col-md-6">
                <div class="pricingTable">
                    <img src="<?php echo S3_URL?>/site/insurance-assets/policy_magnifier/images/termplan.jpg" alt="" class="img-responsive">
                    <h3 class="title">Term Plans
</h3> 
                    <p>Term insurance, a type of life insurance, provides coverage for a certain period of time or years. If the insured dies over the policy tenure a death benefit (or sum assured) is paid out. ... The purpose of taking life insurance is to provide life cover to the policyholder and financial security to his family.

</p>
                    <a href="#" class="pricingTable-signup">Get Quote</a>
                </div>
            </div>

            <div class="col-md-6">
                <div class="pricingTable">
                    <img src="<?php echo S3_URL?>/site/insurance-assets/policy_magnifier/images/pensionplan.jpg" alt="" class="img-responsive">
                    <h3 class="title">Pension Plans
</h3> 
                    <p>Pension plans helps you to secure your retirement life financially. Even after retirement you can spend your life style as you want.

</p>
                    <a href="#" class="pricingTable-signup">Get Quote</a>
                </div>
            </div>
             
             
        </div>
    </div> 
    </div> 
 
	<!-- //about -->
    <div class="services">
        <div class="container">
        <h3 class="title">Why you should choose us?</h3>
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="serviceBox">
                        <i class="service-icon">1</i>
                        <h3 class="title">Investment Planning </h3>
                        <p class="description">
                        we can help our valuable customers to make the right insurance decisions.
                        </p>
                    </div>
                </div> 

                <div class="col-md-3 col-sm-6">
                    <div class="serviceBox">
                        <i class="service-icon">2</i>
                        <h3 class="title">Health Policy </h3>
                        <p class="description">
                        Health insurance helps pay for medical expenses.                        </p>
                    </div>
                </div>
                
                <div class="col-md-3 col-sm-6">
                    <div class="serviceBox">
                        <i class="service-icon">3</i>
                        <h3 class="title">Term Policy
</h3>
                        <p class="description">
                        If the insured dies over the policy tenure a death benefit (or sum assured) is paid out.                    </p>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6">
                    <div class="serviceBox">
                        <i class="service-icon">4</i>
                        <h3 class="title">Retirement & pension plans
</h3>
                        <p class="description">
                        Pension Plans. Pension during your old or retirement age.

            </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br><br><br>

    <footer>
		<div class="copy-right-grids">
			<p class="footer-gd">Copyright 2017 Duelist Technolgies pvt.ltdINC. Designed by SeemaWebGalaxy. All rights reserved.</p>
		</div>
	</footer>    
	<div class="floating-form visiable" id="contact_form">
	<div class="contact-opener">Enquire Now</div>
        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="insurance_jsfrm('<?php echo SITE_URL?>policy/insurance/submit_frm','policy_magnifier','2')">
                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/insurance-assets/policy_magnifier/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Get your Life Insurance</h4>                           
                            </div> 
                        <div class="col">   
                        <div class="form-group">
                            <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                            <span class="help-block" id="name_err2"></span>
                        </div>
                        
                    <div class="form-group">
                        <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                        <span class="help-block" id="email_err2" ></span>
                    </div>
                    
                    <div class="form-group">
                        <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode2">
                        <input type="text" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="phone_err2"> </span>
                        
                    </div>
                    <div class="form-group">
                           <select name="city" id="city2" class="form-control">
                                                                                                  
                                
						        <option value="" >Select City</option>
				                 <?php foreach($all_array['POLICYMAFNIFER_CITIES'] as $k=>$v){ ?>
										<option value="<?php echo $k;?>" > <?php echo $v;?></option>
				               <?php }?>                            
                            </select>
							<span class="help-block" id="city_err2"> </span>
                            
                        </div>
                                          
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>
                </div>
            </div>
        </form>	
	<div>
    <div class="popup-enquiry-form mfp-hide" id="popupForm">
        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="insurance_jsfrm('<?php echo SITE_URL?>policy/insurance/submit_frm','policy_magnifier','3')">
                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/insurance-assets/policy_magnifier/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Get your Life Insurance</h4>                           
                            </div> 
                        <div class="col">   
                        <div class="form-group">
                            <input type="text" class="form-control" id="name3" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                            <span class="help-block" id="name_err3"></span>
                        </div>
              
                    <div class="form-group">
                        <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                        <span class="help-block" id="email_err3" ></span>
                    </div>
                    
                    <div class="form-group">
                        <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode3">
                        <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="phone_err3"> </span>
                       
                    </div>
                    <div class="form-group">
                           <select name="city" id="city3" class="form-control">
                                                                                                  
                                
						        <option value="" >Select City</option>
				                    <?php foreach($all_array['POLICYMAFNIFER_CITIES'] as $k=>$v){ ?>
										<option value="<?php echo $k;?>" > <?php echo $v;?></option>
				               <?php }?>                        
                            </select>
							<span class="help-block" id="city_err3"> </span>
                            
                        </div>
                                         
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>
                </div>
            </div>
        </form>
    </div>

	<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
	<input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
	<input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
	<input type="hidden" id="utm_sub" name="utm_sub" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
	<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
	
	
	<script src="<?php echo S3_URL?>/site/insurance-assets/policy_magnifier/js/vendor.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>  
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
    
    <script src="<?php echo S3_URL?>/site/insurance-assets/policy_magnifier/js/main.js"></script>
	
	 <script src="<?php echo S3_URL?>/site/scripts/insurance.js"></script>
   </body>
</html>