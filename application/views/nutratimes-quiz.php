<?php $all_array = all_arrays(); ?>
<!DOCTYPE html>
<html>

<head>
    <title>Adcanopus</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/nutratimes-quiz-assets/lib/bootstrap-4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/nutratimes-quiz-assets/lib/rangeslider.js-2.3.0/rangeslider.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
    <!--Custom Css-->
    <link rel="stylesheet" type="text/css" href="<?php echo S3_URL?>/site/nutratimes-quiz-assets/css/style.css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119021233-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-119021233-1');
    </script>

    <!-- Taboola Pixel Code -->
    <script type='text/javascript'>
      window._tfa = window._tfa || [];
      window._tfa.push({notify: 'event', name: 'page_view', id: 1065107});
      !function (t, f, a, x) {
             if (!document.getElementById(x)) {
                t.async = 1;t.src = a;t.id=x;f.parentNode.insertBefore(t, f);
             }
      }(document.createElement('script'),
      document.getElementsByTagName('script')[0],
      '//cdn.taboola.com/libtrc/unip/1065107/tfa.js',
      'tb_tfa_script');
    </script>
    <noscript>
      <img src='//trc.taboola.com/1065107/log/3/unip?en=page_view'
          width='0' height='0' style='display:none'/>
    </noscript>
    <!-- End of Taboola Pixel Code -->

</head>

<body>
       
    <div class="desktop">
        <header class="header-logo">
            <div class="logo">
                <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/nutratimes_logo.png" alt="nutratimes_logo" title="Nutratimes" />
            </div>
        </header>

        <div class="step-form">
        <form name="desktopForm" id="desktopForm">

                <section class="quiz-block-list quiz-block-list-desktop">
                    <div class="step step-1 active">
                        <div class="quiz-block-list_item">
                            <div class="image-block">
                                <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/doctors.png" alt="nutratimes" title="Nutratimes" />
                            </div>
                            <div class="question-block">
                                <div>
                                    <h2 class="page-title">Take a Quiz Journey to know<br>"your healthy weight"</h2>
                                    <h2 class="page-title" style="font-size: 22px;  margin-bottom: 0;color: #4CAF50;">Know Your Healthy Body Mass Index (BMI)</h2>
                                    <div class="options">
                                        <h5 class="option-title">
                                            My Age is
                                        </h5>
                                        <div class="option-list ">
                                            <!-- <div class="option-item">
                                                <input type="radio" id="age-bellow-15" value="age-bellow-15" name="ageGroup">
                                                <label for="age-bellow-15">
                                                    <span class="text">Below</span>
                                                    <span class="value">18</span>
                                                </label>
                                            </div> -->
                                            <div class="option-item">
                                                <input type="radio" id="age-18-35"  value="age-18-35" name="ageGroup">
                                                <label for="age-18-35">
                                                    <span class="text">Between</span>
                                                    <span class="value">18 to 35</span>
                                                </label>
                                            </div>
                                            <div class="option-item">
                                                <input type="radio" id="age-35-45" value="age-35-45" name="ageGroup">
                                                <label for="age-35-45">
                                                    <span class="text">Between</span>
                                                    <span class="value">35 to 45</span>
                                                </label>
                                            </div>
                                            <div class="option-item">
                                                <input type="radio" id="age-45-55" value="age-45-55"  name="ageGroup">
                                                <label for="age-45-55">
                                                    <span class="text">Between</span>
                                                    <span class="value">45 to 55</span>
                                                </label>
                                            </div>
                                            <div class="option-item">
                                                <input type="radio" id="age-above-55" value="age-above-55"  name="ageGroup">
                                                <label for="age-above-55">
                                                    <span class="text">Above</span>
                                                    <span class="value">55</span>
                                                </label>
                                            </div>
                                        </div>
                                        <p class="option-info">Select Your Age</p>
                                    </div>
                                </div>
                                <div class="button-block">
                                    <button class="btn btn-icon btn-prev disabled"><i class="fas fa-chevron-left"></i></button>
                                    <button class="btn btn-icon btn-next"><i class="fas fa-chevron-right"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="step step-2">
                        <div class="quiz-block-list_item">
                            <div class="image-block">
                                <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/gender.png" alt="nutratimes" title="Nutratimes" />
                            </div>
                            <div class="question-block">
                                <div>
                                    <h2 class="page-title">Take a Quiz Journey to know<br>"your healthy weight"</h2>
                                    <h2 class="page-title" style="font-size: 22px;  margin-bottom: 0;color: #4CAF50;">Know Your Healthy Body Mass Index (BMI)</h2>
                                    <div class="options">
                                        <h5 class="option-title">
                                            My Gender is
                                        </h5>
                                        <div class="option-list">
                                            <div class="option-item bg-blue">
                                                <input type="radio" id="male" value="male" name="gender">
                                                <label for="male">
                                                    <span class="icon"><i class="fas fa-mars"></i></span>
                                                    <span class="text">Male</span>
                                                </label>
                                            </div>
                                            <div class="option-item bg-red">
                                                <input type="radio" id="female" value="female" name="gender">
                                                <label for="female">
                                                    <span class="icon"><i class="fas fa-venus"></i></span>
                                                    <span class="text">Female</span>
                                                </label>
                                            </div>
                                        </div>
                                        <p class="option-info">Select Your Gender</p>
                                    </div>
                                </div>
                                <div class="button-block">
                                    <button class="btn btn-icon btn-prev"><i class="fas fa-chevron-left"></i></button>
                                    <button class="btn btn-icon btn-next"><i class="fas fa-chevron-right"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="step step-3">
                        <div class="quiz-block-list_item based-on-gender">
                            <div class="image-block">
                                <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/boy-height.png" class="male-img" alt="nutratimes" title="Nutratimes" />
                                <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/girl-height.png" class="female-img" alt="nutratimes" title="Nutratimes" />
                            </div>
                            <div class="question-block">
                                <div>
                                    <h2 class="page-title">Take a Quiz Journey to know<br>"your healthy weight"</h2>
                                    <h2 class="page-title" style="font-size: 22px;  margin-bottom: 0;color: #4CAF50;">Know Your Healthy Body Mass Index (BMI)</h2>
                                    <div class="options">
                                        <h5 class="option-title">
                                            My Height is
                                        </h5>
                                        <div class="option-list">
                                            <div>
                                                <div class="input-item flex-box">
                                                    <input type="number" class="form-control heightValue" name="heightValue" value="100" id="heightValue" disabled="true">
                                                    <span>CM</span>
                                                </div>
                                                <div class="input-range">
                                                    <input class="heightRange" type="range" min="100" max="250" step="0.1"
                                                        value="100" data-orientation="horizontal" name="height" id ="height">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="blink_me">Please Use Yellow Pointer on The scale to change the height.</div>
                                        <p class="option-info">Select Your Height</p>
                                    </div>
                                </div>
                                <div class="button-block">
                                    <button class="btn btn-icon btn-prev"><i class="fas fa-chevron-left"></i></button>
                                    <button class="btn btn-icon btn-next"><i class="fas fa-chevron-right"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="step step-4">
                        <div class="quiz-block-list_item based-on-gender">
                            <div class="image-block">
                                <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/boy-weight.png" class="male-img" alt="nutratimes" title="Nutratimes" />
                                <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/girl-weight.png" class="female-img" alt="nutratimes" title="Nutratimes" />
                            </div>
                            <div class="question-block">
                                <div>
                                    <h2 class="page-title">Take a Quiz Journey to know<br>"your healthy weight"</h2>
                                    <h2 class="page-title" style="font-size: 22px;  margin-bottom: 0;color: #4CAF50;">Know Your Healthy Body Mass Index (BMI)</h2>
                                    <div class="options">
                                        <h5 class="option-title">
                                            My Weight is
                                        </h5>
                                        <div class="option-list">
                                            <div>
                                                <div class="input-item flex-box">
                                                    <input type="number" class="form-control weightValue" name="weightValue" value="20" id="weightValue" disabled="true">
                                                    <span>KG</span>
                                                </div>
                                                <div class="input-range">
                                                    <input class="weightRange" type="range" min="10" max="200" step="0.5"
                                                        value="20" data-orientation="horizontal" name="weight" id="weight">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="blink_me">Please Use Yellow Pointer on The scale to change the Weight.</div>
                                        <p class="option-info">Select Your Weight</p>
                                    </div>
                                </div>
                                <div class="button-block">
                                    <button class="btn btn-icon btn-prev"><i class="fas fa-chevron-left"></i></button>
                                    <button class="btn btn-icon btn-next"><i class="fas fa-chevron-right"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="step step-5">
                        <div class="quiz-block-list_item">
                            <div class="image-block">
                                <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/gender.png" alt="nutratimes" title="Nutratimes" />
                            </div>
                            <div class="question-block">
                                <div>
                                    <h2 class="page-title">Take a Quiz Journey to know<br>"your healthy weight"</h2>
                                    <h2 class="page-title" style="font-size: 22px;  margin-bottom: 0;color: #FF5722;">Last Step to Know Your <br>Body Mass Index (BMI) to stay Fit</h2>
                                    <div class="options">
                                        <h5 class="option-title">
                                            Fill the Details to see the Result
                                        </h5>
                                        <div class="option-list form-details">
                                            <div>
                                                <div class="input-item">
                                                    <input type="text" tabindex="1" name="userName" class="form-control" placeholder="Name">
                                                </div>
                                                <div class="input-item">
                                                    <input type="email" tabindex="2" name="userEmail" class="form-control" placeholder="Email">
                                                </div>
                                                <div class="otp-box" id="otp">
                                                    <div class="input-item">
                                                        <input type="text" tabindex="3" name="userPhone" class="form-control phone_enter"  placeholder="Mobile">
                                                    </div>
                                                    <div class="box">
                                                        <div class="input-item">
                                                            <input type="number" name="otp" class="form-control otp_enter" id="otp" placeholder="OTP" name="otp">
                                                             <span id="otp_error" class="otp_error"></span>
                                                        </div>
                                                        <div class="input-item text-center" id="show_resend">
                                                             <span id="otp_sent" class="otp_sent"></span>
                                                            <span><a href="javascript:otpresend('<?php echo SITE_URL?>nutratimes/otp_resend','d')">Click Here</a> To Resend OTP</span>
                                                        </div>
                                                    </div>
                                                </div>  
                                                <div class="input-item">
                                                <select name="state" id="state" tabindex="4" data-attr="Please enter correct state" class="form-control">
                                                    <option value="">Select State</option>
                                                    <?php 
                                                    foreach ($all_array['NUTRATIMES_STATE_ARR'] as $k => $v) {
                                                        
                                                        echo "<option value='".$k."' >".$v."</option>";
                                                    }
                                                    ?>
                                                    </select>
                                                </div>
                                           
                                                <div class="input-item">
                                                    <button class="btn btn-icon btn-submit-d-form" tabindex="5"  type="submit" id="frm_submit"><i class="fas fa-lock"></i> Unlock Now</button>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="option-info">Enter Correct Details</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
                <input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
                <input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
                <input type="hidden" id="utm_sub" name="utm_sub" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
                <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
            </form>
        </div>
  
        <footer class="page-footer">
            <p>This product is suitable for age above 18 years. This survey will help all the age groups to stay healthy. Disclaimer : The products results may vary from person to person.</p>
    
            <p><a href="http://www.nutratimes.com/privacy-policy" target="_blank">Privacy Policy</a> | <a href="http://www.nutratimes.com/terms-and-conditions" target="_blank">Terms & Conditions</a></p> 
        </footer>                                           
    </div>
    <div class="mobile">
        <section>
            <div class="step-form">
                <form class="form-elements" name="mobileForm" id="mobileForm" action="<?php echo SITE_URL?>nutratimes/submitfrm" method="post">
                    <section class="quiz-block-list quiz-block-list-mobile-view">
                        <div class="step step-1 active">
                            <div class="banner">
                                <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/mobile-doctors.jpg" alt="nutratimes" title="Nutratimes" />
                            </div>
                            <h2 class="page-title">Take a Quiz Journey to know<br>"your healthy weight"</h2>
                            <h2 class="page-title" style="font-size: 18px;  margin-bottom: 0;color: #4CAF50;">Know Your Healthy Body Mass Index (BMI)</h2>
                            <div class="mobile-content">
                                <div class="quiz-block-list_item">
                                    <div class="question-block">
                                        <div>
                                            <div class="options">
                                                <h5 class="option-title">
                                                    My Age is
                                                </h5>
                                                <div class="option-list age-list">
                                                    <!-- <div class="option-item">
                                                        <input type="radio" id="age-bellow-15-mob" name="ageGroup">
                                                        <label for="age-bellow-15-mob">
                                                            <span class="text">Below</span>
                                                            <span class="value">18</span>
                                                        </label>
                                                    </div> -->
                                                    <div class="option-item">
                                                        <input type="radio" id="age-18-35-mob" name="ageGroup">
                                                        <label for="age-18-35-mob">
                                                            <span class="text">Between</span>
                                                            <span class="value">18 to 35</span>
                                                        </label>
                                                    </div>
                                                    <div class="option-item">
                                                        <input type="radio" id="age-35-45-mob" name="ageGroup">
                                                        <label for="age-35-45-mob">
                                                            <span class="text">Between</span>
                                                            <span class="value">35 to 45</span>
                                                        </label>
                                                    </div>
                                                    <div class="option-item">
                                                        <input type="radio" id="age-45-55-mob" name="ageGroup">
                                                        <label for="age-45-55-mob">
                                                            <span class="text">Between</span>
                                                            <span class="value">45 to 55</span>
                                                        </label>
                                                    </div>
                                                    <div class="option-item">
                                                        <input type="radio" id="age-above-55-mob" name="ageGroup">
                                                        <label for="age-above-55-mob">
                                                            <span class="text">Above</span>
                                                            <span class="value">55</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <p class="option-info">Select Your Age</p>
                                            </div>
                                        </div>
                                        <div class="button-block">
                                            <button class="btn btn-icon btn-prev-mob disabled"><i class="fas fa-chevron-left"></i></button>
                                            <button class="btn btn-icon btn-next-mob"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="step step-2">
                            <div class="banner">
                                <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/mobile-gender.jpg" alt="nutratimes" title="Nutratimes" />
                            </div>
                            <h2 class="page-title">Take a Quiz Journey to know<br>"your healthy weight"</h2>
                            <h2 class="page-title" style="font-size: 18px;  margin-bottom: 0;color: #4CAF50;">Know Your Healthy Body Mass Index (BMI)</h2>
                            <div class="mobile-content">

                                <div class="quiz-block-list_item">

                                    <div class="question-block">
                                        <div>
                                            <div class="options">
                                                <h5 class="option-title">
                                                    My Gender is
                                                </h5>
                                                <div class="option-list">
                                                    <div class="option-item bg-blue">
                                                        <input type="radio" id="male-mob" value="gender-male" name="gender">
                                                        <label for="male-mob">
                                                            <span class="icon"><i class="fas fa-mars"></i></span>
                                                            <span class="text">Male</span>
                                                        </label>
                                                    </div>
                                                    <div class="option-item bg-red">
                                                        <input type="radio" id="female-mob" value="gender-female" name="gender">
                                                        <label for="female-mob">
                                                            <span class="icon"><i class="fas fa-venus"></i></span>
                                                            <span class="text">Female</span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <p class="option-info">Select Your Gender</p>
                                            </div>
                                        </div>
                                        <div class="button-block">
                                            <button class="btn btn-icon btn-prev-mob"><i class="fas fa-chevron-left"></i></button>
                                            <button class="btn btn-icon btn-next-mob"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="step step-3 ">
                            <div class="banner based-on-gender">
                                <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/mobile-boy-height.jpg" class="male-img" alt="nutratimes" title="Nutratimes" />
                                <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/mobile-girl-height.jpg" class="female-img" alt="nutratimes" title="Nutratimes" />
                            </div>
                            <h2 class="page-title">Take a Quiz Journey to know<br>"your healthy weight"</h2>
                            <h2 class="page-title" style="font-size: 18px;  margin-bottom: 0;color: #4CAF50;">Know Your Healthy Body Mass Index (BMI)</h2>
                            <div class="mobile-content">
                                <div class="quiz-block-list_item based-on-gender">

                                    <div class="question-block">
                                        <div>

                                            <div class="options">
                                                <h5 class="option-title">
                                                    My Height is
                                                </h5>
                                                <div class="option-list">
                                                    <div>
                                                        <div class="input-item flex-box">
                                                            <input type="number" class="form-control mobileheightValue"
                                                                value="100" id="mobileheightValue" name="heightValue" >
                                                            <span>CM</span>
                                                        </div>
                                                        <!-- <div class="input-range">
                                                            <input class="mobileheightRange" type="range" min="100" max="200"
                                                                step="0.1" value="80" data-orientation="horizontal" name="height" id="height">
                                                        </div> -->
                                                    </div>
                                                </div>
                                                <!-- <div class="blink_me">Please Use Yellow Pointer on The scale to change the height.</div> -->
                                                <p class="option-info">Select Your Height</p>
                                            </div>
                                        </div>
                                        <div class="button-block">
                                            <button class="btn btn-icon btn-prev-mob"><i class="fas fa-chevron-left"></i></button>
                                            <button class="btn btn-icon btn-next-mob"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="step step-4">
                            <div class="banner based-on-gender">
                                <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/mobile-boy-weight.jpg" class="male-img" alt="nutratimes" title="Nutratimes" />
                                <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/mobile-girl-weight.jpg" class="female-img" alt="nutratimes" title="Nutratimes" />
                            </div>
                            <h2 class="page-title">Take a Quiz Journey to know<br>"your healthy weight"</h2>
                            <h2 class="page-title" style="font-size: 18px;  margin-bottom: 0;color: #4CAF50;">Know Your Healthy Body Mass Index (BMI)</h2>
                            <div class="mobile-content">
                                <div class="quiz-block-list_item based-on-gender">

                                    <div class="question-block">
                                        <div>
                                            <div class="options">
                                                <h5 class="option-title">
                                                    My Weight is
                                                </h5>
                                                <div class="option-list">
                                                    <div>
                                                        <div class="input-item flex-box">
                                                            <input type="number" class="form-control mobileweightValue"
                                                                value="20" id="mobileweightValue" name="weightValue" >
                                                            <span>KG</span>
                                                        </div>
                                                        <!-- <div class="input-range">
                                                            <input class="mobileweightRange" type="range" min="10" max="200"
                                                                step="0.5" value="80" data-orientation="horizontal" name="weight" id="weight">
                                                        </div> -->
                                                    </div>
                                                </div>
                                                <!-- <div class="blink_me">Please Use Yellow Pointer on The scale to change the height.</div> -->
                                                <p class="option-info">Select Your Weight</p>
                                            </div>
                                        </div>
                                        <div class="button-block">
                                            <button class="btn btn-icon btn-prev-mob"><i class="fas fa-chevron-left"></i></button>
                                            <button class="btn btn-icon btn-next-mob"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="step step-5">
                            <div class="banner">
                                <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/mobile-header.jpg" alt="nutratimes" title="Nutratimes" />
                            </div>
                            <h2 class="page-title">Take a Quiz Journey to know<br>"your healthy weight"</h2>
                            <h2 class="page-title" style="font-size: 18px;  margin-bottom: 0;color: #FF5722;">Last Step to Know Your <br>Body Mass Index (BMI) to stay Fit</h2>
                            <div class="mobile-content">
                                <div class="quiz-block-list_item">

                                    <div class="question-block">
                                        <div>

                                            <div class="options">
                                                <h5 class="option-title">
                                                    Fill the Details to see the Result
                                                </h5>
                                                <div class="option-list form-details">
                                                    <div>
                                                        <div class="input-item">
                                                            <input type="text" tabindex="1" name="userName" class="form-control" placeholder="Name">
                                                        </div>
                                                        <div class="input-item">
                                                            <input type="email" tabindex="2" name="userEmail" class="form-control" placeholder="Email">
                                                        </div>
                                                        <div class="otp-box">
                                                            <div class="input-item">
                                                                <input type="text" tabindex="3" name="userPhone" class="form-control phone_enter"  placeholder="Mobile">
                                                            </div>
                                                           <div class="box">
                                                                <div class="input-item">
                                                                    <input type="number" tabindex="" name="otp" class="form-control otp_enter" id="otp" placeholder="OTP" name="otp">
                                                                     <span id="otp_error" class="otp_error"></span>
                                                                </div>
                                                                <div class="input-item text-center" id="show_resend">
                                                                     <span id="otp_sent" class="otp_sent"></span>
                                                                    <span><a href="javascript:otpresend('<?php echo SITE_URL?>nutratimes/otp_resend', 'm')">Click Here</a> To Resend OTP</span>
                                                                </div>
                                                             </div>
                                                        </div>    
                                                        <div class="input-item">
                                                            <select name="state" tabindex="4" id="state" data-attr="Please enter correct state" class="form-control">
                                                            <option value="">Select State</option>
                                                            <?php 
                                                            foreach ($all_array['NUTRATIMES_STATE_ARR'] as $k => $v) {
                                                                
                                                                echo "<option value='".$k."' >".$v."</option>";
                                                            }
                                                            ?>
                                                            </select>
                                                        </div>
                                                        <div class="input-item">
                                                            <button type="submit" tabindex="5" class="btn btn-icon"><i class="fas fa-lock"></i> Unlock
                                                                Now</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <p class="option-info">Enter Correct Details</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
                    <input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
                    <input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
                    <input type="hidden" id="utm_sub" name="utm_sub" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
                    <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
                </form>
            </div>
        </section>
        <footer class="page-footer">
            <p>This product is suitable for age above 18 years. This survey will help all the age groups to stay healthy. Disclaimer : The products results may vary from person to person.</p>
    
            <p><a href="http://www.nutratimes.com/privacy-policy" target="_blank">Privacy Policy</a> | <a href="http://www.nutratimes.com/terms-and-conditions" target="_blank">Terms & Conditions</a></p> 
        </footer> 
    </div>
    <div id="loader">
        <div class="indicator" >
            <svg width="16px" height="12px">
                <polyline id="back" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>
                <polyline id="front" points="1 6 4 6 6 11 10 1 12 6 15 6"></polyline>
            </svg>
            
        </div>
         <div class="loader">Calculating</div>
    </div>
    <!--<div class="result-page" id="result">
        <header>
            <div class="container">
                <div class="logo">
                    <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/nutratimes_logo.png" alt="nutratimes_logo" title="Nutratimes" />
                </div>
                <nav class="navbar">
                    <ul class="nav-list">
                       <!-- <li class="nav-list_item">
                            <a href="">Real Stories</a>
                        </li>
                        <li class="nav-list_item">
                            <a href="">Real Stories</a>
                        </li>
                        <li class="nav-list_item">
                            <a href="">Real Stories</a>
                        </li>
                        <li class="nav-list_item">
                            <a href="">Real Stories</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
        <div class="container">
            <div class="user-list">
                <p>
                    <span class="highlight">Dear <span id="user_name"></span>, Your <span id="calculation_Type"></span> Is
                        <span id="user_score"></span>,
                        You Are
                        <span id="user_status"></span>!.</span>
                    Below is the suitable natural and healthy product for your weightloss!
                </p>
            </div>
            <div class="result-card bmi-card" style="display:none;">
                <div class="chart">
                    <div id="gauge"></div>
                </div>
                <div class="details">
                    <span class="flex-box heading-block">
                        <span class="title" style="text-decoration: underline; font-weight: 500">BMI</span>
                        <span class="details" style="text-decoration: underline;  font-weight: 500">CLASSIFICATION</span>
                    </span>
                    <span class="flex-box content-block low-weight">
                        <span class="title">Bellow 18.5</span>
                        <span class="details">Low Weight</span>
                    </span>
                    <span class="flex-box content-block normal-weight">
                        <span class="title">18.5 to 24.9</span>
                        <span class="details">Normnal Weight</span>
                    </span>
                    <span class="flex-box content-block over-weight">
                        <span class="title">25 to 29.9</span>
                        <span class="details">Overweight</span>
                    </span>
                    <span class="flex-box content-block class-one">
                        <span class="title">30 t0 34.9</span>
                        <span class="details">Class I obese</span>
                    </span>
                    <span class="flex-box content-block class-two">
                        <span class="title">35 to 39.9</span>
                        <span class="details">Class II obese</span>
                    </span>
                    <span class="flex-box content-block class-three">
                        <span class="title">40 Upwards</span>
                        <span class="details">Class III obese</span>
                    </span>
                </div>
            </div>
            <div class="main-product-block">
                <div class="row">
                    <div class="col-5">
                        <div class="product-image">
                            <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/main-product.png" alt="nutratimes_logo" title="Nutratimes" />
                        </div>

                    </div>
                    <div class="col-7">
                        <div class="product-details">
                            <h2 class="title">Garcinia Cambogia Herbs -1 Bottle</h2>
                            <div class="price-block">
                                <span class="actual-price">Rs 1785</span>
                                <span class="discount-price">Rs 1984 <small>(you save Rs 199)</small></span>
                            </div>
                            <div class="review">
                                <span class="review-icons">
                                    <i class="fa fa-star checked"></i>
                                    <i class="fa fa-star checked"></i>
                                    <i class="fa fa-star checked"></i>
                                    <i class="fa fa-star checked"></i>
                                    <i class="fa fa-star checked"></i>
                                </span>
                                <span class="review-value">
                                    (12575 Customer's reviews)
                                </span>
                            </div>
                            <div class="offer-description">
                                <p class="offer-title">Special offer</p>
                                <ul class="offer-list">
                                    <li>Cash on delivery (COD) available for Rs.2288</li>
                                    <!--<li>Cash on delivery (COD) available for Rs.2288</li>
                                    <li>Cash on delivery (COD) available for Rs.2288</li>
                                </ul>
                            </div>
                            <div class="button-block">
                                <span class="green">Free Shipping</span>
                                <button class="btn btn-green btn-order">Place Your Order</button>
                                <p class="offer-title thankyou-msg">Thank you for expressing interest.
                                    Our expert will get in touch with you shortly.</p>
                                <span class="red">Hurry Now! Offer Ends Soon</span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="doctor-section">
            <div class="container">
                <div class="row">
                    <div class="col-3">
                        <div class="doctor-image">
                            <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/doctor.jpg" alt="nutratimes_logo" title="Nutratimes" />
                        </div>
                    </div>
                    <div class="col-9">
                        <div class="doctor-description">
                            <p>The magical fruit of Garcinia Cambogia consists of HCA (Hydroxycitric Acid), an
                                ingredient which has
                                shown amazing results as a weight loss supplement. HCA has proven to be a highly
                                effective agent in
                                decomposing fat, especially the deposits across the belly, thighs, and hips. You can
                                lose up to 7 kgs with
                                a bottle of Nutratimes Garcina Cambogia. Clinical studies and research has proved
                                Garcinia Cambogia’s
                                effectiveness in the conversion of excess fats into energy thus preventing the
                                accumulation of fat cells in
                                the body.</p>
                            <p>Garcinia Cambogia suppresses appetite and also blocks the body’s ability to produce fat
                                helping you
                                maintain your cholesterol and blood sugar levels.</p>
                            <div class="trusted-icon">
                                <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/trusted.png" alt="nutratimes_logo" title="Nutratimes" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="related-products">
                <div class="title-block">
                    <h5>Check Other Packages</h5>
                </div>
                <div class="product-block">
                    <div class="product-item">
                        <div class="image-block">
                            <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/pack-1.png" alt="nutratimes_logo" title="Nutratimes" />
                        </div>
                        <div class="button-block">
                            <button class="btn btn-green  btn-order">Place Your Order</button>
                            <p class="offer-title thankyou-msg">Thank you for expressing interest.
                                    Our expert will get in touch with you shortly.</p>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="image-block">
                            <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/pack-2.png" alt="nutratimes_logo" title="Nutratimes" />
                        </div>
                        <div class="button-block">
                            <button class="btn btn-green  btn-order">Place Your Order</button>
                            <p class="offer-title thankyou-msg">Thank you for expressing interest.
                                    Our expert will get in touch with you shortly.</p>
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="image-block">
                            <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/pack-3.png" alt="nutratimes_logo" title="Nutratimes" />
                        </div>
                        <div class="button-block">
                            <button class="btn btn-green  btn-order">Place Your Order</button>
                            <p class="offer-title thankyou-msg">Thank you for expressing interest.
                                    Our expert will get in touch with you shortly.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonial">
            <div class="container">
              <div class="title-block">
                <h5>Testimonial</h5>
              </div>
              <div class="testimonial-list">
                <figure class="snip1232">
                  <div class="author">
                    <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/testimonial-2.jpeg" alt="testimonial-1" />
                    <h5>Adithi</h5><span>Age : 23 | Lose 18 Kg</span>
                  </div>
                  <blockquote>
                    Excellent ayurvedic product.. It helps me to loss my weight and make me slim.. Thanks
                    Nutratimes for such a nice product
                  </blockquote>
                </figure>
                <figure class="snip1232">
                  <div class="author">
                    <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/testimonial-3.jpeg" alt="testimonial-1" />
                    <h5>Sourabh</h5><span>Age : 28 | Lose 24 Kg</span>
                  </div>
                  <blockquote>
                    Best of the best, as I took daily 2 tablets, and I found the change, one of the useful
                    tablets to get
                    slim and fit.
                  </blockquote>
                </figure>
                <figure class="snip1232">
                  <div class="author">
                    <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/testimonial-4.jpeg" alt="testimonial-1" />
                    <h5>Shandeep</h5><span>Age : 53 | Lose 11 Kg</span>
                  </div>
                  <blockquote>
                    It’s an amazing and awesome product; I have lost my 11 kg weight in 50 days. My weight has
                    reduced from 106 kg to 95 kg. Thanks garcinia cambogia
                  </blockquote>
                </figure>
                <figure class="snip1232">
                  <div class="author">
                    <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/testimonial-5.jpeg" alt="testimonial-1" />
                    <h5>Rohan</h5><span>Age : 41 | Lose 12 Kg</span>
                  </div>
                  <blockquote>
                    It works very well. I took some time to use & see the outcome & then I'm writing this
                    review. Very
                    effective & it doesn't cause any side effect. Within 1 week of using it, I got the visible
                    result. So
                    I'm continuing the use of it. I also do pushups & jogging as my regular workout routine.
                    Overall,
                    this combination is working awesome for me.
                  </blockquote>
                </figure>
                <figure class="snip1232">
                  <div class="author">
                    <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/testimonial-1.jpg" alt="testimonial-1" />
                    <h5>Gowthami</h5><span>Age : 29 | Lose 15 Kg</span>
                  </div>
                  <blockquote>
                    Im working for a prefessor in a girls goverment college in thane, mumbai. I recemmed this
                    product to everyone. Try This, it hardly cost 2000 per bottle for a month
                  </blockquote>
                </figure>
                <figure class="snip1232">
                  <div class="author">
                    <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/testimonial-6.jpeg" alt="testimonial-1" />
                    <h5>Raj</h5><span>Age : 33 | Lose 22 Kg</span>
                  </div>
                  <blockquote>
                    I have been taking this product as prescribed for the last two weeks and I have lost
                    craving for
                    food already, am eating less portions now. Works fantastic.
                  </blockquote>
                </figure>
              </div>
            </div>
          </div>
        <div class="add-banner text-center">
            <img class="" src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/add.png" />
        </div>


       
      </div> 
        <footer>
            <div class="container">
                <div class="logo">
                    <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/nutratimes_logo.png" alt="nutratimes_logo" title="Nutratimes" />
                </div>
                <nav class="navbar">
                    <ul class="nav-list">
                        <li class="nav-list_item">
                            <a href="">&copy; Nutratimes.All Rights Reserved</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </footer>
    </div>-->


    <div class="result-page" id="old">
        <header>
            <div class="container">
                <div class="logo">
                    <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/nutratimes_logo.png" alt="nutratimes_logo" title="Nutratimes" />
                </div>
                <nav class="navbar">
                    <ul class="nav-list">
                       <!-- <li class="nav-list_item">
                            <a href="">Real Stories</a>
                        </li>
                        <li class="nav-list_item">
                            <a href="">Real Stories</a>
                        </li>
                        <li class="nav-list_item">
                            <a href="">Real Stories</a>
                        </li>
                        <li class="nav-list_item">
                            <a href="">Real Stories</a>
                        </li>-->
                    </ul>
                </nav>
            </div>
        </header>
        <div class="container">
            <div class="user-list">
                <p>
                    <span class="highlight"> Your are already register.
                </p>
            </div>
            <div class="main-product-block">
                <div class="row">
                    <div class="col-5">
                        <div class="product-image">
                            <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/main-product.png" alt="nutratimes_logo" title="Nutratimes" />
                        </div>

                    </div>
                    <div class="col-7">
                        <div class="product-details">
                            <h2 class="title">Garcinia Cambogia Herbs -1 Bottle</h2>
                            <div class="price-block">
                                <span class="actual-price">Rs 2790</span>
                                <span class="discount-price">Rs 2228 <small>(you save Rs 502)</small></span>
                            </div>
                            <div class="review">
                                <span class="review-icons">
                                    <i class="fa fa-star checked"></i>
                                    <i class="fa fa-star checked"></i>
                                    <i class="fa fa-star checked"></i>
                                    <i class="fa fa-star checked"></i>
                                    <i class="fa fa-star checked"></i>
                                </span>
                                <span class="review-value">
                                    (12575 Customer's reviews)
                                </span>
                            </div>
                            <div class="offer-description">
                                <p class="offer-title">Special offer</p>
                                <ul class="offer-list">
                                    <li>Cash on delivery (COD) available for Rs.2288</li>
                                    <!--<li>Cash on delivery (COD) available for Rs.2288</li>
                                    <li>Cash on delivery (COD) available for Rs.2288</li>-->
                                </ul>
                            </div>
                            <div class="button-block">
                                <span class="green">Free Shipping</span>
                                <button class="btn btn-green btn-order" disabled>Place Your Order</button>
                                <p class="offer-title thankyou-msg">Thank you for expressing interest.
                                    Our expert will get in touch with you shortly.</p>
                                <span class="red">Hurry Now! Offer Ends Soon</span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="doctor-section">
            <div class="container">
                <div class="row">
                    <div class="col-3">
                        <div class="doctor-image">
                            <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/doctor.jpg" alt="nutratimes_logo" title="Nutratimes" />
                        </div>
                    </div>
                    <div class="col-9">
                        <div class="doctor-description">
                            <p>The magical fruit of Garcinia Cambogia consists of HCA (Hydroxycitric Acid), an
                                ingredient which has
                                shown amazing results as a weight loss supplement. HCA has proven to be a highly
                                effective agent in
                                decomposing fat, especially the deposits across the belly, thighs, and hips. You can
                                lose up to 7 kgs with
                                a bottle of Nutratimes Garcina Cambogia. Clinical studies and research has proved
                                Garcinia Cambogia’s
                                effectiveness in the conversion of excess fats into energy thus preventing the
                                accumulation of fat cells in
                                the body.</p>
                            <p>Garcinia Cambogia suppresses appetite and also blocks the body’s ability to produce fat
                                helping you
                                maintain your cholesterol and blood sugar levels.</p>
                            <div class="trusted-icon">
                                <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/trusted.png" alt="nutratimes_logo" title="Nutratimes" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="related-products">
                <div class="title-block">
                    <h5>Check Other Packages</h5>
                </div>
                <div class="product-block">
                    <div class="product-item">
                        <div class="image-block">
                            <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/pack-1.png" alt="nutratimes_logo" title="Nutratimes" />
                        </div>
                        <div class="button-block">
                           <a href="<?php echo SITE_URL?>nutratimes/order"> <button class="btn btn-green  btn-order">Place Your Order</button></a>
                            <!--<p class="offer-title thankyou-msg">Thank you for expressing interest.
                                    Our expert will get in touch with you shortly.</p>-->
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="image-block">
                            <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/pack-2.png" alt="nutratimes_logo" title="Nutratimes" />
                        </div>
                        <div class="button-block">
                            <a href="<?php echo SITE_URL?>nutratimes/order"> <button class="btn btn-green  btn-order">Place Your Order</button></a>
                            <!--<p class="offer-title thankyou-msg">Thank you for expressing interest.
                                    Our expert will get in touch with you shortly.</p>-->
                        </div>
                    </div>
                    <div class="product-item">
                        <div class="image-block">
                            <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/pack-3.png" alt="nutratimes_logo" title="Nutratimes" />
                        </div>
                        <div class="button-block">
                           <a href="<?php echo SITE_URL?>nutratimes/order"> <button class="btn btn-green  btn-order">Place Your Order</button></a>
                            <!--<p class="offer-title thankyou-msg">Thank you for expressing interest.
                                    Our expert will get in touch with you shortly.</p>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonial">
            <div class="container">
              <div class="title-block">
                <h5>Testimonial</h5>
              </div>
              <div class="testimonial-list">
                <figure class="snip1232">
                  <div class="author">
                    <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/testimonial-2.jpeg" alt="testimonial-1" />
                    <h5>Adithi</h5><span>Age : 23 | Lose 18 Kg</span>
                  </div>
                  <blockquote>
                    Excellent ayurvedic product.. It helps me to loss my weight and make me slim.. Thanks
                    Nutratimes for such a nice product
                  </blockquote>
                </figure>
                <figure class="snip1232">
                  <div class="author">
                    <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/testimonial-3.jpeg" alt="testimonial-1" />
                    <h5>Sourabh</h5><span>Age : 28 | Lose 24 Kg</span>
                  </div>
                  <blockquote>
                    Best of the best, as I took daily 2 tablets, and I found the change, one of the useful
                    tablets to get
                    slim and fit.
                  </blockquote>
                </figure>
                <figure class="snip1232">
                  <div class="author">
                    <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/testimonial-4.jpeg" alt="testimonial-1" />
                    <h5>Shandeep</h5><span>Age : 53 | Lose 11 Kg</span>
                  </div>
                  <blockquote>
                    It’s an amazing and awesome product; I have lost my 11 kg weight in 50 days. My weight has
                    reduced from 106 kg to 95 kg. Thanks garcinia cambogia
                  </blockquote>
                </figure>
                <figure class="snip1232">
                  <div class="author">
                    <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/testimonial-5.jpeg" alt="testimonial-1" />
                    <h5>Rohan</h5><span>Age : 41 | Lose 12 Kg</span>
                  </div>
                  <blockquote>
                    It works very well. I took some time to use & see the outcome & then I'm writing this
                    review. Very
                    effective & it doesn't cause any side effect. Within 1 week of using it, I got the visible
                    result. So
                    I'm continuing the use of it. I also do pushups & jogging as my regular workout routine.
                    Overall,
                    this combination is working awesome for me.
                  </blockquote>
                </figure>
                <figure class="snip1232">
                  <div class="author">
                    <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/testimonial-1.jpg" alt="testimonial-1" />
                    <h5>Gowthami</h5><span>Age : 29 | Lose 15 Kg</span>
                  </div>
                  <blockquote>
                    Im working for a prefessor in a girls goverment college in thane, mumbai. I recemmed this
                    product to everyone. Try This, it hardly cost 2000 per bottle for a month
                  </blockquote>
                </figure>
                <figure class="snip1232">
                  <div class="author">
                    <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/testimonial-6.jpeg" alt="testimonial-1" />
                    <h5>Raj</h5><span>Age : 33 | Lose 22 Kg</span>
                  </div>
                  <blockquote>
                    I have been taking this product as prescribed for the last two weeks and I have lost
                    craving for
                    food already, am eating less portions now. Works fantastic.
                  </blockquote>
                </figure>
              </div>
            </div>
          </div>
        <div class="add-banner text-center">
            <img class="" src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/add.jpg" />
        </div>
        <footer>
            <div class="container">
                <div class="logo">
                    <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/nutratimes_logo.png" alt="nutratimes_logo" title="Nutratimes" />
                </div>
                <nav class="navbar">
                    <ul class="nav-list">
                        <li class="nav-list_item">
                            <a href="">&copy; Nutratimes.All Rights Reserved</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </footer>
    </div>



    <!-- jQuery 3 -->
    <script src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/lib/jquery/jquery-3.3.1.min.js"></script>
    <script src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/lib/jquery-validation-1.17.0/jquery.validate.min.js"></script>
    <!-- Range Slider -->
    <script src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/lib/rangeslider.js-2.3.0/rangeslider.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/lib/bootstrap-4.0.0/js/bootstrap.min.js"></script>

    <!-- Customjs -->
    <script src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/js/vendor.js"></script>
    <script src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/js/custom.js"></script>
</body>

</html>