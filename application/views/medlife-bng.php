<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Book Lab Tests Online - Best Discounts - Certified Technicians</title>
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/medlife-images/favicon.ico" type="image/x-icon" />

      <!-- Bootstrap -->
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/medlife-css/bootstrap.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/medlife-css/new-age.min.css">
      <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body id="page-top">
      <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
         <div class="container text-center">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">                
               <a class="navbar-brand page-scroll" href="#page-top">
               <img src="<?php echo S3_URL?>/site/medlife-images/logo.jpg" class="logo"/>
               </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1 ">
               <h4 style="color: #fff;text-shadow: 1px 1px 5px #2562bb;font-size: 22px;
                  margin: 40px 0px;"></h4>
            </div>
            <!-- /.navbar-collapse -->
         </div>
         <!-- /.container-fluid -->
      </nav>
	  
	  
	  <header class="header-bng">
      <?php $all_array = all_arrays(); ?>
         <div class="container form_margin">
            <div class="row">
				<div class="col-md-8 form_box1">
                  <!--<h1>LAB TESTS AT<br> ATTRACTIVE PRICES</h1>-->
               </div>
               <div class="col-md-4 ">
                  <div class="jumbotron form_box">
                     <div class="row text-center" id="f_div" >
                        <div class="text-center  col-md-12 col-lg-12">
                           <h4>BOOK NOW</h4>
                        </div>
                        <div class="text-center col-lg-12">
                           <!-- CONTACT FORM https://github.com/jonmbake/bootstrap3-contact-form -->
                           <form role="form" id="feedbackForm" class="medlife_form" action="JavaScript:void(0)" onsubmit="medlife_jsfrm('<?php echo SITE_URL?>medlife/medlifefrm')">
                              <div class="form-group">
                                 <!--<label for="name">Name</label>-->
                                 <input type="text" class="form-control" id="name" name="name" placeholder="Name" onkeyup="chck_valid_medlife('name', 'Please enter correct name')" data-attr="Please enter correct name">
                                 <span class="help-block" id="name_err"></span>
                              </div>
                              <div class="form-group">
                                 <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" onkeyup="chck_valid_medlife('email', 'Please enter correct email')" data-attr="Please enter correct email">
                                 <span class="help-block" id="email_err" ></span>
                              </div>
                              <div class="form-group">
                                 <input type="text" class="form-control only_numeric" id="phone" name="phone" pattern="\d*" maxlength="10" placeholder="Mobile Number" onkeyup="chck_valid_medlife('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                                 <span class="help-block" id="phone_err"> </span>
                              </div> 
                            
                              <div class="form-group">
                                 <select class="form-control" name="medlife_test" id="medlife_test" onchange="chck_valid_medlife('medlife_test', 'Please select one Test')" data-attr="Please select one Test"> 
                                    <option value="">Select Test</option>
                                     <?php foreach($all_array['MEDLIFE_TEST'] as $k=>$l){
											if($k > 4){	
                                    
												echo "<option value='".$k."'>".$l."</option>";
											} 
										}
									?>
                                 </select>
                                 <span class="help-block" id="medlife_test_err"></span>
                              </div>
							  
							  <input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : "d_sr"); ?>">
							<input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "d_ut"); ?>">
                              
                              <input type="submit" id="frm-sbmtbtn" class="btn btn-primary btn-lg" value="SUBMIT">
                           </form>
                           <!-- END CONTACT FORM --> 
                        </div>
                     </div>
                     <div class="row text-center hidden_div" id="success_div">  
							<form role="form" id="otpForm" class="medlife_form" action="JavaScript:void(0)" onsubmit="medlife_otp('<?php echo SITE_URL?>medlife/medlife_otpverification')" >
							 <input type="text" class="form-control" id="otp" name="otp" placeholder="OTP">
							  <input type="hidden"  id="otp_phone" name="otp_phone" >
							  <input type="hidden"  id="otp_email" name="otp_email" >
							 <span class="help-block" id="otp_err"></span>
							  <input type="submit" id="frm-sbmtbtn" class="btn btn-primary btn-lg" value="VERIFY">
							</form>
                     </div>
					 <div class="row text-center hidden_div" id="res_otp_div">  
							<form role="form" id="resendForm" class="medlife_form" action="JavaScript:void(0)" onsubmit="medlife_otp_resend('<?php echo SITE_URL?>medlife/medlife_reset')" >
								 <span class="help-block" id="otp_res_err"></span>
								  <input type="hidden"  id="res_id" name="res_id" >
								  
							  <input type="submit" id="res-sbmtbtn" class="btn btn-primary btn-lg" value="RE-SEND">
							</form>  
                           
                     </div>
					 <div class="row text-center hidden_div" id="success_verify">  
							
                           <h3 style="color:#3c4850; margin-top:50px;font-size: 20px;line-height: 30px;">Thank you for booking your appointment in Medlife Lab.</h3>
                     </div>
					 
                  </div>
               </div>
               
            </div>
         </div>
      </header>
      <section id="features" class="features" >
         <div class="container">
            <div class="row">
               <div class="container-fluid">
				   <h2 class="text-center">PACKAGES <br><br>   </h2>
                  <div class="row">  
                      <div class="col-lg-4 col-sm-12 text-center">
						 <div class=" offfer-box"> 
						 <h1>Medlife Kushal Basic<br> 2-42 Parameters<br> </h1>
								
                        <div class="feature-item" style="min-height:265px"> 
							<li class="package-test-wrapper">CBC- (20)	</li>
							<li class="package-test-wrapper">Blood Sugar (1)-FBS	</li>
							<li class="package-test-wrapper">Lipid Profile-Basic (6)	</li>
							<li class="package-test-wrapper">Renal/Kidney Function-Basic (4)	</li>
							<li class="package-test-wrapper">Liver Function Test-Basic (10)	</li>
							<li class="package-test-wrapper">Thyroid Test (1)-TSH	</li>
                        </div>
							 <div class="brnd-logo"> 
                        <!--<img class="" alt="140x140" style="width: 110px; height: auto;" src="<?php echo S3_URL?>/site/medlife-images/thyrocare.png" data-holder-rendered="true">-->
							 </div>
						<div class="price">
							<div class="bestprice">																
								<div class="actual-price"><span class="uncut">₹1760</span> ₹499</div> Discount 77%
							</div>
							<a  href="#" type="submit" id="frm-sbmtbtn" class="btn btn-default book_app" value="BOOK NOW" data-attr="1">BOOK NOW</a>
						</div>
						</div> 
                     </div>
					  
					  <div class="col-lg-4 col-sm-12 text-center">
						  <div class=" offfer-box">
						 <h1>Medlife Kushal Comprehensive<br> 1-72 Parameters<br> </h1>
                        
						<div class="feature-item" style="min-height:270px"> 
							<li class="package-test-wrapper">Haemogram Comprehensive (22)-CBC+PS+ESR</li>				
							<li class="package-test-wrapper">Blood Sugar (2) - FBS & HbA1c	</li>						
							<li class="package-test-wrapper">Lipid Profile-Basic (6)	</li>						
							<li class="package-test-wrapper">Renal/Kidney Function-(7)	</li>						
							<li class="package-test-wrapper">Liver Function Test-Basic (10)	</li>						
							<li class="package-test-wrapper">Thyroid Test (3)	</li>						
							<li class="package-test-wrapper">Vitamin D3 (1)	</li>						
							<li class="package-test-wrapper">Urine Complete Analysis (21)	</li>							
                        </div>
							   <!--<img class="" alt="140x140" style="width: 110px; height: auto;" src="<?php echo S3_URL?>/site/medlife-images/thyrocare.png" data-holder-rendered="true">-->
						<div class="price">
							<div class="bestprice">																
								<div class="actual-price"><span class="uncut">₹ 4250 </span> ₹ 949</div>Discount 79%
							</div>
							<a  href="#" type="submit" id="frm-sbmtbtn" class="btn btn-default book_app" value="BOOK NOW" data-attr="2">BOOK NOW</a>
						</div>
							  
					 </div>
                     </div>
					  <div class="col-lg-4 col-sm-12 text-center">
						 <div class=" offfer-box"> 
						 <h1>Medlife Kushal Comprehensive <br> 2-76 Parameters<br></h1>
                        
                        <div class="feature-item" style="min-height:270px"> 
							<li class="package-test-wrapper">Haemogram Comprehensive (22)-CBC+PS+ESR</li>	 	
							<li class="package-test-wrapper">Blood Sugar (2) - FBS & HbA1c	</li>	 
							<li class="package-test-wrapper">Lipid Profile-Basic (6)	</li>	 
							<li class="package-test-wrapper">Renal/Kidney Function-(7)	</li>	 
							<li class="package-test-wrapper">Liver Function Test-Basic (10)	</li>	 
							<li class="package-test-wrapper">Thyroid Test (3)	</li>	 
							<li class="package-test-wrapper">Vitamins (2)Vit D3	</li>	 
							<li class="package-test-wrapper">Vitamin B12	</li>	 
							<li class="package-test-wrapper">Pancreas tests (2)	</li>	 
							<li class="package-test-wrapper">Urine Complete Analysis (21)</li>	 
                        </div> 
							  <!--<img class="" alt="140x140" style="width: 110px; height: auto;" src="<?php echo S3_URL?>/site/medlife-images/thyrocare.png" data-holder-rendered="true">-->
					   <div class="price">
							<div class="bestprice">																
								<div class="actual-price"><span class="uncut">₹ 6300 </span> ₹ 1199</div> Discount 84%

							</div>
							<a  href="#" type="submit" id="frm-sbmtbtn" class="btn btn-default book_app" value="BOOK NOW" data-attr="3">BOOK NOW</a>
						</div>
							 
				 </div>
                     </div>
					  <div class="col-lg-2 col-sm-12 text-center"></div>
					  <div class="col-lg-4 col-sm-12 text-center">
						  <div class=" offfer-box">
						 <h1>Medlife Kushal Comprehensive 3-76 Parameters<br>  </h1>
                        
                        <div class="feature-item" style="min-height:290px"> 
							<li class="package-test-wrapper">Haemogram Comprehensive (22)-CBC+PS+ESR	
							<li class="package-test-wrapper">Blood Sugar (2) - FBS & HbA1c	</li>	
							<li class="package-test-wrapper">Lipid Profile-Basic (6)	</li>	
							<li class="package-test-wrapper">Renal/Kidney Function-(7)	</li>	
							<li class="package-test-wrapper">Liver Function Test-Basic (10)	</li>	
							<li class="package-test-wrapper">Thyroid Test (3)	</li>	
							<li class="package-test-wrapper">Vitamins (2)Vit D3	</li>	
							<li class="package-test-wrapper">Vitamin B12	</li>	
							<li class="package-test-wrapper">Pancreas tests (2)	</li>	
							<li class="package-test-wrapper">Tumour Markers (1) - CA125 for Female</li>	 
	   						<li class="package-test-wrapper">PSA for Male	</li>
							<li class="package-test-wrapper">Urine Complete Analysis (21)	</li>

                        </div>
							 <!--<img class="" alt="140x140" style="width: 110px; height: auto;" src="<?php echo S3_URL?>/site/medlife-images/thyrocare.png" data-holder-rendered="true">--> 
							 
							  
							  <div class="price">
							<div class="bestprice">																
								<div class="actual-price"><span class="uncut">₹ 7700 </span> ₹ 2199</div> Discount 74%
							</div>
							<a  href="#" type="submit" id="frm-sbmtbtn" class="btn btn-default book_app" value="BOOK NOW" data-attr="4">BOOK NOW</a>
						</div>
							  </div>
                     </div>
					  <div class="col-lg-4 col-sm-12 text-center">
						  <div class=" offfer-box">
						 <h1>Medlife Kushal Pro <br>1- 75 Parameters<br>  </h1>
                        
                        <div class="feature-item" style="min-height:290px"> 
							<li class="package-test-wrapper">Haemogram Comprehensive (22)-CBC+PS+ESR	</li>
							<li class="package-test-wrapper">Diabetic Screening -(2) FBS & HbA1c	</li>
							<li class="package-test-wrapper">Lipid Profile-Basic (6)	</li>
							<li class="package-test-wrapper">Renal/Kidney Function-(7)	</li>
							<li class="package-test-wrapper">Liver Function Test-Basic (10)	</li>
							<li class="package-test-wrapper">Thyroid Function Test (3)	</li>
							<li class="package-test-wrapper">Iron Profile (4)	</li>
							<li class="package-test-wrapper">Urine Complete Analysis (21)	</li>

                        </div>
							 <!--<img class="" alt="140x140" style="width: 110px; height: auto;" src="<?php echo S3_URL?>/site/medlife-images/thyrocare.png" data-holder-rendered="true">-->
							 
							  
							  <div class="price">
							<div class="bestprice">																
								<div class="actual-price"><span class="uncut">₹ 2975 </span> ₹ 699</div> Discount 80%
							</div>
							<a  href="#" type="submit" id="frm-sbmtbtn" class="btn btn-default book_app" value="BOOK NOW" data-attr="4">BOOK NOW</a>
						</div>
							  </div>
                     </div>
					  <div class="col-lg-2 col-sm-12 text-center"></div>
                  </div>
               </div>
            </div>
         </div>
      </section>
       
       
      <footer>
      	© 2018 - Medlife Diagnostics. All Rights Reserved
      </footer>
      <!-- jQuery -->
      <script src="<?php echo S3_URL?>/site/scripts/jquery-1.11.3.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="<?php echo S3_URL?>/site/scripts/bootstrap.js"></script>
      <!-- Plugin JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
      <!-- Theme JavaScript -->
      <script src="<?php echo S3_URL?>/site/medlife-css/new-age.min.css"></script>
      <script src="<?php echo S3_URL?>/site/scripts/flatpickr.min.js"?>"></script>
      <script src="<?php echo S3_URL?>/site/scripts/default.js"?>"></script>
      <link href="<?php echo S3_URL?>/site/medlife-css/flatpickr.min.css" rel="stylesheet">
      
        <script>
		$(document).ready(function(e) {    
		$(document).on("click", ".book_app", function(e) {
			var option=($(this).attr("data-attr"));
			// $("#medlife_test option[value='"+option+"']").select();
			 $('#medlife_test option:eq('+option+')').prop('selected', true)
		});
		});
		
		

      </script>
   </body>
</html>