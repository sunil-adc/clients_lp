<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Club Oxygen</title>
    <meta name="description" content="Provident Green Park has some of the best 2 & 3 BHK apartments in Coimbatore. These apartments for sale  give you the best value for money.">
    <link rel="shortcut icon" href="<?php echo S3_URL?>/site/oxygen-resort/images/favicon.png" type="image/x-icon" />


    <link rel="stylesheet" href="<?php echo S3_URL?>/site/oxygen-resort/css/vendor.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/oxygen-resort/css/main.css">
    <!-- fonts -->
    <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
        rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
</head>

<body>

    <?php $all_array = all_arrays(); ?>

    <!-- banner -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="header-banner header-banner-desktop"> 
                        <img src="<?php echo S3_URL?>/site/oxygen-resort/images/desktop-banner.jpg" />
                </div>
                <div class="header-banner header-banner-mobile"> 
                        <img src="<?php echo S3_URL?>/site/oxygen-resort/images/mobile-banner.jpg" />
                </div>
                <div class="">
                    <div class="carousel-caption"> 
                        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="puravankara_jsfrm_greenpark('<?php echo SITE_URL?>realestate/puravankara_provident/frm_submit','greenpark','1')">

                            <div class="">
                                <div class="col">
                                    <div class="form-logo">
                                        <a href="#">
                                            <img src="<?php echo S3_URL?>/site/oxygen-resort/images/logo-blue.png" />
                                        </a>
                                    </div> 
                                    <!-- <div class="form-logo">
                                        <a href="#">
                                            <img src="<?php echo S3_URL?>/site/oxygen-resort/images/holiday-for-all.png" />
                                        </a>
                                    </div> -->
                                    <h4>Thank you for expressing interest.
                            Our expert will get in touch with you shortly.</h4>      

                                </div>
                            </div>
                        </form>
                       
                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal -->
    </div>
    <!--//banner -->

    <div>
        <div class="container">        
            <div class="col-md-12">
                <h5 class="text-center" style="padding:30px 0px 10px; font-size:25px;"><strong>Available Destinations</strong> - Munnar, Thekkady, Goa, Alleppey, Ooty, Gir, Manali</h5>
            </div>
        </div>
    </div>
             
    <!-- about -->
    <div class="about" id="about">
        <div class="container">
          
            <div class="col-md-6 about_right">
                <h3>About</h3>
                <h3 class="bold">Club Oxygen</h3>
                <p>
                Chemmanur International Holidays and Resorts Private Limited; 
                which is part of the Hospitality & Leisure Division of the Chemmanur
                 International Group along with the grand project namely The Oxygen City, 
                 is the Chairman, Dr. Boby Chemmanur’s ambitious foray into the hospitality 
                 industry. This resonates in the mission & vision statement which emphasizes 
                 on the dire need to revolutionize the vacation ownership industry & emerge as 
                 the most trusted brand by following the business principles of ensuring credibility 
                 & transparency, simplified product features thereby creating ease of use & a high value 
                 proposition for the customer, all of which should eventually lead 
                 to the much required pull & a complete departure from the conventional timeshare businesses.
                </p>
                



            </div>
            <div class="col-md-6">
                <div class="col-xs-12 aboutimg-w3l aboutimg-w3l2">
                    <img src="<?php echo S3_URL?>/site/oxygen-resort/images/ab5.jpg" alt="image" style="object-position: right;" />
                </div>
         
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!-- //about -->
  
   <!-- popular -->
   <!-- <div class="popular-w3" id="popular">
        <div class="container">
           
            <div class="popular-grids">
                <div class="">

                    <div class="popular-text">
                        <div class="detail-bottom">
                        
                            <div class="rwd-media">
                                 <iframe  src="https://www.youtube.com/embed/2OBqx1dfqyg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div> -->
    <!-- //popular -->   

    <!-- //popular -->
    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">Destinations</h3>
           
            <div class="row">
                <div class="col-md-12">
                    <!-- begin panel group -->
                    <div class="panel-group" id="spec" role="tablist" aria-multiselectable="true">

                        <!-- panel 1 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab1" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingOne" data-toggle="collapse"
                                    data-parent="#spec" href="#scollapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    <h4 class="panel-title collapsed">OXYGEN RESORTS - THE LAKE VIEW, MUNNAR</h4>
                                </div>
                            </span>

                            <div id="scollapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                  <p>Oxygen Resorts, The Lake View Munnar is one of the most sought out resorts in Munnar with a great ambiance and service and It is a valley of dreams covered with mist clouds. To be close to mother nature. You can't even blink your eyes when you see nature's beauty.</p>
                                  <p>Elegant, exclusive and in its essence a state of mind, the Lake View resort in munnar is an exquisite resort located in Munnar, a largely mountainous place in Kerala, a state in the south of India. Situated beneath an enormous canopy of a lush tropical rainforest, journey into nature's wonderland - in the company of spirit of nature that inhabit the dense Lake View Munnar. You will be seduced by the promise of discovery, bliss and exaltation - body, mind and soul.</p>
                                  <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - The Lake View, MUNNAR" href="<?php echo S3_URL?>/site/oxygen-resort/images/lake-view.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/lake-view.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                    <!-- <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - The Lake View, MUNNAR" href="<?php echo S3_URL?>/site/oxygen-resort/images/m2.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/m2.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                        <!-- / panel 1 -->

                        <!-- panel 2 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab2" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse"
                                    data-parent="#spec" href="#scollapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">OXYGEN RESORTS - WILD CORRIDOR, THEKKADY</h4>
                                </div> 
                            </span>

                            <div id="scollapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                   <p>Lavish. Natural. Serene. The name Thekkady Wild Corridor itself invites a sense of plush nature along with its native name. Located right in Idukki’s green heart of Periyar National Park, this hotel breathes right amidst thick evergreen forests which is home to a wild and meditative experience. This secluded, eco-friendly, and elevated boutique retreat offers an imperturbable episode of calmness, waves of laughter, and living that comes with its top-notch villas and private rooms. The Wild Corridor Resort & Spa by Oxygen Resorts gives you nothing but premium service and palatial rooms. With breathtaking views from the top rooms of the villas, one can sip tea in his comfy chairs inside the luxurious room while treating your eyes to the clouds touching the mountain top. This tangerine and wooden ochre dressed boutique houses a multi-cuisine restaurant giving you a global essence. It also has a poolside BBQ if you want to relish while you swim and dance. Apart from this, you can have your toll of fun in the game room which also has a ping pong table. Adding to the wonder of the cozy rooms is an Ayurvedic Spa facility. This sybaritic boutique gives you a double dose of coziness. It also has a spice plantation in one of its garden. The boutique welcomes people from any part of the globe as it has a multilingual staff who greets you warmly and offers a friendly hand.</p>
                                   <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - WILD CORRIDOR, THEKKADY" href="<?php echo S3_URL?>/site/oxygen-resort/images/t1.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/t1.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                    <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - WILD CORRIDOR, THEKKADY" href="<?php echo S3_URL?>/site/oxygen-resort/images/t2.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/t2.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 2 -->

                        <!-- panel 3 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab3" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingThree" data-toggle="collapse"
                                    data-parent="#spec" href="#scollapseThree" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">OXYGEN RESORTS - HAVELI BACKWATER, ALLEPPEY</h4>
                                </div> 
                            </span>

                            <div id="scollapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                   <p>Nestled parallel to the backwaters of Kerala, Haveli Backwater Resorts is an idle place to lodge at your tour to Alleppey (Alappuzha). This posada offers a great view of backwaters and greenery on every side. Haveli Resort, as the name might prompt; exhibits a Rajasthani architect and built. Oozing with royalty and traditional décor, it is the most preferred and loved resorts by the tourists coming from in and around states. The majestic frontage welcomes you with the classic haveli structure. The infrastructures and interiors of the rooms and suits carry forward the traditional theme exhibited on the exteriors. The rooms are designed with utmost care keeping in mind that the visitors enjoy a merry time with their family and friends. Every room is spacious and has a great view to look at from the balconies. The professional yet friendly staff members caters to your requests while you explore the amenities. Guests can avail a free, wide parking space and a lavish swimming pool on the first floor. And just when you’re about to settle in the hotel, you can hear the waters calling you from just across the road. Boat tours could be enjoyed from the main gate of the hotel. And if you wish to indulge yourself in some body therapy, there is an in-house spa presenting traditional Ayurveda, Swedish Spa at your service. And is any hotel guide complete without talking about its food? Haveli resort offers a Multi-Cuisine Restaurant named Sarangi serving all meal periods with the finest mouthwatering delights from all over the world.</p>
                                   <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - HAVELI BACKWATER, ALLEPPEY" href="<?php echo S3_URL?>/site/oxygen-resort/images/a1.png">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/a1.png" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                    <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - HAVELI BACKWATER, ALLEPPEY" href="<?php echo S3_URL?>/site/oxygen-resort/images/a2.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/a2.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 3 -->
             

                        <!-- panel 4 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab4" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingThree" data-toggle="collapse"
                                    data-parent="#spec" href="#scollapseT" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">OXYGEN RESORTS - COUNTRY TAVERN, OOTY</h4>
                                </div> 
                            </span>

                            <div id="scollapseT" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                   <p>Oxygen Resorts at Country Tavern, Ooty has the splendour of the old world charm nestled in the beautiful hill-town of Ooty. This colonial bungalow comprises of a building dating from 19th century, has old-school fireplaces, huge bay windows, four-poster beds and antique décor within it which itself is an appealing transformation from our modern lives in cities. The major attraction of the property is the old smoke-less fireplace which is considered as a British marvel present in the guest rooms. The premises have a nice stretch of greenery inside with a lot of open area which further helps one relax. An English library, Antique chess rooms are amongst the variety of facilities provided inside the British Colonial building. Oxygen Resorts at Country Tavern features within it the following which consist of a restaurant, coffee shop, book shop, arcade room, garden, business centre, banquet hall, nightclub and gift shop. There is a lobby with travel counter, 24-hour front desk and luggage storage space is also available within the premises. Oxygen Resorts at Country Tavern in Ooty is a cosy holiday resort in an utterly peaceful setting.</p><p>Oxygen Resorts at Country Tavern is situated in Ooty, 2.9 km from Ooty Lake. It provides guestrooms with free Wi-Fi. The resort has a gorgeous garden and is close to more than a few well-known attractions, around a 9-minute walk from Ooty Rose Garden, 1.7 km from Hebron School Ooty and 1.8 km from Ooty Botanical Gardens. It is also situated at a distance of 2 km from Stone House Government Museum.</p>
                                   <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - COUNTRY TAVERN, OOTY" href="<?php echo S3_URL?>/site/oxygen-resort/images/o1.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/o1.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                    <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - COUNTRY TAVERN, OOTY" href="<?php echo S3_URL?>/site/oxygen-resort/images/o2.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/o2.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 4 -->


                        <!-- panel 5 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab5" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingThree" data-toggle="collapse"
                                    data-parent="#spec" href="#scollapseM" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">OXYGEN RESORTS - THE HOLIDAY CLUB, MANALI</h4>
                                </div> 
                            </span>

                            <div id="scollapseM" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                   <p>Oxygen Resorts at The Holiday Club, Manali has the clean and ventilated rooms in the hotel are well-maintained, perfectly designed and crafted as per the taste of guests to make their stay relaxed and comfortable comes with amenities like attached bathroom, hot and cold water.</p>
                                   <p>Oxygen Resorts at The Holiday Club, Manali is situated in Manali, a popular hill station in India that is tucked at a mighty height amid the natural hues of the Himachal Himalayas. The nearest airport is the Bhuntur Airport and the closest rail-head is the Jogindernagar Railway Station.</p>
                                   <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - THE HOLIDAY CLUB, MANALI" href="<?php echo S3_URL?>/site/oxygen-resort/images/the-holidays-club.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/the-holidays-club.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                    <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - THE HOLIDAY CLUB, MANALI" href="<?php echo S3_URL?>/site/oxygen-resort/images/o3.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/o3.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 5 -->


                        <!-- panel 6 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab6" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingThree" data-toggle="collapse"
                                    data-parent="#spec" href="#scollapseGir" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">OXYGEN RESORTS - THE LION'S DEN, GIR</h4>
                                </div> 
                            </span>

                            <div id="scollapseGir" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                   <p>Oxygen Resorts at The Lion’s Den, Gir has the unique concept ideal for families and large groups with 2 bedroom villas spread over acres of green forested area and mango orchard. It’s a weekend retreat with 2 Bedroom Family Villas, Club House, swimming pool, restaurant and a mini theatre.Lion’s Den Weekend Home, a perfect natural beauty within the Gir National Park, surrounded by mango orchard and a monsoon fed river, offers a relaxing atmosphere. The Restaurant serve authentic local kathiawari fresh food.</p>
                                   
                                   <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - THE LION'S DEN, GIR" href="<?php echo S3_URL?>/site/oxygen-resort/images/den.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/den.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                    <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - THE LION'S DEN, GIR" href="<?php echo S3_URL?>/site/oxygen-resort/images/o4.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/o4.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 6 -->


                        <!-- panel 7 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab6" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingThree" data-toggle="collapse"
                                    data-parent="#spec" href="#scollapseg" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">OXYGEN RESORTS - GOA</h4>
                                </div> 
                            </span>

                            <div id="scollapseg" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                   <p>Stunning sunsets, gorgeous beaches, golden sands, swaying palm trees, imposing churches, Portuguese sausages to port wine, Goa is a paradise for the traveler’s eye. A friendly backpacker’s destination that offers the perfect blend of beauty and fun to unwind, relax and rejuvenate, Goa can you get you covered for all. Also called as the “Rome of the East” the tranquil city celebrates a diversity of cultures Indian and European brought perfectly in all aspects of its life surrounding food, music, visual arts and lifestyle. Goa has something for everyone at all times of the year, from the pompous Christmas and New Year’s celebrations to the lovely monsoon lovers bucket list when the countryside is a patchwork of lush green, there is hardly an off-season that keeps Goa uninviting. Being a coastal destination known as the Konkan, Goa remains warm and humid all through the year with the month of May as the hottest with day temperatures soaring almost to 35 degrees while monsoons usually cooler at 28 degrees with abundant downpours and the one of the most scenic captures of Goa. The party capital is a favorite hotspot around November to March when the weather is balmy and pleasant at 22 degrees keeping holiday makers spirits alive with festivities and celebrations and for the late night lovers endless fun at the beach shacks that come to life.</p>
                                   <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - GOA" href="<?php echo S3_URL?>/site/oxygen-resort/images/g1.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/g1.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 7 -->                        

                    </div> <!-- / panel-group -->

                </div> <!-- /col-md-4 -->
             
            </div>
            <!--/ .row -->
        </div> 
    </div>
<div id="pi">
       <?php
     $vr = $this->session->userdata('user_id');
    
     $ut = $this->session->userdata('utm_source');
     if( isset($vr) &&  $vr > 0 && $ut != ""){
       echo $this->lead_check->set_pixel($vr,OXYGENCLUB);
       
     }
     ?>
      </div> 

  
    <!-- projects -->
    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">UpComming</h3>
            <div class="agile_gallery_grids w3-agile demo">
                <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Project Plan" href="<?php echo S3_URL?>/site/oxygen-resort/images/loc.png">
                            <img style="height: auto;" src="<?php echo S3_URL?>/site/oxygen-resort/images/loc.png"
                                alt=" " class="img-responsive" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //projects -->

    <!-- contact -->
    <div class="address" id="contact">
        <div class="container">
            <h3 class="title">Contact Us</h3>
            <div class="address-row">
              
            <div class="col-md-offset-4 col-xs-offset-0 col-md-4 col-xs-6 address-right">
                <div class="address-info wow fadeInRight animated text-center" data-wow-delay=".5s">
                    <h4>Call Us</h4>
                    <p><strong>From 9am to 6PM</strong><br><strong><a href="tel:18003157171">18003157171</a></strong><br><strong><a href="tel:02262770000">022-62770000</a></strong></p>
                </div>
            </div>
               

            </div>
        </div>
    </div>
    <!--//contact-->
    <footer>
        <div class="copy-right-grids">
            <p class="footer-gd">© 2018 Chemmanur International Holidays and Resorts Private Limited. All rights reserved.</p>
        </div>
    </footer>
    

 


            <script src="<?php echo S3_URL?>/site/oxygen-resort/js/vendor.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
            <script src="<?php echo S3_URL?>/site/oxygen-resort/js/main.js"></script>
           
</body>

</html>