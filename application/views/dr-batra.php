<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dr. Batra</title>
	<link rel="shortcut icon" href="http://www.drbatras.com/sites/default/files/favicon_0.ico" type="image/vnd.microsoft.icon"/>
	<!-- Bootstrap -->
	<link rel="stylesheet" href="<?php echo S3_URL?>/site/css/bootstrap.css">
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
</head>

<body>
	<nav class="navbar navbar-default navbar-inverse">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<a class="navbar-brand" href="#">
	  			<img src="<?php echo S3_URL?>/site/images/logo.png" alt="" width="144px" height="auto"/> </a>
			
			</div>
		</div> 
	</nav>
	<section style="background: #bcc9d1">
		<div class="container banner">
			<div class="row">
				<div class="col-xs12">
					<div class="form" id="batra_div">
						<form method="post" action="javascript:void(0)" onsubmit="batra_jsfrm('<?php echo SITE_URL?>drbatra/drbapical')" id="batra_frm" name="batra_frm">
							<h3>Book a <strong>FREE CONSULTATION</strong></h3>
							<p>Find out our Hair Loss Treatment today!</p>
							<div id="user_info" class="userinfo"></div>
							<div class="form-group">
								<input type="text" class="form-control" id="name" name="name" placeholder="Your Name" onkeyup="onkeyup_valid('name', 'Please enter correct name')">
							</div>
							<div class="form-group">
								<input type="email" class="form-control" id="email" name="email" placeholder="Your email" onkeyup="onkeyup_valid('email', 'Please enter correct email')">
							</div>
							<div class="form-group">
								<input type="text" class="form-control only_numeric" id="phone" name="phone" pattern="\d*" maxlength="10" placeholder="Your Telephone" onkeyup="onkeyup_valid('phone', 'Please enter correct phone number')">
							</div>
							<div class="form-group">
								<select class="form-control" id="city" name="city" onchange="onchange_valid('city', 'Please enter correct city')">
									<option value="">Select City</option>
									<?php
									if ( count( $city_arr ) > 0 && is_array( $city_arr ) ) {
										foreach ( $city_arr as $v ) {

											echo "<option value='" . $v[ 'city' ] . "'>" . $v[ 'city' ] . "</option>";

										}
									}
									?>

								</select>
							</div>
							
							<input type="hidden" id="pub" name="pub" value="<?php echo (isset($_REQUEST['publisher']) != "" ? $_REQUEST['publisher'] : "d_pb"); ?>">
							<input type="hidden" id="source" name="source" value="<?php echo (isset($_REQUEST['source']) != "" ? $_REQUEST['source'] : "d_sr"); ?>">
							<input type="hidden" id="utm" name="utm" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "d_ut"); ?>">
							
							<input type="submit" class="btn btn-default" id="frm-sbmtbtn" value="Submit">
						</form>
					</div>

					<div id="success_div" class="form hidden_dev hidden_div">
						<h1>Thank you for choosing Dr. Batra's. We will get in touch with you shortly.</h1>
					</div>
					
					
					
					
					
					

				</div>
			</div>
		</div>
	</section> 
	<section>
		<div class="container">

			<div class="row process">
				<h1>Easy 5 Steps to Restore Your Hair</h1>
				<div class="col-md-5ths col-ss-12">
					<img src="<?php echo S3_URL?>/site/images/b.png" width="50" height="50" alt=""/>
					<div class="clearfix"></div>
					Visit any of our 220+ clinics across 120+ cities and consult our expert hair doctors
				</div>
				<div class="col-md-5ths col-ss-12">
					<img src="<?php echo S3_URL?>/site/images/c.png" width="50" height="50" alt=""/>
					<div class="clearfix"></div>
					Take a painless computerized quick hair test
				</div>
				<div class="col-md-5ths col-ss-12">
					<img src="<?php echo S3_URL?>/site/images/d.png" width="50" height="50" alt=""/>
					<div class="clearfix"></div>
					personalized health plan as per your hair issues offered.
				</div>
				<div class="col-md-5ths col-ss-12">
					<img src="<?php echo S3_URL?>/site/images/e.png" width="50" height="50" alt=""/>
					<div class="clearfix"></div>
					Convenient treatment follow-ups once a month
				</div>
				<div class="col-md-5ths col-ss-12">
					<img src="<?php echo S3_URL?>/site/images/a.png" width="50" height="50" alt=""/>
					<div class="clearfix"></div>
					Book an appointment online in less than 30 seconds </div>
			</div>

			<div class="row">
				<div class="col-md-8">
					<h3 class="title1">About Homeopathy</h3>
					<p>Homeopathy aims at stimulating the human body's defence system. The body's defence mechanisms and processes in turn prevent or treat an illness. The therapy involves small doses of substances that would help produce isolated symptoms of the said condition. This would enable the immune system to adapt and oppose the diagnosed condition. It is a healthier way to overcome ailments because it strengthens the body's own ability to fight diseases without any side effects. more +
					</p>


					<h3 class="title2">hair Loss</h3>
					<p>Hair loss could be a sign of underlying illnesses, such as diabetes and heart disease. It can have negative effects on social life and may also lead to depression. Do not neglect hair loss. Consult Dr Batra’s® Expert Hair Doctors Now.</p>


					<h3 class="title1">Why Dr Batras®</h3>

					<li>Team of over 375 doctors personally trained by Dr Akshay Batra, India’s first trichologist from The Trichological Society of London & Immediate Past President, TTS London.</li>
					<li>Successfully treated over 3 lakh hair loss patients.</li>
					<li>Treatment without any side-effects.</li>
					<li>From instant to permanent solutions for hair growth.</li>
					<li>94% patient satisfaction rate (American Quality Assessors).</li>
					<li>Personalized treatment for every patient</li>

				</div>
				<div class="col-md-4">
					<div class="comp-vid">
						<iframe width="100%" height="200px" src="https://www.youtube.com/embed/EQuqhM_CzbQ" frameborder="0" allowfullscreen></iframe>
					</div>
					<div id="carousel">

						<div class="">
							<div class="quote"><i class="fa fa-quote-left fa-4x"></i>
							</div>
							<div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
								<!-- Carousel indicators -->
								<ol class="carousel-indicators">
									<li data-target="#fade-quote-carousel" data-slide-to="0" class="active"></li>
									<li data-target="#fade-quote-carousel" data-slide-to="1"></li>
									<li data-target="#fade-quote-carousel" data-slide-to="2"></li>
									<li data-target="#fade-quote-carousel" data-slide-to="3"></li>
								</ol>
								<!-- Carousel items -->
								<div class="carousel-inner">
									<div class="item">
										<div class="profile-circle testiomoni1"></div>
										<blockquote>
											<p>"The experience for 2 years of my treatment has been very satisfying. The customer care offered by them is over whelming. The doctor is exceptional. My acne problem improved almost 80% within in a reasonable time frame. I would advise anyone suffering from acne issues to consult Dr...."</p>
										</blockquote>
									</div>
									<div class="item">
										<div class="profile-circle testiomoni2"></div>
										<blockquote>
											<p>"After my 30th session of groHAIR I am very satisfied with the treatment. I felt good at the end of every session, doctors and staff are very helpful and cooperative."</p>
										</blockquote>
									</div>
									<div class="active item">
										<div class="profile-circle testiomoni3"></div>
										<blockquote>
											<p>"I came to Dr Batra's clinic for my daughter's acne problem. My daughter faced this problem as soon as she entered the adolescent stage. Her cheeks were full of acne. With homeopathic treatment at Dr Batra’s, her acne subsided within 8 months. I am very thankful to all the doctors..."</p>
										</blockquote>
									</div>
									<div class="item">
										<div class="profile-circle testiomoni3"></div>
										<blockquote>
											<p>"The treatment is satisfactory. Changes are prominent. Even the conduct and behaviour of the doctors and the staff members are appreciable."</p>
										</blockquote>
									</div>
									<div class="item">
										<div class="profile-circle testiomoni3"></div>
										<blockquote>
											<p>"About a year ago, when our son was diagnosed with vitiligo, we were in a state of complete shock and anxiety. That's when we contacted Dr Batra’s™ clinic, where we consulted Dr Sarabjeet. Her detailed approach put us at ease and we were satisfied that we were in the right hands...."</p>
										</blockquote>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="div_pix"></div>
		</div>
	</section>
	<section>
		<div class="container">
			<div class="col-md-12 text-center" style="background: #efefef; padding: 20px"> © 2014,Dr Batra's™Positive Health Clinic Private Limited.</div>
		</div>
	</section>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<?php echo S3_URL?>/site/scripts/jquery-1.11.3.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="<?php echo S3_URL?>/site/scripts/bootstrap.js"></script>
	<script src="<?php echo S3_URL?>/site/scripts/default.js"></script>
</body>
</html>