<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Book Lab Tests Online - Best Discounts - Certified Technicians</title>
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/medlife-images/favicon.ico" type="image/x-icon" />

      <!-- Bootstrap -->
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/medlife-css/bootstrap.css">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
    
	    <link rel="stylesheet" href="<?php echo S3_URL?>/site/medlife-assets/datepicker/css/bootstrap-datepicker3.min.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/medlife-css/new-age.min.css">
      <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
	  <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119021233-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-119021233-1');
	</script>
	

	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '584563021946205');
	  fbq('track', 'PageView');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=584563021946205&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->
  
    <!-- DO NOT MODIFY -->
  <!-- Quora Pixel Code (JS Helper) -->
  <script>
  !function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
  qp('init', '9187691737054a6baf8d7d028615da3b');
  qp('track', 'ViewContent');
  </script>
  <noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/9187691737054a6baf8d7d028615da3b/pixel?tag=ViewContent&noscript=1"/></noscript>
  <!-- End of Quora Pixel Code -->


      <!-- DO NOT MODIFY -->
      <!-- Quora Pixel Code (JS Helper) -->
      <script>
      !function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
      qp('init', '7cc31ea88ae24531ba00b6301258883a');
      qp('track', 'ViewContent');
      </script>
      <noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/7cc31ea88ae24531ba00b6301258883a/pixel?tag=ViewContent&noscript=1"/></noscript>
      <!-- End of Quora Pixel Code -->

      <!-- Quora Pixel Code (JS Helper) -->
      <script>
      !function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
      qp('init', '114abc52601345698bfe49ffa2f49ab9');
      qp('track', 'ViewContent');
      </script>
      <noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/114abc52601345698bfe49ffa2f49ab9/pixel?tag=ViewContent&noscript=1"/></noscript>
      <!-- End of Quora Pixel Code -->


      <!-- DO NOT MODIFY -->
      <!-- Quora Pixel Code (JS Helper) --> 
      <script>
      !function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
      qp('init', '45fbd1eb043546c5bf1f2c5126c952f9');
      qp('track', 'ViewContent');
      </script>
      <noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/45fbd1eb043546c5bf1f2c5126c952f9/pixel?tag=ViewContent&noscript=1"/></noscript>
      <!-- End of Quora Pixel Code -->


      <!-- DO NOT MODIFY -->
    <!-- Quora Pixel Code (JS Helper) -->
    <script>
    !function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
    qp('init', 'ba33733a1eb646a6a44d843df253b289');
    qp('track', 'ViewContent');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/ba33733a1eb646a6a44d843df253b289/pixel?tag=ViewContent&noscript=1"/></noscript>
    <!-- End of Quora Pixel Code -->

    <!-- DO NOT MODIFY -->
    <!-- Quora Pixel Code (JS Helper) -->
    <script>
    !function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
    qp('init', '0bfdcc6b0af34f41a9a17e74870613dd');
    qp('track', 'ViewContent');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/0bfdcc6b0af34f41a9a17e74870613dd/pixel?tag=ViewContent&noscript=1"/></noscript>
    <!-- End of Quora Pixel Code -->

    <!-- DO NOT MODIFY -->
    <!-- Quora Pixel Code (JS Helper) -->
    <script>
    !function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
    qp('init', '0b68dfe5fa8945c4958f9c04c43ebc05');
    qp('track', 'ViewContent');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/0b68dfe5fa8945c4958f9c04c43ebc05/pixel?tag=ViewContent&noscript=1"/></noscript>
    <!-- End of Quora Pixel Code --> 

    <!-- DO NOT MODIFY -->
    <!-- Quora Pixel Code (JS Helper) -->
    <script>
    !function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
    qp('init', 'd6ab885f6dfc48c884a25cd53dcce98c');
    qp('track', 'ViewContent');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/d6ab885f6dfc48c884a25cd53dcce98c/pixel?tag=ViewContent&noscript=1"/></noscript>
    <!-- End of Quora Pixel Code -->


    <!-- Taboola Pixel Code -->
  <script type='text/javascript'>
    window._tfa = window._tfa || [];
    window._tfa.push({notify: 'event', name: 'page_view', id: 1147016});
    !function (t, f, a, x) {
           if (!document.getElementById(x)) {
              t.async = 1;t.src = a;t.id=x;f.parentNode.insertBefore(t, f);
           }
    }(document.createElement('script'),
    document.getElementsByTagName('script')[0],
    '//cdn.taboola.com/libtrc/unip/1147016/tfa.js',
    'tb_tfa_script');
  </script>
  <noscript>
    <img src='//trc.taboola.com/1147016/log/3/unip?en=page_view'
        width='0' height='0' style='display:none'/>
  </noscript>
  <!-- End of Taboola Pixel Code -->

  <!-- Taboola Pixel Code -->
  <script type='text/javascript'>
    window._tfa = window._tfa || [];
    window._tfa.push({notify: 'event', name: 'page_view', id: 1147017});
    !function (t, f, a, x) {
           if (!document.getElementById(x)) {
              t.async = 1;t.src = a;t.id=x;f.parentNode.insertBefore(t, f);
           }
    }(document.createElement('script'),
    document.getElementsByTagName('script')[0],
    '//cdn.taboola.com/libtrc/unip/1147017/tfa.js',
    'tb_tfa_script');
  </script>
  <noscript>
    <img src='//trc.taboola.com/1147017/log/3/unip?en=page_view'
        width='0' height='0' style='display:none'/>
  </noscript>
  <!-- End of Taboola Pixel Code -->



   </head>
   <body id="page-top">
      <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
         <div class="container text-center">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">                
               <a class="navbar-brand page-scroll" href="#page-top">
               <img src="<?php echo S3_URL?>/site/medlife-images/logo.png" class="logo"/>
               </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1 ">
               <h4 style="color: #fff;text-shadow: 1px 1px 5px #2562bb;font-size: 22px;
                  margin: 40px 0px;"></h4>
            </div>
            <!-- /.navbar-collapse -->
         </div>
         <!-- /.container-fluid -->
      </nav>
	  
    
	  
	  <header>

      

      <?php $all_array = all_arrays(); ?>
         <div class="container form_margin">
            <div class="row">
              <div class="col-md-8 form_containt_box">
              	

                      <div class="row" style="margin:130px 0px 0px 0px;">
                          <h1>Medlife Kushal Basic Health Checkup Package</h1>

                         <h2>First Step Towards Healthy Living</h2>
                         <h1>Know Your Health Status Today</h1>
              
                       <div class="col-sm-6 col-md-4 profile ">
                       <div class="">
                          <h2>Medlife Kushal Basic 2</h2>
                          <p>(42 Parameters)</p> 
                          <hr class="dotted">
                          <h3>₹599</h3>
                          <span>₹1550</span> 
                        </div>
                     </div>

                     <div class="col-sm-6 col-md-4 profile green_bg">
                       <div class="bg-green">
                          <h2>Medlife Kushal Pro 1</h2>
                          <p>(76 Parameters)</p>
                          <hr class="dotted">
                          <h3>₹949</h3>
                          <span>₹2625</span>
                       </div>
                    </div>

                  </div> 
            
               <div class="row" style="margin:20px 0px 0px 0px;">
                   <div class="col-sm-6 col-md-4 profile orange_bg">
                     <div class="">
                        <h2>Medlife Kushal Comprehensive 1</h2>
                        <p>(73 Parameters)</p>
                        <hr class="dotted">
                        <h3>₹1149</h3>
                        <span>₹3750</span> 
                      </div>
                   </div>

                  <div class="col-sm-6 col-md-4 profile red_bg">
                     <div class="bg-green">
                        <h2>Medlife Kushal Comprehensive 3</h2>
                        <p>(77 Parameters)</p>
                        <hr class="dotted">
                        <h3>₹2599</h3>
                        <span>₹6925</span>
                     </div>
                  </div>

                 </div> 

            </div>

               <div class="col-md-4 ">
                  <div class="jumbotron form_box">
                     <div class="row text-center" id="f_div" >
                        <div class="text-center  col-md-12 col-lg-12">
                           <h4>BOOK NOW</h4>
                        </div>
                        <div class="text-center col-lg-12">
                           <!-- CONTACT FORM https://github.com/jonmbake/bootstrap3-contact-form -->
                           <form role="form" id="feedbackForm" class="medlife_form" action="JavaScript:void(0)" onsubmit="medlife_jsfrm('<?php echo SITE_URL?>medlife/medlifefrm', '<?php echo SITE_URL?>')">
                              <div class="form-group">
                                 <!--<label for="name">Name</label>-->
                                 <input type="text" class="form-control" id="name" name="name" placeholder="Name" onkeyup="chck_valid_medlife('name', 'Please enter correct name')" data-attr="Please enter correct name">
                                 <span class="help-block" id="name_err"></span>
                              </div>
                              <div class="form-group">
                                 <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" onkeyup="chck_valid_medlife('email', 'Please enter correct email')" data-attr="Please enter correct email">
                                 <span class="help-block" id="email_err" ></span>
                              </div>
                              <div class="form-group">
                                 <input type="text" class="form-control only_numeric" id="phone" name="phone" pattern="\d*" maxlength="10" placeholder="Mobile Number" onkeyup="chck_valid_medlife('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                                 <span class="help-block" id="phone_err"> </span>
                              </div> 
                            
                            
                              <div class="form-group">
                                 <select class="form-control" name="medlife_test" id="medlife_test" onchange="chck_valid_medlife('medlife_test', 'Please select one Test')" data-attr="Please select one Test"> 
                                    
                                    <option value="">Select Test</option>
                                    <option value="5">Medlife Kushal Basic 2(42 Parameters)</option>
                                    <option value="8">Medlife Kushal Pro 1(76 Parameters)</option>
                                    <option value="6">Medlife Kushal Comprehensive 1(73 Parameters)</option>
                                    <option value="12">Medlife Kushal Comprehensive 3(77 Parameters)</option>
                                     
                                 </select>
                                 <span class="help-block" id="medlife_test_err"></span>
                              </div>


                              <div class="form-group">
                                 <input type="hidden" name="city" id="city" value="blank">
                                 <!--<select class="form-control" name="city" id="city" onchange="chck_valid_medlife('city', 'Please select one City')" data-attr="Please select one City"> 
                                    
                                    <option value="">Select City</option>
                                    <option value="delhi">Delhi</option>
                                    <option value="bangalore">Bangalore</option>
                                    <option value="mumbai">Mumbai</option>
                                    <option value="kolkata">Kolkata</option>
                                    <option value="chennai">Chennai</option>
                                    <option value="ahmedabad">Ahmedabad</option>
                                    <option value="hyderabad">Hyderabad</option>
                                    <option value="jaipur">Jaipur</option>
                                    <option value="pune">Pune</option>
                                    <option value="lucknow">Lucknow</option>
                                    <option value="kochi">Kochi</option>
                                    <option value="kanpur">Kanpur</option>
                                    <option value="indore">Indore</option>
                                    <option value="mysore">Mysore</option>
                                    <option value="others">Others</option>

                                     
                                 </select>-->
                                 <span class="help-block" id="city_err"></span>
                              </div>

							  
                              <?php if($form_type == '2'){ ?>
							                 <div class="form-group">
                                 <input type="text" class="form-control datepicker" id="date" name="date" placeholder="Date" >
                                 <span class="help-block" id="date_err" ></span>
                              </div>
							               <div class="form-group">
                                 <select class="form-control" name="time" id="time"  data-attr="Please select one Test"> 
                                    <option value="">Select Time Slot</option>
                                     <?php 
                  									 foreach($all_array['MEDLIFE_TIME'] as $k=>$l){ 
                  											
                  												echo "<option value='".$l."'>".$l."</option>";
                  											}
                                                        
                  									  ?>
                                 </select>
                                 <span class="help-block" id="time_err"></span>
                              </div>
                          	 <?php } ?>

                          	<input type="hidden" id="form_type"  name="form_type" value="<?php echo $form_type; ?>" >

                          	<input type="hidden" id="otp_verify" name="otp_verify" value="<?php echo $otp_verify; ?>" >
                          	<input type="hidden" id="fire_type"  name="fire_type" value="<?php echo $fire_type; ?>" >
                          	<input type="hidden" id="s_id"  name="s_id" value="<?php echo $s_id; ?>" >
                          	<input type="hidden" id="a_id"  name="a_id" value="<?php echo $a_id; ?>" >
							<input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : "d_sr"); ?>">
							<input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "d_ut"); ?>">
							<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : "d_ut"); ?>">
                            <input type="hidden" id="utm_aff" name="utm_aff" value="<?php echo (isset($_REQUEST['utm_aff']) != "" ? $_REQUEST['utm_aff'] : "d_ut"); ?>">
 
                              <input type="submit" id="frm-sbmtbtn" class="btn btn-primary btn-lg" value="SUBMIT">
                           </form>
                           <!-- END CONTACT FORM --> 
                        </div>
                     </div>

                     <div class="row text-center hidden_div" id="success_div">  
							<form role="form" id="otpForm" class="medlife_form" action="JavaScript:void(0)" onsubmit="medlife_otp('<?php echo SITE_URL?>medlife/medlife_otpverification', '<?php echo SITE_URL?>medlife/')" >
							 <input type="text" class="form-control" id="otp" name="otp" placeholder="OTP">
							  <input type="hidden"  id="otp_phone" name="otp_phone" >
							  <input type="hidden"  id="otp_email" name="otp_email" >
							 <span class="help-block" id="otp_err"></span>
							  <input type="submit" id="frm-sbmtbtn" class="btn btn-primary btn-lg" value="VERIFY">
							</form>
                     </div>
					 <div class="row text-center hidden_div" id="res_otp_div">  
							<form role="form" id="resendForm" class="medlife_form" action="JavaScript:void(0)" onsubmit="medlife_otp_resend('<?php echo SITE_URL?>medlife/medlife_reset')" >
								 <span class="help-block" id="otp_res_err"></span>
								  <input type="hidden"  id="res_id" name="res_id" >
								  
							  <input type="submit" id="res-sbmtbtn" class="btn btn-primary btn-lg" value="RE-SEND">
							</form>  
                           
                     </div>
					 <div class="row text-center hidden_div" id="success_verify">  
							
                           <h3 style="color:#3c4850; margin-top:50px;font-size: 20px;line-height: 30px;">Thank you for booking your appointment in Medlife Lab.</h3>
                     </div>
					 
                  </div>

                  	<br><br>
				          

                  <div class="form_containt_box1">
                     
                   <div class="col-sm-6 col-md-4 profile ">
                     <div class="">
                        <h2>Medlife Kushal Basic 2</h2>
                          <p>(42 Parameters)</p> 
                          <hr class="dotted">
                          <h3>₹599</h3>
                          <span>₹1550</span> 
                        </div>
                     </div>

                     <div class="col-sm-6 col-md-4 profile green_bg">
                       <div class="bg-green">
                          <h2>Medlife Kushal Pro 1</h2>
                          <p>(76 Parameters)</p>
                          <hr class="dotted">
                          <h3>₹949</h3>
                          <span>₹2625</span>
                       </div>
                    </div>
   
               
                     <div class="col-sm-6 col-md-4 profile orange_bg">
                       <div class="">
                          <h2>Medlife Kushal Comprehensive 1</h2>
                          <p>(73 Parameters)</p>
                          <hr class="dotted">
                          <h3>₹1149</h3>
                          <span>₹3750</span> 
                        </div>
                     </div>

                    <div class="col-sm-6 col-md-4 profile red_bg">
                       <div class="bg-green">
                          <h2>Medlife Kushal Comprehensive 3</h2>
                        <p>(77 Parameters)</p>
                        <hr class="dotted">
                        <h3>₹2599</h3>
                        <span>₹6925</span>
                       </div>
                    </div>
               </div>  

               </div>
               
            </div>
         </div>
      </header>

      
      <section id="features" class="features" >
         <div class="container">
            <div class="row">
               <div class="container-fluid">
           <h2 class="text-center"><br>PACKAGES<br><br></h2>
                  <div class="row">  
                     <div class="col-lg-3 col-sm-12 text-center">
             <div class=" offfer-box"> 
             <h1>Medlife Kushal Basic 2 <br>(42 Parameters) </h1>
                
                        <div class="feature-item" style="min-height:337px"> 
              
                           <li class="package-test-wrapper"> CBC- (20) </li>
                           <li class="package-test-wrapper"> Blood Sugar (1)-FBS </li>
                           <li class="package-test-wrapper"> Lipid Profile-Basic (6) </li>
                           <li class="package-test-wrapper"> Renal/Kidney Function-Basic (4) </li>
                           <li class="package-test-wrapper"> Liver Function Test-Basic (10)</li>
                           <li class="package-test-wrapper"> Thyroid Test (1)-TSH </li>
              
                        </div>
               <div class="brnd-logo"> 
                        <!--<img class="" alt="140x140" style="width: 110px; height: auto;" src="<?php echo S3_URL?>/site/medlife-images-new/thyrocare.png" data-holder-rendered="true">-->
               </div>
            <div class="price">
              <div class="bestprice">                               
                <div class="actual-price"><span class="uncut">₹ 1550 </span>  ₹ 599</div>
              </div>
              <a  href="#" type="submit" id="frm-sbmtbtn" class="btn btn-default book_app" value="BOOK NOW" data-attr="5">BOOK NOW</a>
            </div>
            </div> 
                     </div>
            <div class="col-lg-3 col-sm-12 text-center">
              <div class=" offfer-box">
             <h1>Medlife Kushal Pro 1<br> (76 Parameters) </h1>
                        
            <div class="feature-item" style="min-height:337px"> 
              <li class="package-test-wrapper">Haemogram Comprehensive (22)-CBC+PS+ESR</li>
              <li class="package-test-wrapper">Diabetic Screening -(2) FBS & HbA1c</li>
              <li class="package-test-wrapper">Lipid Profile-Basic (6)</li>
              <li class="package-test-wrapper">Renal/Kidney Function-(8)</li>
              <li class="package-test-wrapper">Liver Function Test-Basic (10)</li>
              <li class="package-test-wrapper">Thyroid Function Test (3)</li>
              <li class="package-test-wrapper">Iron Profile (4)</li>
              <li class="package-test-wrapper">Urine Complete Analysis (21)</li>          
                        </div>
                <div class="brnd-logo"> 
                        <!--<img class="" alt="140x140" style="width: 110px; height: auto;" src="<?php echo S3_URL?>/site/medlife-images-new/thyrocare.png" data-holder-rendered="true">-->
               </div> 
            <div class="price">
              <div class="bestprice">                               
                <div class="actual-price"><span class="uncut">₹ 2625  </span> ₹ 949</div>
              </div>
              <a  href="#" type="submit" id="frm-sbmtbtn" class="btn btn-default book_app" value="BOOK NOW" data-attr="8">BOOK NOW</a>
            </div>
                
           </div>
                     </div>
            <div class="col-lg-3 col-sm-12 text-center">
             <div class=" offfer-box"> 
             <h1>Medlife Kushal Comprehensive 1<br>(73 Parameters)</h1>
                        
                        <div class="feature-item" style="min-height:337px"> 
              <li class="package-test-wrapper">Haemogram Comprehensive (22)-CBC+PS+ESR</li>             
              <li class="package-test-wrapper">Blood Sugar (2) - FBS & HbA1c</li>             
              <li class="package-test-wrapper">Lipid Profile-Basic (6)</li>             
              <li class="package-test-wrapper">Renal/Kidney Function-(8)</li>             
              <li class="package-test-wrapper">Liver Function Test-Basic (10)</li>              
              <li class="package-test-wrapper">Thyroid Test (3)</li>              
              <li class="package-test-wrapper">Vitamin D Total (1)</li>             
              <li class="package-test-wrapper">Urine Complete Analysis (21)</li> 
                        </div> 
               <div class="brnd-logo"> 
                        <!--<img class="" alt="140x140" style="width: 110px; height: auto;" src="<?php echo S3_URL?>/site/medlife-images-new/thyrocare.png" data-holder-rendered="true">-->
               </div>
             <div class="price">
              <div class="bestprice">                               
                <div class="actual-price"><span class="uncut">₹4250 </span> ₹1149</div>
              </div>
              <a  href="#" type="submit" id="frm-sbmtbtn" class="btn btn-default book_app" value="BOOK NOW" data-attr="6">BOOK NOW</a>
            </div>
               
         </div>
                     </div>
            <div class="col-lg-3 col-sm-12 text-center">
              <div class=" offfer-box">
             <h1>Medlife Kushal Comprehensive 3<br> (77 Parameters) </h1>
                        
               <div class="feature-item" style="min-height:290px"> 
              <li class="package-test-wrapper">Haemogram Comprehensive (22)-CBC+PS+ESR</li>             
              <li class="package-test-wrapper">Blood Sugar (2) - FBS & HbA1c</li>             
              <li class="package-test-wrapper">Lipid Profile-Basic (6)</li>             
              <li class="package-test-wrapper">Renal/Kidney Function-(8)</li>             
              <li class="package-test-wrapper">Liver Function Test-Basic (10)</li>              
              <li class="package-test-wrapper">Thyroid Test (3)</li>              
              <li class="package-test-wrapper">Vitamins (2)Vit D</li>             
              <li class="package-test-wrapper">Vitamin B12</li>             
              <li class="package-test-wrapper">Pancreas tests (2)</li>              
              <li class="package-test-wrapper">Tumour Markers (1)</li>              
              <li class="package-test-wrapper">CA125 for Female / PSA for Male</li>  
              <li class="package-test-wrapper">Urine Complete Analysis (21)</li>   
                        </div>
              <div class="brnd-logo"> 
                        <!--<img class="" alt="140x140" style="width: 110px; height: auto;" src="<?php echo S3_URL?>/site/medlife-images-new/thyrocare.png" data-holder-rendered="true">-->
               </div>  
               
                
                <div class="price">
              <div class="bestprice">                               
                <div class="actual-price"><span class="uncut">₹6925  </span>₹2599</div>
              </div>
              <a  href="#" type="submit" id="frm-sbmtbtn" class="btn btn-default book_app" value="BOOK NOW" data-attr="12">BOOK NOW</a>
            </div>
                </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>



       <section id="content" class="content" >
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-xs-12">
              <h2>Why do we need health checkup ?<br><br></h2>
              <p>
              Life in a fast lane has led many to ignore their health. Untill its too late then we realize Health is our True Wealth. With growing stress levels at work, competition, frequently eating out, fast food syndrome there is also a steady growth of lifestyle related diseases like heart problems, diabetes and cancers all of which if not checked in time would lead to delirious consequences.      
              </p>  
              <h2><br>Why Testing from us<br><br></h2>  
              <p>Medlife is a renowned name in Wellness industry, Gives assurity of 100% accurate reports.</p>  
            </div>
            <div class="col-sm-6 col-xs-12">
            <img width="100%" src="<?php echo S3_URL?>/site/medlife-images/msg.jpg" class="logo"/>     
            </div>
          </div> 
        </div>
      </section>         

        <section id="content" class="content" >
          <div class="container">
            <h2 class="text-center"><br>Booking Procedure<br><br></h2>
            <div class="row">
              <div class="col-sm-12 col-xs-12">
                
                  <ul>
                  <li><p> Select the Package & fill the above form with Name, Address, Mobile no</p></li>
                    <li><p>Medlife agent will call you and fix an appointment</p></li>
                    <li><p>Blood samples will be collected from your residence.</p></li>
                    <li><p>You need to make the payment to Medlife when Technician comes to pick up the samples</p></li>
                    <li><p>Soft copy Email reports within 48 hrs. In case you opt for Hard copy it will be couriered in 3-4 days.</p></li>
                    </ul> 
                
              </div>
            </div> 
          </div>
        </section>
      

      <footer>
      	© 2018 - Medlife Diagnostics. All Rights Reserved
      </footer>
      <!-- jQuery -->
      <script src="<?php echo S3_URL?>/site/scripts/jquery-1.11.3.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="<?php echo S3_URL?>/site/scripts/bootstrap.js"></script>
      <!-- Plugin JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
       <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
      <!-- Theme JavaScript -->
      <script src="<?php echo S3_URL?>/site/medlife-css/new-age.min.css"></script>
      <script src="<?php echo S3_URL?>/site/scripts/flatpickr.min.js"></script>
	   <script src="<?php echo S3_URL?>/site/medlife-assets/datepicker/js/bootstrap-datepicker.min.js"?>"></script>
     <script src="<?php echo S3_URL?>/site/scripts/default.js"></script>
     <link href="<?php echo S3_URL?>/site/medlife-css/flatpickr.min.css" rel="stylesheet">
     

     <script type="text/javascript">
      $(document).ready(function(){
        $("#testimonial-slider").owlCarousel({
            items:1,
            itemsDesktop:[1000,1],
            itemsDesktopSmall:[979,1],
            itemsTablet:[768,1],
            pagination:false,
            navigation:true,
            navigationText:["",""],
            autoPlay:true
          });
      });
          
      $(document).ready(function(){
          var date = new Date();
          date.setDate(date.getDate()+1);
          $('.datepicker').datepicker({
            startDate: date,
            autoclose: true,
            format: 'dd/mm/yyyy',

          });
          
        $('.datepicker').change(function () {
                  var selectedDate = $('.datepicker').val();
            //alert(selectedDate);
            $("#date").val(selectedDate);
               });
        });
        
        </script>             
     
    

    <script type="text/javascript">
		$(document).ready(function(e) {    
			$(document).on("click", ".book_app", function(e) {
				var option=($(this).attr("data-attr"));
				// $("#medlife_test option[value='"+option+"']").select();
				
        $("#medlife_test").val(option);
        
			});
		});
		</script>	

		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 798077325;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/798077325/?guid=ON&amp;script=0"/>
		</div>
		</noscript>
	  
   </body>
</html>