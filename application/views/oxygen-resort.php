<!DOCTYPE html>

<html lang="en"> 

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Club Oxygen</title>
    <meta name="description" content="Provident Green Park has some of the best 2 & 3 BHK apartments in Coimbatore. These apartments for sale  give you the best value for money.">
    <link rel="shortcut icon" href="<?php echo S3_URL?>/site/oxygen-resort/images/favicon.png" type="image/x-icon" />


    <link rel="stylesheet" href="<?php echo S3_URL?>/site/oxygen-resort/css/vendor.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/oxygen-resort/css/main.css">


    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
</head>

<body>

    <?php $all_array = all_arrays();   ?>

    <!-- banner -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="header-banner header-banner-desktop"> 
                        <img src="<?php echo S3_URL?>/site/oxygen-resort/images/desktop-banner.jpg" />
                </div>
                <div class="header-banner header-banner-mobile"> 
                        <img src="<?php echo S3_URL?>/site/oxygen-resort/images/mobile-banner.jpg" />
                </div>
                <div class="">
                    <div class="carousel-caption"> 
                        <div  id="feedbackForm" class="feedbackForm">

                            <div class="">
                                <div class="col">
                                    <div class="form-logo">
                                        <a href="#">
                                            <img src="<?php echo S3_URL?>/site/oxygen-resort/images/logo-blue.png" />
                                        </a>
                                    </div> 
                                    <!-- <div class="form-logo">
                                        <a href="#">
                                            <img src="<?php echo S3_URL?>/site/oxygen-resort/images/holiday-for-all.png" />
                                        </a>
                                    </div> -->
                                   <!-- <h4>WORLD CLASS HOLIDAY EXPERIENCES</h4> -->

                                </div>
                                <div class="col">
                                 <div class="form-section" id="form_div1">
                                 <form  action="JavaScript:void(0)" onsubmit="oxygenresort_jsfrm('<?php echo SITE_URL?>oxygen_resort/submit_frm','1')">
                                    <div class="group">
                                        <div class="form-group">

                                            <input type="text" class="form-control" id="name1" name="name" placeholder="Name"
                                                onkeyup="chck_valid('name', 'Please enter correct name')"
                                                data-attr="Please enter correct name">
                                            <span class="help-block" id="name_err1"></span>
                                        </div>

                                    </div>
                                    <div class="group" >
                                        <div class="form-group">

                                            <input type="email" class="form-control" id="email1" name="email"
                                                placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')"
                                                data-attr="Please enter correct email">
                                            <span class="help-block" id="email_err1"></span>
                                        </div>
                                        <div class="form-group">
                                           
                                            <input type="text" class="form-control only_numeric " id="phone1" name="phone"
                                                pattern="\d*" placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')"
                                                data-attr="Please enter correct Mobile number">
                                            <span class="help-block" id="phone_err1"> </span>
                                            <span class="help-block" id="CountryCode_err"> </span>
                                        </div>
                                        <div class="form-group">

                                    <input type="text" class="form-control" id="age1" name="age" placeholder="Age"
                                        onkeyup="chck_valid('age', 'Please enter correct age')" data-attr="Please enter correct age">
                                    <span class="help-block" id="age_err1"></span>
                                </div>
                                        <div class="form-group">
                                            <select class="form-control" id="location1" name="location"
                                                 onkeyup="chck_valid('location', 'Please enter correct location')"
                                                data-attr="Please enter correct location">
                                                <option value="" disabled selected>Select City</option>
                                                <option value="AHMEDABAD">AHMEDABAD</option>
                                                <option value="BANGALORE">BANGALORE</option>  
                                                <option value="RAJKOT">RAJKOT</option>
                                                <option value="SURAT">SURAT</option> 
                                                <option value="KANNUR">KANNUR</option>
                                                <option value="KASARGOD">KASARGOD</option> 
                                                <option value="KOZHIKODE">KOZHIKODE</option>
                                                <option value="MALAPPURAM">MALAPPURAM</option> 
                                                <option value="WAYANAD">WAYANAD</option>
                                                <option value="THRISSUR">THRISSUR</option> 
                                                <option value="PALAKKAD">PALAKKAD</option>
                                                <option value="ALAPPUZHA">ALAPPUZHA</option> 
                                                <option value="ERNAKULAM">ERNAKULAM</option> 
                                                <option value="IDUKKI">IDUKKI</option> 
                                                <option value="KOLLAM">KOLLAM</option>   
                                                <option value="KOTTAYAM">KOTTAYAM</option> 
                                                <option value="PATHANAMTHITTA">PATHANAMTHITTA</option>   
                                                <option value="THIRUVANANTHAPURAM">THIRUVANANTHAPURAM</option>                                              
                                                <option value="MUMBAI">MUMBAI</option> 
                                                <option value="PUNE">PUNE</option>
                                                <option value="INDORE ">INDORE </option>
                                                <option value="NAGPUR  ">NAGPUR  </option>
                                                <option value="JAIPUR ">JAIPUR </option>
                                                <option value="OOTY">OOTY</option>      
                                                <option value="NASHIK">NASHIK</option>
                                                <option value="MADURAI">MADURAI</option> 
                                                <option value="CHENNAI">CHENNAI</option>   
                                                <option value="COIMBATORE">COIMBATORE</option>
                                                <option value="VADODARA">VADODARA</option>  
                                                <option value="OTHERS">OTHERS</option>  
                                            </select>
                                            <span class="help-block" id="location_err1"></span>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control" id="lastlocation1" name="lastlocation"
                                                onkeyup="chck_valid('lastlocation', 'Please enter correct last location')"
                                                data-attr="Please enter correct location">
                                                <option value="" disabled selected> How often do you go on vacation? </option>
                                                <option value="1"> < 1 </option>
                                                <option value="2"> 2 </option>
                                                <option value="3"> 3 </option>
                                                <option value="4"> 4 </option>
                                                <option value="more than 5"> More than 5 </option>
                                            </select>
                                            <span class="help-block" id="lastlocation_err1"></span>
                                        </div>
                                        
                                    </div>
 
                                    <div class="form-group radio-form">
                                        <div class="custom-radio">
                                            <p>
                                                <input type="checkbox" id="agree" name="agree" value="agree"  required checked disabled>
                                                <label for="agree">I authorize Club Oxygen to call/email/SMS me for membership plans and ongoing promotions and offers</label>
                                            </p>
                                        </div>    
                                    
                                        <span class="help-block" id="cource_err1"> </span>                           
                                    </div>

                                    <div class="submitbtncontainer">
                                        <input type="submit" id="frm-sbmtbtn1" value="Join The Club" name="submit">
                                    </div>
                                    <p class="disclaimer">All fields are mandatory. T&C and Privacy Policy applies.</p>
                                    </form>
                                    </div>
                                    <div class="otp-section" id ="otp_section1" style="display:none;">
                                     <form  action="JavaScript:void(0)" onsubmit="oxygenresort_otpverify('<?php echo SITE_URL?>oxygen_resort/otp_verify','1')">

                              <input type="hidden"  id="otp_email1" name="otp_email" value="">
                              <input type="hidden"  id="otp_phone1" name="otp_phone" value="">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="otp1" name="otp" placeholder="OTP"
                                                onkeyup="chck_valid('otp', 'Please enter correct otp')"
                                                data-attr="Please enter correct otp">
                                            <span class="help-block" id="otp_err1"></span>
                                        </div>
                                        <div class="submitbtncontainer">
                                            <input type="submit" id="sbmtbtn1" value="Enter OTP" name="submit">
                                        </div>
                                        </form>
                                         <form  action="JavaScript:void(0)" onsubmit="otp_resend('<?php echo SITE_URL?>oxygen_resort/otp_resend','1')">
                                             <p class="disclaimer">OTP Received to the Entered Mobile Number If Not Received <input type="submit" id="res-sbmtbtn1"  value="Click-Here"></p>
                                        </form>
                                    </div>


                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal -->
    </div>
    <!--//banner -->
             
    <!-- about -->
    <div>
        <div class="container">        
            <div class="col-md-12">
                <h5 class="text-center" style="padding:30px 0px 10px; font-size:25px;"><strong>Available Destinations</strong> - Munnar, Thekkady, Goa, Alleppey, Ooty, Gir, Manali</h5>
            </div>
        </div>
    </div>




    <div class="about" id="about">
        <div class="container">
        
            <div class="col-md-6 about_right">
                <h3>About</h3>
                <h3 class="bold">Club Oxygen</h3>
                <p>
                 <strong>Chemmanur International Holidays and Resorts Pvt. Ltd</strong> is 
                 part of The Boby Chemmanur International Group. 
                 The Group began its journey in the year 1863, with a single 
                 store at <strong>Varanthirappilly, Thrissur. 155 years</strong> since the initial 
                 spark, today the Chemmanur Brand stands strong with its legacy 
                 of traditional values and visionary outlook.
                </p>
                <p>
                The <strong>hallmark of the group is enhancing lifestyles</strong>, while promoting progress 
                in all walks of life. This is what has earned us the trust and goodwill of 
                people and continues to widen our reach across the globe.
                </p>
                <p>
                 The BCIG employs over 8000 individuals (3200 directly and 5000 by extension) As 
                 on March 31, 2017 the turnover of the group was INR 4000 cr., with the Jewellery 
                 division valued at INR 1107 cr. and the NBFC division at INR 66.89 cr.
                </p> 
            </div>
            <div class="col-md-6">
                <div class="col-xs-12 aboutimg-w3l aboutimg-w3l2">
                    <img src="<?php echo S3_URL?>/site/oxygen-resort/images/ab5.jpg" alt="image" style="object-position: right;" />
                </div>
                <!-- <div class="col-xs-6 aboutimg-w3l">
					<img src="<?php echo S3_URL?>/site/oxygen-resort/images/ab3.jpg" alt="image" />
				
				</div> -->
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!-- //about -->
  
   <!-- popular -->
   <!-- <div class="popular-w3" id="popular">
        <div class="container">
           
            <div class="popular-grids">
                <div class="">

                    <div class="popular-text">
                        <div class="detail-bottom">
                        
                            <div class="rwd-media">
                                 <iframe  src="https://www.youtube.com/embed/2OBqx1dfqyg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div> -->
    <!-- //popular -->   

    <!-- //popular -->
    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">Destinations</h3>
           
            <div class="row">
                <div class="col-md-12">
                    <!-- begin panel group -->
                    <div class="panel-group" id="spec" role="tablist" aria-multiselectable="true">

                        <!-- panel 1 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab1" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingOne" data-toggle="collapse"
                                    data-parent="#spec" href="#scollapseOne" aria-expanded="false" aria-controls="collapseOne">
                                    <h4 class="panel-title collapsed">OXYGEN RESORTS - THE LAKE VIEW, MUNNAR</h4>
                                </div>
                            </span>

                            <div id="scollapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                  <p>Oxygen Resorts, The Lake View Munnar is one of the most sought out resorts in Munnar with a great ambiance and service and It is a valley of dreams covered with mist clouds. To be close to mother nature. You can't even blink your eyes when you see nature's beauty.</p>
                                  <p>Elegant, exclusive and in its essence a state of mind, the Lake View resort in munnar is an exquisite resort located in Munnar, a largely mountainous place in Kerala, a state in the south of India. Situated beneath an enormous canopy of a lush tropical rainforest, journey into nature's wonderland - in the company of spirit of nature that inhabit the dense Lake View Munnar. You will be seduced by the promise of discovery, bliss and exaltation - body, mind and soul.</p>
                                  <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - The Lake View, MUNNAR" href="<?php echo S3_URL?>/site/oxygen-resort/images/lake-view.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/lake-view.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                    <!-- <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - The Lake View, MUNNAR" href="<?php echo S3_URL?>/site/oxygen-resort/images/m2.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/m2.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                        <!-- / panel 1 -->

                        <!-- panel 2 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab2" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse"
                                    data-parent="#spec" href="#scollapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">OXYGEN RESORTS - WILD CORRIDOR, THEKKADY</h4>
                                </div> 
                            </span>

                            <div id="scollapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                   <p>Lavish. Natural. Serene. The name Thekkady Wild Corridor itself invites a sense of plush nature along with its native name. Located right in Idukki’s green heart of Periyar National Park, this hotel breathes right amidst thick evergreen forests which is home to a wild and meditative experience. This secluded, eco-friendly, and elevated boutique retreat offers an imperturbable episode of calmness, waves of laughter, and living that comes with its top-notch villas and private rooms. The Wild Corridor Resort & Spa by Oxygen Resorts gives you nothing but premium service and palatial rooms. With breathtaking views from the top rooms of the villas, one can sip tea in his comfy chairs inside the luxurious room while treating your eyes to the clouds touching the mountain top. This tangerine and wooden ochre dressed boutique houses a multi-cuisine restaurant giving you a global essence. It also has a poolside BBQ if you want to relish while you swim and dance. Apart from this, you can have your toll of fun in the game room which also has a ping pong table. Adding to the wonder of the cozy rooms is an Ayurvedic Spa facility. This sybaritic boutique gives you a double dose of coziness. It also has a spice plantation in one of its garden. The boutique welcomes people from any part of the globe as it has a multilingual staff who greets you warmly and offers a friendly hand.</p>
                                   <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - WILD CORRIDOR, THEKKADY" href="<?php echo S3_URL?>/site/oxygen-resort/images/t1.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/t1.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                    <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - WILD CORRIDOR, THEKKADY" href="<?php echo S3_URL?>/site/oxygen-resort/images/t2.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/t2.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 2 -->

                        <!-- panel 3 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab3" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingThree" data-toggle="collapse"
                                    data-parent="#spec" href="#scollapseThree" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">OXYGEN RESORTS - HAVELI BACKWATER, ALLEPPEY</h4>
                                </div> 
                            </span>

                            <div id="scollapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                   <p>Nestled parallel to the backwaters of Kerala, Haveli Backwater Resorts is an idle place to lodge at your tour to Alleppey (Alappuzha). This posada offers a great view of backwaters and greenery on every side. Haveli Resort, as the name might prompt; exhibits a Rajasthani architect and built. Oozing with royalty and traditional décor, it is the most preferred and loved resorts by the tourists coming from in and around states. The majestic frontage welcomes you with the classic haveli structure. The infrastructures and interiors of the rooms and suits carry forward the traditional theme exhibited on the exteriors. The rooms are designed with utmost care keeping in mind that the visitors enjoy a merry time with their family and friends. Every room is spacious and has a great view to look at from the balconies. The professional yet friendly staff members caters to your requests while you explore the amenities. Guests can avail a free, wide parking space and a lavish swimming pool on the first floor. And just when you’re about to settle in the hotel, you can hear the waters calling you from just across the road. Boat tours could be enjoyed from the main gate of the hotel. And if you wish to indulge yourself in some body therapy, there is an in-house spa presenting traditional Ayurveda, Swedish Spa at your service. And is any hotel guide complete without talking about its food? Haveli resort offers a Multi-Cuisine Restaurant named Sarangi serving all meal periods with the finest mouthwatering delights from all over the world.</p>
                                   <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - HAVELI BACKWATER, ALLEPPEY" href="<?php echo S3_URL?>/site/oxygen-resort/images/a1.png">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/a1.png" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                    <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - HAVELI BACKWATER, ALLEPPEY" href="<?php echo S3_URL?>/site/oxygen-resort/images/a2.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/a2.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 3 -->
             

                        <!-- panel 4 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab4" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingThree" data-toggle="collapse"
                                    data-parent="#spec" href="#scollapseT" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">OXYGEN RESORTS - COUNTRY TAVERN, OOTY</h4>
                                </div> 
                            </span>

                            <div id="scollapseT" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                   <p>Oxygen Resorts at Country Tavern, Ooty has the splendour of the old world charm nestled in the beautiful hill-town of Ooty. This colonial bungalow comprises of a building dating from 19th century, has old-school fireplaces, huge bay windows, four-poster beds and antique décor within it which itself is an appealing transformation from our modern lives in cities. The major attraction of the property is the old smoke-less fireplace which is considered as a British marvel present in the guest rooms. The premises have a nice stretch of greenery inside with a lot of open area which further helps one relax. An English library, Antique chess rooms are amongst the variety of facilities provided inside the British Colonial building. Oxygen Resorts at Country Tavern features within it the following which consist of a restaurant, coffee shop, book shop, arcade room, garden, business centre, banquet hall, nightclub and gift shop. There is a lobby with travel counter, 24-hour front desk and luggage storage space is also available within the premises. Oxygen Resorts at Country Tavern in Ooty is a cosy holiday resort in an utterly peaceful setting.</p><p>Oxygen Resorts at Country Tavern is situated in Ooty, 2.9 km from Ooty Lake. It provides guestrooms with free Wi-Fi. The resort has a gorgeous garden and is close to more than a few well-known attractions, around a 9-minute walk from Ooty Rose Garden, 1.7 km from Hebron School Ooty and 1.8 km from Ooty Botanical Gardens. It is also situated at a distance of 2 km from Stone House Government Museum.</p>
                                   <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - COUNTRY TAVERN, OOTY" href="<?php echo S3_URL?>/site/oxygen-resort/images/o1.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/o1.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                    <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - COUNTRY TAVERN, OOTY" href="<?php echo S3_URL?>/site/oxygen-resort/images/o2.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/o2.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 4 -->


                        <!-- panel 5 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab5" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingThree" data-toggle="collapse"
                                    data-parent="#spec" href="#scollapseM" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">OXYGEN RESORTS - THE HOLIDAY CLUB, MANALI</h4>
                                </div> 
                            </span>

                            <div id="scollapseM" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                   <p>Oxygen Resorts at The Holiday Club, Manali has the clean and ventilated rooms in the hotel are well-maintained, perfectly designed and crafted as per the taste of guests to make their stay relaxed and comfortable comes with amenities like attached bathroom, hot and cold water.</p>
                                   <p>Oxygen Resorts at The Holiday Club, Manali is situated in Manali, a popular hill station in India that is tucked at a mighty height amid the natural hues of the Himachal Himalayas. The nearest airport is the Bhuntur Airport and the closest rail-head is the Jogindernagar Railway Station.</p>
                                   <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - THE HOLIDAY CLUB, MANALI" href="<?php echo S3_URL?>/site/oxygen-resort/images/the-holidays-club.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/the-holidays-club.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                    <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - THE HOLIDAY CLUB, MANALI" href="<?php echo S3_URL?>/site/oxygen-resort/images/o3.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/o3.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 5 -->


                        <!-- panel 6 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab6" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingThree" data-toggle="collapse"
                                    data-parent="#spec" href="#scollapseGir" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">OXYGEN RESORTS - THE LION'S DEN, GIR</h4>
                                </div> 
                            </span>

                            <div id="scollapseGir" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                   <p>Oxygen Resorts at The Lion’s Den, Gir has the unique concept ideal for families and large groups with 2 bedroom villas spread over acres of green forested area and mango orchard. It’s a weekend retreat with 2 Bedroom Family Villas, Club House, swimming pool, restaurant and a mini theatre.Lion’s Den Weekend Home, a perfect natural beauty within the Gir National Park, surrounded by mango orchard and a monsoon fed river, offers a relaxing atmosphere. The Restaurant serve authentic local kathiawari fresh food.</p>
                                   
                                   <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - THE LION'S DEN, GIR" href="<?php echo S3_URL?>/site/oxygen-resort/images/den.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/den.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                    <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - THE LION'S DEN, GIR" href="<?php echo S3_URL?>/site/oxygen-resort/images/o4.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/o4.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 6 -->


                        <!-- panel 7 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab6" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingThree" data-toggle="collapse"
                                    data-parent="#spec" href="#scollapseg" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">OXYGEN RESORTS - GOA</h4>
                                </div> 
                            </span>

                            <div id="scollapseg" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                   <p>Stunning sunsets, gorgeous beaches, golden sands, swaying palm trees, imposing churches, Portuguese sausages to port wine, Goa is a paradise for the traveler’s eye. A friendly backpacker’s destination that offers the perfect blend of beauty and fun to unwind, relax and rejuvenate, Goa can you get you covered for all. Also called as the “Rome of the East” the tranquil city celebrates a diversity of cultures Indian and European brought perfectly in all aspects of its life surrounding food, music, visual arts and lifestyle. Goa has something for everyone at all times of the year, from the pompous Christmas and New Year’s celebrations to the lovely monsoon lovers bucket list when the countryside is a patchwork of lush green, there is hardly an off-season that keeps Goa uninviting. Being a coastal destination known as the Konkan, Goa remains warm and humid all through the year with the month of May as the hottest with day temperatures soaring almost to 35 degrees while monsoons usually cooler at 28 degrees with abundant downpours and the one of the most scenic captures of Goa. The party capital is a favorite hotspot around November to March when the weather is balmy and pleasant at 22 degrees keeping holiday makers spirits alive with festivities and celebrations and for the late night lovers endless fun at the beach shacks that come to life.</p>
                                   <div class="gallery-grid1">
                                        <a title="OXYGEN RESORTS - GOA" href="<?php echo S3_URL?>/site/oxygen-resort/images/g1.jpg">
                                            <div class="stack twisted">
                                                <img src="<?php echo S3_URL?>/site/oxygen-resort/images/g1.jpg" alt=" "
                                                    class="img-responsive" />
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 7 -->                        

                    </div> <!-- / panel-group -->

                </div> <!-- /col-md-4 -->
             
            </div>
            <!--/ .row -->
        </div> 
    </div>


  
    <!-- projects -->
    <div class="gallery our-products" id="projects">
        <div class="container">
            <h3 class="sub-heading">OUR PRODUCTS</h3>
            <div class="row seprator marginTop40">
                <div class="col-md-6 borderRight">
                    <div class="membership-sec">
                        <h3 class="sub-heading">Membership Seasons</h3>
                        <ul>
                        <li>Peak Season</li>
                        <li>Mid Season </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="membership-sec">
                        <h3 class="sub-heading">Membership Categories </h3>
                        <ul>
                        <li>Ozone (5 Years)</li>
                        <li>Oxylife (10 Years)</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br><br><br><br>
    <!-- //projects -->

    <!-- contact -->
    <!--<div class="address" id="contact">
        <div class="container">
            <h3 class="title">Contact Us</h3>
            <div class="address-row">
              
            <div class="col-md-offset-4 col-xs-offset-0 col-md-4 col-xs-6 address-right">
                <div class="address-info wow fadeInRight animated text-center" data-wow-delay=".5s">
                    <h4>Call Us</h4>
                    <p><strong>From 9am to 6PM</strong><br><strong><a href="tel:18003157171">18003157171</a></strong><br><strong><a href="tel:02262770000">022-62770000</a></strong></p>
                </div>
            </div>
               

            </div>
        </div>
    </div>-->
    <!--//contact-->
    <footer>
        <div class="copy-right-grids">
            <p class="footer-gd">© 2018 Chemmanur International Holidays and Resorts Private Limited. All rights reserved.</p>
        </div>
    </footer>
    <div class="floating-form visiable" id="contact_form">
        <div class="contact-opener">Enquire Now</div>
        <div  id="feedbackForm" class="feedbackForm" >
            <h4></h4>
            <div class="">
                <div class="col text-center">
                    <div class="form-logo">
                        <a href="#">
                            <img src="<?php echo S3_URL?>/site/oxygen-resort/images/logo-blue.png" />
                        </a>
                    </div>
                    <div class="form-logo">
                        <a href="#">
                            <img src="<?php echo S3_URL?>/site/oxygen-resort/images/holiday-for-all.png" />
                        </a>
                    </div>
                </div>
                <div class="col">
                <div class="form-section"id="form_div2">
                 <form action="JavaScript:void(0)" onsubmit="oxygenresort_jsfrm('<?php echo SITE_URL?>oxygen_resort/submit_frm','2')">
                    <div class="groups">
                        <div class="form-group">

                            <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')"
                                data-attr="Please enter correct name">
                            <span class="help-block" id="name_err2"></span>
                        </div>

                    </div>
                    <div class="groups">
                        <div class="form-group">

                            <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address"
                                onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                            <span class="help-block" id="email_err2"></span>
                        </div>
                        <div class="form-group">
                           
                            <input type="text" class="form-control only_numeric" id="phone2" name="phone" pattern="\d*"
                                placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')"
                                data-attr="Please enter correct Mobile number">
                            <span class="help-block" id="phone_err2"> </span>
                            <span class="help-block" id="CountryCode_err"> </span>
                        </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="age2" name="age" placeholder="Age"
                                        onkeyup="chck_valid('age', 'Please enter correct age')" data-attr="Please enter correct age">
                                    <span class="help-block" id="age_err2"></span>
                                </div>
                                <div class="form-group">
                                            <select class="form-control locationSelect" id="location2" name="location"
                                                 onkeyup="chck_valid('location', 'Please enter correct location')"
                                                data-attr="Please enter correct location">
                                                <option value="" disabled selected>Select City</option>
                                                <option value="AHMEDABAD">AHMEDABAD</option>
                                                <option value="BANGALORE">BANGALORE</option>  
                                                <option value="RAJKOT">RAJKOT</option>
                                                <option value="SURAT">SURAT</option> 
                                                <option value="KANNUR">KANNUR</option>
                                                <option value="KASARGOD">KASARGOD</option> 
                                                <option value="KOZHIKODE">KOZHIKODE</option>
                                                <option value="MALAPPURAM">MALAPPURAM</option> 
                                                <option value="WAYANAD">WAYANAD</option>
                                                <option value="THRISSUR">THRISSUR</option> 
                                                <option value="PALAKKAD">PALAKKAD</option>
                                                <option value="ALAPPUZHA">ALAPPUZHA</option> 
                                                <option value="ERNAKULAM">ERNAKULAM</option> 
                                                <option value="IDUKKI">IDUKKI</option> 
                                                <option value="KOLLAM">KOLLAM</option>   
                                                <option value="KOTTAYAM">KOTTAYAM</option> 
                                                <option value="PATHANAMTHITTA">PATHANAMTHITTA</option>   
                                                <option value="THIRUVANANTHAPURAM">THIRUVANANTHAPURAM</option>                                              
                                                <option value="MUMBAI">MUMBAI</option> 
                                                <option value="PUNE">PUNE</option>
                                                <option value="INDORE ">INDORE </option>
                                                <option value="NAGPUR  ">NAGPUR  </option>
                                                <option value="JAIPUR ">JAIPUR </option>
                                                <option value="OOTY">OOTY</option>      
                                                <option value="NASHIK">NASHIK</option>
                                                <option value="MADURAI">MADURAI</option> 
                                                <option value="CHENNAI">CHENNAI</option>   
                                                <option value="COIMBATORE">COIMBATORE</option>
                                                <option value="VADODARA">VADODARA</option>  
                                                <option value="OTHERS">OTHERS</option>  
                                            </select>
                                            <span class="help-block" id="location_err2"></span>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control" id="lastlocation2" name="lastlocation"
                                                onkeyup="chck_valid('lastlocation', 'Please enter correct last location')"
                                                data-attr="Please enter correct location">
                                                <option value="" disabled selected> How often do you go on vacation? </option>
                                                <option value="1"> < 1 </option>
                                                <option value="2"> 2 </option>
                                                <option value="3"> 3 </option>
                                                <option value="4"> 4 </option>
                                                <option value="more than 5"> More than 5 </option>
                                            </select>
                                            <span class="help-block" id="lastlocation_err2"></span>
                                        </div>
                    </div>

                    <div class="form-group radio-form">
                                        <div class="custom-radio">
                                            <p>
                                                <input type="checkbox" id="agree2" name="agree2" value="agree"  required checked disabled>
                                                <label for="agree2">I authorize Club Oxygen to call/email/SMS me for membership plans and ongoing promotions and offers</label>
                                            </p>
                                        </div>    
                                    
                                        <span class="help-block" id="cource_err1"> </span>                           
                                    </div>
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn2"  value="Join The Club" name="submit">
                                    </div>
                                    <p class="disclaimer">All fields are mandatory. T&C and Privacy Policy applies.</p>
                                    </form>
                                    </div>
                                     <div class="otp-section" id ="otp_section2" style="display:none;">
                                     <form  action="JavaScript:void(0)" onsubmit="oxygenresort_otpverify('<?php echo SITE_URL?>oxygen_resort/otp_verify','2')">

                              <input type="hidden"  id="otp_email2" name="otp_email" value="">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="otp2" name="otp" placeholder="OTP"
                                                onkeyup="chck_valid('otp', 'Please enter correct otp')"
                                                data-attr="Please enter correct otp">
                                            <span class="help-block" id="otp_err2"></span>
                                        </div>
                                        <div class="submitbtncontainer">
                                            <input type="submit" id="sbmtbtn2" value="Enter OTP" name="submit">
                                        </div>
                                       
                                        </form>
                                        <form  action="JavaScript:void(0)" onsubmit="otp_resend('<?php echo SITE_URL?>oxygen_resort/otp_resend','2')">
                                             <p class="disclaimer">OTP Received to the Entered Mobile Number If Not Received <input type="submit" id="res-sbmtbtn2"  value="Click-Here"></p>
                                        </form>
                                    </div>
                                </div>
                </div>
            </div>
        </div>
        <div>
            <div class="popup-enquiry-form mfp-hide" id="popupForm">
                <div  id="feedbackForm" class="feedbackForm">

                    <div class="custom-row">
                        <div class="col">
                            <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/oxygen-resort/images/logo-blue.png" />
                                </a>
                            </div>
                            <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/oxygen-resort/images/holiday-for-all.png" />
                                </a>
                            </div>
                            <h4></h4>
                        </div>
                        <div class="col">
                         <div class="form-section"id="form_div3">
                                    <form  action="JavaScript:void(0)" onsubmit="oxygenresort_jsfrm('<?php echo SITE_URL?>oxygen_resort/submit_frm','3')">
                            <div class="groups">
                                <div class="form-group">

                                    <input type="text" class="form-control" id="name3" name="name" placeholder="Name"
                                        onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                                    <span class="help-block" id="name_err3"></span>
                                </div>
                                <div class="form-group">

                                    <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address"
                                        onkeyup="chck_valid('email', 'Please enter correct email')"
                                        data-attr="Please enter correct email">
                                    <span class="help-block" id="email_err3"></span>
                                </div>
                            </div>
                            <div class="groups">

                                <div class="form-group">
                                   
                                    <input type="text" class="form-control only_numeric " id="phone3" name="phone"
                                        pattern="\d*" placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')"
                                        data-attr="Please enter correct Mobile number">
                                    <span class="help-block" id="phone_err3"> </span>
                                    <span class="help-block" id="CountryCode_err"> </span>
                                </div>
                               <div class="form-group">

                                    <input type="text" class="form-control" id="age3" name="age" placeholder="Age"
                                        onkeyup="chck_valid('age', 'Please enter correct age')" data-attr="Please enter correct age">
                                    <span class="help-block" id="age_err3"></span>
                                </div>
                                <div class="form-group">
                                            <select class="form-control" id="location3" name="location"
                                                 onkeyup="chck_valid('location', 'Please enter correct location')"
                                                data-attr="Please enter correct location">
                                                <option value="" disabled selected>Select City</option>
                                                <option value="AHMEDABAD">AHMEDABAD</option>
                                                <option value="BANGALORE">BANGALORE</option>  
                                                <option value="RAJKOT">RAJKOT</option>
                                                <option value="SURAT">SURAT</option> 
                                                <option value="KANNUR">KANNUR</option>
                                                <option value="KASARGOD">KASARGOD</option> 
                                                <option value="KOZHIKODE">KOZHIKODE</option>
                                                <option value="MALAPPURAM">MALAPPURAM</option> 
                                                <option value="WAYANAD">WAYANAD</option>
                                                <option value="THRISSUR">THRISSUR</option> 
                                                <option value="PALAKKAD">PALAKKAD</option>
                                                <option value="ALAPPUZHA">ALAPPUZHA</option> 
                                                <option value="ERNAKULAM">ERNAKULAM</option> 
                                                <option value="IDUKKI">IDUKKI</option> 
                                                <option value="KOLLAM">KOLLAM</option>   
                                                <option value="KOTTAYAM">KOTTAYAM</option> 
                                                <option value="PATHANAMTHITTA">PATHANAMTHITTA</option>   
                                                <option value="THIRUVANANTHAPURAM">THIRUVANANTHAPURAM</option>                                              
                                                <option value="MUMBAI">MUMBAI</option> 
                                                <option value="PUNE">PUNE</option>
                                                <option value="INDORE ">INDORE </option>
                                                <option value="NAGPUR  ">NAGPUR  </option>
                                                <option value="JAIPUR ">JAIPUR </option>
                                                <option value="OOTY">OOTY</option>      
                                                <option value="NASHIK">NASHIK</option>
                                                <option value="MADURAI">MADURAI</option> 
                                                <option value="CHENNAI">CHENNAI</option>   
                                                <option value="COIMBATORE">COIMBATORE</option>
                                                <option value="VADODARA">VADODARA</option>  
                                                <option value="OTHERS">OTHERS</option>  
                                            </select>
                                            <span class="help-block" id="location_err3"></span>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control" id="lastlocation3" name="lastlocation"
                                                onkeyup="chck_valid('lastlocation', 'Please enter correct last location')"
                                                data-attr="Please enter correct location">
                                                <option value="" disabled selected> How often do you go on vacation? </option>
                                                <option value="1"> < 1 </option>
                                                <option value="2"> 2 </option>
                                                <option value="3"> 3 </option>
                                                <option value="4"> 4 </option>
                                                <option value="more than 5"> More than 5 </option>
                                            </select>
                                            <span class="help-block" id="lastlocation_err3"></span>
                                        </div>

                            </div>

                            <div class="form-group radio-form">
                                <div class="custom-radio">
                                    <p>
                                        <input type="checkbox" id="agree3" name="agree3" value="agree"  required checked disabled>
                                        <label for="agree3">I authorize Club Oxygen to call/email/SMS me for membership plans and ongoing promotions and offers</label>
                                    </p>
                                </div>    
                            
                                <span class="help-block" id="cource_err1"> </span>                           
                            </div>
                            <div class="submitbtncontainer">
                                <input type="submit" id="frm-sbmtbtn3" value="Join The Club" name="submit">
                            </div>
                                <p class="disclaimer">All fields are mandatory. T&C and Privacy Policy applies.</p>
                            </form>
                         
                            </div>
                                     <div class="otp-section" id ="otp_section3" style="display:none;">
                                     <form  action="JavaScript:void(0)" onsubmit="oxygenresort_otpverify('<?php echo SITE_URL?>oxygen_resort/otp_verify','3')">

                              <input type="hidden"  id="otp_email3" name="otp_email" value="">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="otp3" name="otp" placeholder="OTP"
                                                onkeyup="chck_valid('otp', 'Please enter correct otp')"
                                                data-attr="Please enter correct otp">
                                            <span class="help-block" id="otp_err3"></span>
                                        </div>
                                        <div class="submitbtncontainer">
                                            <input type="submit" id="sbmtbtn3" value="Enter OTP" name="submit">
                                        </div>
                                      
                                        </form>
                                        <form  action="JavaScript:void(0)" onsubmit="otp_resend('<?php echo SITE_URL?>oxygen_resort/otp_resend','3')">
                                             <p class="disclaimer">OTP Received to the Entered Mobile Number If Not Received <input type="submit" id="res-sbmtbtn3"  value="Click-Here"></p>
                                        </form>
                                    </div>
                                </div>
                        </div>
                        </div>
                    </div>
                </div>

            </div>

            <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
            <input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "
                "); ?>">
            <input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : "
                "); ?>">
            <input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : "
                "); ?>">
            <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : "
                "); ?>">
            
            <input type="hidden" id="utm_term" name="utm_term" value="<?php echo (isset($_REQUEST['utm_term']) != "" ? $_REQUEST['utm_term'] : "
                "); ?>">
                
            <input type="hidden" id="utm_content" name="utm_content" value="<?php echo (isset($_REQUEST['utm_content']) != "" ? $_REQUEST['utm_content'] : "
                "); ?>">
                    

            <script src="<?php echo S3_URL?>/site/oxygen-resort/js/vendor.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
            <script src="<?php echo S3_URL?>/site/oxygen-resort/js/main.js"></script>
            <script src="<?php echo S3_URL?>/site/scripts/oxygenclub.js"></script>
          
</body>

</html>