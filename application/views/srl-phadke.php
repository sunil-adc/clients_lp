<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Diagnostics Lab Test Center Mumbai| Best Pathology Lab</title>

      
        <META name="Description" content="Contact SRL Diagnostics & Pathology Lab,  Dadar (w), Mumbai for Blood Tests, Health Checkup, Hormone Tests etc.">
        <META name="Keywords" content="srl pathology lab, srl diagnostics, pathology lab">
        <META name="Robots" content="index, follow">

      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/srl-images/favicon.ico" type="image/x-icon" />

      <!-- Bootstrap -->
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/srlphadke-css/bootstrap.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/srlphadke-css/new-age.min.css">
      <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">
      
      
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">

      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body id="page-top">
      <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
         <div class="container text-center">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">                
               <a class="navbar-brand page-scroll" href="#page-top">
               <img src="<?php echo S3_URL?>/site/srlphadke-images/logo.png" class="logo"/>
               </a>
			   <img src="<?php echo S3_URL?>/site/srlphadke-images/partners.png" class="logo partners"/>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1 ">
               <img src="<?php echo S3_URL?>/site/srlphadke-images/whatsapp.png" class="watsap"/>
            </div>
            <!-- /.navbar-collapse -->
         </div>
         <!-- /.container-fluid -->
      </nav>
	  
	  
	  <header>
      <?php $all_array = all_arrays(); ?>
         <div class="container form_margin">
            <div class="row">
              
              	<div class="col-md-8 form_box1">                  
                  <h1>
					<strong style="font-family: Segoe UI;">
						Dr. Phadke’s Pathology Laboratory and Infertility center
				    </strong>
                  </h1>
                  <h5>"NABL accredited , ISO certified labs. Over fifty years of expertise and experience in the diagnostic space. High quality,quick and efficient reporting."
				 </h5>
               </div>
              
               <div class="col-md-4 ">
                  <div class="jumbotron form_box">
                     <div class="row text-center" id="f_div" >
                        <div class="text-center  col-md-12 col-lg-12">
                           <h4>REQUEST FOR A HOME COLLECTION</h4>
                        </div>
                        <div class="text-center col-lg-12">
                           <!-- CONTACT FORM https://github.com/jonmbake/bootstrap3-contact-form -->
                           <form role="form" id="feedbackForm" class="srl_form" action="JavaScript:void(0)" onsubmit="srl_jsfrm('<?php echo SITE_URL?>srlworld/srlfrm')">
                              <div class="form-group">
                                 <!--<label for="name">Name</label>-->
                                 <input type="text" class="form-control" id="name" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                                 <span class="help-block" id="name_err"></span>
                              </div>
                              <div class="form-group">
                                 <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                                 <span class="help-block" id="email_err" ></span>
                              </div>
                              <div class="form-group">
                                 <input type="text" class="form-control only_numeric" id="phone" name="phone" pattern="\d*" maxlength="10"placeholder="Phone Number" onkeyup="chck_valid('phone', 'Please enter correct phone')" data-attr="Please enter correct phone number">
                                 <span class="help-block" id="phone_err"> </span>
                              </div>
                              <div HEALTH CHAMPION PACKAGE ADVANCEDclass="form-group">
                                 <select class="form-control" id="city" name="city" onchange="chck_valid('city', 'Please select correct city')" 
								 data-attr="Please select city">
                                       <option value="" >Select city</option>
                                       <option value="Delhi" >Delhi</option>
                                       <option value="Mumbai" >Mumbai</option>
                                       <option value="Pune" >Pune</option>
                                       <option value="Chandigarh" >Chandigarh</option>
                                       <option value="Gurgaon" >Gurgaon</option>
                                       <option value="Faridabad" >Faridabad</option>
                                       <option value="Noida" >Noida</option>
                                       <option value="Lucknow" >Lucknow</option>
                                       <option value="Bangalore" >Bangalore</option>
                                       <option value="Chennai" >Chennai</option>
                                       <option value="Hyderabad" >Hyderabad</option>
                                       <option value="Kolkata" >Kolkata</option>
                                       <option value="other" >Other</option>
                                 </select>

                                 <span class="help-block" id="city_err"></span>
                              </div>
                              <div class="form-group">
                                 
                                <input type="text" class="form-control datepicker" id="get_date" name="get_date" placeholder="Date" onkeyup="chck_valid('get_date', 'Please select Date slot')" data-attr="Please select Date slot">
                                 <span class="help-block" id="get_date_err"></span>
                              </div>
                              <div class="form-group">
                                 <select class="form-control" id="srl_time" name="srl_time" onchange="chck_valid('srl_time', 'Please select time slot')" data-attr="Please select time slot">
                                    <option value="">Select Time Slot</option>
                                    <?php foreach($all_array['SRL_TIME'] as $k=>$l){ 
                                    
                                          echo "<option value='".$k."'>".$l."</option>";
                                      } ?>
                                 </select>
                                 <span class="help-block" id="srl_time_err"></span>
                              </div>
                              <div class="form-group">
                                 <select class="form-control" name="srl_test" id="srl_test" onchange="chck_valid('srl_test', 'Please select one Test')" data-attr="Please select one Test"> 
                                    <option value="">Select Test</option>
                                     <?php foreach($all_array['SRL_TEST'] as $k=>$l){ 
                                    
                                          echo "<option value='".$k."'>".$l[0]."</option>";
                                      } ?>
                                 </select>
                                 <span class="help-block" id="srl_test_err"></span>
                              </div>
                              <div class="form-group">
                                 <input type="text" class="form-control" id="address" name="address" placeholder="Address" onblur="chck_valid('address', 'Please enter correct address')" data-attr="Please enter correct address">
                                 <span class="help-block" id="address_err"></span>
                              </div>
                              <input type="submit" id="frm-sbmtbtn" class="btn btn-primary btn-lg" value="SUBMIT">
                           </form>
                           <!-- END CONTACT FORM --> 
                        </div>
                     </div>
                     <div class="row text-center hidden_div" id="success_div">   
                           <h3 style="color:#3c4850; margin-top:150px;font-size: 20px;line-height: 30px;">Thank you for booking your appointment in SRL Diagnostics.</h3>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </header>
	  
	  
      <section id="features" class="features" >
         <div class="container">
            <div class="row">
               <div class="container-fluid">
                  <div class="row">
				  <br>
				  <h2 class="text-center">Services</h2>
				  <br><br>
                     <div class="col-lg-4 col-sm-12 text-center">
                        <img class="img-circle" alt="140x140" style="margin-bottom:20px" src="<?php echo S3_URL?>/site/srlphadke-images/service-home.png" data-holder-rendered="true">
                        <div class="feature-item"> 
                           <i class="icon-present text-primary"></i>
                           <p class="text-muted">Home Collection Facility.</p>
						   <p style="font-size: 15px;    text-align: justify;">
						   We at Dr Avinash Phadke's lab offer blood sample collection within the comfort of your home or office along with emailing of best quality reports. This service is available across the city of Mumbai.
						   </p>
                        </div>
                     </div>
                     <div class="col-lg-4 col-sm-12 text-center">
                        <img class="img-circle" alt="140x140" style="margin-bottom:20px" src="<?php echo S3_URL?>/site/srlphadke-images/service-sample.png" data-holder-rendered="true">
                        <div class="feature-item">
                           <i class="icon-lock-open text-primary"></i>
                           <p class="text-muted">Sample Pickups.</p>
						   <p style="font-size: 15px;    text-align: justify;">For sample pick up kindly call on the following number -  <strong>02230038042</strong></p>
                        </div>
                     </div>
                     <div class="col-lg-4 col-sm-12 text-center">
                        <img class="img-circle" alt="140x140" style="margin-bottom:20px" src="<?php echo S3_URL?>/site/srlphadke-images/service-email.png" data-holder-rendered="true">
                        <div class="feature-item">
                           <i class="icon-lock-open text-primary"></i>
                           <p class="text-muted">Emailing of Reports.</p>
						   <p style="font-size: 15px;    text-align: justify;">When it comes to patient care we understand the importance of time. We provide Emails of reports (in PDF format). This not only saves valuable time but also helps your clinician in administering timely medical care.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section> 
      
	  
	  
      <section id="contact" class="features" style="background: #f9fbfc">
            <div class="container text-center">
            <div class="row">
               <h2>Featured Comprehensive Test Profiles</h2>
            
            <br> 
             
              		<div class="col-md-12 text-left"></div>
                  
                  <?php 
                     $i = 1;
                     foreach($all_array['SRL_TEST'] as $k=>$l){  
                        if( $i < 13 ){
							
                  ?> 
                        <div class="col-md-4 packages">
                           <p><?php echo $l[0] ?> 
                           <?php echo $l[2] ?>&nbsp;
						   <span style="color:green;font-weight:bold;"><?php echo $l[3] ?></span>
                           </p>
                           <span class="price"><a href="#" class="btn btn-success"  data-toggle="modal" data-target="#myModal_<?php echo $i?>" onclick="change_test('<?php echo $i ?>')"><i class="fa fa-inr" aria-hidden="true"></i>. <?php echo $l[1] ?></a></span> 
                        </div>
						
						  
					<!-- Modal -->
						<div id="myModal_<?php echo $i?>" class="modal fade" role="dialog">
						  <div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"><?php echo $l[0] ?> <span style="color:green"><?php echo $l[3] ?></span></h4>
							  </div>
							  <div class="modal-body">
								<?php 
								foreach($all_array['SRL_TEST_DESC'][$i] as $k=>$v) { ?> 
								<table cellpadding="5" cellspacing="5">
									<tr>
										<td width="25%" align="left" valign="top"><strong><?php echo $k?> :</strong></td>
										<td width="80" align="left" valign="top"><?php echo $v ?></td>
									</tr>
									
								</table>
								<?php } ?>
							  </div>
							  <div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							  </div>
							</div>

						  </div>
						</div>
					
                  <?php  
                        } 
                        $i++;
                     } 
                  ?>
                                                                       
            </div>
            <br>
 

			<br>     
            <div class="row">
                
            
            <br><br>
             
              	 
             
                                                  
                  <?php 
                     $i = 1;
                     foreach($all_array['SRL_TEST'] as $k=>$l){  
                        if( $i > 8 ){
                  ?>
                     
                 
						
						<!-- Modal -->
						<div id="myModal_<?php echo $i?>" class="modal fade" role="dialog">
						  <div class="modal-dialog">
							<!-- Modal content-->
							<div class="modal-content">
							  <div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title"><?php echo $l[0] ?> <span style="color:green"><?php echo $l[3] ?></span></h4>
							  </div>
							  <div class="modal-body">
								<?php 
								foreach($all_array['SRL_TEST_DESC'][$i] as $k=>$v) { ?> 
								<table cellpadding="5" cellspacing="5">
									<tr>
										<td width="25%" align="left" valign="top"><strong><?php echo $k?> :</strong></td>
										<td width="80" align="left" valign="top"><?php echo $v ?></td>
									</tr>
									
								</table>
								<?php } ?>
							  </div>
							  <div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							  </div>
							</div>

						  </div>
						</div>
						
                  <?php  
                        } 
                        $i++;
                     } 
                  ?>
                     
                     
             
           
         </div>  
         
         <br>  
      		</div>
      </section>
      
	  <footer>
      	© 2016 - SRL Diagnostics. All Rights Reserved
      </footer>
      <!-- jQuery -->
      <script src="<?php echo S3_URL?>/site/scripts/jquery-1.11.3.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="<?php echo S3_URL?>/site/scripts/bootstrap.js"></script>
      <!-- Plugin JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
      <!-- Theme JavaScript -->
      <script src="<?php echo S3_URL?>/site/srlphadke-css/new-age.min.css"></script>
      <script src="<?php echo S3_URL?>/site/scripts/flatpickr.min.js"?>"></script>
      <script src="<?php echo S3_URL?>/site/scripts/default.js"?>"></script>
      <link href="<?php echo S3_URL?>/site/srlphadke-css/flatpickr.min.css" rel="stylesheet">
      <script>
         $(function() {
         $(".datepicker").flatpickr({            
			minDate: "2016-12-20",
            maxDate: "2017-12-31",
         });   
      });   


      </script>
   </body>
</html>