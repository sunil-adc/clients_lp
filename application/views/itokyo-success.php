<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Etokyo</title>
     
		<link rel="icon" href="https://www.edelweisstokio.in/images/favicon.png" type="image/x-icon">
      <!-- Bootstrap -->
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/itokyo-css/bootstrap.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/itokyo-css/new-age.min.css">
      <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body id="page-top">
      <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
         <div class="container text-center">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">                
               <a class="navbar-brand page-scroll" href="#page-top">
               <!--<img style="margin-top: 20px;margin-left:0px"; src="<?php echo S3_URL?>/site/itokyo-images/logo.png" class="logo"/>-->
               </a>
            </div>
             
            <!-- /.navbar-collapse -->
         </div>
         <!-- /.container-fluid -->
      </nav>
	  
	  
	  <header>
      <?php $all_array = all_arrays(); ?>
         <div class="container form_margin">
            <div class="row">
               
<div class="col-md-8 form_box1">
              <h1><strong style="font-family: Segoe UI; color:#354b60";> 
                     <span style="color: #f05a22 !important">Wealth Ultima</span><br/></strong>
                  </h1>
				<h3>Enjoy Systematic withdrawals and maturity benefit of Rs. 14 lakh for your family by investing only Rs. 6000 pm.</h3>
               </div> 
                 <div class="col-md-4 ">
                  <div class="jumbotron form_box">
                     
                     <div class="row text-center" id="success_div">   
                           <p style="color:#3c4850; margin:150px 0px 120px;font-size: 20px;line-height: 30px; font-weight: bold"> Edelweiss appreciate your interest you will be contacted soon.</p>
                     </div>
                  </div>
               </div>
              
              
            </div>
         </div>
      </header>
           <!-- jQuery -->
      <script src="<?php echo S3_URL?>/site/scripts/jquery-1.11.3.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="<?php echo S3_URL?>/site/scripts/bootstrap.js"></script>
      <!-- Plugin JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
      <!-- Theme JavaScript -->
      <script src="<?php echo S3_URL?>/site/itokyo-css/new-age.min.css"></script>
      <script src="<?php echo S3_URL?>/site/scripts/flatpickr.min.js"?>"></script>
      <script src="<?php echo S3_URL?>/site/scripts/default.js"?>"></script>
      <link href="<?php echo S3_URL?>/site/itokyo-css/flatpickr.min.css" rel="stylesheet">
      
        <script>
         $(function() {
         $(".datepicker").flatpickr({            
			minDate: "2016-12-20",
            maxDate: "2017-12-31",
         });   
      });   


      </script>
	  
	  <script type="text/javascript">
		window.tfa = window.tfa || [];
		_tfa.push({ notify: 'action',name: 'Edelweiss' });
	  </script>
	  <script src="//cdn.taboola.com/libtrc/adcanopusdigitalmedia-drbatra-sc/tfa.js"></script>

   </body>
</html>