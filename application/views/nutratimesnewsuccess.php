<!DOCTYPE html>
<html lang="en">
<head>
  <title>Nutratimes</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  <link rel="stylesheet" href="<?php echo S3_URL?>/site/nutratimes1-assets/css/custom.css">
</head>
<body>
 
<div class="container">
  <div class="row">
    <div class="col-sm-12 text-center">
      <img src="<?php echo S3_URL?>/site/nutratimes1-assets/images/nutratimes_logo.png" class="logo">
    </div>
  </div>

  <div class="row cod">
    <div class="col-sm-12 text-center">
      <h3>Thank You for Your Order</h3> 
    </div>
  </div>
<?php $all_array = all_arrays(); ?>
  <div class="row guarantee">
     
    <div class="col-sm-12 text-left">
       <?php if(isset($user_details) && $user_details != ""){
    foreach ($user_details as $key ) {
   ?>
      <p><b>Congratulations <?php echo($key->name); ?> <?php echo($key->lastname); ?>!</b><br>
        Your order  has been successfully placed. Welcome to the Nutratimes family! We look forward to helping you achieve your diet goals and excited for you to start losing weight in a balanced, safe and healthy way.</p>
    
    
    </div>
  </div>
 
  <div class="row">
    <div class="col-sm-6">
      <h4 class="shipping-box-header">Shipping Address:</h4>
      <p class="shipping-box-container">
        <?php echo($key->name); ?> <?php echo($key->lastname); ?><br>
        <?php echo($key->address); ?><br>
        <?php echo($key->city); ?>, <?php echo $all_array['NUTRATIMES_STATE_ARR'][$key->state]?> <?php echo($key->pincode); ?><br>
        India<br>
        <?php echo($key->phone); ?>
      </p>
    </div>
    
    <div class="col-sm-6 text-center">
      <h4 class="shipping-box-header">Order Information:</h4>
      <p class="shipping-box-container">
        <b>M.R.P.:</b> <?php echo $product_detail['amount'] ?> <br> 
        <b>Discount:</b> <?php echo $product_detail['discount'] ?> <br>
        <b>Total:</b> <?php echo $product_detail['total_amount'] ?> <br>
        <b>You Save:</b> <?php echo $product_detail['discount_amount'] ?> <br>
        <span style="font-size: 11px; font-style: italic">Inclusive of all taxes </span>
      </p>
    </div>
  </div>   
      <?php }
    }?>
  <div class="row">
    <div class="col-sm-12 text-left" style="font-size: 14px">
     <!-- Order Placed On: <b>04/04/2019 10:09am</b>  <br>
      Your order is guaranteed to ship on <b>05/04/2019</b> <br>
      Quantity: <b>Nutratimes Weight Loss Formula - 3 Bottles</b>  (COD)</div>-->
  </div>  
 
  <div class="row">
    <div class="col-sm-12 text-center" style="background: #f1f8fd;width: 100%;padding: 20px;font-weight: 700;font-size: 17px;   margin: 20px 0px;color: #008ed4;"> 
      <span>Need assistance? Get in touch!</span>
    </div>
  </div>
  
  <div class="row">
    <div class="col-sm-12 text-center" style="padding: 20px; font-size: 14px;">  
      Please contact us directly by email at <a href="mailto:support@nutratimes.com" target="_top">support@nutratimes.com</a> if you have any questions or concerns about our products. <br><br><br>Support Hours: <b>9:30 a.m. to 6:30 p.m.</b> (IST)
    </div>
  </div>
  
  
  
  
  <div class="row" style="    background: #fafafa;">
    <div class="col-sm-12 text-center">  
      <img src="<?php echo S3_URL?>/site/nutratimes1-assets/images/partners.jpg" class="img-fluid" style="width: 650px">
    </div>
  </div>
  
</div>

</body>
</html>
