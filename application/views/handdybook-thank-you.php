<?php  $id = $this->session->userdata('user_id'); 
if($id==''){
    header( "Location: ".echo base_url()."index.php/Handdybook/instantsavebills");
} ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Instant Save Bills</title>
    <link rel="shortcut icon" href="<?php echo S3_URL?>/site/handdybook_assets/images/fav.png" type="image/x-icon">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/handdybook_assets/css/style.css">
  
</head>
<body>
    <?php $this->session->unset_userdata('user_id');?>
   <section id="thankyou">
       <div class="container">
        <div class="col-md-12 mobilePaddingZero">
            </div>
            <nav class="navbar ">
                <div class="container-fluid">
                    <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo base_url()."index.php/Handdybook/";?>"><img src="<?php echo S3_URL?>/site/handdybook_assets/images/Logo.png" alt="" srcset=""></a>
                    </div>
                </div>
            </nav>

            <div class="col-md-12 mobilePaddingZero marginTop5em">

                <div class="col-md-7 thankYouMessage">

                    <h1 class="topHeader fontWeight600"> Thanks for registering. <br> Glad to have you with us! </h1>

                    <p class="bigContent">The best deals and offers matching your expense will <br> be sent to your registered email id.</p>

                </div>

                
                    
            </div>

        </div>

       </div>
       
   </section>

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
     <script src="<?php echo S3_URL?>/site/handdybook_assets/js/script.js"></script>

</body>
</html>