<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Handy Book Tips</title>
    <link rel="shortcut icon" href="<?php echo S3_URL?>/site/handdybook_assets/images/fav.png" type="image/x-icon">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/handdybook_assets/css/style.css">
  
</head>

<body>

    <div class="container-fluid">

        <div class="adverSec">
            <p>ADVERTORIAL</p>
        </div>

        <header>

            <a href="<?php echo base_url()."index.php/Handdybook/";?>" style="display: inline-block;"><div class="headerImage">
                <img src="<?php echo S3_URL?>/site/handdybook_assets/images/handybooktips.png" alt="">
            </div></a>

        </header>

        <section class="bodyContainer bottomPadding2em">

            <div class="container containerClass bottomPadding5em">

                <div class="col-md-12">
                    <h1 class="topHeader">Those extra little expenses take a toll on your Bank account, don’t they? Now save a great deal…</h1>

                    <section>
                        <img src="<?php echo S3_URL?>/site/handdybook_assets/images/secodHeaderImage.png" class="img-responsive" alt="">
                    </section>

                    <p class="bigContent">Let’s get it straight! You always end up paying more on your banal (day-to-day)  goods and services. <br> Hiked prices, additional charges… !!! We totally understand you.  And that’s where we come to your rescue! <b> handybooktips.com </b> is here to help  millions of families save that extra, unwanted cost. <br> Simply put, we legit help you save money on various bills you pay all the time.</p>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12 bottomPadding5em">
                    <h1 class="topHeader"> Travel, shopping or more! Every year, new deal for you.. Switch and give your bank account a break. FOR ABSOLUTELY FREE</h1>

                    <section>
                        <img src="<?php echo S3_URL?>/site/handdybook_assets/images/bill_reduced.png" class="img-responsive" alt="">
                    </section>

                    <p class="bigContent">All you have to do is register once (FOR FREE) </p>


                    <p class="bigContent">Now, be a part of millions of people who are saving a great deal by joining us.</p>

                  

                    <p class="bigContent">Remember, money will save you if you save it!</p>

                </div>
                
                 <button class="btn btn-primary firstContainerButton"> Click Here Join Look After My Bills </button>

            </div>

        </section>

        

    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="<?php echo S3_URL?>/site/handdybook_assets/js/script.js"></script>
</body>
</html>