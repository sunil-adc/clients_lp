<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Etokyo</title>
     
		<link rel="icon" href="https://www.edelweisstokio.in/images/favicon.png" type="image/x-icon">
      <!-- Bootstrap -->
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/itokyo-css/bootstrap.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/itokyo-css/new-age.min.css">
      <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body id="page-top">
      <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
         <div class="container text-center">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">                
               <a class="navbar-brand page-scroll" href="#page-top">
               <!--<img style="margin-top: 20px;margin-left:0px"; src="<?php echo S3_URL?>/site/itokyo-images/logo.png" class="logo"/>-->
               </a>
            </div>
             
            <!-- /.navbar-collapse -->
         </div>
         <!-- /.container-fluid -->
      </nav>
	  
	  
	  <header>
      <?php $all_array = all_arrays(); ?>
         <div class="container form_margin">
            <div class="row">
               
<div class="col-md-8 form_box1">
              <h1><strong style="font-family: Segoe UI; color:#354b60";> 
                     <span style="color: #f05a22 !important">Wealth Ultima</span><br/></strong>
                  </h1>
				<h3>Enjoy Systematic withdrawals and maturity benefit of Rs. 14 lakh for your family by investing only Rs. 6000 pm.</h3>
               </div> 
                 <div class="col-md-4 ">
                  <div class="jumbotron form_box">
                     <div class="row text-center" id="f_div" >
                        <div class="text-center  col-md-12 col-lg-12">
                           <h4>I am Intrested in</h4>
                        </div>
                        <div class="text-center col-lg-12">
                           <!-- CONTACT FORM https://github.com/jonmbake/bootstrap3-contact-form -->
                           <form role="form" id="feedbackForm" class="srl_form" action="JavaScript:void(0)" onsubmit="itokyo_jsfrm('<?php echo SITE_URL?>etokio/itokyofrm')">
                              <div class="form-group">
                                 <!--<label for="name">Name</label>-->
                                 <select class="form-control" id="plan" name="plan" onchange="chck_valid_tokyo('plan', 'Please select correct plan')" 
								 data-attr="Please select plan">
                                       <option value="" >Choose Plan</option>
                                       <option value="1" >Retirement Plan</option>
                                       <option value="2" >Pension Plan</option>
                                       <option value="3" >Child Plan </option>
                                       <option value="4" >ULIP</option> 
									 	<option value="5" >Savings Plan</option> 
                                 </select>
                                 <span class="help-block" id="plan_err"></span>
                              </div>
                              <div class="form-group">
                                 <input type="text" class="form-control" id="name" name="name" placeholder="Your Full Name" onkeyup="chck_valid_tokyo('name', 'Please enter correct name')" data-attr="Please enter Full Name">
                                 <span class="help-block" id="name_err" ></span>
                              </div> 
                              <div class="form-group">
                                 <input type="number" class="form-control only_numeric" id="phone" name="phone" pattern="\d*" maxlength="10"placeholder="Mobile Number" onkeyup="chck_valid_tokyo('Mobile', 'Please enter correct Mobile')" data-attr="Please enter correct Mobile number">
                                 <span class="help-block" id="phone_err"> </span>
                              </div>
                              <div HEALTH CHAMPION PACKAGE ADVANCEDclass="form-group">
                                 <select class="form-control" id="city" name="city" onchange="chck_valid_tokyo('city', 'Please select correct city')" 
								 data-attr="Please select city">
								 <option value="" >Select city</option>
								 <?php foreach($city_arr as $city){?>
                                       
                                       <option value="<?php echo $city['city'];?>" ><?php echo $city['city'];?></option>
								 <?php }?>   
                                 </select>

                                 <span class="help-block" id="city_err"></span>
                              </div>
                              <div class="form-group">
                                 <input type="number" class="form-control only_numeric" id="income" name="income" pattern="\d*" maxlength="10"placeholder="Income Per Month" onkeyup="chck_valid_tokyo('income', 'Please enter correct Salary')" data-attr="Please enter correct Salary">
                                 <span class="help-block" id="income_err"> </span>
                              </div>
                              <div class="form-group">
                                 <select class="form-control" id="age" name="age" onchange="chck_valid_tokyo('age', 'Please select correct city')" 
								 data-attr="Please select age">
                                       <option value="" >Select Age</option>
                                       <option value="1" >0 - 5</option>
                                       <option value="2" >5 - 10</option>
                                       <option value="3" >10 - 15</option>
                                       <option value="4" >15 - 20</option>
                                       <option value="5" >20 - 25</option>
                                       <option value="6" >25 - 30</option>
                                       <option value="7" >30 - 35</option>
                                       <option value="8" >35 - 40</option>
                                       <option value="9" >40 - 45</option>
                                       <option value="10" >45 - 50</option>
                                       <option value="11" >50 - 55</option>
                                       <option value="12" >55 - 60</option>
                                       <option value="13" >60+</option>
                                 </select>
                                 <span class="help-block" id="age_err"></span>
                              </div>
							  <input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : "d_sr"); ?>">
							<input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "d_ut"); ?>">
							
							  
							  
                              <input type="submit" id="frm-sbmtbtn" class="btn btn-primary btn-lg" value="SUBMIT">
                           </form>
                           <!-- END CONTACT FORM --> 
                        </div>
                     </div>
                     <div class="row text-center hidden_div" id="success_div">   
                           <p style="color:#3c4850; margin:150px 0px 120px;font-size: 20px;line-height: 30px; font-weight: bold"> Edelweiss appreciate your interest you will be contacted soon.</p>
                     </div>
                  </div>
               </div>
              
              
            </div>
         </div>
      </header>
           <!-- jQuery -->
      <script src="<?php echo S3_URL?>/site/scripts/jquery-1.11.3.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="<?php echo S3_URL?>/site/scripts/bootstrap.js"></script>
      <!-- Plugin JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
      <!-- Theme JavaScript -->
      <script src="<?php echo S3_URL?>/site/itokyo-css/new-age.min.css"></script>
      <script src="<?php echo S3_URL?>/site/scripts/flatpickr.min.js"?>"></script>
      <script src="<?php echo S3_URL?>/site/scripts/default.js"?>"></script>
      <link href="<?php echo S3_URL?>/site/itokyo-css/flatpickr.min.css" rel="stylesheet">
      
        <script>
         $(function() {
         $(".datepicker").flatpickr({            
			minDate: "2016-12-20",
            maxDate: "2017-12-31",
         });   
      });   


      </script>
   </body>
</html>