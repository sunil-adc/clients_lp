
<!DOCTYPE html>
<?PHP $all_array = all_arrays(); ?>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Jubination</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo S3_URL?>/site/jubi-vendor/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="<?php echo S3_URL?>/site/jubi-vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="<?php echo S3_URL?>/site/jubi-vendor/css/clean-blog.css" rel="stylesheet"> 

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" type="text/javascript"></script>

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand" href="#" style="cursor: default;">
			<img src="<?php echo S3_URL?>/site/jubi-img/logo.png">
		</a>
		
		<a class="navbar-brand pull-right" href="#" style="cursor: default;">
			<img src="<?php echo S3_URL?>/site/jubi-img/associate.png">
		</a>
      </div>
    </nav>

    <!-- Page Header -->
    <header class="masthead" style="background-image: url('<?php echo S3_URL?>/site/jubi-vendor/jubi-img/home-bg.jpg')">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mx-auto">
           
            <div class="site-heading" id="frm_full">
              <h1>BEST PREVENTIVE HEALTH CHECKUP OFFERS</h1>
			  <h3>COMPREHENSIVE HEALTH CHECKUP</h3>
			  <span class="subheading">Most Imp Blood Tests at Home + Free Health Insurance <br>Consultation by Jubination.</span>
			<span class="subheading" style="color:#2d2d2d;font-size:21px;">Top accredited lab + Dedicated health manager by Jubination.</span>
				
				<div class="form_box">
					<div class="health_program">Health Programs starting 
						<span class="pres_price">2400/-</span> only
					 </div>
					<div class="form_fields"> 
						<h2>To avail discount register below 
							<span class="error" id="frm_error"></span>
						</h2>						
						<div class="clearfix"></div> 						 
						<form style="display: flex;" method="post" action="JavaScript:void(0)" onsubmit="jubi_jsfrm('<?php echo SITE_URL?>jubination/jubi_firstfrm');">		
						    <div class="row main_form " id="first_frm">	 
						<div class="col-sm-2">
							<div class="form-group">
								<label for="name">Enter Your Name :</label>
								<input type="name" class="form-control" id="name" name="name" placeholder="Full name" required onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name" tabindex="1">
							</div>
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label for="Mobile">Enter Mobile Number :</label>
								<input type="phone" class="form-control only_numeric" id="phone" name="phone" placeholder="Mobile Number" required pattern="\d*" maxlength="10" tabindex="2">
						</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<label for="email">Enter Your Email :</label>
									<input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" required onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email" tabindex="3">
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<label for="email">Choose Package :</label>
									  <select class="form-control" id="pkg" name="pkg" required onchange="chck_valid('pkg', 'Please select package')" 
							 data-attr="Please select package" tabindex="4">
									  <option value='0' >SELECT</option>
									  <?php
										if (count($all_array['JUBINATION_PKG']) > 0) {
											foreach($all_array['JUBINATION_PKG'] as $key=>$val){
												echo "<option value='".$key."' >".$val."</option>";
											}
										}?>       									   
									  </select>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
								<label for="email">Select City :</label>
								<input type="text" name="city" class="form-control required ui-autocomplete-input valid" id="city" placeholder="Select City" autocomplete="off" aria-required="true" aria-invalid="false" required onkeyup="chck_valid('city', 'Please enter city')" data-attr="Please enter city" tabindex="5">
								</div>
							</div>
							<div class="col-sm-2" style="padding-right: 15px!important;">
								<div class="form-group" style="text-align:left">
									<label for="email"> </label>
									<input type="submit" class="btn btn-warning" tabindex="6" value="Submit" id="frm-sbmtbtn1">
									<input type="hidden" id="pub" name="pub" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : "d_pb"); ?>">
									<input type="hidden" id="utm" name="utm" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "d_ut"); ?>">
								</div>
							</div>
						
						</div>	 
						</form>
						 
						<form style="width:100%" method="post" action="javascript:void(0)" onsubmit="javascript:jubi_secfrmjs('<?php echo SITE_URL?>jubination/jubi_secfrm');">
						<div class="row main_form display_hide" id="sec_frm">
							
							<div class="col-sm-2">
								<div class="form-group">
									<label for="date">Appointment date :</label>
									<input type="text" class="form-control datepicker" id="jubi_date" name="jubi_date" onkeyup="chck_valid('jubi_date', 'Please select Date slot')" data-attr="Please select Date slot" tabindex="1">
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<label for="time">Select Time Slot :</label>
									   <select class="form-control" id="jubi_time" name="jubi_time" onchange="chck_valid('jubi_time', 'Please select time slot')" data-attr="Please select time slot" tabindex="2">
										    <option value="">Select Time Slot</option>
                                			
											<?php
											if (count(JUBINATION_SLOT) > 0) {
												foreach($all_array['JUBINATION_SLOT'] as $key=>$val){
													echo "<option value='".$key."' >".$val." AM</option>";
												}
											}?>   
                                			              
									   </select>
								</div>
							</div>
							<div class="col-sm-4" style=" padding-right: 0px;">
								<div class="form-group">
									<label for="address">Address :</label>
									<input type="text" class="form-control" id="address" 
									placeholder="Enter Full Address" onkeyup="chck_valid('address', 'Please enter correct address')" data-attr="Please enter correct address" tabindex="3">
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<label for="date">Pincode :</label>
									<input type="text" class="form-control" id="jubi_pincode" name="jubi_pincode" onkeyup="chck_valid('jubi_pincode', 'Please enter pincode')" data-attr="Please enter pincode" tabindex="1" pattern="\d*" maxlength="6">
								</div>
							</div>
							<div class="col-sm-2" style="padding-right: 15px!important;">
								<div class="form-group" style="text-align:left">
									<label for="button"> </label>
									<input type="submit" class="btn btn-warning" tabindex="4" value="Submit" id="frm-sbmtbtn2">
								</div>
							</div>
						</div>
						
						
						</form>
						
						
					</div>					
				</div>
					<div class="terms">
					* Service availability PAN India in selected cities and within city limits for home service.
						&nbsp;&nbsp;&nbsp;
					* Get tax exemption on Preventive Health Check-up. Save tax up to Rs.5000/- U/s- 80D
					</div>
            </div>
          
          	<div class="success_msz display_hide" id="success_msg">
          		<h1>THANK YOU!</h1>
				<h5>In case of any query, whatsapp us on <strong>7700967024</strong> or mail us on <strong>support@jubination.com</strong>.</h5>   
        		<br>
         		<hr>
         		<br>
				<h3>Here are some important details that shall help you:</h3>
				<ul class="listitem">
				<li>Your nearest technician will call you on your registered mobile number before your appointment date and confirm an appointment . You may also receive an email/SMS notification of the same</li>
				<li>Blood samples will be collected from the address you suggested here</li>
				<li>You need to do overnight fasting of 10-12 hours before the sample collection. You can drink water or have your medicines if any. Please avoid eating any other food item during the fasting period or drinking any other liquid apart from water</li>
				<li>In case you are paying by cash, you need to make the payment directly to the technician at the time of sample-collection. The technician is mandated to give you a receipt in against the cash</li>
				<li>Soft-copy of the health checkup reports shall be emailed to your registered email id within 48 hours of the sample-collection</li>
         	    </ul>
          	</div>
          </div>
        </div>
      </div>
    </header>

    <!-- Main Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
		<br>
          <div class="post-preview">
            <h2 class="text-center normal-font">Regular health packages for couple :</h2>
          </div> 
		<br>		  
        </div>
		<div class="col-md-12 mx-auto packages">
			<a href="#tab1" onclick="javascript:slide_focus('1')"  class="body-chkup" role="tab" data-toggle="tab">
				<div class="bc_content">
					<h2>FULL BODY CHECKUP PACKAGE FOR COUPLE.</h2>
					<h4>68 Most Imp Blood Tests at Home<br>
					Pro Aarogyam 1.3 (68 tests) for 2 People</h4> 
					Rs. <span class="prev_price">4400/-</span>
					<span class="pres_price">2900/-</span>
					Only
				</div>
			</a>
		</div>
		<div class="col-md-12 mx-auto packages">
			<a href="#tab2" onclick="javascript:slide_focus('2')"  class="body-chkup health-programe" role="tab" data-toggle="tab">
				<div class="bc_content">
					<h2>COMPREHENSIVE HEALTH CHECKUP.</h2>
					<h4>72 Most Imp Blood Tests at Home</h4> 
					Rs. <span class="prev_price">7960/-</span>
					<span class="pres_price">3000/-</span>
					 Only
				</div>
			</a>
		</div>			
		<div class="col-md-12 mx-auto packages">
			<a href="#tab3" onclick="javascript:slide_focus('3')"  class="body-chkup diabetes" role="tab" data-toggle="tab">
				<div class="bc_content">
					<h2>DIABETES COUPLE PACKAGE.</h2>
					<h4>67 Most Imp Blood Tests at Home</h4> 
					Rs. <span class="prev_price">5000/-</span>
					<span class="pres_price">2600/-</span>
					Only
				</div>
			</a>
		</div>
		
		<div class="col-md-12 mx-auto">
		<br><br>
        <div class="post-preview"><h2 class="text-center normal-font">Master health packages :</h2></div> <br> 
		<div class="col-md-6 col-sm-12 wpm">
			<a href="#tab4" onclick="javascript:slide_focus('4')"  class="body-chkup wpm-male" role="tab" data-toggle="tab">
				<div class="bc_content">
					<h2>WELLNESS PLATINUM MALE</h2>
					<h4>(93 Tests including Treadmill + Ultrasound +<br>
					X-ray+ ECG + Pulmonary Function Test)</h4> 
					Rs. <span class="prev_price" style="text-decoration: line-through;">8999/-</span>
					<span class="pres_price">5999/-</span> Only
				</div>
			</a>
		</div>	
		<div class="col-md-6 col-sm-12 wpm">
			<a href="#tab5" onclick="javascript:slide_focus('5')" class="body-chkup wpm-male wpm-female" role="tab" data-toggle="tab">
				<div class="bc_content">
					<h2>WELLNESS PLATINUM FEMALE</h2>
					<h4>(94 Tests including Treadmill + Ultrasound +<br>
					X-ray+ ECG + Pulmonary Function Test + Pap smear.) </h4>
					Rs. <span class="prev_price" style="text-decoration: line-through;">9999/-</span>					
					<span class="pres_price">6999/-</span> Only
				</div>
			</a>
		</div>			
        </div>	
	 
	 <div class="container">
		<div class="row">
			<div class="col-md-12"> 
			<div class="card">
				<ul class="nav nav-tabs tabname" role="tablist">
					<li role="presentation" id="l1" class="active"><a href="#tab1" aria-controls="home" role="tab" data-toggle="tab" onclick="javascript:make_active('1')">Full body checkup for couple</a></li>
					<li role="presentation" id="l2"><a href="#tab2" aria-controls="messages" role="tab" data-toggle="tab" onclick="javascript:make_active('2')">Comprehensive Health Checkup</a></li>
					<li role="presentation" id="l3"><a href="#tab3" aria-controls="messages" role="tab" data-toggle="tab" onclick="javascript:make_active('3')">Diabetes Couple Package</a></li>
					<li role="presentation" id="l4"><a href="#tab4" aria-controls="settings" role="tab" data-toggle="tab" onclick="javascript:make_active('4')">Wellness Platinum Male</a></li>
					<li role="presentation" id="l5"><a href="#tab5" aria-controls="settings" role="tab" data-toggle="tab" onclick="javascript:make_active('5')">Wellness Platinum Female</a></li>
				</ul> 
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="tab1">
						<div class="col-lg-12 col-md-12 test">
						<H4>FULL BODY CHECKUP PACKAGE FOR COUPLE - Tests included :</H4>
							<li class="testcode">Vitamin Profile -</li> <li class="testname">Vitamin D (Total), Vitamin B12</li>
							<li class="testcode">Thyroid Profile -</li> <li class="testname">Thyroid Stimulating Hormone (TSH), Total Thyroxine (T4), Total Triiodothyronine (T3)</li>
							<li class="testcode">Diabetes Profile -</li> <li class="testname">Glycosylated hemoglobin (HbA1c), Average blood glucose</li>
							<li class="testcode">Lipid (cholesterol) Profile -</li> <li class="testname">Total Cholesterol (TC), HDL Cholesterol, LDL Cholesterol, VLDL Cholesterol, Triglycerides, TC/HDL Cholesterol ratio, LDL / HDL ratio</li>
							<li class="testcode">Complete Blood Count -</li> <li class="testname">Hemoglobin, Platelets and 26 other parameters</li>
							<li class="testcode">Iron Deficiency Profile -</li> <li class="testname">Iron, Total iron binding capacity (TIBC), % Transferrin saturation</li>
							<li class="testcode">Liver Profile -</li> <li class="testname">SGOT, SGPT, Total Protein, Serum Albumin, Serum Globulin, Serum Albumin/ Globulin ratio, Bilirubin (Total), Bilirubin (Direct), Bilirubin (Indirect), Alkaline Phosphatase, Gamma Glutamyl Transferase (GGT)</li>
							<li class="testcode">Kidney Profile -</li> <li class="testname">Serum Creatinine, Uric acid, Blood Urea Nitrogen (BUN),  BUN / Sr.Creatinine ratio, Calcium</li>
							<li class="testcode">Cardiac Risk Marker -</strong>  <li class="testname">Homocysteine</li>
							<li class="testcode">Electolytes Profile -</li> <li class="testname">Chloride, Sodium</li>
							<li class="testcode">Pancreatic Profile -</li> <li class="testname">Amylase, Lipase</li>					
						</div> 
					</div>
					 
					<div role="tabpanel" class="tab-pane" id="tab2">
						<div class="col-lg-12 col-md-12 test">
						<H4>COMPREHENSIVE HEALTH CHECKUP - Tests included :</H4>
							<li class="testcode">Thyroid Profile -</li> <li class="testname">Thyroid Stimulating Hormone (TSH), Total Thyroxine (T4), Total Triiodothyronine (T3)</li>
							<li class="testcode">Diabetes Profile -</li> <li class="testname">Glycosylated hemoglobin (HbA1c), Average blood glucose</li>
							<li class="testcode">Lipid -</li> <li class="testname">cholesterol) Profile Total Cholesterol (TC), HDL Cholesterol, LDL Cholesterol, VLDL Cholesterol, Triglycerides, TC/HDL Cholesterol ratio, LDL / HDL ratio </li>
							<li class="testcode">Complete Blood Count -</li> <li class="testname">Hemoglobin, Platelets and 26 other parameters</li>
							<li class="testcode">Iron Deficiency ProfileIron -</li> <li class="testname">Total iron binding capacity (TIBC), % Transferrin saturation</li>
							<li class="testcode">Liver Profile -</li> <li class="testname">SGOT, SGPT, Total Protein, Serum Albumin, Serum Globulin, Serum Albumin/ Globulin ratio, Bilirubin (Total), Bilirubin (Direct), Bilirubin (Indirect), Alkaline Phosphatase, Gamma Glutamyl Transferase (GGT)</li>
							<li class="testcode">Kidney Profile -</li> <li class="testname">Serum Creatinine, Uric acid, Blood Urea Nitrogen (BUN), BUN / Sr.Creatinine ratio, Calcium</li>
							<li class="testcode">Cardiac Risk Marker -</li> <li class="testname">Homocysteine</li>
							<li class="testcode">Pancreatic Profile -</li>  <li class="testname">Amylase, Lipase</li>
							<li class="testcode">Toxic Elements -</li> <li class="testname">Arsenic, Barium, Cadmium, Cobalt, Chromium, Caesium, Mercury, Lead, Selenium	</li>
						</div>	
					</div> 
						 	 
					<div role="tabpanel" class="tab-pane" id="tab3">
						<div class="col-lg-12 col-md-12 test">
						<H4>DIABETES COUPLE PACKAGE - Tests included :</H4>
							<li class="testcode">Liver Profile - </li> <li class="testname"> Gamma Glutamyl Transferase (GGT), Alkaline Phosphatase, Bilirubin - Direct , Bilirubin - Total,Bilirubin -Indirect,Protein-Total ,Serum Albumin,Serum Globulin ,SGOT(AST), SGPT(ALT) , Serum Albumin/ Globulin Ratio</li>
							<li class="testcode">Cholesterol Profile - </li> <li class="testname">LDL Cholesterol,Total Cholesterol,HDL Cholesterol,Non HDL Cholesterol,Triglycerides, VLDL Cholesterol,LDL / HDL ratio, TC/HDL Cholesterol Ratio</li>
							<li class="testcode">Kidney Profile - </li> <li class="testname"> Calcium,Uric acid, Blood Urea Nitrogen (BUN),Serum Creatinine, BUN / Serum Creatinine ratio,</li>
							<li class="testcode">Iron Deficiency Profile - </li> <li class="testname">Total Iron Binding Capacity ,Serum Iron, % Transferrin saturation</li>
							<li class="testcode">Thyroid Profile - </li> <li class="testname"> Total Triiodothyronine, Total Thyroxine,Thyroid Stimulating Hormone(TSH)</li>
							<li class="testcode">Diabetic Screen - </li> <li class="testname">HbA1c,Average Blood Glucose</li>
							<li class="testcode">Electrolytes Profile - </li> <li class="testname"> Sodium, Chloride</li>					
							<li class="testcode">Complete Hemoglobin -</li><li class="testname">28 Tests </li>
							<li class="testcode">Urinary microalbumin  -</li><li class="testname">&nbsp; </li>
						</div>	
					</div>
					
					<div role="tabpanel" class="tab-pane" id="tab4">
					
						<div class="col-lg-12 col-md-12 test">
				<H4>WELLNESS PLATINUM MALE - Tests included :</H4>
					<li class="testtitle">ARTHRITIS</li>
					<div class="clear"></div>
					<li class="testcode">ACCP -</li> <li class="testname">ANTI CCP (ACCP)</li>
					<li class="testcode">ANA -</li> <li class="testname">ANTI NUCLEAR ANTIBODIES (ANA)</li>
					
					<li class="testtitle">CANCER</li>
					<li class="testcode">PSA -</li> <li class="testname">PROSTATE SPECIFIC ANTIGEN (PSA)</li> 
					
					<li class="testtitle">CARDIAC RISK MARKERS</li>
					<li class="testcode">APOB -</li> <li class="testname">APOLIPOPROTEIN - B (APO-B)</li> 
					<li class="testcode">APB/ -</li> <li class="testname">APO B / APO A1 RATIO (Apo B/A1)</li> 
					<li class="testcode">LPA -</li> <li class="testname">LIPOPROTEIN (A) [LP(A)]</li> 
					<li class="testcode">HOMO -</li> <li class="testname">HOMOCYSTEINE</li> 
					<li class="testcode">HSCRP -</li> <li class="testname">HIGH SENSITIVITY C-REACTIVE PROTEIN (hs-CRP)</li> 
					<li class="testcode">PLA2 -</li> <li class="testname">LIPOPROTEIN ASSOCIATED PHOSPHOLIPASE A2 (LP-PLA2)</li> 
					<li class="testcode">APOA -</li> <li class="testname">APOLIPOPROTEIN - A1 (APO-A1)</li> 
					
					<li class="testtitle">COMPLETE HEMOGRAM</li> 
					<li class="testcode">MCH -</li> <li class="testname">MEAN CORPUSCULAR HEMOGLOBIN(MCH)</li> 
					<li class="testcode">LYMPH -</li> <li class="testname">LYMPHOCYTE PERCENTAGE</li> 
					<li class="testcode">IG% -</li> <li class="testname">IMMATURE GRANULOCYTE PERCENTAGE(IG%)</li> 
					<li class="testcode">RBC -</li> <li class="testname">TOTAL RBC</li> 
					<li class="testcode">RDWSD -</li> <li class="testname">RED CELL DISTRIBUTION WIDTH - SD(RDW-SD)</li> 
					<li class="testcode">RDCV -</li> <li class="testname">RED CELL DISTRIBUTION WIDTH (RDW-CV)</li> 
					<li class="testcode">PCV -</li> <li class="testname">HEMATOCRIT(PCV)</li> 
					<li class="testcode">PLT -</li> <li class="testname">PLATELET COUNT</li> 
					<li class="testcode">BASO -</li> <li class="testname">BASOPHILS</li> 
					<li class="testcode">MCHC -</li> <li class="testname">MEAN CORP.HEMO.CONC(MCHC)</li> 
					<li class="testcode">EOS -</li> <li class="testname">EOSINOPHILS</li> 
					<li class="testcode">AMON -</li> <li class="testname">MONOCYTES - ABSOLUTE COUNT</li> 
					<li class="testcode">ANEU -</li> <li class="testname">NEUTROPHILS - ABSOLUTE COUNT</li> 
					<li class="testcode">HB -</li> <li class="testname">HEMOGLOBIN</li> 
					<li class="testcode">NRBC -</li> <li class="testname">NUCLEATED RED BLOOD CELLS</li> 
					<li class="testcode">PLCR -</li> <li class="testname">PLATELET TO LARGE CELL RATIO(PLCR)</li> 
					<li class="testcode">PDW -</li> <li class="testname">PLATELET DISTRIBUTION WIDTH(PDW)</li> 
					<li class="testcode">MCV -</li> <li class="testname">MEAN CORPUSCULAR VOLUME(MCV)</li> 
					<li class="testcode">ABAS -</li> <li class="testname">BASOPHILS - ABSOLUTE COUNT</li> 
					<li class="testcode">PCT -</li> <li class="testname">PLATELETCRIT(PCT)</li> 
					<li class="testcode">IG -</li> <li class="testname">IMMATURE GRANULOCYTES(IG)</li> 
					<li class="testcode">ALYM -</li> <li class="testname">LYMPHOCYTES - ABSOLUTE COUNT</li> 
					<li class="testcode">MPV -</li> <li class="testname">MEAN PLATELET VOLUME(MPV)</li> 
					<li class="testcode">NRBC% -</li> <li class="testname">NUCLEATED RED BLOOD CELLS %</li> 
					<li class="testcode">LEUC -</li> <li class="testname">TOTAL LEUCOCYTES COUNT</li> 
					<li class="testcode">MONO -</li> <li class="testname">MONOCYTES</li> 
					<li class="testcode">AEOS -</li> <li class="testname">EOSINOPHILS - ABSOLUTE COUNT</li> 
					<li class="testcode">NEUT -</li> <li class="testname">NEUTROPHILS</li> 
					
					
					<li class="testtitle">DIABETES</li> 
					<li class="testcode">HBA -</li> <li class="testname">HbA1c</li> 
					<li class="testcode">ABG -</li> <li class="testname">AVERAGE BLOOD GLUCOSE (ABG)</li> 

					<li class="testtitle">ELECTROLYTES</li> 
					<li class="testcode">SOD -</li> <li class="testname">SODIUM</li> 
					<li class="testcode">CHL -</li> <li class="testname">CHLORIDE</li> 
	
					<li class="testtitle">INVIVO</li> 
					<li class="testcode">ECG -</li> <li class="testname">ELECTROCARDIOGRAM</li> 
					<li class="testcode">PC -</li> <li class="testname">PHYSICIAN CONSULTATION</li> 
					<li class="testcode">US -</li> <li class="testname">ULTRASOUND (ABDOMEN / PELVIS)</li> 
					<li class="testcode">XRAY -</li> <li class="testname">X-RAY (CHEST PA / LATERAL)</li> 
					<li class="testcode">PFT -</li> <li class="testname">PULMONARY FUNCTION TEST</li> 
					<li class="testcode">TMT -</li> <li class="testname">TREADMILL TEST</li>  
	
					<li class="testtitle">IRON DEFICIENCY</li> 
					<li class="testcode">TIBC -</li> <li class="testname">TOTAL IRON BINDING CAPACITY (TIBC)</li> 
					<li class="testcode">IRON -</li> <li class="testname">IRON</li> 
					<li class="testcode">FERR -</li> <li class="testname">FERRITIN</li> 
					<li class="testcode">%TSA -</li> <li class="testname">% TRANSFERRIN SATURATION</li>  
	
					<li class="testtitle">LIPID</li> 
					<li class="testcode">NHDL -</li> <li class="testname">NON-HDL CHOLESTEROL</li> 
					<li class="testcode">LDL/ -</li> <li class="testname">LDL / HDL RATIO</li> 
					<li class="testcode">LDL -</li> <li class="testname">LDL CHOLESTEROL - DIRECT</li> 
					<li class="testcode">VLDL -</li> <li class="testname">VLDL CHOLESTEROL</li>  
					<li class="testcode">TC/H -</li> <li class="testname">TC/ HDL CHOLESTEROL RATIO</li>  
					<li class="testcode">CHOL -</li> <li class="testname">TOTAL CHOLESTEROL</li>  
					<li class="testcode">HCHO -</li> <li class="testname">HDL CHOLESTEROL - DIRECT</li>  
					<li class="testcode">TRIG -</li> <li class="testname">TRIGLYCERIDES</li>  
	
					<li class="testtitle">LIVER</li> 
					<li class="testcode">SGPT -</li> <li class="testname">ALANINE TRANSAMINASE (SGPT)</li> 
					<li class="testcode">GGT -</li> <li class="testname">GAMMA GLUTAMYL TRANSFERASE (GGT)</li> 
					<li class="testcode">SEGB -</li> <li class="testname">SERUM GLOBULIN</li> 
					<li class="testcode">BILT -</li> <li class="testname">BILIRUBIN – TOTAL</li>  
					<li class="testcode">PROT -</li> <li class="testname">PROTEIN – TOTAL</li>  
					<li class="testcode">A/GR -</li> <li class="testname">SERUM ALB/GLOBULIN RATIOL</li>  
					<li class="testcode">BILD -</li> <li class="testname">BILIRUBIN –DIRECT</li>  
					<li class="testcode">ALKP -</li> <li class="testname">ALKALINE PHOSPHATASE</li>
					<li class="testcode">BILI -</li> <li class="testname">BILIRUBIN (INDIRECT)</li>
					<li class="testcode">SGOT -</li> <li class="testname">ASPARTATE AMINOTRANSFERASE (SGOT )</li>
					<li class="testcode">SALB -</li> <li class="testname">ALBUMIN – SERUM</li>					
		
					<li class="testtitle">PANCREATIC</li> 
					<li class="testcode">LASE -</li> <li class="testname">LIPASE</li> 
					<li class="testcode">AMYL -</li> <li class="testname">AMYLASE</li> 
		
					<li class="testtitle">RENAL</li> 
					<li class="testcode">BUN -</li> <li class="testname">BLOOD UREA NITROGEN (BUN)</li> 
					<li class="testcode">CALC -</li> <li class="testname">CALCIUM</li> 
					<li class="testcode">URIC -</li> <li class="testname">URIC ACID</li> 
					<li class="testcode">SCRE -</li> <li class="testname">CREATININE – SERUM</li> 
					<li class="testcode">B/CR -</li> <li class="testname">BUN / Sr.CREATININE RATIO</li>  
	
					<li class="testtitle">THYROID</li> 
					<li class="testcode">T3 -</li> <li class="testname">TOTAL TRIIODOTHYRONINE (T3)</li> 
					<li class="testcode">TSH -</li> <li class="testname">THYROID STIMULATING HORMONE (TSH)</li> 
					<li class="testcode">T4 -</li> <li class="testname">TOTAL THYROXINE (T4)</li> 
				
					<li class="testtitle">TOXIC ELEMENTS</li>  
					<li class="testcode">BTEBA -</li> <li class="testname">BARIUM</li> 
					<li class="testcode">BTECS -</li> <li class="testname">CAESIUM</li> 
					<li class="testcode">BTECO -</li> <li class="testname">COBALT</li> 
					<li class="testcode">BTEPB -</li> <li class="testname">LEAD</li> 
					<li class="testcode">BTECD -</li> <li class="testname">CADMIUM</li> 
					<li class="testcode">BTECR -</li> <li class="testname">CHROMIUM</li> 
					<li class="testcode">BTEHG -</li> <li class="testname">MERCURY</li> 
					<li class="testcode">BTEAS -</li> <li class="testname">ARSENIC</li> 
					<li class="testcode">BTESE -</li> <li class="testname">SELENIUM</li> 
	
					<li class="testtitle">VITAMIN</li>  
					<li class="testcode">FOLI -</li> <li class="testname">FOLATE</li> 
					<li class="testcode">VITB -</li> <li class="testname">VITAMIN B-12</li> 
					<li class="testcode">VITDC -</li> <li class="testname">25-OH VITAMIN D (TOTAL)</li> 

				</div>	 
        
					</div>
					
					<div role="tabpanel" class="tab-pane" id="tab5">
						<div class="col-lg-12 col-md-12 test">
				<H4>WELLNESS PLATINUM FEMALE - Tests included :</H4>
					<li class="testtitle">ARTHRITIS</li>
					<div class="clear"></div>
					<li class="testcode">ANA -</li> <li class="testname">ANTI NUCLEAR ANTIBODIES (ANA)</li>
					<li class="testcode">ACCP -</li> <li class="testname">ANTI CCP (ACCP)</li>
					
					
					<li class="testtitle">CANCER</li>
					<li class="testcode">C125 -</li> <li class="testname">CA-125</li> 
					
					<li class="testtitle">CARDIAC RISK MARKERS</li>
					<li class="testcode">APOB -</li> <li class="testname">APOLIPOPROTEIN - B (APO-B)</li> 
					<li class="testcode">APB/ -</li> <li class="testname">APO B / APO A1 RATIO (Apo B/A1)</li> 
					<li class="testcode">LPA -</li> <li class="testname">LIPOPROTEIN (A) [LP(A)]</li> 
					<li class="testcode">HOMO -</li> <li class="testname">HOMOCYSTEINE</li> 
					<li class="testcode">HSCRP -</li> <li class="testname">HIGH SENSITIVITY C-REACTIVE PROTEIN (hs-CRP)</li> 
					<li class="testcode">PLA2 -</li> <li class="testname">LIPOPROTEIN ASSOCIATED PHOSPHOLIPASE A2 (LP-PLA2)</li> 
					<li class="testcode">APOA -</li> <li class="testname">APOLIPOPROTEIN - A1 (APO-A1)</li> 
					
					<li class="testtitle">COMPLETE HEMOGRAM</li> 
					<li class="testcode">MCH -</li> <li class="testname">MEAN CORPUSCULAR HEMOGLOBIN(MCH)</li> 
					<li class="testcode">RDCV -</li> <li class="testname">RED CELL DISTRIBUTION WIDTH (RDW-CV)</li> 
					<li class="testcode">RBC -</li> <li class="testname">TOTAL RBC</li> 	
					<li class="testcode">RDWSD -</li> <li class="testname">RED CELL DISTRIBUTION WIDTH - SD(RDW-SD)</li> 
					<li class="testcode">EOS -</li> <li class="testname">EOSINOPHILS</li> 
					<li class="testcode">PCV -</li> <li class="testname">HEMATOCRIT(PCV)</li> 
					<li class="testcode">PLT -</li> <li class="testname">PLATELET COUNT</li> 	
					<li class="testcode">BASO -</li> <li class="testname">BASOPHILS</li> 
					<li class="testcode">MCHC -</li> <li class="testname">MEAN CORP.HEMO.CONC(MCHC)</li> 
					<li class="testcode">AMON -</li> <li class="testname">MONOCYTES - ABSOLUTE COUNT</li> 
					<li class="testcode">ANEU -</li> <li class="testname">NEUTROPHILS - ABSOLUTE COUNT</li> 
					<li class="testcode">MCV -</li> <li class="testname">MEAN CORPUSCULAR VOLUME(MCV)</li> 
					<li class="testcode">NRBC -</li> <li class="testname">NUCLEATED RED BLOOD CELLS</li> 
					<li class="testcode">PLCR -</li> <li class="testname">PLATELET TO LARGE CELL RATIO(PLCR)</li> 
					<li class="testcode">PDW -</li> <li class="testname">PLATELET DISTRIBUTION WIDTH(PDW)</li> 
					<li class="testcode">LYMPH -</li> <li class="testname">LYMPHOCYTE PERCENTAGE</li> 
					<li class="testcode">IG% -</li> <li class="testname">IMMATURE GRANULOCYTE PERCENTAGE(IG%)</li> 
					<li class="testcode">ABAS -</li> <li class="testname">BASOPHILS - ABSOLUTE COUNT</li> 
					<li class="testcode">PCT -</li> <li class="testname">PLATELETCRIT(PCT)</li> 
					<li class="testcode">IG -</li> <li class="testname">IMMATURE GRANULOCYTES(IG)</li> 
					<li class="testcode">ALYM -</li> <li class="testname">LYMPHOCYTES - ABSOLUTE COUNT</li> 
					<li class="testcode">MPV -</li> <li class="testname">MEAN PLATELET VOLUME(MPV)</li> 
					<li class="testcode">NRBC% -</li> <li class="testname">NUCLEATED RED BLOOD CELLS %</li> 					
					<li class="testcode">MONO -</li> <li class="testname">MONOCYTES</li> 					
					<li class="testcode">HB -</li> <li class="testname">HEMOGLOBIN</li>					
					<li class="testcode">AEOS -</li> <li class="testname">EOSINOPHILS - ABSOLUTE COUNT</li> 	
					<li class="testcode">NEUT -</li> <li class="testname">NEUTROPHILS</li> 
					<li class="testcode">LEUC -</li> <li class="testname">TOTAL LEUCOCYTES COUNT</li>  
					
					
					<li class="testtitle">DIABETES</li> 
					<li class="testcode">HBA -</li> <li class="testname">HbA1c</li> 
					<li class="testcode">ABG -</li> <li class="testname">AVERAGE BLOOD GLUCOSE (ABG)</li> 

					<li class="testtitle">ELECTROLYTES</li> 
					<li class="testcode">SOD -</li> <li class="testname">SODIUM</li> 
					<li class="testcode">CHL -</li> <li class="testname">CHLORIDE</li> 
	
					<li class="testtitle">INVIVO</li> 
					<li class="testcode">ECG -</li> <li class="testname">ELECTROCARDIOGRAM</li> 
					<li class="testcode">GC -</li> <li class="testname">GYNECOLOGIST CONSULTATION</li> 
					<li class="testcode">US -</li> <li class="testname">ULTRASOUND (ABDOMEN / PELVIS)</li> 
					<li class="testcode">PAPS -</li> <li class="testname">PAP SMEAR</li> 
					<li class="testcode">XRAY -</li> <li class="testname">X-RAY (CHEST PA / LATERAL)</li> 
					<li class="testcode">TMT -</li> <li class="testname">TREADMILL TEST</li>  
					<li class="testcode">PFT -</li> <li class="testname">PULMONARY FUNCTION TEST</li> 
					
	
					<li class="testtitle">IRON DEFICIENCY</li> 					
					<li class="testcode">IRON -</li> <li class="testname">IRON</li> 
					<li class="testcode">FERR -</li> <li class="testname">FERRITIN</li> 
					<li class="testcode">TIBC -</li> <li class="testname">TOTAL IRON BINDING CAPACITY (TIBC)</li> 
					<li class="testcode">%TSA -</li> <li class="testname">% TRANSFERRIN SATURATION</li>  
	
					<li class="testtitle">LIPID</li> 
					<li class="testcode">NHDL -</li> <li class="testname">NON-HDL CHOLESTEROL</li> 
					<li class="testcode">LDL/ -</li> <li class="testname">LDL / HDL RATIO</li> 
					<li class="testcode">LDL -</li> <li class="testname">LDL CHOLESTEROL - DIRECT</li> 
					<li class="testcode">VLDL -</li> <li class="testname">VLDL CHOLESTEROL</li>  
					<li class="testcode">TC/H -</li> <li class="testname">TC/ HDL CHOLESTEROL RATIO</li>  
					<li class="testcode">CHOL -</li> <li class="testname">TOTAL CHOLESTEROL</li>  
					<li class="testcode">HCHO -</li> <li class="testname">HDL CHOLESTEROL - DIRECT</li>  
					<li class="testcode">TRIG -</li> <li class="testname">TRIGLYCERIDES</li>  
	
					<li class="testtitle">LIVER</li> 
					<li class="testcode">SGPT -</li> <li class="testname">ALANINE TRANSAMINASE (SGPT)</li> 
					<li class="testcode">GGT -</li> <li class="testname">GAMMA GLUTAMYL TRANSFERASE (GGT)</li> 
					<li class="testcode">SEGB -</li> <li class="testname">SERUM GLOBULIN</li> 
					<li class="testcode">BILT -</li> <li class="testname">BILIRUBIN – TOTAL</li>  
					<li class="testcode">PROT -</li> <li class="testname">PROTEIN – TOTAL</li>  
					<li class="testcode">A/GR -</li> <li class="testname">SERUM ALB/GLOBULIN RATIOL</li>  
					<li class="testcode">BILD -</li> <li class="testname">BILIRUBIN –DIRECT</li>  
					<li class="testcode">ALKP -</li> <li class="testname">ALKALINE PHOSPHATASE</li>
					<li class="testcode">BILI -</li> <li class="testname">BILIRUBIN (INDIRECT)</li>
					<li class="testcode">SGOT -</li> <li class="testname">ASPARTATE AMINOTRANSFERASE (SGOT )</li>
					<li class="testcode">SALB -</li> <li class="testname">ALBUMIN – SERUM</li>					
		
					<li class="testtitle">PANCREATIC</li> 
					<li class="testcode">LASE -</li> <li class="testname">LIPASE</li> 
					<li class="testcode">AMYL -</li> <li class="testname">AMYLASE</li> 
		
					<li class="testtitle">RENAL</li> 
					<li class="testcode">BUN -</li> <li class="testname">BLOOD UREA NITROGEN (BUN)</li> 
					<li class="testcode">CALC -</li> <li class="testname">CALCIUM</li> 					
					<li class="testcode">SCRE -</li> <li class="testname">CREATININE – SERUM</li> 
					<li class="testcode">URIC -</li> <li class="testname">URIC ACID</li> 
					<li class="testcode">B/CR -</li> <li class="testname">BUN / Sr.CREATININE RATIO</li>  
	
					<li class="testtitle">THYROID</li> 
					<li class="testcode">T3 -</li> <li class="testname">TOTAL TRIIODOTHYRONINE (T3)</li> 
					<li class="testcode">TSH -</li> <li class="testname">THYROID STIMULATING HORMONE (TSH)</li> 
					<li class="testcode">T4 -</li> <li class="testname">TOTAL THYROXINE (T4)</li> 
				
					<li class="testtitle">TOXIC ELEMENTS</li>  
					<li class="testcode">BTEBA -</li> <li class="testname">BARIUM</li> 
					<li class="testcode">BTECS -</li> <li class="testname">CAESIUM</li> 
					<li class="testcode">BTECO -</li> <li class="testname">COBALT</li> 
					<li class="testcode">BTEPB -</li> <li class="testname">LEAD</li> 
					<li class="testcode">BTECD -</li> <li class="testname">CADMIUM</li> 
					<li class="testcode">BTECR -</li> <li class="testname">CHROMIUM</li> 
					<li class="testcode">BTEAS -</li> <li class="testname">ARSENIC</li> 
					<li class="testcode">BTESE -</li> <li class="testname">SELENIUM</li> 
					<li class="testcode">BTEHG -</li> <li class="testname">MERCURY</li> 
	
					<li class="testtitle">VITAMIN</li>  
					<li class="testcode">FOLI -</li> <li class="testname">FOLATE</li> 
					<li class="testcode">VITB -</li> <li class="testname">VITAMIN B-12</li> 
					<li class="testcode">VITDC -</li> <li class="testname">25-OH VITAMIN D (TOTAL)</li> 

				</div>	 
					</div>					
				</div>
			</div>
		</div>
	</div> 
	</div>  
  
				
      </div>
	</div> 
 	
		<div class="container">
			<div class="row hdiw">
                     <div class="col-md-12">
					 <br><br><br><br><h2 class="text-center normal-font"><strong>How does it work ?</strong></h2><br><br></div>
					 
                     <div class="col-lg-4 col-sm-12 text-center">
                        <img src="<?php echo S3_URL?>/site/jubi-vendor/jubi-img/howitworks1.jpg" data-holder-rendered="true">
                        <div class="feature-item"> 
							<br>
                           <p class="text-muted">Select the best-customised package for you and your family</p>
                        </div>
                     </div>
					 <div class="col-lg-4 col-sm-12 text-center">
                        <img src="<?php echo S3_URL?>/site/jubi-vendor/jubi-img/howitworks2.jpg" data-holder-rendered="true">
                        <div class="feature-item"> 
							<br>
                           <p class="text-muted">Free painless blood-sample collection from your home or office</p>
                        </div>
                     </div>
                     <div class="col-lg-4 col-sm-12 text-center">
                        <img src="<?php echo S3_URL?>/site/jubi-vendor/jubi-img/howitworks3.jpg" data-holder-rendered="true">
                        <div class="feature-item"> 
							<br>
                           <p class="text-muted">Connect to expert doctors when you get your blood test reports
</p>
                        </div>
                     </div>
                  </div>
		</div>
		 
		<div class="testimonial">
			<div class="container">
				<div class="row">
					<h2 class="text-center normal-font">Some happy people</h2>
					<div class="carousel slide" data-ride="carousel" id="quote-carousel">
				<!-- Bottom Carousel Indicators -->
				<ol class="carousel-indicators">
				  <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
				  <li data-target="#quote-carousel" data-slide-to="1"></li> 
				</ol>
				<!-- Carousel Slides / Quotes -->
				<div class="carousel-inner">

				<!-- Quote 1 -->
				<div class="item active" style="width:100%">
				  <div class="row">
					<div class="col-sm-12">
					  <p style="">Right from the beginning to the end of the process was excellent. More satisfactorily was 
					  to maintain the time factor. Please carry on, good luck.</p>
					  <small><strong>- Somnath C</strong></small>
					</div>
				  </div>
				</div>

				<!-- Quote 2 -->
				<div class="item" style="width:100%">
				  <div class="row">
					<div class="col-sm-12">
					  <p style="">Thanks for the extreme care for blood collection and timely reporting the results. 
					  It is very likely it will be shared with my filial friends and family group</p>
					  <small><strong>- Madhavan R</strong></small>
					</div>
				  </div>
				</div>
 
				  
				</div>
          </div>  
                </div>		
			</div>
		</div>
		
		<div class="faq">
		<div class="container">
			<div class="row">
				<div id="accordion" role="tablist" style="width:100%">
				<h1>FAQs</h1>
				<!--<div class="card">
				<div class="card-header" role="tab" id="headingOne">
				  <h5 class="mb-0">
					<a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
					  HOW DOES THE CASH-BACK WORK ?
					</a>
				  </h5>
				</div>
				<div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
				  <div class="card-body">
					<li>Buy any health check-up package and get cash-back credited to your Jubination account. For e.g. when you buy a health check-up for Rs.2000, you get Rs.2000 credited as cash-back</li>
					<li>You can use the cash-back to get up to 20% discount in your next purchase. That means, you get your next health check-up of Rs.2000 for just Rs.1600</li>
					<li>Your cash-back amount can be used for yourself, your friends or your family members and comes with a 365 days' validity</li>
					<li>In a year, you can save upto Rs.10,000</li>
				  </div>
				</div>
			  </div>-->
				<div class="card">
    <div class="card-header" role="tab" id="headingTwo">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          IF I AM EVER STUCK, CAN I SPEAK TO A HUMAN?
        </a>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
        <li> A big yes. 365 days a year, including holidays. For a lot of our programs, we assign you a dedicated health manager who offers you resolutions to your unlimited number of queries. Treat that as a priority pass.</li>
      </div>
    </div>
  </div>
				<div class="card">
    <div class="card-header" role="tab" id="headingThree">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          WHICH CITIES DO JUBINATION COVER?
        </a>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <li>If you are from one of our top 50 cities, we’ve got you entirely covered. We are also continuously working on increasing coverage.</li>
	<li>If it's an online program, there's no geographical barrier at all. Wherever you may be, Jubination walks the extra-mile to deliver the very best to you.</li>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" role="tab" id="headingThree">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
          IS MY MONEY SAFE?
        </a>
      </h5>
    </div>
    <div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <li> Online payments are compliant with all prevailing laws and 100% safe.</li>
 <li>On top of that, we also provide a money-back assurance if anything does not live up to the set quality standards.</li>
      </div>
    </div>
  </div>
  
  <div class="card">
    <div class="card-header" role="tab" id="headingFive">
      <h5 class="mb-0">
        <a class="collapsed" data-toggle="collapse" href="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
          WHERE DO I STORE ALL MY REPORTS AND DATA?
        </a>
      </h5>
    </div>
    <div id="collapseFive" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <li>We give you a dashboard which is lifetime-free and allows you to digitize and store all your health-records. Move over paper-bills and hassles - Jubination My-page is the intelligent super-storage space that you always wanted</li>
      </div>
    </div>
  </div>
				</div>
			</div>
		</div>
	  </div>
	  
	</div>
 
    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-8 col-md-10 mx-auto"> 
            <span class="copyright text-muted">
			<h3>About Thyrocare</h3>
			<p>
Thyrocare Technologies Limited is India's first and most advanced fully Automated Laboratory having its strong presence in more than 2000 cities / towns in India and internationally with a focus on providing quality reports at affordable costs to laboratories and hospitals in India and other internationally. </p>
			
			<h3>About Jubination</h3>
			<p>We need you in this journey. Together let's raise a healthier and happier country.</p>
			</span>
          </div>
        </div>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo S3_URL?>/site/jubi-vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo S3_URL?>/site/jubi-vendor/popper/popper.min.js"></script>
    <script src="<?php echo S3_URL?>/site/jubi-vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Custom scripts for this template -->
    <!--<script src="<?php echo S3_URL?>/site/jubi-vendor/js/clean-blog.min.js"></script>-->
 
 <script>
$(document).ready(function(){
	var status = false;
	//$("#city").select2();
	var availableTags = [
		"Vishakhapatnam", "West Godavari", "Anantapur", "Kurnool", "Rajahmundry", "Warangal", "Karimnagar", "Ongole", "Nalgonda", "Chittoor", "Suryapet", "Vijayawada", "Guntur", "Adilabad", "Nellore", "Nandyal", "Quthbullapur Mandal", "Nizamabad", "Kakinada", "East Godavari", "Jagtial", "Kadapa", "Mahabubnagar", "Ranga Reddy", "Tirupathi", "Chirala", "Tadipatri", "Guntakal", "Nandigama", "Ravulapalem", "Vizianagaram", "Hindupur", "Secunderabad", "Srikakulam", "Krishna", "Prakasam", "Narasannapeta", "Madanpalle", "Tuni", "Pithapuram", "Kavali", "Rayachoti", "Visakhapatnam", "Bengaluru", "Bardhaman", "Bankura", "Nadia", "Howrah", "Midnapore", "Asansol", "Purba Medinipur", "Kandi", "Belda", "Hoogly", "South 24 Parganas", "Para", "Murshidabad", "Siliguri", "Purulia", "Durgapur", "North 24 Parganas", "Birbhum", "Sankrail", "Jagaddal", "Chinsurah", "Rampurhat", "Memari", "Medinipur", "Hooghly", "Barjora", "Tehatta", "Guma", "Patna", "Munger", "Nalanda", "Begusarai", "Purnea", "Muzaffarpur", "Bhagalpur", "Katihar", "Araria", "Saharsa", "Siwan", "Supaul", "Gaya", "Rohtas", "Chapra", "Kishanganj", "Arrah", "Samastipur", "Banka", "East Champaran", "Khagaria", "Hajipur", "Aurangabad-bihar", "Sasaram", "Vaishali", "Madhepura", "Jamui", "Gopalganj", "Surajgarha", "Madhubani", "Forbesganj", "Darbhanga", "Buxar", "Dalsingsarai", "Bettiah", "Narkatiaganj", "Navi Mumbai", "Delhi", "Jaipur", "Pune", "Raipur", "Korba", "Bilaspur", "Raigarh", "Bhilai", "Bagbahara", "Baloda Bazar", "Chennai", "South Delhi", "South West Delhi", "North West Delhi", "Ghaziabad", "East Delhi", "West Delhi", "North Delhi", "North East Delhi", "New Delhi", "Chandigarh", "Nagpur", "Coimbatore", "Lucknow", "Kochin", "Panaji", "Ahmedabad", "Kolkata", "Aurangabad", "Jodhpur", "Hyderabad", "Calangute-goa", "Indore", "Trivandrum", "Kanpur", "Nashik", "Mumbai", "Hospet", "Gurgaon", "Kumarapuram", "Khalapur", "Surat", "Vadodara", "Junagadh", "Mehsana", "Valsad", "Navsari", "Panchmahal", "Daman", "Bharuch", "Bardoli", "Bhuj", "Nadiad", "Umbergaon", "Fatehabad", "Faridabad", "Panchkula", "Ambala City", "Hisar", "Sirsa", "Kurukshetra", "Narnaul", "Karnal", "Bhiwani", "Kaithal", "Safidon", "Jind", "Spaonta Sahib", "Jhajjar", "Rampur", "Sonipat", "Yamuna Nagar", "Gohana", "Panipat", "Rewari", "Ambala", "Ambala Cantt", "Rohtak", "Vanasthaleeparam", "Jamshedpur", "Ranchi", "Dhanbad", "Bokaro", "Ramgarh", "Hazaribagh", "Jhumari Telaiya", "Giridih", "Godda", "Phusro", "Sahibganj", "Deoghar", "Dumka", "Garhwa", "Bokaro Steel City", "Hazaribag", "Udupi", "Hubli", "Bellary", "Hassan", "Belgaum", "Mangalore", "Mysore", "Davangere", "Bijapur", "Gulbarga", "Bagalkot", "Koppal", "Dakshina Kannada", "Bidar", "Gadag", "Tumkur", "Haveri", "Dharwad", "Basavakalyan", "Arsikere", "Bailhongal", "Vijayapur", "Jamkhandi", "Harihar", "Shimoga", "Thiruvananthapuram", "Kozhikode", "Thrissur", "Kannur", "Kottayam", "Alappuzha", "Kollam", "Cochin", "Kasargod", "Palakkad", "Wayanad", "Aluva", "Cherukunnu", "Rajarhat", "Bhopal", "Jabalpur", "Gwalior", "Chhatarpur", "Satna", "Ratlam", "Chhindwara", "Rewa", "Katni", "Singrauli", "Sagar", "Guna", "Anuppur", "Seoni", "Narsinghpur", "Mandla", "Hoshangabad", "Raisen", "Neemuch", "Sehore", "Vidisha", "Dewas", "Tikamgarh", "Ujjain", "Kolhapur", "Amravati", "Chandrapur", "Latur", "Dhule", "Ahmednagar", "Solapur", "Sangli", "Satara", "Gondia", "Malegaon", "Nanded", "Beed", "Raigad", "Wardha", "Parbhani", "Sindhudurg", "Balaghat", "Waidhan", "Betul", "Harda", "Jalgaon", "Baramati", "Bhandara", "Jalna", "Parli Vaijanath", "Wai", "Gadchiroli", "Talegaon", "Karad", "Ambajogai", "Koregaon", "Udgir", "Satana", "Miraj", "Yavatmal", "Darwha", "Kalyan-Dombivili", "Thane", "Ulhasnagar", "Panvel", "Mira-Bhayendar", "Vasai-Virar", "Palghar", "Nallasopara", "Noida", "Greater Noida", "Modinagar", "Export", "Kathmandu", "Bhubaneswar", "Ganjam", "Sambalpur", "Balangir", "Bargarh", "Jharsuguda", "Angul", "Kalahandi", "Balasore", "Rourkela", "Puri", "Keonjhar", "Jeypore", "Sundargarh", "Jajpur", "Bhunbaneshwar", "Cuttack", "Padampur", "Goa", "Agartala", "North Goa", "South Goa", "Ponda", "Bicholim", "Amritsar", "Pattan", "Ludhiana", "Baramulla", "Sangrur", "Faridkot", "Zirakpur", "Kurali", "Anantnag", "Hoshiarpur", "Kulgam", "Doda", "Pampore", "Fatehgarh Sahib", "Patran", "Jammu", "Barnala", "Srinagar", "Firozpur", "Patiala", "Gurudaspur", "Handwara", "Ropar", "Ganderbal", "Mohali", "Udhampur", "Pulwama", "Sopore", "Shopian", "Jalandhar", "Abohar", "Sri Muktsar Sahib", "Jalalabad", "Kathua", "Muktsar Sahib", "Hanumangarh", "Sri Ganganagar", "Udaipur", "Alwar", "Churu", "Nohar", "Kota", "Bikaner", "Nagaon", "Goalpara", "Guwahati", "Tezpur", "Sonitpur", "Karimganj", "Imphal", "Dibrugarh", "Jorhat", "Sivasagar", "Madurai", "Erode", "Tirunelveli", "Vellore", "Kumbakonam", "Theni", "Cuddalore", "Thanjavur", "Tirupur", "Tiruchirappalli", "Villupuram", "Namakkal", "Karaikudi", "Kanyakumari", "Tenkasi", "Pudukkottai", "Devakottai", "Palani", "Puducherry", "Salem", "Virudhunagar", "Panruti", "Vedaranyam", "Rasipuram", "Chengalpattu", "Tuticorin", "Udumalpet", "Nagercoil", "Mannargudi", "Sircilla", "Nakrekal", "Raikal", "Khammam", "Bhadrachalan", "Jammikunta", "Huzurabad", "Ramagundam", "Quthbullapur", "Bhongir", "Devarakonda", "Medak", "Nagapattinam", "Pattukottai", "Karaikal", "Tirupattur", "Jayankondam", "Dharapuram", "Kuttalam", "Hosur", "Gobi Chettipalayam", "Sultanpur", "Fatehpur", "Mathura", "Jhansi", "Bareilly", "Varanasi", "Ballia", "Aligarh", "Etah", "Bahraich", "Hapur", "Mahoba", "Sitapur", "Bulandshahr", "Khalilabad", "Orai", "Gyanpur", "Basti", "Jaunpur", "Gorakhpur", "Akbarpur", "Barelly", "Mau", "Farrukhabad", "Meerut", "Udham Singh Nagar", "Kashipur", "Nainital", "Haridwar", "Agra", "Allahabad", "Lakhimpur Kheri", "Rai Bareli", "Pratapgarh", "Banda", "Kanshiram Nagar", "Pilibhit", "Baghpat", "Mahamaya Nagar", "Sonebhadra", "Moradabad", "Badaun", "Amethi", "Deoria", "Calcutta", "Bangalore"
      
    ];
	
   $("#city").autocomplete({
      source: availableTags,
      select: function( event, ui ) {
      	//alert("hello");
        //$( "#project" ).val( ui.item.label );
        $("#city").val(ui.item.value);
        var city = ui.item.value;
        var city_autocomplete = $("#city").val();
       
        
      }
    }); 
});  
</script>

<script src="http://jubination.com/customized-healthcheckup/js/jquery.min.js"></script>

<script src="http://jubination.com/customized-healthcheckup/js/jquery.validate.js"></script>	
	
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<script src="<?php echo S3_URL?>/site/jubi-vendor/jquery/flatpickr.min.js"?>"></script>
<script src="<?php echo S3_URL?>/site/jubi-vendor/jquery/jubi-js.js"?>"></script>
<link href="<?php echo S3_URL?>/site/jubi-vendor/css/flatpickr.min.css" rel="stylesheet">	

<script>
$(function() {
	$(".datepicker").flatpickr({            
		minDate: new Date().fp_incr(2),
		maxDate: new Date().fp_incr(16),
	});   
});   


</script>
</body>
</html>
