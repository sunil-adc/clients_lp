<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Sheltrex Karjat | Property in Karjat | 07666767477 | Book 1 and 2 BHK Flats</title>
    <meta name="description" content="Book 1, 2 BHK Property in Karjat. Sheltrex Smart Phone City Project 1 &amp; 2. 100 acres integrated development and 24 Storey high rises towers. To know more call now 07666767477.">
    <meta name="keywords" content="Sheltrex Karjat, 7666767477, Property in Karjat, Sheltrex Smart Phone City, 1bhk in Karjat, 2bhk in Karjat, homes in karjat, Sheltrex Karjat Floorplan, Sheltrex Karjat Possessions, Sheltrex Karjat specifications, Sheltrex Karjat amenities">
    <link rel="shortcut icon" href="<?php echo S3_URL?>/site/sheltrex-assets/images/favicon.ico" type="image/x-icon" />

    <!-- CSS -->
   
    <!-- for banner css files -->
    <link rel="stylesheet" type="text/css" href="<?php echo S3_URL?>/site/sheltrex-assets/css/zoomslider.css" /><!--zoomslider css -->
    <script type="text/javascript" src="<?php echo S3_URL?>/site/sheltrex-assets/js/modernizr-2.6.2.min.js"></script><!--modernizer css -->
    <!-- //for banner css files -->
      

    <!-- custom-theme css files -->
    <link href="<?php echo S3_URL?>/site/sheltrex-assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo S3_URL?>/site/sheltrex-assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <!-- //custom-theme css files-->

    <!-- font-awesome-icons -->
    <link href="<?php echo S3_URL?>/site/sheltrex-assets/css/font-awesome.css" rel="stylesheet"> 
    <!-- //font-awesome-icons -->

    <!-- googlefonts -->
    <link href="//fonts.googleapis.com/css?family=Cabin:400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext,vietnamese" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
    <!-- //googlefonts -->
     
     
	 	<!-- Facebook Pixel Code -->
	<script>
	  !function(f,b,e,v,n,t,s)
	  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
	  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
	  n.queue=[];t=b.createElement(e);t.async=!0;
	  t.src=v;s=b.getElementsByTagName(e)[0];
	  s.parentNode.insertBefore(t,s)}(window, document,'script',
	  'https://connect.facebook.net/en_US/fbevents.js');
	  fbq('init', '1879563222101418');
	  fbq('track', 'PageView');
	  fbq('track', 'Lead');
	</script>
	<noscript><img height="1" width="1" style="display:none"
	  src="https://www.facebook.com/tr?id=1879563222101418&ev=PageView&noscript=1"
	/></noscript>
	<!-- End Facebook Pixel Code -->


  </head>
  <body>
	<!--/main-header-->
	<div id="demo-1" data-zs-src='["<?php echo S3_URL?>/site/sheltrex-assets/images/b1.jpg", "<?php echo S3_URL?>/site/sheltrex-assets/images/b2.jpg", "<?php echo S3_URL?>/site/sheltrex-assets/images/b3.jpg","<?php echo S3_URL?>/site/sheltrex-assets/images/b4.jpg"]' data-zs-overlay="dots">
		<div class="demo-inner-content">

			<!--/banner-info-->
        <div class="header"> 
          <div class="header-main">
            <div class="container">
              <nav class="navbar navbar-default">
                <div class="navbar-header">
            
                  <h1>
                    <a href="index.html">
                    <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/sheltrex-karjat-logo.png" />
                    </a>
                  </h1>
                </div>              
              </nav>
              <div class="clearfix"> </div>
            </div>
          </div>
        </div>
        <div class="w3-banner-head-info">
          <div class="container">
            <div class="banner-text">
              <h2>Houses in Karjat Pay 1%* now and no EMI</h2>
			  <h5>Till possession No Impact of GST* Affordable Home‎ 1Bhk Starts at 21 Lac*. Avail PMAY Subsidy upto 2.67 Lacs. T&C*.</h5>	
              <div class="book-form">
                <form role="form" id="feedbackForm" class="feedbackForm">
                  <label><strong>Thank you for expressing interest on our Properties
				  <br/>Our expert will get in touch with you shortly.
				  </strong></label>
				  
                  <div class="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!--/banner-info-->
      </div>
    </div>
    <!--/banner-section-->
  	<!-- about section -->
	<div class="about" id="about">
		<div class="container">
			<div class="about-top">
				<div class="col-md-4 text-center ">
					<h3>Sheltrex Smart Phone City Project 1 & 2, Karjat</h3>
				</div>
				<div class="col-md-8 text">
					<p>Sheltrex Smart Phone City Karjat 1 & 2 is a large integrated development spread across 100 acres. It offers the best of modern amenities with a breathtaking backdrop of natural greenery. Additionally, due to ease of access and connectivity and proposed infrastructure developments by Karjat Municipal Corporation(KMC), properties at Sheltrex Smart Phone City Karjat 1 & 2 are forecasted to appreciate further. The expansive green plains and the hilly terrains at Karjat also make this place a sought-after destination for trekking and tourism. Sheltrex Smart Phone City Karjat 1 & 2 lies close to the banks of the Ulhas river. You name it - tourism, adventure sports, educational institutions, film-shoot locations, retail stores, nurseries and plantations, etc., are all situated nearby.</p>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="about-bottom-grid2">
				<div class="col-md-6 bottomgridimg">
					<img src="<?php echo S3_URL?>/site/sheltrex-assets/images/espirana.jpg" alt="">
				</div>
				<div class="col-md-6 bottomgridtext bottomleft">
					<h3>ESPIRANA</h3>
					<span></span>
					<p>Espirana is designed for those who aspire to live life to its fullest. Live in smartly-designed G+23 storey towers comprising 1 & 2 BHK apartments.
            Lifestyle Amenities :- • Exclusive Club House • Fully-equipped Gymnasium • Recreational Kids Play Area • Landscaped Gardens • CCTV Survillance</p>
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="about-bottom-grid1">
				<div class="col-md-6 bottomgridtext">
					<h3>PALISO</h3>
					<span></span>
					<p>Living spaces at Paliso go beyond mere luxury. Move in to any of our 1, 2 & 3 BHK apartments and make your home your private retreat. 
            Take advantage of :- •Spacious 24-storey towers •Expansive floor space •Ultra-superior amenities •Complete privacy 
            Podium Level Amenities :- •Exclusive Club House •Fully-equipped Gymnasium •Recreational Kids Play Area •Jogging Track •Manicured Garden •CCTV Survillance •Swimming Pool </p>
				</div>
				<div class="col-md-6 bottomgridimg">
           <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/paliso.jpg" alt="">
				</div>
				<div class="clearfix"></div>
      </div>
 
      <div class="about-bottom-grid2">
				<div class="col-md-6 bottomgridimg">
        <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/nano.jpg" alt="">
				</div>
				<div class="col-md-6 bottomgridtext bottomleft">
					<h3>NANO</h3>
					<span></span>
					<p>
          Nano is built for those who prefer a lifestyle that’s minimalistic yet complete with all necessary amenities. Move into any of our G +7 Studio Apartments Amenities :- • Convenience Shopping • Exclusive Kids Play Area • CCTV Surveillance • Multi-purpose Community Hall • Gymnasium
          </p>
				</div>
				<div class="clearfix"></div>
			</div>
      
		</div>
	</div>
	<!--// about -->
<!-- services -->
<div class="services" id="services">
		<div class="banner-dott">
			<div class="heading">
				<h3>Property Near By</h3>
				<p>Here’s why Sheltrex Smart Phone City Karjat 1 & 2 are worth investing in:</p>
			</div>
			<div class="container">
				<div class="services-top-grids">
					<div class="col-md-6">
						<div class="grid1">
							<i class="fa fa-hand-o-down" aria-hidden="true"></i>
						
							<h4>Trans-Harbour Sea Link & NAINA Development</h4>
						</div>
					</div>
					<div class="col-md-6">
						<div class="grid1">
							<i class="fa fa-hand-o-down" aria-hidden="true"></i>
							
							<h4>Near the upcoming International Airport (Panvel) </h4>
						</div>
					</div>

					<div class="clearfix"></div>
				</div>
				<div class="services-bottom-grids">
        <div class="col-md-6">
						<div class="grid1">
							<i class="fa fa-hand-o-down" aria-hidden="true"></i>
							
							<h4>Near proposed 12-lane multi-modal highway project</h4>
						</div>
					</div>
					<div class="col-md-6">
						<div class="grid1">
							<i class="fa fa-hand-o-down" aria-hidden="true"></i>
							
							<h4>Close proximity to the Mumbai-Pune, Nasik & Goa Highway </h4>
						</div>
					</div>
			
			
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- //services -->
  	<!-- Testimonials -->
	<div class="customer" id="customer">
		<div class="heading">
			<h3>Book 1 BHK Starting @ 21 Lakh** onwards </h3>
			<p>- At Karjat</p>
		</div>
		<div class="container">
			<div class="customer-grids">
				<ul id="flexiselDemo1">
					<li>
			
						<div class="customer-grid">
							<h4>100-acre**</h4>
							<p>INTEGRATED DEVELOPMENT</p>
						</div>
					</li>
					<li>
		
						<div class="customer-grid">
							<h4>Designed By</h4>
							<p>HAFEEZ CONTRACTOR</p>
						</div>
					</li>
					<li>
		
						<div class="customer-grid">
							<h4>24 Storey</h4>
							<p>HIGH RISE TOWERS</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
  <!-- //Testimonials -->
  	<!-- contact -->
	<div class="mail" id="contact">
		<div class="mail-grid1">
			<div class="container">
				<div class="heading ">
					<h3>Contact us</h3>
				</div>
				<div class="col-md-6 mail-agileits-w3layouts">
					<i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>
					<div class="contact-right">
						<p>MahaRERA No</p>
            <span>P52000012196, P52000004186, P52000010525, P52000008685, P51800012307, P51800012221</span>
            <p>For more information log on to</p>
            <span><a href="https://maharera.mahaonline.gov.in">https://maharera.mahaonline.gov.in</a></span>
					</div>
				</div>
				<div class="col-md-6 mail-agileits-w3layouts">
					<i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>
					<div class="contact-right">
						<p>SALES OFFICE</p>
						<span>Sheltrex, B 12-14, 2nd Floor, Art Guild House, Phoenix Market City, Kurla West, Mumbai - 400070 
            SITE OFFICE : Sheltrex Smart Phone City 1 & 2, Shirse - Akurle, Karjat (W) - 410201 </span>
            <p>Tel</p>
            <span><a href="tel:+917666767477">+91 7666767477</a></span>
            <p>Email</p>
            <span><a href="mailto:info@sheltrex.com">info@sheltrex.com</a></span>
            <p>Website</p>
            <span><a href="www.sheltrex.com">www.sheltrex.com</a></span>
          </div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- //contact -->
	<div id="pi">
       <?php
	   $vr = $this->session->userdata('user_id');
	   $ut = $this->session->userdata('utm_source');
	   if( isset($vr) &&  $vr > 0 && $ut != ""){
		   echo $this->lead_check->set_pixel($vr, SHELTERX_USER);
		   
	   }
	   ?>
      </div>

      
	
	<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="agileits-w3layouts-copyright">
				<p>© 2018  All rights reserved 
				</p>
			</div>
		</div>
	</div>
	<!-- //footer -->
	 <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />

    <!-- JavaScript -->
    <!-- js -->
    <script type="text/javascript" src="<?php echo S3_URL?>/site/sheltrex-assets/js/jquery-2.1.4.min.js"></script>
    <!-- for bootstrap working -->
      <script src="<?php echo S3_URL?>/site/sheltrex-assets/js/bootstrap.js"></script>
    <!-- //for bootstrap working -->
    <!-- //js -->
      

    <!-- for banner js file-->
    <script type="text/javascript" src="<?php echo S3_URL?>/site/sheltrex-assets/js/jquery.zoomslider.min.js"></script><!-- zoomslider js -->
    <!-- //for banner js file-->

    <!-- for team slider -->

    <!-- //for team slider -->



    <!-- for smooth scrolling -->
    <script src="<?php echo S3_URL?>/site/sheltrex-assets/js/SmoothScroll.min.js"></script>
      <script type="text/javascript" src="<?php echo S3_URL?>/site/sheltrex-assets/js/move-top.js"></script>
      <script type="text/javascript" src="<?php echo S3_URL?>/site/sheltrex-assets/js/easing.js"></script>
      <!-- here stars scrolling icon -->
      <script type="text/javascript">
        $(document).ready(function() {
          /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear' 
            };
          */
                    
          $().UItoTop({ easingType: 'easeOutQuart' });
                    
          });
      </script>
      <!-- //here ends scrolling icon -->
    <!-- //for smooth scrolling -->

    <!-- scrolling script -->
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        $(".scroll").click(function(event){		
          event.preventDefault();
          $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
      });
    </script> 
    <!-- //scrolling script -->

    <!-- testimonilas slider -->
            <script type="text/javascript">
                  $(window).load(function() {
                    $("#flexiselDemo1").flexisel({
                      visibleItems: 3,
                      animationSpeed: 1000,
                      autoPlay: false,
                      autoPlaySpeed: 3000,    		
                      pauseOnHover: true,
                      enableResponsiveBreakpoints: true,
                      responsiveBreakpoints: { 
                        portrait: { 
                          changePoint:480,
                          visibleItems: 1
                        }, 
                        landscape: { 
                          changePoint:640,
                          visibleItems:3
                        },
                        tablet: { 
                          changePoint:768,
                          visibleItems: 3
                        }
                      }
                    });
                    
                  });
              </script>
    <script type="text/javascript" src="<?php echo S3_URL?>/site/sheltrex-assets/js/jquery.flexisel.js"></script>
    <!-- testimonilas slider -->
    <script src="<?php echo S3_URL?>/site/sheltrex-assets/js/vendor.js"></script>
    <script src="<?php echo S3_URL?>/site/sheltrex-assets/js/main.js"></script>
	<script src="<?php echo S3_URL?>/site/scripts/default.js"></script> 
  </body>
</html>