
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Lenskart</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?php echo S3_URL?>/site/lenskart/img/favicon.webp">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo S3_URL?>/site/lenskart/styles.css">
</head>

<body>

  <div class="container-fluid paddingZero">

    <div class="container widthOne firstContainer">
      <header>
        <img src="<?php echo S3_URL?>/site/lenskart/img/logo.png" alt="" class="leftLogo">
        <img src="<?php echo S3_URL?>/site/lenskart/img/right-header.png" alt="" class="rightLogo">
      </header>
    </div>

    <section class="headerContent">
      <p class="fistPara">12-Step Eye Test & Try 100+ best selling frames | Book online | </p>
      <p>Currently available for the Age Group 12 to 65 years</p>

    </section>

    <div class="container widthTwo secondContainer">

      <div class="col-md-6 col-sm-12 leftImageSection">

        <img src="<?php echo S3_URL?>/site/lenskart/img/leftImg.png" alt="">

      </div>

      <div class="col-md-4 col-sm-12 formSection paddingZero">

        <form action="javascript:void(0)" method="post" onsubmit="lenskart_form()" id="lenskart_form">

          <h3>
            Personal Optician at the
            Comfort of your Home
          </h3>
          <h5><b style="background: #0defda;padding: 3px 5px;">Just @ Rs.130</b></h5>
          <p style="text-align: center;"><span class="formError" id="lenskart_err">  </span></p>
          <div class="input-group selectCity">
            <span class="input-group-addon addressIconInput"><i class="fa fa-map-marker" aria-hidden="true"></i></span>
            <select name="" id="city" name="city" data-attr="Please select city">
              <option value="">Select City</option>
              <option value="chennai">Chennai</option>
              <option value="delhi">Delhi</option>
              <option value="bangalore">Bangalore</option>
              <option value="mumbai">Mumbai</option>
              <option value="kolkata">Kolkata</option>
              <option value="hyderabad">Hyderabad</option>
              <option value="pune">Pune</option>
              <option value="ahmedabad">Ahmedabad</option>
              <option value="others">Others</option>
            </select>
          </div> 


          <div class="input-group">
            <span class="input-group-addon mobileIconInput"><i class="fa fa-mobile" aria-hidden="true"></i></span>
            <input type="text" class="form-control mobileInput" id="mobile" placeholder="Mobile Number" name="mobile" minlength="10" maxlength="10" pattern="\d*" 
            data-attr="Please enter correct mobile number">
          </div>

          <input type="submit" class="btn btn-primary" value="Book Appointment" id="frm-sbmtbtn">

         </form>


          <div class="otp-section" id ="otp_section" style="display: none;">

            <form  action="JavaScript:void(0)" onsubmit="lenskart_otp()" id="otp_form">

              <input type="hidden"  id="otp_mobile" name="otp_mobile">
              <div class="input-group otpInputContain">
                <span class="input-group-addon mobileIconInput"><i class="fa fa-key" aria-hidden="true"></i></span>
                
                <input type="text" class="form-control otpInput" id="otp" name="otp" placeholder="OTP" minlength="4" maxlength="4" pattern="\d*" 
                     data-attr="Please enter correct otp">
                <span class="help-block" id="otp_err"></span> 
              </div>
              <div class="submitbtncontainer">
                  <input class="submitOtp" type="submit" id="otpfrm-sbmtbtn" value="Enter OTP" name="submit">
              </div>

              <div class="clearfix"></div>

              <p style="text-align: left;"><span class="formError"> Enter Correct OTP </span></p>
            
            </form>

            <div class="clearfix"></div>

            <form  action="JavaScript:void(0)" onsubmit="lenskart_otp_resend('<?php echo SITE_URL?>lenskart/lnkkrt_reset')">
              <input type="hidden"  id="otp_mobile_res" name="otp_mobile_res">
              <p class="disclaimer">OTP Received to the Entered Mobile Number If Not Received 
                <input class="resendOtp" type="submit" id="res-sbmtbtn"  value="Click-Here"></p>
            </form>

        </div>

        <p class="cityHead"> <b>Currently Available in -</b> </p>

        <p class="cities">Delhi, Bangalore, Mumbai, Kolkata, Hyderabad, Chennai, Pune and  Ahmedabad.</p>

      </div>

    </div>
    <input type="hidden" name="su" id="su" value="<?php echo SITE_URL?>">
    <input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : "d_sr"); ?>" >
    <input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "d_ut"); ?>" >
    <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : "d_ut"); ?>" >
   

    <footer class="pageFooter">
      <img src="<?php echo S3_URL?>/site/lenskart/img/footer_new_img.png" alt="">
    </footer>

  </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="<?php echo S3_URL?>/site/lenskart/script.js"></script>

</body>

</html>