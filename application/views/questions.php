<?php  $id = $this->session->userdata('user_id'); 
echo $id;
if($id==''){
    header( "Location: ".base_url()."index.php/Handdybook/instantsavebills");
} ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Instant Save Bills</title>

    <link rel="icon" href="">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/handdybook_assets/css/style.css">
</head>
<style>
    .error_class{
        color: #e00e0e;
    }
</style>
<body><?php 
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $url_components = parse_url($actual_link); 
        if(isset($url_components['query'])){
            parse_str($url_components['query'], $params); 
        }
        //echo $params['id']; exit;
        ?>
   <section id="questions">
       <div class="container">
            <div class="col-md-12 mobilePaddingZero">
                <nav class="navbar ">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="<?php echo base_url()."index.php/Handdybook/";?>"><img src="<?php echo S3_URL?>/site/handdybook_assets/images/Logo.png" alt="" srcset=""></a>
                        </div>
                    </div>
                </nav>

                <div class="col-md-12 mobilePaddingZero">
                    <form id="question_form" method="POST">
                        <?php if(isset($params['utm_source'])){ ?>
                        <input type="hidden" id="utm_source" name="utm_source" value="<?php echo $params['utm_source']; ?>">
                        <?php } 
                        if(isset($params['utm_medium'])){ 
                        ?>
                        <input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo $params['utm_medium']; ?>">
                        <?php } 
                        if(isset($params['utm_sub'])){ 
                        ?>
                        <input type="hidden" id="utm_sub" name="utm_sub" value="<?php echo $params['utm_sub']; ?>">
                        <?php } 
                        if(isset($params['utm_campaign'])){ 
                        ?>
                        <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo $params['utm_campaign']; ?>">
                        <?php } ?>
                        <div class="form-group">
                            <div class="col-sm-6 questionInputSec">
                                <label for="food">1. How much do you spend on food every month?</label>
                                <input type="text" class="form-control" id="value_q1" name="value_q1">
                                <div id="value_q1-error" class="error_class"></div>
                            </div>

                            <div class="col-sm-6 questionInputSec">
                                <label for="email">2. What is your average shopping expense per month?</label>
                                <input type="text" class="form-control" id="value_q2" name="value_q2">
                                <div id="value_q2-error" class="error_class"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6 questionInputSec">
                                <label for="email">3. How much do you spend on travel every month?</label>
                                <input type="text" class="form-control" id="value_q3" name="value_q3">
                                <div id="value_q3-error" class="error_class"></div>
                            </div>

                            <div class="col-sm-6 questionInputSec">
                                <label for="email">4. How much is your monthly electricity bill?</label>
                                <input type="text" class="form-control" id="value_q4" name="value_q4">
                                <div id="value_q4-error" class="error_class"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-6 questionInputSec ">
                                <label for="email">5. What is your monthly medical expense?</label>
                                <input type="text" class="form-control" id="value_q5" name="value_q5">
                                <div id="value_q5-error" class="error_class"></div>
                            </div>
                            <div class="col-sm-6 questionInputSec "></div>                            
                        </div>

                        <div class="col-sm-12 marginTop3em">
                            <button class="btn btn-secondary questionButton" type="submit"> Submit Now </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
   </section>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="<?php echo S3_URL?>/site/handdybook_assets/js/script.js"></script>

<script> 

    $(document).ready(function () {
        $('input[type=text]').val('');
    });

    $(document).ready(function(){
        //submit login form
        $('#question_form').submit(function(event){
            event.preventDefault();

            //get the input data
            input = $('#question_form').serialize();
            base_url = '<?php echo base_url();?>';
            $('#join_form .error_class').empty();
            //submit
            $.ajax({
                type: 'POST',
                url: base_url+"/index.php/Handdybook/submit_question_form",
                data: input,
                success : function(response){ 
                    if(response.status == false){
                        $.each(response.data['error'], function(key, value){
                            $('#question_form #'+key+'-error').html(value);
                        });
                    }else{
                        window.location.href = "thank_you";
                    }
                } 
            });
        });
    });
</script>
</html>