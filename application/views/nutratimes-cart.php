<?php $all_array = all_arrays(); ?>
<!DOCTYPE html>
<html>

<head>
    <title>Adcanopus</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/nutratimes-quiz-assets/lib/bootstrap-4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/nutratimes-quiz-assets/lib/rangeslider.js-2.3.0/rangeslider.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU"
        crossorigin="anonymous">
    <!--Custom Css-->
    <link rel="stylesheet" type="text/css" href="<?php echo S3_URL?>/site/nutratimes-quiz-assets/css/style.css">

</head>

<body>
    <div class="cart-page">
        <header>
            <div class="container">
                <div class="logo">
                    <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/nutratimes_logo.png" alt="nutratimes_logo" title="Nutratimes" />
                </div>
                <nav class="navbar">
                    <ul class="nav-list">
                        <!--<li class="nav-list_item">
                            <a href="">Real Stories</a>
                        </li>
                        <li class="nav-list_item">
                            <a href="">Real Stories</a>
                        </li>
                        <li class="nav-list_item">
                            <a href="">Real Stories</a>
                        </li>
                        <li class="nav-list_item">
                            <a href="">Real Stories</a>
                        </li>-->
                    </ul>
                </nav>
            </div>
        </header>
        <form action="javasciipt:void(0)" onsubmit="orderfrm('<?php echo SITE_URL?>nutratimes/confirm_order')">
        <section class="cart-block">
            <div class="shipping-address-block">
                <div class="mobile-container">
                    <div class="block-title" >
                        <h5>Shipping Address</h5>
                        <button data-toggle="collapse" data-target="#cartForm"  aria-expanded="true" type="button" tabindex="0" class="mobile-only">
                            <i class="fa fa-angle-down rotate-icon" aria-hidden="true"></i>
                        </button>
                    </div>
                    <?php if(isset($user_details) && $user_details != ""){
                        
                        foreach ($user_details as $key ) {
                             //print_r($key->name);exit;
                       ?>
                     
                    <div class="form-details collapse show" id="cartForm">
                        <div class="group">
                            <div class="form-group single-coloumn">
                                <label>First Name</label>
                                <input  type="text" class="form-control" name ="name" id="name" value="<?php echo($key->name); ?>" data-attr="Please enter correct name" disabled="true" >
                                 <span class="help-block" id="name_err"></span>
                            </div>
                            <!--<div class="form-group two-coloumn">
                                <label>Last Name</label>
                                <input type="text" class="form-control">
                            </div>-->
                        </div>
                        <div class="group">
                            <div class="form-group two-coloumn">
                                <label>Mobile Number</label>
                                <input disabled="true" type="text" class="form-control" value="<?php echo($key->phone); ?>" name="phone" id="phone" data-attr="Please enter correct phone number">
                                <span class="help-block" id="phone_err"></span>
                            </div>
                            <div class="form-group two-coloumn">
                                <label>Email Address</label>
                                <input type="email" class="form-control" value="<?php echo($key->email); ?>" name="email" id="email" data-attr="Please enter correct email" disabled="true">
                                <span class="help-block" id="email_err"></span>
                            </div>
                        </div>
                        <div class="group">
                            <div class="form-group single-coloumn">
                                <label>Address<span class="color-red">*</span></label>
                                <textarea tabindex="1" class="form-control" id="address" name="address" data-attr="Please enter correct address"></textarea>
                                <span class="help-block" id="address_err"></span>
                            </div>
                        </div>
                        <div class="group">
                            <div class="form-group three-coloumn">
                                <label>Postal Code/Zip<span class="color-red">*</span></label>
                                <input tabindex="2" type="text" class="form-control" id="pincode" name="pincode" data-attr="Please enter correct pincode" maxlength="6">
                                <span class="help-block" id="pincode_err"></span>
                            </div>
                            <div class="form-group three-coloumn">
                                <label>City<span class="color-red">*</span></label>
                                <input tabindex="3" type="text" class="form-control" name="city" id="city" data-attr="Please enter correct city">
                                <span class="help-block" id="city_err"></span>
                            </div>
                            <div class="form-group three-coloumn">
                                <label>State<span class="color-red">*</span></label>
                                 <select tabindex="4" name="state" id="state" data-attr="Please enter correct state" class="form-control" data-attr="Please enter correct state">
                                                    <option value="">Select State</option>
                                                    <?php 
                                                    foreach ($all_array['NUTRATIMES_STATE_ARR'] as $k => $v) {
                                                        
                                                        echo "<option value='".$k."' ".(($k == $key->state) ? 'selected' : '').">".$v."</option>";
                                                    }
                                ?>
                                                    </select>
                                                    <span class="help-block" id="state_err"></span>
                            </div>
                        
                    
                <?php }
            }
            ?>
                          
                        </div>
                    </div>
                </div>
            </div>
            <?php if(isset($product_detail) && $product_detail!=""){
           
                
             ?>
            <div class="cart-details-block">
                <div class="mobile-container">
                    <div class="block-title">
                        <h5>Cart</h5>
                    </div>
                    <div class="cart-product-block">
                        <div class="image-block">
                            <img src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/images/pack-1.png" alt="nutratimes_logo" title="Nutratimes" />
                        </div>
                        <div class="product-name">
                             <input type="hidden" name="product_id" id="product_id" value="<?php echo $product_detail['quantity'] ?>" />
                            <h5>Garcinia Cambogia Herbs - <?php echo $product_detail['quantity'] ?> Bottle</h5>
                        </div>
                    </div>
                    <div class="cart-calculation-block">
                      <!--  <div class="flex-box">
                            <span class="box-label">Quantity</span>
                            <span class="box-value">
                                <span class="qty-block">
                                    <button>-</button>
                                    <input type="number" value="1">
                                    <button>+</button>
                                </span>
                            </span>
                        </div>-->
                        <div class="flex-box">
                            <span class="box-label">Total</span>
                            <span class="box-value">Rs.<?php echo $product_detail['amount'] ?></span>
                        </div>
                        <div class="flex-box">
                            <span class="box-label">Discount <?php echo $product_detail['discount'] ?>(-)</span>
                            <span class="box-value">Rs.<?php echo $product_detail['discount_amount'] ?></span>
                        </div>
                        <!--<div class="flex-box">
                            <span class="box-label">Subtotal</span>
                            <span class="box-value">Rs.2288</span>
                        </div>-->
                        <hr>
                        <div class="flex-box">
                            <span class="box-label">Grand Total</span>
                            <span class="box-value">Rs.<?php echo $product_detail['total_amount'] ?></span>
                        </div>
                    </div>
                    <div class="options-box custom-checkbox">
                        <div class="form-group">
                            <input type="checkbox" id="cod" checked disabled="true">
                            <label for="cod"><span>Cash On Delivery (24 Hours Delivery)</span></label>
                        </div>
                        <div class="form-group">
                            <input type="checkbox" id="tc" checked disabled="true">
                            <label for="tc"><span>I have read and accept the <a href="http://www.nutratimes.com/terms-and-conditions" target="_blank">Terms & Conditions</a>*</span></label>
                        </div>
                    </div>
                    <div class="button-block">
                        <button tabindex="5" class="btn btn-green">Proceed</button>
                    </div>
                </div> 
            </div>
        <?php 
    }?>

        </section>
        </form>
    </div>

  <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
    <!-- jQuery 3 -->
    <script src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/lib/jquery/jquery-3.3.1.min.js"></script>
    <!-- Range Slider -->
    <script src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/lib/rangeslider.js-2.3.0/rangeslider.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/lib/bootstrap-4.0.0/js/bootstrap.min.js"></script>

    <!-- Customjs -->
    <script src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/js/vendor.js"></script>
    <script src="<?php echo S3_URL?>/site/nutratimes-quiz-assets/js/custom.js"></script>
</body>

</html>