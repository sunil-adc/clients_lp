<?php $all_array = all_arrays(); 

?>
<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Get In Touch |  NMIMS Distance Education</title>      
<link rel="shortcut icon" href="<?php echo S3_URL?>/site/nmims-marketing-assets/images/favicon.ico" type="image/x-icon" />


<link rel="stylesheet" href="<?php echo S3_URL?>/site/nmims-marketing-assets/css/vendor.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel="stylesheet" href="<?php echo S3_URL?>/site/nmims-marketing-assets/css/slick.css">    
<link href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.css" rel="stylesheet" />
<link rel="stylesheet" href="<?php echo S3_URL?>/site/nmims-marketing-assets/css/main.css">    
<!-- fonts -->
<link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

<!-- DO NOT MODIFY -->
<!-- Quora Pixel Code (JS Helper) -->
<script>
!function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
qp('init', '0250ea612ac748c6b5516e1e84caa786');
qp('track', 'ViewContent');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/0250ea612ac748c6b5516e1e84caa786/pixel?tag=ViewContent&noscript=1"/></noscript>
<!-- End of Quora Pixel Code --> 
<script>qp('track', 'GenerateLead');</script>


   </head>
   <body> 
	
   <header class="">
         <div class="logo">
            <a href="JavaScript:Void(0);" target=""><img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/p-logo.png"></a>
         </div>
         <div class="clearB"></div>
	  </header>
	  
   <?php $all_array = all_arrays(); ?>   
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="">
					<div class="carousel-caption">

                    </div>
				</div>
			</div>
		</div>

		<!-- The Modal -->
	</div>
    <!--//banner -->    
    
    <section>
        <div class="PDIDM-section" id="Program">
            <div class="pdidm-block">
                <div class="L-pdidm-block">
                    <div class="pdidm-bg">
                        <img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/pdidm-bg.png">
                    </div>
                    <h2 class="heading-01">Professional Diploma in <span class="red-color db-text">Digital Marketing</span></h2>
                    
                    <p>
                        As internet becomes a seamless part of the day &amp; life of a prime population the bar for marketing touchpoints is constantly shifting and evolving. Consumers, empowered by information, are demanding unique-value-attributed communication across platforms &amp; mediums.</p>
                        <p>Few business functions have been as disrupted by this digitalisation as marketing. In order to keep digital marketer at the forefront of these changes, NMIMS has designed the Professional Diploma in Digital Marketing - to develop expertise in every facet of digital marketing, from strategy and organizational design to customer impact and scalable execution.  
                    </p>
                </div>
                <div class="R-pdidm-block">
                    <img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/pdidm-img.png">
                </div>
                <i class="clearB"></i>
            </div>
        </div>
    </section>
    


    <section class="section02">
        <div class="market_strategies_section">			
            <div class="gm-section">
                <div class="gm-mid-block wow zoomIn">
                    <div class="midd-img-sec">
                        <img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/growth_journey_img.png">
                        <div class="ygj_title"><h3 class="heading-01">Your <span class="red-color">Growth Journey</span></h3></div>
                    </div>
                </div>
                <div class="gm-left-block">
                    <ul>
                        <li class="wow zoomIn">
                            <div class="ygj_circle">
                                <div class="inn_circle">
                                    <p>Develop the ability to device
                                        <span class="red-color">content strategies</span> across all
                                        digital channels.</p> 
                                </div>
                            </div>
                        </li>
                        <li class="wow zoomIn">
                            <div class="ygj_circle">
                                <div class="inn_circle">
                                    <p>Build effective <span class="red-color">strategies</span>
                                    across <span class="red-color">Social Media</span> platforms
                                    (Facebook, Twitter, Instagram and
                                    LinkedIn) to engage
                                    new &amp; existing audiences whilst
                                    achieving business goals.</p>
                                </div>
                            </div>
                        </li>
                        <li class="wow zoomIn">
                            <div class="ygj_circle">
                                <div class="inn_circle">										
                                    <p>Leverage 1st and 3rd party
                                        data &amp; <span class="red-color">use analytic tools</span> to meet
                                        marketing &amp; business goals.</p>
                                </div>
                            </div>
                        </li>
                        <li class="wow zoomIn">
                            <div class="ygj_circle" style="width: 300px;">
                                <div class="inn_circle">
                                    <p><span class="red-color">Create Marketing Strategies</span> including defining goals, planning,
                                    analysing, developing, implementing and evaluating plans to
                                    achieve business goals</p>
                                </div>
                            </div>
                        </li>
                        <li class="wow zoomIn"> 
                            <div class="ygj_circle">
                                <div class="inn_circle">
                                    <p>Capability to <span class="red-color">optimise Search Advertising</span>
                                    campaigns, with a special focus on Google Adwords
                                    for end to end campaign management.</p>
                                </div>
                            </div>
                        </li>
                        <li class="wow zoomIn">
                            <div class="ygj_circle">
                                <div class="inn_circle">
                                    <p>Create strategy for <span class="red-color">Omni channel</span>
                                        marketing and integration with
                                        CRM and email tools.</p>
                                </div>
                            </div>
                        </li>
                        <li class="wow zoomIn">
                            <div class="ygj_circle">
                                <div class="inn_circle">
                                    <p>Implement new media buying methods
                                    like <span class="red-color">Programmatic Ad Buying,</span>
                                    Real Time Bidding etc.</p>
                                </div>
                            </div>
                        </li>
                        <i class="clearB"></i>
                    </ul>
                    <i class="clearB"></i>
                </div>
            </div>
            <div class="ygj-bg">
                <img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/growth_bg.png">
            </div>
        </div>
    </section>


    <section class="section03">
        <div class="feature_section">
            <div class="feature_block">
                <div class="feature_img">
                    <img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/feature_bg.png">
                    <div class="feature_title">
                        <img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/feature_img.png">
                        <h4>Features</h4>
                    </div>
                </div>	
                <div class="feature_information">
                    <ul>
                        <li>
                        <div class="feature_box fb_L sm-feature_block L1">
                            <div class="featurebox_img" style="float:right"><img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/unique_approch.png"></div>
                            <div class="feature_text a-right">
                                The only Program of its kind in the country with a <span class="red-color">Unique '6-C Approach'</span>
                            </div>
                            
                        </div>
                        </li>
                        
                        <li>
                        <div class="feature_box fb_R sm-feature_block R1">
                            <div class="featurebox_img"><img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/expert_industries_icon.png"></div>
                            <div class="feature_text a-left">
                                Guest sessions from <div class="red-color">expert Industry practitioners</div> 									
                            </div>								
                        </div>
                        </li>	
                        <i class="clearB"></i>
                    </ul>
                    <ul>
                        <li>
                        <div class="feature_box fb_L L2">
                            <div class="featurebox_img" style="float:right"><img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/partnership_icon.png"></div>
                            <div class="feature_text text a-right">
                                Courseware designed in <div class="red-color">partnership with Stukent©</div> global content partners for leading Universities providing a Digital Marketing program															
                            </div>								
                        </div>
                        </li>
                        <li>
                            <div class="feature_box fb_R R2">
                                <div class="featurebox_img"><img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/program_designed_icon.png"></div>
                                <div class="feature_text a-left">
                                    Program <div class="red-color">designed by</div> an eminent jury of <div class="red-color">academicians</div> and <div class="red-color">industry experts</div> to ensure that the most relevant &amp; up-to-date topics get covered 
                                </div>									
                            </div>								
                        </li>
                        <i class="clearB"></i>
                    </ul>
                    <ul>
                        <li>
                            <div class="feature_box fb_L sm-feature_block L3">
                                <div class="featurebox_img" style="float:right"><img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/live_lecture_icon.png"></div>
                                <div class="feature_text text a-right sm-feature_block">										
                                    <div class="red-color">Live Lectures</div> conducted by eminent faculty comprising a mix of Leading Academicians and Industry Leaders
                                </div>									
                            </div>								
                        </li>
                        <li>
                            <div class="feature_box fb_R sm-feature_block R3">
                                <div class="featurebox_img"><img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/hand_on_learning_icon.png"></div>
                                <div class="feature_text a-left">
                                    <div class="red-color">Hands-on learning</div> of all concepts using 'Real World' simulations
                                </div>									
                            </div>
                            
                        </li>
                    <i class="clearB"></i>
                    </ul>
                </div>					
            </div>
            <i class="clearB"></i>				
        </div>
        <div class="feature_icon-bg">
            <img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/feature_icon_bg.png">
        </div>
    </section>



    <section>
        <div class="programme_structure_section" id="Specialisations">
            <div class="container">					
                <div class="specialisation_block">
                    <h2 class="heading-01 mobileTab">Program Structure</h2>
                    <div class="programme_section">
                        <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                            <ul class="resp-tabs-list">
                                <li class="resp-tab-item1 resp-tab-active" aria-controls="tab_item-0" role="tab">Semester 1</li>
                                <li class="resp-tab-item2" aria-controls="tab_item-1" role="tab">Semester 2</li>
                            </ul>
                            <div class="resp-tabs-container">
                                <h2 class="resp-accordion resp-accordion1 resp-tab-active" role="tab" aria-controls="tab_item-0"><span class="resp-arrow"></span>Core Specialisation</h2><div class="resp-tab-content resp-tab-content1 resp-tab-content-active mobileTab" aria-labelledby="tab_item-0">
                                    <div class="sem01-T">
                                    <li>
										<h4 class="semester-heading">Module 1</h4>
										<!--<p>Cultivate: Revisiting Marketing concepts & Introduction to Online Media</p>-->
										<ul class="semlist">
											<li>Fundamentals of Marketing including Environment Analysis, Segmenting-Targeting- Positioning, the Marketing Mix.</li>
											<li>Basics of Consumer Behaviour</li>
											<li>Market Research Basics</li>
											<li>The Digital Marketing Landscape</li>
										</ul>
									</li>
									<li>
										<h4 class="semester-heading">Module 2</h4>
										<!--<p>Catch: Marketing Framework for the connected consumer</p>-->
										<ul class="semlist">
											<li>Online Consumer Decision Journey</li>
											<li>Paid-Owned-Earned Media (POEM) Framework</li>
											<li>Building Digital Assets - Website, Landing Pages, Mobile Site, Mobile App</li>
											<li>Creating pages on Social Media channels - Facebook, Twitter, LinkedIn etc.</li>
											<li>Introduction to Search Engine Optimization (SEO)</li>
										</ul>
									</li>
                                        <i class="clearB"></i>
                                    </div>

                                    <div class="sem01-B">
                                        <li>
                                            <h4 class="semester-heading">Module 3</h4>
                                            <!--<p>Connect: Marketing to the connected consumer</p>-->
                                            <ul class="semlist">
                                                <li>Introduction and evolution of Paid Media</li>
                                                <li>Content Marketing Strategy - Creating content across different touchpoints</li>
                                                <li>Primer on Search Engine Marketing (SEM)</li>
                                                <li>Introduction to Display Marketing – Google Display Networks (GDN) etc.</li>
                                                <li>Basics of Mobile Marketing including understanding of the Mobile App ecosystem</li>
                                                <li>Programmatic Buying – Evolution, 4 pillars of Programmatic and how to use Data effectively for Programmatic.</li>
                                            </ul>
                                        </li>
                                        <li>										
                                            <h4 class="semester-heading">Simulation</h4>
                                            <p style="color: #fff; text-align: left;">Mimic Pro</p>
                                            <span><img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/mimic-pro.png" alt=""></span>
                                        </li>
                                    </div>
                                    <i class="clearB"></i>
                                </div>
                                <h2 class="resp-accordion resp-accordion2" role="tab" aria-controls="tab_item-1"><span class="resp-arrow"></span>Dual Specialisations</h2><div class="resp-tab-content resp-tab-content2" aria-labelledby="tab_item-1">
                                    <div class="sem01-T sem02-T">
                                        <li>
                                            <h4 class="semester-heading">Module 4</h4>
                                            <!--<p>Consume: Conversations with Content</p>-->
                                            <ul class="semlist">
                                                <li>Content Distribution strategy – distributing content across major social media channels</li>
                                                <li>Introduction to Online Videos Platforms with a special focus on advertising on Youtube</li>
                                                <li>The different online video advertising platforms</li>
                                                <li>Introduction and basics of Affiliate Marketing</li>
                                                <li>Key factors for successful Affiliate Marketing</li>
                                                <li>Introduction and essentials of Native Advertising</li>
                                                <li>Deep diving into Search Engine Optimization (SEO) – Keywords as building blocks, Types of SEO, measuring success</li>
                                                <li>Deep diving into Search Engine Marketing (SEM) – Definitions, Keywords, Quality Score, Account structures, measuring success etc.</li>
                                            </ul>
                                        </li>
                                        <li>
                                            <h4 class="semester-heading">Module 5</h4>
                                            <!--<p>Close: Optimizing for Conversions</p>-->
                                            <ul class="semlist">
                                                <li>Engaging audiences with different Remarketing Strategies</li>
                                                <li>Conversion Rate Optimization (A/B Testing)</li>
                                                <li>Attribution Modelling</li>
                                                <li>Marketing analytics with a special focus on Google Analytics</li>
                                            </ul>
                                        </li>
                                        <i class="clearB"></i>							
                                    </div>

                                    <div class="sem01-B sem02-T">
                                        <li>
                                            <h4 class="semester-heading">Module 6</h4>
                                            <!--<p>Continue: Building loyal consumers and driving brand advocacy</p>-->
                                            <ul class="semlist">
                                                <li>Marketing Automation - The art and science of engaging website visitors</li>
                                                <li>Influencer Marketing – Introduction, Growth and Evolution</li>
                                                <li>Online Reputation Management (ORM)</li>
                                                <li>Primer on Emerging Trends in Digital Marketing - Chatbot, Digital Wallets, VR / AR</li>
                                            </ul>
                                        </li>
                                        <li>										
                                            <h4 class="semester-heading">Simulation</h4>
                                            <p style="color: #fff; text-align: left;">Mimic Social</p>
                                            <span><img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/mimic-social.png" alt=""></span>
                                        </li>
                                    </div>
                                    <i class="clearB"></i>
                                </div>
                            </div>
                        </div>
                        <i class="clearB"></i>
                    </div>
                </div><!--Specialization-->
                
            </div>				
        </div><!--Programme Structure-->
    </section>


	<section class="section04 webfaculty">			
			<div class="faculty_structure" id="Faculty">
				<div class="container">
					<div class="faculty_block">
						<h2 class="heading-01">Faculty</h2>
						<p>Leading business thinkers from academia and industry, to give you dynamic mix of theory and business practices*</p>
						<div style="margin-top: 50px;">
						<ul class="bxslider">
							<li>
								<div class="faculty-info">
									<div class="fc_block">
										
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/amol_patkar.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="faculty_info">
										<h1>Amol Patkar</h1>
										<p>B.E. (Delhi College of Engineering), PGDM (IIM - Calcutta) Founder & Director, Fundamentor.com.</p>
									</div>
								</div>


								<div class="faculty-info">
									<div class="fc_block">
										
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/debabrata_nag.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="faculty_info">
										<h1>Debabrata Nag</h1>
										<p>B.E. Honours ( Jadavpur University), PGDM (IIM - Ahmedabad) GM, Marketing, Reliance Jio &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp; &nbsp;  &nbsp;  &nbsp;  &nbsp; </p>
									</div>
								</div>

								<div class="faculty-info">
									<div class="fc_block">
										
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/siva_kumar.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="faculty_info">
										<h1>Dr. Siva M. Kumar</h1>
										<p>Founder and Director at Augentia LLC Fellow Program in Management (IIM-Indore), MS (BITS Pilani)</p>
									</div>
								 </div>


							</li>

							<li>
								<div class="faculty-info">
									<div class="fc_block">
										
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/sanjay_verma.png">
									    </div>										
										<i class="clearB"></i>
									</div>
									<div class="faculty_info">
										<h1>Sanjay Varma</h1>
										<p>PGDBM (IIM - Ahmedabad), BE (Govt. College of Engineering, Pune) Senior Director, [24]7.ai</p>
									</div>
								 </div>

								 <div class="faculty-info">
									<div class="fc_block">
										
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/nikhil_prabhakar.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="faculty_info">
										<h1>Nikhil Prabhakar</h1>
										<p>PGDM (IIM-Ahmedabad), B.Tech (NIT-Allahabad) Senior Manager, Product Marketing, CreditVidya</p>
									</div>
								</div>

								<div class="faculty-info">
									<div class="fc_block">
										
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/harneet_jaykar.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="faculty_info">
										<h1>Harneet Jayakar</h1>
										<p>PGDM (IMT-Ghaziabad), B.E. (Mumbai University), UGC-NET Assistant Professor, NMIMS-GASCE</p>
									</div>
								</div>

							</li>

					

							</ul>
						</div>

					</div>

				</div>

			</div>


	</section>



	
	<section class="section04 mobilefaculty">			
			<div class="faculty_structure" id="Faculty">
				<div class="container">
					<div class="faculty_block">
						<h2 class="heading-01">Faculty</h2>
						<p>Leading business thinkers from academia and industry, to give you dynamic mix of theory and business practices*</p>
						<div>
						
								<div class="mobile-faculty-info">
									<div class="fc_block">
										
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/amol_patkar.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="mobile-faculty_info">
										<h1>Amol Patkar</h1>
										<p>B.E. (Delhi College of Engineering), PGDM (IIM - Calcutta) Founder & Director, Fundamentor.com.</p>
									</div>
								</div>


								<div class="mobile-faculty-info">
									<div class="fc_block">
										
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/debabrata_nag.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="mobile-faculty_info">
										<h1>Debabrata Nag</h1>
										<p>B.E. Honours ( Jadavpur University), PGDM (IIM - Ahmedabad) GM, Marketing, Reliance Jio</p>
									</div>
								</div>

								<div class="mobile-faculty-info">
									<div class="fc_block">
										
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/siva_kumar.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="mobile-faculty_info">
										<h1>Dr. Siva M. Kumar</h1>
										<p>Founder and Director at Augentia LLC Fellow Program in Management (IIM-Indore), MS (BITS Pilani)</p>
									</div>
								 </div>


							
								<div class="mobile-faculty-info">
									<div class="fc_block">
										
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/sanjay_verma.png">
									    </div>										
										<i class="clearB"></i>
									</div>
									<div class="mobile-faculty_info">
										<h1>Sanjay Varma</h1>
										<p>PGDBM (IIM - Ahmedabad), BE (Govt. College of Engineering, Pune) Senior Director, [24]7.ai</p>
									</div>
								 </div>

								 <div class="mobile-faculty-info">
									<div class="fc_block">
										
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/nikhil_prabhakar.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="mobile-faculty_info">
										<h1>Nikhil Prabhakar</h1>
										<p>PGDM (IIM-Ahmedabad), B.Tech (NIT-Allahabad) Senior Manager, Product Marketing, CreditVidya</p>
									</div>
								</div>

								<div class="mobile-faculty-info">
									<div class="fc_block">
										
										<div class="fc_name">
											<img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/harneet_jaykar.png">
										</div>										
										<i class="clearB"></i>
									</div>
									<div class="mobile-faculty_info">
										<h1>Harneet Jayakar</h1>
										<p>PGDM (IMT-Ghaziabad), B.E. (Mumbai University), UGC-NET Assistant Professor, NMIMS-GASCE</p>
									</div>
								</div>

						</div>

					</div>

				</div>

			</div>


	</section>

    <section class="Section05" id="Admissions">
        <div class="admission_section">
            <div class="container">
                <div class="admission_block">
                    <h2 class="heading-01">Admissions</h2>
                    <div class="main_admission">
                        <div class="Left_admission_block">
                            <div class="programme_duration_block">
                                <ul>
                                    <li class="program_duration">
                                        <span>Duration of the Program:</span>
                                        <h3>12 Months</h3>
                                    </li>
                                </ul>
                            </div>
                            <!--<div class="programme_validity_block">
                                <ul>
                                    <li class="program_validity">
                                        <span>Program Validity:</span>
                                        <h3>24 Months</h3>
                                    </li>
                                </ul>
                            </div>-->
                            <div class="student_eligibility_section">
                                <h3>Student Eligibility & Scope </h3>
                                <ul style="list-style-type:square;">
                                    
                                    <li>Bachelor’s Degree in any discipline from a recognised University or an equivalent degree recognised by AIU, OR</li>
                                    <li>H.S.C plus 2 years of work experience, OR</li>
                                    <li>S.S.C plus 3 years of Diploma recognised by AICTE and 2 years of work experience.</li>
                                    
                                    <!--<li>Bachelor’s Degree in any discipline from recognised Universities with a minimum of 50% and 2 years of work experience.</li>
                                    <li>Post Graduation / Professional Degree with a minimum of 50% and 1 year of work experience.</li>-->
                                    <!--<li>Bachelor’s Degree in any discipline from a recognised University with minimum 50% marks and preferably 2 years of work experience.</li>
                                    <li>Post-Graduation / Professional Degree from a recognised University with minimum 50% marks and preferably 1 year of Work Experience.</li>
                                    <li>Bachelor’s Degree in any discipline from a recognised university with minimum 50% and preferably 2 years of work experience.</li>
                                    <li>Bachelor’s Degree in any discipline from a recognised university with minimum 50% and post-graduation/professional degree with minimum 50% and preferably 1 year of work experience.</li>
                                    <li>Bachelor’s Degree in any discipline from recognized Universities with minimum 50% marks and preferably 2 years of Work Experience.</li>
                                    <li>Post-Graduation / Professional Degree with minimum 50% marks and preferably 1 year of Work Experience.</li>
                                    <li>Founder/Co-founder of a company or startup with graduation from a recognised university</li>-->
                                </ul>
                            </div>
                        </div>
                        <div class="Right_admission_block">
                            <img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/admission_right_img.jpg">
                        </div>
                        <i class="clearB"></i>
                    </div>
                </div>
            </div><!--Container-->
                <i class="clearB"></i>
                
                <div class="admission-bg">
                <img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/admission_bg.png">
            </div>
        </div>
    </section>

    <div class="nmims">
        <div class="container">
        <h2 class="heading-01">About NMIMS Online</h2>
        <p>
            We are NMIMS Global Access School for Continuing Education, India's #1 online management university. From being the country's premiere distance-learning establishment of the 20th century to crafting a 21st century ready online education platform, our journey has been dynamic and gratifying. Thousands of students have graduated from NGA-SCE and are now fulfilling their hopes and dreams.        </p>
        <br><br><br>
        <h2 class="heading-01">NMIMS Edge</h2>
                <div class="col-md-6 text-left ">
                    <img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/edge_img.jpg" alt="" class="img-responsive">   
                </div>
                <div class="col-md-6 Right_edge_block">
                <ul>
								<li>
									<div class="edge_icon">
                                    <img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/accredited_naac_img.png">
									</div>
									<div class="edge_text">
                                        Accredited by NAAC with Grade A+
									</div>
								</li>
								<li>
									<div class="edge_icon">
                                    <img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/granted_autonomy_icon.png">
									</div>
									<div class="edge_text">
                                        Granted Autonomy Category 1 by UGC
									</div>
								</li>
								<li>
									<div class="edge_icon">
                                    <img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/regional_centre_icon.png">
									</div>
									<div class="edge_text">
                                        9 regional offices across India
									</div>
								</li>
								<li>
									<div class="edge_icon">
                                    <img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/flexible_fee_icon.png">
									</div>
									<div class="edge_text">
                                        Flexible Fee Payment options with EMI facility
									</div>
								</li>
								
								<li>
									<div class="edge_icon">
                                    <img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/help_desk_img.png">
									</div>
									<div class="edge_text">
                                        Dedicated student support team to manage all queries
									</div>
								</li>
							</ul>
                </div>
                 
        </div>
    </div>

 


    <footer>
        <!-- <div class="disclaimerSection" style="background-color: #161d2d;">
            <div class="container disclaimer">
                <p>
                    
                </p>
            </div>
        </div> -->

		<div class="copy-right-grids">
            <div class="container">
                <p class="footer-gd"><b>Disclaimer: </b> This page is a property of Adcanopus, a marketing affiliate of NMIMS Online University. All activity on this page is limited to providing information on Professional Diploma In Digital Marketing
                        
                </p>
            </div>
			
		</div>

	</footer>    
	<div class="floating-form visiable" id="contact_form">
	<div class="contact-opener">Enquire Now</div>
        <form role="form" id="feedbackForm" class="second_form feedbackForm" action="JavaScript:void(0)" onsubmit="nmims_jsfrm_digital('<?php echo SITE_URL?>nmims/submit_frm','2')">
                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Make Your Course Enquiry</h4>                           
                            </div> 
                        <div class="col">   
                        <div class="form-group">
                            <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                            <span class="help-block" id="name_err2"></span>
                        </div>
                        
                    <div class="form-group">
                        <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                        <span class="help-block" id="email_err2" ></span>
                    </div>
                    
                    <div class="form-group">
                        <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode2">
                        <input type="text" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="phone_err2"> </span>
                        
                    </div>
                    <div class="form-group">
                        <select name="city" id="city2" class="form-control">                                                                                                  
                            
                            <option value="" > City</option>
                                                        
                        </select>
                        <span class="help-block" id="city_err2"> </span>
                        
                    </div>

                    <div class="form-group">
                        <select name="city" id="center2" class="form-control">
                            <option value="" >  Regional Center</option>                          
                        </select>
                        <span class="help-block" id="regional_center_err3"> </span>
                    </div>

                    <div class="form-group radio-form">
                        <div class="custom-radio">
                            <p>
                                <input type="checkbox" id="agree3" name="agree3" value="agree"  required checked disabled>
                                <label for="agree3">I authorize NMIMS representative to contact me via phone and/or email. This will override registry on DND/NDNC.</label>
                            </p>
                        </div>                        
                    </div>

                  
                                         
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>

                    <p class="disclaimer">All fields are mandatory. T&C and Privacy Policy applies.</p>

                </div>
            </div>

            <!--<div class="otp-section" id ="otp_section2" style="display:none;">
                <form  action="JavaScript:void(0)" onsubmit="">

                <input type="hidden"  id="otp_email2" name="otp_email" value="">
                <div class="form-group">
                    <input type="text" class="form-control" id="otp2" name="otp" placeholder="OTP"
                        onkeyup="chck_valid('otp', 'Please enter correct otp')"
                        data-attr="Please enter correct otp">
                    <span class="help-block" id="otp_err3"></span>
                </div>
                <div class="submitbtncontainer">
                    <input type="submit" id="sbmtbtn3" value="Enter OTP" name="submit">
                </div>
                
                </form>
                <form  action="JavaScript:void(0)" onsubmit="">
                        <p class="disclaimer">OTP Received to the Entered Mobile Number If Not Received <input type="submit" id="res-sbmtbtn3"  value="Click-Here"></p>
                </form>
            </div>-->

        </form>	
    </div>

    <div class="popup-enquiry-form mfp-hide" id="popupForm">
        <form role="form" id="feedbackForm" class="third_form feedbackForm" action="JavaScript:void(0)" onsubmit="nmims_jsfrm_digital('<?php echo SITE_URL?>nmims/submit_frm','3')">                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/nmims-marketing-assets/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Make Your Course Enquiry</h4>                           
                            </div> 
                        <div class="col">   
                        <div class="form-group">
                            <input type="text" class="form-control" id="name3" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                            <span class="help-block" id="name_err3"></span>
                        </div>
              
                    <div class="form-group">
                        <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                        <span class="help-block" id="email_err3" ></span>
                    </div>
                    
                    <div class="form-group">
                        <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode3">
                        <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="phone_err3"> </span>
                       
                    </div>
                    <div class="form-group">
                        <select name="city" id="city3" class="form-control">
                            <option value="" > City</option>                          
                        </select>
                        <span class="help-block" id="city_err3"> </span>
                    </div>

                    <div class="form-group">
                        <select name="center" id="center3" class="form-control">
                            <option value="" >  Regional Center</option>                          
                        </select>
                        <span class="help-block" id="regional_center_err3"> </span>
                    </div>

                    <div class="form-group radio-form">
                        <div class="custom-radio">
                            <p>
                                <input type="checkbox" id="agree3" name="agree3" value="agree"  required checked disabled>
                                <label for="agree3">I authorize NMIMS representative to contact me via phone and/or email. This will override registry on DND/NDNC.</label>
                            </p>
                        </div>    
                    
                        <span class="help-block" id="cource_err3"> </span>                           
                    </div>
                                          
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>

                    <p class="disclaimer">All fields are mandatory. T&C and Privacy Policy applies.</p>

                </div>
            </div>
            <!--<div class="otp-section" id ="otp_section3" style="display:none;">
                <form  action="JavaScript:void(0)" onsubmit="">

                <input type="hidden"  id="otp_email3" name="otp_email" value="">
                <div class="form-group">
                    <input type="text" class="form-control" id="otp3" name="otp" placeholder="OTP"
                        onkeyup="chck_valid('otp', 'Please enter correct otp')"
                        data-attr="Please enter correct otp">
                    <span class="help-block" id="otp_err3"></span>
                </div>
                <div class="submitbtncontainer">
                    <input type="submit" id="sbmtbtn3" value="Enter OTP" name="submit">
                </div>
                
                </form>
                <form  action="JavaScript:void(0)" onsubmit="">
                        <p class="disclaimer">OTP Received to the Entered Mobile Number If Not Received <input type="submit" id="res-sbmtbtn3"  value="Click-Here"></p>
                </form>
            </div>-->
        </form>
    </div>
                                    



	<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
	<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
	<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
	<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
	<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
	
	
	<script src="<?php echo S3_URL?>/site/nmims-marketing-assets/js/vendor.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script> 
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>

    <script src="<?php echo S3_URL?>/site/nmims-marketing-assets/js/slick.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.js"></script>

    <script src="<?php echo S3_URL?>/site/nmims-marketing-assets/js/main.js"></script>
	
	 <script src="<?php echo S3_URL?>/site/scripts/nmims.js"></script>
<script>


$(document).ready(function(){
	var second_statearr = []; var second_sstate = '' ; var second_data ; 
	setTimeout(function(){
		
		$.ajax({
			url:'<?php echo S3_URL?>/site/nmims-marketing-assets/json/cities.json?v=0.1',
			success:function(res){
                second_data = res ;
			                 console.log('Respose');
                
			   for(i=0; i<second_data.length; i++){
                second_statearr.push(second_data[i].state);
			   }
			   $('.second_form').find('#city2').append('<option value="">Select city</option>');
			   second_statearr.sort();
			   second_statearr = $.unique( second_statearr );

			   $.each(second_statearr, function(key, state){
				  $('.second_form').find('#city2').append('<option value="'+state+'">'+state+'</option>');
			   });
               second_sstate = second_statearr[0];
             
			   change_second_city(second_sstate);
			}
			
		 });
	},100);
});

var second_statearr = []; var second_sstate = '' ; var second_data ; 

$('#city2').on('change', function () {
    $.ajax({
			url:'<?php echo S3_URL?>/site/nmims-marketing-assets/json/cities.json?v=0.1',
			success:function(res){
                second_data = res ;
			    console.log('Respose');
                
			   for(i=0; i<second_data.length; i++){
                second_statearr.push(second_data[i].state);
			   }
			   $('.second_form').find('#city2').append('<option value="">Select city</option>');
			   second_statearr.sort();
			   second_statearr = $.unique( second_statearr );

			   $.each(second_statearr, function(key, state){
				  $('.second_form').find('#city2').append('<option value="'+state+'">'+state+'</option>');
			   });
               second_sstate = second_statearr[0];
             
			   change_second_city(second_sstate);
			}
			
		 });
});

function change_second_city(state){
    var citys =$('#city2').val();
 
	if(citys.trim()==''){
		$('.second_form').find('#center2').append('<option value="">Select center</option>');
	}else{
		$('.second_form').find('#center2').html('');
		$('.second_form').find('#center2').append('<option value="">Select center</option>');
	for(i=0; i<second_data.length; i++){
    	if(citys == second_data[i].state){
    		$('.second_form').find('#center2').append('<option value="'+second_data[i].name+'">'+second_data[i].name+'</option>');
    	}
	}}
}

</script>



<script>


$(document).ready(function(){
	var statearr = []; var sstate = '' ; var data ; 
	setTimeout(function(){
		
		$.ajax({
			url:'<?php echo S3_URL?>/site/nmims-marketing-assets/json/cities.json?v=0.1',
			success:function(res){
			   data = res ;
			                 console.log('Respose');
                
			   for(i=0; i<data.length; i++){
				  statearr.push(data[i].state);
			   }
			   $('.first_form').find('#city1').append('<option value="">Select city</option>');
			   statearr.sort();
			   statearr = $.unique( statearr );

			   $.each(statearr, function(key, state){
				  $('.first_form').find('#city1').append('<option value="'+state+'">'+state+'</option>');
			   });
               sstate = statearr[0];
             
			   changecity(sstate);
			}
			
		 });
	},100);
});

var statearr = []; var sstate = '' ; var data ; 

$('#city1').on('change', function () {


        $.ajax({
            url:'<?php echo S3_URL?>/site/nmims-marketing-assets/json/cities.json?v=0.1',
            success:function(res){
               data = res ;
               console.log('Respose');
                console.log(data);

               for(i=0; i<data.length; i++){
                  statearr.push(data[i].state);
               }
              
               statearr.sort();
               statearr = $.unique( statearr );
               
               sstate = statearr;
             
               changecity(sstate);
            }
            
         });
});

function changecity(state){
    var citys =$('#city1').val();
 
	if(citys.trim()==''){
		$('.first_form').find('#center1').append('<option value="">Select center</option>');
	}else{
		$('.first_form').find('#center1').html('');
		$('.first_form').find('#center1').append('<option value="">Select center</option>');
	for(i=0; i<data.length; i++){
    	if(citys == data[i].state){
    		$('.first_form').find('#center1').append('<option value="'+data[i].name+'">'+data[i].name+'</option>');
    	}
	}}
}

</script>






<script>


$(document).ready(function(){
	var statearr_third = []; var sstate_third = '' ; var data_third ; 
	setTimeout(function(){
		
		$.ajax({
			url:'<?php echo S3_URL?>/site/nmims-marketing-assets/json/cities.json?v=0.1',
			success:function(res){
                data_third = res ;
			                 console.log('Respose');
                
			   for(i=0; i<data_third.length; i++){
                statearr_third.push(data_third[i].state);
			   }
			   $('.third_form').find('#city3').append('<option value="">Select city</option>');
			   statearr_third.sort();
			   statearr_third = $.unique( statearr_third );

			   $.each(statearr_third, function(key, state){
				  $('.third_form').find('#city3').append('<option value="'+state+'">'+state+'</option>');
			   });
               sstate_third = statearr_third[0];
             
			   changethird_city(sstate_third);
			}
			
		 });
	},100);
});

var statearr_third = []; var sstate_third = '' ; var data_third ;  

$('#city3').on('change', function () {


    $.ajax({
			url:'<?php echo S3_URL?>/site/nmims-marketing-assets/json/cities.json?v=0.1',
			success:function(res){
                data_third = res ;
			                 console.log('Respose');
                
			   for(i=0; i<data_third.length; i++){
                statearr_third.push(data_third[i].state);
			   }
			   $('.third_form').find('#city3').append('<option value="">Select city</option>');
			   statearr_third.sort();
			   statearr_third = $.unique( statearr_third );

			   $.each(statearr_third, function(key, state){
				  $('.third_form').find('#city3').append('<option value="'+state+'">'+state+'</option>');
			   });
               sstate_third = statearr_third[0];
             
			   changethird_city(sstate_third);
			}
			
		 });
});

function changethird_city(state){
    var citys =$('#city3').val();
 
	if(citys.trim()==''){
		$('.third_form').find('#center3').append('<option value="">Select center</option>');
	}else{
		$('.third_form').find('#center3').html('');
		$('.third_form').find('#center3').append('<option value="">Select center</option>');
	for(i=0; i<data_third.length; i++){
    	if(citys == data_third[i].state){
    		$('.third_form').find('#center3').append('<option value="'+data_third[i].name+'">'+data_third[i].name+'</option>');
    	}
	}}
}

</script>
<script>

$(document).ready(function(){
  $('.bxslider').bxSlider();
});


</script>

<script>
	$(".resp-tab-item2").click(function(){
		$(this).addClass("resp-tab-active");
		$(".resp-tab-item1").removeClass("resp-tab-active");

		$(".resp-tab-content2").addClass("resp-tab-content-active");
		$(".resp-tab-content2").show();

		$(".resp-tab-content1").removeClass("resp-tab-content-active");
		$(".resp-tab-content1").hide();
		
	});

	$(".resp-tab-item1").click(function(){
		$(this).addClass("resp-tab-active");
		$(".resp-tab-item2").removeClass("resp-tab-active");
		
		$(".resp-tab-content1").addClass("resp-tab-content-active");
		$(".resp-tab-content1").show();

		$(".resp-tab-content2").removeClass("resp-tab-content-active");
		$(".resp-tab-content2").hide();
	});


</script>




    </body>
</html>