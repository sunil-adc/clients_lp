<?php $arr_all = all_arrays(); ?>
<script type="application/javascript">

 function checkInputs(){
	  
    var regex = /^[0-9]*$/;
	var role_name  = document.getElementById("role_name").value;
	var role_details = document.getElementById("role_details").value;
	
		
	if(role_name == "" || role_name.trim() ==""){
		alert(" Role Tag is required");
		document.getElementById("role_name").focus();
		return false;
	}
	
	/*if(role_details == "" || role_details.trim() ==""){
		alert(" Role details is required");
		document.getElementById("role_details").focus();
		return false;
	}*/

	
			
}

 </script>


<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Add Role</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_role">Manage Role</a></h3>	
        </div>
        <div class="module-body">
                <?php 
					if( $this->session->flashdata('error') ) { 
					   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
							<strong>'.$this->session->flashdata('error').'</strong></div>';
				
					}else if( $this->session->flashdata('success') ) { 
					
					   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
						  <strong>'.$this->session->flashdata('success').'</strong></div>';
					}
				  ?>
                
                <br />                

                  <form class="form-horizontal row-fluid" method="post" action="<?php echo SITE_URL.'index.php/cms/manage_role/add' ?>" enctype="multipart/form-data">
                    
                        
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Role Name *</label>
                            <div class="controls">
                               <input  type="text" name="role_name" id="role_name" placeholder="role name" tabindex="1" class="span8" />
                            </div>
                        </div>
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Roles  *</label>
                            <div class="controls">
						<?php 
                     	     foreach($menu_list as $k => $v){
							  echo "<label style='color:blue'><strong>".$v[0]."</strong></label>" ; 
								foreach($v[2] as $k1 => $v1) {
									  
									    if(strtolower($v1[1]) == strtolower($v1[2])) {
                                            ?>
                                            <input id="role_details" name="role_details[]" type="checkbox" value="<?php echo $v1[1] ?>"
                                            <?php
                                                if(isset($role_details)) {
                                                    echo (in_array($v1[1], $role_details)) ? "checked='checked'" : "";
                                                }
                                            ?> /> <?php
                                        } else {
                                            ?>
                                            <input id="role_details" name="role_details[]" type="checkbox" value="<?php echo $v1[1].",".$v1[2]; ?>"
                                            <?php
                                                if(isset($role_details)) {
                                                    echo (in_array($v1[1].",".$v1[2], $role_details)) ? "checked='checked'" : "";
                                                }
                                            ?> /> 
                                            <?php
                                        }
                                    
									
									echo "&nbsp;".$v1[0]."<br/>";
                                  
									 }
									 echo "<br/>";
							  }
	                     ?>
                          </div>
                        </div>
                        
                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()">
                            </div>
                        </div>
                    </form>
                </div>
                </div>
                                
            </div><!--/.content-->
        </div>
