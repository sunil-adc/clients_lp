<?php $arr_all = all_arrays(); ?>
<script type="application/javascript">

function checkInputs(){
	  
    var regex = /^[0-9]*$/;
	var bank = document.getElementById("bank").value;
	var offers_name = document.getElementById("offers_name").value;
	var offer_image  = document.getElementById("offer_image").value;
	var offers_url  = document.getElementById("offers_url").value;
	
		
	if(bank == "" || bank.trim() ==""){
		alert("Please bank is required");
		document.getElementById("bank").focus();
		return false;
	}
	
	if(offers_name == "" || offers_name.trim() ==""){
		alert(" Category is required");
		document.getElementById("offers_name").focus();
		return false;
	}
	
	
	if( offers_url== "" || offers_url.trim() ==""){
		alert("Url is required");
		document.getElementById("offers_url").focus();
		return false;
	}
			
}
 </script>


<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Update Offers</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>cms/manage_cc">Manage Offers </a></h3>	
        </div>
        <div class="module-body">
               <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';
			
				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
			  ?>
                
                <br />                
                  <?php foreach($setting as $n){ ?>
                  <form class="form-horizontal row-fluid" method="post" action="<?php echo FULL_CMS_URL.'/manage_cc/edit' ?>" enctype="multipart/form-data">
                    <input type="hidden" name="id" id="id" value="<?php echo $n->id;?>"/>
                       
                         <div class="control-group">
                            <label class="control-label" for="basicinput"> Bank * </label>
                            <div class="controls">
                                <select id="bank" name="bank" tabindex="2" data-placeholder="Offers name" class="span8">
                                   <option value="">Select Bank</option>
                                   <?php foreach($bank as $row){
										  if($row->id== $n->bank){$s='selected';}else{$s='';}
									     echo "<option value='".$row->id."' ".$s.">".$row->bank_name."</option>";
								   }?>
                                </select>
                            </div>
                        </div>
                        
						<div class="control-group">
                            <label class="control-label" for="basicinput">Card Name *</label>
                            <div class="controls">
                               <input type="text" name="offers_name" id="offers_name" placeholder="offers name" tabindex="3" class="span8" value="<?php echo $n->offers_name?>"  />
                            </div>
                        </div>
               
                        <div class="control-group">
                            <label class="control-label" for="basicinput">offer Image</label>
                            <div class="controls">
                                <img src="<?php echo S3_URL.'/site/card_img/'.$n->offer_image;  ?>" width="150" height="40">
                            </div>
                        </div>                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">offer Image *</label>
                            <div class="controls">
                               <input type="file" name="offer_image" id="offer_image" placeholder="Offer Image" tabindex="5" class="span8"  />
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Offer Url *</label>
                            <div class="controls">
                               <textarea name="offers_url" id="offers_url" placeholder="offers url" tabindex="6" class="span8" rows="3"/><?php echo $n->offers_url?></textarea>
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="basicinput">Status</label>
                            <div class="controls">
                               <select tabindex="5" id="status" name="status" data-placeholder="Select Status.." class="span8">
                               <option value="" <?php echo ($n->status == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                               <option value="1" <?php echo ($n->status == '1') ? 'selected="selectec"' : '';?>>Active</option>
                               <option value="0" <?php echo ($n->status == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
                                </select>
                            </div>
                        </div>
                     <div class="control-group">
                        <div class="controls">
                            <input type="submit" name="addform" value="Save" onclick="return checkInputs()">
                        </div>
                    </div>
                 </form>
               <?php } ?>   
              </div>
            </div>                                
         </div><!--/.content-->
       </div>

