<script>
function valid_form(){
	
	var r_= new Array("rewards", "status");
	
	for(var i=0; i < r_.length; i++   ){
		if($.trim($("#"+r_[i]).val()) == "" || $("#"+r_[i]).val() == 0 || $("#"+r_[i]).val() == "undefined"){
		   $("#"+r_[i]+"_err").html($("#"+r_[i]).attr("data-attr"));
		   $("#"+r_[i]).focus();
		   return false;	
		}else{
		   $("#"+r_[i]+"_err").html("");
		}
	}
}
</script>

<?php $arr_all = all_arrays();?>
<div class="page-content-wrap">
                
<div class="row">
    <div class="col-md-12">
        
        <form  class="form-horizontal" action="<?php echo $form_submit?>" method="post" enctype="multipart/form-data">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong><?php echo $page_title?></h3>
                <ul class="panel-controls">
                    <li><a href="<?php echo FULL_CMS_URL."/".$cur_controller."/index/".$result_data['prod_id']?>"><span class="fa fa-times"></span></a></li>
                </ul>
            </div>
            <div class="panel-body">
			  <?php 
                if( $this->session->flashdata('error') ) { 
                    echo '<div class="alert alert-danger" role="alert" style="height:39px; line-height:8px">
                			<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" >&times;</span></button>
                			'.$this->session->flashdata('error').'</div>';
            
                }else if( $this->session->flashdata('success') ) { 
                   echo '<div class="alert alert-success" role="alert" style="height:39px; line-height:8px">
                			<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" >&times;</span></button>
                			'.$this->session->flashdata('success').'</div>';
            
                }
			  ?>
            
            </div>
            
            <div class="panel-body">                                                                        
                
                <div class="form-group">
                    <label class="col-md-2 col-xs-12 control-label">Rewards*</label>
                    <div class="col-md-4 col-xs-12">                                            
                        <div class="input-group">
                            <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                            <input type="text" class="form-control" name="rewards" id="rewards" tabindex="1" data-attr="Please enter rewards" value="<?php echo $result_data['rewards'] ?>"/>
                        	<input type="hidden" name="prod_id" id="prod_id"  value="<?php echo $result_data['prod_id'];?>" />                
                        </div>                                            
                       <span class="help-block" id="min_amt_err"></span>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-md-2 col-xs-12 control-label">Status*</label>
                    <div class="col-md-4 col-xs-12">                                                                                            
                        <select class="form-control select" name="status" id="status" tabindex="5" data-attr="Please select status">
                        <option value=""  <?php echo ($result_data['status'] == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                        <option value="1" <?php echo ($result_data['status'] == '1') ? 'selected="selectec"' : '';?>>Active</option>
                        <option value="2" <?php echo ($result_data['status'] == '2') ? 'selected="selectec"' : '';?>>Inactive</option>
                        </select>
                     <span class="help-block" id="status_err"></span>
                    </div>
                </div>
                
            </div>
            
            <input type="hidden" name="id" id="id" value="<?php echo $result_data[$primary_field]?>" />
            <input type="hidden" name="add_page" id="add_page" value="<?php echo $add_page?>" />
            <input type="hidden" name="mode" id="mode" value="<?php echo ((strtolower ($mode) == 'edit')  ? 'edit' : 'add');?>" />
            
            <div class="panel-footer">
                <button class="btn btn-primary " type="submit" name="submit" value="Go" tabindex="3" onclick="return valid_form()">Submit</button>
                <button class="btn btn-default">Clear Form</button>                                    
            </div>
        </div>
        </form>
        
    </div>
</div>                    
                    
</div>
<!-- END PAGE CONTENT WRAPPER -->  









