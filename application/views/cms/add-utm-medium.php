<?php $arr_all = all_arrays(); ?>
<script type="application/javascript">

 function checkInputs()
 { 
    var regex = /^[0-9]*$/;
	var utm_source =document.getElementById("utm_source").value;
	var utm_medium =document.getElementById("utm_medium").value;
	var pixel      =document.getElementById("pixel").value;
	
	if(utm_source == "" || utm_source.trim() ==""){
		alert("Utm source is required");
		document.getElementById("utm_source").focus();
		return false;
	}
	
	if( utm_medium== "" || utm_medium.trim() ==""){
		alert("Medium is required");
		document.getElementById("utm_medium").focus();
		return false;
	}
	
	if( pixel == "" || pixel.trim() ==""){
		alert("Pixel is required");
		document.getElementById("pixel").focus();
		return false;
	}
		
		
 }

 </script>
 
 

<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Add Utm Medium</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_utm_medium">Manage Utm Medium</a></h3>	
        </div>
        <div class="module-body">
               <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';
			
				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
			  ?>
                
                <br />                

                  <form class="form-horizontal row-fluid" method="post" action="<?php echo SITE_URL.'cms/manage_utm_medium/add' ?>">
                    
        
                      <div class="control-group">
                            <label class="control-label" for="basicinput"> Utm source *</label>
                            <div class="controls">
                              <?php echo '<select name="utm_source" id="utm_source" class="span8" tabindex="1">';
								    echo '<option value="">Select Utm Source</option>';
								    if($net_data->num_rows() > 0){
									   foreach ($net_data->result() as $row) {
										      echo '<option value="'.$row->id.'">'.$row->utm_source.'</option>';
									   }
								    }
								    echo '</select>';
						       ?>
                          </div>
                       </div> 		
						
                        		
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Utm Medium *</label>
                            <div class="controls">
                                <input type="text" id="utm_medium" name="utm_medium" tabindex="2" placeholder="Utm Medium" class="span8">
                            </div>
                        </div>
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Pixel * </label>
                            <div class="controls">
                                <textarea name="pixel" id="pixel" placeholder="pixel" tabindex="3" class="span8"/></textarea>
                                <span class="help-inline">Replace dynamic user id with "{USERID}"</span>
                            </div>
                        </div>
                        
                        
                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()" tabindex="4">
                            </div>
                        </div>
                    </form>
                </div>
                </div>
                                
            </div><!--/.content-->
        </div>
