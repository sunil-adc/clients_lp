<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo SITE_NAME?></title>
        <!--<link rel="stylesheet" type="text/css" href="<?php echo S3_URL;?>/css/pagename.css" />-->
        
        <link type="text/css" href="<?php echo S3_URL;?>/cms/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="<?php echo S3_URL;?>/cms/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
		 <link type="text/css" href="<?php echo S3_URL;?>/cms/bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link type="text/css" href="<?php echo S3_URL;?>/cms/css/theme.css" rel="stylesheet">
        <link type="text/css" href="<?php echo S3_URL;?>/cms/images/icons/css/font-awesome.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 

        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
    </head>
    <body> 
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner" style="background: lightgray;">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="<?php echo FULL_CMS_URL;?>/dashboard"><img src="<?php echo S3_URL ?>/cms/images/logo.png" class="img-responsive"></a>
                    <div class="nav-collapse collapse navbar-inverse-collapse">
                        
                        <!--<ul class="nav pull-right">
                            <li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?php echo S3_URL;?>cms/images/user.png" class="nav-avatar" />
                                <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Your Profile</a></li>
                                    <li><a href="#">Edit Profile</a></li>
                                    <li><a href="#">Account Settings</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Logout</a></li>
                                </ul>
                            </li>
                        </ul>-->
                    </div>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    


    <input type="hidden" id="cms_site_url" value="<?php echo FULL_SITE_URL;?>/cms">
    <input type="hidden" id="cms_url" value="<?php echo SITE_URL;?>cms">
    
