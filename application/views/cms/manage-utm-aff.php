<?php $all_array = all_arrays(); ?>
<div class="span9">
<div class="content">

    <div class="module">
        <div class="module-head">
            <h3>Utm Affiliate Details</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>cms/manage_utm_aff/add_form">Add Utm Affiliate</a></h3>	
        </div>
        <div class="module-body">
            <p>
                <strong>All Utm affiliate List </strong>
            </p>
           <?php 
            if( $this->session->flashdata('error') ) { 
               echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>'.$this->session->flashdata('error').'</strong></div>';
        
            }else if( $this->session->flashdata('success') ) { 
            
               echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>'.$this->session->flashdata('success').'</strong></div>';
            }
          ?>

            

            <form class="navbar-search pull-left input-append" action="<?php echo FULL_CMS_URL?>/manage_utm_aff" method="post">
            <select name="search_field" id="search_field" >                                
                <option value="utm_aff" <?php echo ($search_field == "utm_aff") ? "selected='selected'" : ""; ?>>Utm Affiliate</option>
            </select>
            &nbsp;&nbsp;
            <input type="text" name="search_txt" value="<?php echo $search_txt;?>" class="text-input small-input" />
           
            <button class="btn" id="btn_search" name="btn_search" type="submit" value="GO">
            <i class="icon-search"></i>
            </button>
            </form>
            <br />
            <!-- <hr /> -->
            <br />
            <table class="table table-bordered">
              <thead>
                <tr>
                <th>No.</th>
                <th >Utm Affiliate</th>
                <th >Utm Source</th>
                <th >Fire Type</th>
                <th >Form Type</th>
                <th >Otp Verify</th>
                <th >Status</th>
                <th >Update</th>
                </tr>
              </thead>
              <tbody>
              <?php
                if(is_array($details) && count($details) > 0){	
                  $sl_no=1;
                  foreach($details as $p)
                  {
                        echo "<tr>";
						echo "<td>" .$sl_no. "</td>";
						echo "<td>". $p->utm_aff."</td>";
						echo "<td>".($p->utm_source != '0' ? $this->db_function->get_single_value(SOURCE_PIXEL, "utm_source" ,"id =".$p->utm_source) : '0')."</td>";
             echo "<td>" . ($p->fire_type != 0 ? $all_array['FIRE_LIMIT'][$p->fire_type] : "Not Specified"). "</td>";
              echo "<td>". ($p->form_type ==1 ? 'Small Form' : 'Big Form')."</td>";
              echo "<td>". ($p->opt_verify ==1 ? 'Enable' : 'Disable')."</td>";
						echo "<td>".($p->status == 1 ? 'Active' : 'Inactive' )."</td>";
						echo "<td><a href='".FULL_CMS_URL."/manage_utm_aff/edit_form/".$p->id."'><img alt='Edit' src='".S3_URL."/cms/images/icons/pencil.png'  width='20' height='20'></img></td>";
						echo "</tr>";
						$sl_no++;
                  }
                }
                ?>
               <tr>
                  <td colspan="8" style="line-height: 1.5em; !important">
                    <?php
                      if (count($details) > 0) {
                         echo $links;
                      }
                    ?>
                  </td>
                </tr>
              </tbody>
            </table>
            <br>
        </div>
    </div>

    <!--/.module-->
<br />    
</div><!--/.content-->
</div>





