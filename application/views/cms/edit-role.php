<?php $arr_all = all_arrays(); ?>
<script type="application/javascript">

 function checkInputs(){
	  
    var regex = /^[0-9]*$/;
	var role_name  = document.getElementById("role_name").value;
	var role_details = document.getElementById("role_details").value;
	
		
	if(role_name == "" || role_name.trim() ==""){
		alert(" Role Tag is required");
		document.getElementById("role_name").focus();
		return false;
	}
	
	if(role_details == "" || role_details.trim() ==""){
		alert(" Role details is required");
		document.getElementById("role_details").focus();
		return false;
	}

}

 </script>

<?php
       if (isset($result_data->role_details)) {
		    $unserialize_role = unserialize($result_data->role_details);
			if (is_array($unserialize_role)) {
				$role_details = unserialize($result_data->role_details);
			}
		}
		
		$menu_list = $this->common_model->Menu_Array();
		$total_menu = count($menu_list) - 1;
		$i = 1;
		
?>	

<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Update Role</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_role">Manage Role</a></h3>	
        </div>
        <div class="module-body">
                <?php 
					if( $this->session->flashdata('error') ) { 
					   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
							<strong>'.$this->session->flashdata('error').'</strong></div>';
				
					}else if( $this->session->flashdata('success') ) { 
					
					   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
						  <strong>'.$this->session->flashdata('success').'</strong></div>';
					}
				  ?>
                
                <br />                

                  <form class="form-horizontal row-fluid" method="post" action="<?php echo SITE_URL.'index.php/cms/manage_role/edit' ?>" enctype="multipart/form-data">
                       <input type="hidden" value="<?php echo $result_data->id?>" id="id" name="id" />
                       
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Role Name *</label>
                            <div class="controls">
                               <input  type="text" name="role_name" id="role_name" placeholder="role name" tabindex="1" class="span8" value="<?php echo $result_data->role_name?>"/>
                            </div>
                        </div>
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Roles *</label>
                            <div class="controls"><br/>
						<?php 
                     	     foreach($menu_list as $k => $v){
							  echo "<label ><strong style='color:chocolate';text-transform: uppercase;>".$v[0]."</strong></label>" ; 
								foreach($v[2] as $k1 => $v1) {
									  
									    if(strtolower($v1[1]) == strtolower($v1[2])) {
                                            ?>
                                            <input id="role_details" name="role_details[]" type="checkbox" value="<?php echo $v1[1] ?>"
                                            <?php
                                                if(isset($role_details)) {
                                                    echo (in_array($v1[1], $role_details)) ? "checked='checked'" : "";
                                                }
                                            ?> /> <?php
                                        } else {
                                            ?>
                                            <input id="role_details" name="role_details[]" type="checkbox" value="<?php echo $v1[1].",".$v1[2]; ?>"
                                            <?php
                                                if(isset($role_details)) {
                                                    echo (in_array($v1[1].",".$v1[2], $role_details)) ? "checked='checked'" : "";
                                                }
                                            ?> /> 
                                            <?php
                                        }
                                    
									
									echo "&nbsp;".$v1[0]."<br/>";
                                  
									 }
									 echo "<br/>";
							  }
	                     ?>
                          </div>
                        </div>
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Status</label>
                            <div class="controls">
                               <select tabindex="5" id="status" name="status" data-placeholder="Select Status.." class="span8">
                               <option value="" <?php echo ($result_data->status == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                               <option value="1" <?php echo ($result_data->status == '1') ? 'selected="selectec"' : '';?>>Active</option>
                               <option value="0" <?php echo ($result_data->status == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()">
                            </div>
                        </div>
                        
                    </form>
                </div>
                </div>
                                
            </div><!--/.content-->
        </div>
