<?php $arr_all = all_arrays(); ?>
<script type="application/javascript">

 function checkInputs(){
	  
    var regex = /^[0-9]*$/;
	var tag_line  = document.getElementById("tag_line").value;
	var offers_name = document.getElementById("offers_name").value;
	var campaign_name = document.getElementById("campaign_name").value;
	var bank_logo  = document.getElementById("bank_logo").value;
	var offer_image  = document.getElementById("offer_image").value;
	var offers_url  = document.getElementById("offers_url").value;
	
		
	if(tag_line == "" || tag_line.trim() ==""){
		alert(" Tag is required");
		document.getElementById("tag_line").focus();
		return false;
	}
	
	if(offers_name == "" || offers_name.trim() ==""){
		alert(" Category is required");
		document.getElementById("offers_name").focus();
		return false;
	}
	
	if(campaign_name == "" || campaign_name.trim() ==""){
		alert(" Campaign Name is required");
		document.getElementById("campaign_name").focus();
		return false;
	}

	if( bank_logo== "" || bank_logo.trim() ==""){
		alert("Logo is required");
		document.getElementById("bank_logo").focus();
		return false;
	}
	
	if( offer_image== "" || offer_image.trim() ==""){
		alert("Image is required");
		document.getElementById("offer_image").focus();
		return false;
	}
	
	if( offers_url== "" || offers_url.trim() ==""){
		alert("Url is required");
		document.getElementById("offers_url").focus();
		return false;
	}
			
}

 </script>


<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Add Banner</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_offers">Manage Banners</a></h3>	
        </div>
        <div class="module-body">
                <?php 
					if( $this->session->flashdata('error') ) { 
					   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
							<strong>'.$this->session->flashdata('error').'</strong></div>';
				
					}else if( $this->session->flashdata('success') ) { 
					
					   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
						  <strong>'.$this->session->flashdata('success').'</strong></div>';
					}
				  ?>
                
                <br />                

                  <form class="form-horizontal row-fluid" method="post" action="<?php echo SITE_URL.'index.php/cms/manage_offers/add' ?>" enctype="multipart/form-data">
                    
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Tag line *</label>
                            <div class="controls">
                               <input type="text" name="tag_line" id="tag_line" placeholder="Tag Line" tabindex="1" class="span8"  />
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput"> Category * </label>
                            <div class="controls">
                                <select id="offers_name" name="offers_name" tabindex="2" data-placeholder="Offers name" class="span8">
                                   <option value="">Select Category Types</option>
                                   <?php foreach($arr_all['BANK_OFFERS'] as $k=>$v){
									     echo "<option value='$k'>$v</option>";
								   }?>
                                </select>
                            </div>
                        </div>
                        
						<div class="control-group">
                            <label class="control-label" for="basicinput">Campaign Name *</label>
                            <div class="controls">
                               <input type="text" name="campaign_name" id="campaign_name" placeholder="Campaign Name" tabindex="3" class="span8"  />
                            </div>
                        </div>
                                                
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Brand Logo *</label>
                            <div class="controls">
                               <input type="file" name="bank_logo" id="bank_logo" placeholder="Bank Logo" tabindex="4" class="span8"  />
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">offer Image *</label>
                            <div class="controls">
                               <input type="file" name="offer_image" id="offer_image" placeholder="Offer Image" tabindex="5" class="span8"  />
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Offer Url *</label>
                            <div class="controls">
                               <textarea name="offers_url" id="offers_url" placeholder="offers url" tabindex="6" class="span8" rows="3"/></textarea>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()" tabindex="7">
                            </div>
                        </div>
                    </form>
                </div>
                </div>
                                
            </div><!--/.content-->
        </div>
