</div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->
        <div class="footer">
            <div class="container">
                <b class="copyright">&copy; Adcanopus clients 2015  - adcanopus.com </b>All rights reserved.
            </div>
        </div>
        <script src="<?php echo S3_URL;?>/cms/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="<?php echo S3_URL;?>/cms/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="<?php echo S3_URL;?>/cms/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo S3_URL;?>/cms/bootstrap/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
        <script src="<?php echo S3_URL;?>/cms/scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="<?php echo S3_URL;?>/cms/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
        <script src="<?php echo S3_URL;?>/cms/scripts/common.js" type="text/javascript"></script>
    </body>
