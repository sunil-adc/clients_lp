<script>
function valid_form(){
	
	var r_= new Array( "bank_name", "status");
	
	for(var i=0; i < r_.length; i++   ){
		if($.trim($("#"+r_[i]).val()) == "" || $("#"+r_[i]).val() == 0 || $("#"+r_[i]).val() == "undefined"){
		   $("#"+r_[i]+"_err").html($("#"+r_[i]).attr("data-attr"));
		   $("#"+r_[i]).focus();
		   return false;	
		}else{
		   $("#"+r_[i]+"_err").html("");
		}
	}
}

</script>

<?php $arr_all = all_arrays(); ?>

<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Add Bank</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo FULL_CMS_URL."/".$manage_page?>">Manage Bank</a></h3>	
        </div>
        <div class="module-body">
                <?php 
					if( $this->session->flashdata('error') ) { 
					   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
							<strong>'.$this->session->flashdata('error').'</strong></div>';
				
					}else if( $this->session->flashdata('success') ) { 
					
					   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
						  <strong>'.$this->session->flashdata('success').'</strong></div>';
					}
				  ?>
                
                <br />                



        
        <form  class="form-horizontal row-fluid" action="<?php echo $form_submit?>" method="post" enctype="multipart/form-data">
        
                <div class="control-group">
					<label class="control-label" for="basicinput">Bank Name *</label>
					<div class="controls">
					  <input type="text" class="span8" name="bank_name" id="bank_name" tabindex="2" value="<?php echo $result_data['bank_name']?>"data-attr="Please enter Client name"/>
					</div>                                            
				   <span class="help-block" id="bank_name_err"></span>
				</div>
                
                
                <div class="control-group">
                        <label class="control-label" for="basicinput">Bank Logo*</label>
                            <div class="controls">
                        	<?php if( $result_data[$primary_field] != ""){?>                        
                                <img src="<?php echo S3_URL?>/site/bank_img/<?php echo $result_data['bank_logo']?>" alt="admin image" height="100px" width="100px">    
						<?php } ?>                                                                                                               
                        <input type="file" class="span8" name="bank_image" id="bank_image" title="Browse file" tabindex="5"/>
                        <span class="help-block" id="bank_image_err"></span>
                    </div>
                </div>
                
          
                <div class="control-group">
						<label class="control-label" for="basicinput">Status *</label>
						<div class="controls">
                        <select class="span8 select" name="status" id="status" tabindex="6" data-attr="Please select status">
                        <option value=""  <?php echo ($result_data['status'] == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                        <option value="1" <?php echo ($result_data['status'] == '1') ? 'selected="selectec"' : '';?>>Active</option>
                        <option value="2" <?php echo ($result_data['status'] == '2') ? 'selected="selectec"' : '';?>>Inactive</option>
                        </select>
                     <span class="help-block" id="status_err"></span>
                    </div>
                </div>
                
            
            
            <input type="hidden" name="id" id="id" value="<?php echo $result_data[$primary_field]?>" />
            <input type="hidden" name="add_page" id="add_page" value="<?php echo $add_page?>" />
            <input type="hidden" name="mode" id="mode" value="<?php echo ((strtolower ($mode) == 'edit')  ? 'edit' : 'add');?>" />
            
            <div class="control-group">
				<div class="controls">
                <button class="btn btn-primary " type="submit" name="submit" value="Go" tabindex="7" onclick="return valid_form()">Submit</button>
                <input type="button" class="btn btn-default" name="reset_form" value="Clear Form" onclick="this.form.reset();">
                                                    
				</div>
			</div>
        </form>
        
    </div>
	</div>
						
	</div><!--/.content-->
</div>

<!-- END PAGE CONTENT WRAPPER -->  

