<?php $all_array = all_arrays(); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="application/javascript">
function getLastWeek(d){
    var today = new Date();
    var lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - d);
    return lastWeek ;
}

//var lastWeekMonth = lastWeek.getMonth() + 1;
//var lastWeekDay = lastWeek.getDate();
//var lastWeekYear = lastWeek.getFullYear();

$(document).ready(function(e) {
	$("#date_type").change(function(e) {        
		var d_ty_change = $("#date_type").val();
		var currentdate = new Date(); 
		var date_v = currentdate.getDate() > 9 ? currentdate.getDate() : "0"+currentdate.getDate();
		

		if( d_ty_change == 1){
			var datetime =  currentdate.getFullYear()  + "-" + (currentdate.getMonth()+1) + "-" + date_v ;
			$("#from_date").val(datetime);
			$("#to_date").val(datetime);
			
		}else if(d_ty_change == 2){
			var yest = getLastWeek(1).getDate();
			var yest_month = getLastWeek(1).getMonth()+1 ;
			var yest_1 = yest > 9 ? yest : "0"+yest;
			var yest_month_1 = yest_month > 9  ? yest_month : "0"+yest_month;
			var datetime =  currentdate.getFullYear()  + "-" + yest_month_1 + "-" + yest_1 ;
			
			$("#from_date").val(datetime);
			$("#to_date").val(datetime);
			
		}else if(d_ty_change == 3){
			var dt_7 = getLastWeek(7).getDate();
			var month_7 = getLastWeek(7).getMonth() + 1
			var date_7 = dt_7 > 9 ? dt_7 : "0"+dt_7;
			var month_7 = month_7 > 9 ? month_7 : "0"+month_7;
			
			var datetime1 =  currentdate.getFullYear()  + "-" + ((currentdate.getMonth()+1) > 9 ? (currentdate.getMonth()+1) : "0"+(currentdate.getMonth()+1)) + "-" + date_v ;
			var datetime2 =  currentdate.getFullYear()  + "-" + month_7 + "-" + date_7 ;
			$("#from_date").val(datetime2);
			$("#to_date").val(datetime1);
			
		}else if(d_ty_change == 4){
			
			var datetime1 =  currentdate.getFullYear()  + "-" + ((currentdate.getMonth()+1) > 9 ? (currentdate.getMonth()+1) : "0"+(currentdate.getMonth()+1)) + "-" + date_v ;
			var datetime2 =  currentdate.getFullYear()  + "-" + ((currentdate.getMonth()+1) > 9 ? (currentdate.getMonth()+1) : "0"+(currentdate.getMonth()+1)) + "-" + "01";
			$("#from_date").val(datetime2);
			$("#to_date").val(datetime1);
		}else if(d_ty_change == 5){
			
			var datetime1 =  currentdate.getFullYear()  + "-" + (((currentdate.getMonth()+1) -1) > 9 ? ((currentdate.getMonth()+1) -1) : "0"+((currentdate.getMonth()+1) -1))  + "-" + "01" ;
			var datetime2 =  currentdate.getFullYear()  + "-" + (((currentdate.getMonth()+1) -1) > 9 ? ((currentdate.getMonth()+1) -1) : "0"+((currentdate.getMonth()+1) -1)) + "-" + 30;
			$("#from_date").val(datetime1);
			$("#to_date").val(datetime2);
			
		}else if(d_ty_change == 6){
			var datetime1 =  currentdate.getFullYear()  + "-" + ((currentdate.getMonth()+1) > 9 ? (currentdate.getMonth()+1) : "0"+(currentdate.getMonth()+1)) + "-" + date_v ;
			var datetime2 =  currentdate.getFullYear()  + "-" + (((currentdate.getMonth()+1) - 6) > 9 ? ((currentdate.getMonth()+1) - 6) : "0"+((currentdate.getMonth()+1) - 6)) + "-" + date_v;
			$("#from_date").val(datetime2);
			$("#to_date").val(datetime1);
			
		}else if(d_ty_change == 7){
			var datetime1 =  currentdate.getFullYear()  + "-" + ((currentdate.getMonth()+1) > 9 ? (currentdate.getMonth()+1) : "0"+(currentdate.getMonth()+1)) + "-" + date_v;
			var datetime2 =  "2015-10-01";
			$("#from_date").val(datetime2);
			$("#to_date").val(datetime1);
		}
		
	});    	 
});


$(document).ready(function(e) {
	$("#from_date").change(function(e) { 
	$('#date_type option[value=8]').attr('selected','selected');
	});
});


 function checkInputs(){ 
    var regex   = /^[0-9]*$/;
	var date_type  = document.getElementById("date_type").value;
	var from_date  = document.getElementById("from_date").value;
	var to_date    = document.getElementById("to_date").value;
	
	if(date_type.trim() == ""){
		alert("Select Date option");
		document.getElementById("date_type").focus();
		return false;
	}else{
	   	if(date_type == 8){
			if(from_date.trim() == ""){
				alert("Select From date");
				document.getElementById("from_date").focus();
				return false;
	        }
			
			if(to_date.trim() == ""){
				alert("Select To date");
				document.getElementById("to_date").focus();
				return false;
	        }
			
		}
	}
}

 </script>
 


<?php $all_arr = all_arrays(); ?>
<div class="span9">
<div class="content">

    <div class="module">
        <div class="module-head">
            <h3>Insurance Users Details</h3>	
        </div>
        <div class="module-body">
            
            <!--<div class="module-body">
            <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';

				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
			?>
            
                               
            </div>-->
            
            <form class="navbar-search pull-left input-append" style="width:100%" action="<?php echo FULL_CMS_URL?>/user_details_insurance" method="post">
            
            <label style="float:left; margin-right:10px">Date <font color="#FF0000" size="3">*</font>:</label>
             <select name="date_type" id="date_type" tabindex="1">
             <option value="">Select Date</option>
				<?php foreach($all_arr['DATE_TYPE'] as $k=>$v){
                    echo "<option value='".$k."' ".($k == $date_type ? 'selected' : '')." > ".$v." </option>";
                }?>
            </select>
             
            <input type="date" name="from_date" id="from_date" tabindex="2" value="<?php echo $from_date != "" ? $from_date : NULL ?>"/>
            <input type="date" name="to_date" id="to_date" tabindex="3" value="<?php echo $to_date != "" ? $to_date : NULL ?>"/><br/><br/>
             <!--          
             <select name="otp_optin" id="otp_optin" style="width:186px;" tabindex="4">   
                <option value="">Select</option>
                <option value=1>Optin</option>"
				<option value=0>Non-Optin</option>"
				
            </select> 
            
            <select name="clients" id="clients" style="width:210px;" tabindex="5">
                <option value="">Select Plan</option> 
                <?php foreach($all_arr['MEDLIFE_TEST'] as $k=>$v){
                         echo "<option value='".$k."' >".$v."</option>";
				}?>
            </select>
             -->
			 
			<select name="utm_source" id="utm_source" style="width:210px;" tabindex="6">
                <option value="">Select UTM Source</option> 
                <?php 
					if($this->session->userdata('admin_role_id') == 3){
						
					}else if($this->session->userdata('admin_role_id') == 4){
						
					}else{	
					
						foreach ($source_data->result() as $row) {
							echo "<option value='".$row->utm_source."' ".($utm_source == $row->utm_source ? 'selected' : '')."> ".$row->utm_source." </option>";
						
						}
					}
				?>
            </select> 
			 <select name="lp_name" id="lp_name" style="width:210px;" tabindex="6">
                <option value="">Select LP </option> 
                <?php foreach($all_array['INSURANCE_CLIENTS'] as $k=>$v){ ?>
						<option value="<?php echo $k;?>" <?php echo $k == $lp_name ? 'selected' : '' ; ?> ><?php echo $v;?></option>
				<?php }?>
            </select> 
             <button class="btn" id="btn_search" name="btn_search" type="submit" onclick="return checkInputs()" value="GO" tabindex="6">
            <i class="icon-search"></i>
            </button>

            <?php 
            if($this->session->userdata('admin_role_id') == 1){ ?>

				<button class="btn" style="margin-left:10px;" type="submit" id="dwn-purva" name="dwn-purva" formaction="<?php echo FULL_CMS_URL?>/download/insurance"><i class="icon-download" style="font-size: 20px;"></i></button>
			
			<?php } ?>

            </form>
           
            <br />
			<span style=" width: 100px; height: 31px; color: #000;float:right;">Count - <?php echo ($today_count != "" ? $today_count : '0')?></span>
            <br/>

            <?php 
            if($this->session->userdata('admin_role_id') != 10){ ?>

            <table class="table table-bordered">
              <thead>
                <tr>
                <th >Sl No</th>
				<th >Name</th>
                <th >Country</th>
                <th >Phone</th>              
				<th >UTM Source</th>
				<th >UTM Campaign</th>
				<th >LP Name</th>
				<th >Date</th>
                </tr>
              </thead>
              <tbody>
              <?php
			 
			  
                if(is_array($details) && count($details) > 0){	
				   $sl_no=1;
				   $i=1;
				   foreach($details as $p){
					    
				        echo "<tr>";
						echo "<td>" .$sl_no. "</td>";
						echo "<td>" .$p->name. "</td>";
						echo "<td>" .$p->country. "</td>";
						echo "<td>" .$p->phone. "</td>";
						echo "<td>" .$p->utm_source. "</td>";
						echo "<td>" .$p->utm_campaign. "</td>";
						echo "<td>" .$all_array['INSURANCE_CLIENTS'][$p->client_name]. "</td>";
						echo "<td>" .$p->date_created. "</td>";
						echo "</tr>"; 
						$sl_no++;
						$i++;
						
                   }
				    
					
                }
                ?>
               
                 <tr>
                  <td colspan="8" style="line-height: 1.5em; !important">
                    <?php
                      if (count($details) > 0) {
                         echo $links;
                      }
                    ?>
                  </td>
                </tr>
              </tbody>
            </table>
        	<?php } ?>

            <br>
        </div>
    </div>

    <!--/.module-->
<br />    
</div><!--/.content-->
</div>

