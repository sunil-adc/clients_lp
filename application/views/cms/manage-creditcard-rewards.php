<script>
function valid_form(){
	
	var r_= new Array("rewards", "status");
	
	for(var i=0; i < r_.length; i++   ){
		if($.trim($("#"+r_[i]).val()) == "" || $("#"+r_[i]).val() == 0 || $("#"+r_[i]).val() == "undefined"){
		   $("#"+r_[i]+"_err").html($("#"+r_[i]).attr("data-attr"));
		   $("#"+r_[i]).focus();
		   return false;	
		}else{
		   $("#"+r_[i]+"_err").html("");
		}
	}
}
</script>

<?php $arr_all = all_arrays(); 
/*foreach($get_product as $row){
	$prod_arr[$row['prod_id']] = $row['product_name']; 
	
}*/
?>

<!-- PAGE CONTENT WRAPPER -->
<div class="span9">
<div class="content">

    <div class="module">
        <div class="module-head">
            <h3>Card Rewards</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo FULL_CMS_URL?>/manage_cc/add_form">Card Reward List</a></h3>	
        </div>
        <div class="module-body">
        
 
        <form  class="form-horizontal row-fluid" action="<?php echo $form_submit?>" method="post" enctype="multipart/form-data" >
        <div class="panel panel-default hidden_dev" id="k_f_form">
            <div class="panel-heading">
                <h3 class="panel-title"><strong><?php echo "Add Card Rewards";//$prod_arr[$prod_id]. " rewards"//$page_title ?></h3>
                
            </div>
            
                <div class="control-group">
					<label class="control-label" for="basicinput"> rewards * </label>
					<div class="controls">
						
						<input type="text" class="span8" name="rewards" id="rewards" tabindex="1" data-attr="Please enter rewards"/>
                        <input type="hidden" name="prod_id" id="prod_id"  value="<?php echo $prod_id?>" />                
					</div>                                            
				   <span class="help-block" id="min_amt_err"></span>
				</div>
                
                
                <div class="control-group">
					<label class="control-label" for="basicinput"> Status * </label>
					<div class="controls">
				     <select class="span8 select" name="status" id="status" tabindex="5" data-attr="Please select status">
                        <option value="" >Select status</option>
                        <option value="1" >Active</option>
                        <option value="0" >Inactive</option>
                        </select>
                     <span class="help-block" id="status_err"></span>
                    </div>
                </div>
                
            
            
            <input type="hidden" name="id" id="id" value="" />
            <input type="hidden" name="mode" id="mode" value="<?php echo 'add';?>" />
            
            <div class="control-group">
				<div class="controls">
				   <button class="btn" type="submit" name="submit" value="Go" tabindex="6" onclick="return valid_form()">Submit</button>
                <button class="btn ">Clear Form</button>                                    
				</div>
			</div>
        </form>   
        
		<br><br>
		
        <!-- START DATATABLE EXPORT -->
            <?php 
                    if( $this->session->flashdata('error') ) { 
                        echo '<div class="alert alert-danger" role="alert" style="height:39px; line-height:8px">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" >&times;</span></button>
                                '.$this->session->flashdata('error').'</div>';
                
                    }else if( $this->session->flashdata('success') ) { 
                       echo '<div class="alert alert-success" role="alert" style="height:39px; line-height:8px">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true" >&times;</span></button>
                                '.$this->session->flashdata('success').'</div>';
                
                    }
                  ?>
                
                    <table id="customers2" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>S No</th>
                                <th>Rewards</th>
                                <th>Status</th>
                                <th>Action</th>
                              </tr>
                        </thead>
                        <tbody>
                        <?php
                            if(is_array($details) && count($details) > 0){	
                              $sl_no=1;
                              foreach($details as $p){
                                  
                                    echo "<tr>";
									echo "<td>". $sl_no. "</td>";
                                    echo "<td>". $p->rewards. "</td>";	
									echo "<td><span class='label label-success'>".($p->status ==1 ? 'Active' : 'Inactive')."</span></td>";
                                    echo "<td><a href='".FULL_CMS_URL."/".$add_edit_page."/edit/".$p->id."' class='btn btn-default btn-rounded btn-sm'> <img alt='Edit' src='".S3_URL."/cms/images/icons/pencil.png'  width='20' height='20'></a></td>";
                                    echo "</tr>";
                                    $sl_no++;
                              }
                            }
                        ?>
                            
                        </tbody>
                    </table>                                    
                  
				   </div>
                </div>
                                
            </div><!--/.content-->
        </div>

<!-- END PAGE CONTENT WRAPPER -->


<script>
$(document).ready(function(e) {
    $("#add_k_f").click(function(e) {
    	$("#k_f_form").slideToggle(1000);
		//$("html, body").animate({ scrollTop: $(document).height() }, 1000);
	});
});
</script>
