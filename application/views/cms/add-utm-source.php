<?php $arr_all = all_arrays(); ?>
<script type="application/javascript">

 function checkInputs()
 { 
    var regex = /^[0-9]*$/;
    var utm_source      = document.getElementById("utm_source").value;
	var pixel           = document.getElementById("pixel").value;
    var fire_type       = document.getElementById("fire_type").value;
    var form_type       = document.getElementById("form_type").value;
    var opt_verify      = document.getElementById("opt_verify").value;


	if( utm_source== "" || utm_source.trim() ==""){
		alert("Utm source is required");
		document.getElementById("utm_source").focus();
		return false;
	}

    if( fire_type== "" || fire_type.trim() ==""){
        alert("Fire Type is required");
        document.getElementById("fire_type").focus();
        return false;
    }

    if( form_type == "" || form_type.trim() ==""){
        alert("Form Type is required");
        document.getElementById("form_type").focus();
        return false;
    }

    if( opt_verify == "" || opt_verify.trim() == "" ){
        alert("Otp Verify is required");
        document.getElementById("opt_verify").focus();
        return false;
    }
	
	if(pixel == "" || pixel.trim() ==""){
		alert("pixel is required");
		document.getElementById("pixel").focus();
		return false;
	}
		
		
 }

 </script>


<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Add Utm Source</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>cms/manage_utm_source">Manage Utm Source</a></h3>	
        </div>
        <div class="module-body">
                <?php 
					if( $this->session->flashdata('error') ) { 
					   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
							<strong>'.$this->session->flashdata('error').'</strong></div>';
				
					}else if( $this->session->flashdata('success') ) { 
					
					   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
						  <strong>'.$this->session->flashdata('success').'</strong></div>';
					}
				  ?>
                
                <br />                

                  <form class="form-horizontal row-fluid" method="post" action="<?php echo SITE_URL.'cms/manage_utm_source/add' ?>">
                    
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Utm Source *</label>
                            <div class="controls">
                                <input type="text" id="utm_source" name="utm_source" tabindex="1" placeholder="Utm source" class="span8">
                            </div>
                        </div>
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Network Pixel *</label>
                            <div class="controls">
                                <textarea name="pixel" id="pixel" placeholder="pixel" tabindex="2" class="span8"/></textarea>
                                <span class="help-inline">Replace dynamic user id with "{USERID}" & payout with "{PAYOUT}"</span>
                            </div>
                        </div>
                        
						<div class="control-group">
                            <label class="control-label" for="basicinput">Fire Type*</label>
                            <div class="controls">
                                <select name="fire_type" id="fire_type"  tabindex="7" class="span8">
                                    <option value="">Select Type</option>
                                <?php foreach ($arr_all['FIRE_LIMIT'] as $k => $v){
                                     echo "<option value='".$k ."'>".$v."</option>";
                                } ?>
                                </select>
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="basicinput">Form Type*</label>
                            <div class="controls">
                                <select name="form_type" id="form_type"  tabindex="7" class="span8">
                                    <option value="">Select Type</option>
									  <option value="1">Small Form</option>
									  <option value="2">Large Form</option>
                                </select>
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="basicinput">Otp Verification*</label>
                            <div class="controls">
                                <select name="opt_verify" id="opt_verify"  tabindex="7" class="span8">
                                    <option value="">Select Type</option>
									  <option value="1">Enable</option>
									  <option value="2">Disable</option>
                                </select>
                            </div>
                        </div>
						
						
                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()" tabindex="3">
                            </div>
                        </div>
                    </form>
                </div>
                </div>
                                
            </div><!--/.content-->
        </div>
