<?php $all_arr = all_arrays(); ?>
<script type="application/javascript">

 function checkInputs(){
	  
    var regex = /^[0-9]*$/;
	var caller_status  = document.getElementById("caller_status").value;
	var comment = document.getElementById("comment").value;
	
	if(caller_status == "" ){
		alert(" Caller status is required");
		document.getElementById("caller_status").focus();
		return false;
	}

	if( comment == "" || comment.trim() ==""){
		alert("Comment is required");
		document.getElementById("comment").focus();
		return false;
	}
	
			
}

 </script>
 
 <script>

$(document).ready(function(e) {
    $("#caller_status").on('change', function(e) {
       var t = $(this).val();
		if(t != "undifined" && t == 1){
		   $("#appointment_date").removeAttr("disabled");
		   $(".add-on").show();
		   if($("#appointment_date").val() == ""){
		       alert("Please select date ");
               $("#appointment_date").focus();
			   return false;			   
		   }
		}
    });
});
 </script>

<script type="text/javascript">
  $(function() {
    $('#datetimepicker1').datetimepicker({
      language: 'pt-BR'
    });
  });
</script>

<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Update details</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/user_details">Users list</a></h3>	
        </div>
        <div class="module-body">
                <?php 
					if( $this->session->flashdata('error') ) { 
					   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
							<strong>'.$this->session->flashdata('error').'</strong></div>';
				
					}else if( $this->session->flashdata('success') ) { 
					
					   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
						  <strong>'.$this->session->flashdata('success').'</strong></div>';
					}
				  ?>
                
                <br />                

                  <form class="form-horizontal row-fluid" method="post" action="<?php echo SITE_URL.'index.php/cms/user_details/edit' ?>" enctype="multipart/form-data">
                    <?php foreach($setting as $rows){?>
                    
                    <input type="hidden" id="id" name="id" value="<?php echo $rows->id ?>" />
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Name</label>
                            <div class="controls">
                              <input type="text" name="name" id="name" placeholder="name" tabindex="1" class="span8" value="<?php echo $rows->name ?>" readonly="readonly" />
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Phone</label>
                            <div class="controls">
                              <input type="text" name="phone" id="phone" placeholder="phone" tabindex="2" class="span8" value="<?php echo $rows->phone?>"   readonly="readonly"/>
                            </div>
                        </div>
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">City</label>
                            <div class="controls">
                               <input type="text" name="city" id="city" placeholder="City" tabindex="3" class="span8"  value="<?php echo ($rows->city != 0 ? $this->db_function->get_single_value(CITY, "City" ,"CityId =".$rows->city) : 'null') ?>" readonly="readonly"/>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Amount</label>
                            <div class="controls">
                              <input type="text" name="amount" id="amount" placeholder="amount" tabindex="4" class="span8" value="<?php echo ($rows->amount == 0 ? "" : $all_arr['LOAN_AMOUNT'][$rows->amount])?>"  readonly="readonly"/>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Company</label>
                            <div class="controls">
                              <input type="text" name="company" id="company" placeholder="company" tabindex="5" class="span8" value="<?php echo $rows->company_name?>"  readonly="readonly"/>
                            </div>
                        </div>
						
						<div class="control-group">
                            <label class="control-label" for="basicinput">Company Address</label>
                            <div class="controls">
                              <textarea name="company_address" id="company_address" placeholder="company address" tabindex="5" class="span8"><?php echo $rows->company_address?>  </textarea>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Year (experience)</label>
                            <div class="controls">
                              <input type="text" name="year_exp" id="year_exp" placeholder="Year of exp" tabindex="6" class="span8" value="<?php echo ($rows->year_exp == 0 ? "" : $all_arr['BUSINESS_EXP'][$rows->year_exp])?>"  readonly="readonly"/>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Turnover</label>
                            <div class="controls">
                              <input type="text" name="turnover" id="turnover" placeholder="turnover" tabindex="7" class="span8" value="<?php echo ($rows->turnover == 0 ? "" : $all_arr['TURNOVER'][$rows->turnover])?>"  readonly="readonly"/>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Bussiness category</label>
                            <div class="controls">
                              <input type="text" name="business_cat" id="business_cat" placeholder="business category" tabindex="8" class="span8" value="<?php echo ($rows->business_category == 0 ? "" : $all_arr['BUSSINESS_CAT'][$rows->business_category])?>"  readonly="readonly"/>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Caller status</label>
                            <div class="controls">
                            <select name="caller_status" id="caller_status" class="span8" tabindex="9">
                            <option value="">Select caller status</option> 
                            <?php foreach($all_arr['CALLER_STATUS'] as $k=>$v){
							  	echo "<option value='".$k."' ".($rows->caller_status == $k ? 'selected' : '')."> ".$v." </option>";
							}?>
                            </select>
                            </div>
                        </div>
                         
						 <div class="control-group">
                            <label class="control-label" for="basicinput">Appointment Date & Time</label>
                            <?php if($rows->caller_status == 1){ ?>
							
								<div class="controls " id="datetimepicker1">
								<input type="text" data-format="dd/MM/yyyy hh:mm:ss" name="appointment_date" id="appointment_date" class="span8" value="<?php echo $rows->appointment_date  ?>">
									<span class="add-on ">
										<i data-time-icon="icon-time" data-date-icon="icon-calendar">
									  </i>
									</span>
								</div>
							<?php }else{  ?>	
								
							<div class="controls " id="datetimepicker1">
								<input type="text" data-format="dd/MM/yyyy hh:mm:ss" name="appointment_date" id="appointment_date" class="span8" disabled value="<?php echo $rows->appointment_date  ?>">
									<span class="add-on hide">
										<i data-time-icon="icon-time" data-date-icon="icon-calendar">
									  </i>
									</span>
								</div>
								
							<?php } ?>
                        </div>
						
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Comment</label>
                            <div class="controls">
                            <textarea name="comment" id="comment" tabindex="10" class="span8"><?php echo $rows->comment?></textarea>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()" tabindex="11">
                            </div>
                        </div>
                    </form>
                    <?php } ?>
                </div>
                </div>
                                
            </div><!--/.content-->
        </div>
