<link href="<?php echo SITE_URL?>cdn/cms/css/dashboard-bootstrap.min.css" rel="stylesheet">

<?php $all_arr = all_arrays(); ?>
<script>

function getLastWeek(d){
    var today = new Date();
    var lastWeek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - d);
    return lastWeek ;
}

//var lastWeekMonth = lastWeek.getMonth() + 1;
//var lastWeekDay = lastWeek.getDate();
//var lastWeekYear = lastWeek.getFullYear();

$(document).ready(function(e) {
	$("#date_type").change(function(e) {        
		var d_ty_change = $("#date_type").val();
		var currentdate = new Date(); 
		var date_v = currentdate.getDate() > 9 ? currentdate.getDate() : "0"+currentdate.getDate();
		

		if( d_ty_change == 1){
			var datetime =  currentdate.getFullYear()  + "-" + (currentdate.getMonth()+1) + "-" + date_v ;
			$("#from_date").val(datetime);
			$("#to_date").val(datetime);
			
		}else if(d_ty_change == 2){
			var yest = getLastWeek(1).getDate();
			var yest_month = getLastWeek(1).getMonth()+1 ;
			var yest_1 = yest > 9 ? yest : "0"+yest;
			var yest_month_1 = yest_month > 9  ? yest_month : "0"+yest_month;
			var datetime =  currentdate.getFullYear()  + "-" + yest_month_1 + "-" + yest_1 ;
			
			$("#from_date").val(datetime);
			$("#to_date").val(datetime);
			
		}else if(d_ty_change == 3){
			var dt_7 = getLastWeek(7).getDate();
			var month_7 = getLastWeek(7).getMonth() + 1
			var date_7 = dt_7 > 9 ? dt_7 : "0"+dt_7;
			var month_7 = month_7 > 9 ? month_7 : "0"+month_7;
			
			var datetime1 =  currentdate.getFullYear()  + "-" + ((currentdate.getMonth()+1) > 9 ? (currentdate.getMonth()+1) : "0"+(currentdate.getMonth()+1)) + "-" + date_v ;
			var datetime2 =  currentdate.getFullYear()  + "-" + month_7 + "-" + date_7 ;
			$("#from_date").val(datetime2);
			$("#to_date").val(datetime1);
			
		}else if(d_ty_change == 4){
			
			var datetime1 =  currentdate.getFullYear()  + "-" + ((currentdate.getMonth()+1) > 9 ? (currentdate.getMonth()+1) : "0"+(currentdate.getMonth()+1)) + "-" + date_v ;
			var datetime2 =  currentdate.getFullYear()  + "-" + ((currentdate.getMonth()+1) > 9 ? (currentdate.getMonth()+1) : "0"+(currentdate.getMonth()+1)) + "-" + "01";
			$("#from_date").val(datetime2);
			$("#to_date").val(datetime1);
		}else if(d_ty_change == 5){
			
			var datetime1 =  currentdate.getFullYear()  + "-" + (((currentdate.getMonth()+1) -1) > 9 ? ((currentdate.getMonth()+1) -1) : "0"+((currentdate.getMonth()+1) -1))  + "-" + "01" ;
			var datetime2 =  currentdate.getFullYear()  + "-" + (((currentdate.getMonth()+1) -1) > 9 ? ((currentdate.getMonth()+1) -1) : "0"+((currentdate.getMonth()+1) -1)) + "-" + 30;
			$("#from_date").val(datetime1);
			$("#to_date").val(datetime2);
			
		}else if(d_ty_change == 6){
			var datetime1 =  currentdate.getFullYear()  + "-" + ((currentdate.getMonth()+1) > 9 ? (currentdate.getMonth()+1) : "0"+(currentdate.getMonth()+1)) + "-" + date_v ;
			var datetime2 =  currentdate.getFullYear()  + "-" + (((currentdate.getMonth()+1) - 6) > 9 ? ((currentdate.getMonth()+1) - 6) : "0"+((currentdate.getMonth()+1) - 6)) + "-" + date_v;
			$("#from_date").val(datetime2);
			$("#to_date").val(datetime1);
			
		}else if(d_ty_change == 7){
			var datetime1 =  currentdate.getFullYear()  + "-" + ((currentdate.getMonth()+1) > 9 ? (currentdate.getMonth()+1) : "0"+(currentdate.getMonth()+1)) + "-" + date_v;
			var datetime2 =  "2015-10-01";
			$("#from_date").val(datetime2);
			$("#to_date").val(datetime1);
		}
		
	});    	 
});


$(document).ready(function(e) {
	$("#from_date").change(function(e) { 
	$('#date_type option[value=8]').attr('selected','selected');
	});
});

 function checkInputs(){ 
    var regex   = /^[0-9]*$/;
	var date_type  = document.getElementById("date_type").value;
	var from_date  = document.getElementById("from_date").value;
	var to_date    = document.getElementById("to_date").value;
	
	if(date_type.trim() == ""){
		alert("Select date type");
		document.getElementById("date_type").focus();
		return false;
	}else{
	   	if(date_type == 8){
			if(from_date.trim() == ""){
				alert("Select From date");
				document.getElementById("from_date").focus();
				return false;
	        }
			
			if(to_date.trim() == ""){
				alert("Select To date");
				document.getElementById("to_date").focus();
				return false;
	        }
			
		}
	}
}

</script>

<div class="span-9"> 
        <div class="content">
            <div class="btn-controls">
                <div class="btn-box-row row-fluid">
                    <div class="btn-box-row row-fluid">
                      
                      <form  class="input-append" action="<?php echo FULL_CMS_URL?>/dashboard" method="post">
						<div class="row">
                        <div href="#" class="col-md-3">						
							<select name="date_type" id="date_type" tabindex="1" class="form-control" style="    margin-bottom: 10px;width:100%" >
							  <option value="">Select Date </option>
								<?php foreach($all_arr['DATE_TYPE'] as $k=>$v){
									echo "<option value='".$k."' ".($k == $dt_type ? 'selected' : '')." > ".$v." </option>";
								}?>
							</select>
						</div>
                        
						<div href="#" class="col-md-3">
							<input type="date" style="padding:19px; width:100%; margin-bottom: 10px;" name="from_date" class="form-control" id="from_date" tabindex="2" value="<?php echo $from_date != "" ? $from_date : NULL ?>"/>
						</div>
						
						<div href="#" class="col-md-3">
							<input type="date"style="padding:19px; width:100%;    margin-bottom: 10px;" name="to_date" class="form-control"  id="to_date" tabindex="3" value="<?php echo $to_date != "" ? $to_date : NULL ?>"/>
                        </div>
						
						<div href="#" class="col-md-3">
							<button class="btn" id="btn_search" name="btn_search" type="submit" value="GO" tabindex="4" onclick="return checkInputs()" style="margin-right: 22px;" title="Search">
							<i class="icon-search" style="font-size: 20px;"></i>
							</button>
							<?php
							if($this->session->userdata('admin_role_id') == 3){
							?>
							<button class="btn" id="btn_search" name="btn_search" type="submit" value="GO" tabindex="4" onclick="return checkInputs()" title="Download" formaction="<?php echo FULL_CMS_URL?>/download/client/2">
							<i class="icon-download" style="font-size: 20px;"></i>
							</button>
							<?php } 
							if($this->session->userdata('admin_role_id') == 4){
							?>
							<button class="btn" id="btn_search" name="btn_search" type="submit" value="GO" tabindex="4" onclick="return checkInputs()" title="Download" formaction="<?php echo FULL_CMS_URL?>/download/client/3">
							<i class="icon-download" style="font-size: 20px;"></i>
							</button>
							<?php } 

							if($this->session->userdata('uid') == 11){
							?>
							<button class="btn" id="btn_search" name="btn_search" type="submit" value="GO" tabindex="4" onclick="return checkInputs()" title="Download" formaction="<?php echo FULL_CMS_URL?>/download/medlife_publisher"> 
							<i class="icon-download" style="font-size: 20px;"></i>
							</button>
							<?php } ?>	

							?>	
                        
                        </div>
						</div>
						</form>
                    </div>
                </div>
                </div>
                
                
                
                <?php

					if($this->session->userdata('admin_role_id') == 9){ 

						if($this->session->userdata('uid') == 11){ ?>
	                    	<div class="btn-box-row row-fluid">

							<a href="#"  class="btn-box big span4"><i class="icon-user"></i><b><?php echo $tot_med_pubwise ?></b>
		                    <p class="text-muted">Medlife</p>
		                    </a>
		                    </div>
						
						<?php 
						}else{
	                    ?>
	                    	<div class="btn-box-row row-fluid">

							<a href="#"  class="btn-box big span4"><i class="icon-user"></i><b><?php echo $total_med_count ?></b>
		                    <p class="text-muted">Medlife</p>

		                    </a>
							</div>

					<?php
						} 
					}else if($this->session->userdata('admin_role_id') == 1 || $this->session->userdata('admin_role_id') == 10 ){ ?>
                    
					<div class="btn-box-row row-fluid">

                    <a href="<?php echo FULL_CMS_URL?>/user_details_chaipoint"  class="btn-box big span4"><i class="icon-user"></i><b><?php echo $total_chaipoint_count?></b>
                    <p class="text-muted">Chaipoint</p>
                    </a>

                    
					<a href="<?php echo FULL_CMS_URL?>/user_details_medlife"  class="btn-box big span4" >MEDLIFE <b><?php echo $total_med_count?></b>
                    <p class="text-muted" style="font-size: 13px;">SF Optin - <?php echo $small_form_optin ?>  SF NON - <?php echo $small_form_nonoptin ?></p>
                    <p class="text-muted" style="font-size: 13px;">BF Optin - <?php echo $big_form_optin ?> BF NON - <?php echo $big_form_nonoptin ?></p>
                    </a>
                    
                    <a href="<?php echo FULL_CMS_URL?>/user_details_purvankara"  class="btn-box big span4"><i class="icon-user"></i><b><?php echo $total_purva_count?></b>
                    <p class="text-muted">Purvankara</p>
                    </a>
                	</div>
                    
                    <div class="btn-box-row row-fluid">

                    <a href="<?php echo FULL_CMS_URL?>/user_details_lenskart"  class="btn-box big span4"><i class="icon-user"></i><b><?php echo $total_lenskart_count?></b>
                    	<p class="text-muted">Lenskart</p>
                    </a>

                    <a href="<?php echo FULL_CMS_URL?>/user_details_religare"  class="btn-box big span4"><i class="icon-user"></i><b><?php echo $total_religare_health_count?></b>
                    	<p class="text-muted">Religare Health</p>
                    </a>

                    <a href="<?php echo FULL_CMS_URL?>/user_details_nmims"  class="btn-box big span4"><i class="icon-user"></i><b><?php echo $total_nmims_count?></b>
                    	<p class="text-muted">Nmims</p>
                    </a>

                	</div>

                	<div class="btn-box-row row-fluid">
                		<a href="<?php echo FULL_CMS_URL?>/user_details_insurance"  class="btn-box big span4"><i class="icon-user"></i><b><?php echo $total_maxlife_count?></b>
                    	<p class="text-muted">MaxLife</p>
                    	</a>

                    	<a href="<?php echo FULL_CMS_URL?>/user_details_mfine"  class="btn-box big span4"><i class="icon-user"></i><b><?php echo $total_mfine_count?></b>
                    	<p class="text-muted">Mfine</p>
                    	</a>

                	</div>	
					
				  <?php
					}
					 
				  ?>   
                   
                
            </div>
            
            
            
            
            
        </div>
   <!--/.content-->
   </div>