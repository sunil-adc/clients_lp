<?php $arr_all = all_arrays(); ?>
<script type="application/javascript">

function checkInputs(){
    
	var regex = /^[0-9]*$/;
	var tag_line  = document.getElementById("tag_line").value;
	var offers_name = document.getElementById("offers_name").value;
	var campaign_name = document.getElementById("campaign_name").value;
	var bank_logo  = document.getElementById("bank_logo").value;
	var offer_image  = document.getElementById("offer_image").value;
	var offers_url  = document.getElementById("offers_url").value;
	
		
	if(tag_line == "" || tag_line.trim() ==""){
		alert(" Tag is required");
		document.getElementById("tag_line").focus();
		return false;
	}
	
	if(offers_name == "" || offers_name.trim() ==""){
		alert(" Category is required");
		document.getElementById("offers_name").focus();
		return false;
	}
	
	if(campaign_name == "" || campaign_name.trim() ==""){
		alert(" Campaign Name is required");
		document.getElementById("campaign_name").focus();
		return false;
	}
	
	if( offers_url== "" || offers_url.trim() ==""){
		alert("Url is required");
		document.getElementById("offers_url").focus();
		return false;
	}
		
}
 </script>


<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Update banner</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_offers">Manage Banner </a></h3>	
        </div>
        <div class="module-body">
               <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';
			
				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
			  ?>
                
                <br />                
                  <?php foreach($setting as $n){ ?>
                  <form class="form-horizontal row-fluid" method="post" action="<?php echo FULL_CMS_URL.'/manage_offers/edit' ?>" enctype="multipart/form-data">
                    <input type="hidden" name="id" id="id" value="<?php echo $n->id;?>"/>
                        
                     <div class="control-group">
                            <label class="control-label" for="basicinput">Tag line</label>
                            <div class="controls">
                               <input type="text" name="tag_line" id="tag_line" placeholder="Tag Line" tabindex="1" class="span8" value="<?php echo $n->tag_line?>" />
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput"> Category </label>
                            <div class="controls">
                                <select id="offers_name" name="offers_name" tabindex="2" data-placeholder="Offers name" class="span8">
                                   <option value="">Select Category Types</option>
                                   <?php foreach($arr_all['BANK_OFFERS'] as $k=>$v){
									     echo "<option value='$k' ".($k == $n->offers_name ? 'selected' : '')." >$v</option>";
								   }?>
                                </select>
                            </div>
                        </div>
                        
						<div class="control-group">
                            <label class="control-label" for="basicinput">Campaign Name *</label>
                            <div class="controls">
                               <input type="text" name="campaign_name" id="campaign_name" placeholder="Campaign Name" tabindex="3" class="span8" value="<?php echo $n->campaign_name?>" />
                            </div>
                        </div>
						
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Brand Logo</label>
                            <div class="controls">
                               <input type="file" name="bank_logo" id="bank_logo" placeholder="Bank Logo" tabindex="3" class="span8"  />
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Brand Logo</label>
                            <div class="controls">
                                <img src="<?php echo SITE_URL.BANK_BANNERS.$n->bank_logo;  ?>" width="150" height="40">
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">offer Image</label>
                            <div class="controls">
                               <input type="file" name="offer_image" id="offer_image" placeholder="Offer Image" tabindex="4" class="span8"  />
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">offer Image</label>
                            <div class="controls">
                                <img src="<?php echo SITE_URL.BANK_BANNERS.$n->offer_image;  ?>" width="150" height="40">
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Offers Url</label>
                            <div class="controls">
                               <textarea name="offers_url" id="offers_url" placeholder="offers url" tabindex="5" class="span8" rows="3"/><?php echo $n->offers_url?></textarea>
                            </div>
                        </div>
                        
                      <div class="control-group">
                            <label class="control-label" for="basicinput">Status</label>
                            <div class="controls">
                               <select tabindex="5" id="status" name="status" data-placeholder="Select Status.." class="span8">
                               <option value="" <?php echo ($n->status == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                               <option value="1" <?php echo ($n->status == '1') ? 'selected="selectec"' : '';?>>Active</option>
                               <option value="0" <?php echo ($n->status == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
                                </select>
                            </div>
                        </div>
                        
                     <div class="control-group">
                        <div class="controls">
                            <input type="submit" name="addform" value="Save" onclick="return checkInputs()">
                        </div>
                    </div>
                 </form>
               <?php } ?>   
              </div>
            </div>                                
         </div><!--/.content-->
       </div>

