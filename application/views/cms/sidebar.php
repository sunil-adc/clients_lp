
<div class="span3">
  <div class="sidebar">
   <ul class="widget widget-menu unstyled">
      <li><a href="<?php echo FULL_CMS_URL;?>/dashboard"><i class="menu-icon icon-signout"></i>Dashboard</a></li>
    </ul>
    
   
<?php

if($this->session->userdata('admin_role_id') != 9){




$cur_sub_menu = NULL;
$cur_menu = NULL;
$main_menu_permitted = NULL;

//$main_menu_permitted = true;

// CONVERT CUR CONTROLLER NAME INTO SMALL FOR CONDITION CHECKING
$cur_controller = strtolower($cur_controller);
// CONVERT CUR CONTROLLER NAME INTO SMALL FOR CONDITION CHECKING

// CREATE PERMISSION ARRAY FOR EACH AND EVERY CONTROLLER
$arr_permitted_controller = create_permission_array($this->session->userdata('admin_role_details'));


if( is_array ($menu)){
		$i = 1;
		$j = 0;
		
		foreach($menu as $k => $v) {
			echo '<ul class="widget widget-menu unstyled">';
			// CHECK FOR MAIN MENU SELECTION
			if( is_array ($v[2]) && count($v[2]) > 0){
				for ($j = 0; $j < count($v[2]); $j++) {
					if (in_array ($cur_controller, array_lower($v[2][$j]))) {
						$cur_menu = "active";
					}
				}
			}
			
			// CHECK FOR PERMISSION FOR PARENT MENU 
			if( is_array ($arr_permitted_controller) && count($arr_permitted_controller) > 0){
				if( is_array ($v[2]) && count($v[2]) > 0){
					for ($j = 0; $j < count($v[2]); $j++) {
						if (in_array ($v[2][$j][1], $arr_permitted_controller) || in_array ($v[2][$j][2], $arr_permitted_controller)) {
							$main_menu_permitted = true;
						}
					}
				}		
			}
			
			// IF MAIN MENU IS PERMITTED THEN START THE UI
			//echo ($main_menu_permitted == true) ? '<div class="popmenu '.$v[4].'">'.$v[0].'</div>' : '';
			echo ($main_menu_permitted == true) ? '<li><a class="collapsed menu-toggle" data-toggle="collapse" data-id="'.$i.'" href="#"><i class="menu-icon icon-cog"></i><i class="icon-chevron-down pull-right"></i><i class="icon-chevron-up pull-right"></i>' .$v[0].' </a>':'';
								
			if( is_array ($v[2]) && count($v[2]) > 0){
				echo '<ul id="togglePages'.$i.'" class="collapse unstyled">';
				foreach($v[2] as $k1 => $v1) {
					// IF USER IS PERMITTED TO VIEW THAT PAGE THEN AND THEN ONLY DISPLAY THAT LINK
					if(in_array(strtolower($v1[1]), $arr_permitted_controller)) {
						$cur_sub_menu = (($cur_controller == strtolower($v1[1])) || ($cur_controller == strtolower($v1[2]))) ? 'class="active"' : '';
						echo '<li><a '.$cur_sub_menu.' href="'.FULL_CMS_URL."/".$v1[1].'"><i class="icon-inbox"></i>'.$v1[0].'</a></li>';
					}
				}
				
				// IF MAIN MENU IS PERMITTED THEN CLOSE THE UI
				echo ($main_menu_permitted == true) ? '</ul>' : '';
				echo '</li>';
				
			}
			echo '</ul>';
			$i++;
			$cur_menu = "";
			$main_menu_permitted = "";
		}
	}
}

?>
     <ul class="widget widget-menu unstyled">
      <li><a href="<?php echo FULL_CMS_URL;?>/logout_user/logout"><i class="menu-icon icon-signout"></i>Logout </a></li>
    </ul>
    
 </div>
</div>


<!--
<div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <li class="active">
                <a href="<?php echo PANEL_URL?>"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
            </li>
            
            <li>
                <a href="<?php echo PANEL_URL?>manageclient"><i class="fa fa-fw fa-table"></i> Manage Client</a>
            </li>
            
            <li>
                <a href="<?php echo PANEL_URL?>manageVolume"><i class="fa fa-fw fa-table"></i>Volumes</a>
            </li>
            
            <li>
                <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-table"></i>Reports <i class="fa fa-fw fa-caret-down"></i></a>
                <ul id="demo" class="collapse">
                    <li>
                        <a href="<?php echo PANEL_URL?>reportCampaignwise" >Volume Report</a>
                    </li>
                </ul>
            </li>
           
            <?php if($this->session->userdata('admin_role_id') == 1 ) {?>
            
            <li>
                <a href="<?php echo PANEL_URL?>manageAdminRole"><i class="fa fa-fw fa-table"></i>Manage Roles</a>
            </li>
            <li>
                <a href="<?php echo PANEL_URL?>manageUser"><i class="fa fa-fw fa-table"></i>Manage Users</a>
            </li>
            <?php } ?>
        </ul>
    </div>
    

-->