<?php $arr_all = all_arrays(); 
?>
<div class="span9">
<div class="content">

    <div class="module">
        <div class="module-head">
            <h3>Bank List</h3>
        
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo FULL_CMS_URL."/".$add_edit_page?>">Add Bank</a></h3>
		</div>	
        <div class="module-body">
            <!--<p>
                <strong>Bank list </strong>
            </p>-->
            <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';
			
				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
			  ?>

            

            <!--<form class="navbar-search pull-left input-append" action="<?php echo FULL_CMS_URL?>/manage_banklist" method="post">
            <select name="search_field" id="search_field" >
                <option value="">Select option</option>                               
                <option value="email" <?php echo ($search_field == "email")       ? "selected" : ""; ?> >Email</option>
                <option value="username" <?php echo ($search_field == "username") ? "selected" : ""; ?> >Username</option>                
            </select>
            &nbsp;&nbsp;
            
            <input type="text" name="search_txt" id="search_txt"  value="<?php echo ($search_txt != "" ? $search_txt : '')?>" />
             
            <button class="btn" id="btn_search" name="btn_search" type="submit" value="GO">
            <i class="icon-search"></i>
            </button>
            </form>-->
            <br />
            <!-- <hr /> -->
            <br />


			<table id="customers2" class="table datatable">
                        <thead>
                            <tr>
                                <th>S no</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
						    if(is_array($details) && count($details) > 0){	
                              $sl_no=1;
                              foreach($details as $p){
                                    $status_css  = $p->status ==1 ? 'label-success' : 'label-warning';
                                    echo "<tr>";
                                    echo "<td>". $sl_no. "</td>";
                                    echo "<td>". ucfirst($p->bank_name). "</td>";
                                    echo "<td><span class='label ".$status_css."'>".($p->status ==1 ? 'Active' : 'Inactive')."</span></td>";
									echo "<td><a href='".FULL_CMS_URL."/".$add_edit_page."/edit/".$p->id."' class='btn btn-default btn-rounded btn-sm'><img alt='Edit' src='".S3_URL."/cms/images/icons/pencil.png' width='20' height='20'></a></td>";
                                    echo "</tr>";
                                    $sl_no++;
                              }
                            }
                        ?>
                            
                        </tbody>
                    </table>                                    
               
			
			<br>
        </div>
    </div>

    <!--/.module-->
<br />    
</div><!--/.content-->
</div>




	
			   
<!-- END PAGE CONTENT WRAPPER -->
