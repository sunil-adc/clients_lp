<?php $arr_all = all_arrays(); ?>
<script type="application/javascript">

 function checkInputs(){
	  
    var regex = /^[0-9]*$/;
	var role  = document.getElementById("role").value;
	var username = document.getElementById("username").value;
	var email  = document.getElementById("email").value;
	var password  = document.getElementById("password").value;	
		
	if(role == "" || role.trim() ==""){
		alert(" role is required");
		document.getElementById("role").focus();
		return false;
	}
	
	if(username == "" || username.trim() ==""){
		alert(" username is required");
		document.getElementById("username").focus();
		return false;
	}

	if( email== "" || email.trim() ==""){
		alert("email is required");
		document.getElementById("email").focus();
		return false;
	}
	
	
	/*if( password == "" || password.trim() ==""){
		alert("Password is required");
		document.getElementById("password").focus();
		return false;
	}*/
			
}

$(document).ready(function(e) {
	$("#pass_flag").click(function(e) {        
	   if ($('input#pass_flag').is(':checked')) {
		   $("#password").attr("readonly", false);	
		}else{
			$("#password").attr("readonly", true);	
		}
    });
});
 </script>


<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Add admin user</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_admin">Manage Admin</a></h3>	
        </div>
        <div class="module-body">
                <?php 
					if( $this->session->flashdata('error') ) { 
					   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
							<strong>'.$this->session->flashdata('error').'</strong></div>';
				
					}else if( $this->session->flashdata('success') ) { 
					
					   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
						  <strong>'.$this->session->flashdata('success').'</strong></div>';
					}
				  ?>
                
                <br />                

                  <form class="form-horizontal row-fluid" method="post" action="<?php echo SITE_URL.'index.php/cms/manage_admin/edit' ?>" enctype="multipart/form-data">
                    <?php foreach($setting as $rows){?>
                    
                    <input type="hidden" id="id" name="id" value="<?php echo $rows->id ?>" />
                        <div class="control-group">
                            <label class="control-label" for="basicinput"> Role  </label>
                            <div class="controls">
                                <select id="role" name="role" tabindex="1" data-placeholder="Role" class="span8">
                                   <option value="">Select Roles</option>
                                   <?php 
								      foreach($role as $val){
									     echo "<option value='".$val->id."' ".($val->id == $rows->adm_role_id ? 'selected' : '' ).">".$val->role_name."</option>";
								      }?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Username</label>
                            <div class="controls">
                               <input type="text" name="username" id="username" placeholder="username" tabindex="2" class="span8" value="<?php echo $rows->username ?>"  />
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Email</label>
                            <div class="controls">
                               <input type="text" name="email" id="email" placeholder="Email" tabindex="3" class="span8"  value="<?php echo $rows->email ?>" />
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Password</label>
                            <div class="controls">
                               <input type="password"  name="password" id="password" placeholder="Enter Password" tabindex="4" class="span8"  value="<?php echo $rows->passwd?>" readonly="readonly"/><br />
                               <input type="checkbox" name="pass_flag" id="pass_flag" class="pass_flag" value="yes"/>&nbsp;select checkbox for change password
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Status</label>
                            <div class="controls">
                               <select tabindex="5" id="status" name="status" data-placeholder="Select Status.." class="span8">
                               <option value="" <?php  echo ($rows->status == NULL) ? 'selected' : '';?>>Select Status</option>
                               <option value="1" <?php echo ($rows->status == '1') ? 'selected' : '';?>>Active</option>
                               <option value="0" <?php echo ($rows->status == '0') ? 'selected' : '';?>>Inactive</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()">
                            </div>
                        </div>
                    </form>
                    <?php } ?>
                </div>
                </div>
                                
            </div><!--/.content-->
        </div>
