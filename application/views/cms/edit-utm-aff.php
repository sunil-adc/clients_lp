<?php $arr_all = all_arrays(); ?>
<script type="application/javascript">

 function checkInputs()
 { 
    var regex = /^[0-9]*$/;
	var utm_source      = document.getElementById("utm_source").value;
	var utm_aff         = document.getElementById("utm_sub").value;
    var fire_type       = document.getElementById("fire_type").value;
    var form_type       = document.getElementById("form_type").value;
    var opt_verify      = document.getElementById("opt_verify").value;
	
	var status=document.getElementById("status").value;
	
	if(utm_source == "" || utm_source.trim() ==""){
		alert("Utm source is required");
		document.getElementById("utm_source").focus();
		return false;
	}
	
	if( utm_aff== "" || utm_aff.trim() ==""){
		alert("Affiliate is required");
		document.getElementById("utm_aff").focus();
		return false;
	}
	
    if( fire_type== "" || fire_type.trim() ==""){
        alert("Fire Type is required");
        document.getElementById("fire_type").focus();
        return false;
    }

    if( form_type == "" || form_type.trim() ==""){
        alert("Form Type is required");
        document.getElementById("form_type").focus();
        return false;
    }

    if( opt_verify == "" || opt_verify.trim() == "" ){
        alert("Otp Verify is required");
        document.getElementById("opt_verify").focus();
        return false;
    }

	if( pixel == "" || pixel.trim() ==""){
		alert("Pixel is required");
		document.getElementById("pixel").focus();
		return false;
	}

	
	if(status == "" || status.trim() ==""){
		alert("status field is required");
		return false;
	}
		
 }

 </script>
 

<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Update Utm sub</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>cms/manage_utm_aff">Manage Utm Affiliate</a></h3>	
        </div>
        <div class="module-body">
                 <?php 
					if( $this->session->flashdata('error') ) { 
					   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
							<strong>'.$this->session->flashdata('error').'</strong></div>';
				
					}else if( $this->session->flashdata('success') ) { 
					
					   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
						  <strong>'.$this->session->flashdata('success').'</strong></div>';
					}
				  ?>
                
                <br />                
                 <?php foreach($pub_pixel as $p){?>
                  <form class="form-horizontal row-fluid" method="post" action="<?php echo SITE_URL.'cms/manage_utm_aff/edit' ?>">
                    <input type="hidden" name="id" id="id" value="<?php echo $p->id;?>"/>
                    
                      <div class="control-group">
                            <label class="control-label" for="basicinput"> Utm Source</label>
                            <div class="controls">
                            <?php
                              if($source_data->num_rows() > 0){
                                    echo '<select name="utm_source" id="utm_source" tabindex="1" class="span8">';
                                    foreach ($source_data->result() as $row) {
                                        $selected = ($p->utm_source == $row->id) ? 'selected' : '';
                                        echo '<option '.$selected.' value="'.$row->id.'">'.$row->utm_source.'</option>';
                                    }
                                    echo '</select>';
                               }
                            ?>
                          </div>
                       </div> 		
						
                        		
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Utm Affiliate</label>
                            <div class="controls">
                                <input type="text" id="utm_aff" name="utm_aff" tabindex="2" placeholder="Utm Affiliate" class="span8" value="<?php echo $p->utm_aff?>">
                            </div>
                        </div>
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Pixel</label>
                            <div class="controls">
                                <textarea name="pixel" id="pixel" placeholder="pixel" tabindex="3" class="span8"/><?php echo $p->pixel ?></textarea>
                                <span class="help-inline">Replace dynamic user id with "{USERID}"</span>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Fire Type*</label>
                            <div class="controls">
                                <select name="fire_type" id="fire_type"  tabindex="7" class="span8">
                                    <option value=""  <?php echo ($p->fire_type == NULL) ? 'selected="selectec"' : '';?>>Select Fire Type</option>
                                <?php foreach ($arr_all['FIRE_LIMIT'] as $k => $v){
                                     echo "<option value='".$k."' ".($p->fire_type == $k ? 'selected' : '')."> ".$v." </option>";

                                } ?>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Form Type</label>
                            <div class="controls">
                               <select name="form_type" id="form_type"  tabindex="4" class="span8">
                                    <option value=""  <?php echo ($p->form_type == NULL) ? 'selected="selectec"' : '';?>>Select Form Type</option>
                                    <option value="1" <?php echo ($p->form_type == '1') ? 'selected="selectec"' : '';?>>Small Form</option>
                                    <option value="2" <?php echo ($p->form_type == '2') ? 'selected="selectec"' : '';?>>Large Form</option>
                                </select>
                           </div>
                        </div>
                        
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Otp Verification</label>
                            <div class="controls">
                               <select name="opt_verify" id="opt_verify"  tabindex="4" class="span8">
                                    <option value=""  <?php echo ($p->opt_verify == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                                    <option value="1" <?php echo ($p->opt_verify == '1') ? 'selected="selectec"' : '';?>>Enable</option>
                                    <option value="2" <?php echo ($p->opt_verify == '2') ? 'selected="selectec"' : '';?>>Disable</option>
                                </select>
                           </div>
                        </div>
						
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Status</label>
                            <div class="controls">        
                                <select name="status" id="status"  tabindex="4" class="span8">
                                    <option value=""  <?php echo ($p->status == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                                    <option value="1" <?php echo ($p->status == '1') ? 'selected="selectec"' : '';?>>Active</option>
                                    <option value="0" <?php echo ($p->status == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
                                </select>
                           </div>
                         </div>
                        
                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()" tabindex="5">
                            </div>
                        </div>
                    </form>
                   <?php } ?> 
                </div>
                </div>
                                
            </div><!--/.content-->
        </div>




