<script type="application/javascript">

 function checkInputs()
 { 
    var regex = /^[0-9]*$/;
	var utm_source =document.getElementById("utm_source").value;
	var utm_medium =document.getElementById("utm_medium").value;
	var pixel      =document.getElementById("pixel").value;
	var status=document.getElementById("status").value;
	
	if(utm_source == "" || utm_source.trim() ==""){
		alert("Utm source is required");
		document.getElementById("utm_source").focus();
		return false;
	}
	
	if( utm_medium== "" || utm_medium.trim() ==""){
		alert("Medium is required");
		document.getElementById("utm_medium").focus();
		return false;
	}
	
	if( pixel == "" || pixel.trim() ==""){
		alert("Pixel is required");
		document.getElementById("pixel").focus();
		return false;
	}

	
	if(status == "" || status.trim() ==""){
		alert("status field is required");
		return false;
	}
		
 }

 </script>
 

<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Update Utm Medium</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_utm_medium">Manage Utm Medium</a></h3>	
        </div>
        <div class="module-body">
                 <?php 
					if( $this->session->flashdata('error') ) { 
					   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
							<strong>'.$this->session->flashdata('error').'</strong></div>';
				
					}else if( $this->session->flashdata('success') ) { 
					
					   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
						  <strong>'.$this->session->flashdata('success').'</strong></div>';
					}
				  ?>
                
                <br />                
                 <?php foreach($pub_pixel as $p){?>
                  <form class="form-horizontal row-fluid" method="post" action="<?php echo SITE_URL.'cms/manage_utm_medium/edit' ?>">
                    <input type="hidden" name="id" id="id" value="<?php echo $p->id;?>"/>
                    
                      <div class="control-group">
                            <label class="control-label" for="basicinput"> Utm Source</label>
                            <div class="controls">
                            <?php
                              if($source_data->num_rows() > 0){
                                    echo '<select name="utm_source" id="utm_source" tabindex="1" class="span8">';
                                    foreach ($source_data->result() as $row) {
                                        $selected = ($p->utm_source == $row->id) ? 'selected' : '';
                                        echo '<option '.$selected.' value="'.$row->id.'">'.$row->utm_source.'</option>';
                                    }
                                    echo '</select>';
                               }
                            ?>
                          </div>
                       </div> 		
						
                        		
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Utm Medium</label>
                            <div class="controls">
                                <input type="text" id="utm_medium" name="utm_medium" tabindex="2" placeholder="Utm Medium" class="span8" value="<?php echo $p->utm_medium?>">
                            </div>
                        </div>
                        
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Pixel</label>
                            <div class="controls">
                                <textarea name="pixel" id="pixel" placeholder="pixel" tabindex="3" class="span8"/><?php echo $p->pixel ?></textarea>
                                <span class="help-inline">Replace dynamic user id with "{USERID}"</span>
                            </div>
                        </div>
                        
						
                         <div class="control-group">
                            <label class="control-label" for="basicinput">Status</label>
                            <div class="controls">        
                                <select name="status" id="status"  tabindex="4" class="span8">
                                    <option value=""  <?php echo ($p->status == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                                    <option value="1" <?php echo ($p->status == '1') ? 'selected="selectec"' : '';?>>Active</option>
                                    <option value="0" <?php echo ($p->status == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
                                </select>
                           </div>
                         </div>
                        
                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()" tabindex="5">
                            </div>
                        </div>
                    </form>
                   <?php } ?> 
                </div>
                </div>
                                
            </div><!--/.content-->
        </div>




