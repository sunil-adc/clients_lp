<?php $arr_all = all_arrays(); ?>
<div class="span9">
<div class="content">

    <div class="module">
        <div class="module-head">
            <h3>Card List</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo FULL_CMS_URL?>/manage_cc/add_form">Add Card</a></h3>	
        </div>
        <div class="module-body">
            
            <?php 
				if( $this->session->flashdata('error') ) { 
				   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
						<strong>'.$this->session->flashdata('error').'</strong></div>';
			
				}else if( $this->session->flashdata('success') ) { 
				
				   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
					  <strong>'.$this->session->flashdata('success').'</strong></div>';
				}
			  ?>

            

            <form class="navbar-search pull-left input-append" action="<?php echo FULL_CMS_URL?>/manage_cc" method="post">
            <!--<select name="search_field" id="search_field" >                                
                <option value="offers_name" <?php echo ($search_field == "tag_line") ? "selected='selected'" : ""; ?>>Offer Name</option>
            </select>-->
            <input type="hidden" name="search_field" id="search_field"  value="offers_name" />
            &nbsp;&nbsp;
            
            <select name="search_txt" id="search_txt" > 
            <option value=''>Select Category</option>
            <?php foreach($arr_all['BANK_OFFERS'] as $a=>$b ){				     
                echo "<option value='$a'  ".($search_txt == $a ? 'selected' : '' )." >$b</option>";
            } ?>
            </select>
            
            <select name="status" id="status" >     
                <option value="1" <?php echo ($status == "1") ? "selected='selected'" : ""; ?>>Active</option>
                <option value="0" <?php echo ($status == "0") ? "selected='selected'" : ""; ?>>Inactive</option>
            </select>
            
            <button class="btn" id="btn_search" name="btn_search" type="submit" value="GO">
            <i class="icon-search"></i>
            </button>
            </form>
            <br />
            <!-- <hr /> -->
            <br />
            <table class="table table-bordered">
              <thead>
                <tr>
                <th width="5%">No.</th>
                <th width="15%">Bank </th>
				<th width="15%">Card </th>
                <th width="40%">Url</th>
                <th width="10">Status</th>
                <th width="5%">Action</th>
				<th width="5%">Rewards</th>
                </tr>
              </thead>
              <tbody>
              <?php
                if(is_array($details) && count($details) > 0){	
                  $sl_no=1;
                  foreach($details as $p){
					  
                        echo "<tr>";
						echo "<td>". $sl_no. "</td>";
						echo "<td>". $p->bank. "</td>";
						echo "<td>". $p->offers_name."</td>";
						echo "<td>". $p->offers_url."</td>";
						echo "<td>".($p->status ==1 ? 'Active' : 'Inactive')."</td>";
						echo "<td><a href='".FULL_CMS_URL."/manage_cc/edit_form/".$p->id."'><img alt='Edit' src='".S3_URL."/cms/images/icons/pencil.png'  width='20' height='20'></img></td>";
						echo "<td><a href='".FULL_CMS_URL."/creditcard_rewards/index/".$p->id."'><img alt='Edit' src='".S3_URL."/cms/images/icons/pencil.png'  width='20' height='20'></img></td>";
						
						echo "</tr>";
						
						$sl_no++;
                  }
                }
                ?>
               <tr>
                  <td colspan="9" style="line-height: 1.5em; !important">
                    <?php
                      if (count($details) > 0) {
                         echo $links;
                      }
                    ?>
                  </td>
                </tr>
              </tbody>
            </table>
            <br>
        </div>
    </div>

    <!--/.module-->
<br />    
</div><!--/.content-->
</div>




