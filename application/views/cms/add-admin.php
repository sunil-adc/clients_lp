<?php $arr_all = all_arrays(); ?>
<script type="application/javascript">

 function checkInputs(){
	  
    var regex = /^[0-9]*$/;
	var role  = document.getElementById("role").value;
	var username = document.getElementById("username").value;
	var email  = document.getElementById("email").value;
	var password  = document.getElementById("password").value;
	
		
	if(role == "" || role.trim() ==""){
		alert(" role is required");
		document.getElementById("role").focus();
		return false;
	}
	
	if(username == "" || username.trim() ==""){
		alert(" username is required");
		document.getElementById("username").focus();
		return false;
	}

	if( email== "" || email.trim() ==""){
		alert("email is required");
		document.getElementById("email").focus();
		return false;
	}
	
	if( password == "" || password.trim() ==""){
		alert("Password is required");
		document.getElementById("password").focus();
		return false;
	}
			
}

 </script>


<div class="span9">
<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>Add admin user</h3>
            <h3><a style ="margin-top:-22px;float:right;background: #ffffff;padding: 10px 10px 10px 11px;" href="<?php echo SITE_URL?>index.php/cms/manage_admin">Manage Admin</a></h3>	
        </div>
        <div class="module-body">
                <?php 
					if( $this->session->flashdata('error') ) { 
					   echo '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>
							<strong>'.$this->session->flashdata('error').'</strong></div>';
				
					}else if( $this->session->flashdata('success') ) { 
					
					   echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>
						  <strong>'.$this->session->flashdata('success').'</strong></div>';
					}
				  ?>
                
                <br />                
				
				<div class="btn-controls">
                <div class="btn-box-row row-fluid">
                    <div class="btn-box-row row-fluid">
                 
				
                  <form class="form-horizontal row-fluid" method="post" action="<?php echo SITE_URL.'index.php/cms/manage_admin/add' ?>" enctype="multipart/form-data">
                    
                        <div class="control-group">
                            <label class="control-label" for="basicinput"> Role *</label>
                            <div class="controls">
                                <select id="role" name="role" tabindex="1" data-placeholder="Role" class="span8">
                                   <option value="">Select Roles </option>
                                   <?php 
								      foreach($role as $rows){
									     echo "<option value='".$rows->id."'>".$rows->role_name."</option>";
								      }?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Username *</label>
                            <div class="controls">
                               <input type="text" name="username" id="username" placeholder="username" tabindex="2" class="span8"  />
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Email *</label>
                            <div class="controls">
                               <input type="text" name="email" id="email" placeholder="Email" tabindex="3" class="span8"  />
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Password *</label>
                            <div class="controls">
                               <input type="password"  name="password" id="password" placeholder="Enter Password" tabindex="4" class="span8"  />
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <div class="controls">
                                <input type="submit" name="addform" value="Save" onclick="return checkInputs()">
                            </div>
                        </div>
                    </form>
					
				</div></div></div>
				
                </div>
                </div>
                                
            </div><!--/.content-->
        </div>
