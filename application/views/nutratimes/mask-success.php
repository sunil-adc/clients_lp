<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nutra Times Mask</title>
    <link href="<?php echo S3_URL?>/site/nutratimes-assets/mask/images/favicon.png" rel="icon" />  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/nutratimes-assets/mask/css/style.css">
    <!-- fontawesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Playfair+Display&display=swap" rel="stylesheet">
</head>
<body>
    
    <!-- Header starts Here -->
  
          
    <!-- Header ends Here -->

    <!-- Banner Starts Here -->

    <section id="banner">
      <div class="wrapper">
        <div class="bg_banner">
          <div class="container">
              <div class="col-md-12 mobile_hiden">
                  <div class="col-md-6 col-sm-6">
                    <!-- <img src="" alt="bottle" class="img-responsive"> -->
                  </div>
                  <div class="col-md-6 col-sm-6">
                      
                      <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="insurance_jsfrm('http://www.amazepromos.com/policy/insurance/submit_frm','getinsured','1')">
                        <div class="col heading" style="background-color: green; padding: 25px 4px;">
                          <h4>Thank you for your interest. Anti Pollution mask is out of Stock. We will update you once it is in stock.<br> <br> Happy Breathing...!! </h4>                           
                        </div> 
                     </form>
                      <!-- <div class="stamps"><img src="" alt="stamps" class="img-responsive"></div>         -->
                  </div>
              </div>
              <div class="col-sm-12 desktop-hidden">
                  
              </div>
                

          </div>

        </div>  
      </div>
    </section>

    <!-- banner Ends Here -->
    <div class="desktop-hidden">
      <form role="form" id="feedbackForm" class="feedbackForm mobileForm" action="JavaScript:void(0)" onsubmit="insurance_jsfrm('http://www.amazepromos.com/policy/insurance/submit_frm','getinsured','1')">
        <div class="col heading" style="background-color: green; padding: 25px 4px;">
          <h4>Thank You for Your Purchase <br> <br> Happy Breathing...!! </h4>                           
        </div>
      </form>
    </div>
    


    <div class="clearfix"></div>

      <!-- product details Starts Here -->


    <section id="details">
        <div class="container">
            <div class="col-md-12">
                <div class="wrapper">
                    <div class="col-md-12 textcentre">
                      <h1 id="details">Our Products</h1>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-6 product">
                        <h1>N95 Anti Pollution Mask with 4 Activated Carbon Filters</h1>
                        <h2>PRICE -  210/-</h2>
                        <div class="col-md-6 mask-img">  
                        <img src="<?php echo S3_URL?>/site/nutratimes-assets/mask/images/mask1.jpg" alt="secure" class="img-responsive">
                        </div>
                        <div class="col-md-6 mobileFeature">
                          <h3>Features:</h3>
                          <ul>
                            <li>Offers protection against PM 2.5 Automobile exhausts</li>
                            <li>Washable cotton masks</li>
                            <li>Powerful, replaceable carbon filters</li>
                            <li>5 Layered Carbon Filters</li>
                            <li>100 hours of life for each filter</li>
                            <li>4 carbon filters within pack</li>
                          </ul>
                          <div class="clearfix"></div>
                          <input type="submit" id="frm-sbmtbtn1" value="Order Now" name="submit" class="button orderButtonStyles buyNow">
                        </div>
                        </div>

                        
                        <div class="col-md-6 product">
                        <h1>N95 PM 2.5 Anti Pollution Face Mask with Easy Exhalation Valve</h1>
                        <h2>PRICE - (Pack of 5) - 195/-</h2>
                        <div class="col-md-6 mask-img">  
                        <img src="<?php echo S3_URL?>/site/nutratimes-assets/mask/images/mask2.jpg" alt="secure" class="img-responsive">
                        </div>
                        <div class="col-md-6 mobileFeature">
                          <h3>Features:</h3>
                          <ul>
                            <li>PM 2.5 Pollution mask</li>
                            <li>Foldable mask</li>
                            <li>Comprehensive guard against pollutants, haze, germs etc.</li>
                            <li>5 in 1 guard</li>
                            <li>95% filtration support</li>
                            <li>Comfy ventilation system</li>
                            <li>Speedy Exhaled Air Dissipation through advanced exhalation valve</li>
                            <li>Easily wearable & removable</li>
                          </ul>
                          <div class="clearfix"></div>
                          <input type="submit" id="frm-sbmtbtn1" value="Order Now" name="submit" class="button orderButtonStyles buyNow">
                        </div>
                        </div>
                        
                        <div class="col-md-6 product">
                        <h1>N99 Standard Anti-Pollution Mask</h1>
                        <h2>PRICE -  299/-</h2>
                        <div class="col-md-6 mask-img">  
                        <img src="<?php echo S3_URL?>/site/nutratimes-assets/mask/images/mask3.jpg" alt="secure" class="img-responsive">
                        </div>
                        <div class="col-md-6 mobileFeature">
                          <h3>Features:</h3>
                          <ul>
                            <li>High quality particulate protection</li>
                            <li>Filters occupy 100% of in-mask area</li>
                            <li>99% filtration efficiency</li>
                            <li>Protection beyond 2.5 PM specifications</li>
                            <li>Highly functional diversion breathing valves</li>
                            <li>Advanced exhale valve with one way technology</li>
                            <li>Filters with 5 micro particulate layers</li>
                            <li>Stylish & Comfortable masks</li>
                          </ul>
                          <div class="clearfix"></div>
                          <input type="submit" id="frm-sbmtbtn1" value="Order Now" name="submit" class="button orderButtonStyles buyNow">
                        </div>
                        </div>
                        
                        <div class="col-md-6 product">
                        <h1>N99 Anti Pollution Face Mask with Activated Carbon</h1>
                        <h2>PRICE -  260/-</h2>
                        <div class="col-md-6 mask-img">  
                        <img src="<?php echo S3_URL?>/site/nutratimes-assets/mask/images/mask4.jpg" alt="secure" class="img-responsive">
                        </div>
                        <div class="col-md-6 mobileFeature">
                          <h3>Features:</h3>
                          <ul>
                            <li>6 layer carbon filtration with 99% filtration power</li>
                            <li>Protection against PM 2.5 Particulate matter & other harmful substances</li>
                            <li>Odor control mask</li>
                            <li>Comprehensive respiratory protection</li>
                            <li>Washable & Reusable Mask</li>
                            <li>Comfortable mask with perfect fit</li>
                          </ul>
                          <div class="clearfix"></div>
                          <input type="submit" id="frm-sbmtbtn1" value="Order Now" name="submit" class="button orderButtonStyles buyNow">
                        </div>
                        </div>

                        
                    </div>
                    

                </div>
            </div>
        </div>
    </section>
      <!-- product details ends Here -->

    <div class="clearfix"></div>

    <section id="product">
      <div class="container">
        <div class="col-md-12">
            <div class="wrapper">
                <div class="col-md-12 textcentre">
                  <h1 id="details">About Products</h1>
                </div>

                <div class="clearfix"></div>

                <div class="col-md-12 aboutContent">
                  <div class="col-md-7 aboutPOne">
                    <p>Rising pollution levels in air is now a global concern. Auto exhausts, air borne pathogens, dust particles and other such impurities have deteriorated the quality of air you breathe in. Polluted air around is posing great risks for your health & general well being. Pollution levels in the air of our country capital Delhi have already put Delhi on red alert. Also, the situation is rapidly turning bleak & grave in the other metropolitan cities of the country. Rising pressure of population and pressure of traffic on the roads has made air pollution a pan India concern now. Breathing day after day in contaminated, polluted air can give rise to several health issues in us and some of those could be fatal enough. Here are some of the health issues that polluted air can cause:</p>
                  </div>

                  <div class="col-md-5 about">
                  <h3> <b>Air Pollution Threats</b> </h3>
                    <ul>
                      <li>Respiratory issues such as bronchitis, asthma,emphysema</li>
                      <li>Poor lung function</li>
                      <li>Congestive failure of heart</li>
                      <li>Headache and nausea</li>
                      <li>Fatigue</li>
                      <li>COPD or Chronic Obstructive Pulmonary Disease</li>
                      <li>Lung damage</li>
                      <li>Coughing</li>
                      <li>Irregular heartbeat </li>
                    </ul>
                  </div>
                </div>

                <div class="clearfix"></div>

                <br>

                <p class="aboutTopContent">The above mentioned health issues triggered by pollution can make you critically ill and may even lead to premature deaths. To rule out such possibility all you need to do is to order an Air Pollution Mask that keeps pollution away from you. These Pollution Control Masks are specially equipped with layers of filters that filter out pollutants and harmful particulate matters from the air you inhale and that lets you inhale only clean and safe air. Multi-layer filtration technology that these anti-pollution masks come equipped with can help you fight various types of pollutants and can save you from health hazards triggered by pollution. Choose the air pollution mask that fits your needs & budget and place your order without delay. You deserve to breathe in clean air…Choose your mask today & stop losing your life to pollution!</p>
            </div>
        </div>
      </div>
    </section>

   
    <div class="clearfix"></div>

    <section>
      <div class="container">
        <div class="col-md-12">
          <div class="wrapper">

            <div class="col-md-12 textcentre">
              <h1 id="details">How To Use</h1>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12">
              <div class="col-md-4 useSec">
                <img src="<?php echo S3_URL?>/site/nutratimes-assets/mask/images/use1.jpg" alt="">
              </div>
              <div class="col-md-4 useSec">
                <img src="<?php echo S3_URL?>/site/nutratimes-assets/mask/images/use2.jpg" alt="">
              </div>
              <div class="col-md-4 useSec">
                <img src="<?php echo S3_URL?>/site/nutratimes-assets/mask/images/use3.jpg" alt="">
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

      <!-- footer starts here  -->
    <footer>
      <div class="footer1">
        <div class="container">
          <div class="col-md-12">
              <div class="wrapper">

                <div class="col-md-12 mtb">
                    
                    <div class="col-md-12 col-xs-12 textcentre">
                      <h3>Contact us</h3>
                      <p class="lineheight1_6"> Email Us: <a href="#">support@nutratimes.com</a> <br>
                          Call Us: +91 08046328320 </p>
                    </div>
                    
                </div>

              </div>
          </div>
        </div>
      </div>

      <div class="footer2">
        <p>© 2019 <a href="https://www.nutratimes.com/" target="_blank"> nutratimes.com </a> | &nbsp  All rights reserved</p>
      </div>

    </footer>
    
    <!---Scripts------>
    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>  

    <script>
      $(document).ready(function(){
        $(".buyNow").click(function(){

          if(window.innerWidth < 600){
            $('html, body').animate({
              scrollTop: $('.mobileForm').offset().top
            }, 1800);
          }else{
            $('html, body').animate({
              scrollTop: $('.feedbackForm').offset().top
            }, 1800);
          }

        });
      });
    </script>


</body>
</html>