<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Book Lab Tests Online - Best Discounts - Certified Technicians</title>
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/medlife-images/favicon.ico" type="image/x-icon" />

      <!-- Bootstrap -->
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/medlife-css/bootstrap.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/medlife-css/new-age.min.css">
      <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700" rel="stylesheet">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
	  
	  <!-- Google Code for Remarketing Tag -->
		<!--------------------------------------------------
		Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
		--------------------------------------------------->
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 798077187;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/798077187/?guid=ON&amp;script=0"/>
		</div>
		</noscript>


		<!-- Facebook Pixel Code -->
		<script>
		  !function(f,b,e,v,n,t,s)
		  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		  n.queue=[];t=b.createElement(e);t.async=!0;
		  t.src=v;s=b.getElementsByTagName(e)[0];
		  s.parentNode.insertBefore(t,s)}(window, document,'script',
		  'https://connect.facebook.net/en_US/fbevents.js');
		  fbq('init', '584563021946205');
		  fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		  src="https://www.facebook.com/tr?id=584563021946205&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->	
	  
   </head>
   <body id="page-top">
   	
   	<script>
	  fbq('track', 'Lead', {
	    value: 235,
	    currency: 'INR',
	  });
	</script>

      <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
         <div class="container text-center">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">                
               <a class="navbar-brand page-scroll" href="#page-top">
               <img src="<?php echo S3_URL?>/site/medlife-images/logo.jpg" class="logo"/>
               </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1 ">
               <h4 style="color: #fff;text-shadow: 1px 1px 5px #2562bb;font-size: 22px;
                  margin: 40px 0px;"></h4>
            </div>
            <!-- /.navbar-collapse -->
         </div>
         <!-- /.container-fluid -->
      </nav>
	  
	  
	  <header>
      <?php $all_array = all_arrays(); ?>
         <div class="container form_margin">
            <div class="row">
				
				<div class="col-md-8 form_containt_box">
               		<div class="row" style="margin:130px 0px 0px 0px;">
						<h2>First Step Towards Healthy Living</h2>	
						<h1>Know Your Health Status Today</h1>	
                   <div class="col-sm-6 col-md-4 profile ">
                     <div class="">
                        <h2>Aarogyam B</h2>
                        <p>(60 Tests)</p> 
                        <hr class="dotted">
                        <h3>₹700</h3>
                        <span>₹2700</span> 
                      </div>
                   </div>

                   <div class="col-sm-6 col-md-4 profile green_bg">
                     <div class="bg-green">
                        <h2>Aarogyam C</h2>
                        <p>(71  Tests)</p>
                        <hr class="dotted">
                        <h3>₹1000</h3>
                        <span>₹3250</span>
                     </div>
                  </div>

				</div> 
					  
					<div class="row" style="margin:20px 0px 0px 0px;">
                   <div class="col-sm-6 col-md-4 profile orange_bg">
                     <div class="">
                        <h2>Healthscreen B Profile</h2>
                        <p>(74 Tests)</p>
                        <hr class="dotted">
                        <h3>₹1800</h3>
                        <span>₹4135</span> 
                      </div>
                   </div>

                  <div class="col-sm-6 col-md-4 profile red_bg">
                     <div class="bg-green">
                        <h2>Healthscreen C Profile</h2>
                        <p>(79 Tests)</p>
                        <hr class="dotted">
                        <h3>₹2000</h3>
                        <span>₹6900</span>
                     </div>
                  </div>

                 </div> 
				</div>
				

               <div class="col-md-4 ">
                  <div class="jumbotron form_box">
                     
					 <div class="row text-center" id="success_verify">  
							
                           <h3 style="color:#3c4850; margin-top:50px;font-size: 20px;line-height: 30px;">Thank you for booking your appointment in Medlife Lab.</h3>
                     </div>
					 
                  </div>
               </div>
               
            </div>
         </div>
      </header>
      <section id="features" class="features" >
         <!--<div class="container">
            <div class="row">
               <div class="container-fluid">
				   <h2 class="text-center">PACKAGES <br><br>   </h2>
                  <div class="row">  
                     <div class="col-lg-3 col-sm-12 text-center">
						 <div class=" offfer-box"> 
						 <h1>Healthscreen M Profile<br> (33 Tests) </h1>
								
                        <div class="feature-item" style="min-height:290px"> 
							<li class="package-test-wrapper">Thyroid Profile (3)</li>
							<li class="package-test-wrapper">Diabetic Screen (2)</li>
							<li class="package-test-wrapper">Complete Hemogram (28)</li>
                        </div>
							 <div class="brnd-logo"> 
                        <img class="" alt="140x140" style="width: 110px; height: auto;" src="<?php echo S3_URL?>/site/medlife-images/thyrocare.png" data-holder-rendered="true">
							 </div>
						<div class="price">
							<div class="bestprice">																
								<div class="actual-price"><span class="uncut">₹1000</span> ₹500</div>
							</div>
							<a  href="#" type="submit" id="frm-sbmtbtn" class="btn btn-default book_app" value="BOOK NOW" data-attr="1">BOOK NOW</a>
						</div>
						</div> 
                     </div>
					  <div class="col-lg-3 col-sm-12 text-center">
						  <div class=" offfer-box">
						 <h1>Healthscreen A Profile<br> (71 Tests) </h1>
                        
						<div class="feature-item" style="min-height:290px"> 
							<li class="package-test-wrapper">Toxic Elements Profile (9)</li>
							<li class="package-test-wrapper">Diabetic Screen (2)</li>
							<li class="package-test-wrapper">Pancreas Profile (2)</li>
							<li class="package-test-wrapper">Liver Profile (11)</li>
							<li class="package-test-wrapper">Thyroid Profile (3)</li>
							<li class="package-test-wrapper">Lipid Profile (8)</li>
							<li class="package-test-wrapper">Iron Deficiency Profile (3)</li>
							<li class="package-test-wrapper">Kidney Profile (5)</li>
							<li class="package-test-wrapper">Complete Hemogram (28)</li>							
                        </div>
							  <div class="brnd-logo"> 
                        <img class="" alt="140x140" style="width: 110px; height: auto;" src="<?php echo S3_URL?>/site/medlife-images/thyrocare.png" data-holder-rendered="true">
							 </div> 
						<div class="price">
							<div class="bestprice">																
								<div class="actual-price"><span class="uncut">₹ 2700 </span> ₹ 1200</div>
							</div>
							<a  href="#" type="submit" id="frm-sbmtbtn" class="btn btn-default book_app" value="BOOK NOW" data-attr="2">BOOK NOW</a>
						</div>
							  
					 </div>
                     </div>
					  <div class="col-lg-3 col-sm-12 text-center">
						 <div class=" offfer-box"> 
						 <h1>Healthscreen B Profile<br> (74 Tests)</h1>
                        
                        <div class="feature-item" style="min-height:290px"> 
							<li class="package-test-wrapper">Toxic Elements Profile (9)</li>							
							<li class="package-test-wrapper">Diabetic Screen (2)</li>							
							<li class="package-test-wrapper">Homocysteine</li>							
							<li class="package-test-wrapper">Pancreas Profile (2)</li>							
							<li class="package-test-wrapper">Liver Profile (11)</li>							
							<li class="package-test-wrapper">Thyroid Profile (3)</li>							
							<li class="package-test-wrapper">Lipid Profile (8)</li>							
							<li class="package-test-wrapper">Vitamin Profile (2)</li>							
							<li class="package-test-wrapper">Iron Deficiency Profile (3)</li>							
							<li class="package-test-wrapper">Kidney Profile (5)</li>							
							<li class="package-test-wrapper">Complete Hemogram (28)</li>	 
                        </div> 
							 <div class="brnd-logo"> 
                        <img class="" alt="140x140" style="width: 110px; height: auto;" src="<?php echo S3_URL?>/site/medlife-images/thyrocare.png" data-holder-rendered="true">
							 </div>
					   <div class="price">
							<div class="bestprice">																
								<div class="actual-price"><span class="uncut">₹ 4135 </span> ₹ 1800</div>
							</div>
							<a  href="#" type="submit" id="frm-sbmtbtn" class="btn btn-default book_app" value="BOOK NOW" data-attr="3">BOOK NOW</a>
						</div>
							 
				 </div>
                     </div>
					  <div class="col-lg-3 col-sm-12 text-center">
						  <div class=" offfer-box">
						 <h1>Healthscreen C Profile<br> (79 Tests) </h1>
                        
                        <div class="feature-item" style="min-height:290px"> 
							<li class="package-test-wrapper">Toxic Elements Profile (9)</li>							
							<li class="package-test-wrapper">Diabetic Screen (2)</li>							
							<li class="package-test-wrapper">Homocysteine</li>							
							<li class="package-test-wrapper">Pancreas Profile (2)</li>							
							<li class="package-test-wrapper">Liver Profile (11)</li>							
							<li class="package-test-wrapper">Thyroid Profile (3)</li>							
							<li class="package-test-wrapper">Lipid Profile (8)</li>							
							<li class="package-test-wrapper">Vitamin Profile (2)</li>							
							<li class="package-test-wrapper">Iron Deficiency Profile (3)</li>							
							<li class="package-test-wrapper">Kidney Profile (5)</li>							
							<li class="package-test-wrapper">Complete Hemogram (28)</li>	 
							<li class="package-test-wrapper">Cardiac Risk Markers (5)</li>	 
                        </div>
							<div class="brnd-logo"> 
                        <img class="" alt="140x140" style="width: 110px; height: auto;" src="<?php echo S3_URL?>/site/medlife-images/thyrocare.png" data-holder-rendered="true">
							 </div>  
							 
							  
							  <div class="price">
							<div class="bestprice">																
								<div class="actual-price"><span class="uncut">₹ 6900 </span> ₹ 2000</div>
							</div>
							<a  href="#" type="submit" id="frm-sbmtbtn" class="btn btn-default book_app" value="BOOK NOW" data-attr="4">BOOK NOW</a>
						</div>
							  </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
        --> 
	  </section>
	  <div id="pi">
       <?php
	   $vr = $this->session->userdata('medlife_id');
	   $ut = $this->session->userdata('utm_source');
	   if( isset($vr) &&  $vr > 0 && $ut != ""){
		   echo $this->lead_check->set_pixel($vr, MEDLIFE_USER);
		   
	   }
	   ?>
      </div> 
      <footer>
      	© 2018 - Medlife Diagnostics. All Rights Reserved
      </footer>
      <!-- jQuery -->
      <script src="<?php echo S3_URL?>/site/scripts/jquery-1.11.3.min.js"></script>
      <!-- Bootstrap Core JavaScript -->
      <script src="<?php echo S3_URL?>/site/scripts/bootstrap.js"></script>
      <!-- Plugin JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
      <!-- Theme JavaScript -->
      <script src="<?php echo S3_URL?>/site/medlife-css/new-age.min.css"></script>
      <script src="<?php echo S3_URL?>/site/scripts/flatpickr.min.js"?>"></script>
      <script src="<?php echo S3_URL?>/site/scripts/default.js"?>"></script>
      <link href="<?php echo S3_URL?>/site/medlife-css/flatpickr.min.css" rel="stylesheet">
      
        <script>
		$(document).ready(function(e) {    
		$(document).on("click", ".book_app", function(e) {
			var option=($(this).attr("data-attr"));
			// $("#medlife_test option[value='"+option+"']").select();
			 $('#medlife_test option:eq('+option+')').prop('selected', true)
		});
		});
		
		</script>
		
		
		<!-- Google Code for Remarketing Tag -->
		<!--------------------------------------------------
		Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
		--------------------------------------------------->
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 798077325;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/798077325/?guid=ON&amp;script=0"/>
		</div>
		</noscript>
		

				<!-- Google Code for Medlife Lead Conversion Page -->
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 798077325;
		var google_conversion_label = "r_CECIejtIcBEI3jxvwC";
		var google_remarketing_only = false;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/798077325/?label=r_CECIejtIcBEI3jxvwC&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>

   </body>
</html>