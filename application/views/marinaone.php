<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Marina One: Kerala's Largest Waterfront Luxury Apartments</title>
      <meta name="description" content="Marina One, Kerala's largest waterfront apartments are located at the best locale one can get a peaceful luxury life and waterfront view in Kochi.">
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/favicon.png" type="image/x-icon" />


	  <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/marinaone/css/vendor.css">
	  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/marinaone/css/main.css">    
      <!-- fonts -->
      <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
            rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
   
   <!-- Global site tag (gtag.js) - Google Ads: 798490175 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-798490175"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-798490175');
    </script>



    <!-- Global site tag (gtag.js) - Google Ads: 790151821 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-790151821"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-790151821');
    </script>

   </head>
   <body>

    <?php $all_array = all_arrays(); ?>      
      
    <!-- banner -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="">
                    <div class="carousel-caption">
                        <div role="form" id="feedbackForm" class="feedbackForm" >
                            <div class="">       
                                <div class="col">
                                    <div class="form-logo">
                                        <a href="#">
                                            <img src="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/logo-small.png" />
                                        </a> 
                                    </div> 
                                    <h4>Super-luxury 3.5 BHK Apartments Starting Rs 1.92 Cr at Marine Drive, Kochi</h4>                         
                                </div> 
                                <div class="col">  
                                    <div class="form-section" id="form_div1">
                                        <form action="JavaScript:void(0)" onsubmit="puravankara_jsfrm_marinaone('<?php echo SITE_URL?>realestate/puravankara/puravankara_frm','marinaone','1')"> 
                                            <div class="group">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid_puravankara('name', 'Please enter correct name')" data-attr="Please enter correct name">
                                                    <span class="help-block" id="name_err1"></span>
                                                </div>
                                            </div>  
                                            <div class="group">
                                                <div class="form-group"> 
                                                    <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid_puravankara('email', 'Please enter correct email')" data-attr="Please enter correct email">
                                                    <span class="help-block" id="email_err1" ></span>
                                                </div>
                                                <div class="form-group">
                                                    <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode1">
                                                    <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid_puravankara('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                                                    <span class="help-block" id="phone_err1"> </span>
                                                    <span class="help-block" id="CountryCode_err"> </span>
                                                </div>
                                            </div>      
                                            <div class="submitbtncontainer">
                                                <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal -->
    </div>
    <!--//banner -->      
    <!-- about -->
    <div class="about" id="about">
        <div class="container">
            <div class="col-md-6 about-left">
            <div class="col-xs-12 aboutimg-w3l aboutimg-w3l2">
            <div class="rwd-media">
            <iframe  src="https://www.youtube.com/embed/CPmtTdTzNjI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               


                </div>
                </div>
                <!-- <div class="col-xs-6 aboutimg-w3l">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/ab3.jpg" alt="image" />
                
                </div> -->
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-6 about_right">
                <h3 class="bold">Overview</h3>
                <h3 >Marina One</h3>
                <p>
                Marina One, Kerala's largest waterfront residential development, is a super-luxury home offering from SOBHA in joint development with Puravankara. With 1141 homes of 3BHK+study and 4BHK, with sizes ranging from 2296 SFT to 3710 SFT, this project aims to define the concept of super luxury in Kochi and Kerala. 
                </p>
                <!-- <p> This project is uniquely located at probably the best possible location one can get for a waterfront development in Kochi, at the corner of Marine Drive. You have waterfront on two sides, and panoramic views of the Arabian Sea. High Court Junction is just 1.5 kms away. The project location is secluded while being highly accessible. Some of Kochi’s best shopping, dining and entertainment destinations are closeby. Also at a short distance away are prestigious educational institutions, super-speciality hospitals, malls & markets and places of worship.</p> -->
        
        

            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!-- //about -->
    <!-- popular -->
    <div class="popular-w3" id="popular">
    
    </div>
    <!-- //popular -->
    <!-- popular -->
    <div class="gallery" id="popular">
        <div class="gallery-grid1">        
            <div class="stack twisted">
                <img  src="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/loc.jpg" alt=" " class="img-responsive" />
            </div>
        </div>
    </div> 
    <!-- //popular -->

    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">Lifestyle</h3>
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12"> 
                    <img src="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/about.jpg" alt="image" style="width: 100%;height: 510px;object-fit: cover;object-position: right;"/>				
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <!-- begin panel group -->
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <!-- panel 1 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab1" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingOne"data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <h4 class="panel-title">Water One</h4>
                                </div>
                            </span>
                            
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                    <ul class="tab-pane-inner-ul">
                                        <li>Leisure Pool</li>
                                        <li>Beach Deck</li>
                                        <li>Pool Deck</li>
                                        <li>Step Plaza</li>
                                        <li>Sun Deck</li>
                                        <li>Tree Promenade</li>
                                        <li>Event Plaza</li>
                                        <li>Poolside Party Lawn</li>
                                        <li>Changing Rooms</li>
                                        <li>50m Lap Pool</li>
                                    </ul>
                                </div>
                            </div>
                        </div> 
                        <!-- / panel 1 -->
                        <!-- panel 2 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab2" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingtwo"data-toggle="collapse" data-parent="#accordion" href="#collapsetwo" aria-expanded="true" aria-controls="collapsetwo">
                                    <h4 class="panel-title">Activity One</h4>
                                </div>
                            </span>
                            
                            <div id="collapsetwo" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                    <ul class="tab-pane-inner-ul">
                                        <li>Cricket Pitch</li>
                                        <li>Fitness Stations</li>
                                        <li>Jogging / Bike Track</li>
                                        <li>Multi-Court</li>
                                        <li>Tennis Court</li>
                                    </ul>
                                </div>
                            </div>
                        </div> 
                        <!-- / panel 2 -->
                        <!-- panel 3 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab3" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingtwo"data-toggle="collapse" data-parent="#accordion" href="#collapsethree" aria-expanded="true" aria-controls="collapsethree">
                                    <h4 class="panel-title">Entertainment One</h4>
                                </div>
                            </span>
                            
                            <div id="collapsethree" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                    <ul class="tab-pane-inner-ul">
                                        <li>Waterside Party Lawn</li>
                                        <li>Seating Alcove</li>
                                        <li>Amphitheatre</li>
                                        <li>Children’s Play Mound</li>
                                        <li>Skating Rink</li>
                                        <li>BBQ Corner</li>
                                        <li>Children’s Play Park</li>
                                        <li>Activity Lawns</li>
                                        <li>Fountain Roundabout</li>
                                    </ul>
                                </div>
                            </div>
                        </div> 
                        <!-- / panel 3 -->                       
                        <!-- panel 4 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab3" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingtwo"data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="true" aria-controls="collapsefour">
                                    <h4 class="panel-title">Recharge One</h4>
                                </div>
                            </span>
                            
                            <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                    <ul class="tab-pane-inner-ul">
                                        <li>Library</li>
                                        <li>Salon</li>
                                        <li>Steam / Sauna</li>
                                        <li>Chess / Carrom</li>
                                        <li>Gym</li>
                                        <li>2 Multi-purpose Halls</li>
                                        <li>Crèche</li>
                                        <li>Convenience Store</li>
                                        <li>Clinic</li>
                                        <li>Badminton Courts</li>
                                        <li>Squash Court</li>
                                        <li>Yoga Room</li>
                                        <li>Table Tennis</li>
                                        <li>30 Seat Mini Theatre</li>
                                    </ul>
                                </div>
                            </div>
                        </div> 
                        <!-- / panel 4 -->   
                        <!-- panel 5 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab5" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingtwo"data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="true" aria-controls="collapsefive">
                                    <h4 class="panel-title">Tranquility One</h4>
                                </div>
                            </span>
                            
                            <div id="collapsefive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                    <ul class="tab-pane-inner-ul">
                                        <li>Bio-pond</li>
                                        <li>Meditation Deck</li>
                                        <li>Butterfly Eco-trail</li>
                                    </ul>
                                </div>
                            </div>
                        </div> 
                        <!-- / panel 5 -->  
                    </div> <!-- / panel-group -->
                </div> <!-- /col-md-4 -->
            </div> <!--/ .row -->
        </div>      
    </div>    
    <!-- popular -->
    <div class="popular-w4" id="popular">
        
    </div>
    <!-- //popular -->
   
    <!-- projects -->
    <div class="gallery " id="projects">
        <div class="container">
            <h3 class="title">Gallery</h3>
            <div class="agile_gallery_grids w3-agile demo">
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                    
                            <div class="stack twisted">
                            <a title="marinaone" href="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h1.jpg">
                                <img  src="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h1.jpg" alt=" " class="img-responsive" />
                                </a>
                            </div>							
                        
                    </div>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        
                        <div class="stack twisted">
                        <a title="marinaone" href="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h2.jpg">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h2.jpg" alt=" " class="img-responsive" />
                            </a>
                            </div>
                        
                    </div>
            
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        
                        <div class="stack twisted">
                        <a title="marinaone" href="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h3.jpg">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h3.jpg" alt=" " class="img-responsive" />
                            </a>
                            </div>
                    
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        
                        <div class="stack twisted">
                        <a title="marinaone" href="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h4.jpg">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h4.jpg" alt=" " class="img-responsive" />
                           </a> 
                            </div>
                    
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        
                        <div class="stack twisted">
                        <a title="marinaone" href="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h5.jpg">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h5.jpg" alt=" " class="img-responsive" />
                            </a>
                            </div>
                    
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        
                        <div class="stack twisted">
                        <a title="marinaone" href="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h6.jpg">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h6.jpg" alt=" " class="img-responsive" />
                            </a>
                            </div>
                    
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        
                        <div class="stack twisted">
                        <a title="marinaone" href="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h7.jpg">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h7.jpg" alt=" " class="img-responsive" />
                            </a>
                            </div>
                    
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        
                        <div class="stack twisted">
                        <a title="marinaone" href="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h8.jpg">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h8.jpg" alt=" " class="img-responsive" />
                            </a>
                            </div>
                    
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        
                        <div class="stack twisted">
                        <a title="marinaone" href="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h9.jpg">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h9.jpg" alt=" " class="img-responsive" />
                            </a>
                            </div>
                    
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        
                        <div class="stack twisted">
                        <a title="marinaone" href="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h10.jpg">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/h10.jpg" alt=" " class="img-responsive" />
                            </a>
                            </div>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //projects -->      
    <!-- projects -->
    <div class="gallery " id="projects">
        <div class="container">
            <h3 class="title">Plans</h3>
            <div class="agile_gallery_grids w3-agile demo">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
                        <div class="gallery-grid1">
                            <div class="stack twisted">
                                <a title="Master Plan" href="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/mp.jpg">
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/mp.jpg" alt=" " class="img-responsive" />
                                </a>
                            </div>		
                        </div>
                    </div>
                </div>
        
            </div>
        </div>
    </div>
    <!-- //projects -->      
    <!-- contact -->
    <div class="address" id="contact">
        <div class="container">
            <h3 class="title">LOCATION</h3>
            <div class="address-row">

                <div class="col-md-12 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
                    <div class="address-grid">
                        <!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
                        <div id="location">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7858.4122379729115!2d76.27342!3d9.999826!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcb34c249287a74f2!2sMarina+One%2C+Kochi!5e0!3m2!1sen!2sin!4v1552388984713" style="border:0;width: 100%;height: 400px;" allowfullscreen></iframe>

                                
                        </div>
                    </div>
                </div>
            
            </div>
        </div>
    </div>
    <!--//contact-->
    <footer>
        <div class="copy-right-grids">
            <p class="footer-gd">© 2019 Puravankara. All rights reserved.</p>
        </div>
    </footer>   

    <div class="floating-form visiable" id="contact_form">
        <div class="contact-opener">Enquire Now</div>
        <div  id="feedbackForm" class="feedbackForm">
            <div class="">       
                <div class="col text-center">
                    <div class="form-logo">
                        <a href="#">
                            <img src="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/logo-small.png" />
                        </a> 
                    </div> 
                    <h4>Super-luxury 3.5 BHK Apartments Starting Rs 1.92 Cr at Marine Drive, Kochi</h4>     
                </div> 
                <div class="col">     
                    <div class="form-section" id="form_div2">
                    <form role="form" action="JavaScript:void(0)" onsubmit="puravankara_jsfrm_marinaone('<?php echo SITE_URL?>realestate/puravankara/puravankara_frm','marinaone','2')">
                            <div class="groups">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid_puravankara('name', 'Please enter correct name')" data-attr="Please enter correct name">
                                    <span class="help-block" id="name_err2"></span>
                                </div>
                            </div>  
                            <div class="groups">
                                <div class="form-group">
                                    <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid_puravankara('email', 'Please enter correct email')" data-attr="Please enter correct email">
                                    <span class="help-block" id="email_err2" ></span>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode2">
                                    <input type="text" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid_puravankara('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                                    <span class="help-block" id="phone_err2"> </span>
                                    <span class="help-block" id="CountryCode_err"> </span>
                                </div>
                            </div>    
                            <div class="submitbtncontainer">
                                <input type="submit" id="frm-sbmtbtn2" value="Submit" name="submit">
                            </div>
                        </form>    
                    </div>    
                    
                </div>
            </div>
        </div>	
    <div>

    <div class="popup-enquiry-form mfp-hide" id="popupForm">
        <div  id="feedbackForm" class="feedbackForm">
            <div class="">       
                <div class="col">
                    <div class="form-logo">
                        <a href="#">
                            <img src="<?php echo S3_URL?>/site/realestate-assets/marinaone/images/logo-small.png" />
                        </a> 
                    </div>
                    <h4>Super-luxury 3.5 BHK Apartments Starting Rs 1.92 Cr at Marine Drive, Kochi</h4>     
                </div> 
                <div class="col">
                    <div class="form-section" id="form_div3">
                        <form role="form"  action="JavaScript:void(0)" onsubmit="puravankara_jsfrm_marinaone('<?php echo SITE_URL?>realestate/puravankara/puravankara_frm','marinaone','3')">
                            <div class="groups">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="name3" name="name" placeholder="Name" onkeyup="chck_valid_puravankara('name', 'Please enter correct name')" data-attr="Please enter correct name">
                                    <span class="help-block" id="name_err3"></span>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid_puravankara('email', 'Please enter correct email')" data-attr="Please enter correct email">
                                    <span class="help-block" id="email_err3" ></span>
                                </div>
                            </div>  
                            <div class="groups">
                                <div class="form-group">
                                    <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode3">
                                    <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid_puravankara('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                                    <span class="help-block" id="phone_err3"> </span>
                                    <span class="help-block" id="CountryCode_err"> </span>
                                </div>
                            </div>      
                            <div class="submitbtncontainer">
                                <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                            </div>
                        </form>
                    </div>    
                </div>
            </div>
        </div>
    </div>

	<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
	<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
	<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
	<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
	<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
	<input type="hidden" id="utm_content" name="utm_content" value="<?php echo (isset($_REQUEST['utm_content']) != "" ? $_REQUEST['utm_content'] : " "); ?>">
	
	<script src="<?php echo S3_URL?>/site/realestate-assets/marinaone/js/vendor.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>  
    <script src="<?php echo S3_URL?>/site/realestate-assets/marinaone/js/main.js"></script>
	<script src="<?php echo S3_URL?>/site/scripts/default.js"></script>
	 <script src="<?php echo S3_URL?>/site/scripts/realesate.js"></script>
   </body>
</html>