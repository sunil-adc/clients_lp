<?php $all_array = all_arrays(); 

?>
<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>LSBF | Make Your Course Enquiry</title>
      
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/lsbf-assets/images/favicon.png" type="image/x-icon" />


	  <link rel="stylesheet" href="<?php echo S3_URL?>/site/lsbf-assets/css/vendor.css">
	  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/lsbf-assets/css/main.css">    
      <!-- fonts -->
      <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
            rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
   </head>
   <body>

 <?php $all_array = all_arrays(); ?>      
        <!-- <div class="header-top">
            <p>
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                Chokkanahalli Thanisandra Main Road, Yelahanka Hobli, Bengaluru-560064
            </p>
        </div> -->
	<!-- banner -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="">
					<div class="carousel-caption">
                    <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="lsbf_jsfrm('<?php echo SITE_URL?>lsbf/submit_frm','1')">
                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/lsbf-assets/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Make Your Course Enquiry</h4>                           
                            </div> 
                        <div class="col">   
                        <div class="form-group">
                            <input type="text" class="form-control" id="fname1" name="fname" placeholder="First Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                            <span class="help-block" id="fname_err1"></span>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="lname1" name="lname" placeholder="Last Name" onkeyup="chck_valid('lname', 'Please enter correct lastname')" data-attr="Please enter correct lastname">
                            <span class="help-block" id="lname_err1"></span>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                            <span class="help-block" id="email_err1" ></span>
                        </div>
                        
                        <div class="form-group">
                            <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode1">
                            <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                            <span class="help-block" id="phone_err1"> </span>
                            
                        </div>
                       
                        <div class="form-group">
                           <select name="cource" id="cource1" class="form-control">
                                <option value="" selected="selected">Course</option>                                                                       
                                <?php foreach($all_array['LSBF_COURCE'] as $k=>$v){ ?>
						<option value="<?php echo $k;?>" > <?php echo $v;?></option>
				<?php }?>                                            
                            </select>
							<span class="help-block" id="cource_err1"> </span>
                            
                        </div>
                       
                      
                    <div class="submitbtncontainer">
                    <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
                    </div>
                    </div>
                    </div>
                </form>
					</div>
				</div>
			</div>
		</div>

		<!-- The Modal -->
	</div>
    <!--//banner -->      
	<!-- about -->
	<div class="about" id="about">
		<div class="container">
			
			<div class="col-md-6 about_right">
				<h3>The</h3>
				<h3 class="bold">LSBF Story</h3>
				
                <p>Founded by a genuine entrepreneur, LSBF has always been one step ahead when it comes to business education. From day one, the school has adopted a global mindset, offering industry relevant programmes that are tailored to the career goals of students.
                 </P>
                 <P>
                 Under the royal patronage of Prince Michael of Kent, LSBF has established itself as one of the world’s fastest growing business schools, teaching over 38,000 ambitious students worldwide.
                 </P>
                 <P>
                 LSBF has seen exponential growth over the last 10 years, establishing campuses in the UK, Singapore and Canada, and regional offices in over 12 cities around the world. Over the years, LSBF has won several industry accolades, including the recent Queen’s Award for Enterprise for its contribution to the international trade.
                 </P>
                 <P>
                 <i>
                 London School of Business & Finance in Singapore; shaping success in business and finance.
                 </i>
                 </P>
            </div>
            <div class="col-md-6 about-left">
                <div class="col-xs-12 aboutimg-w3l aboutimg-w3l2">
                    <div class="rwd-media">
                            <iframe src="https://www.youtube.com/embed/tvZ0Sx38ycM" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
				</div>
				<div class="clearfix"> </div>
			</div>
            <div class="clearfix"> </div>
		</div>
	</div>
	<!-- //about -->


    <!-- projects -->
	<div class="gallery" id="projects">
        <div class="container"> 
            <h3 class="title">Courses</h3>
            <div class="row">
                <div class="col-md-12">
                    <!-- begin panel group -->
                    <div  class="panel-group">
                         <div class="panel panel-default"> 
                           <span class="side-tab" data-toggle="tab" role="tab" aria-expanded="false"> 
                                <div class="panel-heading title-header-block" role="tab" id="" data-toggle="collapse" data-parent="#" href="#" aria-expanded="" aria-controls=""> 
                                    <h4 class="panel-title">Course Name</h4> 
                                    <h4>Course Type</h4> 
                                    <h4>Location</h4> 
                                    <span class="btn"></span> 
                                </div> 
                           </span> 
                        </div>   
                    </div>

                    <div class="panel-group course-list-accoedian" id="accordion" role="tablist" aria-multiselectable="true">
                     
                    </div> <!-- / panel-group --> 
                </div> <!-- /col-md-12 -->
            </div> <!--/ .row -->
        </div>   
	</div>
    <!-- //projects -->

  


    	<!-- contact -->
	<div class="address" id="contact">
		<div class="container">
			<h3 class="title">Contact Us</h3>
			<div class="address-row">
				<div class="col-md-7 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
					<div class="address-grid">
						<!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
						<div id="location">
                       
                            <iframe style="border:0;width: 100%; height: 450px;"  src="https://www.google.com/maps/embed?pb=!4v1538631496364!6m8!1m7!1sCAoSLEFGMVFpcFAtMmhkX0lQVXFwYVJwUWI2dnhIWENOTVlMYzNFd2JCV0ROdDJY!2m2!1d1.2751834852575!2d103.84627962078!3f307.07!4f-0.8700000000000045!5f0.7301823886297167"  allowfullscreen></iframe>
                        </div>
					</div>
                </div>
                <div class="col-md-5 col-xs-12 address-right">
					<div class="address-info wow fadeInRight animated" data-wow-delay=".5s">
						<h4>Address</h4>
						<p>3 Anson Road,<br> Springleaf Tower,<br> #06-01,<br> Singapore 079909</p>
					</div>				
				</div>
			</div>
		</div>
	</div>
	<!--//contact-->
    <footer>
		<div class="copy-right-grids">
			<p class="footer-gd">© 2018 London School of Business & Finance. All rights reserved.| Maintained by Adcanopus</p>
		</div>
	</footer>    
	<div class="floating-form visiable" id="contact_form">
	<div class="contact-opener">Enquire Now</div>
        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="lsbf_jsfrm('<?php echo SITE_URL?>lsbf/submit_frm','2')">
                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/lsbf-assets/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Make Your Course Enquiry</h4>                           
                            </div> 
                        <div class="col">   
                        <div class="form-group">
                            <input type="text" class="form-control" id="fname2" name="fname" placeholder="First Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                            <span class="help-block" id="fname_err2"></span>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="lname2" name="lname" placeholder="Last Name" onkeyup="chck_valid('lname', 'Please enter correct lastname')" data-attr="Please enter correct lastname">
                            <span class="help-block" id="lname_err2"></span>
                        </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                        <span class="help-block" id="email_err2" ></span>
                    </div>
                    
                    <div class="form-group">
                        <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode2">
                        <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="phone_err2"> </span>
                        
                    </div>
                    
                    <div class="form-group">
                        <select name="cource" id="cource2" class="form-control">
                            <option value="" selected="selected">Course</option>                                                                       
                            <?php foreach($all_array['LSBF_COURCE'] as $k=>$v){ ?>
						<option value="<?php echo $k;?>" > <?php echo $v;?></option>
				<?php }?>                                                           
                        </select>
                        <span class="help-block" id="cource_err2"> </span>
                    </div>                    
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>
                </div>
            </div>
        </form>	
	<div>
    <div class="popup-enquiry-form mfp-hide" id="popupForm">
        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="lsbf_jsfrm('<?php echo SITE_URL?>lsbf/submit_frm','3')">
                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/lsbf-assets/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Make Your Course Enquiry</h4>                           
                            </div> 
                        <div class="col">   
                        <div class="form-group">
                            <input type="text" class="form-control" id="fname3" name="fname" placeholder="First Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                            <span class="help-block" id="fname_err3"></span>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="lname3" name="lname" placeholder="Last Name" onkeyup="chck_valid('lname', 'Please enter correct lastname')" data-attr="Please enter correct lastname">
                            <span class="help-block" id="lname_err3"></span>
                        </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                        <span class="help-block" id="email_err3" ></span>
                    </div>
                    
                    <div class="form-group">
                        <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode3">
                        <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                        <span class="help-block" id="phone_err3"> </span>
                       
                    </div>
                    
                    <div class="form-group">
                        <select name="cource" id="cource3" class="form-control">
                            <option value="" selected="selected">Course</option>                                                                       
                            <?php foreach($all_array['LSBF_COURCE'] as $k=>$v){ ?>
						<option value="<?php echo $k;?>" > <?php echo $v;?></option>
				<?php }?>                                                        
                        </select>
                        <span class="help-block" id="cource_err3"></span>
                    </div>                    
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>
                </div>
            </div>
        </form>
    </div>

	<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
	<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
	<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
	<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
	<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
	
	
	<script src="<?php echo S3_URL?>/site/lsbf-assets/js/vendor.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>  
    <script src="<?php echo S3_URL?>/site/lsbf-assets/js/main.js"></script>
	
	 <script src="<?php echo S3_URL?>/site/scripts/lsbf.js"></script>
   </body>
</html>