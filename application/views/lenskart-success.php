
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Lenskart</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="<?php echo S3_URL?>/site/lenskart/img/favicon.webp">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo S3_URL?>/site/lenskart/styles.css">
</head>

<body>

  <div class="container-fluid paddingZero">

    <div class="container widthOne firstContainer">
      <header>
        <img src="<?php echo S3_URL?>/site/lenskart/img/logo.png" alt="" class="leftLogo">
        <img src="<?php echo S3_URL?>/site/lenskart/img/right-header.png" alt="" class="rightLogo">
      </header>
    </div>

    <section class="headerContent">
      <p class="fistPara">12-Step Eye Test & Try 100+ best selling frames | Book online | </p>
      <p>Currently available for the Age Group 12 to 65 years</p>

    </section>

    <div class="container widthTwo secondContainer">

      <div class="col-md-6 col-sm-12 leftImageSection">

        <img src="<?php echo S3_URL?>/site/lenskart/img/leftImg.png" alt="">

      </div>

      <div class="col-md-6 col-sm-12 formSection paddingZero">

        <form action="" style="background: #98faff; padding: 1em; border-radius: 5px;">

          <h4 style="line-height: 23px; font-weight: 600;"> 
            Thank you for showing interest in Lenskart. Our Executive will contact you shortly.
          </h4>

        </form>

        <p class="cityHead"> <b>Currently Available in -</b> </p>

        <p class="cities">Delhi, Bangalore, Mumbai, Kolkata, Hyderabad, Chennai, Pune and  Ahmedabad.</p>

      </div>

    </div>

   

    <footer class="pageFooter">

  <?php
     $vr = $this->session->userdata('user_id');
     //echo $vr;
     $ut = $this->session->userdata('utm_source');
     if( isset($vr) &&  $vr > 0 && $ut != ""){
       echo $this->lead_check->set_pixel($vr, LENSKART_USER);
       
     }
     ?>
     
      <img src="<?php echo S3_URL?>/site/lenskart/img/footer_new_img.png" alt="">
    </footer>

  </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="<?php echo S3_URL?>/site/lenskart/script.js"></script>

</body>

</html>