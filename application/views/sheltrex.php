
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Sheltrex Karjat | Property in Karjat | 07666767477 | Book 1 and 2 BHK Flats</title>
    <meta name="description" content="Book 1, 2 BHK Property in Karjat. Sheltrex Smart Phone City Project 1 &amp; 2. 100 acres integrated development and 24 Storey high rises towers. To know more call now 07666767477.">
    <meta name="keywords" content="Sheltrex Karjat, 7666767477, Property in Karjat, Sheltrex Smart Phone City, 1bhk in Karjat, 2bhk in Karjat, homes in karjat, Sheltrex Karjat Floorplan, Sheltrex Karjat Possessions, Sheltrex Karjat specifications, Sheltrex Karjat amenities">
    <link rel="shortcut icon" href="<?php echo S3_URL?>/site/sheltrex-assets/images/favicon.ico" type="image/x-icon" />

    <!-- CSS -->
   
    <!-- for banner css files -->
    <link rel="stylesheet" type="text/css" href="<?php echo S3_URL?>/site/sheltrex-assets/css/zoomslider.css" /><!--zoomslider css -->

    <script type="text/javascript" src="<?php echo S3_URL?>/site/sheltrex-assets/js/modernizr-2.6.2.min.js"></script><!--modernizer css -->
  
    <!-- //for banner css files -->
      
    
    
    

    <!-- custom-theme css files -->
    <link href="<?php echo S3_URL?>/site/sheltrex-assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="http://kenwheeler.github.io/slick/slick/slick-theme.css"/>
    <link href="<?php echo S3_URL?>/site/sheltrex-assets/css/vendor.css" rel="stylesheet" type="text/css" media="all" />
    <link href="<?php echo S3_URL?>/site/sheltrex-assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
    <!-- //custom-theme css files-->
   
    <!-- font-awesome-icons -->
    <link href="<?php echo S3_URL?>/site/sheltrex-assets/css/font-awesome.css" rel="stylesheet"> 
    <!-- //font-awesome-icons -->

    <!-- googlefonts -->
    <link href="//fonts.googleapis.com/css?family=Cabin:400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext,vietnamese" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
    <!-- //googlefonts -->

        <!-- Facebook Pixel Code -->
  <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1879563222101418');
    fbq('track', 'PageView');
    fbq('track', 'Lead');
  </script>
  <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1879563222101418&ev=PageView&noscript=1"
  /></noscript>
  <!-- End Facebook Pixel Code -->


  </head>
  <body>
	<!--/main-header-->
	<div id="demo-1" data-zs-src='["<?php echo S3_URL?>/site/sheltrex-assets/images/b1.jpg"]' data-zs-overlay="dots">
		<div class="demo-inner-content">

			<!--/banner-info-->
        <div class="header"> 
          <div class="header-main">
            <div class="container">
              <nav class="navbar navbar-default">
                <div class="navbar-header">
            
                  <h1>
                    <a >
                    <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/sheltrex-karjat-logo.png" />
                    </a>
                  </h1>
                </div>              
              </nav>
              <div class="clearfix"> </div>
            </div>
          </div>
        </div>
        <div class="w3-banner-head-info">
          <div class="container">
            <div class="banner-text">
              <h2>Houses in Karjat Pay 1%* now and no EMI</h2>
			 
              <div class="book-form">
                <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="sheltrex_jsfrm('<?php echo SITE_URL?>sheltrex/sheltrex_frm','1')">
                  <div class="col-md-3 form-time-w3layouts">
                    <label>
                      <i class="fa fa-user" aria-hidden="true"></i> Name</label>
                      <input type="text" id="name1" name="name" placeholder="Name" onkeyup="chck_valid_sheltrex('name', 'Please enter correct name')" data-attr="Please enter correct name">
                      <span class="help-block" id="name_err1"></span>
                  </div>
                  <div class="col-md-3 form-date-w3-agileits">
                    <label>
                      <i class="fa fa-envelope" aria-hidden="true"></i> Email</label>
                      <input type="email"  id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid_sheltrex('email', 'Please enter correct email')" data-attr="Please enter correct email">
                      <span class="help-block" id="email_err1" ></span>
                  </div>
                  
                  <div class="col-md-3 form-left-agileits-w3layouts ">
                    <label>
                      <i class="fa fa-phone-square" aria-hidden="true"></i> Phone</label>
                      <input type="text" class="only_numeric" id="phone1" name="phone" pattern="\d*" maxlength="10" placeholder="Mobile Number" onkeyup="chck_valid_sheltrex('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                      <span class="help-block" id="phone_err1"> </span>
                  </div>
                 
                  <div class="col-md-3 form-left-agileits-submit">
                    <input type="submit" value="Request Call back"  id="frm-sbmtbtn1" name="submit">
                  </div>
                  <div class="clearfix"></div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!--/banner-info-->
      </div>
    </div>
    <!--/banner-section-->
  
     	<!-- about section -->
	<div class="about" id="about">
		<div class="container">
	

			<div class="about-bottom-grid1">
				<div class="col-md-6 bottomgridtext">
        <h3>Why Sheltrex?</h3>
				
					<p>Sheltrex is one of the leading and well-known Real Estate development companies headquartered in Mumbai. It strives to create aspirational developments for its customers with distinctive designs and functional aesthetics. The unique customer-centric designs are planned in such a way that they ensure space efficiency and maximum usability. Sheltrex is known in the market for developing projects using cutting-edge technology, modern architecture, strong project execution and quality construction.</p>
				</div>
				<div class="col-md-6 bottomgridimg">
           <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/exterier-row3-planColor.png" alt="">
				</div>
				<div class="clearfix"></div>
      </div>
      <div class="about-bottom-grid2">
				<div class="col-md-4 bottomgridimg">
					<img src="<?php echo S3_URL?>/site/sheltrex-assets/images/exterier-row2-circlephoto.png" alt="">
				</div>
				<div class="col-md-8 bottomgridtext bottomleft">
					<h3>1 & 2 BHKapartments</h3>
				
          <p>Book yourself a Home in any of our 1 & 2 BHK apartments. Designed by the most trusted architect, Hafeez Contractor, you can be rest assured that space is utilised to the fullest. Moreover, you get everything under one roof! Nature, Amenities, Lifestyle. Make your senses come alive - only at Sheltrex Smart Phone City, Karjat.

</p>
				
				</div>
				<div class="clearfix"></div>
			</div>
  
      
		</div>
	</div>
	<!--// about -->
    	<!-- about section -->
      <div class="about" id="about">
		<div class="container">
			<div class="about-top">
				<div class="col-md-12 text-center ">
					<h3>Sheltrex Smart Phone City 1</h3>
				</div>
				<div class="col-md-12 text">
					<p>Sheltrex Smart Phone City 1 is a meticulously-designed integrated commune with a wide array of facilities. Each apartment is built to offer a one-of-a-kind, luxurious living experience. Large open spaces have been created to greet you while grand walkways bring with them a sense of timeless majesty.</p>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="about-bottom-grid2">
				<div class="col-md-6 bottomgridimg">
					<img src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra5.jpg" alt="">
				</div>
				<div class="col-md-6 bottomgridtext bottomleft">

          <p>It’s time to own a house that’s meant only for a privileged few.</p>
          <p>Here are a few features of this project:</p>
          <ul>
            <li>A Well-Equipped School</li>
            <li>Club House</li>
            <li>Medical Centre/ Hospital*</li>
            <li>Avenue Shopping</li>
            <li>Multi-Screen Movie Theatre</li>
            <li>1.5 km riverside Bollywood-themed Promenade</li>
            <li>South Point Mall</li>
            <li>High-Street Shopping</li>
            <li>A sprawling Playground</li>

          </ul>
				
				</div>
				<div class="clearfix"></div>
			</div>

 
  
      
		</div>
	</div>
	<!--// about -->
  	<!-- about section -->
    <div class="about" id="about">
		<div class="container">
			<div class="about-top">
				<div class="col-md-12 text-center ">
					<h3>Sheltrex Smart Phone City 2</h3>
				</div>
				<div class="col-md-12 text">
					<p>One couldn’t ask for more. With the view of the magnificent Ulhas River on one side and lush green mountains on the other, Sheltrex Smart Phone City 2 comprises two apartment complexes of high-rise towers – Espirana and Paliso. You can also choose any of our G+7 studio apartments at Nano.</p>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="about-bottom-grid2">
				<div class="col-md-4 bottomgridimg">
					<img src="<?php echo S3_URL?>/site/sheltrex-assets/images/espirana.jpg" alt="">
				</div>
				<div class="col-md-8 bottomgridtext bottomleft">
					<div class="property-logo">
             <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/espirano_logo.png" alt="">
          </div>
				
          <p>Espirana is designed for those who aspire to live life to its fullest.</p>
				
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="about-bottom-grid1">
				<div class="col-md-8 bottomgridtext">
        <div class="property-logo">
             <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/paliso.png" alt="">
          </div>
				
					<p>Living spaces at Paliso go beyond mere luxury. Move in to any of our 1, 2 & 3 BHK apartments and make your home your private retreat.</p>
				</div>
				<div class="col-md-4 bottomgridimg">
           <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/paliso1.jpg" alt="">
				</div>
				<div class="clearfix"></div>
      </div>
 
      <div class="about-bottom-grid2">
				<div class="col-md-4 bottomgridimg">
        <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/nano1.jpg" alt="">
				</div>
				<div class="col-md-8 bottomgridtext bottomleft">
        <div class="property-logo">
             <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/nano_logo.jpg" alt="">
          </div>

					<p>
          Nano is built for those who prefer a lifestyle that’s minimalistic yet complete with all necessary amenities.
          </p>
				</div>
				<div class="clearfix"></div>
			</div>
      
		</div>
	</div>
	<!--// about -->
<!-- services -->
<div class="services" id="services">
		<div class="banner-dott">
    <div class="heading">
				<h3>Highlights</h3>
				<p>Here’s why Sheltrex Smart Phone City Karjat 1 & 2 are worth investing in:</p>
			</div>
			<div class="container">
				<div class="services-top-grids">
					<div class="col-md-6">
						<div class="grid1">
              <h4>Education</h4>
              <p>Karjat has several schools and colleges of good repute. Rest assured, when you stay at any of our Sheltrex properties, you won’t have to compromise on your kids’ education. From academic, physical, emotional, spiritual to social learning it's all taken care of!</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="grid1">
              <h4>Lesiure</h4>
              <p>Walk around a beautiful path that’s dotted with tree-lined avenues and benches. Make the most of the club-house, parks, shopping avenues and more – a myriad of options for relaxation and recreation.</p>
						</div>
					</div>

					<div class="clearfix"></div>
				</div>
				<div class="services-bottom-grids">
        <div class="col-md-6">
						<div class="grid1">
              <h4>Security</h4>
              <p>Sheltrex Smart Phone City 1 &amp; 2, Karjat takes into account the latest monitoring technologies and applies it to its projects to take care of your security every step of the way.</p>
						</div>
					</div>
					<div class="col-md-6">
						<div class="grid1">
              <h4>Health</h4>
              <p>A healthy fitness routine can help you ward off many diseases. Workout at any of our designated gymnasiums or take advantage of indoor games at your very own club houses.</p>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
  <!-- //services -->
      	<!-- about section -->
        <div class="about" id="about">
		<div class="container">
			<div class="about-bottom-grid2">
				<div class="col-md-6 bottomgridimg">
					<img src="<?php echo S3_URL?>/site/sheltrex-assets/images/exterier-2.jpg" alt="">
				</div>
				<div class="col-md-6 bottomgridtext bottomleft">
        <h3>Overview (100 Acres)</h3>
        
          <p>Sheltrex Smart Phone City Karjat 1 & 2 is a large integrated development spread across 100 acres. It offers the best of modern amenities with a breathtaking backdrop of natural greenery. Additionally, due to ease of access and connectivity and proposed infrastructure developments by Karjat Municipal Corporation(KMC), properties at Sheltrex Smart Phone City Karjat 1 & 2 are forecasted to appreciate further. The expansive green plains and the hilly terrains at Karjat also make this place a sought-after destination for trekking and tourism. </p>
          
</p>
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="about-bottom-grid1">
				<div class="col-md-5 bottomgridtext" style="padding-right: 0;">
        <h3>Location</h3>
          <p>The last two decades have witnessed remarkable growth towards the North (Borivali, Mira Road, Vasai, Virar) and North East (Mulund, Navi Mumbai, Thane, Kalyan, Badlapur) of Mumbai. With these corridors now saturated, the growth momentum has shifted towards Panvel and nearby areas.</p>
          <p>Soon you will have the: </p>
  
            <p style="margin: 0;">• Upcoming New International Airport</p>
            <p style="margin: 0;">• (NAINA) Navi Mumbai Airport Influence Notified Area Development</p>
            <p style="margin: 0;">• Trans-Harbour Sea Link</p>
          
				</div>
				<div class="col-md-7 bottomgridimg">
           <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/location-map-withword.png" alt="">
				</div>
				<div class="clearfix"></div>
      </div>
 
    
      
		</div>
	</div>
  <!--// about -->
  	<!-- about section -->
    <div class="about" id="about">
		<div class="container">
			<div class="about-top">
				<div class="col-md-12 text-center ">
					<h3>Gallery</h3>
				</div>
        <div class="clearfix"></div>
        <div class="image-gallery ">
         <!-- projects -->
        <div class="gallery" id="projects">
          <div>
              <div class="agile_gallery_grids w3-agile demo">
                <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra1.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra1.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                 <!--End Item -->
                  <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra3.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra3.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                 <!--End Item -->
                <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra4.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra4.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                 <!--End Item -->
                <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra5.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra5.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                <!--End Item -->
                    <!--Start Item -->
                    <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra6.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra6.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                 <!--End Item -->
                <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra7.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra7.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                <!--End Item -->
                <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra8.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra8.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                 <!--End Item -->
                  <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra9.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra9.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                 <!--End Item -->
                <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra10.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra10.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                 <!--End Item -->
                <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra11.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra11.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                <!--End Item -->
                    <!--Start Item -->
                    <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra12.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra12.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                 <!--End Item -->
                <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra13.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra13.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                <!--End Item -->   
                <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra14.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra14.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                 <!--End Item -->
                  <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra15.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra15.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                 <!--End Item -->
                <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra16.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra16.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                 <!--End Item -->
                <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra17.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra17.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                <!--End Item -->
                    <!--Start Item -->
                    <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra18.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra18.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                 <!--End Item -->
                <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra19.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra19.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                <!--End Item -->   
                  <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra21.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra21.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                 <!--End Item -->
                <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra22.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra22.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                 <!--End Item -->
                <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra23.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra23.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                <!--End Item -->
                    <!--Start Item -->
                    <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra24.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra24.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                 <!--End Item -->
                <!--Start Item -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra25.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra25.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                <!--End Item -->   
                  <!--Start Item -->
                  <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
                      <a title="Sheltrex Smart Phone City Karjat" 
                         href="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra26.jpg">
                          <div class="stack twisted">
                            <img  src="<?php echo S3_URL?>/site/sheltrex-assets/images/xtra26.jpg" class="img-responsive" />
                          </div>
                      </a>
                    </div>
                </div>
                <!--End Item -->                                        
              </div>
          </div>
        </div>
        <!-- //projects -->  
        </div>
			</div>
		</div>
	</div>
	<!--// about -->
    	<!-- about section -->
      <div class="about" id="about">
		<div class="container">
			<div class="about-top">
				<div class="col-md-12 text-center ">
					<h3>3R's</h3>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="about-bottom-grid2">
				<div class="col-md-4 bottomgridimg">
					<img src="<?php echo S3_URL?>/site/sheltrex-assets/images/thumb1.jpg" alt="">
				</div>
				<div class="col-md-8 bottomgridtext bottomleft">
				<h3>Reduce (Through Power Management)</h3>
          <p>The estimated consumption of energy at Sheltrex, Karjat will be relatively lower than in a conventional development. Energy will be produced through a solar power plant in the form of solar radiation, which will make the production of solar electricity possible.</p>
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="about-bottom-grid1">
				<div class="col-md-8 bottomgridtext">
          <h3>Reuse (Through Water Management)</h3>
				
					<p>Rain water available from rooftops of buildings, paved and unpaved areas tends to get wasted. This water can be recharged with the help of an aquifer. By doing so we can arrest the flow of ground water and increase the storage within the aquifer. The project will feature dual plumbing systems, where all the waste water will be recycled and reused for flushing and landscaping/gardening purposes, and the rest will be collected for reuse.</p>
				</div>
				<div class="col-md-4 bottomgridimg">
           <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/thumb2.jpg" alt="">
				</div>
				<div class="clearfix"></div>
      </div>
 
      <div class="about-bottom-grid2">
				<div class="col-md-4 bottomgridimg">
        <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/thumb3.jpg" alt="">
				</div>
				<div class="col-md-8 bottomgridtext bottomleft">
        <h3>Recycle (Through Waste Management)</h3>
          

					<p>
          Sheltrex has invested in an OWC (Organic Waste Convertor). This will be used in its Waste Management System to convert large amounts of organic waste i.e. kitchen, garden and food processing waste. into compost. While all organic waste will be used as manure, there will also be paper, bottle and plastic recycling industries.
          </p>
				</div>
				<div class="clearfix"></div>
			</div>
      
		</div>
	</div>
  <!--// about -->
   	<!-- about section -->
     <div class="about" id="about">
		<div class="container">
			<div class="about-top">
				<div class="col-md-12 text-center ">
					<h3>Construction Updates</h3>
				</div>
        <div class="clearfix"></div>
        <div class="construction-gallery col-md-12">
            <div class="slick-slider">
                <div class="slider-image">
                  <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/img1.jpg" alt="">
                </div>  
                <div class="slider-image">
                  <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/img2.jpg" alt="">
                </div>  
                <div class="slider-image">
                  <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/img3.jpg" alt="">
                </div>  
                <div class="slider-image">
                  <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/img4.jpg" alt="">
                </div>  
                <div class="slider-image">
                  <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/img5.jpg" alt="">
                </div>  
                <div class="slider-image">
                  <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/img6.jpg" alt="">
                </div>  
                <div class="slider-image">
                  <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/img7.jpg" alt="">
                </div>  
                <div class="slider-image">
                  <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/img8.jpg" alt="">
                </div>  
                <div class="slider-image">
                  <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/img9.jpg" alt="">
                </div>  
                <div class="slider-image">
                  <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/img10.jpg" alt="">
                </div>  
                <div class="slider-image">
                  <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/img11.jpg" alt="">
                </div>  
                <div class="slider-image">
                  <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/img12.jpg" alt="">
                </div>  
                <div class="slider-image">
                  <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/img13.jpg" alt="">
                </div>  
                <div class="slider-image">
                  <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/img14.jpg" alt="">
                </div>  
                <div class="slider-image">
                  <img src="<?php echo S3_URL?>/site/sheltrex-assets/images/img15.jpg" alt="">
                </div>  
            </div>  
        </div>
			</div>
		</div>
	</div>
	<!--// about -->
  	<!-- Testimonials -->
	<div class="customer" id="customer">
		<div class="container">
			<div class="customer-grids">
				<ul id="">
					<li>
			
						<div class="customer-grid">
							<h4>100-acre**</h4>
							<p>INTEGRATED DEVELOPMENT</p>
						</div>
					</li>
					<li>
		
						<div class="customer-grid">
							<h4>Designed By</h4>
							<p>HAFEEZ CONTRACTOR</p>
						</div>
					</li>
					<li>
		
						<div class="customer-grid">
							<h4>24 Storey</h4>
							<p>HIGH RISE TOWERS</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
  <!-- //Testimonials -->
  	<!-- contact -->
	<div class="mail" id="contact">
		<div class="mail-grid1">
			<div class="container">
				<div class="heading ">
					<h3>Contact us</h3>
				</div>
				<div class="col-md-6 mail-agileits-w3layouts">
					<div class="contact-right">
						<p>MahaRERA No</p>
            <span>P52000012196, P52000004186, P52000010525, P52000008685, P51800012307, P51800012221</span>
            <p>For more information log on to</p>
            <span><a href="https://maharera.mahaonline.gov.in">https://maharera.mahaonline.gov.in</a></span>
					</div>
				</div>
				<div class="col-md-6 mail-agileits-w3layouts">
					<div class="contact-right">
						<p>Sheltrex</p>
						<span>Smart Phone City 1 & 2, Shirse - Akurle, Karjat (W) - 410201  
         </span>
      
          </div>
				</div>
        <div class="clearfix"></div>
      
			</div>
    </div>
    <div class="agileits-w3layouts-copyright">
				<p>© 2018  All rights reserved 
				</p>
			</div>
	</div>
	<!-- //contact -->

	<!-- footer -->

    <!-- contact form start -->
    <div class="floating-form book-form custom-form visiable" id="contact_form">
    <div class="contact-opener">Enquire Now</div>
      <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="sheltrex_jsfrm('<?php echo SITE_URL?>sheltrex/sheltrex_frm','2')">
          <div class="col-md-12">
            <h4 class="custom-title">Houses In Karjat Pay 1%* Now And No EMI</h4>
          </div>
          <div class="col-md-12 form-time-w3layouts">
          <label>
            <i class="fa fa-user" aria-hidden="true"></i> Name</label>
            <input type="text" id="name2" name="name" placeholder="Name" onkeyup="chck_valid_sheltrex('name', 'Please enter correct name')" data-attr="Please enter correct name">
            <span class="help-block" id="name_err2"></span>
        </div>
        <div class="col-md-12 form-date-w3-agileits">
          <label>
            <i class="fa fa-envelope" aria-hidden="true"></i> Email</label>
            <input type="email"  id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid_sheltrex('email', 'Please enter correct email')" data-attr="Please enter correct email">
            <span class="help-block" id="email_err2" ></span>
        </div>
        
        <div class="col-md-12 form-left-agileits-w3layouts ">
          <label>
            <i class="fa fa-phone-square" aria-hidden="true"></i> Phone</label>
            <input type="text" class="only_numeric" id="phone2" name="phone" pattern="\d*" maxlength="10" placeholder="Mobile Number" onkeyup="chck_valid_sheltrex('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
            <span class="help-block" id="phone_err2"> </span>
        </div> 
        <div class="col-md-12 form-left-agileits-submit">
          <input type="submit" value="Request Call back"  id="frm-sbmtbtn2" name="submit">
        </div>
        <div class="clearfix"></div>
      </form>
    <div>
    <div class="popup-enquiry-form book-form custom-form mfp-hide" id="popupForm">
      <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="sheltrex_jsfrm('<?php echo SITE_URL?>sheltrex/sheltrex_frm','3')">
      <div class="col-md-12">
            <h4 class="custom-title">Houses In Karjat Pay 1%* Now And No EMI</h4>
          </div>
      <div class="col-md-12 form-time-w3layouts">
          <label>
            <i class="fa fa-user" aria-hidden="true"></i> Name</label>
            <input type="text" id="name3" name="name" placeholder="Name" onkeyup="chck_valid_sheltrex('name', 'Please enter correct name')" data-attr="Please enter correct name">
            <span class="help-block" id="name_err3"></span>
        </div>
        <div class="col-md-12 form-date-w3-agileits">
          <label>
            <i class="fa fa-envelope" aria-hidden="true"></i> Email</label>
            <input type="email"  id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid_sheltrex('email', 'Please enter correct email')" data-attr="Please enter correct email">
            <span class="help-block" id="email_err3" ></span>
        </div>
        
        <div class="col-md-12 form-left-agileits-w3layouts ">
          <label>
            <i class="fa fa-phone-square" aria-hidden="true"></i> Phone</label>
            <input type="text" class="only_numeric" id="phone3" name="phone" pattern="\d*" maxlength="10" placeholder="Mobile Number" onkeyup="chck_valid_sheltrex('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
            <span class="help-block" id="phone_err3"> </span>
        </div> 
        <div class="col-md-12 form-left-agileits-submit">
          <input type="submit" value="Request Call back"  id="frm-sbmtbtn3" name="submit">
        </div>
        <div class="clearfix"></div>        
      </form>
    </div> 


	<!-- //footer -->
	 <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
     <input type="hidden" id="utm_leadsource" name="utm_leadsource"  value="<?php echo (isset($_REQUEST['utm_leadsource']) != "" ? $_REQUEST['utm_leadsource'] : "Adcanopus"); ?>">
	<input type="hidden" id="utm_category" name="utm_category" value="<?php echo (isset($_REQUEST['utm_category']) != "" ? $_REQUEST['utm_category'] : "Digital"); ?>">
	<input type="hidden" id="utm_camp1" name="utm_camp1" value="<?php echo (isset($_REQUEST['utm_camp1']) != "" ? $_REQUEST['utm_camp1'] : "email"); ?>">
	<input type="hidden" id="utm_camp2" name="utm_camp2" value="<?php echo (isset($_REQUEST['utm_camp2']) != "" ? $_REQUEST['utm_camp2'] : "Adcanopus"); ?>">
	<input type="hidden" id="utm_mobile1" name="utm_mobile1" value="<?php echo (isset($_REQUEST['utm_mobile1']) != "" ? $_REQUEST['utm_mobile1'] : "mobile"); ?>">

    <!-- JavaScript -->
    <!-- js -->
    <script type="text/javascript" src="<?php echo S3_URL?>/site/sheltrex-assets/js/jquery-2.1.4.min.js"></script>
    <!-- for bootstrap working -->
      <script src="<?php echo S3_URL?>/site/sheltrex-assets/js/bootstrap.js"></script>
    <!-- //for bootstrap working -->
    <!-- //js -->
      

    <!-- for banner js file-->
    <script type="text/javascript" src="<?php echo S3_URL?>/site/sheltrex-assets/js/jquery.zoomslider.min.js"></script><!-- zoomslider js -->
    <!-- //for banner js file-->

    <!-- for team slider -->

    <!-- //for team slider -->



    <!-- for smooth scrolling -->
    <script src="<?php echo S3_URL?>/site/sheltrex-assets/js/SmoothScroll.min.js"></script>
      <script type="text/javascript" src="<?php echo S3_URL?>/site/sheltrex-assets/js/move-top.js"></script>
      <script type="text/javascript" src="<?php echo S3_URL?>/site/sheltrex-assets/js/easing.js"></script>
      <!-- here stars scrolling icon -->
      <script type="text/javascript">
        $(document).ready(function() {
          /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear' 
            };
          */
                    
          $().UItoTop({ easingType: 'easeOutQuart' });
                    
          });
      </script>
      <!-- //here ends scrolling icon -->
    <!-- //for smooth scrolling -->

    <!-- scrolling script -->
    <script type="text/javascript">
      jQuery(document).ready(function($) {
        $(".scroll").click(function(event){		
          event.preventDefault();
          $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
      });
    </script> 
    <!-- //scrolling script -->

    <!-- testimonilas slider -->
            <script type="text/javascript">
                  $(window).load(function() {
                    $("#flexiselDemo1").flexisel({
                      visibleItems: 3,
                      animationSpeed: 1000,
                      autoPlay: false,
                      autoPlaySpeed: 3000,    		
                      pauseOnHover: true,
                      enableResponsiveBreakpoints: true,
                      responsiveBreakpoints: { 
                        portrait: { 
                          changePoint:480,
                          visibleItems: 1
                        }, 
                        landscape: { 
                          changePoint:640,
                          visibleItems:3
                        },
                        tablet: { 
                          changePoint:768,
                          visibleItems: 3
                        }
                      }
                    });
                    
                  });
              </script>
    <script type="text/javascript" src="<?php echo S3_URL?>/site/sheltrex-assets/js/jquery.flexisel.js"></script>
    <!-- testimonilas slider -->
    <script src="<?php echo S3_URL?>/site/sheltrex-assets/js/vendor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="<?php echo S3_URL?>/site/sheltrex-assets/js/main.js"></script>
	<script src="<?php echo S3_URL?>/site/scripts/default.js"></script> 
  </body>
</html>