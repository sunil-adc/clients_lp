<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Apartments in Yelahanka, Apartments near Hebbal, Bangalore North | Mittal Elanza</title>
<meta name="description" content="Mittal Elanza offers luxury homes as 2, 2.5 & 3 BHK apartments near Manyata Tech Park, apartments in Yelahanka, flats in Kogilu & provides flats for sale in Yelahanka.">
<meta name="keywords" content="apartments in yelahanka, apartments near manyata tech park, apartments in north bangalore, flats in kogilu, flats for sale in yelahanka, 2 & 3 bhk flats in yelahanka, new properties in bangalore, new projects in north bangalore">
<meta name="google-site-verification" content="0dbmPCmO4667-u3ow1vZI8ma61UpYpOFJIso_aqvUzs" />
<link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/css/vendor.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
<link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/css/main.css">
<!-- fonts -->
<link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
            rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
</head>
<body>
<?php $all_array = all_arrays(); ?>
<div class="header-top">
  <p> <i class="fa fa-info-circle" aria-hidden="true"></i> RERA Approval No.PRM/KA/RERA/1251/309/PR/171015/000683 </p>
</div>
<!-- banner -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <div class="">
        <div class="carousel-caption">
          <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)">
            <div class="">
             <h4>Thank you for expressing interest on our Properties
                            Our expert will get in touch with you shortly.</h4>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  
  <!-- The Modal --> 
</div>
<!--//banner --> 
<!-- about -->
<div class="about" id="about">
  <div class="container">
    <div class="col-md-6 about_right">
      <h3>ABOUT</h3>
      <h3 class="bold">Mittal Elanza</h3>
      <p> Mittal Universal has been a pioneer in the real estate industry for over 60 years and has constructed over 50 million sq. ft. of space. Since 1952, the Group's quest has been to bring about innovation, strong project execution and quality construction with redefined living - delivering cutting-edge practices at par with the best in the world. This spirit of true entrepreneurship has evolved Mittal Universal into a leading conglomerate which has constructed over 2,000 buildings across India. </p>
      <p> As part of this legacy, Mittal Universal has comprised a unique mix of forward thinkers and functional creators who strike a balance between local needs and global influences in the development of every property. Aware that knowledge is constantly evolving and critical to innovative growth, Mittal Universal believes in bringing together visionaries in order to implement an effective and time-tested combination of creativity and quality which culminates into landmark properties. This sharp understanding of market dynamics and latest construction trends has enabled Mittal Universal to redesign and redefine the skyline of one of the fastest growing economies of the world. </p>
    </div>
    <div class="col-md-6 about-left">
      <div class="col-xs-12 aboutimg-w3l aboutimg-w3l2">
        <div class="rwd-media">
          <iframe src="https://www.youtube.com/embed/INVizqo_is0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
      </div>
      <div class="clearfix"> </div>
    </div>
    <div class="clearfix"> </div>
  </div>
</div>
<!-- //about --> 
<!-- popular -->
<div class="popular-w3" id="popular">
  <div class="container">
    <h3 class="title">Property Description</h3>
    <div class="popular-grids">
      <div class="">
        <div class="popular-text" style="text-align:left!important;">
          <div class="detail-bottom background-white" style="">
            <p>Do it all and more at Mittal Elanza, where life is a celebration of where you’ve come & what you’ve achieved.</p>
            <p>Designed by the world renowned Architect Hafeez Contractor, Elanza enthrals you with its imposing structure, surreal façade, inviting homes, unmatched location and world-class amenities.</p>
            <p>The smart planning ensures optimum use of space, steady flow of natural light and breath-taking views of the locale from each and every one of the 2 & 3 BHK Flats in Yelahnka.</p>
            <p>So, step into your home at Mittal Elanza and live more.</p>
          </div>
        </div>
      </div>
      <div class="clearfix"><br>
      </div>
      <h3 class="title">Property Specification</h3>
      <div class="popular-grids">
        <div class="">
          <div class="popular-text">
            <div class="detail-bottom background-white">
              <div class="col-sm-12">
                <div class="col-sm-4">
                  <div id="sub-box"> <i class="fa fa fa-arrow-circle-o-right"></i> <span><strong>Structure</strong></span>
                    <ul class="desc_ul" style="margin:5px 0px 10px 16px; font-size:13px;">
                      <li style="">RCC framed structure</li>
                      <li style="">Car park in basement, stilt and ground Finishes</li>
                    </ul>
                  </div>
                  <div id="sub-box"> <i class="fa fa fa-arrow-circle-o-right"></i> <span><strong>Bathrooms</strong></span>
                    <ul class="desc_ul" style="margin:5px 0px 10px 16px; font-size:13px;">
                      <li style=" ">Matt finish anti-skid vitrified tile flooring</li>
                      <li style=" ">Vitrified wall tiling upto false ceiling</li>
                      <li style="position: absolute; left: 0px; top: 32px; display: none;">False ceiling</li>
                    </ul>
                  </div>
                  <div id="sub-box"> <i class="fa fa fa-arrow-circle-o-right"></i> <span><strong>Sanitary &amp; Plumbing</strong></span>
                    <ul class="desc_ul" style="margin:5px 0px 10px 16px; font-size:13px;">
                      <li style=" ">Chromium plated fittings - Grohe or equivalent</li>
                      <li style="position: absolute; left: 0px; top: 32px; display: none;">Sanitary fixtures - Toto or equivalent</li>
                    </ul>
                  </div>
                  <div id="sub-box"> <i class="fa fa fa-arrow-circle-o-right"></i> <span><strong>Common Areas</strong></span>
                    <ul class="desc_ul" style="margin:5px 0px 10px 16px; font-size:13px;">
                      <li style=" ">Marble/granite flooring and skirting for ground floor lobby</li>
                      <li style="position: absolute; left: 0px; top: 32px; display: none;">Granite flooring and skirting for upper floors</li>
                      <li style="position: absolute; left: 0px; top: 48px; display: none;">Oil bound distemper/textured paint for walls</li>
                      <li style="position: absolute; left: 0px; top: 64px; display: none;">Oil bound distemper paint for ceiling</li>
                      <li style="position: absolute; left: 0px; top: 80px; display: none;">MS handrail</li>
                    </ul>
                  </div>
                  <div id="sub-box"> <i class="fa fa fa-arrow-circle-o-right"></i> <span><strong>Air Conditioning</strong></span>
                    <ul class="desc_ul" style="margin:5px 0px 10px 16px; font-size:13px;">
                      <li style=" ">Provision for split type AC for all rooms with electrical points in living /dining and master bedroom only</li>
                    </ul>
                  </div>
                  <div id="sub-box"> <i class="fa fa fa-arrow-circle-o-right"></i> <span><strong>D.G.</strong></span>
                    <ul class="desc_ul" style="margin:5px 0px 10px 16px; font-size:13px;">
                      <li style=" ">100% back-up for all common areas and units</li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div id="sub-box"> <i class="fa fa fa-arrow-circle-o-right"></i> <span><strong>Foyer / Living / Dining</strong></span>
                    <ul class="desc_ul" style="margin:5px 0px 10px 16px; font-size:13px;">
                      <li style=" ">Vitrified tile flooring and skirting</li>
                      <li style=" ">Emulsion paint for ceiling and walls</li>
                    </ul>
                  </div>
                  <div id="sub-box"> <i class="fa fa fa-arrow-circle-o-right"></i> <span><strong>Kitchen</strong></span>
                    <ul class="desc_ul" style="margin:5px 0px 10px 16px; font-size:13px;">
                      <li style=" ">Vitrified tile flooring</li>
                      <li style=" ">Ceramic wall tiling 2 ft above counter levels</li>
                      <li style="position: absolute; left: 0px; top: 32px; display: none;">Emulsion paint for ceiling and walls</li>
                      <li style="position: absolute; left: 0px; top: 48px; display: none;">Granite counter and SS sink with drain board</li>
                      <li style="position: absolute; left: 0px; top: 64px; display: none;">Provision for exhaust</li>
                      <li style="position: absolute; left: 0px; top: 80px; display: none;">Provision for water purifier</li>
                    </ul>
                  </div>
                  <div id="sub-box"> <i class="fa fa fa-arrow-circle-o-right"></i> <span><strong>Common Staircase</strong></span>
                    <ul class="desc_ul" style="margin:5px 0px 10px 16px; font-size:13px;">
                      <li style=" ">Granite for landing, treads &amp; risers from basement to first floor</li>
                      <li style="position: absolute; left: 0px; top: 32px; display: none;">Kota for landing, treads &amp; risers in upper floors</li>
                      <li style="position: absolute; left: 0px; top: 64px; display: none;">Oil bound distemper paint for walls and ceiling</li>
                      <li style="position: absolute; left: 0px; top: 96px; display: none;">MS handrail</li>
                    </ul>
                  </div>
                  <div id="sub-box"> <i class="fa fa fa-arrow-circle-o-right"></i> <span><strong>Telephone/Data Points</strong></span>
                    <ul class="desc_ul" style="margin:5px 0px 10px 16px; font-size:13px;">
                      <li style=" ">In living and master bedroom</li>
                    </ul>
                  </div>
                  <div id="sub-box"> <i class="fa fa fa-arrow-circle-o-right"></i> <span><strong>Lifts</strong></span>
                    <ul class="desc_ul" style="margin:5px 0px 10px 16px; font-size:13px;">
                      <li style=" ">Mitsubishi/Schindler or equivalent</li>
                    </ul>
                  </div>
                  <div id="sub-box"> <i class="fa fa fa-arrow-circle-o-right"></i> <span><strong>TV Points</strong></span>
                    <ul class="desc_ul" style="margin:5px 0px 10px 16px; font-size:13px;">
                      <li style=" ">In living and all bedrooms</li>
                    </ul>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div id="sub-box"> <i class="fa fa fa-arrow-circle-o-right"></i> <span><strong>Bedrooms</strong></span>
                    <ul class="desc_ul" style="margin:5px 0px 10px 16px; font-size:13px;">
                      <li style=" ">Vitrified tile flooring and skirting</li>
                      <li style=" ">Emulsion paint for ceiling and walls</li>
                    </ul>
                  </div>
                  <div id="sub-box"> <i class="fa fa fa-arrow-circle-o-right"></i> <span><strong>Balconies / Utilities</strong></span>
                    <ul class="desc_ul" style="margin:5px 0px 10px 16px; font-size:13px;">
                      <li style=" ">Matt finish vitrified tile flooring and skirting</li>
                      <li style=" ">Cement based paint for ceiling</li>
                      <li style="position: absolute; left: 0px; top: 32px; display: none;">External emulsion / textured paint for walls</li>
                      <li style="position: absolute; left: 0px; top: 48px; display: none;">Main door - hard wood frame and Masonite skin shutter with melamine polish</li>
                      <li style="position: absolute; left: 0px; top: 80px; display: none;">Internal doors - hard wood frame with both side Masonite skin shutter with paint</li>
                      <li style="position: absolute; left: 0px; top: 112px; display: none;">Toilet doors - hard wood frame with Masonite skin shutter on external side and laminated on the internal side</li>
                      <li style="position: absolute; left: 0px; top: 160px; display: none;">Aluminum/UPVC glazed windows</li>
                    </ul>
                  </div>
                  <div id="sub-box"> <i class="fa fa fa-arrow-circle-o-right"></i> <span><strong>Electrical</strong></span>
                    <ul class="desc_ul" style="margin:5px 0px 10px 16px; font-size:13px;">
                      <li style=" ">All products like switches &amp; accessories, PVC Conduit, switch gear, wiring cable, etc., are of reputed make &amp; confirming to ISI standards</li>
                      <li style="position: absolute; left: 0px; top: 48px; display: none;">5 KW EB power supply for 2 BHK apartments</li>
                      <li style="position: absolute; left: 0px; top: 64px; display: none;">7 KW EB power supply for 3 BHK apartments</li>
                    </ul>
                  </div>
                  <div id="sub-box"> <i class="fa fa fa-arrow-circle-o-right"></i> <span><strong>Security</strong></span>
                    <ul class="desc_ul" style="margin:5px 0px 10px 16px; font-size:13px;">
                      <li style=" ">CCTV in common areas</li>
                      <li style=" ">Intercom system</li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
<!-- //popular --> 

<!-- projects -->
<div class="" id="projects" style="padding:50px 0px;">
  <div class="container">
    <h3 class="title">NEIGHBORHOOD</h3>
    <div class="col-md-4 col-sm-12 col-xs-12">
      <div class="gallery-grid1"><i class="fa fa-road"></i>
        <div class="p-mask">
          <h4>NH7 ELEVATED HIGHWAY</h4>
          <br>
          <p>2 Mins to Signal-free corridor comprising a stretch of 6 lanes elevated expressway, connecting Hebbal flyover to the Kempegowda International Airport Bengaluru within 20 mins. </p>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12">
      <div class="gallery-grid1"><i class="fa fa-plus"></i>
        <div class="p-mask">
          <h4>HOSPITALS</h4>
          <br>
          <p>Within 5 -10 to from Cauvery hospital and Columbia Asia hospital which is a state of the art modern hospital. </p>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12">
      <div class="gallery-grid1"><i class="fa fa-shopping-cart"></i>
        <div class="p-mask">
          <h4>ENTERTAINMENT</h4>
          <br>
          <p>5 -10 Mins to Galleria Mall, August MallL & MSR Elements Mall which are abound with PVR and INOX cinema hall, hypermarket, life style stores, fast food and fine dining, supermarkets and Star rated Hotels. </p>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12">
      <div class="gallery-grid1"><i class="fa fa-shopping-cart"></i>
        <div class="p-mask">
          <h4>EDUCATIONAL INSTITUTIONS</h4>
          <br>
          <p>Within 10 - 15 Mins to 7-8 prominent national and International educational intuitions like</p>
        
          <li style="font-size:14px;     margin-top: 5px;">Vidhya Shilp</li>
          <li style="font-size:14px;    margin-top: 5px;">Vidhya Niketan</li>
          <li style="font-size:14px;    margin-top: 5px;">Delhi Public School</li>
          <li style="font-size:14px;    margin-top: 5px;">Mallya Aditi</li>
          <li style="font-size:14px;    margin-top: 5px;">Rayan International School</li>
          <li style="font-size:14px;    margin-top: 5px;">Canadian International School</li>
         
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12">
      <div class="gallery-grid1"><i class="fa fa-shopping-cart"></i>
        <div class="p-mask">
          <h4>PUBLIC TRANSPORT</h4>
          <br>
       
          <li style="font-size:14px;    margin-top: 5px;">Bengaluru Metro: 2 Mins to proposed Metro rail Station, which gives you a great connectivity to any part of Bengaluru city</li>
          <li style="font-size:14px;    margin-top: 5px;">Mono Rail: 10-15 Mins to proposed Mono Rail Terminal Station at Hebbal outer ring road. </li>
          <li style="font-size:14px;    margin-top: 5px;">Bus Stop: 100 Meters away from Kogilu Bus Stop. </li>
          <li style="font-size:14px;    margin-top: 5px;">Airport Shuttle: 2 Mins to Airport Shuttle Stop at Kogilu Cross, which makes you reach the Airport within 20 Mins.</li>
          
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12">
      <div class="gallery-grid1"><i class="fa fa-shopping-cart"></i>
        <div class="p-mask">
          <h4>IT PARKS</h4>
          <br>
          <p>5 -10 Mins to Kirlosker Business Park and Manyata Tech Park which are home to many IT firms like IBM, Philips, Target, Cognizant, Aditi, Fidelity, Microsoft and mixed use office development with commercial amenities that include a retail wing with F&B outlets, shopping, health and fitness and a business centre. </p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- //projects -->
<div class="simple-bg"> </div>

<!-- projects -->
<div class="gallery" id="projects">
  <div class="container">
    <h3 class="title">AMENITIES</h3>
    <div class="agile_gallery_grids w3-agile demo">
      <div class="col-md-6 col-sm-6 col-xs-6 gal-sec">
        <div class="gallery-grid1"> <a title="Vajram mittalelanza" href="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/clubhouse.jpg">
          <div class="stack twisted"> <img  src="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/clubhouse.jpg" alt=" " class="img-responsive" /> <br>
            <h3>Club House </h3>
            <p>You won’t have to travel an inch to mingle and be entertained. The rooftop clubhouse will become a standout feature to your social calendar with recreational facilities such as a health club, aerobics / yoga room, games room and spa.</p>
          </div>
          </a> </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-6 gal-sec">
        <div class="gallery-grid1"> <a title="Vajram mittalelanza" href="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/skygarden.jpg">
          <div class="stack twisted"> <img  src="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/skygarden.jpg" alt=" " class="img-responsive" /> <br>
            <h3>Sky Garden</h3>
            <p>Experience the rejuvenating ambience of the beautifully laid out landscape sky garden with the stars for the company. The sky garden offers an ideal setting for conversations, leisurely walks, a dip in the infinity swimming pool as well as a refreshing environment to unwind after a busy day.</p>
          </div>
          </a> </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-6 gal-sec">
        <div class="gallery-grid1"> <a title="Vajram mittalelanza" href="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/infinitypool.jpg">
          <div class="stack twisted"> <img  src="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/infinitypool.jpg" alt=" " class="img-responsive" /> <br>
            <h3>Infinity Pool </h3>
            <p>Welcome to your paradise in the sky. A place where you can lounge with stars, indulge your love for the finer things in life, with a lavish pool offering you a stunning infinity view of the cityscape. </p>
          </div>
          </a> </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-6 gal-sec">
        <div class="gallery-grid1"> <a title="Vajram mittalelanza" href="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/security.jpg">
          <div class="stack twisted"> <img  src="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/security.jpg" alt=" " class="img-responsive" /> <br>
            <h3>Security </h3>
            <p>Security at Mittal Elanza is state-of-the-art designed to keep you and your loved ones safe. Trained personnel provide the complex with multi-level security. A gated entry system checks and monitors the arrival and departure of visitors. The property is also equipped with the latest firefighting equipment, smoke detectors, CCTV cameras and intercom systems. </p>
          </div>
          </a> </div>
      </div>
    </div>
  </div>
</div>
<!-- //projects --> 
 
</div>
<!-- projects -->
<div class="gallery" id="projects">
  <div class="container">
    <h3 class="title">Plan</h3>
    <div class="agile_gallery_grids w3-agile demo">
      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
        <div class="gallery-grid1"> <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/type-a.jpg"> <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/type-a.jpg" alt=" " class="img-responsive" /> </a> </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
        <div class="gallery-grid1"> <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/type-b.jpg"> <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/type-b.jpg" alt=" " class="img-responsive" /> </a> </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
        <div class="gallery-grid1"> <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/type-b2.jpg"> <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/type-b2.jpg" alt=" " class="img-responsive" /> </a> </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
        <div class="gallery-grid1"> <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/type-b1.jpg"> <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/type-b1.jpg" alt=" " class="img-responsive" /> </a> </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
        <div class="gallery-grid1"> <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/type-b3.jpg"> <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/type-b3.jpg" alt=" " class="img-responsive" /> </a> </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
        <div class="gallery-grid1"> <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/type-b4.jpg"> <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/type-b4.jpg" alt=" " class="img-responsive" /> </a> </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
        <div class="gallery-grid1"> <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/type-b5.jpg"> <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/type-b5.jpg" alt=" " class="img-responsive" /> </a> </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
        <div class="gallery-grid1"> <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/type-b6.jpg"> <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/type-b6.jpg" alt=" " class="img-responsive" /> </a> </div>
      </div>
    </div>
  </div>
</div>
<!-- //projects --> 

<!-- contact -->
<div class="address" id="contact">
  <div class="container">
    <h3 class="title">Contact Us</h3>
    <div class="address-row">
      <div class="col-md-12 col-xs-12  wow agile fadeInLeft animated" data-wow-delay=".5s">
        <div class="address-info wow fadeInDown animated" data-wow-delay=".5s">
          <h4>Map</h4>
          <img src="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/images/map-img.jpg" /> </div>
      </div>
      <div class="col-md-6 col-xs-6 address-right">
        <div class="address-info wow fadeInRight animated" data-wow-delay=".5s">
          <h4>MUMBAI OFFICE</h4>
          <p>Mittal Court 144, C Wing 14th Floor, 224 Nariman Point Mumbai - 400 021 <br>
            TEL: (91-22) 4347 3090 / (91-22) 4347 3091 <br>
            FAX: (91-22) 4347 3093 </p>
        </div>
      </div>
      <div class="col-md-6 col-xs-6 address-right">
        <div class="address-info wow fadeInRight animated" data-wow-delay=".5s">
          <h4>BENGALURU OFFICE</h4>
          <p>Pranava Park 16(old no 11/1), Infantry Road Municipal ward no. 78 Bengaluru - 560 001 <br>
            TEL: (91-80) 4219 0090 / (91-80) 4219 0091 <br>
            FAX: (91-80) 4219 0094 </p>
        </div>
      </div>
      <div class="col-md-12 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
        <div class="address-grid"> 
          <!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
          <div id="location">
            <iframe style="border:0;width: 100%;height: 300px;"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3885.931139764101!2d77.61810509947918!3d13.103548800239958!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae19a78fe121df%3A0x6798275f8e8e29d1!2sMittal+Elanza!5e0!3m2!1sen!2sin!4v1553511406225"  allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--//contact-->
<footer>
  <div class="copy-right-grids">
    <p class="footer-gd">Copyright © 2018 Mittal Elanza | Maintained by Adcanopus</p>
  </div>
</footer>

     <div id="pi">
       <?php
           $vr = $this->session->userdata('user_id');
           $ut = $this->session->userdata('utm_source');
           if( isset($vr) &&  $vr > 0 && $ut != ""){
               echo $this->lead_check->set_pixel($vr, REALESTATE_USER);
               
           }
       ?>
      </div> 

<div class="floating-form visiable" id="contact_form">
</div>
<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
<script src="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/js/vendor.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script> 
<script src="<?php echo S3_URL?>/site/realestate-assets/mittalelanza/js/main.js"></script> 

</body>
</html>