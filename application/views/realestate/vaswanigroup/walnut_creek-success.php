<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Villas at Walnut Creek – Vaswani</title>
    <meta name="description" content="Developers of residential and commercial real estate across Bangalore, Mumbai, Pune and Goa.">
    <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/favicon.png" type="image/x-icon" />


    <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/css/vendor.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/css/main.css">
    <!-- fonts -->
    <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
        rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
</head>

<body>

    <?php $all_array = all_arrays(); ?>

    <!-- banner -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="header-banner header-banner-desktop"> 
                        <img src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/desktop-banner.jpg" />
                </div>
                <div class="header-banner header-banner-mobile"> 
                        <img src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/mobile-banner.jpg" />
                </div>
                <div class="">
                    <div class="carousel-caption"> 
                        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="puravankara_jsfrm_walnutcreek('<?php echo SITE_URL?>realestate/puravankara_provident/frm_submit','walnutcreek','1')">

                            <div class="">
                                <div class="col">
                                    <div class="form-logo">
                                        <a href="#">
                                            <img src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/logo-blue.png" />
                                        </a>
                                    </div> 
                                    <h4>Thank you for expressing interest on our Properties
                            Our expert will get in touch with you shortly.</h4>

                                </div>

                            </div>
                        </form>
                       
                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal -->
    </div>
    <!--//banner -->
             
    <!-- about -->
    <div class="about" id="about">
        <div class="container">
         
            <div class="col-md-6 about_right">
                <h3 class="bold">VILLAS AT WALNUT CREEK</h3>
                <p>
                A welcome oasis in Bangalore’s urban chaos. Uber spacious villas and townhouses  with internationally styled clubhouse and world-class amenities developed on almost 3.25 acres of land
                </p>
                <ul>
                <li>Just 22 Villas &amp; 8 Townhouses</li>
                <li>4 BHK Villas ranging from 3747 sft to 4943 sft</li>
                <li>3 BHK Townhouse of 3030 sft&nbsp;and 3148 sft</li>
                <li>Upto 3,000sft of garden &amp; terrace area per villa</li>
                <li>OC Received</li>
                </ul>
                <p>We invite you to Vaswani Walnut Creek for an Exclusive Private tour of the project.</p>
                <p>Project Location : Off Sarjapur Main Road, Near Wipro Bangalore.</p>



            </div>
            <div class="col-md-6 about-left">
                <div>
                    <img src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/ab5.jpg" alt="image" style="object-position: right;width: 100%;" />
                </div>

            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <!-- //about -->




  




    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">GALLERY</h3>
            <div class="agile_gallery_grids w3-agile demo">

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="walnutcreek BY PROVIDENT" href="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g1.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g1.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="walnutcreek BY PROVIDENT" href="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g2.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g2.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="walnutcreek BY PROVIDENT" href="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g3.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g3.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="walnutcreek BY PROVIDENT" href="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g4.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g4.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>                
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="walnutcreek BY PROVIDENT" href="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g5.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g5.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="walnutcreek BY PROVIDENT" href="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g6.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g6.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="walnutcreek BY PROVIDENT" href="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g7.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g7.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="walnutcreek BY PROVIDENT" href="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g8.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g8.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="walnutcreek BY PROVIDENT" href="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g9.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g9.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="walnutcreek BY PROVIDENT" href="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g10.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/g10.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- //projects -->
    <!-- projects -->
    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">Plan</h3>
            <div class="agile_gallery_grids w3-agile demo">
                <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/masterplan.jpg">
                            <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/masterplan.jpg"
                                alt=" " class="img-responsive" />
                        </a>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/unit-plan.jpg">
                            <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/unit-plan.jpg"
                                alt=" " class="img-responsive" />
                        </a>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/unit-plan-2.jpg">
                            <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/images/unit-plan-2.jpg"
                                alt=" " class="img-responsive" />
                        </a>
                    </div>
                </div>
           
        
    

            </div>
        </div>
    </div>
    <!-- //projects -->


    <!-- contact -->
    <div class="address" id="contact">
        <div class="container">
            <h3 class="title">Contact Us</h3>
            <div class="address-row">
              
                <div class="col-md-12 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
                    <div class="address-grid">
                        <!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
                        <div id="location">
                       
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15556.839641879404!2d77.6939122!3d12.8942198!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x368f0801610761ed!2sVaswani+Walnut+Creek+Villas!5e0!3m2!1sen!2sin!4v1546601874947" style="border:0;width: 100%;height: 350px;" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                
               

            </div>
        </div>
    </div>

    <div id="pi">
       <?php
       $vr = $this->session->userdata('user_id');
       $ut = $this->session->userdata('utm_source');
       if( isset($vr) &&  $vr > 0 && $ut != ""){
           echo $this->lead_check->set_pixel($vr, VASWANIGROUP);
           
       }
       ?>
      </div> 
    <!--//contact-->
    <footer>
        <div class="copy-right-grids">
            <p class="footer-gd">© 2019 Vaswanigroup. All rights reserved.</p>
        </div>
    </footer>
    
       




            <script src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/js/vendor.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
            <script src="<?php echo S3_URL?>/site/realestate-assets/walnutcreek/js/main.js"></script>
            <script src="<?php echo S3_URL?>/site/scripts/default.js"></script>
            <script src="<?php echo S3_URL?>/site/scripts/realesate.js"></script>
</body>

</html>