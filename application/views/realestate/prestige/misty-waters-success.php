<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8"> 
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Prestige Misty Waters- Vista Tower</title>
      <meta name="keywords" content="Prestige Misty Waters- Vista Tower" >
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/favicon.png" type="image/x-icon" />


	  <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/mistywaters/css/vendor.css">
	  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/mistywaters/css/main.css">    



   </head>
   <body> 
 
    <?php $all_array = all_arrays(); ?>
	<!-- banner -->
	<div id="home" class="w3ls-banner cd-section">
		<div class="banner-info">
			<!-- header -->
			<div class="header-w3layouts">
				<div class="container">
				
				</div>
			</div>
			<!-- //header --> 
			<!-- banner-text -->
			<div class="container banner-w3ltext"> 
                
			<div>
			<?php $all_array = all_arrays(); ?>
            <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="puravankara_jsfrm_mistywaters('<?php echo SITE_URL?>realestate/puravankara_provident/frm_submit','mistywaters','1')">
            <div class="form-logo">
						<a href="#">
                             <img src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/logo.png" />
						</a> 
                    </div> 
                    <h4>Thank you for expressing interest on our Properties
                            Our expert will get in touch with you shortly.</h4>
        
           
           
          
          </form>
               	
                </div>   
			</div>
			<!-- //banner-text -->
		
		</div>
	</div>
    <!-- //banner --> 
    	<!-- about -->
	<div id="about" class="about cd-section">  
		<div class="container">  
			<div class="col-md-6 about-w3lleft"> 
				<h3 class="w3stitle"><span>About Project</span></h3>
                <P>Prestige Misty Waters</P> 
				<h4>Vista Tower - 2 & 3 BHK Luxury Homes, Hebbal, Bangalore</h4>
				<p>
                 Prestige Misty Waters - Vista Tower is a verdant, well laid out enclave of high-rise apartments situated just off the outer ring road at Hebbal. With a superb elevation, top of the line amenities, a refreshing landscape and a view to fall in love with. Every quality in fact that you have come to take for granted from prestige. Close to all essential conveniences and yet comfortable insulated from the bustle and bother. Just the way you prefer your home to be. Discover a home where you will wake up to each new day renewed vigour to take on the world.
                </p>
			</div> 
			<div class="col-md-6 about-w3lright">
    
            <img class="img-responsive about-w3lright-img2"  src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/company-profile-banner.jpg" style="height: 415px;object-fit: cover;object-position: right;"/>
			</div>
			<div class="clearfix"> </div> 
		</div>
    </div>

    <!-- //about -->
	<!-- blog -->
	<div id="blog" class="blog cd-section">
		<div class="container"> 
			<div class="agileits-hdng">
				<h3 class="w3stitle"><span>HIGHLIGHTS</span></h3>
                <P>Location Advantage</P> 
			</div>
			<div class="blog-agileinfo">
				<div class="col-md-7 blog-w3grid-img">
					<div class="wthree-blogimg">  
						<img style="height: 280px;" src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/mistywaters-overview.jpg" class="img-responsive" alt=""/>
					</div>  
				</div>
				<div class="col-md-5 blog-w3grid-text"> 
               
					
					<p>

                    FAR AWAY FROM THE CITY CONGESTION. YET AT THE CENTRE OF EVERYTHING YOU NEED.
                   <ul>
                                <li>- Hebbal: 1.7Km</li>
                                <li>- Manyata Tech Park: 3Km</li>
                                <li>- City Railway Station: 11Km</li>
                                <li>- KR Puram: 11.5Km</li>
                                <li>- Indiranagar: 15Km</li>
                                <li>- International Airport: 27Km</li>
                            </ul>
                    </p>
					
				</div> 
				<div class="clearfix"> </div>
			</div> 
		</div>
	</div>
    <!-- //blog -->    
    	<!-- features -->
	<div class="features">
		<div class="container"> 
			<div class="features-agileinfo">
				<div class="col-md-4 col-sm-4 features-wthree-grids"> 
					<h4>Specification</h4>
					<p>NEW LAUNCH | VISTA TOWER | House:2.5 & 3 BHK | Units:119 Apartments </p> 
					<h5 class="w3ls-featext">1</h5>
				</div>
				<div class="col-md-4 col-sm-4 features-wthree-grids"> 
					<h4>Info</h4>
					<p>LAND AREA:5 .7 Acres* | LOCATION:Hebbal, Bangalore</p>
					<h5 class="w3ls-featext">2</h5>
				</div>
				<div class="col-md-4 col-sm-4 features-wthree-grids"> 
					<h4>Areas</h4>
					<p>2.5 BHK 1339 SFT. ₹1.24 cr Onwards | 3 BHK 1615-1693 SFT.₹ 1.49cr Onwards </p> 
					<h5 class="w3ls-featext">3</h5>
				</div> 
				<div class="clerfix"> </div>
			</div>
		</div>
	</div>
	<!-- //features -->
    <!-- amenities -->
    <div id="amenities" class="services cd-section">
		<div class="container"> 
			<div class="agileits-hdng">
				<h3 class="w3stitle"><span>AMENITIES</span></h3> 
			</div>
			<div class="services-w3ls-row">
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/yoga.png" class="img-icon" alt=""/>
				<h5>Aerobics / Yoga Room</h5>
					
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/kids-play-zone.png" class="img-icon" alt=""/>
					<h5>Children's Play Area</h5>
					
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/fitness-station.png" class="img-icon" alt=""/>
					<h5>Gymnasium</h5>				
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/health.png" class="img-icon" alt=""/>
					<h5>Health Club</h5>
				</div>
                <div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/mini-theatre.png" class="img-icon" alt=""/>
				<h5>Mini Theatre</h5>
					
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/festival-pav.png" class="img-icon" alt=""/>
					<h5>Multipurpose Hall</h5>
					
				</div>
		
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/swimming.png" class="img-icon" alt=""/>
					<h5>Swimming Pool</h5>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/grand-club.png" class="img-icon" alt=""/>
				<h5>Reading Room </h5>
					
				</div>

				<div class="clearfix"> </div>
			</div>  
		</div>
	</div>
    <!-- //amenities -->
    
 	<!-- info -->
   	<!-- projects -->
	<div class="gallery" id="projects">
		<div class="container">
            <div class="agileits-hdng">
				<h3 class="w3stitle"><span>GALLERY</span></h3> 
			</div>
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
					<div class="gallery-grid1">
                        <a title="Prestige Misty Waters" href="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/1.jpg">
                            <div class="stack twisted">
                                <img  src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/1.jpg" alt=" " class="img-responsive" />
                            </div>
							
						</a>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Prestige Misty Waters" href="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/2.jpg">
                        <div class="stack twisted">    
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/2.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
                    </div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Prestige Misty Waters" href="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/3.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/3.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Prestige Misty Waters" href="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/4.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/4.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Prestige Misty Waters" href="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/5.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/5.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
              
			</div>
		</div>
	</div>
    <!-- //projects -->  
    <!-- //blog -->   
    	<!-- info -->
        <div id="location" class="blog cd-section">
		<div class="container gallery-grid1"> 
			<div class="agileits-hdng">
				<h3 class="w3stitle"><span>Plans</span></h3> 
			</div>
            <div class="col-md-6 col-xs-12 blog-agileinfo">
                <a title="Prestige Misty Waters" href="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/plan.jpg">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/plan.jpg" class="img-responsive" alt=""/>
                </a>
			
				<div class="clearfix"> </div>
            </div> 
            <div class="col-md-6 col-xs-12 blog-agileinfo">
            <a title="Prestige Misty Waters" href="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/plan2.jpg">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/plan2.jpg" class="img-responsive" alt=""/>
           </a>
			
				<div class="clearfix"> </div>
            </div> 
            <div class="clearfix"> </div>
			<div class="col-md-4  col-xs-12 blog-agileinfo">
            <a title="Prestige Misty Waters" href="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/1p.png">
			<img src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/1p.png" class="img-responsive" alt=""/>
</a>
				<div class="clearfix"> </div>
            </div> 
            <div class="col-md-4  col-xs-12 blog-agileinfo">
            <a title="Prestige Misty Waters" href="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/2p.png">
			<img src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/2p.png" class="img-responsive" alt=""/>
</a>
				<div class="clearfix"> </div>
			</div> 
            <div class="col-md-4  col-xs-12 blog-agileinfo">
            <a title="Prestige Misty Waters" href="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/3p.png">
            <img src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/3p.png" class="img-responsive" alt=""/>
</a>
			
				<div class="clearfix"> </div>
			</div> 
		</div>
	</div>
    <!-- //blog -->   
        <!-- //blog -->   
    	<!-- info -->
        <div id="location" class="blog cd-section">
		<div class="container"> 
			<div class="agileits-hdng">
				<h3 class="w3stitle"><span>Map</span></h3> 
			</div>
            <div class="col-md-12 blog-agileinfo gallery-grid1">
            <a title="Prestige Misty Waters" href="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/map.jpg">
            <img src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/images/map.jpg" class="img-responsive" alt=""/>
</a>
			
				<div class="clearfix"> </div>
            </div> 
           
            <div class="col-md-12 blog-agileinfo">
            <br>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3886.995781223881!2d77.59600641482265!3d13.03594029081371!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae17986e19cbb3%3A0xcfb36765a70d17dc!2sPrestige+Misty+Waters%2C+4th+Cross+Rd%2C+Thimakka+Layout%2C+Coconut+Garden%2C+Cholanayakanahalli%2C+Hebbal%2C+Bengaluru%2C+Karnataka+560032!5e0!3m2!1sen!2sin!4v1548835943725"  style="width:100%;" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
            
            <div class="clearfix"> </div>
            </div> 
		</div>
	</div>
    <!-- //blog --> 
    <p class="text-center">Disclaimer: RERA NO : Vista Tower - PRM/KA/RERA/1251/309/PR/181010/002053</p>
    <div class="copyw3-agile">
		<div class="container"> 
			<p>© 2019 Prestige Misty Waters. All Rights Reserved</p>
		</div>
	</div>
<div id="pi">
       <?php
       $vr = $this->session->userdata('user_id');
       $ut = $this->session->userdata('utm_source');
       if( isset($vr) &&  $vr > 0 && $ut != ""){
           echo $this->lead_check->set_pixel($vr, PRESTIGE);
           
       }
       ?>
      </div> 


	
	<script src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/js/vendor.js"></script>  
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>  
	<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/mistywaters/js/main.js"></script>
	 
   </body>
</html>