<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Prestige Lake Ridge</title>
    <meta name="description" content="Prestige Lake Ridge">
    <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/favicon.png" type="image/x-icon" />


    <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/lakeridge/css/vendor.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/lakeridge/css/main.css">
    <!-- fonts -->
    <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
        rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
</head>

<body>

    <?php $all_array = all_arrays(); ?>

    <!-- banner -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="header-banner header-banner-desktop">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/desktop-banner.jpg" />
                </div>
                <div class="header-banner header-banner-mobile">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/mobile-banner.jpg" />
                </div>
                <div class="">
                    <div class="carousel-caption">
                        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="puravankara_jsfrm_greenpark('<?php echo SITE_URL?>realestate/puravankara_provident/frm_submit','greenpark','1')">

                            <div class="">
                                <div class="col">
                                    <div class="form-logo">
                                        <a href="#">
                                            <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/logo-blue.png" />
                                        </a>
                                    </div>
                                    <h4>Thank you for expressing interest on our Properties
                            Our expert will get in touch with you shortly.</h4>

                                </div>
                           
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal -->
    </div>
    <!--//banner -->

    <!-- about -->
    <div class="about" id="about">
        <div class="container">
            <div class="col-md-6 about-left">
                <div class="col-xs-12 aboutimg-w3l aboutimg-w3l2">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/ab5.jpg" alt="image" style="object-position: center;" />
                </div>
                <!-- <div class="col-xs-6 aboutimg-w3l">
					<img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/ab3.jpg" alt="image" />
				
				</div> -->
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-6 about_right">
                <h3>ABOUT</h3>
                <h3 class="bold">Prestige Lake Ridge</h3>
                <p>
                In the fast-developing suburb of Yadalam Nagar, Uttarahalli, Bengaluru, Prestige brings to you contemporary dwellings that resonate perfectly with your needs. Prestige Lake Ridge houses 1,119 intelligently planned and luxuriously laid out high-rise homes set in 12 towers of 17 & 18 floors, with a spectrum of sizes and configurations, so you can choose a home that suits you the most. From 1Bhk to 3Bhk homes, every apartment is a spacious and airy abode that overflows with natural light and ventilation. Conveniently located to provide you ready access via arterial roads, the property is within easy reach of well reputed schools, hospitals, shopping areas, and entertainment hubs. With extensive open lung spaces, Prestige Lake Ridge is landscaped in the signature Prestige style, which soothes the mind and pampers your senses. It also includes a plush, well-equipped clubhouse that has plentiful ways to keep your leisure hours meaningfully occupied.
                </p>
            </div>
            <div class="clearfix"> </div>
            <p>Project Location : 80 Feet Rd Sahara Layout, Lingadheeranahalli, Bengaluru, Karnataka 560062</p>
        </div>
    </div>
    <!-- //about -->
    <!-- popular -->
    <div class="popular-w3" id="popular">
        <div class="container">
            <!-- <h3 class="title">Location Video</h3> -->
            <div class="popular-grids">
                <div class="">

                    <div class="popular-text">
                        <div class="detail-bottom">

                            <div class="rwd-media">
                            <iframe src="https://www.youtube.com/embed/8INj8TFSniU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div>
    <!-- //popular -->
    <div class="gallery highlight" id="projects">
        <div class="container">
            <h3 class="title">Highlights</h3>

            <div class="row">
                <div class="col-md-12">
                    <div class="desktop-only">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/price_pc.png" alt=" "
                                    class="img-responsive" />    
                    </div>
                    <div class="mobile-only">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/price_mobile.png" alt=" "
                                    class="img-responsive" />    
                    </div>
                </div> <!-- /col-md-4 -->

            </div>
            <!--/ .row -->
        </div>
    </div>

    <!-- projects -->
    <div class="gallery popular-w3" id="projects">
        <div class="container">
            <h3 class="title">Ammenities</h3>
            <div class="agile_gallery_grids w3-agile amenities-block demo">
				<div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/yoga.png" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Aerobics/Yoga Room</h4>
							</div>
					</div> 

				</div>
				<div class="gal-sec">
					<div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/badminton.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Badminton Court</h4>
                        </div>
                    </div>            
				</div>
				<div class="gal-sec">
			
                    <div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/kids-play-zone.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Children's Play Area</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/festival-pav.png" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Clubhouse</h4>
							</div>
					</div> 

				</div>
				<div class="gal-sec">
					<div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/Creche.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Creche</h4>
                        </div>
                    </div>            
				</div>
				<div class="gal-sec">
			
                    <div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/fitness-station.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Gymnasium</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/Jogging.png" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Jogging Track</h4>
							</div>
					</div> 

				</div>
				<div class="gal-sec">
					<div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/swimming.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Kids Pool</h4>
                        </div>
                    </div>            
				</div>
				<div class="gal-sec">
			
                    <div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/grand-club.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Library Space</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
			
                    <div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/mini-theatre.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Mini Theatre</h4>
                        </div>
					</div>                  
				</div>
                <div class="gal-sec">
                    
                <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/squash-court.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Squash Court</h4>
                        </div>
                    </div>                  
                </div>
                <div class="gal-sec">
                    
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/swimming.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Swimming Pool</h4>
                        </div>
                    </div>                  
                </div>

                <div class="gal-sec">
                    
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/glass.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Terrace Party Area</h4>
                        </div>
                    </div>                  
                </div>
                <div class="gal-sec">
                    
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/tennis-court.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Tennis Court</h4>
                        </div>
                    </div>                  
                </div>
                <div class="gal-sec">
                    
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/TT.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Table Tennis</h4>
                        </div>
                    </div>                  
                </div>
                <div class="gal-sec">
                    
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/grand-club.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Multipurpose Hall</h4>
                        </div>
                    </div>                  
                </div>

			</div>
        </div>
    </div>
    <!-- //projects -->

    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">GALLERY</h3>
            <div class="agile_gallery_grids w3-agile demo">

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Prestige Lake Ridge" href="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery1.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery1.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Prestige Lake Ridge" href="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery2.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery2.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Prestige Lake Ridge" href="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery3.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery3.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Prestige Lake Ridge" href="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery4.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery4.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Prestige Lake Ridge" href="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery5.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery5.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Prestige Lake Ridge" href="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery6.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery6.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Prestige Lake Ridge" href="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery7.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery7.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Prestige Lake Ridge" href="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery8.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery8.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Prestige Lake Ridge" href="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery9.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery9.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Prestige Lake Ridge" href="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery10.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/gallery10.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- //projects -->
    <!-- projects -->
    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">Plan</h3>
            <div class="agile_gallery_grids w3-agile demo">
                <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/masterplan.jpg">
                            <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/masterplan.jpg"
                                alt=" " class="img-responsive" />
                        </a>
                    </div>
                </div>
           




            </div>
        </div>
    </div>
    <!-- //projects -->


    <!-- contact -->
    <div class="address" id="contact">
        <div class="container">
            <h3 class="title">Contact Us</h3>
            <div class="address-row">
                <div class="col-md-12 col-xs-12  wow agile fadeInLeft animated" data-wow-delay=".5s">
                    <div class="address-info wow fadeInDown animated gallery-grid1" data-wow-delay=".5s">
                        <!-- <h4>Map</h4> -->
                        <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/map-img.jpg">
                            <img src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/images/map-img.jpg" />
                        </a>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
                    <div class="address-grid">
                        <!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
                        <div id="location">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15556.677240251793!2d77.5411672!3d12.8968323!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb10f3d987585e96!2sPrestige+Lake+Ridge!5e0!3m2!1sen!2sin!4v1539699527660"
                                style="border:0;width: 100%;height: 350px;" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
    <!--//contact-->
    <footer>
        <p class="footer-data text-center">Disclaimer: RERA NO.PRM/KA/RERA/1251/310/PR/170916/000334 | Phase 2: PRM/KA/RERA/1251/310/PR/170916/000336</p>
        <div class="copy-right-grids">
            <p class="footer-gd">© 2019 Prestige Lake Ridge. All rights reserved.</p>
        </div>
    </footer>
<div id="pi">
       <?php
       $vr = $this->session->userdata('user_id');
       $ut = $this->session->userdata('utm_source');
       if( isset($vr) &&  $vr > 0 && $ut != ""){
           echo $this->lead_check->set_pixel($vr, PRESTIGE);
           
       }
       ?>
      </div> 

   




    <script src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/js/vendor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/lakeridge/js/main.js"></script>
    
</body>

</html>