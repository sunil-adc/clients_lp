<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8"> 
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Emaar Beachfront - Residential Community | Emaar Properties</title>
	  <meta name="description" content="Invest and buy property in Dubai Creek Harbour. Dubai Creek Tower, an upcoming iconic monument in Dubai."/>
	  <meta type="keywords" content="buy property in Dubai, luxury property for sale in Dubai, Dubai properties real estate">
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/favicon.png" type="image/x-icon" />
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/css/vendor.css">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/css/main.css">   

    <!-- Global site tag (gtag.js) - AdWords: 790145831 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-790145831"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'AW-790145831');
    </script>

    <script type="text/javascript">
        !function(s,a,e,v,n,t,z){if(s.saq)return;n=s.saq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!s._saq)s._saq=n;
n.push=n;n.loaded=!0;n.version='1.0';n.queue=[];t=a.createElement(e);t.async=0;t.src=v;z=a.getElementsByTagName(e)[0];z.parentNode.insertBefore(t,z)}(window,document,'script','https://tags.srv.stackadapt.com/events.js');saq('ts', 'iVGOc6c5q8U7g1lxL_UbSA');
    </script>
   </head>
   <body> 

    <?php $all_array = all_arrays(); ?>
    <!-- banner -->
    <div id="home" class="w3ls-banner cd-section">
        <div class="banner-info">
            <!-- header -->
            <div class="header-w3layouts"><div class="container"></div></div>
            <div class="container banner-w3ltext"> 
                <div>
                    <?php $all_array = all_arrays(); ?>
                    <div role="form" id="feedbackForm" class="feedbackForm formmob form_hide" >
                        <div class="form-logo">
                            <a href="#">
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/blackVersion.png" />
                            </a> 
                        </div> 
                        <h4>inquiry now</h4>
                        <div class="form-section" id="form_div1">
                            <form action="JavaScript:void(0)" onsubmit="universalprime_jsfrm('<?php echo SITE_URL?>realestate/universalprime/frm_submit','lg_18_emaar','1')">
                                <div class="group">
                                    <div class="form-group">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                        <input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                                        <span class="help-block" id="name_err1"></span>
                                    </div>
                                </div>  
                                <div class="group">
                                    <div class="form-group">
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                        <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                                        <span class="help-block" id="email_err1" ></span>
                                    </div>
                                   
                                    <div class="form-group">
                                        <input type="hidden" class="hiddenCountry" name="CountryCode" value="" id="CountryCode1">
                                        <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                                        <span class="help-block" id="phone_err1"> </span>
                                        <span class="help-block" id="CountryCode_err"> </span>
                                    </div>
                                </div>      
                                <div class="submitbtncontainer">
                                    <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
                                </div>
                            </form>     
                        </div>
                         
                    </div>
                </div>   
            </div>
            <!-- //banner-text -->
        </div>
    </div>
    <!-- //banner --> 
    <!-- about -->

    <div role="form" id="feedbackForm" class="feedbackForm mobile_form" >
                        <div class="form-logo">
                            <a href="#">
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/blackVersion.png" />
                            </a> 
                        </div> 
                        <h4>inquiry now</h4>
                        <div class="form-section" id="form_div1">
                            <form action="JavaScript:void(0)" onsubmit="universalprime_jsfrm('<?php echo SITE_URL?>realestate/universalprime/frm_submit','lg_18_emaar','4')">
                                <div class="group">
                                    <div class="form-group">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                        <input type="text" class="form-control" id="name4" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                                        <span class="help-block" id="name_err4"></span>
                                    </div>
                                </div>  
                                <div class="group">
                                    <div class="form-group">
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                        <input type="email" class="form-control" id="email4" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                                        <span class="help-block" id="email_err4" ></span>
                                    </div>
                                   
                                    <div class="form-group">
                                        <input type="hidden" class="hiddenCountry" name="CountryCode" value="" id="CountryCode4">
                                        <input type="text" class="form-control only_numeric phone" id="phone4" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                                        <span class="help-block" id="phone_err4"> </span>
                                        <span class="help-block" id="CountryCode_err"> </span>
                                    </div>
                                </div>      
                                <div class="submitbtncontainer">
                                    <input type="submit" id="frm-sbmtbtn4" value="Submit" name="submit">
                                </div>
                            </form>     
                        </div>
                         
                    </div>


    <div id="about" class="about cd-section">  
        <div class="container">  
            <div class="col-md-6"> 
                <h3 class="w3stitle">AN ISLAND WITH A CITY ADDRESS</h3>
                <p>
                Emaar Beachfront is cradled between two of Dubai’s most desirable destinations – Dubai Marina and new Dubai. Due to its prime location, you can enjoy easy access to the shimmering beaches, marinas, yacht clubs, malls and dining venues of the surrounding districts, as well as seamless access to Sheikh Zayed Road and Dubai Marina.
                </p>
            </div> 
            <div class="col-md-6">
            
            <video class="header-video" id="header-video" style="width:100%; height:300px" controls>
                <source src="https://universalprime.ae/LG-18-Emaar/img/emar-video.mp4" type="video/mp4"> 
                Your browser does not support HTML5 video. 
             </video>
            
            </div>
            <div class="clearfix"> </div> 
        </div>
    </div>
    <!-- //about -->
    <!-- blog -->
    <div id="amenities" class="services cd-section">
        <div class="container"> 
            <div class="agileits-hdng">
                <h3 class="w3stitle"><span>Top Features of Emaar</span></h3>
                <br/><br/>
            </div>

 
             <div class="row">
                <div class="col-md-4 text-center">                     
					<img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/installment.png" alt=""/>
					<h4>Installments up to 3 years.</h4> 
                   
				</div> 
               <div class="col-md-4 text-center">                     
					<img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/dld.png" alt=""/>
					<h4>100% DLD waiver.</h4> 
				</div> 
               <div class="col-md-4 text-center">                     
					<img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/3years.png" alt=""/>
					<h4>3 years service charge waiver.</h4> 
				</div> 
                
              </div>
               
               
            </div> 



        </div>
    </div>
    <!-- //blog -->    
        <!-- features -->
    <div id="blog" class="blog cd-section">
        <div class="container-fluid"> 
           <div class="agileits-hdng">
                <h3 class="w3stitle"><span>Limited opportunity from "Emaar"</span></h3>
                <br/><br/>
            </div>
            <div class="features-agileinfo">
                <div class="col-md-4 text-center"><a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/1.jpg" alt="" class="img-responsive"/></a></div>
                <div class="col-md-4 text-center"><a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/2.jpg" alt="" class="img-responsive"/></a></div>
                <div class="col-md-4 text-center"><a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/3.jpg" alt="" class="img-responsive"/></a></div>
                <br>
                <div class="col-md-4 text-center"><a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/4.jpg" alt="" class="img-responsive"/></a></div>
                <div class="col-md-4 text-center"><a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/5.jpg" alt="" class="img-responsive"/></a></div>
                <div class="col-md-4 text-center"><a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/6.jpg" alt="" class="img-responsive"/></a></div>

            </div>
        </div>
    </div>
    <!-- //features -->
    <div class="copyw3-agile">
		<div class="container"> 
			<p>UNIVERSAL PRIME - © Copyright 2019. All Rights Reserved</p>
		</div>
	</div>
   
    <!-- //amenities -->


    <!-- contact form start -->
    <div class="floating-form visiable" id="contact_form">
        <div class="contact-opener">Enquire Now</div>
        <div role="form" id="feedbackForm" class="feedbackForm" >
            <div class="form-logo">
                <a href="#">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/blackVersion.png" />
                </a> 
            </div> 
            <h4>inquiry now</h4>
            <div class="form-section" id="form_div2">
                <form action="JavaScript:void(0)" onsubmit="universalprime_jsfrm('<?php echo SITE_URL?>realestate/universalprime/frm_submit','lg_18_emaar','2')">
                    <div class="group">
                        <div class="form-group">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err2"></span>
                        </div>
                    </div>  
                    <div class="group">
                        <div class="form-group">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                            <span class="help-block" id="email_err2" ></span>
                        </div>
                        
                        <div class="form-group">
                            <input type="hidden" class="hiddenCountry" name="CountryCode" value="" id="CountryCode2">
                            <input type="text" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                            <span class="help-block" id="phone_err2"> </span>
                            <span class="help-block" id="CountryCode_err"> </span>
                        </div>
                    </div>      
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn2" value="Submit" name="submit">
                    </div>
                </form>
            </div>
             
        </div>
    <div>
    <div class="popup-enquiry-form mfp-hide" id="popupForm">
        <div role="form" id="feedbackForm" class="feedbackForm " >
            <div class="form-logo">
                <a href="#">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/blackVersion.png" />
                </a> 
            </div> 
            <h4>inquiry now</h4>
            <div class="form-section" id="form_div3">
                <form action="JavaScript:void(0)" onsubmit="universalprime_jsfrm('<?php echo SITE_URL?>realestate/universalprime/frm_submit','lg_18_emaar','3')">
                    <div class="group">
                        <div class="form-group">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <input type="text" class="form-control" id="name3" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err3"></span>
                        </div>               
                    </div>  
                    <div class="group">
                        <div class="form-group">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                            <span class="help-block" id="email_err3" ></span>
                        </div>
                        
                        <div class="form-group">
                            <input type="hidden" class="hiddenCountry" name="CountryCode" value="" id="CountryCode3">
                            <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                            <span class="help-block" id="phone_err3"> </span>
                            <span class="help-block" id="CountryCode_err"> </span>
                        </div>
                    </div>   
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>
                </form>     
            </div>
             
        </div>
    </div> 

     <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
     <input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
     <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
     <input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
     <input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
     <input type="hidden" id="utm_content" name="utm_content" value="<?php echo (isset($_REQUEST['utm_content']) != "" ? $_REQUEST['utm_content'] : " "); ?>">
    
    <script src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/js/vendor.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/js/main.js"></script>
      <script src="<?php echo S3_URL?>/site/scripts/universalprime.js"></script>
      
   </body>
</html>