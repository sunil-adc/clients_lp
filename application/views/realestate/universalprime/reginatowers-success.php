<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Universal Prime</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="google-site-verification" content="0dbmPCmO4667-u3ow1vZI8ma61UpYpOFJIso_aqvUzs" />
<link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/images/favicon.png" type="image/x-icon" />
<link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/css/vendor.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
<link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/css/main.css">
<link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/css/slider.css">
<!-- fonts -->
<link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
            rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

<!--Brandklik Pixel Code-->
<iframe  id="trackierIFrame" src="" scrolling="no" frameborder="0" width="1" height="1"></iframe>

<script language="JavaScript" type="text/javascript">
    
    var custom_field1 = "<?php echo $this->session->userdata('name')  ?>"; 
    var custom_field2 = "<?php echo $this->session->userdata('email') ?>"; 
    var custom_field3 = "<?php echo $this->session->userdata('phone') ?>";
    var custom_field4 = "<?php echo $this->session->userdata('utm_source') ?>";
    var custom_field5 = "<?php echo $this->session->userdata('utm_campaign') ?>";
    
    document.getElementById("trackierIFrame").src = "https://brandklik.gotrackier.com/pixel?adid=5d84811fb6920d2c2a7fb5e2&sub1=" + custom_field1 + "&sub2=" + custom_field2 + "&sub3=" + custom_field3+ "&sub4=" + custom_field4+ "&sub5=" + custom_field5;
    document.getElementById('trackierIFrame').src += '';
    document.getElementById('trackierIFrame').contentWindow.location.reload();
    

</script>


<body>
<?php $all_array = all_arrays(); ?>

<header class="">

    <div class="logoLeft">
      <a href="JavaScript:Void(0);" target=""><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/images/logo.png" style="height: 70px;"></a>
    </div>
        <!-- <div class="container-fluid" style="background-color: #6b4f1f; padding: 6px;"><p class="text-center" style="color: #fff;">PRM/KA/RERA/1251/309/PR/190129/002311</p></div> -->
    <div class="logoRight">
      <a href="JavaScript:Void(0);" target=""><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/images/reg_logo.png" style="width: 125px; height: 60px;"></a>
    </div>

    <div class="clearB"></div>
</header>

<!-- banner -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
    <div class="">
      
      <div class="">
        <div class="carousel-caption baner-mob">
        <video autoplay="" loop="" controls="" muted="" style="width: 100%;">
          <source src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/video/bg1.mp4" type="video/mp4">
          Your browser does not support the video playbacks.
        </video>
          <form role="form" id="feedbackForm" class="feedbackForm form_hide webBannerForm" action="JavaScript:void(0)" onsubmit="universalprime_jsfrm('<?php echo SITE_URL?>realestate/universalprime/frm_submit','reginatowers','1','<?php echo SITE_URL?>')">
            <div class="">
              <div class="col">
                <div class="form-logo"> <a href="#"> <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/images/form_logo.png" /> </a> </div>
              </div>
                <h4 style="color: yellow; line-height: 1.4;">Thank you for expressing interest on our Properties
                Our expert will get in touch with you shortly.</h4>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  
  <!-- The Modal --> 
</div>
<!--//banner --> 

<!--Overview --> 

<div class="about" id="about">
  <div class="container">
    <h3 class="title"> </h3>

    <h5></h5>
    <div class="col-md-12 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
      
      

    </div>
	</div>
</div>

<!--//Overview --> 


<!-- about -->
<div class="about" id="about">
  <div class="container">
  <h3 class="title"> Project Overview  </h3>
  <h3>Introducing Regina Tower by Tiger Properties at Jumeirah Village Circle (JVC), Dubai</h3>


  <p class="marginTop40"> 
    Be a part of an Upscale neighbourhood to experience an exceptional lifestyle. 
    The family-friendly development with its modern and contemporary design offers a 
    serene environment. New age homes, equipped with some of the best Facilities are sure
    to elevate the state of your mind and offer you a great opportunity to relax.Witness spectacular
    views of the Dubai 
    skyline with these residential units customized to suit your luxury requirements.
  </p>



    <div class="col-md-6 about_right marginTop50 aboutRightBorder">  
        <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/images/highlightLeft.jpg" alt="group" width="100%">
    </div>
    <div class="col-md-6 about-left marginTop50 aboutParagraph">
      
      <p> • 15%-20% Of Average Annual Yield </p>
      <p> • Increase in Rental Returns </p>
      <p> • High Demand amongst Nuclear Families </p>
      <p> • High Return on Investment </p>
      <p> • Starting at just <b>399,000 AED </b></p>
      

    </div>
    <div class="clearfix"> </div>

    
    <div class="col-md-6 about-left marginTop50 aboutParagraphSamll">

      <h3>Highlights:</h3>
      
      <p> • Studio and Single Bedroom Luxury Apartments </p>
      <p> • From 390 to 1,200 sq.Ft </p>
      <p> • State Of The Art Architecture </p>
      <p> • Heart of the Jumeirah Village Circle (JVC) Dubai </p>
      <p> • Offers stunning views of the surrounding area and the city’s skyline </p>
      <p> • 24x7 Security </p>
      <p> • Ample parking space </p>
      

    </div>

    <div class="col-md-6 about_right marginTop50 aboutRightBorder">  
        <video autoplay="" loop="" controls="" muted="" width="100%" height="350px">
				  <source src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/video/bg2.mp4" type="video/mp4">
				  Your browser does not support the video playbacks.
				</video>
      </div>

    <div class="clearfix"> </div>


    <div class="col-md-6 about_right marginTop50 aboutRightBorder">  
        <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/images/highlightRight.jpg" alt="group" width="100%">
    </div>
    <div class="col-md-6 about-left marginTop50 aboutParagraphSamll font22">

      <h3>Features & Amenities:</h3>
      
      <p> • Temperature-Controlled Swimming Pools </p>
      <p> • Lush Green Park </p>
      <p> • Play Area For Children </p>
      <p> • Cafe & Restaurants </p>
      <p> • Clubhouse & Recreational Centre </p>
      

    </div>
    <div class="clearfix"> </div>

  </div>
</div>
<!-- //about --> 
<!-- Location -->

<div class="gallery" id="projects">
  <div class="container">
  <div class="row">
    <h3 class="title">Location</h3>
      

      <div class="col-md-7 col-sm-7 col-xs-12 gal-sec leftText locationLeftSection">
      <h3> ACCESSIBLE </h3>
      
      <br>
      <br>

      <b class="font14">Jumeirah Village Circle (JVC) Dubai is one amongst the prime location for investments.</b>

      <br>
      <br>
      <p> • Affordable </p>
      <p> • Access to Primary Landmarks. </p>
      <p> • Access to Dubai Downtown.</p>
      
      </div>

      <div class="col-md-5 col-sm-5 col-xs-12 gal-sec">
        
        <div class="gallery-grid1" style="margin-top: 0px;"> <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/images/locationmapbig.png"> <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/images/locationmapbig.png" alt=" " class="img-responsive"> </a> </div>
      </div>

  </div>
</div>

<div class="col-md-12" style="margin: 30px 0;">
  <span class="imgBig"></span>
  <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/images/img4.jpg" style="width: 100%; height: auto;">
</div>

</div>

<!-- //Location --> 

<!-- Map Start -->

<section class="mapBig ">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28913.745619045247!2d55.19108357551822!3d25.060592449090482!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f6c36babd2c79%3A0x675853b2595d1a65!2sJumeirah+Village+Circle+-+Dubai+-+United+Arab+Emirates!5e0!3m2!1sen!2s!4v1565883285181!5m2!1sen!2s" width="100%" height="400px" frameborder="0" style="border:0" allowfullscreen=""></iframe>
</section>

<!-- Map Ends -->



<div >
<h3 class="title h3Section">WHY DUBAI?</h3>
  <div class="container">
    


    <div class="col-md-12 col-sm-12 col-xs-12 about-left marginTop50">
    <div class="slideshow-container">

      <div class="mySlides fade">
        <div class="numbertext">1 / 5</div>
        <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/images/car1.jpg" alt="group" width="100%">
      
      </div>

      <div class="mySlides fade">
        <div class="numbertext">2 / 5</div>
        <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/images/car2.jpg" alt="group" width="100%">

      </div>

      <div class="mySlides fade">
        <div class="numbertext">3 / 5</div>
        <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/images/car3.jpg" alt="group" width="100%">
        
      </div>
      <div class="mySlides fade">
        <div class="numbertext">4 / 5</div>
        <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/images/car4.jpg" alt="group" width="100%">
        
      </div>
      <div class="mySlides fade">
        <div class="numbertext">5 / 5</div>
        <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/images/car5.jpg" alt="group" width="100%">
      
      </div>

      <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
      <a class="next" onclick="plusSlides(1)">&#10095;</a>

    </div>
      <br>

    <div style="text-align:center; display: none;">
      <span class="dot" onclick="currentSlide(1)"></span> 
      <span class="dot" onclick="currentSlide(2)"></span> 
      <span class="dot" onclick="currentSlide(3)"></span> 
      <span class="dot" onclick="currentSlide(4)"></span> 
      <span class="dot" onclick="currentSlide(5)"></span> 

    </div>

      <div class="clearfix"> </div>

    </div>
  
	</div>
</div>
	

<!-- projects -->
<div class="gallery" id="projects">
  <div class="container">
  <div class="row">
   
      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec whyUsLeft">
        <div class="gallery-grid1"> 
          <a title="Project Plan"> 
            <img  src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/images/whyusbg.jpg" alt=" " class="img-responsive">
            <div class="centered">
              <h3>“There is a tremendous difference between a broker who sells houses and a consultant who sells investments”</h3>
            </div>
          </a> 
        </div>
      </div>

      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec whyUsRight">
      <center><h3>Why Universal Prime?</h3>
          <br><br><br>
          <p>
            Universal Prime Real Estate is a property investment consultant who understands your vision and real estate needs. We are not just another brokerage agency, rather your guides in the real estate market. We will not only share market knowledge with you but also help to find the best property as per your budget and requirements.
          </p>
          <br><br><br>
      </center>
      </div>
  </div>
</div>
</div>
<!-- //projects --> 



<!-- contact -->





<!-- //contact -->





<footer class="bg-bark">
  <div class="copy-right-grids">
    <div class="col-sm-12 bg-bark" style="background-color: #000; border-bottom: 1px solid #737373; padding: 30px 0;">
     <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/images/footLogo.png" alt="group" width="180"> 

    <br>
            
    </div>
    <br>
    
      <div class="col-sm-12 bg-bark" style="background-color: #000;">         
      
      <h5 style="color: #ADADAD; font-size: 16px; padding-top: 10px; line-height: 1.5; font-weight: 600; padding: 30px 0;">
      RERA Number - 12875Office 1907, Opal TowerBurj Khalifa Blvd, Business BayDubai, UAE</h5>

    </div>
  </div>

</footer>


<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
<script src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/js/vendor.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script> 
<script src="<?php echo S3_URL?>/site/realestate-assets/universalprime/reginatowers/js/main.js"></script> 
<script src="<?php echo S3_URL?>/site/scripts/universalprime.js"></script>
   
<script>
// var slideIndex = 1;
// showSlides(slideIndex);

// function plusSlides(n) {
//   showSlides(slideIndex += n);
// }

// function currentSlide(n) {
//   showSlides(slideIndex = n);
// }

// function showSlides(n) {
//   var i;
//   var slides = document.getElementsByClassName("mySlides");
//   var dots = document.getElementsByClassName("dot");
//   if (n > slides.length) {slideIndex = 1}    
//   if (n < 1) {slideIndex = slides.length}
//   for (i = 0; i < slides.length; i++) {
//       slides[i].style.display = "none";  
//   }
//   for (i = 0; i < dots.length; i++) {
//       dots[i].className = dots[i].className.replace(" active", "");
//   }
//   slides[slideIndex-1].style.display = "block";  
//   dots[slideIndex-1].className += " active";
//   setTimeout(showSlides, 3000); 
// }


var slideIndex = 0;
showSlides();

function showSlides() {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}    
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 4000); // Change image every 4 seconds
}





</script>

</body>
</html>