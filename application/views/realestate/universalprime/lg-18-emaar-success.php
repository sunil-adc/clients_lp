<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8"> 
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Invest in Dubai Properties | Creek Edge</title>
	  <meta name="description" content="Invest and buy property in Dubai Creek Harbour. Dubai Creek Tower, an upcoming iconic monument in Dubai."/>
	  <meta type="keywords" content="buy property in Dubai, luxury property for sale in Dubai, Dubai properties real estate">
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/favicon.png" type="image/x-icon" />
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/css/vendor.css">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/css/main.css">   

    <!-- Global site tag (gtag.js) - AdWords: 790145831 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-790145831"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-790145831');
    </script>

    <script type="text/javascript">
        !function(s,a,e,v,n,t,z){if(s.saq)return;n=s.saq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!s._saq)s._saq=n;
n.push=n;n.loaded=!0;n.version='1.0';n.queue=[];t=a.createElement(e);t.async=0;t.src=v;z=a.getElementsByTagName(e)[0];z.parentNode.insertBefore(t,z)}(window,document,'script','https://tags.srv.stackadapt.com/events.js');saq('ts', 'iVGOc6c5q8U7g1lxL_UbSA');
        
    </script>
       
   </head>
   <body> 

        <?php $all_array = all_arrays(); ?>
    <!-- banner -->
    <div id="home" class="w3ls-banner cd-section">
        <div class="banner-info">
            <!-- header -->
            <div class="header-w3layouts"><div class="container"></div></div>
            <div class="container banner-w3ltext"> 
                <div>
                    <?php $all_array = all_arrays(); ?>
                    <div role="form" id="feedbackForm" class="feedbackForm formmob form_hide" >
                        <div class="form-logo">
                            <a href="#">
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/blackVersion.png" />
                            </a> 
                            
                        <br><br><br>
                        <h5>Thank you for expressing interest on our Properties</h5>
                        <br><br><br>
                        <h5>Our expert will get in touch with you shortly.</h5>    
                        </div> 
                        </div>
                         
                    </div>
                </div>   
            </div>
            <!-- //banner-text -->
        </div>
    </div>
    <!-- //banner --> 
    <!-- about -->

     

    <div id="about" class="about cd-section">  
        <div class="container">  
            <div class="col-md-6"> 
                <h3 class="w3stitle">AN ISLAND WITH A CITY ADDRESS</h3>
                <p>
                Emaar Beachfront is cradled between two of Dubai’s most desirable destinations – Dubai Marina and new Dubai. Due to its prime location, you can enjoy easy access to the shimmering beaches, marinas, yacht clubs, malls and dining venues of the surrounding districts, as well as seamless access to Sheikh Zayed Road and Dubai Marina.
                </p>
            </div> 
            <div class="col-md-6">
            
            <video class="header-video" id="header-video" style="width:100%; height:300px" controls>
                <source src="https://universalprime.ae/LG-18-Emaar/img/emar-video.mp4" type="video/mp4"> 
                Your browser does not support HTML5 video. 
             </video>
            
            </div>
            <div class="clearfix"> </div> 
        </div>
    </div>
    <!-- //about -->
    <!-- blog -->
    <div id="amenities" class="services cd-section">
        <div class="container"> 
            <div class="agileits-hdng">
                <h3 class="w3stitle"><span>Top Features of Emaar</span></h3>
                <br/><br/>
            </div>

 
             <div class="row">
                <div class="col-md-4 text-center">                     
					<img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/installment.png" alt=""/>
					<h4>Installments up to 3 years.</h4> 
                   
				</div> 
               <div class="col-md-4 text-center">                     
					<img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/dld.png" alt=""/>
					<h4>100% DLD waiver.</h4> 
				</div> 
               <div class="col-md-4 text-center">                     
					<img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/3years.png" alt=""/>
					<h4>3 years service charge waiver.</h4> 
				</div> 
                
              </div>
               
               
            </div> 



        </div>
    </div>
    <!-- //blog -->    
        <!-- features -->
    <div id="blog" class="blog cd-section">
        <div class="container-fluid"> 
           <div class="agileits-hdng">
                <h3 class="w3stitle"><span>Limited opportunity from "Emaar"</span></h3>
                <br/><br/>
            </div>
            <div class="features-agileinfo">
                <div class="col-md-4 text-center"><a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/1.jpg" alt="" class="img-responsive"/></a></div>
                <div class="col-md-4 text-center"><a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/2.jpg" alt="" class="img-responsive"/></a></div>
                <div class="col-md-4 text-center"><a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/3.jpg" alt="" class="img-responsive"/></a></div>
                <br>
                <div class="col-md-4 text-center"><a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/4.jpg" alt="" class="img-responsive"/></a></div>
                <div class="col-md-4 text-center"><a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/5.jpg" alt="" class="img-responsive"/></a></div>
                <div class="col-md-4 text-center"><a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/images/6.jpg" alt="" class="img-responsive"/></a></div>

            </div>
        </div>
    </div>
    <!-- //features -->
    <div class="copyw3-agile">
		<div class="container"> 
			<p>UNIVERSAL PRIME - © Copyright 2019. All Rights Reserved</p>
		</div>
	</div>
   
   <div id="pi">
       <?php
           $vr = $this->session->userdata('user_id');
           $ut = $this->session->userdata('utm_source');
           if( isset($vr) &&  $vr > 0 && $ut != ""){
               echo $this->lead_check->set_pixel($vr, UNIVERSALPRIME_USER);
               
           }
       ?>
      </div> 


    <!-- contact form start -->
     

 
    
    <script src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/js/vendor.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-18-emaar/js/main.js"></script>
      <script src="<?php echo S3_URL?>/site/scripts/default.js"></script>
      <script src="<?php echo S3_URL?>/site/scripts/realesate.js"></script>
   </body>
</html>