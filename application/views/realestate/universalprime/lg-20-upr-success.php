<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8"> 
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>LG-20-UPR</title>
	  <meta type="description" content="Leading Dubai based property consulting and real estate advisory firm. From buying Dubai properties to investing in luxury apartments in Dubai, We assist in all your investing requirements.">
	  <meta type="keywords" content="buy property in dubai,  buy apartment in dubai , ready villas for sale in dubai">
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/images/favicon.png" type="image/x-icon" />
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/css/vendor.css">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/css/main.css">  

    <!-- Global site tag (gtag.js) - AdWords: 790145831 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-790145831"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-790145831');
    </script>

    <script type="text/javascript">
        !function(s,a,e,v,n,t,z){if(s.saq)return;n=s.saq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!s._saq)s._saq=n;
n.push=n;n.loaded=!0;n.version='1.0';n.queue=[];t=a.createElement(e);t.async=0;t.src=v;z=a.getElementsByTagName(e)[0];z.parentNode.insertBefore(t,z)}(window,document,'script','https://tags.srv.stackadapt.com/events.js');saq('ts', 'iVGOc6c5q8U7g1lxL_UbSA');
        
    </script>
       
   </head>
   <body> 

        <?php $all_array = all_arrays(); ?>
    <!-- banner -->
    <div id="home" class="w3ls-banner cd-section">
        <div class="banner-info">
            <!-- header -->
            <div class="header-w3layouts"><div class="container"></div></div>
            <div class="container banner-w3ltext"> 
                <div>
                    <?php $all_array = all_arrays(); ?>
                    <div role="form" id="feedbackForm" class="feedbackForm formmob form_hide" >
                        <div class="form-logo">
                            <a href="#">
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/images/UP-Final-Logo.png" />
                            </a> <br> <br> <br>
                            <h5>Thank you for expressing interest on our Properties</h5>
                    <br><br>
                    <h5>Our expert will get in touch with you shortly.</h5>
                        </div>  
                         
                    </div>
                </div>   
            </div>
            <!-- //banner-text -->
        </div>
    </div>
    <!-- //banner --> 
    <!-- about -->

     


    <div id="about" class="about cd-section">  
          <div class="container">  
           <div class="col-md-6 about-w3lright">
            <img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/images/universalprime.gif" />
            
            </div>
            <div class="col-md-6 about-w3lleft"> 
                <h3 class="w3stitle">5 reasons of why to invest with Universal Prime Real Estate</h3>
                
                <ul class="ul-check list-unstyled success">
				  <li><span style='font-size:20px; color:#8bc34a'>&#x2713;</span>&nbsp; 4% DLD waiver</li>
				  <li><span style='font-size:20px; color:#8bc34a'>&#x2713;</span>&nbsp; Investor’s visa</li>
				  <li><span style='font-size:20px; color:#8bc34a'>&#x2713;</span>&nbsp; Trade license</li>
				  <li><span style='font-size:20px; color:#8bc34a'>&#x2713;</span>&nbsp; Variety of real estate projects</li>
				  <li><span style='font-size:20px; color:#8bc34a'>&#x2713;</span>&nbsp; Free investment consultancy</li>
				</ul>
                
            </div> 
            
            <div class="clearfix"> </div> 
        </div>
    </div>
    <!-- //about -->
    <!-- blog -->
    <div id="amenities" class="services cd-section" style="background-color: #115173">
        <div class="container"> 
            <div class="agileits-hdng">
                <h3 class="w3stitle"><span>We chose best projects for you</span></h3>
                <br/><br/>
            </div>

 
             <div class="row">
                <div class="col-md-4 text-center">                     
					<img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/images/prime.png" alt=""/>
					<h4>Prime locations</h4>
				</div> 
               <div class="col-md-4 text-center">                     
					<img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/images/down-payment.png" alt=""/>
					<h4>AED 50,000 down payment</h4>
				</div> 
               <div class="col-md-4 text-center">                     
					<img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/images/design.png" alt=""/>
					<h4>Exceptional design</h4>
				</div> 
               <div class="col-md-4 text-center">                     
					<img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/images/roi.png" alt=""/>
					<h4>High ROI</h4>
				</div> 
               <div class="col-md-4 text-center">                     
					<img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/images/developers.png" alt=""/>
					<h4>World class developers</h4>
				</div>  
              </div>
               
               
            </div> 



        </div>
    </div>
    <!-- //blog -->  
       
             <div id="blog" class="blog cd-section">
        <div class="container"> 
           <div class="agileits-hdng">
                <h3 class="w3stitle"><span>Ramadan Offer</span></h3>
                <br/><br/>
            </div>
           <div class="features-agileinfo">
                                <video class="video" controls>
                                  <source src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/images/ramadan-offer.mp4" type="video/mp4">
                                  Your browser does not support the video tag.
                                </video>
                            </div>
        </div>
    </div>
           
             
                 
        <!-- features -->
    <div id="blog" class="blog cd-section">
        <div class="container"> 
           <div class="agileits-hdng">
                <h3 class="w3stitle"><span>Benefits of investing in Dubai!</span></h3>
                <br/><br/>
            </div>
            <div class="features-agileinfo">
                <div class="row">
                
						    <div class="col-md-2">
						        <div class="card">
						           <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/images/tax.png" alt="group" width="70">
    							   <h3>No property tax</h3> 
						        </div>
						    </div>
						    <div class="col-md-2">
						        <div class="card">
    						        <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/images/visa.png" alt="shield" class="img-fluid" width="70">
    								<h3>5-, 10-years investor's visa</h3>
								</div>
						    </div>
						    <div class="col-md-2">
						        <div class="card">
    						        <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/images/pool.png"alt="tour" class="img-fluid" width="70">
    								<h3>Growing economy</h3>
								</div>
						    </div>
						    <div class="col-md-2">
						        <div class="card">
    						        <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/images/expo.png" alt="tour" class="img-fluid" width="70">
    								<h3>Expo 2020 2020</h3>
								</div>
						    </div>
						    <div class="col-md-3">
						        <div class="card">
    						        <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/images/safe.png" alt="tour" class="img-fluid" width="70">
    								<h3>One of the safest cities in the world</h3>
								</div> 
						</div>
            </div>
        </div>
    </div>
    </div>
    <!-- //features -->
    <!-- amenities -->
    <div class="features">
        <div class="container"> 
           
            <div class="col-md-6 about-w3lright">
            <img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/images/universalprime.png" />            
            </div>
              
            <div class="col-md-6 about-w3lleft"> 
                <h3 class="w3stitle">Who is Universal Prime?</h3>
                <p style="color: #fff">
                Property investment consultancy agency Universal Prime Real Estate understands your vision and needs when it comes to real estate investments. We are not another brokerage agency, rarther your guides in the real estate market. We will not only share market knowledge with you but also help to find the best property as per your budget and requirements. Our relationship with you starts when you sign your contract.
                </p>
                <p style="color: #fff">
                We are more than an agency – we are your partners!
                </p>
                
            </div> 
            
            <div class="clearfix"> </div> 
        </div>
    </div>
   
    <!-- //amenities -->
   <div class="copyw3-agile">
		<div class="container"> 
			<p>UNIVERSAL PRIME - © Copyright 2019. All Rights Reserved</p>
		</div>
	</div>

    <!-- contact form start -->
     
     

     <div id="pi">
       <?php
           $vr = $this->session->userdata('user_id');
           $ut = $this->session->userdata('utm_source');
           if( isset($vr) &&  $vr > 0 && $ut != ""){
               echo $this->lead_check->set_pixel($vr, UNIVERSALPRIME_USER);
               
           }
       ?>
      </div> 
    
    <script src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/js/vendor.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/universalprime/lg-20-upr/js/main.js"></script>
      <script src="<?php echo S3_URL?>/site/scripts/default.js"></script>
      <script src="<?php echo S3_URL?>/site/scripts/realesate.js"></script>
   </body>
</html>