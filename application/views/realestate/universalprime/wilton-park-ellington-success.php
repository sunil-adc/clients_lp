<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8"> 
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Invest in one of the world’s prime residential locations.</title>
	  <meta name="description" content="Invest and buy property in Dubai Creek Harbour. Dubai Creek Tower, an upcoming iconic monument in Dubai."/>
	  <meta type="keywords" content="buy property in Dubai, luxury property for sale in Dubai, Dubai properties real estate">
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/creek-edge-dubai/images/favicon.png" type="image/x-icon" />
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/creek-edge-dubai/css/vendor.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/css/style.css">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/creek-edge-dubai/css/main.css">   

    <!-- Global site tag (gtag.js) - AdWords: 790145831 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-790145831"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-790145831');
    </script>

    <script type="text/javascript">
        !function(s,a,e,v,n,t,z){if(s.saq)return;n=s.saq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!s._saq)s._saq=n;
n.push=n;n.loaded=!0;n.version='1.0';n.queue=[];t=a.createElement(e);t.async=0;t.src=v;z=a.getElementsByTagName(e)[0];z.parentNode.insertBefore(t,z)}(window,document,'script','https://tags.srv.stackadapt.com/events.js');saq('ts', 'iVGOc6c5q8U7g1lxL_UbSA');
        
    </script>
       
   </head>
   <body> 

        <?php $all_array = all_arrays(); ?>
    <!-- banner -->
    <div id="home" class="w3ls-banner cd-section">
    <div class="banner-info">
      <!-- header -->
      <div class="header-w3layouts">
        <div class="container">
        
        </div>
      </div>
      <!-- //header --> 
      <!-- banner-text -->
      <div class="container banner-w3ltext"> 
                
      <div>
      <?php $all_array = all_arrays(); ?>
            <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="puravankara_jsfrm_palmbeach('<?php echo SITE_URL?>realestate/puravankara_provident/frm_submit','palmbeach','1')">
            <div class="col text-center">
									<div class="form-logo"> <a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/logo.png" /></a> </div>
								  </div><br><br> 
                    <h5>Thank you for expressing interest on our Properties</h5><br>
                    <h5>Our expert will get in touch with you shortly.</h5>
                    <br><br> 
          </form>
                
                </div>   
      </div>
      <!-- //banner-text -->
    
    </div>
  </div>
    <!-- //banner --> 
    <!-- about -->

   <div id="about" class="about cd-section">  
        <div class="container">  
            <div class="col-md-12 text-center"> 
                <h3 class="w3stitle">WHY SHOULD YOU INVEST?</h3>
                <div class="clearfix"></div>
                <div class="text-center"><p>Mohammed Bin Rashid Al Maktoum City - District One is a premier lifestyle 
community nestled amidst picturesque surroundings at the bustling centre of Dubai.
Enter a world of tranquility and indulgence in distinctly styled homes, 
where magnificence is simply a way of life.</p>
                </div>
                <br>
                <div class="text-center">
                    <video class="header-video" id="header-video" style="width:100%; " controls>
                    <source src="https://universalprime.ae/Mohammed-Bin-Rashid-City/src/images/res_video_2.mp4" type="video/mp4"> 
                    Your browser does not support HTML5 video. 
                    </video>
                </div>
            
                
            </div> 
            
          
        </div>
    </div>
    <!-- //about -->
    
    
    
    <!-- //about -->
    <!-- blog 
    <div id="amenities" class="services cd-section">
        <div class="container"> 
            <div class="agileits-hdng">
                <h3 class="w3stitle"><span>PROJECT AMENITIES</span></h3>
                <br/><br/>
            </div>

 
             <div class="row">
                <div class="col-md-4 text-center">                     
					<img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/creek-edge-dubai/images/pool.png" alt=""/>
					<h4>Infinity Pool</h4>
				</div> 
               <div class="col-md-4 text-center">                     
					<img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/creek-edge-dubai/images/yatch.png" alt=""/>
					<h4>Yacht Deck and Top-notch Hotels</h4>
				</div> 
               <div class="col-md-4 text-center">                     
					<img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/creek-edge-dubai/images/school.png" alt=""/>
					<h4>Schools and Nurseries</h4>
				</div> 
               <div class="col-md-4 text-center">                     
					<img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/creek-edge-dubai/images/jogging.png" alt=""/>
					<h4>Green Walkways and Jogging Tracks</h4>
				</div> 
               <div class="col-md-4 text-center">                     
					<img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/creek-edge-dubai/images/mosque.png" alt=""/>
					<h4>Forthcoming Iconic Mosque</h4>
				</div>  
              </div>
               
               
            </div> 
        </div>
    </div>
    <!-- //blog -->    
       
        <!-- blog -->
    <div id="amenities" class="services cd-section">
        <div class="container"> 
             

 
             <div class="row"> 
               <div class="col-md-4 text-center">                     
                    <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/golfing.png" style="width:30%"/>
                    <h4>Golf Course</h4>
                </div> 
               <div class="col-md-4 text-center">                     
                    <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/bus.png" style="width:30%"/>
                    <h4>School and Nursery</h4>
                </div> 
               <div class="col-md-4 text-center">                     
                    <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/tennis.png" style="width:30%"/>
                    <h4>Tennis Academy</h4>
                </div> 
               <div class="col-md-4 text-center">                     
                    <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/race.png"  style="width:30%"/>
                    <h4>Racecourse</h4>
                </div>  
                
                <div class="col-md-4 text-center">                     
                    <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/park.png" alt="" style="width:30%"/>
                    <h4>Community  and Kids Parks</h4>
                </div>
              </div>
               
               
            </div> 



        </div>
    </div>
    <!-- //blog --> 
      
      
      <!-- features -->
    <div id="blog" class="blog cd-section">
        <div class="container"> 
           <div class="agileits-hdng">
                <h3 class="w3stitle"><span>WHO IS UNIVERSAL PRIME?</span></h3>
                <br/><br/>
            </div>
            <div class="features-agileinfo">
                <p align="center" class="p">Universal Prime Real Estate is a property investment consultant who understands your vision and real estate needs. We are not just another brokerage agency, rather your guides in the real estate market. We will not only share market knowledge with you but also help to find the best property as per your budget and requirements. Our relationship with you starts when you sign your contract.</p><br/>
                <p align="center" class="p">We are more than just an agency – we are your partners!</p>
            </div>
        </div>
    </div>
    <!-- //features -->
    
    
    
    <!-- amenities -->

	
	    <section class="wrapper style1">
	  <div class="inner">
		<article class="feature left"> <span class="image"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/project-1.png" alt="" /></span>
		  <div class="content">     
		  <h3 class="w3stitle">Premier Project</h3>
                <p>Project inaugurated and personally controlled by his Highness Sheikh Mohammed Bin Rashid Al Maktoum
		  </div>
		</article>
		<article class="feature right"> <span class="image"> <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/project-2.png" alt="" /></span>

		  <div class="content">
		  <h3 class="w3stitle">Top-notch Architecture</h3>
                <p>Wide range of Contemporary, Mediterranean and Modern Arabic style villas combined with luxury apartments.</p> 
		</article>
	  </div>
	</section>
    
    
    
    <section class="wrapper style1">
	  <div class="inner">
		<article class="feature left"> <span class="image"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/project-3.png" alt="" /></span>
		  <div class="content">     
		  <h3 class="w3stitle">Extensive Service</h3>
                <p>Service charge waiver for 3 years. </p> 
		  </div>
		</article>
		<article class="feature right"> <span class="image"> <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/project-4.png" alt="" /></span>

		  <div class="content">
		  <h3 class="w3stitle">Luxury</h3>
                <p>Complete luxury indulged furnished apartments and villas.</p>
		  </div>
		</article>
	  </div>
	</section>
   
   
   
   
   <section class="wrapper style1">
	  <div class="inner">
		<article class="feature left"> <span class="image"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/project-5.png" alt="" /></span>
		  <div class="content">     
		  <h3 class="w3stitle">Crystal Lagoon</h3>
                <p>One-of-a-kind Crystal Lagoon.</p> 
		  </div>
		</article>
		
	  </div>
	</section>
	
	
	 <footer class="footer">
	  <div class="section">
		<div class="container-fluid space"> </div>
	  </div>
	  <div class="container text-center">
		<div class="large-12 medium-12 small-12">
		  <p style="color: #999">© 2019 . All rights reserved.</p>
		</div>
	  </div>
	  <div class="section">
		<div class="container-fluid space"> </div>
	  </div>
	</footer>
	
   <div id="pi">
       <?php
           $vr = $this->session->userdata('user_id');
           $ut = $this->session->userdata('utm_source');
           if( isset($vr) &&  $vr > 0 && $ut != ""){
               echo $this->lead_check->set_pixel($vr, UNIVERSALPRIME_USER);
               
           }
       ?>
      </div> 
    <!-- //amenities -->


    <!-- contact form start -->
     
    <script src="<?php echo S3_URL?>/site/realestate-assets/universalprime/creek-edge-dubai/js/vendor.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/universalprime/creek-edge-dubai/js/main.js"></script>
     
   </body>
</html>