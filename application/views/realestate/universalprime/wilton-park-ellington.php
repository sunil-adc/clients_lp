<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8"> 
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Invest in one of the world’s prime residential locations.</title>
      <meta name="description" content="Invest and buy property in Dubai Creek Harbour. Dubai Creek Tower, an upcoming iconic monument in Dubai."/>
      <meta type="keywords" content="buy property in Dubai, luxury property for sale in Dubai, Dubai properties real estate">
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/favicon.png" type="image/x-icon" />
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/css/vendor.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/css/style.css">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/css/main.css">   
      <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700|Source+Sans+Pro:400,600&display=swap" rel="stylesheet">

    <!-- Global site tag (gtag.js) - AdWords: 790145831 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-790145831"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-790145831');
    </script>

    <script type="text/javascript">
        !function(s,a,e,v,n,t,z){if(s.saq)return;n=s.saq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!s._saq)s._saq=n;
n.push=n;n.loaded=!0;n.version='1.0';n.queue=[];t=a.createElement(e);t.async=0;t.src=v;z=a.getElementsByTagName(e)[0];z.parentNode.insertBefore(t,z)}(window,document,'script','https://tags.srv.stackadapt.com/events.js');saq('ts', 'iVGOc6c5q8U7g1lxL_UbSA');
        
    </script>
       
   </head>
   <body> 

        <?php $all_array = all_arrays(); ?>
    <!-- banner -->
    <div id="home" class="w3ls-banner cd-section">
        <div class="banner-info">
            <!-- header -->
            <div class="header-w3layouts"><div class="container"></div></div>
            <div class="container-fluid banner-w3ltext"> 
                <div>
                    <?php $all_array = all_arrays(); ?>
                    <div role="form" id="feedbackForm" class="feedbackForm formmob form_hide" >
                          <div class="col text-center">
									<div class="form-logo"> <a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/logo.png" /></a> </div>
								  </div>
                        <h4>Register your Interest</h4>
                        <div class="form-section" id="form_div1">
                            <form action="JavaScript:void(0)" onsubmit="universalprime_jsfrm('<?php echo SITE_URL?>realestate/universalprime/frm_submit','creek_edge_dubai','1')">
                                
                                <div class="group">
                                    <div class="form-group">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                        <input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                                        <span class="help-block" id="name_err1"></span>
                                    </div>
                                </div>  
                                <div class="group">
                                    <div class="form-group">
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                        <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                                        <span class="help-block" id="email_err1" ></span>
                                    </div>
                                   
                                    <div class="form-group">
                                        <input type="hidden" class="hiddenCountry" name="CountryCode" value="" id="CountryCode1">
                                        <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                                        <span class="help-block" id="phone_err1"> </span>
                                        <span class="help-block" id="CountryCode_err"> </span>
                                    </div>
                                </div>      
                                <div class="submitbtncontainer">
                                    <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
                                </div>
                            </form>     
                        </div>
                         
                    </div>
                </div>   
            </div>
            <!-- //banner-text -->
        </div>
    </div>
    <!-- //banner --> 
    <!-- about -->

    <div role="form" id="feedbackForm" class="feedbackForm mobile_form" >
                         
                        <h4>Register your Interest</h4>
                        <div class="form-section" id="form_div1">
                            <form action="JavaScript:void(0)" onsubmit="universalprime_jsfrm('<?php echo SITE_URL?>realestate/universalprime/frm_submit','creek_edge_dubai','4')">
                                <div class="group">
                                    <div class="form-group">
                                        <i class="fa fa-user" aria-hidden="true"></i>
                                        <input type="text" class="form-control" id="name4" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                                        <span class="help-block" id="name_err1"></span>
                                    </div>
                                </div>  
                                <div class="group">
                                    <div class="form-group">
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                        <input type="email" class="form-control" id="email4" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                                        <span class="help-block" id="email_err1" ></span>
                                    </div>
                                   
                                    <div class="form-group">
                                        <input type="hidden" class="hiddenCountry" name="CountryCode" value="" id="CountryCode4">
                                        <input type="text" class="form-control only_numeric phone" id="phone4" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                                        <span class="help-block" id="phone_err1"> </span>
                                        <span class="help-block" id="CountryCode_err"> </span>
                                    </div>
                                </div>      
                                <div class="submitbtncontainer">
                                    <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
                                </div>
                            </form>     
                        </div>
                         
                    </div>


    <div id="about" class="about cd-section">  
        <div class="container">  
            <div class="col-md-12 text-center"> 
                <h3 class="w3stitle">WHY SHOULD YOU INVEST?</h3>
               
                <div class="text-center"><p>Mohammed Bin Rashid Al Maktoum City - District One is a premier lifestyle 
community nestled amidst picturesque surroundings at the bustling centre of Dubai.
Enter a world of tranquility and indulgence in distinctly styled homes, 
where magnificence is simply a way of life.</p>
                </div>
                <br>
                <div class="text-center">
                    <video class="header-video" id="header-video" style="width:100%; " controls>
                    <source src="https://universalprime.ae/Mohammed-Bin-Rashid-City/src/images/res_video_2.mp4" type="video/mp4"> 
                    Your browser does not support HTML5 video. 
                    </video>
                </div>
            
                
            </div> 
            
          
        </div>
    </div>
    <!-- //about -->
    <!-- blog -->
    <div id="amenities" class="services cd-section">
        <div class="container"> 
             

 
             <div class="row"> 
               <div class="col-md-4 text-center">                     
                    <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/golfing.png" style="width:30%"/>
                    <h4>Golf Course</h4>
                </div> 
               <div class="col-md-4 text-center">                     
                    <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/bus.png" style="width:30%"/>
                    <h4>School and Nursery</h4>
                </div> 
               <div class="col-md-4 text-center">                     
                    <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/tennis.png" style="width:30%"/>
                    <h4>Tennis Academy</h4>
                </div> 
               <div class="col-md-4 text-center">                     
                    <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/race.png"  style="width:30%"/>
                    <h4>Racecourse</h4>
                </div>  
                
                <div class="col-md-4 text-center">                     
                    <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/park.png" alt="" style="width:30%"/>
                    <h4>Community  and Kids Parks</h4>
                </div>
              </div> 
               
            </div>  
        </div>
    </div>
    <!-- //blog -->    
        <!-- features -->
    <div id="blog" class="blog cd-section">
        <div class="container"> 
           <div class="agileits-hdng">
                <h3 class="w3stitle"><span>WHO IS UNIVERSAL PRIME?</span></h3>
                <br/><br/>
            </div>
            <div class="features-agileinfo">
                <p align="center" class="p">Universal Prime Real Estate is a property investment consultant who understands your vision and real estate needs. We are not just another brokerage agency, rather your guides in the real estate market. We will not only share market knowledge with you but also help to find the best property as per your budget and requirements. Our relationship with you starts when you sign your contract.</p><br/>
                <p align="center" class="p">We are more than just an agency – we are your partners!</p>
            </div>
        </div>
    </div>
    <!-- //features -->
    <!-- amenities 
    <div class="features">
        <div class="container"> 
           
            <div class="col-md-6">
            <img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/project-1.png" />            
            </div>              
            <div class="col-md-6 "> 
                <h3 class="w3stitle">Premier Project</h3>
                <p>Project inaugurated and personally controlled by his Highness Sheikh Mohammed Bin Rashid Al Maktoum
                </p>                 
            </div>
            <div class="clearfix"> </div>  

 

            <div class="col-md-6 "> 
                <h3 class="w3stitle">Top-notch Architecture</h3>
                <p>Wide range of Contemporary, Mediterranean and Modern Arabic style villas combined with luxury apartments.</p>                 
            </div>
            <div class="col-md-6">
            <img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/project-2.png" />            
            </div>
            <div class="clearfix"> <br><br><br></div>  


            <div class="col-md-6">
            <img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/project-3.png" />            
            </div>              
            <div class="col-md-6 "> 
                <h3 class="w3stitle">Extensive Service</h3>
                <p>Service charge waiver for 3 years. </p>                 
            </div>
            <div class="clearfix"> <br><br><br></div>  

 

            <div class="col-md-6 "> 
                <h3 class="w3stitle">Luxury</h3>
                <p>Complete luxury indulged furnished apartments and villas.</p>
                </div>
            <div class="col-md-6">
            <img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/project-4.png" />            
            </div>
            <div class="clearfix"> <br><br><br></div>  

            <div class="col-md-6">
            <img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/project-5.png" />            
            </div>              
            <div class="col-md-6 "> 
                <h3 class="w3stitle">Crystal Lagoon</h3>
                <p>One-of-a-kind Crystal Lagoon.</p>                 
            </div>
            <div class="clearfix"> </div> 
        </div>
    </div>
   
 amenities -->

    
    <section class="wrapper style1">
	  <div class="inner">
		<article class="feature left"> <span class="image"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/project-1.png" alt="" /></span>
		  <div class="content">     
		  <h3 class="w3stitle">Premier Project</h3>
                <p>Project inaugurated and personally controlled by his Highness Sheikh Mohammed Bin Rashid Al Maktoum
		  </div>
		</article>
		<article class="feature right"> <span class="image"> <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/project-2.png" alt="" /></span>

		  <div class="content">
		  <h3 class="w3stitle">Top-notch Architecture</h3>
                <p>Wide range of Contemporary, Mediterranean and Modern Arabic style villas combined with luxury apartments.</p> 
		</article>
	  </div>
	</section>
    
    
    
    <section class="wrapper style1">
	  <div class="inner">
		<article class="feature left"> <span class="image"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/project-3.png" alt="" /></span>
		  <div class="content">     
		  <h3 class="w3stitle">Extensive Service</h3>
                <p>Service charge waiver for 3 years. </p> 
		  </div>
		</article>
		<article class="feature right"> <span class="image"> <img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/project-4.png" alt="" /></span>

		  <div class="content">
		  <h3 class="w3stitle">Luxury</h3>
                <p>Complete luxury indulged furnished apartments and villas.</p>
		  </div>
		</article>
	  </div>
	</section>
   
   
   
   
   <section class="wrapper style1">
	  <div class="inner">
		<article class="feature left"> <span class="image"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/project-5.png" alt="" /></span>
		  <div class="content">     
		  <h3 class="w3stitle">Crystal Lagoon</h3>
                <p>One-of-a-kind Crystal Lagoon.</p> 
		  </div>
		</article>
		
	  </div>
	</section>
    
    
    
    
    <footer class="footer">
	  <div class="section">
		<div class="container-fluid space"> </div>
	  </div>
	  <div class="container text-center">
		<div class="large-12 medium-12 small-12">
		  <p style="color: #999">© 2019 . All rights reserved.</p>
		</div>
	  </div>
	  <div class="section">
		<div class="container-fluid space"> </div>
	  </div>
	</footer>
    
    

    <!-- contact form start -->
    <div class="floating-form visiable" id="contact_form">
        <div class="contact-opener">Enquire Now</div>
        <div role="form" id="feedbackForm" class="feedbackForm" >
             <div class="col text-center">
									<div class="form-logo"> <a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/logo.png" /></a> </div>
								  </div>
            <h4>Register your Interest</h4>
            <div class="form-section" id="form_div2">
                <form action="JavaScript:void(0)" onsubmit="universalprime_jsfrm('<?php echo SITE_URL?>realestate/universalprime/frm_submit','creek_edge_dubai','2')">
                    <div class="group">
                        <div class="form-group">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err2"></span>
                        </div>
                    </div>  
                    <div class="group">
                        <div class="form-group">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                            <span class="help-block" id="email_err2" ></span>
                        </div>
                        
                        <div class="form-group">
                            <input type="hidden" class="hiddenCountry" name="CountryCode" value="" id="CountryCode2">
                            <input type="text" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                            <span class="help-block" id="phone_err2"> </span>
                            <span class="help-block" id="CountryCode_err"> </span>
                        </div>
                    </div>      
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn2" value="Submit" name="submit">
                    </div>
                </form>
            </div>
             
        </div>
    <div>
    <div class="popup-enquiry-form mfp-hide" id="popupForm">
        <div role="form" id="feedbackForm" class="feedbackForm " >
            <div class="col text-center">
									<div class="form-logo"> <a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/images/logo.png" /></a> </div>
								  </div>
            <h4>Register your Interest</h4>
            <div class="form-section" id="form_div3">
                <form action="JavaScript:void(0)" onsubmit="universalprime_jsfrm('<?php echo SITE_URL?>realestate/universalprime/frm_submit','creek_edge_dubai','3')">
                    <div class="group">
                        <div class="form-group">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            <input type="text" class="form-control" id="name3" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err3"></span>
                        </div>               
                    </div>  
                    <div class="group">
                        <div class="form-group">
                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                            <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                            <span class="help-block" id="email_err3" ></span>
                        </div>
                        
                        <div class="form-group">
                            <input type="hidden" class="hiddenCountry" name="CountryCode" value="" id="CountryCode3">
                            <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                            <span class="help-block" id="phone_err3"> </span>
                            <span class="help-block" id="CountryCode_err"> </span>
                        </div>
                    </div>   
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>
                </form>     
            </div>
             
        </div>
    </div> 

     <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
     <input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
     <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
     <input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
     <input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
     <input type="hidden" id="utm_content" name="utm_content" value="<?php echo (isset($_REQUEST['utm_content']) != "" ? $_REQUEST['utm_content'] : " "); ?>">
    
    <script src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/js/vendor.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/universalprime/mohammed-bin-rashid-city/js/main.js"></script>
        <script src="<?php echo S3_URL?>/site/scripts/universalprime.js"></script>
   </body>
</html>