<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>TATA NEW HEAVEN |  Tata Housing</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="google-site-verification" content="0dbmPCmO4667-u3ow1vZI8ma61UpYpOFJIso_aqvUzs" />
<link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/favicon.jpg" type="image/x-icon" />
<link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/css/vendor.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
<link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/css/main.css">
<link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/css/slider.css">
<!-- fonts -->
<link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
            rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<body>
<?php $all_array = all_arrays(); ?>

<!-- banner -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <div class="">
        <div class="carousel-caption baner-mob">
          <form role="form" id="feedbackForm" class="feedbackForm form_hide" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>realestate/properties/frm_submit','tata','1','<?php echo SITE_URL?>')">
            <div class="">
              <div class="col">
                <div class="form-logo"> <a href="#"> <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/logo.png" /> </a> </div>
              </div>
              <div class="col">
                <div class="group">
                  <div class="form-group">
                    <input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                    <span class="help-block" id="name_err1"></span> </div>
                </div>
                <div class="group">
                  <div class="form-group">
                    <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                    <span class="help-block" id="email_err1" ></span> </div>
                  <div class="form-group">
                    <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode1">
                    <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                    <span class="help-block" id="phone_err1"> </span> <span class="help-block" id="CountryCode_err"> </span> </div>
                </div>
                
                 <div class="group">
                  <div style="display:flex;font-size:10px;line-height:1.2;padding-bottom: 10px">
                <input type="checkbox" checked="{agree}">
                <div style="padding-left: 5px; color:#000">I agree and authorize team to contact me. This will override the registry with DNC / NDNC</div>
              </div>
                </div>
                
                <!--<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
					<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
					<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">-->
                <div class="submitbtncontainer">
                  <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
                </div>
              </div>
            </div>
          </form>
          
          <form role="form" id="feedbackForm" class="feedbackForm mobile_form" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>realestate/properties/frm_submit','tata','4','<?php echo SITE_URL?>')">
            <div class="">
              <div class="col">
                <div class="form-logo"> <a href="#"> <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/logo.png" /> </a> </div>
              </div>
              <div class="col">
                <div class="group">
                  <div class="form-group">
                    <input type="text" class="form-control" id="name4" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                    <span class="help-block" id="name_err4"></span> </div>
                </div>
                <div class="group">
                  <div class="form-group">
                    <input type="email" class="form-control" id="email4" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                    <span class="help-block" id="email_err4" ></span> </div>
                  <div class="form-group">
                    <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode4">
                    <input type="text" class="form-control only_numeric phone" id="phone4" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                    <span class="help-block" id="phone_err4"> </span> <span class="help-block" id="CountryCode_err"> </span> </div>
                </div>
                
                 <div class="group">
                  <div style="display:flex;font-size:10px;line-height:1.2;padding-bottom: 10px">
                <input type="checkbox" checked="{agree}">
                <div style="padding-left: 5px; color:#000">I agree and authorize team to contact me. This will override the registry with DNC / NDNC </div>
              </div>
                </div>
                
                <!--<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
					<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
					<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">-->
                <div class="submitbtncontainer">
                  <input type="submit" id="frm-sbmtbtn4" value="Submit" name="submit">
                </div>
              </div>
            </div>
          </form>
        
          
        </div>
      </div>
    </div>
  </div>
  
  <!-- The Modal --> 
</div>
<!--//banner --> 
<!-- about -->
<div class="about" id="about">
  <div class="container">
    <div class="col-md-6 about_right">
      <h3>About New Haven Bengaluru</h3>
      <h3 class="bold">Overview</h3>
      <p> Get ready to experience a premium lifestyle, surrounded by nature at New Haven, Bengaluru’s first green township. Located off Tumkur Road - Bengaluru’s fastest developing corridor - the project is well-connected to Nice Ring Road and Tumkur Road as well as Metro Station. You can choose between 1.5, 2 & 3 BHK towers in a stepped terraced formation. Get ready to enjoy health with a fully equipped gymnasium, swimming pool, skating rink, basketball court & many more amenities. </p>
      <p>With the Metro Station and easy connectivity to major highways, New Haven is well on its way to becoming Bengaluru’s next big location.</p>
    </div>
    <div class="col-md-6 about-left">
      <div class="col-xs-12 aboutimg-w3l aboutimg-w3l2">
        <div class="rwd-media">          
          <iframe width="100%" src="https://www.youtube.com/embed/TDbFVm_dmQk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
      <div class="clearfix"> </div>
    </div>
    <div class="clearfix"> </div>
  </div>
</div>
<!-- //about --> 
<!-- popular -->
<div class="popular-w3" id="popular">
  <div class="container">
    <h3 class="title">Project Amenities</h3>
    <h3 class="text-center" style="color: #fff;">Explore a plethora of amenities that are designed to give you everything you imagined.</h3>
          
      <div class="row show-grid">
        
        <div class="col-sm-2 col-md-4-5 col-lg-1-5">
					           <div class="card">
						           <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/multipurpose-hall.png" alt="group" width="70">
    							   <h4 class="text-center space-h4">Multi-purpose Hall</h4> 
						        </div>
        </div>
        <div class="col-sm-2 col-md-5-5 col-lg-1-5">
   						        <div class="card">
    						        <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/study-room.png" alt="shield" class="img-fluid" width="70">
    								<h4 class="text-center space-h4">Reading Room</h4> 
								</div>
        </div>
        <div class="col-sm-2 col-md-3-5 col-lg-1-5">
   						        <div class="card">
    						        <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/cafe.png"alt="tour" class="img-fluid" width="70">
    								<h4 class="text-center space-h4">Cafe</h4> 
								</div>
        </div>
        <div class="col-sm-2 col-md-2-5 col-lg-1-5">
   						        <div class="card">
    						        <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/retail-zone.png" alt="tour" class="img-fluid" width="70">
    								<h4 class="text-center space-h4">Retail Zone</h4> 
								</div>
        </div>
        <div class="col-sm-2 col-md-4-5 col-lg-1-5">
   						        <div class="card">
    						        <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/spa.png" alt="tour" class="img-fluid" width="70">
    								<h4 class="text-center space-h4">Sream, Suna & Spa</h4> 
								</div>
        </div>
        

   
        
        <div class="col-sm-2 col-md-4-5 col-lg-1-5">
   						        <div class="card">
    						        <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/kidsplayare.png" alt="tour" class="img-fluid" width="70">
    								<h4 class="text-center space-h4">Kids Play Area</h4> 
								</div>
        </div>
        <div class="col-sm-2 col-md-5-5 col-lg-1-5">
   						        <div class="card">
    						        <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/basketbal-courtl.png" alt="tour" class="img-fluid" width="70">
    								<h4 class="text-center space-h4">Half Basket Ball Court</h4> 
								</div>
        </div>
        <div class="col-sm-2 col-md-3-5 col-lg-1-5">
   						        <div class="card">
    						        <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/skating.png" alt="tour" class="img-fluid" width="70">
    								<h4 class="text-center space-h4">Skating Rink</h4> 
								</div>
        </div>
        <div class="col-sm-2 col-md-2-5 col-lg-1-5">
   						        <div class="card">
    						        <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/gym.png" alt="tour" class="img-fluid" width="70">
    								<h4 class="text-center space-h4">Gym</h4> 
								</div>
        </div>
        <div class="col-sm-2 col-md-4-5 col-lg-1-5"> 
   						        <div class="card">
    						        <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/indoor-game.png" alt="tour" class="img-fluid" width="70">
    								<h4 class="text-center space-h4">Indoor Games</h4> 
								</div>
        </div>
        
  </div>
	</div>
</div>
<!-- //popular --> 


<div class=""  style="padding:50px 0px;">
  <div class="container">
    <h3 class="title">Explore Your Home</h3>
    
	<div class="slideshow-container">

	<div class="mySlides fade">
	  <div class="numbertext">1 / 5</div>
	  <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/44.jpg" alt="group" width="100%">
	 
	</div>

	<div class="mySlides fade">
	  <div class="numbertext">2 / 5</div>
	  <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/45.jpg" alt="group" width="100%">
 
	</div>

	<div class="mySlides fade">
	  <div class="numbertext">3 / 5</div>
	  <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/46.jpg" alt="group" width="100%">
	  
	</div>
	<div class="mySlides fade">
	  <div class="numbertext">4 / 5</div>
	  <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/47.jpg" alt="group" width="100%">
	  
	</div>
	<div class="mySlides fade">
	  <div class="numbertext">5 / 5</div>
	  <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/48.jpg" alt="group" width="100%">
	 
	</div>

	<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
	<a class="next" onclick="plusSlides(1)">&#10095;</a>

	</div>
	<br>

	<div style="text-align:center">
	  <span class="dot" onclick="currentSlide(1)"></span> 
	  <span class="dot" onclick="currentSlide(2)"></span> 
	  <span class="dot" onclick="currentSlide(3)"></span> 
	  <span class="dot" onclick="currentSlide(4)"></span> 
	  <span class="dot" onclick="currentSlide(5)"></span> 
	</div>
    
	</div>
</div>
	

<!-- projects -->
<div class="gallery" id="projects">
  <div class="container">
  <div class="row">
    <h3 class="title">Master Plan & Floor Plans</h3>
   
      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
        <div class="gallery-grid1"> <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/project_plans.png"> <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/project_plans.png" alt=" " class="img-responsive"> </a> </div>
      </div>

      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
        <div class="gallery-grid1"> <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/floorplans_1.png"> <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/floorplans_1.png" alt=" " class="img-responsive"> </a> </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
        <div class="gallery-grid1"> <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/floorplans_2.png"> <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/floorplans_2.png" alt=" " class="img-responsive"> </a> </div>
      </div>
      
      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
        <div class="gallery-grid1"> <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/floorplans_3.png"> <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/floorplans_3.png" alt=" " class="img-responsive"> </a> </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
        <div class="gallery-grid1"> <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/floorplans_4.png"> <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/floorplans_4.png" alt=" " class="img-responsive"> </a> </div>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
        <div class="gallery-grid1"> <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/floorplans_5.png"> <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/floorplans_5.png" alt=" " class="img-responsive"> </a> </div>
      </div>

  </div>
</div>
<!-- //projects --> 

<!-- contact -->
<div class="address" id="contact">
  <div class="container">
    <h3 class="title">Contact Us</h3>
    <div class="address-row">
      <div class="col-md-12 col-xs-12  wow agile fadeInLeft animated" data-wow-delay=".5s">
        <div class="address-info wow fadeInDown animated" data-wow-delay=".5s">
          <h4>Explore New Haven Bengaluru’s Neighbourhood</h4>
          <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/locationadvt.jpg" width="100%" /> </div>
      </div>
     <h4>FUTURE HAS A NEW DESTINATION NEW WEST BENGALURU</h4>
      <div class="col-md-3 col-xs-6 address-right">
        <div class="wow fadeInRight animated center" data-wow-delay=".5s">
      	<img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/cricket.png" alt="tour">
          <h5 class="text-center space-h4">KSCA Alur Cricket Stadium</h5>
        </div>
      </div>
      <div class="col-md-3 col-xs-6 address-right">
        <div class="wow fadeInRight animated center" data-wow-delay=".5s">
   		 <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/metro.png" alt="tour">
          <h5 class="text-center space-h4">Namma Metro</h5>
        </div>
      </div>
  	  <div class="col-md-3 col-xs-6 address-right">
        <div class="wow fadeInRight animated center" data-wow-delay=".5s">
     	 <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/center.png" alt="tour">
          <h5 class="text-center space-h4">Bengaluru International Exhibition Centre</h5>
        </div>
      </div>
      <div class="col-md-3 col-xs-6 address-right">
        <div class="wow fadeInRight animated center" data-wow-delay=".5s">
   		 <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/corridor.png" alt="tour">
          <h5 class="text-center space-h4">Proposed Mumbai-Bangalore Industrial Corridor</h5>
        </div>
      </div>
    </div>
  </div>
</div>
<!--//contact-->

<div class="map" id="map">
  <div class="container">
    <h3 class="title">Contact Us</h3>
    <div class="col-md-12 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
        <div class="address-grid"> 
          <!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
          <div id="location">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15543.813840259065!2d77.4420684!3d13.1021349!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xadc2f25315adf2ca!2sTata+Value+Homes+New+Haven-+Bengaluru!5e0!3m2!1sen!2sin!4v1558943651764!5m2!1sen!2sin" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen=""></iframe>
          </div>
        </div>
      </div>
	</div>
</div>


  
    <div class="container">
        
         <br>
            
         
  </div>


<footer class="bg-bark">
  <div class="copy-right-grids">
    <div class="col-sm-12 bg-bark" style="background-color: #000; border-bottom: 1px solid #737373;">
            
           
			  <p style="color: #fff">
          
			Project RERA No. PR/KN/170728/000146<br></p>
         
            <p style="color: #fff">
              © 2019 TATA Housing Development Company Limited, All Rights Reserved.
            </p>    
    <br>
            
          </div>
          
           <div class="col-sm-12 bg-bark" style="background-color: #000;">         
           
            
            <p style="font-size: 12px; padding: 10px; line-height: 1.5;color: #ADADAD; ">
              The Occupation Certificates (OC) in respect of towers 1 to 7 was received on 09/11/2016, for 8 to 18 on 16/06/2017 and for 30 to 34 on 28/2/2018. KRERA Registration No. PRM/KA/RERA/1250/307/PR/171014/000171 for towers 19, 26 & 35. KRERA Registration No. PRM/KA/RERA/1251/309/PR/170916/000146 for towers 30 to 34. Anarock Property Consultants Private Limited-KRERA Registration No. PRM/KA/RERA/1251/446/AG/171110/000554 available on <a href="https://rera.karnataka.gov.in/home">https://rera.karnataka.gov.in/home</a>
            </p>
            <h5 style="color: #ADADAD; font-size: 16px; padding-top: 10px; line-height: 1.5; font-weight: 600">Disclaimer: </h5>
            <p style="color: #ADADAD; font-size: 12px; padding: 10px; line-height: 1.5">
             
              This offer is valid on select units and for limited period only. The Company reserves the right to modify or withdraw the same at any point of time. The lucky winners will be chosen through a lucky draw after closure of the offer. The gold vouchers will be handed to the customer upon registration of Agreement for Sale and the same shall be subject to the terms of the company issuing the gold vouchers. The gold vouchers cannot be clubbed with any other offer or scheme. The gold vouchers cannot be exchanged for equivalent amount of cash or discount. *The price excludes other charges, stamp duty, statutory charges & taxes. The sale is subject to the terms of Application form and Agreement for sale.  **Distance and timelines mentioned are indicative and may vary depending upon traffic. For more information, please contact the site office at New Haven, Off Tumkur Road, Dasanapura Hobli, Near The Golden Palms Hotel & Spa, Bengaluru – 562 123 or call 1800 200 3553. Visit <a href="https://www.tatavaluehomes.com/">https://www.tatavaluehomes.com/ </a>
            </p>
            <h5 style="color: #ADADAD; font-size: 16px; padding-top: 10px; line-height: 1.5; font-weight: 600">ANAROCK</h5>
            <p style="color: #ADADAD;font-size: 12px; padding: 10px; line-height: 1.5">
              ANAROCK
Call done to the mentioned number may be monitored and recorded for record-keeping, training and quality-assurance purposes. By calling on the given number you agree to terms as mentioned on <a href="https://www.anarock.com/terms-of-use">www.anarock.com/terms-conditions</a>

            </p>
            
          </div>
  </div>
</footer>



<div class="floating-form visiable" id="contact_form">
<div class="contact-opener">Enquire Now</div>
<form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>realestate/properties/frm_submit','tata','2','<?php echo SITE_URL?>')">
  <div class="">
    <div class="col text-center">
      <div class="form-logo"> <a href="#"> <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/logo.png" /> </a> </div>
    </div>
    <div class="col">
      <div class="groups">
        <div class="form-group">
          <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
          <span class="help-block" id="name_err2"></span> </div>
      </div>
      <div class="groups">
        <div class="form-group">
          <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
          <span class="help-block" id="email_err2" ></span> </div>
        <div class="form-group">
          <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode2">
          <input type="text" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
          <span class="help-block" id="phone_err2"> </span> <span class="help-block" id="CountryCode_err"> </span> </div>
      </div>
      <div class="submitbtncontainer">
        <input type="submit" id="frm-sbmtbtn2" value="Submit" name="submit">
      </div>
    </div>
  </div>
</form>
<div>
<div class="popup-enquiry-form mfp-hide" id="popupForm">
  <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>realestate/properties/frm_submit','tata','3','<?php echo SITE_URL?>')">
    <div class="">
      <div class="col">
        <div class="form-logo"> <a href="#"> <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/images/logo.png" /> </a> </div>
      </div>
      <div class="col">
        <div class="groups">
          <div class="form-group">
            <input type="text" class="form-control" id="name3" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
            <span class="help-block" id="name_err3"></span> </div>
          <div class="form-group">
            <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
            <span class="help-block" id="email_err3" ></span> </div>
        </div>
        <div class="groups">
          <div class="form-group">
            <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode3">
            <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
            <span class="help-block" id="phone_err3"> </span> <span class="help-block" id="CountryCode_err"> </span> </div>
        </div>
        <div class="submitbtncontainer">
          <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
        </div>
      </div>
    </div>
  </form>
</div>
<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
<script src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/js/vendor.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script> 
<script src="<?php echo S3_URL?>/site/realestate-assets/tata/tatavaluehomes/js/main.js"></script> 
<script src="<?php echo S3_URL?>/site/scripts/properties.js"></script>
   
<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}
</script>

    
    
</body>
</html>