<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>THE PROMONT - Residences on Promont Hill at Banashankari Bengaluru, Luxury Apartments, Penthouse |  Tata Housing</title>
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="google-site-verification" content="0dbmPCmO4667-u3ow1vZI8ma61UpYpOFJIso_aqvUzs" />
<link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/images/favicon.jpg" type="image/x-icon" />
<link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/css/vendor.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
<link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/css/main.css">
<link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/css/slider.css">
<!-- fonts -->
<link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
            rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<body>
<?php $all_array = all_arrays(); ?>

<!-- banner -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <div class="">
        <div class="carousel-caption baner-mob">
          <form role="form" id="feedbackForm" class="feedbackForm form_hide" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>realestate/properties/frm_submit','tatapromont','1','<?php echo SITE_URL?>')">
            <div class="">
            <h4>Thank you for expressing interest on our Properties
                            Our expert will get in touch with you shortly.</h4>
              
            </div>
          </form>
          
          <form role="form" id="feedbackForm" class="feedbackForm mobile_form" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>realestate/properties/frm_submit','tatapromont','4','<?php echo SITE_URL?>')">
            <div class="">
              <div class="col">
                <div class="form-logo"> <a href="#"> <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/images/promont.png" /> </a> </div>
              </div>
              <div class="col">
                <div class="group">
                  <div class="form-group">
                    <input type="text" class="form-control" id="name4" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                    <span class="help-block" id="name_err4"></span> </div>
                </div>
                <div class="group">
                  <div class="form-group">
                    <input type="email" class="form-control" id="email4" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                    <span class="help-block" id="email_err4" ></span> </div>
                  <div class="form-group">
                    <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode4">
                    <input type="text" class="form-control only_numeric phone" id="phone4" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                    <span class="help-block" id="phone_err4"> </span> <span class="help-block" id="CountryCode_err"> </span> </div>
                </div>
                
                 <div class="group">
                  <div style="display:flex;font-size:10px;line-height:1.2;padding-bottom: 10px">
                <input type="checkbox" checked="{agree}">
                <div style="padding-left: 5px; color:#000">I agree and authorize team to contact me. This will override the registry with DNC / NDNC </div>
              </div>
                </div>
                
                <!--<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
					<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
					<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">-->
                <div class="submitbtncontainer">
                  <input type="submit" id="frm-sbmtbtn4" value="Submit" name="submit">
                </div>
              </div>
            </div>
          </form>
        
          
        </div>
      </div>
    </div>
  </div>
  
  <!-- The Modal --> 
</div>
<!--//banner --> 

<!--Overview --> 

<div class="about" id="about">
  <div class="container">
    <h3 class="title"> Project Overview </h3>
    <div class="col-md-12 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
      
      <p> Presenting Tata Housing's exclusive lifestyle at Banashankari, Bengaluru. Set atop the majestic Banashankari Hill, The Promont scales above the city and gives you an enviable view. Splendid design, international architecture and an outdoor infinity-edge pool with a 270 degree city view awaits you. Come home to Bengaluru’s most coveted terraced hillside residences. Live amongst the crème de la crème. </p>

    </div>
	</div>
</div>

<!--//Overview --> 


<!-- about -->
<div class="about" id="about">
  <div class="container">
  <h3 class="title"> Project Highlights </h3>
  <h3>Ready-to-Move-In 3 & 4 BHK Luxury Residences, Banashankari Hill</h3>
    <div class="col-md-6 about_right marginTop50 aboutRightBorder">  
        <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/images/highlightLeft.png" alt="group" width="100%">
    </div>
    <div class="col-md-6 about-left marginTop50 aboutParagraph">
      
      <p> OC received. No GST </p>
      <p> Air-Conditioned Residences</p>
      <p>Smart Home Automation</p>
      <p>Modular Kitchen</p>
      <p>3-Tier Security system</p>
      <p>3 Level Clubhouse with Squash court</p>
      <p>Outdoor Infinity Edge Pool</p>
      <p>Temperature Controlled Indoor Pool</p>
      <p>180 degree panoramic view of the city</p>
      <p>Platinum IGBC Certified building</p>

    </div>
    <div class="clearfix"> </div>
  </div>
</div>
<!-- //about --> 
<!-- popular -->

<!-- //popular --> 


<div class=""  style="padding:50px 0px;">
  <div class="container">
    <h3 class="title">Walkthrough</h3>

    <div class="col-md-6 about-left">
      <div class="col-xs-12 aboutimg-w3l aboutimg-w3l2">
        <div class="rwd-media">          
          <iframe width="100%" height="100%" src="https://www.youtube.com/embed/5z-3KzLIvck" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
        </div>
      </div>
      <div class="clearfix"> </div>
    </div>

    <div class="col-md-6 about-left marginTop50">
    <div class="slideshow-container">

      <div class="mySlides fade">
        <div class="numbertext">1 / 7</div>
        <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/images/04.jpg" alt="group" width="100%">
      
      </div>

      <div class="mySlides fade">
        <div class="numbertext">2 / 7</div>
        <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/images/05.jpg" alt="group" width="100%">

      </div>

      <div class="mySlides fade">
        <div class="numbertext">3 / 7</div>
        <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/images/06.jpg" alt="group" width="100%">
        
      </div>
      <div class="mySlides fade">
        <div class="numbertext">4 / 7</div>
        <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/images/07.jpg" alt="group" width="100%">
        
      </div>
      <div class="mySlides fade">
        <div class="numbertext">5 / 7</div>
        <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/images/08.jpg" alt="group" width="100%">
      
      </div>

      <div class="mySlides fade">
        <div class="numbertext">6 / 7</div>
        <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/images/09.jpg" alt="group" width="100%">
      
      </div>

      <div class="mySlides fade">
        <div class="numbertext">7 / 7</div>
        <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/images/09.jpg" alt="group" width="100%">
      
      </div>

      <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
      <a class="next" onclick="plusSlides(1)">&#10095;</a>

    </div>
      <br>

    <div style="text-align:center">
      <span class="dot" onclick="currentSlide(1)"></span> 
      <span class="dot" onclick="currentSlide(2)"></span> 
      <span class="dot" onclick="currentSlide(3)"></span> 
      <span class="dot" onclick="currentSlide(4)"></span> 
      <span class="dot" onclick="currentSlide(5)"></span> 
      <span class="dot" onclick="currentSlide(6)"></span> 
      <span class="dot" onclick="currentSlide(7)"></span> 

    </div>

      <div class="clearfix"> </div>

    </div>
  
	</div>
</div>
	

<!-- projects -->
<div class="gallery" id="projects">
  <div class="container">
  <div class="row">
    <h3 class="title">Master Plan</h3>
   
      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
        <div class="gallery-grid1"> <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/images/masterplan.jpg"> <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/images/masterplan.jpg" alt=" " class="img-responsive"> </a> </div>
      </div>

      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
            <table class="table">
              <thead>
                <tr>
                  <th>Unit Type</th>
                  <th>Super Built-Up Area<small>(SFT)</small></th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>

                <tr>
                  <td align="center">3BHK Supreme</td> <td align="center">2376 SQFT</td> 
                  <td align="center"><a href="javascript:void(0)" class="btn btn-warning makeNewEnquiry" >Enquire Now</a></td>
                </tr>

                <tr>
                  <td align="center">3BHK Grande</td> <td align="center">2480 SQFT</td>
                 <td align="center"><a href="javascript:void(0)"  class="btn btn-warning makeNewEnquiry" >Enquire Now</a></td>
                </tr>

				        <tr>
                  <td align="center">4BHK Supreme</td> <td align="center">3024 SQFT</td> 
                  <td align="center"><a href="javascript:void(0)"  class="btn btn-warning makeNewEnquiry">Enquire Now</a></td>
                </tr>

				        <tr>
                  <td align="center">4BHK Grande</td> <td align="center">3205 SQFT</td> 
                  <td align="center"><a href="javascript:void(0)"   class="btn btn-warning makeNewEnquiry" >Enquire Now</a></td>
                </tr>

				        <tr>
                  <td align="center">Penthouses</td> <td align="center">7754 SQFT</td>
                  <td align="center"><a href="javascript:void(0)"  class="btn btn-warning makeNewEnquiry">Enquire Now</a></td>
                </tr>

				        <tr>
                  <td align="center">Penthouses</td> <td align="center">8482 SQFT</td> 
                  <td align="center"><a href="javascript:void(0)"  class="btn btn-warning makeNewEnquiry" >Enquire Now</a><br><br></td>
                </tr>

              </tbody>
            </table>
      </div>
  </div>
</div>
</div>
<!-- //projects --> 



<!-- contact -->


<div class="gallery" id="projects">
  <div class="container">
  <div class="row">
    <h3 class="title">Location</h3>
      

      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec leftText">
      <h3> Location Advantage </h3>
      
      <br>
      <br>

      <b class="font14">FAR AWAY FROM THE CITY CONGESTION. YET AT THE CENTRE OF EVERYTHING YOU NEED.</b>

      <br>
      <br>
      <p> - DG Hospital : 3.5 km* </p>
      <p> - Metro Station : 4 km* </p>
      <p> - Kumarans School : 5.7 km* </p>
      <p> - Gopalan Mall : 6 km* </p>
      <p> - BGS Global Hospital : 8 km* </p>
      <p> - City Railway Station : 9 km* </p>
      <p> - City Bus Station (Majestic) : 9 km* </p>
      <p> - MG Road : 13 km* </p>
      </div>

      <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
        <h3> Site Location </h3>
        <div class="gallery-grid1"> <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/images/locationmapbig.jpg"> <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/images/locationmapbig.jpg" alt=" " class="img-responsive"> </a> </div>
      </div>

  </div>
</div>
</div>


<!-- //contact -->

<div id="pi">
       <?php
           $vr = $this->session->userdata('user_id');
           $ut = $this->session->userdata('utm_source');
           if( isset($vr) &&  $vr > 0 && $ut != ""){
               echo $this->lead_check->set_pixel($vr, REALESTATE_USER);
               
           }
       ?>
      </div> 



<footer class="bg-bark">
  <div class="copy-right-grids">
    <div class="col-sm-12 bg-bark" style="background-color: #000; border-bottom: 1px solid #737373; padding-bottom: 20px;">
     <img src="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/images/Anarock-logo.png" alt="group" width="180"> 

    <br>
            
          </div>
          <br>
          
           <div class="col-sm-12 bg-bark" style="background-color: #000;">         
           
           <h5 style="color: #ADADAD; font-size: 16px; padding-top: 10px; line-height: 1.5; font-weight: 600">Regional Office - Bengaluru:</h5>
            <p style="font-size: 12px; padding: 10px; line-height: 1.5;color: #ADADAD; ">
              Corniche Al-Latheef, A Wing, Ground Floor, No - 25, Cunningham Road, Bengaluru - 560 052 
            </p>
            <h5 style="color: #ADADAD; font-size: 16px; padding-top: 10px; line-height: 1.5; font-weight: 600">Disclaimer: </h5>
            <p style="color: #ADADAD; font-size: 12px; padding: 10px; line-height: 1.5">
              Occupation Certificate received for towers for towers Almora &amp; Elana on 23/08/2016 and towers Altura &amp; Altezza
               on 23/08/2017. The distances and timelines are tentative and may differ subject to infrastructure and weather 
               conditions. The upcoming developments will be done by government or third parties. The price shall be subject to
                the terms and condition of allotment letter and agreement for sale. For further communication please contact Site
                 office at Promont Hill, SY No.168, Off Ittamadu Main Road, Hosakerehalli Village, Banashankari Stage III,
                  Bengaluru–560 085 085.<br> <font style="font-size: 14px"> 
                 *T&amp;C Apply.</a></font> 
            </p>
            <h5 style="color: #ADADAD; font-size: 16px; padding-top: 10px; line-height: 1.5; font-weight: 600">ANAROCK</h5>
            <p style="color: #ADADAD;font-size: 12px; padding: 10px; line-height: 1.5">
              ANAROCK Property Consultants Pvt. Ltd. (Formerly Jones Lang LaSalle Residential Pvt. Ltd.) PRM/KA/1251/446/AG/171110/000554
            </p>
            
          </div>
  </div>
</footer>

<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
<script src="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/js/vendor.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script> 
<script src="<?php echo S3_URL?>/site/realestate-assets/tata/tatahomespromont/js/main.js"></script> 
<script src="<?php echo S3_URL?>/site/scripts/properties.js"></script>
   
<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}
</script>

    
    
</body>
</html>