<?php $all_array = all_arrays(); 

?>
<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title> Brigade North Home Fest | Aug 30th to Sept 1st at Brigade Orchards, Devanahalli </title>      
<link rel="shortcut icon" href="<?php echo S3_URL?>/site/brigade-assets/images/fav.png" type="image/x-icon" />


<link rel="stylesheet" href="<?php echo S3_URL?>/site/brigade-assets/css/vendor.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel="stylesheet" href="<?php echo S3_URL?>/site/brigade-assets/css/main.css">    
<!-- fonts -->
<link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

<!-- DO NOT MODIFY -->
<!-- Quora Pixel Code (JS Helper) -->
<script>
!function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
qp('init', '0250ea612ac748c6b5516e1e84caa786');
qp('track', 'ViewContent');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/0250ea612ac748c6b5516e1e84caa786/pixel?tag=ViewContent&noscript=1"/></noscript>
<!-- End of Quora Pixel Code --> 
<script>qp('track', 'GenerateLead');</script>


   </head>
   <body> 
<!-- 	
   <header class="">
         <div class="logo">
            <a href="JavaScript:Void(0);" target=""><img src="<?php echo S3_URL?>/site/brigade-assets/images/brigade.png"></a>
         </div>
         <div class="clearB"></div>
	  </header> -->
	  
   <?php $all_array = all_arrays(); ?>   
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="">
					<div class="carousel-caption">
                        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="nmims_jsfrm('<?php echo SITE_URL?>nmims/submit_frm','1')">
                            <div class="">
                            <div class="col">
                                <div class="form-logo">
                                    <a href="#">
                                        <img src="<?php echo S3_URL?>/site/brigade-assets/images/brigade.png" />
                                    </a> 
                                </div> 
                                <h4>Building Positive Experience</h4>  
                                
                            </div>
                        </form>
                    </div>
				</div>
			</div>
		</div>

	</div>
    <!--//banner -->    
    
    <section>
        <div class="PDIDM-section" id="Program">
            <div class="pdidm-block">
                <div class="L-pdidm-block">
                    <div class="pdidm-bg">
                        
                    </div>
                    <h2 class="heading-01"> Brigade <span class="green-color db-text"> North Home Fest </span></h2>
                    
                    <p>
                        As internet becomes a seamless part of the day &amp; life of a prime population the bar for marketing touchpoints is constantly shifting and evolving. Consumers, empowered by information, are demanding unique-value-attributed communication across platforms &amp; mediums.</p>
                        <br>
                        <p>Few business functions have been as disrupted by this digitalisation as marketing. In order to keep digital marketer at the forefront of these changes, Brigade has designed the Professional Diploma in Digital Marketing - to develop expertise in every facet of digital marketing, from strategy and organizational design to customer impact and scalable execution.  
                    </p>
                </div>
                <div class="R-pdidm-block">
                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/about_north.jpg">
                </div>
                <i class="clearB"></i>
            </div>
        </div>
    </section>
    

    <section>
        <div class="programme_structure_section" id="Specialisations">
            <div class="container">					
                <div class="specialisation_block">
                    <h2 class="heading-01 ">Projects</h2>
                    <div class="programme_section">
                        <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                            <ul class="resp-tabs-list">
                                <li class="resp-tab-item1 resp-tab-active" aria-controls="tab_item-0" role="tab">All Projects</li>
                                <li class="resp-tab-item2" aria-controls="tab_item-1" role="tab">New Lunches</li>
                                <li class="resp-tab-item3" aria-controls="tab_item-2" role="tab">Apartments</li>
                                <li class="resp-tab-item4" aria-controls="tab_item-3" role="tab">Villas</li>
                                <li class="resp-tab-item5" aria-controls="tab_item-4" role="tab">Senior Living</li>
                            </ul>
                            <div class="resp-tabs-container">
                                <h2 class="resp-accordion resp-accordion1 resp-tab-active" role="tab" aria-controls="tab_item-0"><span class="resp-arrow"></span>Core Specialisation</h2><div class="resp-tab-content resp-tab-content1 resp-tab-content-active mobileTab" aria-labelledby="tab_item-0">
                                    <div class="sem01-T">
                                    <div class="latest-post sppb-col-sm-6 col-md-6">
                                        <div class="prop-div">
                                            <a class="move">
                                                <div class="prop-img-div">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/Orchards-Apartments.jpg" class="full-width zoomimg containerImageHeigth">
                                                    <p class="prop-name">
                                                        Brigade Orchards
                                                        -
                                                        Apartments</p>
                                                    <p class="prop-price">
                                                        Rs. 54 Lakh
                                                        Onwards<sup>*</sup>
                                                    </p>
                                                    
                                                </div>
                                            </a>
                                            <div class="prop-details">
                                                <p class="rera">
                                                    Orchards: Deodar &amp;
                                                    Cedar: OC Received
                                                    <br>
                                                    Juniper RERA Number:
                                                    PRM/KA/RERA/1250/303/PR/170916/000462
                                                </p>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Location</p>
                                                    <p class="prop-desc">
                                                        Devanahalli, 10
                                                        Mins
                                                        from Bangalore
                                                        Airport
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Configuration
                                                    </p>
                                                    <p class="prop-desc">
                                                        2 &amp; 3 Bedroom
                                                        Homes
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Cost range</p>
                                                    <p class="prop-desc">
                                                        <a class="move" title="Check Price" onclick="pricePopProjectname('Orchards Apts');">Check
                                                            Price</a>
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        About the
                                                        Project
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink1 move">view</a>
                                                    </p>

                                                </div>
                                                <div class="prop-inner viewAboutDiv1 displayNone" >
                                                    <p>Brigade Orchards, One Of India’s Top 3 Smart Townships is set to
                                                         revolutionise urban lifestyle by integrating more than just the
                                                          ordinary into a single enclave. A 130-acre haven, within the
                                                           complex are Residences, School by Jain Group, Proposed Hospital,
                                                            Signature Club resort, World Class Sports Arena, Shopping,
                                                             Leisure and Offices. Just 10-minutes away from Bangalore 
                                                             International airport, Brigade Orchards is strategically
                                                              located away from the road and air-traffic related noise 
                                                              and pollution. A six-lane highway from Hebbal to Devanahalli 
                                                              makes commute from Brigade Orchards to Hebbal / Whitefield a
                                                               mere 30-45 minutes drive.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Amenities &amp;
                                                        Neighbourhood
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink2 move">view</a>
                                                    </p>
                                                </div>

                                                <div class="prop-inner viewAboutDiv2 displayNone" >
                                                    <p>Ready to Use 80,000 Sq Ft Signature Club Resort with Six-lane Indoor 
                                                        Heated Pool, Two Badminton Courts with Oak Wood Flooring, Squash Court with
                                                         Maple Wood Flooring, State of the Art Gymnasium, Indoor Games, Aerobics Room.
                                                          Multi-Cuisine Restaurant, Cafe &amp; Bar, Over 15,000 sq.ft of Banqueting Space 
                                                          with Green Lawns. Full-Fledged Stadium for Sports like Cricket, Tennis, Football
                                                          , Volleyball and Basketball with Practice Nets, Pitches and a 400 Metre 
                                                          Track Centred around
                                                         Seating for 1500 People. School with JGI Group from Kindergarten to Class 10/12.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        For more details
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="move clickhere" onclick="pricePopProjectname('Orchards Apts');">click
                                                            here</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="sem01-B">
                                    <div class="latest-post sppb-col-sm-6 col-md-6">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/kino.jpg" class="full-width zoomimg containerImageHeigth">
                                                    <p class="prop-name">
                                                    KINO At Brigade Orchards</p>
                                                    <p class="prop-price">
                                                        Less Than 27 Lakhs<sup>*</sup>
                                                    </p>
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/gst.png" class="gst">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/newlaunch.png" class="newlaunch">
                                                </div>
                                            </a>
                                            <div class="prop-details">
                                                <p class="rera">
                                                KINO RERA Number:
                                                    <br>
                                                    PRM/KA/RERA/1250/303/PR/190614/002610
                                                </p>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Location</p>
                                                    <p class="prop-desc">
                                                        Devanahalli, 10
                                                        Mins
                                                        from Bangalore
                                                        Airport
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Configuration
                                                    </p>
                                                    <p class="prop-desc">
                                                    1, 1.5 & 2 Bedroom Homes
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Cost range</p>
                                                    <p class="prop-desc">
                                                        <a class="move" title="Check Price" onclick="pricePopProjectname('Orchards Apts');">Check
                                                            Price</a>
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        About the
                                                        Project
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink3 move">view</a>
                                                    </p>

                                                </div>
                                                <div class="prop-inner viewAboutDiv3 displayNone">
                                                    <p>These are exclusive, one-a-floor homes at the most sought-after address in Hyderabad. Just 55 apartments in all make each one a premium investment and desirable address. Every home here has the best fittings and finishes, using the finest of marbles, hardwood flooring, and vitrified tiles.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Amenities &amp;
                                                        Neighbourhood
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink4 move">view</a>
                                                    </p>
                                                </div>

                                                <div class="prop-inner viewAboutDiv4 displayNone" >
                                                    <p>Open Gym Area, Yoga Meditation area, kids Play area, Party lawn, Liesure Seating space, Pool Deck</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        For more details
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="move clickhere" onclick="pricePopProjectname('Orchards Apts');">click
                                                            here</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <i class="clearB"></i>

                                    <div class="sem01-T">
                                    <div class="latest-post sppb-col-sm-6 col-md-6">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/Atmosphere.jpg" class="full-width zoomimg containerImageHeigth">
                                                    <p class="prop-name">
                                                    Brigade Orchards - Pavilion Villas</p>
                                                    <p class="prop-price">
                                                    Rs. 3.99 Crore Onwards*<sup>*</sup>
                                                    </p>
                                                </div>
                                            </a>
                                            <div class="prop-details">
                                            <p class="rera">
                                            OC Received
                                            </p>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Location</p>
                                                    <p class="prop-desc">
                                                        Devanahalli, 10
                                                        Mins
                                                        from Bangalore
                                                        Airport
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Configuration
                                                    </p>
                                                    <p class="prop-desc">
                                                    4 Bedroom Villas
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Cost range</p>
                                                    <p class="prop-desc">
                                                        <a class="move" title="Check Price" onclick="pricePopProjectname('Orchards Apts');">Check
                                                            Price</a>
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        About the
                                                        Project
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink5 move">view</a>
                                                    </p>

                                                </div>
                                                <div class="prop-inner viewAboutDiv5 displayNone" >
                                                    <p>Brigade Orchards, One Of India’s Top 3 Smart Townships is set to revolutionise urban lifestyle by integrating more than just the ordinary into a single enclave. A 130-acre haven, within the complex are Residences, School by Jain Group, Proposed Hospital, Signature Club resort, World Class Sports Arena, Shopping, Leisure and Offices. Just 10-minutes away from Bangalore International airport, Brigade Orchards is strategically located away from the road and air-traffic related noise and pollution. A six-lane highway from Hebbal to Devanahalli makes commute from Brigade Orchards to Hebbal / Whitefield a mere 30-45 minutes drive.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Amenities &amp;
                                                        Neighbourhood
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink6 move">view</a>
                                                    </p>
                                                </div>

                                                <div class="prop-inner viewAboutDiv6 displayNone">
                                                    <p>Ready to Use 80,000 Sq Ft Signature Club Resort with Six-lane Indoor 
                                                        Heated Pool, Two Badminton Courts with Oak Wood Flooring, Squash Court with
                                                         Maple Wood Flooring, State of the Art Gymnasium, Indoor Games, Aerobics Room.
                                                          Multi-Cuisine Restaurant, Cafe &amp; Bar, Over 15,000 sq.ft of Banqueting Space 
                                                          with Green Lawns. Full-Fledged Stadium for Sports like Cricket, Tennis, Football
                                                          , Volleyball and Basketball with Practice Nets, Pitches and a 400 Metre 
                                                          Track Centred around
                                                         Seating for 1500 People. School with JGI Group from Kindergarten to Class 10/12.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        For more details
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="move clickhere" onclick="pricePopProjectname('Orchards Apts');">click
                                                            here</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                               <div class="sem01-B">
                                    <div class="latest-post sppb-col-sm-6 col-md-6">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/Brigade-Pavilion-Villas.jpg" class="full-width zoomimg containerImageHeigth">
                                                    <p class="prop-name">
                                                    Brigade Atmosphere</p>
                                                    <p class="prop-price">
                                                    Rs. 2.18 Crore Onwards<sup>*</sup>
                                                    </p>
                                                </div>
                                            </a>
                                            <div class="prop-details">
                                            <p class="rera">
                                                OC Received
                                            </p>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Location</p>
                                                    <p class="prop-desc">
                                                    Devanahalli, Near Bangalore Airport
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Configuration
                                                    </p>
                                                    <p class="prop-desc">
                                                    4 Bedroom Villas
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Cost range</p>
                                                    <p class="prop-desc">
                                                        <a class="move" title="Check Price" onclick="pricePopProjectname('Orchards Apts');">Check
                                                            Price</a>
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        About the
                                                        Project
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink7 move">view</a>
                                                    </p>

                                                </div>
                                                <div class="prop-inner viewAboutDiv7 displayNone" >
                                                    <p>A truly unique residential layout of 109 villas. A cluster of 8 Courtyard Villas that surround a beautiful courtyard with a Central Boulevard, connecting all the clusters. Each Courtyard Villa, is a fine example of what luxury ought to be. The contemporary aesthetics resonating across the different levels of the villa create a residence of understated luxury.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Amenities &amp;
                                                        Neighbourhood
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink8 move">view</a>
                                                    </p>
                                                </div>

                                                <div class="prop-inner viewAboutDiv8 displayNone">
                                                    <p>Convenience Store, Swimming Pool, Gymnasium, Health Club, Badminton Court, Amphitheatre, Children's Play Area, Guest Rooms, Library, Multi-Purpose Hall, Indoor Games.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        For more details
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="move clickhere" onclick="pricePopProjectname('Orchards Apts');">click
                                                            here</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    </div>


                                    <div class="sem01-T">
                                    <div class="latest-post sppb-col-sm-6 col-md-6">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/parkside-north.jpg" class="full-width zoomimg containerImageHeigth" >
                                                    <p class="prop-name">
                                                    Parkside North By Brigade</p>
                                                    <p class="prop-price">
                                                        Rs. 38 Lakh
                                                        Onwards<sup>*</sup>
                                                    </p>
                                                </div>
                                            </a>
                                            <div class="prop-details">
                                                <p class="rera">
                                                RERA Number: PRM/KA/RERA/1251/309/PR/181122/00215</p>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Location</p>
                                                    <p class="prop-desc">
                                                    Jalahalli
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Configuration
                                                    </p>
                                                    <p class="prop-desc">
                                                    1 and 2 Bedroom Homes
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Cost range</p>
                                                    <p class="prop-desc">
                                                        <a class="move" title="Check Price" onclick="pricePopProjectname('Orchards Apts');">Check
                                                            Price</a>
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        About the
                                                        Project
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink9 move">view</a>
                                                    </p>

                                                </div>
                                                <div class="prop-inner viewAboutDiv9 displayNone" >
                                                    <p>Introducing CLASSIC & PRIME at Parkside North by Brigade - Parkside North, a unique gated communities where kids can spend quality time with their grandparents, listening to their stories from yesteryears and where grandparents can forget about age and just be kids. Thoughtfully crafted, keeping in mind the specific needs of varying ages, this community has one set of blocks named CLASSIC, dedicated solely for the seniors, with a range of special services and features. While the other set of blocks named PRIME, caters to the lifestyle choices of today's cosmopolitan families.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Amenities &amp;
                                                        Neighbourhood
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink10 move">view</a>
                                                    </p>
                                                </div>

                                                <div class="prop-inner viewAboutDiv10 displayNone" >
                                                    <p>Swimming Pool, Reflexology Walk, Jogging Track, Outdoor Gym, Multiplay Court, Yoga Area, Outdoor Cafe</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        For more details
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="move clickhere" onclick="pricePopProjectname('Orchards Apts');">click
                                                            here</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                               <div class="sem01-B">
                                    <div class="latest-post sppb-col-sm-6 col-md-6">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/Bricklane.jpg" class="full-width zoomimg containerImageHeigth" >
                                                    <p class="prop-name">
                                                    Brigade Bricklane</p>
                                                    <p class="prop-price">
                                                        Rs. 34 Lakh
                                                        Onwards<sup>*</sup>
                                                    </p>
                                                </div>
                                            </a>
                                            <div class="prop-details">
                                            <p class="rera">
                                            RERA Number: PRM/KA/RERA/1251/309/PR/180808/001981
                                            </p>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Location</p>
                                                    <p class="prop-desc">
                                                    Kogilu Road, Jakkur
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Configuration
                                                    </p>
                                                    <p class="prop-desc">
                                                        1 and 2 Bedroom
                                                        Homes
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Cost range</p>
                                                    <p class="prop-desc">
                                                        <a class="move" title="Check Price" onclick="pricePopProjectname('Orchards Apts');">Check
                                                            Price</a>
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        About the
                                                        Project
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink11 move">view</a>
                                                    </p>

                                                </div>
                                                <div class="prop-inner viewAboutDiv11 displayNone" >
                                                    <p>Brigade Bricklane is a thoughtfully conceived low-rise complex that blends in modern conveniences and amenities with an enriching lifestyle. Celebrating life’s countless memorable moments, one can rejoice in the benefits of residing within a friendly urban community close to the International Airport and several renowned offices.
Its key highlight, an urban window that allows seamless accessibility through the enclave, Brigade Bricklane is just the place for young and discerning home buyers.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Amenities &amp;
                                                        Neighbourhood
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink12 move">view</a>
                                                    </p>
                                                </div>

                                                <div class="prop-inner viewAboutDiv12 displayNone">
                                                    <p>Tennis Court, Half Basketball Court, Badminton Court, Swimming Pool, Fitness Center, Landscaped Garden, Jogging Track, Chidlren's Play Area, Todler's Pool, Skating Arena, Amphitheatre, Senior Citizen Sitting.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        For more details
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="move clickhere" onclick="pricePopProjectname('Orchards Apts');">click
                                                            here</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    </div>


                                    <div class="sem01-T">
                                    <div class="latest-post sppb-col-sm-6 col-md-6">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/el-dorado.jpg" class="full-width zoomimg containerImageHeigth">
                                                    <p class="prop-name">
                                                    Brigade El Dorado</p>
                                                    <p class="prop-price">
                                                    Less Than 35 Lakh<sup>*</sup>
                                                    </p>
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/gst.png" class="gst">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/newlaunch.png" class="newlaunch">
                                                </div>
                                            </a>
                                            <div class="prop-details">
                                            <p class="rera">Gallium at Brigade El Dorado <br>
                                                            RERA Number:PRM/KA/RERA/1251/472/PR/190427/002540
                                            </p>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Location</p>
                                                    <p class="prop-desc">
                                                    Aerospace Park, Bangalore North
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Configuration
                                                    </p>
                                                    <p class="prop-desc">
                                                         2 &amp; 3 Bedroom Homes
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Cost range</p>
                                                    <p class="prop-desc">
                                                        <a class="move" title="Check Price" onclick="pricePopProjectname('Orchards Apts');">Check
                                                            Price</a>
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        About the
                                                        Project
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink13 move">view</a>
                                                    </p>

                                                </div>
                                                <div class="prop-inner viewAboutDiv13 displayNone" >
                                                    <p>Brigade El Dorado - A integrated enclave designed with elegant and expansive landscaping, clubhouses with world-class amenities, sports facilities, retail for a healthy lifestyle and much more.
The holistic approach to landscape planning, urban design, and architecture allows the 50-acre land parcel to have a large 10-acre central green for outdoor sports and leisure activities.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Amenities &amp;
                                                        Neighbourhood
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink14 move">view</a>
                                                    </p>
                                                </div>

                                                <div class="prop-inner viewAboutDiv14 displayNone" >
                                                    <p>Football Field, Multi Purpose Lawn/Party halls, Cricket Field, Swimming Pool, Kids Pool, Tennis Court, Badminton Court, Volleyball Court, Squash Court, Table Tennis, Library, Gym, Yoga + Aerobics, Spa, Meeting Rooms, Convenience Store, Basketball Court, Skating Rink, Golf Putting Green, Pet Park, Open Air Amphitheatre, Multiple Childrens’ Playgrounds, Crèche, Pool Tables</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        For more details
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="move clickhere" onclick="pricePopProjectname('Orchards Apts');">click
                                                            here</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                </div>
                                <h2 class="resp-accordion resp-accordion2" role="tab" aria-controls="tab_item-2"><span class="resp-arrow"></span>Second Specialisations</h2>
                                <div class="resp-tab-content resp-tab-content2" aria-labelledby="tab_item-2">
                                <div class="sem01-T">
                                    <div class="latest-post sppb-col-sm-6 col-md-6">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/kino.jpg" class="full-width zoomimg containerImageHeigth" >
                                                    <p class="prop-name">
                                                    KINO At Brigade Orchards</p>
                                                    <p class="prop-price">
                                                        Less Than 27 Lakhs<sup>*</sup>
                                                    </p>
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/gst.png" class="gst">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/newlaunch.png" class="newlaunch">
                                                </div>
                                            </a>
                                            <div class="prop-details">
                                                <p class="rera">
                                                KINO RERA Number:
                                                    <br>
                                                    PRM/KA/RERA/1250/303/PR/190614/002610
                                                </p>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Location</p>
                                                    <p class="prop-desc">
                                                        Devanahalli, 10
                                                        Mins
                                                        from Bangalore
                                                        Airport
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Configuration
                                                    </p>
                                                    <p class="prop-desc">
                                                    1, 1.5 & 2 Bedroom Homes
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Cost range</p>
                                                    <p class="prop-desc">
                                                        <a class="move" title="Check Price" onclick="pricePopProjectname('Orchards Apts');">Check
                                                            Price</a>
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        About the
                                                        Project
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink3 move">view</a>
                                                    </p>

                                                </div>
                                                <div class="prop-inner viewAboutDiv3 displayNone">
                                                    <p>These are exclusive, one-a-floor homes at the most sought-after address in Hyderabad. Just 55 apartments in all make each one a premium investment and desirable address. Every home here has the best fittings and finishes, using the finest of marbles, hardwood flooring, and vitrified tiles.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Amenities &amp;
                                                        Neighbourhood
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink4 move">view</a>
                                                    </p>
                                                </div>

                                                <div class="prop-inner viewAboutDiv4 displayNone" >
                                                    <p>Open Gym Area, Yoga Meditation area, kids Play area, Party lawn, Liesure Seating space, Pool Deck</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        For more details
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="move clickhere" onclick="pricePopProjectname('Orchards Apts');">click
                                                            here</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="sem01-B">
                                    <div class="latest-post sppb-col-sm-6 col-md-6">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/el-dorado.jpg" class="full-width zoomimg containerImageHeigth" >
                                                    <p class="prop-name">
                                                    Brigade El Dorado</p>
                                                    <p class="prop-price">
                                                    Less Than 35 Lakh<sup>*</sup>
                                                    </p>
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/gst.png" class="gst">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/newlaunch.png" class="newlaunch">
                                                </div>
                                            </a>
                                            <div class="prop-details">
                                            <p class="rera">Gallium at Brigade El Dorado <br>
                                                            RERA Number:PRM/KA/RERA/1251/472/PR/190427/002540
                                            </p>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Location</p>
                                                    <p class="prop-desc">
                                                    Aerospace Park, Bangalore North
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Configuration
                                                    </p>
                                                    <p class="prop-desc">
                                                         2 &amp; 3 Bedroom Homes
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Cost range</p>
                                                    <p class="prop-desc">
                                                        <a class="move" title="Check Price" onclick="pricePopProjectname('Orchards Apts');">Check
                                                            Price</a>
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        About the
                                                        Project
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink13 move">view</a>
                                                    </p>

                                                </div>
                                                <div class="prop-inner viewAboutDiv13 displayNone" >
                                                    <p>Brigade El Dorado - A integrated enclave designed with elegant and expansive landscaping, clubhouses with world-class amenities, sports facilities, retail for a healthy lifestyle and much more.
The holistic approach to landscape planning, urban design, and architecture allows the 50-acre land parcel to have a large 10-acre central green for outdoor sports and leisure activities.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Amenities &amp;
                                                        Neighbourhood
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink14 move">view</a>
                                                    </p>
                                                </div>

                                                <div class="prop-inner viewAboutDiv14 displayNone" >
                                                    <p>Football Field, Multi Purpose Lawn/Party halls, Cricket Field, Swimming Pool, Kids Pool, Tennis Court, Badminton Court, Volleyball Court, Squash Court, Table Tennis, Library, Gym, Yoga + Aerobics, Spa, Meeting Rooms, Convenience Store, Basketball Court, Skating Rink, Golf Putting Green, Pet Park, Open Air Amphitheatre, Multiple Childrens’ Playgrounds, Crèche, Pool Tables</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        For more details
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="move clickhere" onclick="pricePopProjectname('Orchards Apts');">click
                                                            here</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                    
                                    <i class="clearB"></i>
                                </div>

                                

                                <h2 class="resp-accordion resp-accordion3" role="tab" aria-controls="tab_item-3"><span class="resp-arrow"></span>Third Specialisations</h2>
                                <div class="resp-tab-content resp-tab-content3" aria-labelledby="tab_item-3">
                                

                                <div class="sem01-T">
                                    <div class="latest-post sppb-col-sm-6 col-md-6">
                                        <div class="prop-div">
                                            <a class="move">
                                                <div class="prop-img-div">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/Orchards-Apartments.jpg" class="full-width zoomimg containerImageHeigth" >
                                                    <p class="prop-name">
                                                        Brigade Orchards
                                                        -
                                                        Apartments</p>
                                                    <p class="prop-price">
                                                        Rs. 54 Lakh
                                                        Onwards<sup>*</sup>
                                                    </p>
                                                    
                                                </div>
                                            </a>
                                            <div class="prop-details">
                                                <p class="rera">
                                                    Orchards: Deodar &amp;
                                                    Cedar: OC Received
                                                    <br>
                                                    Juniper RERA Number:
                                                    PRM/KA/RERA/1250/303/PR/170916/000462
                                                </p>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Location</p>
                                                    <p class="prop-desc">
                                                        Devanahalli, 10
                                                        Mins
                                                        from Bangalore
                                                        Airport
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Configuration
                                                    </p>
                                                    <p class="prop-desc">
                                                        2 &amp; 3 Bedroom
                                                        Homes
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Cost range</p>
                                                    <p class="prop-desc">
                                                        <a class="move" title="Check Price" onclick="pricePopProjectname('Orchards Apts');">Check
                                                            Price</a>
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        About the
                                                        Project
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink1 move">view</a>
                                                    </p>

                                                </div>
                                                <div class="prop-inner viewAboutDiv1 displayNone" >
                                                    <p>Brigade Orchards, One Of India’s Top 3 Smart Townships is set to
                                                         revolutionise urban lifestyle by integrating more than just the
                                                          ordinary into a single enclave. A 130-acre haven, within the
                                                           complex are Residences, School by Jain Group, Proposed Hospital,
                                                            Signature Club resort, World Class Sports Arena, Shopping,
                                                             Leisure and Offices. Just 10-minutes away from Bangalore 
                                                             International airport, Brigade Orchards is strategically
                                                              located away from the road and air-traffic related noise 
                                                              and pollution. A six-lane highway from Hebbal to Devanahalli 
                                                              makes commute from Brigade Orchards to Hebbal / Whitefield a
                                                               mere 30-45 minutes drive.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Amenities &amp;
                                                        Neighbourhood
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink2 move">view</a>
                                                    </p>
                                                </div>

                                                <div class="prop-inner viewAboutDiv2 displayNone" >
                                                    <p>Ready to Use 80,000 Sq Ft Signature Club Resort with Six-lane Indoor 
                                                        Heated Pool, Two Badminton Courts with Oak Wood Flooring, Squash Court with
                                                         Maple Wood Flooring, State of the Art Gymnasium, Indoor Games, Aerobics Room.
                                                          Multi-Cuisine Restaurant, Cafe &amp; Bar, Over 15,000 sq.ft of Banqueting Space 
                                                          with Green Lawns. Full-Fledged Stadium for Sports like Cricket, Tennis, Football
                                                          , Volleyball and Basketball with Practice Nets, Pitches and a 400 Metre 
                                                          Track Centred around
                                                         Seating for 1500 People. School with JGI Group from Kindergarten to Class 10/12.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        For more details
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="move clickhere" onclick="pricePopProjectname('Orchards Apts');">click
                                                            here</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                    <div class="sem01-B">
                                    <div class="latest-post sppb-col-sm-6 col-md-6">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/kino.jpg" class="full-width zoomimg containerImageHeigth">
                                                    <p class="prop-name">
                                                    KINO At Brigade Orchards</p>
                                                    <p class="prop-price">
                                                        Less Than 27 Lakhs<sup>*</sup>
                                                    </p>
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/gst.png" class="gst">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/newlaunch.png" class="newlaunch">
                                                </div>
                                            </a>
                                            <div class="prop-details">
                                                <p class="rera">
                                                KINO RERA Number:
                                                    <br>
                                                    PRM/KA/RERA/1250/303/PR/190614/002610
                                                </p>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Location</p>
                                                    <p class="prop-desc">
                                                        Devanahalli, 10
                                                        Mins
                                                        from Bangalore
                                                        Airport
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Configuration
                                                    </p>
                                                    <p class="prop-desc">
                                                    1, 1.5 & 2 Bedroom Homes
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Cost range</p>
                                                    <p class="prop-desc">
                                                        <a class="move" title="Check Price" onclick="pricePopProjectname('Orchards Apts');">Check
                                                            Price</a>
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        About the
                                                        Project
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink3 move">view</a>
                                                    </p>

                                                </div>
                                                <div class="prop-inner viewAboutDiv3 displayNone">
                                                    <p>These are exclusive, one-a-floor homes at the most sought-after address in Hyderabad. Just 55 apartments in all make each one a premium investment and desirable address. Every home here has the best fittings and finishes, using the finest of marbles, hardwood flooring, and vitrified tiles.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Amenities &amp;
                                                        Neighbourhood
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink4 move">view</a>
                                                    </p>
                                                </div>

                                                <div class="prop-inner viewAboutDiv4 displayNone" >
                                                    <p>Open Gym Area, Yoga Meditation area, kids Play area, Party lawn, Liesure Seating space, Pool Deck</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        For more details
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="move clickhere" onclick="pricePopProjectname('Orchards Apts');">click
                                                            here</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <i class="clearB"></i>                                


                               


                                    <div class="sem01-T">
                                    <div class="latest-post sppb-col-sm-6 col-md-6">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/parkside-north.jpg" class="full-width zoomimg containerImageHeigth" >
                                                    <p class="prop-name">
                                                    Parkside North By Brigade</p>
                                                    <p class="prop-price">
                                                        Rs. 38 Lakh
                                                        Onwards<sup>*</sup>
                                                    </p>
                                                </div>
                                            </a>
                                            <div class="prop-details">
                                                <p class="rera">
                                                RERA Number: PRM/KA/RERA/1251/309/PR/181122/00215</p>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Location</p>
                                                    <p class="prop-desc">
                                                    Jalahalli
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Configuration
                                                    </p>
                                                    <p class="prop-desc">
                                                    1 and 2 Bedroom Homes
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Cost range</p>
                                                    <p class="prop-desc">
                                                        <a class="move" title="Check Price" onclick="pricePopProjectname('Orchards Apts');">Check
                                                            Price</a>
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        About the
                                                        Project
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink9 move">view</a>
                                                    </p>

                                                </div>
                                                <div class="prop-inner viewAboutDiv9 displayNone" >
                                                    <p>Introducing CLASSIC & PRIME at Parkside North by Brigade - Parkside North, a unique gated communities where kids can spend quality time with their grandparents, listening to their stories from yesteryears and where grandparents can forget about age and just be kids. Thoughtfully crafted, keeping in mind the specific needs of varying ages, this community has one set of blocks named CLASSIC, dedicated solely for the seniors, with a range of special services and features. While the other set of blocks named PRIME, caters to the lifestyle choices of today's cosmopolitan families.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Amenities &amp;
                                                        Neighbourhood
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink10 move">view</a>
                                                    </p>
                                                </div>

                                                <div class="prop-inner viewAboutDiv10 displayNone" >
                                                    <p>Swimming Pool, Reflexology Walk, Jogging Track, Outdoor Gym, Multiplay Court, Yoga Area, Outdoor Cafe</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        For more details
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="move clickhere" onclick="pricePopProjectname('Orchards Apts');">click
                                                            here</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>


                               <div class="sem01-B">
                                    <div class="latest-post sppb-col-sm-6 col-md-6">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/Bricklane.jpg" class="full-width zoomimg containerImageHeigth" >
                                                    <p class="prop-name">
                                                    Brigade Bricklane</p>
                                                    <p class="prop-price">
                                                        Rs. 34 Lakh
                                                        Onwards<sup>*</sup>
                                                    </p>
                                                </div>
                                            </a>
                                            <div class="prop-details">
                                            <p class="rera">
                                            RERA Number: PRM/KA/RERA/1251/309/PR/180808/001981
                                            </p>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Location</p>
                                                    <p class="prop-desc">
                                                    Kogilu Road, Jakkur
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Configuration
                                                    </p>
                                                    <p class="prop-desc">
                                                        1 and 2 Bedroom
                                                        Homes
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Cost range</p>
                                                    <p class="prop-desc">
                                                        <a class="move" title="Check Price" onclick="pricePopProjectname('Orchards Apts');">Check
                                                            Price</a>
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        About the
                                                        Project
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink11 move">view</a>
                                                    </p>

                                                </div>
                                                <div class="prop-inner viewAboutDiv11 displayNone" >
                                                    <p>Brigade Bricklane is a thoughtfully conceived low-rise complex that blends in modern conveniences and amenities with an enriching lifestyle. Celebrating life’s countless memorable moments, one can rejoice in the benefits of residing within a friendly urban community close to the International Airport and several renowned offices.
Its key highlight, an urban window that allows seamless accessibility through the enclave, Brigade Bricklane is just the place for young and discerning home buyers.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Amenities &amp;
                                                        Neighbourhood
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink12 move">view</a>
                                                    </p>
                                                </div>

                                                <div class="prop-inner viewAboutDiv12 displayNone">
                                                    <p>Tennis Court, Half Basketball Court, Badminton Court, Swimming Pool, Fitness Center, Landscaped Garden, Jogging Track, Chidlren's Play Area, Todler's Pool, Skating Arena, Amphitheatre, Senior Citizen Sitting.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        For more details
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="move clickhere" onclick="pricePopProjectname('Orchards Apts');">click
                                                            here</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    </div>


                                    <div class="sem01-T">
                                    <div class="latest-post sppb-col-sm-6 col-md-6">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/el-dorado.jpg" class="full-width zoomimg containerImageHeigth" >
                                                    <p class="prop-name">
                                                    Brigade El Dorado</p>
                                                    <p class="prop-price">
                                                    Less Than 35 Lakh<sup>*</sup>
                                                    </p>
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/gst.png" class="gst">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/newlaunch.png" class="newlaunch">
                                                </div>
                                            </a>
                                            <div class="prop-details">
                                            <p class="rera">Gallium at Brigade El Dorado <br>
                                                            RERA Number:PRM/KA/RERA/1251/472/PR/190427/002540
                                            </p>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Location</p>
                                                    <p class="prop-desc">
                                                    Aerospace Park, Bangalore North
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Configuration
                                                    </p>
                                                    <p class="prop-desc">
                                                         2 &amp; 3 Bedroom Homes
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Cost range</p>
                                                    <p class="prop-desc">
                                                        <a class="move" title="Check Price" onclick="pricePopProjectname('Orchards Apts');">Check
                                                            Price</a>
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        About the
                                                        Project
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink13 move">view</a>
                                                    </p>

                                                </div>
                                                <div class="prop-inner viewAboutDiv13 displayNone" >
                                                    <p>Brigade El Dorado - A integrated enclave designed with elegant and expansive landscaping, clubhouses with world-class amenities, sports facilities, retail for a healthy lifestyle and much more.
The holistic approach to landscape planning, urban design, and architecture allows the 50-acre land parcel to have a large 10-acre central green for outdoor sports and leisure activities.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Amenities &amp;
                                                        Neighbourhood
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink14 move">view</a>
                                                    </p>
                                                </div>

                                                <div class="prop-inner viewAboutDiv14 displayNone" >
                                                    <p>Football Field, Multi Purpose Lawn/Party halls, Cricket Field, Swimming Pool, Kids Pool, Tennis Court, Badminton Court, Volleyball Court, Squash Court, Table Tennis, Library, Gym, Yoga + Aerobics, Spa, Meeting Rooms, Convenience Store, Basketball Court, Skating Rink, Golf Putting Green, Pet Park, Open Air Amphitheatre, Multiple Childrens’ Playgrounds, Crèche, Pool Tables</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        For more details
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="move clickhere" onclick="pricePopProjectname('Orchards Apts');">click
                                                            here</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                    
                                    <i class="clearB"></i>
                                </div>

                                <h2 class="resp-accordion resp-accordion4" role="tab" aria-controls="tab_item-4"><span class="resp-arrow"></span>Fouth Specialisations</h2>
                                <div class="resp-tab-content resp-tab-content4" aria-labelledby="tab_item-4">
                                <div class="sem01-T">
                                    <div class="latest-post sppb-col-sm-6 col-md-6">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/Atmosphere.jpg" class="full-width zoomimg containerImageHeigth" >
                                                    <p class="prop-name">
                                                    Brigade Orchards - Pavilion Villas</p>
                                                    <p class="prop-price">
                                                    Rs. 3.99 Crore Onwards*<sup>*</sup>
                                                    </p>
                                                </div>
                                            </a>
                                            <div class="prop-details">
                                            <p class="rera">
                                            OC Received
                                            </p>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Location</p>
                                                    <p class="prop-desc">
                                                        Devanahalli, 10
                                                        Mins
                                                        from Bangalore
                                                        Airport
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Configuration
                                                    </p>
                                                    <p class="prop-desc">
                                                    4 Bedroom Villas
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Cost range</p>
                                                    <p class="prop-desc">
                                                        <a class="move" title="Check Price" onclick="pricePopProjectname('Orchards Apts');">Check
                                                            Price</a>
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        About the
                                                        Project
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink5 move">view</a>
                                                    </p>

                                                </div>
                                                <div class="prop-inner viewAboutDiv5 displayNone" >
                                                    <p>Brigade Orchards, One Of India’s Top 3 Smart Townships is set to
                                                         revolutionise urban lifestyle by integrating more than just the
                                                          ordinary into a single enclave. A 130-acre haven, within the
                                                           complex are Residences, School by Jain Group, Proposed Hospital,
                                                            Signature Club resort, World Class Sports Arena, Shopping,
                                                             Leisure and Offices. Just 10-minutes away from Bangalore 
                                                             International airport, Brigade Orchards is strategically
                                                              located away from the road and air-traffic related noise 
                                                              and pollution. A six-lane highway from Hebbal to Devanahalli 
                                                              makes commute from Brigade Orchards to Hebbal / Whitefield a
                                                               mere 30-45 minutes drive.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Amenities &amp;
                                                        Neighbourhood
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink6 move">view</a>
                                                    </p>
                                                </div>

                                                <div class="prop-inner viewAboutDiv6 displayNone">
                                                    <p>Ready to Use 80,000 Sq Ft Signature Club Resort with Six-lane Indoor 
                                                        Heated Pool, Two Badminton Courts with Oak Wood Flooring, Squash Court with
                                                         Maple Wood Flooring, State of the Art Gymnasium, Indoor Games, Aerobics Room.
                                                          Multi-Cuisine Restaurant, Cafe &amp; Bar, Over 15,000 sq.ft of Banqueting Space 
                                                          with Green Lawns. Full-Fledged Stadium for Sports like Cricket, Tennis, Football
                                                          , Volleyball and Basketball with Practice Nets, Pitches and a 400 Metre 
                                                          Track Centred around
                                                         Seating for 1500 People. School with JGI Group from Kindergarten to Class 10/12.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        For more details
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="move clickhere" onclick="pricePopProjectname('Orchards Apts');">click
                                                            here</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    <div class="sem01-B">
                                    <div class="latest-post sppb-col-sm-6 col-md-6">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/Brigade-Pavilion-Villas.jpg" class="full-width zoomimg containerImageHeigth" >
                                                    <p class="prop-name">
                                                    Brigade Atmosphere</p>
                                                    <p class="prop-price">
                                                    Rs. 2.18 Crore Onwards<sup>*</sup>
                                                    </p>
                                                </div>
                                            </a>
                                            <div class="prop-details">
                                            <p class="rera">
                                                OC Received
                                            </p>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Location</p>
                                                    <p class="prop-desc">
                                                    Devanahalli, Near Bangalore Airport
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Configuration
                                                    </p>
                                                    <p class="prop-desc">
                                                    4 Bedroom Villas
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Cost range</p>
                                                    <p class="prop-desc">
                                                        <a class="move" title="Check Price" onclick="pricePopProjectname('Orchards Apts');">Check
                                                            Price</a>
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        About the
                                                        Project
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink7 move">view</a>
                                                    </p>

                                                </div>
                                                <div class="prop-inner viewAboutDiv7 displayNone" >
                                                    <p>A truly unique residential layout of 109 villas. A cluster of 8 Courtyard Villas that surround a beautiful courtyard with a Central Boulevard, connecting all the clusters. Each Courtyard Villa, is a fine example of what luxury ought to be. The contemporary aesthetics resonating across the different levels of the villa create a residence of understated luxury.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Amenities &amp;
                                                        Neighbourhood
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink8 move">view</a>
                                                    </p>
                                                </div>

                                                <div class="prop-inner viewAboutDiv8 displayNone">
                                                    <p>Convenience Store, Swimming Pool, Gymnasium, Health Club, Badminton Court, Amphitheatre, Children's Play Area, Guest Rooms, Library, Multi-Purpose Hall, Indoor Games.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        For more details
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="move clickhere" onclick="pricePopProjectname('Orchards Apts');">click
                                                            here</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    </div>

                                    
                                    <i class="clearB"></i>
                                </div>

                                <h2 class="resp-accordion resp-accordion5" role="tab" aria-controls="tab_item-5"><span class="resp-arrow"></span>Fifth Sec</h2>
                                <div class="resp-tab-content resp-tab-content5" aria-labelledby="tab_item-5">
                                <div class="sem01-T">
                                    <div class="latest-post sppb-col-sm-6 col-md-6">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="<?php echo S3_URL?>/site/brigade-assets/images/parkside-north.jpg" class="full-width zoomimg containerImageHeigth">
                                                    <p class="prop-name">
                                                    Parkside North By Brigade</p>
                                                    <p class="prop-price">
                                                        Rs. 38 Lakh
                                                        Onwards<sup>*</sup>
                                                    </p>
                                                </div>
                                            </a>
                                            <div class="prop-details">
                                                <p class="rera">
                                                RERA Number: PRM/KA/RERA/1251/309/PR/181122/00215</p>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Location</p>
                                                    <p class="prop-desc">
                                                    Jalahalli
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Configuration
                                                    </p>
                                                    <p class="prop-desc">
                                                    1 and 2 Bedroom Homes
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Cost range</p>
                                                    <p class="prop-desc">
                                                        <a class="move" title="Check Price" onclick="pricePopProjectname('Orchards Apts');">Check
                                                            Price</a>
                                                    </p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        About the
                                                        Project
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink9 move">view</a>
                                                    </p>

                                                </div>
                                                <div class="prop-inner viewAboutDiv9 displayNone" >
                                                    <p>Introducing CLASSIC & PRIME at Parkside North by Brigade - Parkside North, a unique gated communities where kids can spend quality time with their grandparents, listening to their stories from yesteryears and where grandparents can forget about age and just be kids. Thoughtfully crafted, keeping in mind the specific needs of varying ages, this community has one set of blocks named CLASSIC, dedicated solely for the seniors, with a range of special services and features. While the other set of blocks named PRIME, caters to the lifestyle choices of today's cosmopolitan families.</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        Amenities &amp;
                                                        Neighbourhood
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="aboutLink10 move">view</a>
                                                    </p>
                                                </div>

                                                <div class="prop-inner viewAboutDiv10 displayNone" >
                                                    <p>Swimming Pool, Reflexology Walk, Jogging Track, Outdoor Gym, Multiplay Court, Yoga Area, Outdoor Cafe</p>
                                                </div>
                                                <div class="prop-inner">
                                                    <p class="prop-title">
                                                        For more details
                                                    </p>
                                                    <p class="prop-desc">
                                                        <a class="move clickhere" onclick="pricePopProjectname('Orchards Apts');">click
                                                            here</a></p>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    

                                    
                                    <i class="clearB"></i>
                                </div>
                            </div>
                        </div>
                        <i class="clearB"></i>
                    </div>
                </div><!--Specialization-->
                
            </div>				
        </div><!--Programme Structure-->
    </section>

    <footer class="footerSec">

        <div id="pi">
        <?php
           $vr = $this->session->userdata('user_id');
           $ut = $this->session->userdata('utm_source');
           if( isset($vr) &&  $vr > 0 && $ut != ""){
               echo $this->lead_check->set_pixel($vr, REALESTATE_USER);
               
           }
        ?>
       </div> 


    
		<div class="copy-right-grids">
            <div class="container">
                <h2>Disclaimer</h2>
                <p class="footer-gd">Please note that the contents and information on this Website are in no way intended to advertise, market, book, sell or invite any member of public or agents to purchase any units or flats in the project.The developer reserves the right to change,revise or make any modification,addition or omission or alteration in the scheme, price as a whole or part thereof at their sole discretion. The final plan will be released once all the government's permission approved.The website is for imaginative purpose and is not a legal document or a binding one.
                        
                </p>
            </div>
		</div>

	</footer>    
                                

	<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
	<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
	<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
	<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
	<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
	
	<script src="<?php echo S3_URL?>/site/brigade-assets/js/vendor.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script> 

    <script src="<?php echo S3_URL?>/site/brigade-assets/js/main.js"></script>
	
	<!-- <script src="<?php echo S3_URL?>/site/scripts/.js"></script> -->


<script>

    $(".resp-tab-item1").click(function(){
		$(this).addClass("resp-tab-active");
        $(".resp-tab-item2").removeClass("resp-tab-active");
        $(".resp-tab-item3").removeClass("resp-tab-active");
        $(".resp-tab-item4").removeClass("resp-tab-active");
        $(".resp-tab-item5").removeClass("resp-tab-active");
		
		$(".resp-tab-content1").addClass("resp-tab-content-active");
		$(".resp-tab-content1").show();

        $(".resp-tab-content2").removeClass("resp-tab-content-active");
        $(".resp-tab-content3").removeClass("resp-tab-content-active");
        $(".resp-tab-content4").removeClass("resp-tab-content-active");
        $(".resp-tab-content5").removeClass("resp-tab-content-active");

        $(".resp-tab-content2").hide();
        $(".resp-tab-content3").hide();
        $(".resp-tab-content4").hide();
        $(".resp-tab-content5").hide();

    });


	$(".resp-tab-item2").click(function(){
		$(this).addClass("resp-tab-active");
        $(".resp-tab-item1").removeClass("resp-tab-active");
        $(".resp-tab-item3").removeClass("resp-tab-active");
        $(".resp-tab-item4").removeClass("resp-tab-active");
        $(".resp-tab-item5").removeClass("resp-tab-active");
		
		$(".resp-tab-content2").addClass("resp-tab-content-active");
		$(".resp-tab-content2").show();

        $(".resp-tab-content1").removeClass("resp-tab-content-active");
        $(".resp-tab-content3").removeClass("resp-tab-content-active");
        $(".resp-tab-content4").removeClass("resp-tab-content-active");
        $(".resp-tab-content5").removeClass("resp-tab-content-active");

        $(".resp-tab-content1").hide();
        $(".resp-tab-content3").hide();
        $(".resp-tab-content4").hide();
        $(".resp-tab-content5").hide();
		
	});

    
    $(".resp-tab-item3").click(function(){
		$(this).addClass("resp-tab-active");
        $(".resp-tab-item2").removeClass("resp-tab-active");
        $(".resp-tab-item1").removeClass("resp-tab-active");
        $(".resp-tab-item4").removeClass("resp-tab-active");
        $(".resp-tab-item5").removeClass("resp-tab-active");
		
		$(".resp-tab-content3").addClass("resp-tab-content-active");
		$(".resp-tab-content3").show();

        $(".resp-tab-content2").removeClass("resp-tab-content-active");
        $(".resp-tab-content1").removeClass("resp-tab-content-active");
        $(".resp-tab-content4").removeClass("resp-tab-content-active");
        $(".resp-tab-content5").removeClass("resp-tab-content-active");

        $(".resp-tab-content2").hide();
        $(".resp-tab-content1").hide();
        $(".resp-tab-content4").hide();
        $(".resp-tab-content5").hide();
    });
    
    $(".resp-tab-item4").click(function(){
		$(this).addClass("resp-tab-active");
        $(".resp-tab-item2").removeClass("resp-tab-active");
        $(".resp-tab-item3").removeClass("resp-tab-active");
        $(".resp-tab-item1").removeClass("resp-tab-active");
        $(".resp-tab-item5").removeClass("resp-tab-active");
		
		$(".resp-tab-content4").addClass("resp-tab-content-active");
		$(".resp-tab-content4").show();

        $(".resp-tab-content2").removeClass("resp-tab-content-active");
        $(".resp-tab-content3").removeClass("resp-tab-content-active");
        $(".resp-tab-content1").removeClass("resp-tab-content-active");
        $(".resp-tab-content5").removeClass("resp-tab-content-active");

        $(".resp-tab-content2").hide();
        $(".resp-tab-content3").hide();
        $(".resp-tab-content1").hide();
        $(".resp-tab-content5").hide();
    });
    
    $(".resp-tab-item5").click(function(){
		$(this).addClass("resp-tab-active");
        $(".resp-tab-item2").removeClass("resp-tab-active");
        $(".resp-tab-item3").removeClass("resp-tab-active");
        $(".resp-tab-item4").removeClass("resp-tab-active");
        $(".resp-tab-item1").removeClass("resp-tab-active");
		
		$(".resp-tab-content5").addClass("resp-tab-content-active");
		$(".resp-tab-content5").show();

        $(".resp-tab-content2").removeClass("resp-tab-content-active");
        $(".resp-tab-content3").removeClass("resp-tab-content-active");
        $(".resp-tab-content4").removeClass("resp-tab-content-active");
        $(".resp-tab-content1").removeClass("resp-tab-content-active");

        $(".resp-tab-content2").hide();
        $(".resp-tab-content3").hide();
        $(".resp-tab-content4").hide();
        $(".resp-tab-content1").hide();
	});

    


</script>

<script>
    function pricePopProjectname(project) {
        var projname = project;
        $('.pricepopupTit').html(projname);
        $('#enqproject').val(projname);
        $('#price-pop').modal('show');
    }
  


$(document).ready(function(){

  $(".aboutLink1").click(function(){
    $(".viewAboutDiv1").removeClass("displayNone");
  });

  $(".aboutLink2").click(function(){
    $(".viewAboutDiv2").toggleClass("displayNone");
  });

  $(".aboutLink3").click(function(){
    $(".viewAboutDiv3").toggleClass("displayNone");
  });

  $(".aboutLink4").click(function(){
    $(".viewAboutDiv4").toggleClass("displayNone");
  });

  $(".aboutLink5").click(function(){
    $(".viewAboutDiv5").toggleClass("displayNone");
  });

  $(".aboutLink6").click(function(){
    $(".viewAboutDiv6").toggleClass("displayNone");
  });

  $(".aboutLink7").click(function(){
    $(".viewAboutDiv7").toggleClass("displayNone");
  });

  $(".aboutLink8").click(function(){
    $(".viewAboutDiv8").toggleClass("displayNone");
  });

  $(".aboutLink9").click(function(){
    $(".viewAboutDiv9").toggleClass("displayNone");
  });

  $(".aboutLink10").click(function(){
    $(".viewAboutDiv10").toggleClass("displayNone");
  });

  $(".aboutLink11").click(function(){
    $(".viewAboutDiv11").toggleClass("displayNone");
  });

  $(".aboutLink12").click(function(){
    $(".viewAboutDiv12").toggleClass("displayNone");
  });

  $(".aboutLink13").click(function(){
    $(".viewAboutDiv13").toggleClass("displayNone");
  });

  $(".aboutLink14").click(function(){
    $(".viewAboutDiv14").toggleClass("displayNone");
  });

  $(".aboutLink15").click(function(){
    $(".viewAboutDiv15").toggleClass("displayNone");
  });

  $(".aboutLink1").click(function(){
    $(".viewAboutDiv1").toggleClass("displayNone");
  });


});

</script>

</body>
</html>