<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>LGCL High Street | 2BHK 3BHK Flats for Sale in Bangalore</title>
    <meta name="description" content="It’s a rare luxury when you have a home in the heart of Bengaluru, it’s even more of a rarity to still be surrounded by natural greenery. Have the best of both, urban and natural worlds with a home at LGCL High Street, on Old Airport Road, Domlur. Be a proud owner of spacious 2 and 3 bedroom apartments and be at the centre of the happening city."/>
    <link rel="shortcut icon" type="image/x-icon" href="http://lgcl.in/wp-content/uploads/2018/01/lgcl-logo.png">
	<link rel="apple-touch-icon" href="http://lgcl.in/wp-content/uploads/2018/01/lgcl-logo.png"/>


    <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/css/vendor.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/css/main.css">
            
    <!-- fonts -->
    <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
        rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
</head>

<body>

    <?php $all_array = all_arrays(); ?>
    <div class="header-top">
            <p> 
                RERA PROJECT REGISTRATION NO: PRM/KA/RERA/1251/446/PR/171015/000712
            </p>
    </div>
    <!-- banner -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="header-banner header-banner-desktop">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/images/desktop-banner.jpg" />
                </div>
                <div class="header-banner header-banner-mobile">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/images/mobile-banner.jpg" />
                </div>
                <div class="">
                    <div class="carousel-caption">
                        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>realestate/properties/frm_submit','lgcl_highstreet','1','<?php echo SITE_URL?>')">

                            <div class="">
                                <div class="col">
                                    <div class="form-logo">
                                        <a href="#">
                                            <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/images/High-Street-Logo.png" />
                                        </a>
                                    </div>
                                    <h4>Limited Period Offer</h4>
                                    <h4>Luxury Apartments starting @2.17Cr in Old Airport Road Domlur, Bengaluru.</h4>

                                </div>
                                <div class="col">
                                    <div class="group">
                                        <div class="form-group">

                                            <input type="text" class="form-control" id="name1" name="name" placeholder="Name"
                                                onkeyup="chck_valid('name', 'Please enter correct name')"
                                                data-attr="Please enter correct name">
                                            <span class="help-block" id="name_err1"></span>
                                        </div>

                                    </div>
                                    <div class="group">
                                        <div class="form-group">

                                            <input type="email" class="form-control" id="email1" name="email"
                                                placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')"
                                                data-attr="Please enter correct email">
                                            <span class="help-block" id="email_err1"></span>
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode1">
                                            <input type="tel" class="form-control only_numeric phone" id="phone1" name="phone"
                                                pattern="\d*" placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')"
                                                data-attr="Please enter correct Mobile number" >
                                            <span class="help-block" id="phone_err1"> </span>
                                            <span class="help-block" id="CountryCode_err"> </span>
                                        </div>

                                    </div>
                                    <div class="submitbtncontainer">
                                        <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
                                    </div>
                                </div> 
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal -->
    </div>
    <!--//banner -->

                             
    <!-- about -->
    <div class="about" id="about">
        <div class="container"> 
                 
            <div class="col-md-12 text-center">
                <h3>ENTER SEVENTH HEAVEN ON EARTH.</h3><br>  
                <p>
It’s a rare luxury when you have a home in the heart of Bengaluru, it’s even more of a rarity to still be surrounded by natural greenery. Have the best of both, urban and natural worlds with a home at LGCL High Street, on Old Airport Road, Domlur. Be a proud owner of spacious 2 and 3 bedroom apartments and be at the centre of the happening city.</p>
            </div>
            <div class="clearfix"><br><br><br><br> </div>          
             
    </div>
    </div>
    <!-- //about --> 

<!-- projects -->
  <div class="gallery" id="projects">
    <div class="container">
     <h3 class="title">SPECIFICATION</h3>       
     <div class="container">
      <div class="row">
       <h3>STRUCTURE AND INFILL</h3><br>  
        <table class="table table-bordered">    
          <caption style="background-color: #8bb643;"><br></caption>    
        <tbody>
         <tr>
          <td>Structure & infill</td>
         <td>Rcc framed structure with concrete block masonry & plastered</td>       
         </tr>        
        </tbody>
       </table>
	  </div>
    </div>    
    <div class="container">
    <div class="row"><div class="container"><div class="row"><br></div></div>
    <h3>FLOORING / SKIRTING</h3><br>  
     <table class="table table-bordered text-center">    
      <caption style="background-color: #8bb643;"><br></caption>    
      <tbody>
       <tr>
        <td>Living / dining room</td>
        <td>Marble</td>
        <tr>  
        <td>Entrance foyer/lobby inside</td>
        <td>Marble</td>  
        </tr>
        <tr>
        <td>Master bedroom</td>
        <td>Laminate wooden flooring</td> 
        </tr> 
        <tr>
        <td>Other bedrooms</td>
        <td>Vitrified tiles</td>
        </tr> 
        <tr> 
        <td>Study</td>
        <td>Vitrified tiles</td>       
       </tr>        
       <tr>
        <td>Kitchen</td>
        <td>Vitrified tiles</td>
        <tr>  
        <td>Utility</td>
        <td>Ceramic tiles</td>  
        </tr>
        <tr>
        <td>Toilet</td>
        <td>Anti-skid ceramic tiles</td> 
        </tr> 
        <tr>
        <td>Balcony</td>
        <td>Anti-skid ceramic tiles</td>
        </tr> 
        <tr> 
        <td>Deck</td>
        <td>Anti-skid ceramic tiles</td>       
       </tr>        
      </tbody>
     </table>
	</div>
    </div>
    <div class="container">
    <div class="row"><div class="container"><div class="row"><br></div></div>
    <h3>DADO</h3><br>  
     <table class="table table-bordered text-center">    
      <caption style="background-color: #8bb643;"><br></caption>    
      <tbody>
       <tr>
        <td>Toilet</td>
        <td>Ceramic tiles upto 7<sup>'</sup> height</td>
        <tr>  
        <td>Kitchen</td>
        <td>Ceramic tiles for 2<sup>'</sup>0<sup>"</sup> height above kitchen counter shall be supplied</td>  
        </tr>
      </tbody>
     </table>
	</div>
    </div>
    <div class="container">
    <div class="row"><div class="container"><div class="row"><br></div></div>
    <h3>JOINERY</h3><br>  
     <table class="table table-bordered text-center">    
      <caption style="background-color: #8bb643;"><br></caption>    
      <tbody>
       <tr>
        <td>Main door</td>
        <td>Teak wood frame with one side teak shutter</td>
        <tr>  
        <td>Bed room</td>
        <td>Hardwood frame with moulded panel skin shutter</td>  
        </tr>
        <tr>
        <td>Toilet doors</td>
        <td>Hardwood frame with water resistant resin coated shutter</td> 
        </tr> 
        <tr>
        <td>Utility doors</td>
        <td>Aluminium joinery</td>
        </tr> 
        <tr> 
        <td>Balcony doors</td>
        <td>Aluminium joinery</td>       
       </tr>        
       <tr>
        <td>Windows</td>
        <td>Aluminium joinery three track</td>
        <tr>  
        <td>Ventilators</td>
        <td>Aluminium joinery</td>  
        </tr>
      </tbody>
     </table>
	</div>
    </div>
    <div class="container">
    <div class="row"><div class="container"><div class="row"><br></div></div>
    <h3>PAINTING / POLISHING</h3><br>  
     <table class="table table-bordered text-center">    
      <caption style="background-color: #8bb643;"><br></caption>    
      <tbody>
       <tr>
        <td>Ceiling</td>
        <td>Oil bound distemper</td>
        <tr>  
        <td>Internal walls</td>
        <td>Acrylic emulsion</td>  
        </tr>
        <tr>  
        <td>External walls</td>
        <td>Weather proof paint</td>  
        </tr>
        <tr>  
        <td>Main door</td>
        <td>Polish</td>  
        </tr>
        <tr>  
        <td>Internal door</td>
        <td>Synthetic enamel paint</td>  
        </tr>
        <tr>  
        <td>Ms works</td>
        <td>Synthetic enamel paint</td>  
        </tr>
      </tbody>
     </table>
	</div>
    </div>
    <div class="container">
    <div class="row"><div class="container"><div class="row"><br></div></div>
    <h3>ELECTRICAL</h3><br>  
     <table class="table table-bordered text-center">    
      <caption style="background-color: #8bb643;"><br></caption>    
      <tbody>
       <tr>  
        <td>Conduits and wiring</td>
        <td>Concealed in walls and ceiling</td>  
        </tr>
        <tr>  
        <td>Lighting point</td>
        <td>Provision in wall & ceiling</td>  
        </tr>
        <tr>  
        <td>Telephone point</td>
        <td>Provision in all bedroom, living & dining</td>  
        </tr>
        <tr>  
        <td>Tv point</td>
        <td>Provision in all bedroom, living & dining</td>  
        </tr>
        <tr>  
        <td>Ac point</td>
        <td>Provision in all bedroom, living & dining</td>  
        </tr>
        <tr>  
        <td>Aquaguard point</td>
        <td>Provision in kitchen</td>  
        </tr>
        <tr>  
        <td>Chimney point</td>
        <td>Provision in kitchen</td>  
        </tr>
        <tr>  
        <td>Microwave oven point</td>
        <td>Provision in kitchen</td>  
        </tr>
        <tr>  
        <td>Washing machine point</td>
        <td>Provision for one point</td>  
        </tr>
        <tr>  
        <td>Refrigerator point</td>
        <td>Provision in kitchen</td>  
        </tr>
        <tr>  
        <td>Exhaust fan point</td>
        <td></td>  
        </tr>
      </tbody>
     </table>
	</div>
    </div>
    <div class="container">
    <div class="row"><div class="container"><div class="row"><br></div></div>
    <h3>AMENITIES IN TOILETS</h3><br>  
     <table class="table table-bordered text-center">    
      <caption style="background-color: #8bb643;"><br></caption>    
      <tbody>
       <tr>  
        <td>Fixtures</td>
        <td>Cp fixtures with hot and cold water supply in the wash basin and shower area</td>  
        </tr>
        <tr>  
        <td>Sanitaryware</td>
        <td>Wash basin and ewc</td>  
        </tr>              
      </tbody>
     </table>
	</div>
    </div>
    <div class="container">
    <div class="row"><div class="container"><div class="row"><br></div></div>
    <h3>AMENITIES IN KITCHEN</h3><br>  
     <table class="table table-bordered text-center">    
      <caption style="background-color: #8bb643;"><br></caption>    
      <tbody>
       <tr>  
        <td>Fixtures</td>
        <td>Cp fixtures with hot & cold water supply</td>  
        </tr>
        <tr>  
        <td>Sanitaryware</td>
        <td>Ss double bowl sink single drain board shall be handed over</td>  
        </tr>
        <tr>  
        <td>Counter</td>
        <td>20mm thick polished granite slab shall be handed over</td>  
        </tr>              
      </tbody>
     </table>
	</div>
    </div>
    </div>
  </div>
</div>

    <!-- //projects -->

    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">GALLERY</h3>
            <div class="agile_gallery_grids w3-agile demo">

                <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Galleria Residences" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/images/ab5.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/images/ab5.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

    


            </div>
        </div>
    </div>
    <!-- //projects -->
    	<!-- projects -->
	<div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">FLOOR PLAN</h3>
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-6 col-sm-6 col-xs-6 gal-sec">
					<div class="gallery-grid1">
						<a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/images/ground-floar.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/images/ground-floar.jpg" alt=" " class="img-responsive" />							 
						</a>
					</div> 					              
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 gal-sec">
					<div class="gallery-grid1">
						<a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/images/unit-d-2-bhk.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/images/unit-d-2-bhk.jpg" alt=" " class="img-responsive" />						 
						</a>
					</div> 					              
                </div>
                
                <div class="col-md-6 col-sm-6 col-xs-6 gal-sec">
					<div class="gallery-grid1">
						<a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/images/unit-e-3-bhk.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/images/unit-e-3-bhk.jpg" alt=" " class="img-responsive" />					 
						</a>
					</div> 					              
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 gal-sec">
					<div class="gallery-grid1">
						<a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/images/unit-f-3-bhk.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/images/unit-f-3-bhk.jpg" alt=" " class="img-responsive" />							 
						</a>
					</div> 					              
                </div>
					              
                </div>
			</div>
		</div>
	</div>
    <!-- //projects -->
    <!-- projects -->
    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">Master Plan</h3>
            <div class="agile_gallery_grids w3-agile demo">
                <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/images/masterplan.jpg">
                            <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/images/masterplan-1.jpg"
                                alt=" " class="img-responsive" />
                        </a>
                    </div>
                </div>
           




            </div>
        </div>
    </div>
    <!-- //projects -->


    <!-- contact -->
    <div class="address" id="contact">
        <div class="container">
            <h3 class="title">Contact Us</h3>
            <div class="address-row">
            <div class="col-md-12 col-xs-12  wow agile fadeInLeft animated" data-wow-delay=".5s">
                     
                </div>
                <div class="col-md-12 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
                    <div class="address-grid">
                        <!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
                        <div id="location">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15552.663642720714!2d77.6374274!3d12.9612332!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x439b46ca654409b8!2sLGCL+HIGH+STREET!5e0!3m2!1sen!2sin!4v1554124242134!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border: 0px; pointer-events: none;" allowfullscreen=""></iframe>                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--//contact-->
    <footer>
     
        <div class="copy-right-grids">
            <p class="footer-gd">2018 ©LGCL. All Rights Reserved.</p>
        </div>
    </footer>

    <div class="floating-form visiable" id="contact_form">
        <div class="contact-opener">
            Enquire Now
        </div>
        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>realestate/properties/frm_submit','lgcl_highstreet','2','<?php echo SITE_URL?>')">
        
            <div class="">
                <div class="col text-center">
                    <div class="form-logo">
                        <a href="#">
                            <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/images/High-Street-Logo.png" 
                             />
                        </a>
                    </div>
                </div>
                <h4>Limited Period Offer</h4>
                <h4>Luxury Apartments starting @2.17Cr in Old Airport Road Domlur, Bengaluru.</h4>
                <div class="col">
                    <div class="groups">
                        <div class="form-group">

                            <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')"
                                data-attr="Please enter correct name">
                            <span class="help-block" id="name_err2"></span>
                        </div>

                    </div>
                    <div class="groups">
                        <div class="form-group">

                            <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address"
                                onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                            <span class="help-block" id="email_err2"></span>
                        </div>
                        <div class="form-group">
                            <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode2">
                            <input type="tel" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*"
                                placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')"
                                data-attr="Please enter correct Mobile number">
                            <span class="help-block" id="phone_err2"> </span>
                            <span class="help-block" id="CountryCode_err"> </span>
                        </div>

                    </div>
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn2" value="Submit" name="submit">
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="popup-enquiry-form mfp-hide" id="popupForm">
        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>realestate/properties/frm_submit','lgcl_highstreet','3','<?php echo SITE_URL?>')">

            <div class="">
                <div class="col">
                    <div class="form-logo">
                        <a href="#">
                            <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/images/High-Street-Logo.png" />
                        </a>
                    </div>
                    <h4>Limited Period Offer</h4>
                    <h4>Luxury Apartments starting @2.17Cr in Old Airport Road Domlur, Bengaluru.</h4>
                </div>
                <div class="col">
                    <div class="groups">
                        <div class="form-group">

                            <input type="text" class="form-control" id="name3" name="name" placeholder="Name"
                                onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err3"></span>
                        </div>
                        <div class="form-group">

                            <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address"
                                onkeyup="chck_valid('email', 'Please enter correct email')"
                                data-attr="Please enter correct email">
                            <span class="help-block" id="email_err3"></span>
                        </div>
                    </div>
                    <div class="groups">

                        <div class="form-group">
                            <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode3">
                            <input type="tel" class="form-control only_numeric phone" id="phone3" name="phone"
                                pattern="\d*" placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')"
                                data-attr="Please enter correct Mobile number">
                            <span class="help-block" id="phone_err3"> </span>
                            <span class="help-block" id="CountryCode_err"> </span>
                        </div>

                    </div>


                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>
                </div>
            </div>
        </form>
    </div>

    <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
    <input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "
        "); ?>">
    <input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : "
        "); ?>">
    <input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : "
        "); ?>">
    <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : "
        "); ?>">


    <script src="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/js/vendor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/lgcl-highstreet/js/main.js"></script>
    <script src="<?php echo S3_URL?>/site/scripts/properties.js"></script>    
    
</body>

</html>