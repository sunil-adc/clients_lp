<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Villas in South Bangalore | Villas in Bangalore | Best Villas in Banglore</title>
    <meta name="description" content="A place of extreme beauty and delight. Welcome to a paradise that is LGCL New Life.Villas in South Bangalore, Villa Projects in Bangalore South, Villas in South Bangalore, Villa Projects in Bangalore South, Villa Projects in South Bangalore, Villas for Sale in Bangalore South."/>
    <link rel="shortcut icon" type="image/x-icon" href="http://lgcl.in/wp-content/uploads/2018/01/lgcl-logo.png">
	<link rel="apple-touch-icon" href="http://lgcl.in/wp-content/uploads/2018/01/lgcl-logo.png"/>


    <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/css/vendor.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/css/main.css">
    <!-- fonts -->
    <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
        rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
</head>

<body>

    <?php $all_array = all_arrays(); ?>
    <div class="header-top">
            <p> 
                RERA PROJECT REGISTRATION NO: PRM/KA/RERA/1251/308/PR/171015/000303

            </p>
    </div>
    <!-- banner -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="header-banner header-banner-desktop">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/desktop-banner.jpg" />
                </div>
                <div class="header-banner header-banner-mobile">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/mobile-banner.jpg" />
                </div>
                <div class="">
                    <div class="carousel-caption">
                        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>realestate/properties/frm_submit','lgcl_newlife','1','<?php echo SITE_URL?>')">

                            <div class="">
                                <div class="col">
                                    <div class="form-logo">
                                        <a href="#">
                                            <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/logo-blue.png" />
                                        </a>
                                    </div>
                                    <h4>Limited Period Offers</h4>
                                    <h4>Get Villa @2.03Cr Off Sarjapur Road Bengaluru</h4>

                                </div>
                                <div class="col">
                                    <div class="group">
                                        <div class="form-group">

                                            <input type="text" class="form-control" id="name1" name="name" placeholder="Name"
                                                onkeyup="chck_valid('name', 'Please enter correct name')"
                                                data-attr="Please enter correct name">
                                            <span class="help-block" id="name_err1"></span>
                                        </div>

                                    </div>
                                    <div class="group">
                                        <div class="form-group">

                                            <input type="email" class="form-control" id="email1" name="email"
                                                placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')"
                                                data-attr="Please enter correct email">
                                            <span class="help-block" id="email_err1"></span>
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode1">
                                            <input type="tel" class="form-control only_numeric phone" id="phone1" name="phone"
                                                pattern="\d*" placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')"
                                                data-attr="Please enter correct Mobile number" >
                                            <span class="help-block" id="phone_err1"> </span>
                                            <span class="help-block" id="CountryCode_err"> </span>
                                        </div>

                                    </div>


                                    <div class="submitbtncontainer">
                                        <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
                                    </div>
                                </div> 
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal -->
    </div>
    <!--//banner -->

                             
    <!-- about -->
    <div class="about" id="about">
        <div class="container"> 
                 
            <div class="col-md-12 text-center">
                <h3>ENTER SEVENTH HEAVEN ON EARTH.</h3><br>  
                <p>
Inspired by the principles and aesthetics of traditional Balinese architecture, this novel venture promises you your own slice of paradise. 
Located just off sarjapur Road, LGCL New Life is a respite from the raucous, polluted and busy city life with its lush green landscapes,
graceful water bodies and captivating design.</p>
            </div>
            <div class="clearfix"><br><br><br><br> </div>
           
             
    </div>
    </div>
    <!-- //about --> 
   
	<!-- projects -->
	<div class="gallery" id="projects">
     <div class="container">
    <h3 class="title">SPECIFICATION</h3>
    <div class="agile_gallery_grids w3-agile demo">
      <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
        <div class="gallery-grid1"> <a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-1.jpg"> <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-1.jpg" alt=" " class="img-responsive" />
          <div class="p-mask">
            <h4>STRUCTURE</h4>
          </div>
          </a> </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
        <div class="gallery-grid1"> <a title="LUXURY CLUBHOUSE" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-2.jpg"> <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-2.jpg" alt=" " class="img-responsive" />
          <div class="p-mask">
            <h4>FLOORING</h4>
          </div>
          </a> </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
        <div class="gallery-grid1"> <a title="LIBRARY" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-3.jpg"> <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-3.jpg" alt=" " class="img-responsive" />
          <div class="p-mask">
            <h4>WALLS</h4>
          </div>
          </a> </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
        <div class="gallery-grid1"> <a title="MEDITATION ZONE" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-4.jpg"> <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-4.jpg" alt=" " class="img-responsive" />
          <div class="p-mask">
            <h4>WINDOWS & VENTILATORS</h4>
          </div>
          </a> </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
        <div class="gallery-grid1"> <a title="TENNIS COURT" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-5.jpg"> <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-5.jpg" alt=" " class="img-responsive" />
          <div class="p-mask">
            <h4>DOORS</h4>
          </div>
          </a> </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
        <div class="gallery-grid1"> <a title="TABLE TENNIS" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-6.jpg"> <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-6.jpg" alt=" " class="img-responsive" />
          <div class="p-mask">
            <h4>ELECTRICAL</h4>
          </div>
          </a> </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
        <div class="gallery-grid1"> <a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-7.jpg"> <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-7.jpg" alt=" " class="img-responsive" />
          <div class="p-mask">
            <h4>KITCHEN</h4>
          </div>
          </a> </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
        <div class="gallery-grid1"> <a title="LUXURY CLUBHOUSE" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-8.jpg"> <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-8.jpg" alt=" " class="img-responsive" />
          <div class="p-mask">
            <h4>TOILETS</h4>
          </div>
          </a> </div>
      </div>
     
      <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
        <div class="gallery-grid1"> <a title="MEDITATION ZONE" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-10.jpg"> <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-10.jpg" alt=" " class="img-responsive" />
          <div class="p-mask">
            <h4>OTHER AMENITIES</h4>
          </div>
          </a> </div>
      </div>
      <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
        <div class="gallery-grid1"> <a title="TENNIS COURT" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-11.jpg"> <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/s-11.jpg" alt=" " class="img-responsive" />
          <div class="p-mask">
            <h4>CLUB HOUSE</h4>
          </div>
          </a> </div>
      </div>
      
    </div>
  </div>
</div>

    <!-- //projects -->

    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">GALLERY</h3>
            <div class="agile_gallery_grids w3-agile demo">

                <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Galleria Residences" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/ab5.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/ab5.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

    


            </div>
        </div>
    </div>
    <!-- //projects -->
    	<!-- projects -->
	<div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">FLOOR PLAN</h3>
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-6 col-sm-6 col-xs-6 gal-sec">
					<div class="gallery-grid1">
						<a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/floorplan-1.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/floorplan-1.jpg" alt=" " class="img-responsive" />							 
						</a>
					</div> 					              
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 gal-sec">
					<div class="gallery-grid1">
						<a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/floorplan-2.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/floorplan-2.jpg" alt=" " class="img-responsive" />							 
						</a>
					</div> 					              
                </div>
                
                <div class="col-md-6 col-sm-6 col-xs-6 gal-sec">
					<div class="gallery-grid1">
						<a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/floorplan-3.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/floorplan-3.jpg" alt=" " class="img-responsive" />							 
						</a>
					</div> 					              
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 gal-sec">
					<div class="gallery-grid1">
						<a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/floorplan-4.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/floorplan-4.jpg" alt=" " class="img-responsive" />							 
						</a>
					</div> 					              
                </div>

                <div class="col-md-6 col-sm-6 col-xs-6 gal-sec">
					<div class="gallery-grid1">
						<a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/floorplan-5.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/floorplan-5.jpg" alt=" " class="img-responsive" />							 
						</a>
					</div> 					              
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 gal-sec">
					<div class="gallery-grid1">
						<a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/floorplan-6.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/floorplan-6.jpg" alt=" " class="img-responsive" />							 
						</a>
					</div> 					              
                </div>
			</div>
		</div>
	</div>
    <!-- //projects -->
    <!-- projects -->
    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">Master Plan</h3>
            <div class="agile_gallery_grids w3-agile demo">
                <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/masterplan.jpg">
                            <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/masterplan.jpg"
                                alt=" " class="img-responsive" />
                        </a>
                    </div>
                </div>
           




            </div>
        </div>
    </div>
    <!-- //projects -->


    <!-- contact -->
    <div class="address" id="contact">
        <div class="container">
            <h3 class="title">Contact Us</h3>
            <div class="address-row">
            <div class="col-md-12 col-xs-12  wow agile fadeInLeft animated" data-wow-delay=".5s">
                     
                </div>
                <div class="col-md-12 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
                    <div class="address-grid">
                        <!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
                        <div id="location">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3889.356029513128!2d77.68296331419991!3d12.88481322026439!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae13324d2c8fd3%3A0xe2d0de700fa98d73!2sLGCL+New+Life!5e0!3m2!1sen!2sin!4v1515610458346" style="border:0;width: 100%;height: 350px;"  allowfullscreen></iframe>                                
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
    <!--//contact-->
    <footer>
     
        <div class="copy-right-grids">
            <p class="footer-gd">2018 ©LGCL. All Rights Reserved.</p>
        </div>
    </footer>

    <div class="floating-form visiable" id="contact_form">
        <div class="contact-opener">
            Enquire Now
        </div>
        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>realestate/properties/frm_submit','lgcl_newlife','2','<?php echo SITE_URL?>')">
        
            <div class="">
                <div class="col text-center">
                    <div class="form-logo">
                        <a href="#">
                            <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/logo-blue.png" 
                             />
                        </a>
                    </div>
                </div>
                <h4>Limited Period Offer</h4>
                <h4>Get Villa @2.03Cr Off Sarjapur Road Bengaluru</h4>
                <div class="col">
                    <div class="groups">
                        <div class="form-group">

                            <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')"
                                data-attr="Please enter correct name">
                            <span class="help-block" id="name_err2"></span>
                        </div>

                    </div>
                    <div class="groups">
                        <div class="form-group">

                            <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address"
                                onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                            <span class="help-block" id="email_err2"></span>
                        </div>
                        <div class="form-group">
                            <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode2">
                            <input type="tel" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*"
                                placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')"
                                data-attr="Please enter correct Mobile number">
                            <span class="help-block" id="phone_err2"> </span>
                            <span class="help-block" id="CountryCode_err"> </span>
                        </div>

                    </div>
                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn2" value="Submit" name="submit">
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="popup-enquiry-form mfp-hide" id="popupForm">
        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>realestate/properties/frm_submit','lgcl_newlife','3','<?php echo SITE_URL?>')">

            <div class="">
                <div class="col">
                    <div class="form-logo">
                        <a href="#">
                            <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/images/logo-blue.png" />
                        </a>
                    </div>
                    <h4>Limited Period Offer</h4>
                    <h4>Get Villa @2.03Cr Off Sarjapur Road Bengaluru</h4>
                </div>
                <div class="col">
                    <div class="groups">
                        <div class="form-group">

                            <input type="text" class="form-control" id="name3" name="name" placeholder="Name"
                                onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err3"></span>
                        </div>
                        <div class="form-group">

                            <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address"
                                onkeyup="chck_valid('email', 'Please enter correct email')"
                                data-attr="Please enter correct email">
                            <span class="help-block" id="email_err3"></span>
                        </div>
                    </div>
                    <div class="groups">

                        <div class="form-group">
                            <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode3">
                            <input type="tel" class="form-control only_numeric phone" id="phone3" name="phone"
                                pattern="\d*" placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')"
                                data-attr="Please enter correct Mobile number">
                            <span class="help-block" id="phone_err3"> </span>
                            <span class="help-block" id="CountryCode_err"> </span>
                        </div>

                    </div>


                    <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>
                </div>
            </div>
        </form>
    </div>

    <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
    <input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "
        "); ?>">
    <input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : "
        "); ?>">
    <input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : "
        "); ?>">
    <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : "
        "); ?>">


    <script src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/js/vendor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/lgcl-newlife/js/main.js"></script>
    <script src="<?php echo S3_URL?>/site/scripts/properties.js"></script>
</body>

</html>