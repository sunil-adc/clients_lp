<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Villas for Sale in Bangalore South | Villa Projects in South Bangalore</title>
    <meta name="description" content="Escape to a rustic & elegant Spanish retreat right in the hub of Bangalore"/>
    <link rel="shortcut icon" type="image/x-icon" href="http://lgcl.in/wp-content/uploads/2018/01/lgcl-logo.png">
	<link rel="apple-touch-icon" href="http://lgcl.in/wp-content/uploads/2018/01/lgcl-logo.png"/>


    <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-pueblo/css/vendor.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-pueblo/css/main.css">
    <!-- fonts -->
    <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
        rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
</head>

<body>

    <?php $all_array = all_arrays(); ?>
    <div class="header-top">
            <p> 
               RERA PROJECT REGISTRATION NO: PRM/KA/RERA/1251/310/PR/171014/000286
            </p>
    </div>
    <!-- banner -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="header-banner header-banner-desktop">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-pueblo/images/desktop-banner.jpg" />
                </div>
                <div class="header-banner header-banner-mobile">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-pueblo/images/mobile-banner.jpg" />
                </div>
                <div class="">
                    <div class="carousel-caption">
                        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>realestate/properties/frm_submit','lgcl_pueblo','1')">

                            <div class="">
                                <div class="col">
                                    <div class="form-logo">
                                        <a href="#">
                                            <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-pueblo/images/pueblo-logo.png" />
                                        </a>
                                    </div>
                                    <h4>Thank you for expressing interest on our Properties Our expert will get in touch with you shortly.</h4>

                                </div>
                                
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal -->
    </div>
    <!--//banner -->

                             
    <!-- about -->
    <div class="about" id="about">
        <div class="container"> 
                 
            <div class="col-md-12 text-center">
                <h3>ENTERSEVENTH HEAVEN ON EARTH.</h3><br>  
                <p>
 <strong style="font-size: 48px; line-height: 1px;">N</strong>ow you can dwell in a serene 4 acre village environment while the business district is
just next door. Pueblo is inspired by exotic Spanish settlements of antiquity where large
nucleated villages housed communities in adobe mud and other local materials. With the
ambience of a Spanish village in the middle of the metro at Nagnathapura, off Sarjapur Road, the
59 villas at Pueblo are a captivating mix of architecture and lifestyle. These Spanish-inspired
villas offer the perfect upscale community for cosmopolitan living for world travellers. Live at the
hub of this ever-changing city and bustling metro while escaping to your own village retreat.</p>
            </div>
            <div class="clearfix"><br><br><br><br> </div>
           
             
    </div>
    </div>
    <!-- //about --> 
   
	<!-- projects -->
  <div class="gallery" id="projects">
    <div class="container">
     <h3 class="title">SPECIFICATION</h3>       
     <div class="container">
      <div class="row">
       <h3>STRUCTURE AND INFILL</h3><br>  
        <table class="table table-bordered">    
          <caption style="background-color: #8bb643;"><br></caption>    
        <tbody>
         <tr>
          <td>Structure & infill</td>
         <td>Rcc framed structure with concrete block masonry & plastered</td>       
         </tr>        
        </tbody>
       </table>
	  </div>
    </div>    
    <div class="container">
    <div class="row"><div class="container"><div class="row"><br></div></div>
    <h3>FLOORING / SKIRTING</h3><br>  
     <table class="table table-bordered text-center">    
      <caption style="background-color: #8bb643;"><br></caption>    
      <tbody>
       <tr>
        <td>Living / dining room</td>
        <td>Marble</td>
        <tr>  
        <td>Lift lobby</td>
        <td>Vitrified tiles</td>  
        </tr>
        <tr>
        <td>Master bedroom</td>
        <td>Laminate wooden flooring</td> 
        </tr> 
        <tr>
        <td>Other bedrooms</td>
        <td>Vitrified tiles</td>
        </tr> 
        <tr> 
        <td>Study</td>
        <td>Vitrified tiles</td>       
       </tr>        
       <tr>
        <td>Kitchen</td>
        <td>Vitrified tiles</td>
        <tr>  
        <td>Utility</td>
        <td>Ceramic tiles</td>  
        </tr>
        <tr>
        <td>Toilet</td>
        <td>Anti-skid ceramic tiles</td> 
        </tr> 
        <tr>
        <td>Balcony</td>
        <td>Anti-skid ceramic tiles</td>
        </tr> 
        <tr> 
        <td>Deck</td>
        <td>Anti-skid ceramic tiles</td>       
       </tr>        
      </tbody>
     </table>
	</div>
    </div>
    <div class="container">
    <div class="row"><div class="container"><div class="row"><br></div></div>
    <h3>DADO</h3><br>  
     <table class="table table-bordered text-center">    
      <caption style="background-color: #8bb643;"><br></caption>    
      <tbody>
       <tr>
        <td>Toilet</td>
        <td>Ceramic tiles upto 7<sup>'</sup> height</td>
        <tr>  
        <td>Kitchen</td>
        <td>Ceramic tiles for 2<sup>'</sup>0<sup>"</sup> height above kitchen counter shall be supplied</td>  
        </tr>
      </tbody>
     </table>
	</div>
    </div>
    <div class="container">
    <div class="row"><div class="container"><div class="row"><br></div></div>
    <h3>JOINERY</h3><br>  
     <table class="table table-bordered text-center">    
      <caption style="background-color: #8bb643;"><br></caption>    
      <tbody>
       <tr>
        <td>Main door</td>
        <td>Teak wood frame with one side teak shutter</td>
        <tr>  
        <td>Bedroom / kitchen door</td>
        <td>Hardwood frame with moulded panel skin shutter</td>  
        </tr>
        <tr>
        <td>Toilet doors</td>
        <td>Hardwood frame with water resistant resin coated shutter</td> 
        </tr> 
        <tr>
        <td>Utility doors</td>
        <td>Hardwood frame with water resistant resin coated shutter</td>
        </tr> 
        <tr> 
        <td>Balcony doors</td>
        <td>Aluminium joinery</td>       
       </tr>        
       <tr>
        <td>Windows</td>
        <td>Aluminium joinery</td>
        <tr>  
        <td>Ventilators</td>
        <td>Aluminium joinery</td>  
        </tr>
      </tbody>
     </table>
	</div>
    </div>
    <div class="container">
    <div class="row"><div class="container"><div class="row"><br></div></div>
    <h3>PAINTING / POLISHING</h3><br>  
     <table class="table table-bordered text-center">    
      <caption style="background-color: #8bb643;"><br></caption>    
      <tbody>
       <tr>
        <td>Ceiling</td>
        <td>Oil bound distemper</td>
        <tr>  
        <td>Internal walls</td>
        <td>Acrylic emulsion</td>  
        </tr>
        <tr>  
        <td>External walls</td>
        <td>Weather proof paint</td>  
        </tr>
        <tr>  
        <td>Main door</td>
        <td>Polish</td>  
        </tr>
        <tr>  
        <td>Internal door</td>
        <td>Synthetic enamel paint</td>  
        </tr>
        <tr>  
        <td>Ms works</td>
        <td>Synthetic enamel paint</td>  
        </tr>
      </tbody>
     </table>
	</div>
    </div>
    <div class="container">
    <div class="row"><div class="container"><div class="row"><br></div></div>
    <h3>ELECTRICAL</h3><br>  
     <table class="table table-bordered text-center">    
      <caption style="background-color: #8bb643;"><br></caption>    
      <tbody>
       <tr>  
        <td>Conduits and wiring</td>
        <td>Concealed in walls and ceiling</td>  
        </tr>
        <tr>  
        <td>Lighting point</td>
        <td>Provision in wall & ceiling</td>  
        </tr>
        <tr>  
        <td>Telephone point</td>
        <td>Provision in all bedroom, living & dining</td>  
        </tr>
        <tr>  
        <td>Tv point</td>
        <td>Provision in all bedroom, living & dining</td>  
        </tr>
        <tr>  
        <td>Ac point</td>
        <td>Provision in all bedroom, living & dining</td>  
        </tr>
        <tr>  
        <td>Internet point</td>
        <td>One point</td>  
        </tr>
        <tr>  
        <td>Aquaguard point</td>
        <td>Provision in kitchen</td>  
        </tr>
        <tr>  
        <td>Chimney point</td>
        <td>Provision in kitchen</td>  
        </tr>
         <tr>  
        <td>Hob point</td>
        <td>Provision in kitchen</td>  
        </tr>
        <tr>  
        <td>Microwave oven point</td>
        <td>Provision in kitchen</td>  
        </tr>
        <tr>  
        <td>Washing machine point</td>
        <td>Provision in utility</td>  
        </tr>
        <tr>  
        <td>Refrigerator point</td>
        <td>Provision in kitchen</td>  
        </tr>
        <tr>  
        <td>Exhaust fan point</td>
        <td>Provision in all toilets</td>  
        </tr>
      </tbody>
     </table>
	</div>
    </div>
    <div class="container">
    <div class="row"><div class="container"><div class="row"><br></div></div>
    <h3>AMENITIES IN TOILETS</h3><br>  
     <table class="table table-bordered text-center">    
      <caption style="background-color: #8bb643;"><br></caption>    
      <tbody>
       <tr>  
        <td>Fixtures</td>
        <td>Cp fixtures with hot and cold water supply in the wash basin and shower area</td>  
        </tr>
        <tr>  
        <td>Sanitaryware</td>
        <td>Wash basin and ewc</td>  
        </tr>              
      </tbody>
     </table>
	</div>
    </div>
    <div class="container">
    <div class="row"><div class="container"><div class="row"><br></div></div>
    <h3>AMENITIES IN KITCHEN</h3><br>  
     <table class="table table-bordered text-center">    
      <caption style="background-color: #8bb643;"><br></caption>    
      <tbody>
       <tr>  
        <td>Fixtures</td>
        <td>Cp fixtures with hot & cold water supply</td>  
        </tr>
        <tr>  
        <td>Sanitaryware</td>
        <td>Ss double bowl sink single drain board shall be handed over</td>  
        </tr>
        <tr>  
        <td>Counter</td>
        <td>20mm thick polished granite slab shall be handed over</td>  
        </tr>              
      </tbody>
     </table>
	</div>
    </div>
    </div>
  </div>
</div>

    <!-- //projects -->
    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">GALLERY</h3>
            <div class="agile_gallery_grids w3-agile demo">

                <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Galleria Residences" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-pueblo/images/ab5.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/lgcl-pueblo/images/ab5.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //projects -->
    <!-- projects -->
	<div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">Floor Plan</h3>
            <div class="agile_gallery_grids w3-agile demo">
                <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-pueblo/images/floorplan.jpg">
                            <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/lgcl-pueblo/images/floorplan.jpg" alt=" " class="img-responsive" />
                        </a>
                    </div>
                </div> 
            </div>
        </div>
    </div>
    <!-- //projects -->
      <!-- projects -->
	<div class="gallery" id="projects">
        <div class="container">           
            <div class="agile_gallery_grids w3-agile demo">
                <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-pueblo/images/floor-plan-type-q.jpg">
                            <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/lgcl-pueblo/images/floor-plan-type-q.jpg" alt=" " class="img-responsive" />
                        </a>
                    </div>
                </div> 
            </div>
        </div>
    </div>
    <!-- //projects -->
    <!-- projects -->
    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">Master Plan</h3>
            <div class="agile_gallery_grids w3-agile demo">
                <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/lgcl-pueblo/images/master-plan.jpg">
                            <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/lgcl-pueblo/images/master-plan.jpg" alt=" " class="img-responsive" />
                        </a>
                    </div>
                </div> 
            </div>
        </div>
    </div>
    <!-- //projects -->
    <!-- contact -->
    <div class="address" id="contact">
        <div class="container">
            <h3 class="title">Contact Us</h3>
            <div class="address-row">
            <div class="col-md-12 col-xs-12  wow agile fadeInLeft animated" data-wow-delay=".5s">
                     
                </div>
                <div class="col-md-12 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
                    <div class="address-grid">
                        <!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
                        <div id="location">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7779.303040707259!2d77.666977!3d12.865770000000001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4632c1a09867ba8c!2sLGCL+Pueblo!5e0!3m2!1sen!2sin!4v1515608616064" style="border:0;width: 100%;height: 350px;" frameborder="0" style="border: 0px; pointer-events: none;" allowfullscreen=""></iframe>                               
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
    <!--//contact-->
    <footer>
    <div id="pi">
       <?php
           $vr = $this->session->userdata('user_id');
           $ut = $this->session->userdata('utm_source');
           if( isset($vr) &&  $vr > 0 && $ut != ""){
               echo $this->lead_check->set_pixel($vr, REALESTATE_USER);
               
           }
       ?>
      </div> 

    <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
    <input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : "
        "); ?>">
    <input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : "
        "); ?>">
    <input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : "
        "); ?>">
    <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : "
        "); ?>">


    <script src="<?php echo S3_URL?>/site/realestate-assets/lgcl-pueblo/js/vendor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/lgcl-pueblo/js/main.js"></script>
    <script src="<?php echo S3_URL?>/site/scripts/properties.js"></script>
</body>

</html>