<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title> Evita - 2, 2.5 & 3 BHK APARTMENTS Starting @ ₹83* LAKHS ONWARDS.. </title>
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/evita/images/favicon.jpg" type="image/x-icon" />


	  <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/evita/css/vendor.css">
	  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/evita/css/main.css">    
      <!-- fonts -->
      <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
            rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
   </head>
   <body>

 <?php $all_array = all_arrays(); ?>      
        <div class="header-top">
            <!-- <div class="h-left">
				<a href="index.html" class="logo" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
					<img src="<?php echo S3_URL?>/site/realestate-assets/evita/images/p1.png" alt="" style="height:50px">
				</a>
            </div> -->
            
            <p>
                <i class="fa fa-info-circle" aria-hidden="true"></i>
                 Evita - 2, 2.5 & 3 BHK APARTMENTS Starting @ ₹83* LAKHS ONWARDS..
            </p>

        </div>
	<!-- banner -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="">
					<div class="carousel-caption">
                    <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="hiranandani_jsfrm('<?php echo SITE_URL?>realestate/hiranandani/submit_frm','1','evita')">
                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/evita/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Evita - 2, 2.5 & 3 BHK APARTMENTS <br>Starting @ ₹83* LAKHS </h4>
                            
                            </div> 
                            <div class="col">   
                    <div class="group">
                        <div class="form-group">
                       
                            <input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err1"></span>
                        </div>
              
                    </div>  
                    <div class="group">
                    <div class="form-group">
                   
                    <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                    <span class="help-block" id="email_err1" ></span>
                    </div>
                    <div class="form-group">
					<input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode1">
                    <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                    <span class="help-block" id="phone_err1"> </span>
				    <span class="help-block" id="CountryCode_err"> </span>
                    </div>
                    
                    </div>      
                
                    <!--<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
					<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
					<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">-->
                    <div class="submitbtncontainer">
                    <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
                    </div>
                    </div>
                    </div>
                </form>
					</div>
				</div>
			</div>
		</div>

		<!-- The Modal -->
	</div>
    <!--//banner -->      
	<!-- about -->
	<div class="about" id="about">
		<div class="container marginBottom100">
			<div class="col-md-12 about-left">
             <div class="col-md-6 col-xs-12 aboutimg-w3l aboutimg-w3l2">
                <iframe width="100%" height="315" src="https://www.youtube.com/embed/pXctSqI37Hg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
				<div class="clearfix"> </div>
			</div>
			<div class="col-md-6 about_right" >
				<h3>ABOUT</h3>
				
				<p> Evita stands 18 storeyed tall and offers 2, 2.5 and 3 bed residences. </p>
                <p> Upcoming metro station is just 5 mins drive away from our project. </p>
                <p> All the Kitchens can be converted into Open Kitchen </p>         
                <p> Every apartment has 2 large balconies </p>    
                <p> All 3 Bed Apartments have a lake view </p>
                <p> No 2 towers or balconies overlook into each other </p>
                <p> FTTH provided to all apartments </p>

			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
    <!-- //about -->



    <!-- Homecoming -->

    <section class="sec-configuration lazyImg is-loaded marginBottom100" id="sec-configuration" style="background-image: url(&quot;<?php echo S3_URL?>/site/realestate-assets/evita/images/configuration.png?=v1.1&quot;);" data-image-full="<?php echo S3_URL?>/site/realestate-assets/evita/images/configuration.png?=v1.1">
			<div class="container">
				<div class="configuration-para">
					<h4 class="title2 has-animation" style="opacity: 1; transform: translate3d(0px, 0px, 0px);">THE HOME COMING</h4>
					<p class="has-animation" style="opacity: 1; transform: translate3d(0px, 0px, 0px);">It is rightly said “home is where the heart is” and the heart is where your loved ones and friends are. Evita’s architectural brilliance and vast open spaces guarantee you warm </p>
				</div>
				<div class="config-wraper has-animation" style="opacity: 1; transform: translate3d(0px, 0px, 0px);">
					<div class="config-box has-animation" data-delay="50" style="opacity: 1; transform: translate3d(0px, 0px, 0px);">
						<h4>2 BHK</h4>
						<a href="javascript:void(0);" class="hoverBtn eqClick hover-target pricingTable-signup demoButtons" >
							<div class="left-c"></div>
							<span>Click for floor plan</span>
							
						</a>
					</div>
          <div class="config-box has-animation" data-delay="50" style="opacity: 1; transform: translate3d(0px, 0px, 0px);">
						<h4>2.5 BHK</h4>
						<a href="javascript:void(0);" class="hoverBtn eqClick pricingTable-signup hover-target demoButtons" >
							<div class="left-c"></div>
							<span>Click for floor plan</span>
							
						</a>
					</div>
					<div class="config-box has-animation" data-delay="120" style="opacity: 1; transform: translate3d(0px, 0px, 0px);">
						<h4>3 BHK</h4>
						<a href="javascript:void(0);" class="hoverBtn eqClick pricingTable-signup hover-target demoButtons" >
							<div class="left-c"></div>
							<span>Click for floor plan</span>
							
						</a>
					</div>
				</div>
			</div>
		</section>
    
    <!-- //Homecoming -->






   
    <!-- projects -->
    
	<div class="gallery" id="projects">
		<div class="container">
            <h3 class="title"> GALLERY & AMENITIES GALORE </h3>
            <br>
            <h4 class="textAlignCenter">
                 A host of amenities for every age group along with unmatched landscaping that is synonymous with the brand.
            </h4>
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-6 col-sm-12 col-xs-12 gal-sec">
					<div class="gallery-grid1">
                        <a title="evita" href="<?php echo S3_URL?>/site/realestate-assets/evita/images/gallery15.png">
                            <div class="stack twisted">
                                <img  src="<?php echo S3_URL?>/site/realestate-assets/evita/images/gallery15.png" alt=" " class="img-responsive" />
                                
                            </div>
							
						</a>
					</div>
                </div>
                
            
                
                <div class="col-md-6 col-sm-12 col-xs-12 gal-sec">
					<div class="gallery-grid1">
                        <a title="evita" href="<?php echo S3_URL?>/site/realestate-assets/evita/images/gallery16.png">
                            <div class="stack twisted">
                                <img  src="<?php echo S3_URL?>/site/realestate-assets/evita/images/gallery16.png" alt=" " class="img-responsive" />
                                
                            </div>
							
						</a>
					</div>
				</div>
			
			</div>
		</div>
	</div>

    <!-- projects -->
    <!-- projects -->


	<div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">AMENITIES</h3>
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="GYMNASIUM" href="<?php echo S3_URL?>/site/realestate-assets/evita/images/gallery05.png">
							<img src="<?php echo S3_URL?>/site/realestate-assets/evita/images/gallery05.png" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>GYMNASIUM</h4>
							</div>
						</a>
                    </div>
        
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
						<a title="SPORTS FACILITIES" href="<?php echo S3_URL?>/site/realestate-assets/evita/images/gallery06.png">
							<img src="<?php echo S3_URL?>/site/realestate-assets/evita/images/gallery06.png" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>SPORTS FACILITIES</h4>
							</div>
						</a>
                    </div>              
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
			
                    <div class="gallery-grid1">
						<a title="SWIMMING POOL" href="<?php echo S3_URL?>/site/realestate-assets/evita/images/gallery07.png">
							<img src="<?php echo S3_URL?>/site/realestate-assets/evita/images/gallery07.png" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>SHOPPING MALL</h4>
							</div>
						</a>
					</div>                  
				</div>
			</div>
		</div>
	</div>
    <!-- //projects -->
	<!-- projects -->

    <!-- //projects -->      
 	<!-- projects -->
	 <!-- <div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">Plan</h3>
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/evita/images/masterplan.jpg">
							<img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/evita/images/masterplan.jpg" alt=" " class="img-responsive" />					
						</a>
					</div>			
				</div>
			</div>
		</div>
	</div> -->
	<!-- //projects -->	


    	<!-- contact -->
	<div class="address" id="contact">
		<div class="container">
			<h3 class="title">Location Map</h3>
			<div class="address-row" style="margin-top: 50px;">
                
				<div class="col-md-12 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
					<div class="address-grid" >
						<!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
						<div id="location">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3890.757139759449!2d77.620802!3d12.79427!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae6ac4118aa827%3A0xd0612b84eac47d4!2sBannerghatta+Main+Rd%2C+Karnataka!5e0!3m2!1sen!2sin!4v1561028568880!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                        
                        </div>
					</div>
				</div>

			
			</div>
		</div>
	</div>
	<!--//contact-->
    <footer class="footerStyles">
        
        
        

        <div class="container disclaimer">

            <div class="col-md-6 col-xs-12 address-right salesAdress" >
                <div class="address-info wow fadeInRight animated textAlignCenter" data-wow-delay=".5s">
                    
                    <h1>Sales Office</h1>
                    <br>
                   
                    <p style="font-size: 15px;">
                        Bannerghatta Office, Begur Village, Next to Hulimavu Lake, Akshay Nagar Begur Hobli, Off Bannerghatta Road, Taluk, Dommasandra, Bengaluru, Karnataka 560068
                    </p>
                    <br>
                           
                    <p>Project RERA No.: PRM/KA/RERA/1251/310/PR/190222/002441 also available on website</p>
                    <br>
                    
                    <p>website https://rera.karnataka.gov.in/</p>
                </div>
            </div>

            <div class="col-md-12 col-xs-12 address-right textAlignCenter">
                <p class="textAlignCenter">
                    <small>
                    Privacy Policy | Disclaimer: The above mentioned price is excluding other charges and statutory payments. The images shown are for Illustration purposes only and may not be an exact representation of the product. The layout details, amenities and facilities mentioned or shown are subject to changes or relocation within the composite development and are subject to modification, amendment or changes, without any prior notice, at the sole discretion of the Developer. *Conditions apply. By using or accessing the website, you agree to the Disclaimer without any qualification or limitation. All the information including brochures, images, designs, specifications, project details, plans, marketing collaterals and other information that are available on this website are solely for informational purpose only & are indicative in nature and the visitor has not relied on this information for making any booking/purchase in any project of the Company. Visitors are therefore requested to directly verify all details and aspects of any proposed booking/acquisition of units/premises, directly with our authorised representatives. The company under no circumstance will be liable for any expense, loss or damage including, without limitation, indirect or consequential loss or damage, or any expense, loss or damage whatsoever arising from use, or loss of use, of data, arising out of or in connection with the use of this website. We thank you for your patience and understanding.”.
                    </small>    
                </p>

                <br>
                
                <br>

                <img src="<?php echo S3_URL?>/site/realestate-assets/evita/images/p1.png" alt="" class="pLogo has-animation" style="opacity: 1; transform: translate3d(0px, 0px, 0px);">
                
                <br>
                
                <br>

                <p class="textAlignCenter">
                    <small>
                    Disclaimer 
                    "We collect information from you when you register on our site or fill out a form. When filling out a form on our site, for any of the above-mentioned reasons, you may be asked to enter your: name, e-mail address and phone number. You may, however, visit our site anonymously. Any of the information we collect from you is to personalize your experience, to improve our website and to improve customer service." The advertiser does not share the information with any third parties 
                    </small>    
                </p>
            
            </div>
           
        </div>    

		<div class="copy-right-grids">
			<p class="footer-gd">© 2000-2019 House Of Hiranandani. All Rights Reserved | Maintained by <a target="_blank" href="http://www.adcanopus.com/">Adcanopus</a></p>
		</div>
	</footer>    
	<div class="floating-form visiable" id="contact_form">
	<div class="contact-opener">Enquire Now</div>
    <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="hiranandani_jsfrm('<?php echo SITE_URL?>realestate/hiranandani/submit_frm','2','evita')">

    <div class="">       
                    <div class="col text-center">
                             <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/evita/images/logo-small.png" />
                                </a> 
                            </div>
                            <h4>Evita - 2, 2.5 & 3 BHK APARTMENTS <br>Starting @ ₹83* LAKHS </h4> 
                            </div> 
                            <div class="col">     
                    <div class="groups">
                        <div class="form-group">
                       
                            <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err2"></span>
                        </div>
              
                    </div>  
                    <div class="groups">
                    <div class="form-group">
                   
                    <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                    <span class="help-block" id="email_err2" ></span>
                    </div>
                    <div class="form-group">
					<input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode2">
                    <input type="text" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                    <span class="help-block" id="phone_err2"> </span>
				    <span class="help-block" id="CountryCode_err"> </span>
                    </div>
                 
                    </div>      
                
                
                    <div class="submitbtncontainer">
                    <input type="submit" id="frm-sbmtbtn2" value="Submit" name="submit">
                    </div>
                    </div>
                    </div>
                </form>	
				<div>
    <div class="popup-enquiry-form mfp-hide" id="popupForm">
    <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="hiranandani_jsfrm('<?php echo SITE_URL?>realestate/hiranandani/submit_frm','3','evita')">
    
     <div class="">       
                    <div class="col">
                             <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/evita/images/logo-small.png" />
                                </a> 
                            </div>
                            <h4>Evita - 2, 2.5 & 3 BHK APARTMENTS <br>Starting @ ₹83* LAKHS </h4>
                            </div> 
                            <div class="col">
                    <div class="groups">
                        <div class="form-group">
                       
                            <input type="text" class="form-control" id="name3" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err3"></span>
                        </div>
						<div class="form-group">
                   
				   <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
				   <span class="help-block" id="email_err3" ></span>
				   </div>
                    </div>  
                    <div class="groups">
                  
                    <div class="form-group">
					<input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode3">
                    <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                    <span class="help-block" id="phone_err3"> </span>
				    <span class="help-block" id="CountryCode_err"> </span>
                    </div>
         
                    </div>      
                
                    
                    <div class="submitbtncontainer">
                    <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>
                    </div>
                    </div>
                </form>

								 </div>

	<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
	<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
	<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
	<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
	<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
	<input type="hidden" id="cid" name="cid" value="hoh-bannerghatta-adcanopus">
	
	<script src="<?php echo S3_URL?>/site/realestate-assets/evita/js/vendor.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>  
    <script src="<?php echo S3_URL?>/site/realestate-assets/evita/js/main.js"></script>
	<script src="<?php echo S3_URL?>/site/scripts/hiranandani.js"></script>
	
   </body>
</html>