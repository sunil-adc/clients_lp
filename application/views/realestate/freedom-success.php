<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">

      
	  <!-- favicon -->
		<link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/freedom/img/favicon.png" />
		<link rel="icon" type="image/ico" href="<?php echo S3_URL?>/site/realestate-assets/freedom/img/favicon.png" />
		<title>Affordable Flats in Chennai | Freedom by Provident, Pudupakkam</title>
		<meta name="description" content="Freedom by Provident has affordable homes located in Pudupakkam, in proximity to the IT corridor of Chennai at OMR are equipped with premium amenities">
      <!--core-css-->
      <link href="<?php echo S3_URL?>/site/realestate-assets/freedom/css/reset.css" rel="stylesheet" />
      <link href="<?php echo S3_URL?>/site/realestate-assets/freedom/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo S3_URL?>/site/realestate-assets/freedom/css/scrolling-nav.css" rel="stylesheet">
      <link href="<?php echo S3_URL?>/site/realestate-assets/freedom/css/style.css" rel="stylesheet" />
      <link href="<?php echo S3_URL?>/site/realestate-assets/freedom/css/intlTelInput.css" rel="stylesheet" />
    
      <!--font-google-->
      <link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,500,500i,600,600i,700,700i,800&amp;subset=cyrillic,cyrillic-ext,latin-ext,vietnamese" rel="stylesheet">
   </head>
   <body id="page-top">
   <?php $all_array = all_arrays(); ?>
      <!--model-1-->
      <div class="modal fade" id="myModal" role="dialog">
         <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title">Privacy Policy</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                  <p>Provident Housing Ltd. (as used herein, the name "Provident Housing" includes Provident Housing Ltd. And all companies wholly or partially owned by it) makes every effort to treat your personal information as safely and securely as reasonably possible. Please read the following privacy policy to better understand how your personal information may be collected and used as you access various areas of our website. As described below, your information may be collected by Provident Housing, disclosed to third parties, and used by third parties and Provident Housing. This Privacy Policy describes the information we collect from you and how we may use that information. Some of the reasons why we collect your personal information include:</p>
                  <ul>
                     <li>To register you so that you access certain parts of the Provident Housing site;</li>
                     <li>To register you so that you may subscribe to our newsletters;</li>
                     <li>So that you may participate in discussion forums on our site; and</li>
                     <li>To provide you with a more personalized and meaningful experience on the Provident Housing site.</li>
                  </ul>
                  <p>We occasionally employ other companies and individuals to perform functions on our behalf. For example, other companies and individuals may fulfill your orders from us, send you postal mail and e-mail, remove repetitive information from our customer lists, analyze our data, provide us with marketing assistance, and process credit card payments. These third party service providers have access to personal information needed to perform their functions, but may not use it for other purposes. We do not sell, rent, share, or otherwise disclose personally identifiable information from customers for commercial purposes. We do not intend to disclose any personal information of yours without your consent to any third party who is not bound to act on our behalf unless such disclosure is required by law.</p>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <!--model-2-->
      <div class="modal fade" id="myModal2" role="dialog">
         <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title">Disclaimer</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                  <p>The imagery used in the website is indicative of style only. The photographs of the interiors, surrounding views and location may have been digitally enhanced or altered and do not represent actual views or surrounding views. These photographs are indicative only.</p>
                  <p>Changes may be made during the development and standard fittings and specifications are subject to change without notice. Standard fittings and finishes are subject to availability and vendor discretion. Fittings, finishes and fixtures shown in the images contained in this website are not standard and will not be provided as part of an apartment. The information contained herein is believed to be correct but is not guaranteed. Prospective purchasers should make and must rely on their own enquiries. The colours of the buildings are indicative only. This website is a guide only and does not constitute an offer or contract.</p>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>
      <!-- Navigation -->
      <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
         <div class="container">
            <a class="navbar-brand js-scroll-trigger" href="#page-top"><img alt="" src="<?php echo S3_URL?>/site/realestate-assets/freedom/img/CosmoLogo.png"/></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
               <ul class="navbar-nav ml-auto">
                  <li class="nav-item">
                     <a class="nav-link js-scroll-trigger" href="#about">About</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link js-scroll-trigger" href="#Overview">Overview</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link js-scroll-trigger" href="#Amenities">Amenities</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link js-scroll-trigger" href="#Location">Location</a>
                  </li>
                 
               </ul>
            </div>
         </div>
      </nav>
      <header class="bg-area">
         <div class="overlay">
            <div class="container-fluid">
               <div class="row">
                  <div class="full-block intro-box">
                     <div class="full-block text-center">
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6 left-block-intro">
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6 right-block-intro">
                     <div class="form-block">
						
                           <div class="form-group" style="text-align:center; color:#fff; text-transform:uppercase; font-size:13px;">
                            <h3 style="color:#fff; margin-top:50px;font-size: 20px;line-height: 30px;">Thank you for expressing interest on our Properties
							<br/>Our expert will get in touch with you shortly.
							</h3>
                           </div>
                        
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </header>
      <section id="about">
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
                  <div class="full-block">
                     <h3 class="title-page">ABOUT PROJECT</h3>
                     <div class="media about-con">
                        <div class="pull-left">
                           <img alt="" src="<?php echo S3_URL?>/site/realestate-assets/freedom/img/about-pic.jpg" class="img-fluid" />
                        </div>
                        <div class="media-body">
                           <p>This 31-acre project is located in Pudupakkam, in proximity to the IT corridor of Chennai on OMR. This region is in the heart of the IT hub near Siruseri IT Park and also close to some ofthe best educational institutions like the Padma Seshadri School behind Siruseri IT Park, DAV Public School in Eggatur, Vellore Institute of Technology (VIT), Sathyabhama University and HindustanEngineering College. Hospitals like Chettinadu and campuses of IT giants like Infosys, TCS, Wipro, Accenture and Cognizant are also few minutes' drive from Freedom by provident.</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section id="Overview" class="bg-parrlx">
         <div class="container-fluid">
            <div class="row">
               <div class="col-lg-8 mx-auto bg-white">
                  <h3 class="title-page">EVERYTHING IS READY!</h3>
                  <div class="row">
                     <div class="col-lg-6">
                        <ul class="special-features">
                           <li>Ready to swim in the blue pool</li>
                           <li>Ready to gym in the clubhouse</li>
                           <li>Ready to ping pong @ table tennis room</li>
                           <li>Ready to celebrate @ the party hall</li>
                           <li>Ready to go to Siruseri IT park in 5 min</li>
                        </ul>
                     </div>
                     <div class="col-lg-6">
                        <ul class="special-features">
                           <li>Ready to shop in the neighbourhood</li>
                           <li>Ready to play @ children's zone</li>
                           <li>Ready to explore the toddler's area</li>
                           <li>Ready to experience 70% open space</li>
                           <li>Ready to bond with neighbour</li>
                        </ul>
                     </div>
                  </div>
                  <h3 class="title-page">DISTANCE FROM FREEDOM BY PROVIDENT TO</h3>
                  <div class="row">
                     <div class="col-lg-4">
                        <ul class="special-features">
                           <li>Koyembedu 44 k.m</li>
                           <li>Tambaram 22 k.m</li>
                           <li>Siruseri 3 k.m</li>
                           <li>Thoraipakam 22k.m</li>
                           <li>Kovalam 8.5k.m</li>
                        </ul>
                     </div>
                     <div class="col-lg-4">
                        <ul class="special-features">
                           <li>Central 41 k.m</li>
                           <li>Vandaloor 15.6 k.m</li>
                           <li>Navalur 11 k.m</li>
                           <li>Tidel 27.5k.m</li>
                           <li>Mahabalipuram 26.6k.m</li>
                        </ul>
                     </div>
                     <div class="col-lg-4">
                        <ul class="special-features">
                           <li>Airport 33 k.m</li>
                           <li>Kelambakkam 4.1 k.m</li>
                           <li>Sholinganalur 16 k.m</li>
                           <li>Madhyakailash 30k.m</li>
                        </ul>
                     </div>
                  </div>
                  <div class="full-block">
                     <div class="row">
                        <div class="col-md-12">
                           <div class="row">
                              <div class="col-md-12">
                                 <div class="table-responsive">
                                    <table class="table table-striped">
                                       <tbody>
                                          <tr>
                                             <td class="head">Configuration Type</td>
                                             <!--<td class="head">Saleable Area (Sq. Ft.)</td>
                                                <!-- <td class="head">Carpet Area (Sq. Ft.)</td> -->
                                             <td class="head">Price</td>
                                          </tr>
                                          <tr>
                                             <td>2 BHK Comfort</td>
                                             <!-- <td>848</td>-->
                                             <td style="color:#000"><a style="color:#000" class="move" href="#" data-event-category="Configuration" data-event-action="click" data-event-name="Price">Click For Price</a>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>3 BHK Comfort</td>
                                             <!--<td>983</td>-->
                                             <td style="color:#000"><a style="color:#000" class="move" href="#" data-event-category="Configuration" data-event-action="click" data-event-name="Price">Click For Price</a>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>3 BHK Grand</td>
                                             <!--<td>1062</td>-->
                                             <td style="color:#000"><a style="color:#000" class="move" href="#" data-event-category="Configuration" data-event-action="click" data-event-name="Price">Click For Price</a>
                                             </td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section id="Amenities" class="bg-parrlx amenties-bg">
         <div class="container-fluid">
            <div class="row">
               <div class="col-lg-12">
                  <h3 class="title-page white-style">AMENITIES</h3>
                  <ul class="core-values">
                     <li>
                        <div class="core rotate">
                           <img class="black rotate-pic" alt=" 24x7 Security" src="<?php echo S3_URL?>/site/realestate-assets/freedom/img/icons/icons44/black/security.png">
                           <img class="white rotate-pic" alt=" 24x7 Security" src="<?php echo S3_URL?>/site/realestate-assets/freedom/img/icons/icons44/white/security.png">
                        </div>
                        <h4>24x7 Security</h4>
                     </li>
                     <li>
                        <div class="core rotate">
                           <img class="black rotate-pic" alt="65% Open Space with Landscaped Garden" src="<?php echo S3_URL?>/site/realestate-assets/freedom/img/icons/icons44/black/landscape.png">
                           <img class="white rotate-pic" alt="65% Open Space with Landscaped Garden" src="<?php echo S3_URL?>/site/realestate-assets/freedom/img/icons/icons44/white/landscape.png">
                        </div>
                        <h4>Landscaped Garden</h4>
                     </li>
                     <li>
                        <div class="core rotate">
                           <img class="black rotate-pic" alt="Jogging Track" src="<?php echo S3_URL?>/site/realestate-assets/freedom/img/icons/icons44/black/jog.png">
                           <img class="white rotate-pic" alt="Jogging Track" src="<?php echo S3_URL?>/site/realestate-assets/freedom/img/icons/icons44/white/jog.png">
                        </div>
                        <h4>Jogging Track</h4>
                     </li>
                     <li>
                        <div class="core rotate">
                           <img class="black rotate-pic" alt="Indoor and Outdoor Games" src="<?php echo S3_URL?>/site/realestate-assets/freedom/img/icons/icons44/black/badminton.png">
                           <img class="white rotate-pic" alt="Indoor and Outdoor Games" src="<?php echo S3_URL?>/site/realestate-assets/freedom/img/icons/icons44/white/badminton.png">
                        </div>
                        <h4>Indoor and Outdoor Games</h4>
                     </li>
                     <li>
                        <div class="core rotate">
                           <img class="black rotate-pic" alt="Clubhouse" src="<?php echo S3_URL?>/site/realestate-assets/freedom/img/icons/icons44/black/clubhouse.png">
                           <img class="white rotate-pic" alt="Clubhouse" src="<?php echo S3_URL?>/site/realestate-assets/freedom/img/icons/icons44/white/clubhouse.png">
                        </div>
                        <h4>Clubhouse</h4>
                     </li>
                     <li>
                        <div class="core rotate">
                           <img class="black rotate-pic" alt="Swimming Pool and Toddler's Pool " src="<?php echo S3_URL?>/site/realestate-assets/freedom/img/icons/icons44/black/swimming.png">
                           <img class="white rotate-pic" alt="Swimming Pool and Toddler's Pool " src="<?php echo S3_URL?>/site/realestate-assets/freedom/img/icons/icons44/white/swimming.png">
                        </div>
                        <h4>Swimming Pool</h4>
                     </li>
                     <li>
                        <div class="core rotate">
                           <img class="black rotate-pic" alt="Well Equipped Gymnasium (Separate for ladies) " src="<?php echo S3_URL?>/site/realestate-assets/freedom/img/icons/icons44/black/gym.png">
                           <img class="white rotate-pic" alt="Well Equipped Gymnasium (Separate for ladies) " src="<?php echo S3_URL?>/site/realestate-assets/freedom/img/icons/icons44/white/gym.png">
                        </div>
                        <h4>Well Equipped Gymnasium </h4>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </section>
      <section id="Location">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 mx-auto">
                  <h3 class="title-page">Location</h3>
                  <img alt="" src="<?php echo S3_URL?>/site/realestate-assets/freedom/img/locationmap.jpg" class="img-fluid" />
               </div>
            </div>
         </div>
      </section>
	  <div id="pi">
       <?php
	   $vr = $this->session->userdata('puser_id');
	   $ut = $this->session->userdata('utm_source');
	   if( isset($vr) &&  $vr > 0 && $ut != ""){
		   echo $this->lead_check->set_pixel($vr, PURAVANKARA);
		   
	   }
	   ?>
      </div> 
      <!-- Footer -->
      <footer class="bg-dark">
         <div class="container">
            <div class="row">
               <div class="col-lg-6">
                  <ul class="list-unstyled model-listfooter">
                     <li><a href="#"  data-toggle="modal" data-target="#myModal">Privacy Policy | </a></li>
                     <li><a href="#" data-toggle="modal" data-target="#myModal2">Disclaimer </a></li>
                  </ul>
               </div>
               <div class="col-lg-6 copyright">
                  <p>Copyright 2018 Provident Housing Projects Limited. All rights reserved</p>
               </div>
            </div>
         </div>
      </footer>
      <!-- core-js -->
	  <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
      <!--<script src="<?php echo S3_URL?>/site/realestate-assets/freedom/js/jquery.min.js"></script>-->
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="<?php echo S3_URL?>/site/realestate-assets/freedom/js/bootstrap.bundle.min.js"></script>
      <script src="<?php echo S3_URL?>/site/realestate-assets/freedom/js/jquery.easing.min.js"></script>
      <script src="<?php echo S3_URL?>/site/realestate-assets/freedom/js/scrolling-nav.js"></script>
      <script src="<?php echo S3_URL?>/site/realestate-assets/freedom/js/intlTelInput.min.js"></script>
	   <script src="<?php echo S3_URL?>/site/scripts/default.js"></script>
      <script>
          $("body #phone").intlTelInput({
              initialCountry: "auto",
              geoIpLookup: function (callback) {
                  $.get('//freegeoip.net/json', function () { }, "jsonp").always(function (data) {
                      var countryCode = (data && data.country_code) ? data.country_code : "";
                      callback(countryCode);
                  });
              },
              utilsScript: "ipbasedtel/build/js/utils.js" // just for formatting/placeholders etc

          });
          $(".phone").on("countrychange", function (e, countryData) {

              $(".hiddenCountry").val(countryData['dialCode']);

          });
      </script>
   </body>
</html>