<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Anchorage - 2/3/5 BHK Apartments in OMR, Chennai</title>
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/anchorage/images/favicon.jpg" type="image/x-icon" />


	  <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/anchorage/css/vendor.css">
	  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/anchorage/css/main.css">    
      <!-- fonts -->
      <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
            rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
   </head>
   <body>

 <?php $all_array = all_arrays(); ?>      
        <div class="header-top">
            <p>
                <i class="fa fa-bullhorn" aria-hidden="true"></i>
                RERA NUMBER - TN/01/BUILDING/0102/2017
            </p>
        </div>
	<!-- banner -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="">
					<div class="carousel-caption">
                    <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" >
                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/anchorage/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Thank you for expressing interest on our Properties
                            Our expert will get in touch with you shortly.</h4>
                            
                            </div> 
                          
                    </div>
                </form>
					</div>
				</div>
			</div>
		</div>

		<!-- The Modal -->
	</div>
    <!--//banner -->      
	<!-- about -->
	<div class="about" id="about">
		<div class="container">
			<div class="col-md-6 about-left">
             <div class="col-xs-12 aboutimg-w3l aboutimg-w3l2">
					<img src="<?php echo S3_URL?>/site/realestate-assets/anchorage/images/ab5.jpg" alt="image" style="object-position: right;"/>				
				</div>
				<!-- <div class="col-xs-6 aboutimg-w3l">
					<img src="<?php echo S3_URL?>/site/realestate-assets/anchorage/images/ab3.jpg" alt="image" />
				
				</div> -->
				<div class="clearfix"> </div>
			</div>
			<div class="col-md-6 about_right">
				<h3>ABOUT</h3>
				<h3 class="bold">Landmark in making</h3>
				<p>
                Anchorage is a 45-floor building in the beautiful city of Chennai. The tallest residential building in Chennai, Anchorage is sure to become a landmark in the near future. The possession date will be March, 2023. The new building overlooks the Bay of Bengal and Muttukadu backwaters. It consists of premium sky apartments of 2, 3 & 4.5 BHK. The world-class amenities offered by this project are sure to enhance your living experience.
                </p>
                <h3 class="bold">Vicinity experiences</h3>
                <p>
                Located in OMR, Anchorage strategically merges with the East Coast Road (ECR). It makes commuting easy between Palavakkam, Neelankarai, and Thiruvanmiyur, and its location houses many MNCs, IT hubs, hotels, and educational institutions.
                </p>
                <a class="download" target="_blank" href="https://www.houseofhiranandani.com/sites/default/files/Anchorage%20revNov17.pdf">
                Download Brochure</a>                 

			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- //about -->

    <div class="gallery" id="projects">
		<div class="container">
            <h3 class="title">Specification</h3>
            <p>
            Every project is made to measure, engineered to meet committed quality standards to enhance the overall home experience. We make sure all specifications are met for rooms, interiors, flooring, paint, electricity, security and others.

            </p>
            <div class="row">
        <div class="col-md-12">
            <!-- begin panel group -->
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                
                <!-- panel 1 -->
                <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#tab1" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingOne"data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <h4 class="panel-title">FLOORING</h4>
                        </div>
                    </span>
                </div> 
                <!-- / panel 1 -->
                
                <!-- panel 2 -->
                <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#tab2" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <h4 class="panel-title collapsed">ENTRANCE HALL</h4>
                        </div>
                    </span>
                </div>
                <!-- / panel 2 -->
                
                <!--  panel 3 -->
                <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#tab3" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingThree"  class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <h4 class="panel-title">LIFT LOBBY / ELEVATORS</h4>
                        </div>
                    </span>

                        
                      </div>


              
                <!-- panel 2 -->
                <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#Living" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#LivingRoom" aria-expanded="false" aria-controls="LivingRoom">
                            <h4 class="panel-title collapsed">DOORS/WINDOWS</h4>
                        </div>
                    </span>

                   
                </div>
                <!-- / panel 2 -->


            <!-- panel 2 -->
            <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#BalconyC" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#Balcony" aria-expanded="false" aria-controls="BalconyC">
                            <h4 class="panel-title collapsed">KITCHEN</h4>
                        </div>
                    </span>

                  
                </div>
                <!-- / panel 2 -->

                <!-- panel 2 -->
                <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#Living" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#Electricity" aria-expanded="false" aria-controls="LivingRoom">
                            <h4 class="panel-title collapsed">TOILETS</h4>
                        </div>
                    </span>

                </div>
                <!-- / panel 2 -->

            </div> <!-- / panel-group -->
             
        </div> <!-- /col-md-4 -->

    </div> <!--/ .row -->
        </div>      
    </div>    
	<!-- projects -->
	<div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">AMENITIES</h3>
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="GYMNASIUM" href="<?php echo S3_URL?>/site/realestate-assets/anchorage/images/a4.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/anchorage/images/a4.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>GYMNASIUM</h4>
							</div>
						</a>
                    </div>
        
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
                    <div class="gallery-grid1">
						<a title="SPORTS FACILITIES" href="<?php echo S3_URL?>/site/realestate-assets/anchorage/images/a6.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/anchorage/images/a6.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>SPORTS FACILITIES</h4>
							</div>
						</a>
                    </div>              
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
			
                    <div class="gallery-grid1">
						<a title="SWIMMING POOL" href="<?php echo S3_URL?>/site/realestate-assets/anchorage/images/a5.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/anchorage/images/a5.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>SWIMMING POOL</h4>
							</div>
						</a>
					</div>                  
				</div>
			</div>
		</div>
	</div>
    <!-- //projects -->
	<!-- projects -->
	<div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">GALLERY</h3>
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
					<div class="gallery-grid1">
                        <a title="ANCHORAGE" href="<?php echo S3_URL?>/site/realestate-assets/anchorage/images/bg.jpg">
                            <div class="stack twisted">
                                <img  src="<?php echo S3_URL?>/site/realestate-assets/anchorage/images/bg.jpg" alt=" " class="img-responsive" />
                            </div>
							
						</a>
					</div>
				</div>
			
			</div>
		</div>
	</div>
    <!-- //projects -->      
 	<!-- projects -->
	 <div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">Plan</h3>
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/anchorage/images/masterplan.jpg">
							<img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/anchorage/images/masterplan.jpg" alt=" " class="img-responsive" />					
						</a>
					</div>			
				</div>
			</div>
		</div>
	</div>
	<!-- //projects -->	


    	<!-- contact -->
        <div class="address" id="contact">
		<div class="container">
			<h3 class="title">Location Map</h3>
			<div class="address-row">
                 <div class="col-md-12 col-xs-12  wow agile fadeInLeft animated" data-wow-delay=".5s">
					<div class="address-info location-map-image wow fadeInDown animated" data-wow-delay=".5s">
						<img src="<?php echo S3_URL?>/site/realestate-assets/anchorage/images/map-img.jpg" />
					</div>
				</div>
				<div class="col-md-12 col-xs-12 address-right">
					<div class="address-info wow fadeInRight animated" data-wow-delay=".5s">
						<h4>Address</h4>
						<p>5/63 Old Mahapalipuram Road,<br>  Opp Sipcot IT Park,<br> Egattur Village, Padur P.O., Kelambakkam,<br>via Kanchipuram Dist, Chennai,<br> Tamil Nadu 600130</p>
					</div>
					
				
				</div>
				<div class="col-md-12 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
					<div class="address-grid" >
						<!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
						<div id="location">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3890.126399956357!2d80.230256214185!3d12.835107690945486!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a525af5f7282fa3%3A0xe13e6787a846ea43!2sHouse+of+Hiranandani+Admin+and+Engineering+Office!5e0!3m2!1sen!2sin!4v1537852700376" frameborder="0" style="border:0;width: 100%;height: 360px;"  allowfullscreen=""></iframe>
                        
                        </div>
					</div>
				</div>
			
			</div>
		</div>
	</div>
	<!--//contact-->
    <footer>
        <div class="container disclaimer">
            <p>
                <small>
                Disclaimer:The project has been developed by Hiranandani Realtors Pvt. Ltd and registered under Tamil Nadu as Anchorage registration number : TN/01/Building/0102/2017 and the details are available on the website http://www.tnrera.in/reg_projects_tamilnadu *Terms and Conditions Apply. The above mentioned price is excluding other charges and statutory payments. The images I illustration shown are for reference purposes only and may not be an exact representation of the product. The developer reserves the absolute right to change, omit. delete, add or revise any terms and conditions, specifications, amenities, features or images at any time without giving prior notice.
                </small>    
            </p>
        </div>    
		<div class="copy-right-grids">
			<p class="footer-gd">© 2000-2016 House Of Hiranandani. All Rights Reserved | Maintained by <a target="_blank" href="http://www.adcanopus.com/">Adcanopus</a></p>
		</div>
	</footer>   
   	 <div id="pi">
       <?php
	   $vr = $this->session->userdata('user_id');
	   $ut = $this->session->userdata('utm_source');
	   if( isset($vr) &&  $vr > 0 && $ut != ""){
		   echo $this->lead_check->set_pixel($vr, HIRANANDANI);
		   
	   }
	   ?>
      </div> 

	
	
	<script src="<?php echo S3_URL?>/site/realestate-assets/anchorage/js/vendor.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>  
    <script src="<?php echo S3_URL?>/site/realestate-assets/anchorage/js/main.js"></script>
	<script src="<?php echo S3_URL?>/site/scripts/hiranandani.js"></script>
	 
   </body>
</html>