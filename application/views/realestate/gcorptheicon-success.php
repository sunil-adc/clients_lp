<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>G: Corp Builders | G: Corp Developers | G: Corp Builders Bangalore | G: Corp The icon</title>
    <meta name="keywords" content="apartments in thanisandra, high end apartments in thanisandra, premium apartments in thanisandra, high end apartments near manyata, premium apartments near manyata, apartments near manyata, apartments in nagawara">
    <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/favicon.png" type="image/x-icon" />


    <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/css/vendor.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/css/main.css">
    <!-- fonts -->
    <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
        rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
</head>

<body>

    

    <!-- banner -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="header-banner header-banner-desktop">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/desktop-banner.jpg" />
                </div>
                <div class="header-banner header-banner-mobile">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/mobile-banner.jpg" />
                </div>
                <div class="">
                    <div class="carousel-caption">
                        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="">

                            <div class="">
                                <div class="col">
                                    <div class="form-logo">
                                        <a href="#">
                                            <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/logo-blue.png" />
                                        </a>
                                    </div>
                                    <h4>Thank you for expressing interest on our Properties
                            Our expert will get in touch with you shortly.</h4>

                                </div>
                               
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal -->
    </div>
    <!--//banner -->

    <!-- about -->
    <div class="about" id="about">
        <div class="container">
            
            <div class="col-md-6 about_right">
                <h3>Overview of</h3> 
                <h3 class="bold">THE ICON</h3>
                <p>
                G:Corp Developers presents The ICON, a premium lifestyle destination spread over 19.27 acres offering 3BHK, 4BHK and Penthouse options. With 85% open spaces, this development is ideally located adjacent to Manyata Tech Park at Nagawara Junction next to Elements Mall. Designed for IGBC Gold certification, The ICON offers energy efficient eco-friendly environment with zero vehicular movement at the ground level. All apartments come with servant room and utility space.
                </p>
                
            </div>
            <div class="col-md-6 about-left">
                <div class="col-xs-12 aboutimg-w3l aboutimg-w3l2">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/ab5.jpg" alt="image" style="object-position: center;" />
                </div>
                <!-- <div class="col-xs-6 aboutimg-w3l">
					<img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/ab3.jpg" alt="image" />
				
				</div> -->
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
            <p>At The ICON, 5 towers are already handed over and 250+ families have already moved in. You have an option to choose either Under construction or Ready to move-in apartments. </p>
                <p class="strong">REAR Tower F – PRM/KA/RERA/1251/446/PR/171016/000420 | Tower C, D, E - PRM/KA/RERA/1251/446/PR/171015/000326</p>
                <p>Site office: The ICON, 13/2, Nagawara Junction, Thanisandra Main Road, Adjacent to Elements Mall, Bengaluru, Karnataka 560077 </p>
           
         
        </div>
    </div>
    <!-- //about -->
     <!-- popular -->
     <div class="popular-w3" id="popular">
        <div class="container">
            <!-- <h3 class="title">Location Video</h3> -->
            <div class="popular-grids">
                <div class="">

                    <div class="popular-text">
                        <div class="detail-bottom">

                            <div class="rwd-media">
                            <iframe src="https://www.youtube.com/embed/Pls88ElPZ2M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                           
                           
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div>
    <!-- //popular -->
    <div class="gallery highlight" id="projects">
        <div class="container">
            <h3 class="title"></h3>

            <div class="row">
                <div class="col-md-12">
                    <div class="desktop-only">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/price_pc.png" alt=" "
                                    class="img-responsive" />    
                    </div>
                    <div class="mobile-only">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/price_pc.png" alt=" "
                                    class="img-responsive" />    
                    </div>
                </div> <!-- /col-md-4 -->

            </div>
            <!--/ .row -->
        </div>
    </div>
 <!-- //popular -->
 <div class="gallery" id="projects">
		<div class="container">
            <h3 class="title">Highlights </h3>
           
            <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
            <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/hgl.png" alt="image" style="width: 100%;"/>				
            </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <!-- begin panel group -->
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                
                <!-- panel 1 -->
                <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#tab1" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingOne"data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <h4 class="panel-title">Comprising 3 BHK, 4 BHK & Penthouse, THE ICON</h4>
                        </div>
                    </span>
                    
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                        <!-- Tab content goes here -->
                            <ul class="tab-pane-inner-ul">
                                <li>Strategic location</li>
                                <li>85% Open Space</li>
                                <li>Saleable area: 1513 sq.ft. to 3600 sq. ft.</li>
                                <li>4 Flats per floor with No Common Walls.</li>
                                <li>IGBC Pre-Certified for Gold Rating</li>
                                <li>45000 sq.ft. Club House (Approx.)</li>
                                <li>Zero vehicular movement at ground level</li>
                                <li>Motion sensor lights for staircases</li>
                                <li>2 level Basement Parking</li>
                                <li>Proposed metro station: 500mts</li>
                            </ul>
                        </div>
                    </div>
                </div> 
                <!-- / panel 1 -->
                
                <!-- panel 2 -->
                <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#tab2" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <h4 class="panel-title collapsed">Location Advantage:</h4>
                        </div>
                    </span>

                    <div id="collapseTwo" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                        <!-- Tab content goes here -->
                        <ul class="tab-paness-inner-ul">
                        <p>FAR AWAY FROM THE CITY CONGESTION. YET AT THE CENTRE OF EVERYTHING YOU NEED.</p>
                            <li>Manyata Tech Park: 500 Mtrs. </li>
                            <li>Multiple Shopping Destinations: 500 Mtrs</li>
                            <li>Institutions & Colleges: 5 KMS</li>
                            <li>Multi Speciality Hospital: 6 KMS</li>
                            <li>International Airport: 25 KMS</li>
                            <li>City Railway/Metro Stations: 15 KMS</li>
                        </ul>
                        </div>
                    </div>
                </div>
                <!-- / panel 2 -->
                
            </div> <!-- / panel-group -->
             
        </div> <!-- /col-md-4 -->

    </div> <!--/ .row -->
        </div>      
    </div>    
    	<!-- popular -->
    <!-- projects -->
    <div class="gallery popular-w3" id="projects">
        <div class="container">
            <h3 class="title">Ammenities <span>FULLY FUNCTIONAL</span></h3>
            <div class="agile_gallery_grids w3-agile amenities-block demo">
				<div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/a1.png" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Putting Green</h4>
							</div>
					</div> 

				</div>
				<div class="gal-sec">
					<div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/a2.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Cricket Pitch</h4>
                        </div>
                    </div>            
				</div>
				<div class="gal-sec">
			
                    <div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/a3.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Tennis Court</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/a4.png" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Basket Ball Court</h4>
							</div>
					</div> 

				</div>
				<div class="gal-sec">
					<div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/a5.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Volleyball Court</h4>
                        </div>
                    </div>            
				</div>
				<div class="gal-sec">
			
                    <div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/a6.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Steam & Sauna</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/a7.png" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Mini Gym</h4>
							</div>
					</div> 

				</div>
				<div class="gal-sec">
					<div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/a8.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Central park</h4>
                        </div>
                    </div>            
				</div>
				<div class="gal-sec">
			
                    <div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/a9.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>10 Guest rooms</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
			
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/a10.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Out Door Pool</h4>
                        </div>
                    </div>                  
                </div>
                <div class="gal-sec">
			
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/a11.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Soccer Ground</h4>
                        </div>
                    </div>                  
                </div>
			</div>
            <h3 class="title">Ammenities <span>UPCOMING</span></h3>
            <div class="agile_gallery_grids w3-agile amenities-block demo">
				<div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/a12.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>45,000sq.ft Club house (Apprx.)</h4>
                        </div>
					</div> 
				</div>
                <div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/a13.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Pool & Snooker</h4>
                        </div>
					</div> 
				</div>
                <div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/a14.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Mini theatre</h4>
                        </div>
					</div> 
				</div>
                <div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/a15.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Heated Indoor Pool</h4>
                        </div>
					</div> 
				</div>
                <div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/a16.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Squash Court</h4>
                        </div>
					</div> 
				</div>
                <div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/a17.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Fully Loaded Gym</h4>
                        </div>
					</div> 
				</div>
            </div>    
        </div>
    </div>
    <!-- //projects -->

    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">GALLERY</h3>
            <div class="agile_gallery_grids w3-agile demo">

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="gcorptheicon" href="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/gallery1.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/gallery1.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="gcorptheicon" href="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/gallery2.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/gallery2.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="gcorptheicon" href="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/gallery3.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/gallery3.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="gcorptheicon" href="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/gallery4.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/gallery4.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="gcorptheicon" href="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/gallery5.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/gallery5.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="gcorptheicon" href="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/gallery6.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/gallery6.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="gcorptheicon" href="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/gallery7.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/gallery7.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="gcorptheicon" href="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/gallery8.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/gallery8.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>



            </div>
        </div>
    </div>
    <!-- //projects -->
    <!-- projects -->
    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">Master Plan</h3>
            <div class="agile_gallery_grids w3-agile demo">
                <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/masterplan.jpg">
                            <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/masterplan.jpg"
                                alt=" " class="img-responsive" />
                        </a>
                    </div>
                </div>
           




            </div>
        </div>
    </div>
    <!-- //projects -->


    <!-- contact -->
    <div class="address" id="contact">
        <div class="container">
            <h3 class="title">Contact Us</h3>
            <div class="address-row">
            <div class="col-md-12 col-xs-12  wow agile fadeInLeft animated" data-wow-delay=".5s">
                    <div class="address-info wow fadeInDown animated gallery-grid1" data-wow-delay=".5s">
                        <!-- <h4>Map</h4> -->
                        <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/map-img.jpg">
                            <img src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/images/map-img.jpg" />
                        </a>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
                    <div class="address-grid">
                        <!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
                        <div id="location">
                             <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7773.632509127559!2d77.625484!3d13.047365!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa1241768c93f9265!2sThe+Icon!5e0!3m2!1sen!2sin!4v1552365763673" style="border:0;width: 100%;height: 350px;" allowfullscreen></iframe>
             
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
    <!--//contact-->
    <footer>
     
        <div class="copy-right-grids">
            <p class="footer-gd">© 2019 G:Corp Group. All rights reserved.</p>
        </div>
    </footer>

      <div id="pi">
       <?php
       $vr = $this->session->userdata('user_id');
       $ut = $this->session->userdata('utm_source');
       if( isset($vr) &&  $vr > 0 && $ut != ""){
           echo $this->lead_check->set_pixel($vr, GCORPTHEICON);
           
       }
       ?>
      </div> 





    <script src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/js/vendor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/gcorptheicon/js/main.js"></script>
   
</body>

</html>