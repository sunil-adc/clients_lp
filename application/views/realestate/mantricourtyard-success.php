<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8"> 
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title> Mantri Courtyard </title>
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/favicon.ico" type="image/x-icon" />


	  <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/css/vendor.css">
	  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
	  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/css/main.css">    
   	
      <!-- Facebook Pixel Code -->
		<script>
		  !function(f,b,e,v,n,t,s)
		  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		  n.queue=[];t=b.createElement(e);t.async=!0;
		  t.src=v;s=b.getElementsByTagName(e)[0];
		  s.parentNode.insertBefore(t,s)}(window, document,'script',
		  'https://connect.facebook.net/en_US/fbevents.js');
		  fbq('init', '2230877000515713');
		  fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
		  src="https://www.facebook.com/tr?id=2230877000515713&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->


   </head>
   <body> 

   		<script>
		  fbq('track', 'Lead');
		</script>


    <?php $all_array = all_arrays(); ?>
	<!-- banner -->
	<div id="home" class="w3ls-banner cd-section">
		<div class="banner-info">
			<!-- header -->
	
			<!-- //header --> 
		<!-- banner-text -->
	
				
			
			
			<div class="container banner-w3ltext"> 
				<div class="header-banner-content">
					<h3>See Green in Every Aspect of Life</h3>
					<h4>Now Pre-Launching Mantri Courtyard Phase 4</h4>
					<div style="" class="rera_number">RERA Number : PRM/KA/RERA/1251/310/PR/180808/001977</div>
				</div>	
	    	<div class="">
			<div class="">
			<?php $all_array = all_arrays(); ?>
           	 <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="mantri_jsfrm('<?php echo SITE_URL?>realestate/mantricourtyard/submit_frm','1')">
					<div class="form-logo">
								<a href="#">
									<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/logo.png" />
								</a> 
							</div> 
							<h4>Thank you for expressing interest on our Properties
                            Our expert will get in touch with you shortly.</h4>
				 
				
				
				</form>
               	
				</div>  
				
				</div> 
			
			
				
			</div>
			<!-- //banner-text -->
		
		</div>
	</div>
    <!-- //banner --> 
    	<!-- about -->
<!--	<div id="about" class="about cd-section">  
		<div class="container">  
			<div class="col-md-6 about-w3lleft"> 
				<h3 class="w3stitle"><span>About Project</span></h3>
                <P>Mantri Courtyard</P> 
				<h4>REDISCOVER THE JOY OF LIVING</h4>
				<p>
                Introducing Mantri Courtyard, Ready to move in Row villas with a private green space that fills joy in your life. Get back to a home with a private outdoor space, a space that fills you up with happiness, a space where you can recharge, refresh and rebalance yourself. Find that space at Mantri Courtyard and rediscover the joy of living.
                </p>
			</div> 
			<div class="col-md-6 about-w3lright">
            <img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/main-4.jpg" />
			</div>
			<div class="clearfix"> </div> 
		</div>
    </div>-->

    <!-- //about -->
	<!-- blog -->
	<div id="blog" class="blog cd-section">
		<div class="container"> 
			<div class="agileits-hdng">
				<h3 class="w3stitle"><span>OVERVIEW</span></h3>
			</div>
			<!--<div class="blog-agileinfo">
				<div class="col-md-7 blog-w3grid-img">
					<div class="wthree-blogimg">  
						<img style="height: 280px;" src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/main-1.jpg" class="img-responsive" alt=""/>
					</div>  
				</div>
				<div class="col-md-5 blog-w3grid-text"> 
					<h4>Project Specification</h4>
					
					<p><ul>
                                <li>3BHK, 4BHK Row villas starting from Rs 1.35 Crore onwards.</li>
                                <li>Every home is well equipped with uber-chic interiors and ultra-modern facilities.</li>
                                <li>Colossal clubhouse with array of amenities that leave you spoilt for choice.</li>
                            </ul></p>
				</div> 
				<div class="clearfix"> </div>
			</div> -->

			<div class="blog-agileinfo blog-agileinfo-mdl">
				<div class="col-md-6 blog-w3grid-img blog-img-rght">
					<div class="wthree-blogimg">  
						<div class="wthree-blogimg carousel slide" data-ride="carousel" id="himageCarousel"> 
							<div class="carousel-inner">
								<div class="item active">
									<img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/main-4.jpg" / style="border:10px solid #006064"> 
								</div>
								<div class="item ">
									<img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/main-3.jpg" / style="border:10px solid #006064"> 
								</div>
								<div class="item">
									<img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/main-2.jpg" / style="border:10px solid #006064"> 
								</div>
								<div class="item">
									<img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/main-1.jpg" / style="border:10px solid #006064"> 
								</div>
							</div>		
						</div>
					</div>  
				</div>
				<div class="col-md-6 blog-w3grid-text"> 
					<h4>Project Highlights</h4>
					
					<p>

                    <ul>
                                <li>Mantri Courtyard Phase 4 is just 0.8 kms off Kanakpura Main Road, it is very 
    close to the upcoming Anjanapura Metro Station.</li>
                                <li>With Nice Road junction being just 1.2 Kms from Mantri Courtyard Phase 4 it 
    gives good connectivity to locations like Bannergatta Road & Electronic City</li>
                                <li>
                                Mantri Courtyard, 3 & 4 BHK Row Villas with private green space that fill joy in 
    your life. 
                                </li>
                                <li>Successfully Completed Phase 1, Phase 2 & Phase 3 with 80+ Families already 
    living in.</li>
                                <li>Ready to use 17000 Sq. ft clubhouse.</li>
                                <li>Row Villas designed with parking in the Ground Floor, thus eliminating vehicular 
    movement in Front of the homes.</li>
                                <li>2 parking slots with each row villa 
</li>
                            </ul>
                  
					
					</p>
				</div>  
			</div> 
		
			<!--<div class="blog-agileinfo blog-agileinfo-mdl">
            	<div class="col-md-7 blog-w3grid-img ">
					<div class="wthree-blogimg">  
						<img  style="height: 300px;" src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/15.jpg" class="img-responsive" alt=""/>
					</div>  
				</div>
				<div class="col-md-5 blog-w3grid-text"> 
					<h4>Project  Clubhouse</h4>
					
					<p>

                    <ul>
                                <li>A WELL EQUIPPED GYM</li>
                                <li>SWIMMING POOL</li>
                                <li>SPACIOUS BANQUET HALL
                                </li>
                                <li>21 SEAT MINI - THEATRE</li>
                                <li>CONVENIENCE STORE</li>
                                <li>SQUASH COURT</li>
                                <li>BILLIARDS</li>
                                <li>TABLE TENNIS</li>
                                <li>INDOOR GAMES</li>
                            </ul>
                    </p>
				</div>  
			</div>--> 
	

		</div>
	</div>
    <!-- //blog -->    
<div id="blog" class="blog cd-section">
		<div class="container"> 
			<div class="agileits-hdng">
				<h3 class="w3stitle"><span>Location Advantage</span></h3>
			</div> 
			<div class="blog-agileinfo blog-agileinfo-mdl"> 
				<div class="services-w3ls-row">
					
				<div class="col-md-3 col-sm-12 col-xs-12  ">
					<div class="location-advan">
               		<div class="service-icon">
					<i class="fa fa-university" style="font-size: 35px"></i>
				</div> 
					<h5>Sri Kumaran School Delhi Public School Jain International School</h5> 
                    </div> 
				</div> 
					
			    <div class="col-md-3 col-sm-12 col-xs-12">
					<div class="location-advan">
               		<div class="service-icon">
					<i class="fa fa-hospital-o" style="font-size: 35px"></i>
					</div> 
					<h5>Apollo Hospital Fortis Hospital Vasan Eye Care</h5>
					</div> 	
				</div> 
					
				<div class="col-md-3 col-sm-12 col-xs-12 ">
					<div class="location-advan">
               		<div class="service-icon">
					<i class="fa fa-shopping-cart" style="font-size: 35px"></i>
				</div> 
					<h5>Meenakshi Mall Vega City Upcoming Mantri Arena Mall</h5> 
					</div> 	
				</div> 
					
				<div class="col-md-3 col-sm-12 col-xs-12">
					<div class="location-advan">
               		<div class="service-icon">
					<i class="fa fa-building-o" style="font-size: 35px"></i>
				    </div> 
					<h5>Wipro Infosys Oracle</h5>  
					</div> 
				</div> 
			 
 
			</div> 
			</div>  
		</div>
	</div>
<!-- amenities -->
<div id="amenities" class="services cd-section">
		<div class="container"> 
			<div class="agileits-hdng">
				<h3 class="w3stitle"><span>Amenities & Specifications</span></h3> 
			</div>
			<div class="services-w3ls-row">
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/swimming.png" class="img-icon" alt=""/>
				<h5>Swimming Pool </h5> 
				</div> 
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/multihall.png" class="img-icon" alt=""/>
					<h5>Multipurpose Hall</h5> 
				</div> 
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/gym.png" class="img-icon" alt=""/>
				<h5>Gymnasium</h5>
				</div> 
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/cards.png" class="img-icon" alt=""/>
					<h5>Game Room</h5>				
				</div> 
				<div class="clearfix"> </div>
			<div class="blog cd-section" id="blog">	
				<div class="col-md-6 blog-w3grid-text">	
				<ul> 
					<li><strong>STRUCTURE:</strong> SEISMIC ZONE COMPLIANT STRUCTURE ADHERING TO STRUCTURAL REQUIREMENTS, PARTITION WALLS WITH BLOCK MASONRY.</li> 
					
					<li><strong>PAINTING:</strong> INTERIOR FINISHED WITH EMULSION/EQUIVALENT PAINT AND EXTERIOR FINISHED WITH EMULSION/EQUIVALENT PAINT/ TEXTURE PAINT / GFRC / METAL JAALI.</li> 
					 
					<li><strong>FLOORING:</strong> LIVING, DINING, FAMILY, OTHER BEDROOMS AND KITCHEN WITH VITRIFIED TILE/EQUIVALENT FLOORING, MASTER BED ROOM WITH LAMINATED WOODEN FLOORING, UTILITY/BALCONY/PRIVATE TERRACE WITH CERAMIC TILE/EQUIVALENT AND TOILETS WITH CERAMIC TILE/EQUIVALENT FLOORING AND DADOING.</li> 
					<li><strong>DOORS:</strong> WOODEN DOOR WITH HARDWARES.</li> 
					<li>WINDOWS/BALCONY DOOR/VENTILATORS: UPVC /EQUIVALENT.</li>

				</ul>
			</div>  
				<div class="col-md-6 blog-w3grid-text">	
				<ul> 
					<li><strong>RAILINGS:</strong> SS WITH GLASS FOR EXTERIOR BALCONY. A.	SS FOR STAIRCASE.</li> 
					<li>OUTDOOR PARTY AREA LANDSCAPE GARDENS AND WATER FEATURES</li>
					<li><strong>KITCHEN:</strong> PROVISION FOR AQUA GUARD, WASHING MACHINE, DISHWASHER.</li>
					<li>TV POINT, TELEPHONE POINT PROVIDED IN EACH BEDROOM AND LIVING ROOM.</li>
					<li>MINIATURE CIRCUIT BREAKER & EARTH LEAKAGE CIRCUIT BREAKER: PROVIDED</li>
					<li><strong>LANDSCAPE:</strong> PROVIDED.</li>
					<li><strong>CLUB HOUSE:</strong> PROVIDED IN PARCEL 2 & THE SAME AMENITIES SHALL BE USED BY PARCEL 4 RESIDENTS AS WELL</li> 					
				</ul>		
			</div> 	
			</div> 	
				
			</div> 
			
			
			
		</div>
	</div>
    <!-- //amenities -->
    
 	<!-- info -->
     <!--<div id="info" class="blog cd-section">
		<div class="container"> 
			<div class="agileits-hdng">
				<h3 class="w3stitle"><span>NEAR BY</span></h3> 
			</div>
			<div class="blog-agileinfo">
				<div class="col-md-7 blog-w3grid-img">
					<div class="wthree-blogimg">  
						<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/about-1.jpg" class="img-responsive" alt=""/>
                    </div>  
				</div>
				<div class="col-md-5 "> 
                <div class="detail-section-holder"><ul id="flat_count"><li>
                                            <div class="bhk-status-inner">
                                                <h5>Location USPs:</h5>
                                                <p>Just 0.8 kms off Kanakpura Main Road</p>  
                                                <p>Kumaran's Public School, Indus Business Academy,
                                                 The Valley School, Delhi Public School, all within 
                                                 close vicinity  
                                                 </p> 
                                                 <p>Located just 3kms from upcoming mall by Mantri & Krishna Leela park</p>   
                                                 <p>1 km from the Kanakpura Nice Junction</p>
                                                 <p>Electronic City and Bannergatta Road well connected through Nice Road</p>
                                            </div>
                                        </li>
   
                                   
                                     </ul>
                                    </div>
				</div> 
				<div class="clearfix"> </div>
			</div> 
		</div>
	</div>-->
    <!-- //blog -->   
    	<!-- info -->
        <div id="location" class="blog cd-section">
		<div class="container"> 
			<div class="agileits-hdng">
				<h3 class="w3stitle"><span>Gallery</span></h3> 
				<p>Actual Images of Mantri Courtyard Phase 1, Phase 2 & Phase 3</p>
			</div>

            
            <div class="col-md-6 blog-agileinfo">
				<a class="image-popup-fit-width" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/5.jpg" title="Mantri">
					<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/5.jpg" class="img-responsive" alt=""/>
				</a>	
				<div class="clearfix"> </div>
			</div> 
			<div class="col-md-6 blog-agileinfo">
				<a class="image-popup-fit-width" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/2.jpg" title="Mantri">
					<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/2.jpg" class="img-responsive" alt=""/>
				</a>	
				<div class="clearfix"> </div>
            </div> 
			<div class="col-md-6 blog-agileinfo">
				<a class="image-popup-fit-width" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/3.jpg" title="Mantri">
					<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/3.jpg" class="img-responsive" alt=""/>
				</a>	
				<div class="clearfix"> </div>
			</div> 
			<div class="col-md-6 blog-agileinfo">
				<a class="image-popup-fit-width" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/4.jpg" title="Mantri">
					<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/4.jpg" class="img-responsive" alt=""/>
				</a>	
				<div class="clearfix"> </div>
            </div> 
		</div>
	</div>
	
	<div id="blog" class="blog cd-section">
	<div class="container">
	<div class="agileits-hdng">
				<h3 class="w3stitle"><span>Ready to Use 17000 Sq.Ft Clubhouse</span></h3> 
			</div>
		<div class="blog-agileinfo blog-agileinfo-mdl">
				<div class="col-md-6 blog-w3grid-img blog-img-rght">
					<div class="wthree-blogimg carousel slide" data-ride="carousel" id="imageCarousel">  
					     <!-- Bottom Carousel Indicators -->
						 <ol class="carousel-indicators">
							<li data-target="#imageCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#imageCarousel" data-slide-to="1"></li>
							<li data-target="#imageCarousel" data-slide-to="2"></li>
							<li data-target="#imageCarousel" data-slide-to="3"></li>
							<li data-target="#imageCarousel" data-slide-to="4"></li>
							<li data-target="#imageCarousel" data-slide-to="5"></li>
							<li data-target="#imageCarousel" data-slide-to="6"></li>
							<li data-target="#imageCarousel" data-slide-to="7"></li>
							<li data-target="#imageCarousel" data-slide-to="8"></li>
							<li data-target="#imageCarousel" data-slide-to="9"></li>
						</ol>
						<div class="carousel-inner">
							<div class="item active">
								<img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/13.jpg" / style="border:10px solid #006064"> 
							</div>
							<div class="item">
								<img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/15.jpg" / style="border:10px solid #006064"> 
							</div>
							<div class="item">
								<img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/17.jpg" / style="border:10px solid #006064"> 
							</div>
							<div class="item">
								<img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/21.jpg" / style="border:10px solid #006064"> 
							</div>
							<div class="item">
								<img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/22.jpg" / style="border:10px solid #006064"> 
							</div>
							<div class="item">
								<img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/23.jpg" / style="border:10px solid #006064"> 
							</div>
							<div class="item">
								<img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/26.jpg" / style="border:10px solid #006064"> 
							</div>
							<div class="item">
								<img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/27.jpg" / style="border:10px solid #006064"> 
							</div>
							<div class="item">
								<img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/28.jpg" / style="border:10px solid #006064"> 
							</div>
							<div class="item">
								<img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/30.jpg" / style="border:10px solid #006064"> 
							</div>
						</div>
						 <!-- Carousel Buttons Next/Prev -->
						 <!-- <a data-slide="prev" href="#imageCarousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
        				 <a data-slide="next" href="#imageCarousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a> -->
					</div>  
				</div>
				<div class="col-md-6 blog-w3grid-text"> 
	
					  

                    <ul>
						<li>A WELL EQUIPPED GYM</li>
						<li>SWIMMING POOL</li>
						<li>SPACIOUS BANQUET HALL</li>
						<li>21 SEAT MINI - THEATRE</li>
						<li>CONVENIENCE STORE</li>
						<li>SQUASH COURT</li>
						<li>BILLIARDS</li>
						<li>TABLE TENNIS</li>
						<li>INDOOR GAMES</li>						
                    </ul>
                   
				</div>  
			</div> 
	</div>	
	</div>
	
		<!-- info -->
        <div id="" class="blog cd-section">
			<div class="container"> 
				<div class="agileits-hdng">
					<h3 class="w3stitle"><span>Plans</span></h3> 
				</div>
				<div class="row">
				<div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
							<li class="active"><a href="#tab1default" data-toggle="tab">Landscape Plan</a></li>
							<li><a href="#tab4default" data-toggle="tab">Master Plan</a></li>
                            <li><a href="#tab2default" data-toggle="tab">Floor Plan Type1</a></li>
                            <li><a href="#tab3default" data-toggle="tab">Floor Plan Type 2</a></li>
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">
							<div class="col-md-12 blog-agileinfo">
							<a class="image-popup-fit-width" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/lp-1.jpg" title="Landscape Plan">
								<img style="margin: 0 auto;" src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/lp-1.jpg" class="img-responsive" alt=""/>					
							</a>	
								<div class="clearfix"> </div>
							</div> 
						</div>
						<div class="tab-pane fade" id="tab4default">
							<div class="col-md-12 blog-agileinfo">
							<a class="image-popup-fit-width" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/mp-1.jpg" title="Master Plan">
								<img style="margin: 0 auto;" src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/mp-1.jpg" class="img-responsive" alt=""/>					
							</a>	
								<div class="clearfix"> </div>
							</div> 
						</div>
                        <div class="tab-pane fade" id="tab2default">
							<div class="row">
								<div class="col-md-6 blog-agileinfo">
								<a class="image-popup-fit-width" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/ef-b.jpg" title="Carpet Area - 1953sft">
									<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/ef-b.jpg" class="img-responsive" alt=""/>													
								</a>	
									<p>Carpet Area - 1953sft</p>
									<div class="clearfix"> </div>
								</div> 
								<div class="col-md-6 blog-agileinfo">
								<a class="image-popup-fit-width" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/ef-gf.jpg" title="Garden - 176sft">
									<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/ef-gf.jpg" class="img-responsive" alt=""/>		</a>												
									<p>Garden - 176sft</p>
									<div class="clearfix"> </div>
								</div> 
								<div class="col-md-6 blog-agileinfo">
								<a class="image-popup-fit-width" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/ef-ff.jpg" title="SBUA - 2475sf">
									<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/ef-ff.jpg" class="img-responsive" alt=""/>	</a>				
									<p>SBUA - 2475sft</p>
									<div class="clearfix"> </div>
								</div> 
								<div class="col-md-6 blog-agileinfo">
								<a class="image-popup-fit-width" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/ef-t.jpg" title="Terrace - 756sft">
									<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/ef-t.jpg" class="img-responsive" alt=""/>		</a>			
									
									<p>Terrace - 756sft</p>
									<div class="clearfix"> </div>
								</div> 
							</div>
						</div>
                        <div class="tab-pane fade" id="tab3default">
							<div class="row">
								<div class="col-md-6 blog-agileinfo">
								<a class="image-popup-fit-width" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/wf-b.jpg" title="Carpet Area - 2017sft">
									<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/wf-b.jpg" class="img-responsive" alt=""/>		</a>			
									
									<p>Carpet Area - 2017sft</p>
									<div class="clearfix"> </div>
								</div> 
								<div class="col-md-6 blog-agileinfo">
								<a class="image-popup-fit-width" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/wf-gf.jpg" title="Garden - 176sft">
									<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/wf-gf.jpg" class="img-responsive" alt=""/>	</a>				
									
									<p>Garden - 176sft</p>
									<div class="clearfix"> </div>
								</div> 
								<div class="col-md-6 blog-agileinfo">
								<a class="image-popup-fit-width" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/wf-ff.jpg" title="SBUA - 2550sft">
									<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/wf-ff.jpg" class="img-responsive" alt=""/>		</a>			
									<p>SBUA - 2550sft</p>
									<div class="clearfix"> </div>
								</div> 
								<div class="col-md-6 blog-agileinfo">
								<a class="image-popup-fit-width" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/wf-t.jpg" title="Terrace - 2017sft">
									<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/wf-t.jpg" class="img-responsive" alt=""/>	</a>				
									
									<p>Terrace - 2017sft</p>
									<div class="clearfix"> </div>
								</div> 
							</div>
						</div>
                    </div>
                </div>
            </div>
					
				</div>
			</div>
		</div>				
		
		
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3889.646930793398!2d77.52319851392099!3d12.866065990925144!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae4399ee352555%3A0x4d5eff12a1e7c00f!2sMantri+Courtyard!5e0!3m2!1sen!2sin!4v1535096746289" width="100" height="450" frameborder="0" style="width:100%;" allowfullscreen></iframe>
    </div>

    <!-- //blog -->   
    <div class="copyw3-agile">
		<div class="container"> 
			<p>© 2018 MANTRI. All Rights Reserved</p>
		</div>
	</div>
<div id="pi">
       <?php
	   $vr = $this->session->userdata('user_id');
	   //echo $vr;
	   $ut = $this->session->userdata('utm_source');
	   if( isset($vr) &&  $vr > 0 && $ut != ""){
		   echo $this->lead_check->set_pixel($vr,MANTRI);
		   
	   }
	   ?>
      </div> 

   
         
	 <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
	 <input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
	 <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
	 <input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
	 <input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
	<script src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/js/vendor.js"></script>  
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>  
	<script src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/js/intlTelInput.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/js/main.js"></script>
	  <script src="<?php echo S3_URL?>/site/scripts/default.js"></script>
	  <script src="<?php echo S3_URL?>/site/scripts/realesate.js"></script>
   </body>
</html>