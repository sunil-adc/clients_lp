<!DOCTYPE html>

<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Maya Indradhanush</title>
	<meta name="description" content=""/>
	<link rel="shortcut icon" type="image/x-icon" href="https://dpgvclkvnnsee.cloudfront.net/microsites/maya-indradhanush/images/favicon.png">
	<link rel="apple-touch-icon" href="https://dpgvclkvnnsee.cloudfront.net/microsites/maya-indradhanush/images/favicon.png"/>

	<link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/css/vendor.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
	<link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/css/main.css">

	<!-- fonts -->
	<link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
</head>

<body>

	<?php $all_array = all_arrays(); ?>
	<!-- banner -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="header-banner header-banner-desktop">
					<img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/desktop-banner.jpg"/>
				</div>
				<div class="header-banner header-banner-mobile">
					<img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/mobile-banner.jpg"/>
				</div>				
					<div class="carousel-caption">
						<form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>realestate/properties/frm_submit','maya_indradhanush','1','<?php echo SITE_URL?>')">
							<div class="">
								<div class="col">
									<div class="form-logo">
										<a href="#">
										<img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/maya-indradhanush.png" /></a>
									</div>
									<h3>ENQUIRE NOW</h3>
								</div>
								<div class="col">
									<div class="group">
										<div class="form-group">
											<input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
											<span class="help-block" id="name_err1"></span>
										</div>
									</div>
									<div class="group">
										<div class="form-group">
											<input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
											<span class="help-block" id="email_err1"></span>
										</div>
										<div class="form-group">
											<input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode1">
											<input type="tel" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*" placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
											<span class="help-block" id="phone_err1"> </span>
											<span class="help-block" id="CountryCode_err"> </span>
										</div>
									</div>								
									<div class="submitbtncontainer">
										<input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
									</div>
								</div>
							</div>
						</form>
					</div>	
				</div>
		</div>
		<!-- The Modal -->
	</div>
	<!--//banner -->


	<form role="form" id="feedbackForm" class="feedbackForm mobile_form" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>realestate/properties/frm_submit','maya_indradhanush','1','<?php echo SITE_URL?>')">
		<div class="">
			<div class="col">
				<div class="form-logo">
					<a href="#">
					<img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/maya-indradhanush.png" /></a>
				</div>
				<h3>ENQUIRE NOW</h3>
			</div>
			<div class="col marginTop20">
				<div class="group">
					<div class="form-group">
						<input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
						<span class="help-block" id="name_err1"></span>
					</div>
				</div>
				<div class="group">
					<div class="form-group">
						<input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
						<span class="help-block" id="email_err1"></span>
					</div>
					<div class="form-group">
						<input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode1">
						<input type="tel" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*" placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
						<span class="help-block" id="phone_err1"> </span>
						<span class="help-block" id="CountryCode_err"> </span>
					</div>
				</div>								
				<div class="submitbtncontainer">
					<input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
				</div>
			</div>
		</div>
	</form>
	
	<!-- about -->
	<div class="about" id="about">
		<div class="container">
			<h3 class="title">Overview</h3>
			<div class="col-md-12 text-center">
				<h3>EXCLUSIVE 2 & 3 BED SPACIOUS HOMES IN A PREMIUM LOCALITY</h3><br>
				<p>
					Maya Indradhanush is a luxury high-rise residential project by Maya Ventures. Nestled in the serene Judicial layout, just two minutes away from Kankapura road and the upcoming Metro station. Indradhanush offers spacious homes with breathtaking views. Having built South Bengaluru’s landmark - Maya Indraprastha, we have only bettered ourselves with Indradhanush.</p>
				<strong>See it to believe it!</strong>
			</div>
		</div>
	</div>
	<!-- //about -->

	<!-- projects -->
	<div class="gallery">

		<div class="container">
			<h3 class="title">Project Highlights</h3>
			<div class="row">
				<h4 class="amenities-spacing"><strong>EXCLUSIVE 2 & 3 BED SPACIOUS HOMES IN A PREMIUM LOCALITY</strong></h4>
				<div class="col-md-6">
					<ul style="margin-bottom: 30px; padding: 20px">
						<li>33 storied building</li>
						<li>128 luxury apartments</li>
						<li>Spacious living rooms</li>
						<li>Access Control and CCTV</li>
						<li>Visitor’s car parking</li>
						<li>Centralized gas bank</li>
						<li>Generators for power back-up</li>
						<li>Excellent lighting and ventillation</li>
						<li>Apartments with no common walls</li>
						<li>Heat reflecting paint for the exteriors</li>

					</ul>
				</div>
				<div class="col-md-6">
					<ul style="margin-bottom: 30px; padding: 20px">
						<li>Picture window</li>
						<li>Safety grills for all windows</li>
						<li>Green lung space in lift lobby on every alternate floor</li>
						<li>89% open space</li>
						<li>80 feet road on all 3 sides</li>
						<li>Earthquake resistant structure</li>
						<li>Grid solar power backup for lobby and corridors</li>
						<li>Solar Lights for driveways</li>
						<li>Rain water harvesting for recharging</li>
						<li>Safe and clean water using water treatment plant</li>
						<li>Garbage chute with segregation</li>
					</ul>
				</div>

			</div>
		</div>
	</div>

	<div class="gallery">
		<div class="container">
			<h3 class="title">Amenities</h3>
			
			<div class="row">
				<div class="container">
				  <div class="row">
					<div class="col-md-3 col-sm-6 col-xs-6 text-center amenities-spacing">
					  <img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/icon_1.png" alt=" "/><br>
					 <h4 class="amenities-spacing">Lift Lobby</h4>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 text-center amenities-spacing">
					  <img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/icon_2.png" alt=" " />
					   <h4 class="amenities-spacing">Swimming Pool</h4>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 text-center amenities-spacing">
					  <img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/icon_3.png" alt=" " />
					   <h4 class="amenities-spacing">Golf Simulator</h4>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 text-center amenities-spacing">
					  <img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/icon_4.png" alt=" " />
					   <h4 class="amenities-spacing">AV Room</h4>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 text-center amenities-spacing">
					  <img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/icon_5.png" alt=" "/>
					   <h4 class="amenities-spacing">Billiards</h4>					  
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 text-center amenities-spacing">
					  <img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/icon_6.png" alt=" " />
					   <h4 class="amenities-spacing">Gym & Aerobics</h4>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 text-center amenities-spacing">
					  <img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/icon_7.png" alt=" " />
					   <h4 class="amenities-spacing">Jogging Track</h4>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 text-center amenities-spacing">
					  <img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/icon_8.png" alt=" " />
					   <h4 class="amenities-spacing">Children’s Play Area</h4>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 text-center amenities-spacing">
					  <img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/icon_9.png" alt=" " />
					   <h4 class="amenities-spacing">Badminton Court</h4>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 text-center amenities-spacing">
					  <img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/icon_10.png" alt=" " />
					   <h4 class="amenities-spacing">Table tennis</h4>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 text-center amenities-spacing">
					  <img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/icon_11.png" alt=" " />
					   <h4 class="amenities-spacing">Steam bath</h4>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 text-center amenities-spacing">
					  <img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/icon_12.png" alt=" " />
					   <h4 class="amenities-spacing">Sauna & Massage</h4>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 text-center amenities-spacing">
					  <img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/icon_13.png" alt=" "/>
					   <h4 class="amenities-spacing">Virtual gaming room</h4>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 text-center amenities-spacing">
					  <img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/icon_14.png" alt=" " />
					   <h4 class="amenities-spacing">Reading Area</h4>
					</div>
				  </div>
				  </div>
				</div>
			</div>
			
			
		</div>
	</div>

	<!-- //projects -->

	<div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">GALLERY</h3>
			<div class="agile_gallery_grids w3-agile demo">

				<div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Galleria Residences" href="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery1.jpg">
							<div class="stack twisted">
								<img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery1.jpg" alt=" " class="img-responsive"/>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-3 col-sm-6 col-xs-6 gal-sec">
					<div class="gallery-grid1">
						<a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery2.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery2.jpg" alt=" " class="img-responsive" /></a>
					</div>
				</div>

				<div class="col-md-3 col-sm-6 col-xs-6 gal-sec">
					<div class="gallery-grid1">
						<a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery3.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery3.jpg" alt=" " class="img-responsive" /></a>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-6 gal-sec">
					<div class="gallery-grid1">
						<a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery4.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery4.jpg" alt=" " class="img-responsive" /></a>
					</div>
				</div>
				<div class="col-md-3 col-sm-6 col-xs-6 gal-sec">
					<div class="gallery-grid1">
						<a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery5.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery5.jpg" alt=" " class="img-responsive" /></a>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- //projects -->

	<div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">Walkthrough</h3>
			<div class="row">
				<iframe width="100%" height="415" src="https://www.youtube.com/embed/65KDIEDbZes" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
	</div>


	<!-- contact -->
	<div class="address" id="contact">
		<div class="container">
			<h3 class="title">The Location</h3>
			<div class="address-row">
				<div class="col-md-12 col-xs-12">

				</div>
				<div class="col-md-12 col-xs-12">
					<div class="address-grid">
						<!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
						<div id="location">
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d62234.82967800023!2d77.540109!3d12.864136!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd169b91452d398c7!2sMaya+Indradhanush%2C+Judicial+Lyt%2C+Kanakpura+Rd!5e0!3m2!1sen!2sus!4v1557983629001!5m2!1sen!2sus" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- projects -->
	<div class="gallery">

		<div class="container">
			<h3 class="title">Accessibility And Convenience</h3>
			<div class="row">				
				<div class="col-md-6">
					<ul style="margin-bottom: 30px; padding: 20px">
						<li>5 Minutes by walk to upcoming Metro Station</li>
						<li>7 Minutes drive to Nice Ring Road</li>
						<li>15 Minutes drive to Electronics City</li>
						<li>Easy access with other parts with Metro</li>
					</ul>
				</div>
					<div class="col-md-6">
						<ul style="margin-bottom: 30px; padding: 20px">
							<li>Top Schools and Colleges nearby</li>
							<li>5 minute walking distance to upcoming Metro Station</li>
							<li>7 minutes to Ring Road</li>
							<li>15 minutes to Electronics City</li>
						</ul>
					</div>
				</div>
			</div>
	</div>


	<!--//contact-->
	<!-- <footer>

		<div class="copy-right-grids">
			<div class="col-8">
        	
				<p class="para-fotter">Project RERA: PR/KN/170831/001752</p>
				<p class="para-fotter"><span><img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/address.png" alt=" "/></span> Site Address: Maya Indradhanush
5 &amp; 6/1, MALLASSANDRA VILLAGE TALAGHATTAPURA, KANAKAPURA ROAD, Bangalore</p>
				<p class="para-fotter">© 2018 All Rights Reserved.</p>
			</div>
		</div>
	</footer> -->



	<footer class="bg-bark" style="text-align: center;">
		<div class="copy-right-grids">
			<div class="col-sm-12 bg-bark" style="background-color: #000; border-bottom: 1px solid #737373; padding-bottom: 20px;">
				<img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/Anarock-logo.png" alt="group" width="180"> 

				<br>
					
			</div>
				<br>
				
			<div class="col-sm-12 bg-bark" style="background-color: #000;">         
			
				<h5 style="color: #ADADAD; font-size: 16px; padding-top: 10px; line-height: 1.5; font-weight: 600">Project RERA: PR/KN/170831/001752</h5>
				<p style="font-size: 12px; padding: 10px; line-height: 1.5;color: #ADADAD; ">
				Site Address: Maya Indradhanush 5 & 6/1, MALLASSANDRA VILLAGE TALAGHATTAPURA, KANAKAPURA ROAD, Bangalore
				</p>

				<h5 style="color: #ADADAD; font-size: 16px; padding-top: 10px; line-height: 1.5; font-weight: 600">
					© 2019 All Rights Reserved.
				</h5>

				<h5 style="color: #ADADAD; font-size: 16px; padding-top: 10px; line-height: 1.5; font-weight: 600">Disclaimer: </h5>
				<p style="color: #ADADAD; font-size: 12px; padding: 10px; line-height: 1.5">
				This advertisement/printed material does not constitute an offer or contract of any nature whatsoever between the
				 Promoters/Developers and the recipient. All transactions in this development shall be subject to
				 and governed by the terms & conditions of the Agreement for Sale to be entered into between the parties.
				</p>
				<h5 style="color: #ADADAD; font-size: 16px; padding-top: 10px; line-height: 1.5; font-weight: 600">ANAROCK</h5>
				<p style="color: #ADADAD;font-size: 12px; padding: 10px; line-height: 1.5">
				ANAROCK Property Consultants Pvt. Ltd. (Formerly Jones Lang LaSalle Residential Pvt. Ltd.) UPRERA Registration Number: UPRERAAGT10265
				</p>
				
			</div>
		</div>
	</footer>

	<div class="floating-form visiable" id="contact_form">
		<div class="contact-opener">
			Enquire Now
		</div>
		<form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>realestate/properties/frm_submit','maya_indradhanush','2','<?php echo SITE_URL?>')">
 				<div class="col text-center">
					<div class="form-logo">
					<a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/maya-indradhanush.png" 
				/></a>
				</div>
			</div>
				<h3>ENQUIRE NOW</h3>
				<div class="col">
					<div class="groups">
						<div class="form-group">

							<input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
							<span class="help-block" id="name_err2"></span>
						</div>

					</div>
					<div class="groups">
						<div class="form-group">

							<input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
							<span class="help-block" id="email_err2"></span>
						</div>
						<div class="form-group">
							<input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode2">
							<input type="tel" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*" placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
							<span class="help-block" id="phone_err2"> </span>
							<span class="help-block" id="CountryCode_err"> </span>
						</div>
					
					
					</div>

					<div class="submitbtncontainer">
						<input type="submit" id="frm-sbmtbtn2" value="Submit" name="submit">
					</div>
				</div>
			</div>
		</form>
	</div>

	<div class="popup-enquiry-form mfp-hide" id="popupForm">
		<form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>realestate/properties/frm_submit','maya_indradhanush','3','<?php echo SITE_URL?>')">			
				<div class="col">
					<div class="form-logo">
						<a href="#">
	<img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/maya-indradhanush.png" />
	</a>
					</div>
					<h3>ENQUIRE NOW</h3>
				</div>
				<div class="col">
					<div class="groups">
						<div class="form-group">
							<input type="text" class="form-control" id="name3" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
							<span class="help-block" id="name_err3"></span>
						</div>
						<div class="form-group">

							<input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
							<span class="help-block" id="email_err3"></span>
						</div>
					</div>
					<div class="groups">
						<div class="form-group">
							<input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode3">
							<input type="tel" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*" placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
							<span class="help-block" id="phone_err3"> </span>
							<span class="help-block" id="CountryCode_err"> </span>
						</div>
					</div>
				
				
					<div class="submitbtncontainer">
						<input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
					</div>
				</div>		
		</form>
	</div>
    <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
    <input type="hidden" name="utm_source1" id="utm_source1" value="<?php echo (isset($_REQUEST['utm_source'])?$_REQUEST['utm_source'] : "") ?>" />
    <input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium'])? $_REQUEST['utm_medium'] : ""); ?>">
    <input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub'])? $_REQUEST['utm_sub'] : ""); ?>">
    <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign'])?$_REQUEST['utm_campaign']:" ");?>">
	<script src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/js/vendor.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/js/main.js"></script>
	<script src="<?php echo S3_URL?>/site/scripts/properties.js"></script>

</body>

</html>