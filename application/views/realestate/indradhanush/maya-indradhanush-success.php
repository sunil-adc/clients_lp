<!DOCTYPE html>

<html lang="en">

<head>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Maya Indradhanush</title>
	<meta name="description" content=""/>
	<link rel="shortcut icon" type="image/x-icon" href="https://dpgvclkvnnsee.cloudfront.net/microsites/maya-indradhanush/images/favicon.png">
	<link rel="apple-touch-icon" href="https://dpgvclkvnnsee.cloudfront.net/microsites/maya-indradhanush/images/favicon.png"/>

	<link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/css/vendor.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
	<link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/css/main.css">

	<!-- fonts -->
	<link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
</head>

<body>

    <?php $all_array = all_arrays(); ?>

    <!-- banner -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="header-banner header-banner-desktop">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/desktop-banner.jpg" />
                </div>
                <div class="header-banner header-banner-mobile">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/mobile-banner.jpg" />
                </div>
                <div class="">
                    <div class="carousel-caption">
                        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" >

                            <div class="">
                                <div class="col">
                                    <div class="form-logo">
                                        <a href="#">
                                            <img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/maya-indradhanush.png" />
                                        </a>
                                    </div>
                                    <h4>Thank you for expressing interest on our Properties Our expert will get in touch with you shortly.</h4> 
                                </div>
                                 
                                </div> 
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal -->
    </div>
    <!--//banner -->

                             
	<!-- about -->
	<div class="about" id="about">
		<div class="container">
			<h3 class="title">Overview</h3>
			<div class="col-md-12 text-center">
				<h3>EXCLUSIVE 2 & 3 BED SPACIOUS HOMES IN A PREMIUM LOCALITY</h3><br>
				<p>
					Maya Indradhanush is a luxury high-rise residential project by Maya Ventures. Nestled in the serene Judicial layout, just two minutes away from Kankapura road and the upcoming Metro station. Indradhanush offers spacious homes with breathtaking views. Having built South Bengaluru’s landmark - Maya Indraprastha, we have only bettered ourselves with Indradhanush.</p>
				<strong>See it to believe it!</strong>
			</div>
		</div>
	</div>
	<!-- //about -->


	<!-- projects -->
	<div class="gallery">

		<div class="container">
			<h3 class="title">Project Heighlights</h3>
			<div class="row">
				<h3>SEXCLUSIVE 2 & 3 BED SPACIOUS HOMES IN A PREMIUM LOCALITY</h3>
				<div class="col-md-6">
					<ul style="margin-bottom: 30px; padding: 10px">
						<li>33 storied building</li>
						<li>128 luxury apartments</li>
						<li>Spacious living rooms</li>
						<li>Access Control and CCTV</li>
						<li>Visitor’s car parking</li>
						<li>Centralized gas bank</li>
						<li>Generators for power back-up</li>
						<li>Excellent lighting and ventillation</li>
						<li>Apartments with no common walls</li>
						<li>Heat reflecting paint for the exteriors</li>

					</ul>
				</div>
				<div class="col-md-6">
					<ul style="margin-bottom: 30px; padding: 10px">
						<li>Picture window</li>
						<li>Safety grills for all windows</li>
						<li>Green lung space in lift lobby on every alternate floor</li>
						<li>89% open space</li>
						<li>80 feet road on all 3 sides</li>
						<li>Earthquake resistant structure</li>
						<li>Grid solar power backup for lobby and corridors</li>
						<li>Solar Lights for driveways</li>
						<li>Rain water harvesting for recharging</li>
						<li>Safe and clean water using water treatment plant</li>
						<li>Garbage chute with segregation</li>
					</ul>
				</div>

			</div>
		</div>
	</div>

    <!-- //projects -->

	<div class="gallery">
		<div class="container">
			<h3 class="title">Amenities</h3>
			<div class="row">
				<div class="col-md-4"><img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/Lift-Lobby.jpg" alt=" " class="img-responsive"/>
				</div>
				<div class="col-md-4"><img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/Swimming-Pool.jpg" alt=" " class="img-responsive"/>
				</div>
				<div class="col-md-4"><img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/Golf-Simulator.jpg" alt=" " class="img-responsive"/>
				</div>
			</div>
		</div><br/>
		<div class="container">
			<div class="row">
				<div class="col-md-4"><img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/AV-Room.jpg" alt=" " class="img-responsive"/>
				</div>
				<div class="col-md-4"><img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/Billiards.jpg" alt=" " class="img-responsive"/>
				</div>
				<div class="col-md-4"><img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/Gym-&-Aerobics.jpg" alt=" " class="img-responsive"/>
				</div>
			</div>
		</div><br/>
		<div class="container">
			<div class="row">
				<div class="col-md-4"><img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/Jogging-Track.jpg" alt=" " class="img-responsive"/>
				</div>
				<div class="col-md-4"><img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/Children-Play-Area.jpg" alt=" " class="img-responsive"/>
				</div>
				<div class="col-md-4"><img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/Badminton-Court.jpg" alt=" " class="img-responsive"/>
				</div>
			</div>
		</div><br/>
		<div class="container">
			<div class="row">
				<div class="col-md-4"><img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/Table-tennis.jpg" alt=" " class="img-responsive"/>
				</div>
				<div class="col-md-4"><img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/Steam-bath.jpg" alt=" " class="img-responsive"/>
				</div>
				<div class="col-md-4"><img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/Sauna-&-Massage.jpg" alt=" " class="img-responsive"/>
				</div>
			</div>
		</div><br/>
		<div class="container">
			<div class="row">
				<div class="col-md-4"><img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/Virtual-gaming-room.jpg" alt=" " class="img-responsive"/>
				</div>
				<div class="col-md-4"><img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/Reading-Area.jpg" alt=" " class="img-responsive"/>
				</div>
			</div>
		</div>
	</div>

	<!-- //projects -->
   
  
<div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">GALLERY</h3>
			<div class="agile_gallery_grids w3-agile demo">

				<div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Galleria Residences" href="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery1.jpg">
							<div class="stack twisted">
								<img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery1.jpg" alt=" " class="img-responsive"/>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-4 col-sm-6 col-xs-6 gal-sec">
					<div class="gallery-grid1">
						<a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery2.jpg">
	<img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery2.jpg" alt=" " class="img-responsive" /></a>
					</div>
				</div>

				<div class="col-md-4 col-sm-6 col-xs-6 gal-sec">
					<div class="gallery-grid1">
						<a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery3.jpg">
	<img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery3.jpg" alt=" " class="img-responsive" /></a>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 col-xs-6 gal-sec">
					<div class="gallery-grid1">
						<a title="LANDSCAPED GARDENS" href="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery4.jpg">
	<img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/gallery4.jpg" alt=" " class="img-responsive" /></a>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- //projects -->
   
    
    <!-- Walkthrough -->

	<div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">Walkthrough</h3>
			<div class="row">
				<iframe width="100%" height="415" src="https://www.youtube.com/embed/65KDIEDbZes" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
		</div>
	</div>
	<!-- //Walkthrough -->

	<!-- contact -->
	<div class="address" id="contact">
		<div class="container">
			<h3 class="title">The Location</h3>
			<div class="address-row">
				<div class="col-md-12 col-xs-12">

				</div>
				<div class="col-md-12 col-xs-12">
					<div class="address-grid">
						<!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
						<div id="location">
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d62234.82967800023!2d77.540109!3d12.864136!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd169b91452d398c7!2sMaya+Indradhanush%2C+Judicial+Lyt%2C+Kanakpura+Rd!5e0!3m2!1sen!2sus!4v1557983629001!5m2!1sen!2sus" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <!-- //contact -->


	<!-- projects -->
	<div class="gallery">

		<div class="container">
			<h3 class="title">Accessibility And Convenience</h3>
			<div class="row">				
				<div class="col-md-6">
					<ul style="margin-bottom: 30px; padding: 10px">
						<li>5 Minutes by walk to upcoming Metro Station</li>
						<li>7 Minutes drive to Nice Ring Road</li>
						<li>15 Minutes drive to Electronics City</li>
						<li>Easy access with other parts with Metro</li>
					</ul>
				</div>
					<div class="col-md-6">
						<ul style="margin-bottom: 30px; padding: 10px">
							<li>Top Schools and Colleges nearby</li>
							<li>5 minute walking distance to upcoming Metro Station</li>
							<li>7 minutes to Ring Road</li>
							<li>15 minutes to Electronics City</li>
						</ul>
					</div>
				</div>
			</div>
	</div>

	<footer>

		<div class="copy-right-grids">
			<div class="col-8">
        	
				<p class="para-fotter">Project RERA: PR/KN/170831/001752</p>
				<p class="para-fotter"><span><img src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/images/address.png" alt=" "/></span> Site Address: Maya Indradhanush
5 &amp; 6/1, MALLASSANDRA VILLAGE TALAGHATTAPURA, KANAKAPURA ROAD, Bangalore</p>
				<p class="para-fotter">© 2018 All Rights Reserved.</p>
			</div>
		</div>
	</footer>

  <div id="pi">
       <?php
           $vr = $this->session->userdata('user_id');
           $ut = $this->session->userdata('utm_source');
           if( isset($vr) &&  $vr > 0 && $ut != ""){
               echo $this->lead_check->set_pixel($vr, REALESTATE_USER);
               
           }
       ?>
      </div> 
    


    <script src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/js/vendor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/maya-indradhanush/js/main.js"></script>
        
    
</body>

</html>