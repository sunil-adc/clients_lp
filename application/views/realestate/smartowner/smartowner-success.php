

<!DOCTYPE html>
<html>

<head>
    <title>Redefining Real Estate Investing | Asia's Leading Property Marketplace</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8" />
   
	<meta name="description" content="Invest flexibly in exclusive real estate investment opportunities not available on the open market with Asia's largest online property marketplace.">
    <meta name="keywords" content="Real estate investment, property investment, real estate marketplace, property marketplace, investing in real estate, investing in properties">
    <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/fav_icon.ico" type="image/x-icon">


    <!--CSS -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="<?php echo S3_URL?>/site/realestate-assets/smart-owners/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link href="<?php echo S3_URL?>/site/realestate-assets/smart-owners/css/tel.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/smart-owners/css/style.css" type="text/css" media="all" />
    <!---CSS -->

    <!-- /Fonts -->
    <link href="//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800" rel="stylesheet">
    <!-- //Fonts -->

</head>

<body>
    


    <!-- main-content -->
    <div class="main-content" id="home">
        <div class="mobile-background">
        </div>    
        <!--/Top-Header-->
        <div class="top-bar-w3layouts pt-4">
            <div class="container">
                <div class="row">
                    <div class="offset-xl-5">

                    </div>
                    <div class="col-xl-7 top-social-lavi text-md-right text-center mt-md-0 mt-2">
                        <div class="row right-top-info">
                            <div class="col-md-12 header-top text-xl-right text-center">
                                <p class="mr-2">
                                    <span class="fa fa-map-marker mr-2"></span> India | US | UK | Singapore | Australia | Canada | Bahrain
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--//Top-Header-->
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-block">
                        <form class="form-inline header-form" action="JavaScript:void(0)" onsubmit="smartowner_jsfrm('<?php echo SITE_URL?>realestate/smartowner/submit_frm','1')">
                            <div class="logo-block">
                                 <img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/logo.png" alt="" class="img-fluid image1">
                            </div>
                            <div class="form-message mt-3">
                                <h5 style="padding:30px;">Thank you for Expressing interest Our expert will <br>get in touch with you shortly.</h5>
                            </div>
                        </form>
                    </div>
                </div> 
            </div>
        </div>
        <!-- banner -->
        <section class="banner">
            <div class="container">
                <div class="row banner-grids">
                    <div class="col-lg-6 banner-info-w3ls">
                        <h2>A Slice of Tomorrow | SmartOwner
                        </h2>
                        <h3 class="mb-3">Asia’s leading Real Estate FinTech Firm</h3>
                        <p class="mb-4"> Specializes in identifying, funding, and offering the real estate deals of the future, allowing the discerning investor to own a slice of tomorrow at the push of a button.</p>
                        <a href="javascript:void(0)" class="btn open-popupform">Enquire More</a>
                    </div>
                    <div class="col-lg-6 banner-image">
                        <div class="hbanners">

                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- //banner -->
    </div>
    <!-- main-content -->
    

    <!--/How It Works -->
    <section class="about">
        <div class="container">
            <div class="inner-sec-w3pvt pb-5">        
                <!-- services -->
                <div class="fetured-info">
                    <h3 class="tittle  text-center my-lg-5 my-3">How It Works <span class="sub-tittle">SmartOwner makes profiting from the FinTech revolution an effortless and straightforward affair. Investing through SmartOwner is as easy as three simple steps</span></h3>
                    <div class="row fetured-sec mt-lg-5 mt-3 align-items-center">
                        <div class="col-lg-6 p-0">
                            <div class="img-effect">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/img1.jpg" alt="" class="img-fluid image1">
                            </div>

                        </div>
                        <div class="col-lg-6 serv_bottom feature-grids pl-lg-5">
                            <div class="featured-left text-left">
                                <div class="bottom-gd px-3">
                                   <div class="d-flex align-items-center">
                                         <img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/Select.png" alt="" class="img-icon">
                                         <h3 class="mx-4"> SELECT</h3>
                                    </div>   
                                    <p>Choose from our highly curated offerings, then complete your transaction securely through our hassle-free process.</p>
                                </div>
                                <div class="bottom-gd px-3" data-aos="fade-left">
                                    <div class="d-flex align-items-center">
                                        <img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/Earn.png" alt="" class="img-icon">
                                        <h3 class="mx-3 ">EARN</h3>
                                    </div>    
                                    
                                    <p>Receive payouts and stay updated about every aspect of your investment through our comprehensive control panel, while our team of experts takes care of the underlying assets.</p>
                                </div>
                                <div class="bottom-gd px-3" data-aos="fade-left">
                                    <div class="d-flex align-items-center">
                                        <img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/Profit.png" alt="" class="img-icon">
                                        <h3 class="mx-3 ">PROFIT</h3>
                                    </div>    
                                    
                                    <p>Exit the project on our platform at the click of a button and have your earnings wired directly into your bank account.</p>
                                </div>
                            </div>
                        </div>

                    </div>
                    
                </div>

            </div>
        </div>
        <!-- //services -->
    </section>
    <!-- //How It Works -->


    <hr class="custom-divider my-0">

    <!-- What we do -->
    <div class="middle-tem-insidel ">
        <div class="progress-info">

            <!-- slides images -->
            <div class="slide-img" id="masthead">

            </div>
            <!-- //slides images -->

            <div class="left-build-main-temps">
                <h3 class="tittle  text-left my-lg-5 my-3"><span class="sub-tittle"></span>What we do</h3>
                <ul class="tic-info list-unstyled">
                    <li class="progress-tittle">

                    With so much complex change happening at a breakneck pace, getting in at the ground floor of real estate’s future can be challenging. SmartOwner makes it possible.

                    </li>
                    <li class="progress-tittle">

                    Our team of experts curates exclusive and lucrative opportunities maximizing cash flow and growth potential. We are highly selective about our offerings, and only 1% of the deals examined are offered to clients.

                    </li>
                    <li class="progress-tittle">

                    We utilize a proprietary high-tech platform to ensure that unlocking the real estate opportunities of the future is a secure and seamless experience, with clients being able to transact, monitor, and exit their portfolios through our online control panel.

                    </li>
                </ul>




            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!--//What we do -->

    <!-- /The Opportunity -->
    <section class="hand-crafted py-5">
        <div class="container py-lg-5">
            <div class="row accord-info align-items-center">
                <div class="col-lg-6 pl-md-5">

                    <h3 class="mb-md-5 tittle">The Opportunity</h3>

                    <p>Real estate is the world's largest asset class, currently valued at</p>
                    <p class="my-3 highlight-text">$228 Trillion <small>worldwide.</small></p>
                    <p>A paradigm shift in the sector is creating an incredible amount of wealth for those who are able to seize these new opportunities in the world's pre-eminent asset class.
                        <p>
                </div>
                <div class="col-lg-6 banner-image">
                    <div class="img-effect">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/img3.jpg" alt="" class="img-fluid image1">
                    </div>

                </div>

            </div>
        </div>
    </section>
    <!-- //The Opportunity -->    

    <!--/A Slice of Tomorrow -->
    <section class="about">
        <div class="container">
            <div class="inner-sec-w3pvt pt-lg-5 pt-3 pb-1">
                <h3 class="tittle text-center mb-lg-5 mb-3 px-lg-5">A Slice of Tomorrow <span class="sub-tittle">The real estate of tomorrow will look dramatically different from the property markets of yesteryear.</span></h3>
                <div class="feature-grids row mt-3 mb-lg-5 mb-3 text-center">
                    <div class="col" data-aos="fade-up">
                        <div class="bottom-gd px-3">
                            <div class="img-effect">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/co-working.png" alt="" class="img-fluid image1">
                            </div>
                            <h3 class="my-4">Coworking</h3>
                            <!-- <p>Platforms like Airbnb disrupting hospitality</p> -->
                        </div>
                    </div>
                    <div class="col" data-aos="fade-up">
                        <div class="bottom-gd px-3">
                            <div class="img-effect">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/commercial.jpg" alt="" class="img-fluid image1">
                            </div>
                           <h3 class="my-4">Commercial</h3>
                            <!-- <p>Platforms like Airbnb disrupting hospitality</p> -->
                        </div>
                    </div>
                    <div class="col" data-aos="fade-up">
                        <div class="bottom-gd px-3">
                            <div class="img-effect">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/apartments.png" alt="" class="img-fluid image1">
                            </div>
                            <h3 class="my-4">Apartments</h3>
                            <!-- <p>Coworking spaces transforming businesses</p> -->
                        </div>
                    </div>
                    <div class="col" data-aos="fade-up">
                        <div class="bottom-gd px-3">
                            <div class="img-effect">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/gatedcommunities.jpg" alt="" class="img-fluid image1">
                            </div>
                            <h3 class="my-4">Gated Communities</h3>
                            <!-- <p>Co-living spaces redefining residences</p> -->
                        </div>
                    </div>
                    <div class="col" data-aos="fade-up">
                        <div class="bottom-gd px-3">
                            <div class="img-effect">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/villas.jpg" alt="" class="img-fluid image1">
                            </div>
                            <h3 class="my-4">Villas</h3>
                            <!-- <p>Urbanization reshaping the fabric of society</p> -->
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- //services -->
    </section>
    <!-- //A Slice of Tomorrow -->

    <!--/Our Track Record-->
    <section class="stats py-lg-5 py-4">
        <div class="container">
        <h3 class="tittle  text-center my-3">Our Track Record <span class="sub-tittle text-white">Ranked by the Financial Times as the fastest growing FinTech firm in South Asia, and among the 100 fastest growing companies in all of Asia-Pacific</span></h3>
            <div class="row text-center">
                <div class="col">
                    <div class="counter">
                        <h3 class="timer count-title count-number">16-24%</h3>
                        <p class="count-text">IRR track record of SmartOwner clients</p>
                    </div>
                </div>
                <div class="col">
                    <div class="counter">
                        <h3 class="timer count-title count-number">$1.3 Billion</h3>
                        <p class="count-text">Total value of projects funded to date</p>
                    </div>
                </div>
                <div class="col">
                    <div class="counter">
                        <h3 class="timer count-title count-number">14 Million</h3>
                        <p class="count-text">Total square feet funded to date</p>
                    </div>
                </div>
               
            </div>
        </div>
    </section>
    <!--//Our Track Record-->

    <!--/Our Edge -->
    <section class="about our-edge py-lg-5 py-md-5 py-5">
        <div class="container">
            <div class="inner-sec-w3pvt">
                <h3 class="tittle text-center mb-lg-5 mb-3 px-lg-5">Our Edge <span class="sub-tittle">Every SmartOwner investment is</span></h3>
                <div class="feature-grids row mt-3 mb-lg-5 mb-3 text-center">
                    <div class="col-lg-3" data-aos="fade-up">
                        <div class="bottom-gd px-3">
                           
                            <h3 class="my-4">LUCRATIVE</h3>
                            <p>Lock in profitable opportunities in rapidly growing asset classes</p>
                        </div>
                    </div>
                    <div class="col-lg-3" data-aos="fade-up">
                        <div class="bottom-gd px-3">
                         
                            <h3 class="my-4">FLEXIBLE</h3>
                            <p>Add to and subtract from your portfolio at the click of a button</p>
                        </div>
                    </div>
                    <div class="col-lg-3" data-aos="fade-up">
                        <div class="bottom-gd px-3">
                         
                            <h3 class="my-4">DIVERSIFIED</h3>
                            <p>Create a safe portfolio across asset classes and countries</p>
                        </div>
                    </div>
                    <div class="col-lg-3" data-aos="fade-up">
                        <div class="bottom-gd px-3">
                         
                            <h3 class="my-4">EXCLUSIVE</h3>
                            <p>Every offering is a unique deal not available on the general market</p>
                        </div>
                    </div>

                </div>
                <h3 class="tittle text-center px-lg-5"><span class="sub-tittle">Whether it’s a coworking space in the heart of Asia’s fastest-growing city or a revolutionary short term rental portfolio in Dubai, SmartOwner helps you own a slice of tomorrow.</span></h3>

            </div>
        </div>
        <!-- //services -->
    </section>
    <!-- //Our Edge -->

    <!-- /SmartOwner Capital Growth Fund  -->
    <section class="hand-crafted py-5">
        <div class="container py-lg-5">
            <div class="row accord-info align-items-center">
                <div class="col-lg-6 banner-image">
                    <div class="img-effect">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/sebi.png" alt="" class="img-fluid image1">
                    </div>

                </div>
                <div class="col-lg-6 pl-md-5">

                    <h3 class="mb-md-5 tittle">SmartOwner Capital Growth Fund </h3>

                    <p>A SEBI-registered real estate Alternative Investment Fund (AIF) structured to deliver strong upside protection with superior risk mitigation, we select highly curated opportunities in the fastest growing locations, cities, and asset classes and secure them with structural enhancements backed by rigorous due diligence.</p>
                    <ul>
                        <li><strong>Highly curated</strong> projects with optimized structuring</li>
                        <li><strong>Stage-agnostic</strong> investments that maximize profitability</li>
                        <li><strong>Top 8 cities</strong> in India, with a special emphasis on the fastest growing ones</li>
                        <li><strong>Prime locations</strong> and high-demand pockets within each city</li>
                        <li><strong>Rigorously vetted</strong> investment opportunities</li>
                        <li><strong>Diversified portfolio</strong> across a variety of asset classes</li>
                    </ul>
                    <p class="mt-4">For more details, please connect with us <a href="javascript:void(0)" class="btn btn-warning open-popupform">Enquire Now</a></p>
                    
                </div>
            </div>
        </div>
    </section>
    <!-- //SmartOwner Capital Growth Fund  -->  

    <!-- testimonials -->
    <div class="testimonials py-md-5 py-5">
        <div class="container ">
            <h3 class="tittle  text-center mb-lg-5 mb-3"><span class="sub-tittle">As</span>Featured In</h3>

            <section class="testimonials-logos slider">
                <div class="slide"><img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/BusinessToday.png"></div>
                <div class="slide"><img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/FT1000.png"></div>
                <div class="slide"><img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/IndiaTimes.png"></div>
                <div class="slide"><img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/MoneyControl.png"></div>
                <div class="slide"><img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/SiliconIndia.png"></div>
                <div class="slide"><img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/TechCo.png"></div>
                <div class="slide"><img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/TheEconomicTimes.png"></div>
                <div class="slide"><img src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/images/VC-Circle.png"></div>
            </section>

        </div>
    </div>
    <!-- //testimonials -->
    
    <!--footer -->
    <footer>
        <div class="footer_1its">
            <div class="container pt-md-4">

                <div class="footer-grid_section text-center">
                    <div class="footer-title-w3pvt mb-4">
                        <h3>Follow with us</h3>
                    </div>
                    <ul class="social_section_1info">
                        <li class="facebook"><a target="_blank" href="https://www.facebook.com/smartownerglobal"><span class="fa fa-facebook mr-1"></span>facebook</a></li>
                        <li class="linkedin"><a target="_blank" href="https://www.linkedin.com/company/smartowner-services-india-pvt-ltd"><span class="fa fa-linkedin mr-1"></span>linkedin</a></li>
                        <li class="google"><a target="_blank" href="https://plus.google.com/+SmartOwner"><span class="fa fa-google-plus mr-1"></span>google</a></li>
                        <li class="google"><a target="_blank" href="https://www.youtube.com/c/SmartOwner"><span class="fa fa-youtube mr-1"></span>youtube</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </footer>
    <div id="pi">
       <?php
       $vr = $this->session->userdata('user_id');
       $ut = $this->session->userdata('utm_source');
       if( isset($vr) &&  $vr > 0 && $ut != ""){
           echo $this->lead_check->set_pixel($vr, SMARTOWNER);
           
       }
       ?>
      </div> 
    <!-- //footer -->

    <!-- copyright -->
    <div class="cpy-right text-center py-3">
        <p class="copy-w3layouts">©2019 SMARTOWNER SERVICES INDIA PVT. LTD. ALL RIGHTS RESERVED | MAINTAIN BY
            <a href="http://www.adcanopus.com/">ADCANOPUS.</a>
        </p>
        <div class="move-top"><a href="#home" class="move-top"> <span class="fa fa-angle-up  mb-3" aria-hidden="true"></span></a></div>
    </div>
    <!-- //copyright -->





   

    <!-- Script -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>  
    <script src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/js/tel.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/smart-owners/js/custom.js"></script>
     <script src="<?php echo S3_URL?>/site/scripts/smartowner.js"></script>
</body>

</html>
