<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Vajram - Esteva</title>
      <meta name="description" content="Esteva offers you a luxurious lifestyle">
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/favicon.png" type="image/x-icon" />
	  <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/esteva/css/vendor.css">
	  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/esteva/css/main.css">    
      <!-- fonts -->
      <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
            rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
   </head>
   <body>

 <?php $all_array = all_arrays(); ?>      
        <div class="header-top">
            <p>
                <i class="fa fa-info-circle" aria-hidden="true"></i>
                RERA Approval No.PRM/KA/RERA/1251/446/PR/170916/000254
            </p>
        </div> 
	<!-- banner -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="">
					<div class="carousel-caption">
                    <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="vajram_jsfrm('<?php echo SITE_URL?>realestate/vajram/submit_frm','esteva','1')">
                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Esteva offers you a luxurious lifestyle</h4>
                            
                            </div> 
                            <div class="col">   
                    <div class="group">
                        <div class="form-group">
                       
                            <input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err1"></span>
                        </div>
              
                    </div>  
                    <div class="group">
                        <div class="form-group">
                        <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                        <span class="help-block" id="email_err1" ></span>
                        </div>
                        <div class="form-group">
                            <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode1">
                            <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                            <span class="help-block" id="phone_err1"> </span>
                            <span class="help-block" id="CountryCode_err"> </span>
                        </div>
                    
                    </div>   
                    <div class="group building-type">
                        <label class="heading-label">I am interested in Vajram Esteva</label>
                        <div class="form-group checkbox-list">
                            <div class="set">
                                <div class="checkbox-item w-25">
                                    <input class="inp-cbx" id="cbx11" type="checkbox" style="display: none;"  name="bhk1[]" value ="3-bhk"/>
                                    <label class="cbx" for="cbx11"><span>
                                        <svg width="12px" height="10px" viewbox="0 0 12 10">
                                        <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                                        </svg></span><span>3 BHK</span>
                                    </label>
                                </div>   
                                <div class="checkbox-item">
                                    <input class="inp-cbx" id="cbx12" type="checkbox" style="display: none;" id ="bhk1[]" name="bhk1[]" value ="3-bhk-staff"/>
                                    <label class="cbx" for="cbx12"><span>
                                        <svg width="12px" height="10px" viewbox="0 0 12 10">
                                        <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                                        </svg></span><span>3 BHK + Staff</span>
                                    </label>
                                </div> 
                            </div>    
                            <div class="set">
                                <div class="checkbox-item w-25">
                                    <input class="inp-cbx" id="cbx13" type="checkbox" style="display: none;" id ="bhk1[]" name="bhk1[]" value ="4-bhk"/>
                                    <label class="cbx" for="cbx13"><span>
                                        <svg width="12px" height="10px" viewbox="0 0 12 10">
                                        <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                                        </svg></span><span>4 BHK</span>
                                    </label>
                                </div> 
                                <div class="checkbox-item">
                                    <input class="inp-cbx" id="cbx14" type="checkbox" style="display: none;" id ="bhk1[]" name="bhk1[]" value ="4-bhk-staff"/>
                                    <label class="cbx" for="cbx14"><span>
                                        <svg width="12px" height="10px" viewbox="0 0 12 10">
                                        <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                                        </svg></span><span>4 BHK + Staff</span>
                                    </label>
                                </div>  
                            </div>    
                        </div>
                    </div>       
                
                    <!--<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
					<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
					<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">-->
                    <div class="submitbtncontainer">
                    <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
                    </div>
                    </div>
                    </div>
                </form>
					</div>
				</div>
			</div>
		</div>

		<!-- The Modal -->
	</div>
    <!--//banner -->      
	<!-- about -->
	<div class="about" id="about">
		<div class="container">
			<div class="col-md-6 about_right">
				<h3>ABOUT</h3>
				<h3 class="bold">Vajram ESTEVA</h3>
                <p>
                     Vajram Esteva is a luxury 3 and 4 bhk condominiums located in Bellandur, (Outer Ring Road.) Esteva offers you a luxurious lifestyle that is unparalleled. It’s a unique blend of retail outlets and residential places with unmatched design and architecture.
                </p>    
                <P>
                     It is located strategically located in close proximity to around 8 major tech parks. Vajram Esteva allows you to relax in the classy rooftop swimming pool or lounge in the indoor sky lounge or stroll around the aroma garden or the lotus pond. It is strategically located close to many educational institutes, malls, medical institutions, etc making it an adept project. Your house will be the talk of your social group as the beauty, luxury and facilities provided at the Vajram Esteva is sure to garner a lot of appreciation from your guests.
                </P>
            </div>
            <div class="col-md-6 about-left">
                <div class="col-xs-12 aboutimg-w3l aboutimg-w3l2">
                    <div class="rwd-media">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3890.757139759449!2d77.620802!3d12.79427!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae6ac4118aa827%3A0xd0612b84eac47d4!2sBannerghatta+Main+Rd%2C+Karnataka!5e0!3m2!1sen!2sin!4v1561028568880!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                    </div>
                </div>
                <div class="clearfix"> </div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- //about -->
	<!-- popular -->
	<div class="popular-w3" id="popular">
		<div class="container">
            <h3 class="title">Project Highlights</h3>
			<div class="popular-grids">
				<div class="">
					
					<div class="popular-text">
						<div class="detail-bottom background-white">
                            <ul style="list-style: inside;">
                                <li>Tech Parks nearby: Ecospace – 500 m, Prestige Park-1 km</li>
                                <li>MG Road – 13 km</li>
                                <li>Nearby Education centres: Kidzee Bellandur, New Horizon Gurukul, Geetanjali Olympiad School, Sri Ravishankar Bal Mandir, GEAR Innovative International School, Little Millennium</li>
                                <li>Nearby Hospitals: Sakra World Hospital, § Partha Dental Hospitals & Clinics, Nelivigi Eye Hospital and Surgical Centre, Sankara Eye Hospital.</li>
                            </ul>                
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
            </div>
      
		</div>
	</div>
    <!-- //popular -->
   
 
	<!-- projects -->
	<div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">AMENITIES</h3>
			<div class="agile_gallery_grids w3-agile amenities-block demo">
				<div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/1.png" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Amphitheater</h4>
							</div>
					</div> 

				</div>
				<div class="gal-sec">
					<div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/2.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Sports Facilities</h4>
                        </div>
                    </div>            
				</div>
				<div class="gal-sec">
			
                    <div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/3.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Kids Play Area</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/4.png" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Swimming Pool </h4>
							</div>
					</div> 

				</div>
				<div class="gal-sec">
					<div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/5.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Video Security</h4>
                        </div>
                    </div>            
				</div>
				<div class="gal-sec">
			
                    <div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/6.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Lift</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/7.png" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>24x7 Security</h4>
							</div>
					</div> 

				</div>
				<div class="gal-sec">
					<div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/8.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Gym</h4>
                        </div>
                    </div>            
				</div>
				<div class="gal-sec">
			
                    <div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/9.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Aroma Garden</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/10.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Children’s Play Area</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/11.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Foosball Table</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/12.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Gazebos</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/13.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Gym and Aerobics hall</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/14.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Indoor Kids Play Area</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/15.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Indoor Sky Lounge</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/16.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Lotus Pond</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/17.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Mini Theatre</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/18.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Outdoor Lounge</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/19.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Seating Alcoves</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/20.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Sky Garden Seating</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/21.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Table tennis</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/22.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Toddler Play Area</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
                    <div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/23.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>Yoga Room</h4>
                        </div>
					</div>                  
                </div>
			</div>
		</div>
	</div>
    <!-- //projects -->   	
<div class="simple-bg">
</div>    
  

	<!-- projects -->
	<div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">GALLERY</h3>
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
                        <a title="Vajram Esteva" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p1.jpg">
                            <div class="stack twisted">
                                <img  src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p1.jpg" alt=" " class="img-responsive" />
                            </div>
							
						</a>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Esteva" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p2.jpg">
                        <div class="stack twisted">    
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p2.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
                    </div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Esteva" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p3.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p3.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Esteva" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p4.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p4.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Esteva" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p5.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p5.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Esteva" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p6.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p6.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Esteva" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p7.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p7.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
                </div>
                <!-- <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Esteva" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p8.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p8.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
                </div> -->
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Esteva" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p9.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p9.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Esteva" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p10.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/p10.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
    <!-- //projects -->      
    <!-- //popular -->
    <div class="gallery" id="projects">
		<div class="container">
            <h3 class="title">Location AND Specification</h3>
            <div class="row">
        <div class="col-md-12">
            <!-- begin panel group -->
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                
                <!-- panel 1 -->
                <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#tab1" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingOne"data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <h4 class="panel-title">Structure</h4>
                        </div>
                    </span>
                    
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                        <!-- Tab content goes here -->
                        <ul class="tab-pane-inner-ul">
                            <li>Two Basements + Ground Floor + 12 Upper Floor, RCC Frame structure compliance to Seismic Zone II</li>
                            <li>Walls – Eco friendly light weight bricks with smooth finish plastering</li>
                            <li>Superior Quality Steel reinforcement with high quality in-house RMC</li>
                            <li>Structural design validation vetted by Bureau Veritas Civil-Aid</li>
                        </ul>
                           
                        </div>
                    </div>
                </div> 
                <!-- / panel 1 -->
                
                <!-- panel 2 -->
                <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#tab2" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <h4 class="panel-title collapsed">Kitchen</h4>
                        </div>
                    </span>

                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                        <!-- Tab content goes here -->
                        <ul class="tab-pane-inner-ul">
                            <li>Superior quality 800 x 1200 double charge vitrified tiles by leading brands/ imported</li>
                            <li>Superior quality ceramic tile dadoing up to 2′-0 above counter level</li>
                            <li>Premium emulsion paint for Walls</li>
                            <li>Polished granite slabs (20 mm thick) shall be provided for the counter tops</li>
                        </ul>
                        </div>
                    </div>
                </div>
                <!-- / panel 2 -->
                
                <!--  panel 3 -->
                <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#tab3" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingThree"  class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <h4 class="panel-title">Bedroom</h4>
                        </div>
                    </span>

                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">
                          <!-- tab content goes here -->
                            <ul class="tab-pane-inner-ul">
                            <li>Superior Quality Laminated Wooden Flooring for Master Bedroom</li>
                            <li>Superior quality 800 x 1200 double charge vitrified tiles by leading brands/ imported</li>
                            <li>Premium Emulsion paint for walls</li>
                            </ul>
                          </div>
                        </div>
                      </div>


              
                <!-- panel 2 -->
                <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#Living" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#LivingRoom" aria-expanded="false" aria-controls="LivingRoom">
                            <h4 class="panel-title collapsed">Wet Kitchen & Staff Room</h4>
                        </div>
                    </span>

                    <div id="LivingRoom" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Living">
                        <div class="panel-body">
                        <!-- Tab content goes here -->
                        <ul class="tab-pane-inner-ul">
                            <li>Superior Quality 600 x 600 anti skid Ceramic/Verified floor tiles for the wet kitchen &amp; staff room</li>
                            <li>Premium emulsion paints for walls</li>
                            <li>Ceramic tile dadoing up to 2′-0 above counter level for wet kitchen</li>
                            <li>Polished granite slabs (20 mm thick) shall be provided for the counter tops</li>
                        </ul>
                        </div>
                    </div>
                </div>
                <!-- / panel 2 -->


            <!-- panel 2 -->
            <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#BalconyC" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#Balcony" aria-expanded="false" aria-controls="BalconyC">
                            <h4 class="panel-title collapsed">Toilets</h4>
                        </div>
                    </span>

                    <div id="Balcony" class="panel-collapse collapse" role="tabpanel" aria-labelledby="BalconyC">
                        <div class="panel-body">
                        <!-- Tab content goes here -->
                        <ul class="tab-pane-inner-ul">
                            <li>Superior quality double charge 600×1200 vitrified tile wall dadoing up to false ceiling in Master Bedroom toilet &amp; common toilet</li>
                            <li>Superior quality ceramic tile wall dadoing up to false ceiling for kids toilet</li>
                            <li>Superior quality anti skid ceramic flooring False ceiling with grid panels</li>
                            <li>Granite vanity counter in Master bedroom & common toilet</li>
                            <li>Shower partition in master bedroom</li>
                        </ul>
                        </div>
                    </div>
                </div>
                <!-- / panel 2 -->

                <!-- panel 2 -->
                <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#Living" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#Electricity" aria-expanded="false" aria-controls="LivingRoom">
                            <h4 class="panel-title collapsed">Sit Out & Terraces</h4>
                        </div>
                    </span>

                    <div id="Electricity" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Living">
                        <div class="panel-body">
                        <!-- Tab content goes here -->
                        <ul class="tab-pane-inner-ul">
                            <li>Superior quality anti skid ceramic tile flooring</li>
                            <li>MS handrail with enamel panel paint as per design</li>
                        </ul>
                        </div>
                    </div>
                </div>
                <!-- / panel 2 -->

            <!-- panel 2 -->
            <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#Living" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#Utility" aria-expanded="false" aria-controls="LivingRoom">
                            <h4 class="panel-title collapsed">Doors & Windows</h4>
                        </div>
                    </span>

                    <div id="Utility" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Living">
                        <div class="panel-body">
                        <!-- Tab content goes here -->
                        <ul class="tab-pane-inner-ul">
                            <li>Main Door- Hard wood door frame with BST Shutter & Architrave finished with PU polish</li>
                            <li>Internal Door- Hard wood frame with BST finish Shutter & Architrave furnished with PU polish</li>
                            <li>Toilet Door-Hard Wood frame with One side teak shutter finished with Polish & other side laminate</li>
                            <li>Superior quality ironmongery and fittings for all doors</li>
                            <li>Balcony sliding door- 3 track UPVC sliding for living &amp; others with 2.5 track UPVC sliding</li>
                            <li>Windows – 2.5 track UPVC sliding with mosquito mesh</li>
                        </ul>
                        </div>
                    </div>
                </div>
                <!-- / panel 2 -->


                
            <!-- panel 2 -->
            <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#Living1" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#Living1" aria-expanded="false" aria-controls="Living1">
                            <h4 class="panel-title collapsed">Electrical</h4>
                        </div>
                    </span>

                    <div id="Living1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Living1">
                        <div class="panel-body">
                            <!-- Tab content goes here -->
                            <ul class="tab-pane-inner-ul">
                                <li>BESCOM power supply: 5 kW for Flats</li>
                                <li>Generator power back up for all lighting Points, TV Units, Refrigerator and 100% power back up for common facilities</li>
                                <li>Superior quality modular switches from reputed brands</li>
                                <li>Television points in living and all bedrooms</li>
                                <li>Telephone points in living and Master Bed room</li>
                                <li>Intercom facility from security cabin to each Flat</li>
                                <li>Split AC provision in living room and all bedrooms</li>
                                <li>Provision for Exhaust fans in kitchen and toilets.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- / panel 2 -->

                <!-- panel 2 -->
                <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#Living2" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#Living2" aria-expanded="false" aria-controls="Living2">
                            <h4 class="panel-title collapsed">Plumbing & Sanitary</h4>
                        </div>
                    </span>

                    <div id="Living2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Living2">
                        <div class="panel-body">
                            <!-- Tab content goes here -->
                            <ul class="tab-pane-inner-ul">
                                 <li>CP Fittings &amp; sanitary wares from reputed brands in all toilets</li>
                                <li>Stainless steel single bowl sink with drain board in Kitchen</li>
                                <li>Water supply &amp; drainage pipes from reputed brands</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- / panel 2 -->        
                
                
                   <!-- panel 2 -->
                   <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#Living3" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#Living3" aria-expanded="false" aria-controls="Living3">
                            <h4 class="panel-title collapsed">Common Areas</h4>
                        </div>
                    </span>

                    <div id="Living3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Living3">
                        <div class="panel-body">
                            <!-- Tab content goes here -->
                            <ul class="tab-pane-inner-ul">
                                <li>Granite / vitrified flooring</li>
                                <li>Premium Emulsion paint for walls</li>
                                <li>MS handrail as per design</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- / panel 2 -->    
                   <!-- panel 2 -->
                   <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#Living4" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#Living4" aria-expanded="false" aria-controls="Living2">
                            <h4 class="panel-title collapsed">Lifts</h4>
                        </div>
                    </span>

                    <div id="Living4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Living4">
                        <div class="panel-body">
                            <!-- Tab content goes here -->
                            <ul class="tab-pane-inner-ul">
                                 <li>Total No. of 2 lifts of reputed brand for each block</li>
                                 <li>Capacity  1 No. of 10- passengers and 1 No. of 15-passengers</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- / panel 2 -->    
                   <!-- panel 2 -->
                   <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#Living2" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#Living5" aria-expanded="false" aria-controls="Living5">
                            <h4 class="panel-title collapsed">Foyer / Formal Living / Family Room / Dining</h4>
                        </div>
                    </span>

                    <div id="Living5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Living5">
                        <div class="panel-body">
                            <!-- Tab content goes here -->
                            <ul class="tab-pane-inner-ul">
                                 <li>CP Fittings &amp; sanitary wares from reputed brands in all toilets</li>
                                <li>Stainless steel single bowl sink with drain board in Kitchen</li>
                                <li>Water supply &amp; drainage pipes from reputed brands</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- / panel 2 -->    
                   
            </div> <!-- / panel-group -->
             
        </div> <!-- /col-md-4 -->

    </div> <!--/ .row -->
        </div>      
    </div>    
 	<!-- projects -->
	 <div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">Plan</h3>
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/masterplan.jpg">
							<img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/masterplan.jpg" alt=" " class="img-responsive" />					
						</a>
					</div>			
				</div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/type-b4.jpg">
							<img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/type-b4.jpg" alt=" " class="img-responsive" />					
						</a>
					</div>			
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/type-a.jpg">
							<img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/type-a.jpg" alt=" " class="img-responsive" />					
						</a>
					</div>			
				</div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/type-b.jpg">
							<img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/type-b.jpg" alt=" " class="img-responsive" />					
						</a>
					</div>			
				</div>
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/type-b1.jpg">
							<img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/type-b1.jpg" alt=" " class="img-responsive" />					
						</a>
					</div>			
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/type-b2.jpg">
							<img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/type-b2.jpg" alt=" " class="img-responsive" />					
						</a>
					</div>			
				</div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/esteva/images/type-b3.jpg">
							<img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/type-b3.jpg" alt=" " class="img-responsive" />					
						</a>
					</div>			
                </div>
                
			</div>
		</div>
	</div>
	<!-- //projects -->	


    	<!-- contact -->
	<div class="address" id="contact">
		<div class="container">
			<h3 class="title">Contact Us</h3>
			<div class="address-row">
                 <div class="col-md-12 col-xs-12  wow agile fadeInLeft animated" data-wow-delay=".5s">
					<div class="address-info wow fadeInDown animated" data-wow-delay=".5s">
						<h4>Map</h4>
						<img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/map-img.jpg" />
					</div>
				</div>
				<div class="col-md-12 col-xs-12 address-right">
					<div class="address-info wow fadeInRight animated" data-wow-delay=".5s">
						<h4>Address (Corporate Office)</h4>
						<p>Sy No. 61,Shri Vijayaraja Estate,<br> Chokkanahalli Thanisandra Main Road,<br> Yelahanka Hobli, Bengaluru,<br> Karnataka-560064</p>
					</div>
					
				
				</div>
				<div class="col-md-12 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
					<div class="address-grid">
						<!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
						<div id="location">
                       
                            <iframe style="border:0;width: 100%;height: 300px;"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.643388555558!2d77.68414291418627!3d12.930626690882802!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae13a66a78dfe5%3A0xa2563151d5b2c53a!2sVajram+Esteva!5e0!3m2!1sen!2sin!4v1538122970238"  allowfullscreen></iframe>
                        </div>
					</div>
				</div>
			
			</div>
		</div>
	</div>
	<!--//contact-->
    <footer>
		<div class="copy-right-grids">
			<p class="footer-gd">© 2018 vajramgroup. All rights reserved.| Maintained by Adcanopus</p>
		</div>
	</footer>    
	<div class="floating-form visiable" id="contact_form">
	<div class="contact-opener">Enquire Now</div>
    <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="vajram_jsfrm('<?php echo SITE_URL?>realestate/vajram/submit_frm','esteva','2')">
    
    <div class="">       
                    <div class="col text-center">
                             <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Esteva offers you a luxurious lifestyle</h4>
                            </div> 
                            <div class="col">     
                    <div class="groups">
                        <div class="form-group">
                       
                            <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err2"></span>
                        </div>
              
                    </div>  
                    <div class="groups">
                    <div class="form-group">
                   
                    <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                    <span class="help-block" id="email_err2" ></span>
                    </div>
                    <div class="form-group">
					<input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode2">
                    <input type="text" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                    <span class="help-block" id="phone_err2"> </span>
				    <span class="help-block" id="CountryCode_err"> </span>
                    </div>
                 
                    </div>      
                
                     <div class="group building-type">
                        <label class="heading-label">I am interested in Vajram Esteva</label>
                        <div class="form-group checkbox-list">
                            <div class="set">
                                <div class="checkbox-item w-25">
                                    <input class="inp-cbx" id="cbx21" type="checkbox" style="display: none;"  name="bhk2[]" value ="3-bhk"/>
                                    <label class="cbx" for="cbx21"><span>
                                        <svg width="12px" height="10px" viewbox="0 0 12 10">
                                        <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                                        </svg></span><span>3 BHK</span>
                                    </label>
                                </div>   
                                <div class="checkbox-item">
                                    <input class="inp-cbx" id="cbx22" type="checkbox" style="display: none;"  name="bhk2[]" value ="3-bhk-staff"/>
                                    <label class="cbx" for="cbx22"><span>
                                        <svg width="12px" height="10px" viewbox="0 0 12 10">
                                        <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                                        </svg></span><span>3 BHK + Staff</span>
                                    </label>
                                </div> 
                            </div>    
                            <div class="set">
                                <div class="checkbox-item w-25">
                                    <input class="inp-cbx" id="cbx23" type="checkbox" style="display: none;"  name="bhk2[]" value ="4-bhk"/>
                                    <label class="cbx" for="cbx23"><span>
                                        <svg width="12px" height="10px" viewbox="0 0 12 10">
                                        <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                                        </svg></span><span>4 BHK</span>
                                    </label>
                                </div> 
                                <div class="checkbox-item">
                                    <input class="inp-cbx" id="cbx24" type="checkbox" style="display: none;"  name="bhk2[]" value ="4-bhk-staff"/>
                                    <label class="cbx" for="cbx24"><span>
                                        <svg width="12px" height="10px" viewbox="0 0 12 10">
                                        <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                                        </svg></span><span>4 BHK + Staff</span>
                                    </label>
                                </div>  
                            </div>    
                        </div>
                    </div>        
                
                    <div class="submitbtncontainer">
                    <input type="submit" id="frm-sbmtbtn2" value="Submit" name="submit">
                    </div>
                    </div>
                    </div>
                </form>	
				<div>
    <div class="popup-enquiry-form mfp-hide" id="popupForm">
    <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="vajram_jsfrm('<?php echo SITE_URL?>realestate/vajram/submit_frm','esteva','3')">
    
     <div class="">       
                    <div class="col">
                             <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/esteva/images/logo-small.png" />
                                </a> 
                            </div>
                            <h4>Esteva offers you a luxurious lifestyle</h4>
                            </div> 
                            <div class="col">
                    <div class="groups">
                        <div class="form-group">
                       
                            <input type="text" class="form-control" id="name3" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err3"></span>
                        </div>
						<div class="form-group">
                   
				   <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
				   <span class="help-block" id="email_err3" ></span>
				   </div>
                    </div>  
                    <div class="groups">
                  
                    <div class="form-group">
					<input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode3">
                    <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                    <span class="help-block" id="phone_err3"> </span>
				    <span class="help-block" id="CountryCode_err"> </span>
                    </div>
         
                    </div>      
                
                    <div class="group building-type">
                        <label class="heading-label">I am interested in Vajram Esteva</label>
                        <div class="form-group checkbox-list">
                            <div class="set">
                                <div class="checkbox-item w-25">
                                    <input class="inp-cbx" id="cbx31" type="checkbox" style="display: none;"  name="bhk3[]" value ="3-bhk"/>
                                    <label class="cbx" for="cbx31"><span>
                                        <svg width="12px" height="10px" viewbox="0 0 12 10">
                                        <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                                        </svg></span><span>3 BHK</span>
                                    </label>
                                </div>   
                                <div class="checkbox-item">
                                    <input class="inp-cbx" id="cbx32" type="checkbox" style="display: none;"  name="bhk3[]" value ="3-bhk-staff"/>
                                    <label class="cbx" for="cbx32"><span>
                                        <svg width="12px" height="10px" viewbox="0 0 12 10">
                                        <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                                        </svg></span><span>3 BHK + Staff</span>
                                    </label>
                                </div> 
                            </div>    
                            <div class="set">
                                <div class="checkbox-item w-25">
                                    <input class="inp-cbx" id="cbx33" type="checkbox" style="display: none;"  name="bhk3[]" value ="4-bhk"/>
                                    <label class="cbx" for="cbx33"><span>
                                        <svg width="12px" height="10px" viewbox="0 0 12 10">
                                        <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                                        </svg></span><span>4 BHK</span>
                                    </label>
                                </div> 
                                <div class="checkbox-item">
                                    <input class="inp-cbx" id="cbx34" type="checkbox" style="display: none;"  name="bhk3[]" value ="4-bhk-staff"/>
                                    <label class="cbx" for="cbx34"><span>
                                        <svg width="12px" height="10px" viewbox="0 0 12 10">
                                        <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                                        </svg></span><span>4 BHK + Staff</span>
                                    </label>
                                </div>  
                            </div>    
                        </div>
                    </div>
                
                    <div class="submitbtncontainer">
                    <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>
                    </div>
                    </div>
                </form>

	</div>

	<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
	<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
	<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
	<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
	<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
	
	
	<script src="<?php echo S3_URL?>/site/realestate-assets/esteva/js/vendor.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>  
    <script src="<?php echo S3_URL?>/site/realestate-assets/esteva/js/main.js"></script>
	
	 <script src="<?php echo S3_URL?>/site/scripts/vajram.js"></script>
   </body>
</html>