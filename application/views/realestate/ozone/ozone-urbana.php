<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Apartments For Sale In Devanahalli Bangalore - Urbana Ozone Group</title>
      <meta name="description" content="2, 3 BHK apartments for sale in Devanahalli Bangalore. Urbana Ozone Group offers integrated township in Devanahalli Bangalore on affordable price.">
      <meta name="keywords" content="Apartments For Sale In Devanahalli Bangalore">
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/favicon.ico" type="image/x-icon" />


	  <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/css/vendor.css">
	  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/css/main.css">    
      <!-- fonts -->
      <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
            rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
   </head>
   <body>

 <?php $all_array = all_arrays(); ?>      
        <!-- <div class="header-top">
            <p>
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                Chokkanahalli Thanisandra Main Road, Yelahanka Hobli, Bengaluru-560064
            </p>
        </div> -->
	<!-- banner -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="">
					<div class="carousel-caption">
                    <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="ozone_jsfrm('<?php echo SITE_URL?>realestate/ozone/submit_frm','ozone-urbana','1')">
                   
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>185 acres Integrated Township adjacent to Bengaluru International Airport</h4>
                            
                            </div> 
                            <div class="col">   
                    <div class="group">
                        <div class="form-group">
                       
                            <input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err1"></span>
                        </div>
              
                    </div>  
                    <div class="group">
                        <div class="form-group">
                        <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                        <span class="help-block" id="email_err1" ></span>
                        </div>
                        <div class="form-group">
                            <input type="hidden" class="Countryname" name="Countryname" value="in" id="Countryname1">
                            <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode1">
                            <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                            <span class="help-block" id="phone_err1"> </span>
                            <span class="help-block" id="CountryCode_err"> </span>
                        </div>
                    
                    </div>   
                
                    <!--<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
					<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
					<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">-->
                    <div class="submitbtncontainer">
                    <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
                    </div>
                    </div>
                    </div>
                </form>
					</div>
				</div>
			</div>
		</div>

		<!-- The Modal -->
	</div>
    <!--//banner -->      
	<!-- about -->
	<div class="about" id="about">
		<div class="container">
			<div class="col-md-6 about-left">
             <div class="col-xs-12 aboutimg-w3l aboutimg-w3l2">
             <div class="rwd-media">
             <br><br>
                            <iframe src="https://www.ozonegroup.com/urbana/video/nps-video-2.mp4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
				</div>
				<!-- <div class="col-xs-6 aboutimg-w3l">
					<img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/ab3.jpg" alt="image" />
				
				</div> -->
				<div class="clearfix"> </div>
			</div>
			<div class="col-md-6 about_right">
				<h3>ABOUT</h3>
				<h3 class="bold">Ozone Urbana</h3>
				
                <p>
                Bangalore’s largest integrated township, with nearly 750 flats for sale in 2015, and 150 acres of land, offers a lifestyle solution within the confines of home. With 12 million sq. ft. of development, 9 kms internal road networking, 70% open space and 1 million sq. ft. of clubhouse, this infrastructural masterpiece is in place with regards to roads, street lights and sewerage. Offering 2, 2.5, 3 and 4 bedroom flats, this north Bangalore’s largest gated community is strategically located. Ozone Urbana is the only township from North Bangalore to have a dedicated water connection from the airport. Ozone Urbana has witnessed an appreciation of 25% year on year. 
                </p>    
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- //about -->

    <div class="gallery" id="projects">
		<div class="container">
            <h3 class="title">Project Highlights</h3>
            <p>With just 5 kms away from the international airport, the site is just 8 minutes ride far with a direct access to NH 7 Highway. It gets connected to the city Hebbal flyover. The nearest railway station - Yelahanka, Hebbal, the ITC factory, are in the vicinity of less than half an hour distance.</p>
            <br>
            <p>Providing for large commercial spaces, the project not only offers best flats but also amenities like a hospital by BR Life Healthcare group and the National Public School within its gated community. A retail mall, multiplex, hospital sports facilities and club house, ¾ star hotels and service apartments, with neighborhood pharmacies and departmental stores, the facility provides best in class facilities to blend in with the lifestyle</p>
        </div>
    </div>    
    <div class="gallery" id="projects">
		<div class="container">
            <h3 class="title">Ozone Urbana Prime – A life like no other</h3>
            <p>With just 5 kms away from the international airport, the site is just 8 minutes ride far with a direct access to NH 7 Highway. It gets connected to the city Hebbal flyover. The nearest railway station - Yelahanka, Hebbal, the ITC factory, are in the vicinity of less than half an hour distance.</p>
            <br>
            <p>Ozone Group has come up with a fantastic real-estate project in the northern sector of Bangalore. Ozone Urbana Prime will offer spacious 1, 2 and 3 BHK apartments to choose from. Spread over 185 acres of a splendid landscape this project will house an unending list of amenities and social infrastructures.</p>
            <br>
            <p>A total of 128 units of 1BHK apartments, 197 2BHK apartments and 355 3BHK units will be on offer. The 3BHK units will house both compact and large units – 231 compact units and 142 – large units. The 3 towers which are currently ready for launch will span over 10.5 Acres which is a cluster in 185 acres.</p>
            <br>
            <p>The 2BHK units here will span between 1035 – 1296 Sqft. while the 1BHK units will be 650 Sqft. The 3BHK compact units will be roughly 1360-1468 Sqft. and the large units will be 1536 – 1543 Sqft. Each of the floors here will house 8 units and 4 elevators in total. The magnificent landscape here will mesmerize a resident. 70% open space in the project will residents an unforgettable experience to live.</p>
            <br>
            <p>The possession of the units at this splendid project is set to begin from 2022. The tower here will be G+16 floors here.</p>
        </div>
    </div>        
    <!-- projects -->
	<div class="gallery" id="projects">
		<div class="container">
             <h3 class="title">Amenities</h3>
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Gymnasium" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a1.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a1.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Gymnasium</h4>
							</div>
						</a>
					</div> 
					<div class="gallery-grid1">
						<a title="Multipurpose Hall" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a2.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a2.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Multipurpose Hall</h4>
							</div>
						</a>
                    </div>
                    <div class="gallery-grid1">
						<a title="Table Tennis" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a3.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a3.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Table Tennis</h4>
							</div>
						</a>
                    </div>
                    <div class="gallery-grid1">
						<a title="25 seat – Mini Theatre" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a4.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a4.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>25 seat – Mini Theatre</h4>
							</div>
						</a>
                    </div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Swimming Pool" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a5.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a5.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Swimming Pool</h4>
							</div>
						</a>
                    </div>
                    <div class="gallery-grid1">
						<a title="Cricket Pitches" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a6.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a6.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Cricket Pitches</h4>
							</div>
						</a>
                    </div> 
                    <div class="gallery-grid1">
						<a title="Tennis Courts" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a7.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a7.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Tennis Courts</h4>
							</div>
						</a>
                    </div> 
                    <div class="gallery-grid1">
						<a title="Jogging Tracks" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a8.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a8.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Jogging Tracks</h4>
							</div>
						</a>
                    </div>  
                             
				</div>    
                <div class="col-md-4 col-sm-4 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Lounge Bar" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a9.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a9.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Lounge Bar</h4>
							</div>
						</a>
                    </div>
                    <div class="gallery-grid1">
						<a title="Café" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a10.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a10.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Café</h4>
							</div>
						</a>
                    </div> 
                    <div class="gallery-grid1">
						<a title="Bicycle Track" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a11.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a11.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Bicycle Track</h4>
							</div>
						</a>
                    </div> 
                    <div class="gallery-grid1">
						<a title="Fountain Plaza" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a12.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/a12.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Fountain Plaza</h4>
							</div>
						</a>
                    </div>            
				</div>              
			</div>
		</div>
         
	</div>
    <!-- //projects -->

  <div class="gallery">
     <div class="container">
            <h3 class="title">Urbana Prime - Overview</h3>
            <p><strong>The perfect combination of comfort and luxury.</strong></p>
            <p>Located on the Northern edge of Ozone Urbana is Urbana Prime, specially designed apartments that combine every modern day luxury. The view is magical, lush, landscaped vistas with the famed Nandi Hills in the distance. As part of Ozone Urbana, every conceivable comfort is close by. You have the magic of space, with apartment sizes ranging from 635 sq. ft. to 2572 sq. ft. The designs are truly captivating, and you have a choice of 1, 2, 3 & 4 BHK bedroom homes.</p>
            <div class="row">
                <div class="col-md-12">
                    <!-- begin panel group -->
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        
                        <!-- panel 1 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab1" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingOne"data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <h4 class="panel-title">Clubhouse Amenities:</h4>
                                </div>
                            </span>
                            
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                    <ul class="tab-pane-inner-ul">
                                        <li>Lobby</li>
                                        <li>Multipurpose Hall</li>
                                        <li>Crèche</li>
                                        <li>Yoga</li>
                                        <li>Library</li>
                                        <li>Indoor Badminton Court</li>
                                        <li>Table Tennis</li>
                                        <li>Indoor games room</li>
                                        <li>Billiards</li>
                                        <li>Yoga</li>
                                        <li>Aerobics</li>
                                        <li>Gym</li>
                                        <li>Steam & Sauna</li>
                                        <li>Discussion Room</li>
                                        <li>Swimming Pool</li>
                                        <li>Barbeque Deck</li>
                                    </ul>
                                </div>
                            </div>
                        </div> 
                        <!-- / panel 1 -->
                        
                        <!-- panel 2 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab2" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">Township Amenities</h4>
                                </div>
                            </span>

                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                <ul class="tab-pane-inner-ul">
                                    <li>Entrance Plaza</li>
                                    <li>Tennis Court</li>
                                    <li>Jogging Track</li>
                                    <li>Bicycle Track</li>
                                    <li>Kids’ Play Area</li>
                                    <li>Landscaped Garden</li>
                                    <li>Outdoor Gym</li>
                                    <li>Café</li>
                                    <li>Senior Citizen Area</li>
                                    <li>Fountain Plaza</li>
                                    <li>Volleyball</li>
                                    <li>Community Boulevard</li>
                                    <li>Retail Kiosks Courts</li>
                                    <li>Basketball Court</li>
                                    <li>Pharmacy</li>
                                    <li>Telemedicine Centre</li>
                                    <li>Barbeque Area</li>
                                    <li>Multipurpose Court</li>
                                    <li>Cricket Pitch With Ball Throwing Machine</li>
                                </ul>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 2 -->
                        
                    
                    </div> <!-- / panel-group -->
                    
                </div> <!-- /col-md-4 -->
            </div> <!--/ .row -->
        </div> 
  </div>
  <div class="bg-image">
  <img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/popup-img-horizontal.png" alt=" " class="img-responsive" />
  </div>
  <div class="gallery">
     <div class="container">
            <h3 class="title">Urbana Prime - Specifications</h3>
            <p><strong>A few of the salient features of Ozone Urbana Prime</strong></p>
            
            <div class="row">
                <div class="col-md-12">
                    <!-- begin panel group -->
                    <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                        
                        <!-- panel 1 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab1" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingOne"data-toggle="collapse" data-parent="#accordion2" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <h4 class="panel-title">STRUCTURE:</h4>
                                </div>
                            </span>
                            
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                    <ul class="tab-pane-inner-ul">
                                        <li>RCC framed structure.</li>
                                        <li>Seismic zone II structural design for earthquake resistance.</li>
                                    </ul>
                                </div>
                            </div>
                        </div> 
                        <!-- / panel 1 -->
                        
                        <!-- panel 2 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab2" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">FLOORING & SKIRTING:</h4>
                                </div>
                            </span>

                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                <ul class="tab-pane-inner-ul">
                                    <li>Foyer / Living / Dining / Kitchen / Bedrooms: Good quality 600 x 600 mm glazed vitried tiles.</li>
                                    <li>Balconies / Utility / Toilets: Anti-skid ceramic tiles.</li>
                                    <li>Corridors / Lift Lobbies / Staircases: Natural stone / Vitried tiles.</li>
                                </ul>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 2 -->
                        
                        <!-- panel 3 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab3" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion2" href="#collapse3" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">KITCHEN & UTILITY:</h4>
                                </div>
                            </span>

                            <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                <ul class="tab-pane-inner-ul">
                                    <li>Ceramic / Vitried tiles dado 600 mm above counter level. Provision for RO unit.</li>
                                    <li>Provision for washing machine in utility.</li>
                                </ul>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 3 -->         

                        <!-- panel 4 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab4" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion2" href="#collapse4" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">TOILETS:</h4>
                                </div>
                            </span>

                            <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                <ul class="tab-pane-inner-ul">
                                    <li>Antiskid ceramic tiles flooring.</li>
                                    <li>Good quality ceramic / Vitrified tiles dado.</li>
                                    <li>Good quality EWC & Wall mounted wash basins in all toilets.</li>
                                    <li>CP fittings of reputed make.</li>
                                    <li>Geyser provision in all toilets.</li>
                                    <li>False ceiling in all toilets.</li>
                                </ul>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 4 -->   
                    
                        <!-- panel 5 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab5" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion2" href="#collapse5" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">INTERNAL WALLS AND EXTERNAL FINISHES::</h4>
                                </div>
                            </span>

                            <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                <ul class="tab-pane-inner-ul">
                                    <li>Internal Walls & Ceilings: Emulsion paint / Oil bound distemper.</li>
                                    <li>External walls: Exterior weather resistant anti-fungal paint.</li>
                                </ul>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 5 -->       

                        <!-- panel 6 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab6" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion2" href="#collapse6" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">FENESTRATIONS/ DOORS / WINDOWS / VENTILATORS::</h4>
                                </div>
                            </span>

                            <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                <ul class="tab-pane-inner-ul">
                                    <li>Main Door: Designer ush door with veneer on the outside and laminate inside.</li>
                                    <li>Bedrooms / Toilets: Engineered wood frame & shutters with laminate.</li>
                                    <li>Windows: UPVC/ aluminum sliding windows with provision for mosquito mesh.</li>
                                    <li>Ventilators: UPVC/ aluminum ventilators with glass louvers/ openable shutters.</li>
                                </ul>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 6 -->     

                        <!-- panel 7 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab7" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion2" href="#collapse7" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">IRON MONGERY & HARDWARE:</h4>
                                </div>
                            </span>

                            <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                <ul class="tab-pane-inner-ul">
                                    <li>Balconies: MS enamel painted railing.</li>
                                    <li>MS enamel grill for windows.</li>
                                </ul>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 7 -->       

                        <!-- panel 8 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab8" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion2" href="#collapse8" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">WATER SUPPLY/ PLUMBING:</h4>
                                </div>
                            </span>

                            <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                <ul class="tab-pane-inner-ul">
                                    <li>Rainwater harvesting, sewage treatment plant and water treatment plant.</li>
                                    <li>CPVC and UPVC schedule 40 / 80 water supply lines</li>
                                    <li>Pressure tested plumbing lines.</li>
                                    <li>UPVC sewer lines.</li>
                                    <li>Organic waste converter.</li>
                                </ul>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 8 -->   

                        <!-- panel 9 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab9" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion2" href="#collapse9" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">ELECTRICAL:</h4>
                                </div>
                            </span>

                            <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                <ul class="tab-pane-inner-ul">
                                    <li>Fire resistant, concealed wiring with PVC insulated copper wires.</li>
                                    <li>Modular switches and sockets of reputed make.</li>
                                    <li>1 no of earth leakage circuit breaker (ELCB) in every unit for safety.</li>
                                    <li>1 no of miniature circuit breaker (MCB) for each circuit provided at main distribution box in every flat.</li>
                                    <li>Electrical point provision for split AC units in master bedroom.</li>
                                    <li>Conduit provision for split AC units in living/ dining area.</li>
                                    <li>TV point in Living and Master Bedroom.</li>
                                    <li>Telephone point in Living and Master Bedroom.</li>
                                    <li>Power points for geysers & exhaust fan in Toilets.</li>
                                    <li>DG backup for only lighting circuit for each at.</li>
                                    <li>100% power back up for common areas.</li>
                                </ul>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 9 --> 

                        <!-- panel 10 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab10" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion2" href="#collapse10" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">LIFTS:</h4>
                                </div>
                            </span>

                            <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                <ul class="tab-pane-inner-ul">
                                    <li>3 nos of lifts in each block (2 Passenger and 1 Service lift)</li>
                                </ul>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 10 -->                                                                                                                     
                        <!-- panel 11 -->
                        <div class="panel panel-default">
                            <!--wrap panel heading in span to trigger image change as well as collapse -->
                            <span class="side-tab" data-target="#tab10" data-toggle="tab" role="tab" aria-expanded="false">
                                <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion2" href="#collapse11" aria-expanded="false" aria-controls="collapseTwo">
                                    <h4 class="panel-title collapsed">GAS BANK:</h4>
                                </div>
                            </span>

                            <div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                <!-- Tab content goes here -->
                                <ul class="tab-pane-inner-ul">
                                    <li>Piped gas distribution system for kitchen in all the units with metering facility.</li>
                                </ul>
                                </div>
                            </div>
                        </div>
                        <!-- / panel 11 -->                                                                                                                     

                    </div> <!-- / panel-group -->
                    
                </div> <!-- /col-md-4 -->
            </div> <!--/ .row -->
        </div> 
  </div>
	<!-- projects -->
	<div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">GALLERY</h3>
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
                        <a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p1.jpg">
                            <div class="stack twisted">
                                <img  src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p1.jpg" alt=" " class="img-responsive" />
                            </div>
							
						</a>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p2.jpg">
                        <div class="stack twisted">    
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p2.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
                    </div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p3.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p3.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p4.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p4.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p5.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p5.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p6.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p6.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p7.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p7.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p8.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p8.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p9.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p9.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p10.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p10.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p11.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p11.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p12.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p12.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p13.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/p13.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>

			</div>
		</div>
	</div>
    <!-- //projects -->      
    <div class="gallery" id="projects">
		<div class="container">
            <h3 class="title">Ozone Urbana Prime - A layout perfect for small and medium families</h3>
            <p>The floor plan of Ozone Urbana Prime will be perfect for small ad nuclear families. They are also a perfect option for investment purposes. Ozone Urbana will offer both 1 and 2 BHK apartments to choose from. The 1 BHK units here will be 650 Sqft, While the 2BHK units here will be 1035-1296 Sqft and the 3 BHK units are of two types that is, Compact: 1360-1468 sqft and the Large : 1536-1543 sqft. </p>
        </div>
    </div>    
 	<!-- projects -->
	 <div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">Plan</h3>
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/masterplan.jpg">
							<img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/masterplan.jpg" alt=" " class="img-responsive" />					
						</a>
					</div>			
				</div>
                <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/typical_plan_ozone-urbana.jpg">
							<img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/typical_plan_ozone-urbana.jpg" alt=" " class="img-responsive" />					
						</a>
					</div>			
				</div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                        <div class="gallery-grid1">
                            <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/type-a.jpg">
                                <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/type-a.jpg" alt=" " class="img-responsive" />					
                            </a>
                        </div>			
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                        <div class="gallery-grid1">
                            <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/type-b.jpg">
                                <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/type-b.jpg" alt=" " class="img-responsive" />					
                            </a>
                        </div>			
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                        <div class="gallery-grid1">
                            <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/type-b1.jpg">
                                <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/type-b1.jpg" alt=" " class="img-responsive" />					
                            </a>
                        </div>			
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                        <div class="gallery-grid1">
                            <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/type-b2.jpg">
                                <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/type-b2.jpg" alt=" " class="img-responsive" />					
                            </a>
                        </div>			
                    </div>
                </div>

			</div>
		</div>
	</div>
	<!-- //projects -->	


    	<!-- contact -->
	<div class="address" id="contact">
		<div class="container">
			<h3 class="title">Contact Us</h3>
			<div class="address-row">
                 <div class="col-md-12 col-xs-12  wow agile fadeInLeft animated" data-wow-delay=".5s">
					<div class="address-info wow fadeInDown animated" data-wow-delay=".5s">
						<h4>Map</h4>
						<img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/map-img.jpg" />
					</div>
				</div>
				<div class="col-md-12 col-xs-12 address-right">
					<div class="address-info wow fadeInRight animated" data-wow-delay=".5s">
						<h4>Address</h4>
						<p>Ozone Urbana Prime,<br> Bangalore - Hyderabad Hwy,<br> Yerthiganahalli,Udayagiri, Yerthiganahalli,Bengaluru,<br> Karnataka-562110</p>
					</div>
					
				
				</div>
				<div class="col-md-12 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
					<div class="address-grid">
						<!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
						<div id="location">
                       
                            <iframe style="border:0;width: 100%;height: 300px;"  src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7768.702037737028!2d77.663598!3d13.203265!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8e71363ac809599c!2sOzone+Urbana+Prime!5e0!3m2!1sen!2sin!4v1540274739354"  allowfullscreen></iframe>
                        </div>
					</div>
				</div>
			
			</div>
		</div>
	</div>
	<!--//contact-->
    <footer>
		<div class="copy-right-grids">
			<p class="footer-gd">© 2018  Ozone Group . All rights reserved.| Maintained by Adcanopus</p>
		</div>
	</footer>    
	<div class="floating-form visiable" id="contact_form">
	<div class="contact-opener">Enquire Now</div>
    <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="ozone_jsfrm('<?php echo SITE_URL?>realestate/ozone/submit_frm','ozone-urbana','2')">
    
    <div class="">       
                    <div class="col text-center">
                             <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>185 acres Integrated Township adjacent to Bengaluru International Airport</h4>
                            </div> 
                            <div class="col">     
                    <div class="groups">
                        <div class="form-group">
                       
                            <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err2"></span>
                        </div>
              
                    </div>  
                    <div class="groups">
                    <div class="form-group">
                   
                    <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                    <span class="help-block" id="email_err2" ></span>
                    </div>
                    <div class="form-group">
                    <input type="hidden" class="Countryname" name="Countryname" value="in" id="Countryname2">
					<input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode2">
                    <input type="text" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                    <span class="help-block" id="phone_err2"> </span>
				    <span class="help-block" id="CountryCode_err"> </span>
                    </div>
                 
                    </div>      
                
                         
                
                    <div class="submitbtncontainer">
                    <input type="submit" id="frm-sbmtbtn2" value="Submit" name="submit">
                    </div>
                    </div>
                    </div>
                </form>	
				<div>
    <div class="popup-enquiry-form mfp-hide" id="popupForm">
    <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="ozone_jsfrm('<?php echo SITE_URL?>realestate/ozone/submit_frm','ozone-urbana','3')">
    
     <div class="">       
                    <div class="col">
                             <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/images/logo-small.png" />
                                </a> 
                            </div>
                            <h4>185 acres Integrated Township adjacent to Bengaluru International Airport</h4>
                            </div> 
                            <div class="col">
                    <div class="groups">
                        <div class="form-group">
                       
                            <input type="text" class="form-control" id="name3" name="name" placeholder="Name" onkeyup="chck_valid('name', 'Please enter correct name')" data-attr="Please enter correct name">
                            <span class="help-block" id="name_err3"></span>
                        </div>
						<div class="form-group">
                   
				   <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
				   <span class="help-block" id="email_err3" ></span>
				   </div>
                    </div>  
                    <div class="groups">
                  
                    <div class="form-group">
                    <input type="hidden" class="Countryname" name="Countryname" value="in" id="Countryname3">
					<input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode3">
                    <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                    <span class="help-block" id="phone_err3"> </span>
				    <span class="help-block" id="CountryCode_err"> </span>
                    </div>
         
                    </div>      
                
                  
                
                    <div class="submitbtncontainer">
                    <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                    </div>
                    </div>
                    </div>
                </form>

								 </div>

	<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
	<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
	<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
	<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
	<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
	
	
	<script src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/js/vendor.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>  
    <script src="<?php echo S3_URL?>/site/realestate-assets/ozone-urbana/js/main.js"></script>
	
	 <script src="<?php echo S3_URL?>/site/scripts/ozone.js"></script>
   </body>
</html>