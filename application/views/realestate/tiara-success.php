<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title>Vajram - Tiara</title>
      <meta name="description" content="Vajram Tiara is a veritable paradise of living space located in Yelahanka">
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/tiara/images/favicon.png" type="image/x-icon" />


	  <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/tiara/css/vendor.css">
	  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/tiara/css/main.css">    
      <!-- fonts -->
      <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
      <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
            rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
   </head>
   <body>

 <?php $all_array = all_arrays(); ?>      
        <!-- <div class="header-top">
            <p>
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                Chokkanahalli Thanisandra Main Road, Yelahanka Hobli, Bengaluru-560064
            </p>
        </div> -->
	<!-- banner -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="">
					<div class="carousel-caption">
                    <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="vajram_jsfrm('<?php echo SITE_URL?>realestate/vajram/submit_frm','tiara','1')">
                   
                
                    <div class="">       
                    <div class="col">
                           <div class="form-logo">
                                <a href="#">
                                    <img src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/logo-small.png" />
                                </a> 
                            </div> 
                            <h4>Thank you for expressing interest on our Properties
                            Our expert will get in touch with you shortly.</h4>
                            
                            </div> 
                        
                    </div>
                </form>
					</div>
				</div>
			</div>
		</div>

		<!-- The Modal -->
	</div>
    <!--//banner -->      
	<!-- about -->
	<div class="about" id="about">
		<div class="container">
			<div class="col-md-6 about-left">
             <div class="col-xs-12 aboutimg-w3l aboutimg-w3l2">
             <div class="rwd-media">
                            <iframe src="https://www.youtube.com/embed/FvuWr9ICIwk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>
				</div>
				<!-- <div class="col-xs-6 aboutimg-w3l">
					<img src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/ab3.jpg" alt="image" />
				
				</div> -->
				<div class="clearfix"> </div>
			</div>
			<div class="col-md-6 about_right">
				<h3>ABOUT</h3>
				<h3 class="bold">Vajram Tiara</h3>
				
                <p>
                Vajram Tiara is a veritable paradise of living space located in Yelahanka, (North Bangalore). A premium housing property with over 237 spacious 3 and 4 BHK condominiums. Vajram Tiara is an amalgamation of modern architecture and innovative design. It offers you a 24,000 sq. ft clubhouse spread across an acre of land with cafe, library, gymnasium and many more attractive amenities. Apart from the premium amenities, Vajram offers unique recreational amenities such as an aroma park, acupressure walkways, butterfly garden, spa, open air barbeque area and lots more. It’s well connected to several educational institutes, malls, It companies, etc. With Vajram Tiara, you are sure to fall in love with life.
                </p>    
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<!-- //about -->

    <div class="gallery" id="projects">
		<div class="container">
            <h3 class="title">Project Highlights</h3>
            <ul style="list-style: inside;">
                <li>Nearby Education centres: Mallya Aditi International School, Ryan International Scool, Canadian International School, NITTE Internationa School</li>
                <li>Nearby Colleges: BMS Institute of Technology, M.E.C. Junior, Bishop Cotton Academy of Professional Mang, Seshadripuram First Grade College</li>
                <li>Nearby Hospitals: Columbia Asia Hospital, Narayana Hrudalaya Dental Hospital, M.S Ramaiah Memorial Hospital, Cauvery Medical Centre</li>
                <li>Yelahanka Railway Station: 05 km</li>
                <li>Esteem Mall: 10km</li>
            </ul>
        </div>
    </div>        
    	<!-- projects -->
	<div class="gallery" id="projects">
		<div class="container">
        <h3 class="title">Amenities</h3>
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Club House" href="<?php echo S3_URL?>/site/realestate-assets/tiara/images/a1.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/a1.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Club House</h4>
							</div>
						</a>
					</div> 
					<div class="gallery-grid1">
						<a title="Sports & Health" href="<?php echo S3_URL?>/site/realestate-assets/tiara/images/a2.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/a2.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Sports & Health</h4>
							</div>
						</a>
                    </div>
        
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Leisure & Lifestyle" href="<?php echo S3_URL?>/site/realestate-assets/tiara/images/a3.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/a3.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Leisure & Lifestyle</h4>
							</div>
						</a>
                    </div>
                    <div class="gallery-grid1">
						<a title="Kids & Security" href="<?php echo S3_URL?>/site/realestate-assets/tiara/images/a4.jpg">
							<img src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/a4.jpg" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>Kids & Security</h4>
							</div>
						</a>
                    </div>              
				</div>               
			</div>
		</div>
        <div class="container">
            
            <div class="row">
        <div class="col-md-12">
            <!-- begin panel group -->
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                
                <!-- panel 1 -->
                <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#tab1" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingOne"data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <h4 class="panel-title">Club House</h4>
                        </div>
                    </span>
                    
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                        <!-- Tab content goes here -->
                            <ul class="tab-pane-inner-ul">
                            <li>Banquet Hall</li>
                                    <li>Squash Court and Badminton Court</li>
                                    <li>Half Olympic size Swimming Pool with deck</li>
                                    <li>Cafe area with Pantry</li>
                                    <li>3000 sft Indoor Gymnasium and Aerobics</li>
                                    <li>Open air Barbecue</li>
                                    <li>Library with attached terrace</li>
                                    <li>Indoor games room</li>
                                    <li>Business Centre and much more…</li>
                            </ul>
                        </div>
                    </div>
                </div> 
                <!-- / panel 1 -->
                
                <!-- panel 2 -->
                <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#tab2" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <h4 class="panel-title collapsed">Sports & Health</h4>
                        </div>
                    </span>

                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                        <!-- Tab content goes here -->
                        <ul class="tab-pane-inner-ul">
                            <li>Jogging Track and Skating Arena</li>
                            <li>Half Basket ball court</li>
                            <li>Lawn Tennis and Badminton court</li>
                            <li>Beach Volley Ball</li>
                            <li>Cricket practice Net</li>
                            <li>Meditation / Yoga Garden</li>
                            <li>Table Tennis</li>
                            <li>Cascade Wall</li>
                        </ul>
                        </div>
                    </div>
                </div>
                <!-- / panel 2 -->
                
                <!--  panel 3 -->
                <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#tab3" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingThree"  class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <h4 class="panel-title">Leisure & Lifestyle</h4>
                        </div>
                    </span>

                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">
                          <!-- tab content goes here -->
                            <ul class="tab-pane-inner-ul">
                            <li>Old folks Area</li>
                                    <li>Aromatic garden</li>
                                    <li>Butterfly Garden</li>
                                    <li>Water feature</li>
                                    <li>Spa</li>
                                    <li>AV room</li>
                                    <li>Acupressure Walkway</li>
                                    <li>Outdoor Gym</li>
                                    <li>Cards area</li>
                            </ul>
                          </div>
                        </div>
                      </div>


              
                <!-- panel 2 -->
                <div class="panel panel-default">
                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                    <span class="side-tab" data-target="#Living" data-toggle="tab" role="tab" aria-expanded="false">
                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#LivingRoom" aria-expanded="false" aria-controls="LivingRoom">
                            <h4 class="panel-title collapsed">Kids & Security</h4>
                        </div>
                    </span>

                    <div id="LivingRoom" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Living">
                        <div class="panel-body">
                        <!-- Tab content goes here -->
                            <ul class="tab-pane-inner-ul">
                                <li>Children's Play area</li>
                                <li>Creche</li>
                                <li>Shaded Hobby Area</li>
                                <li>Study Room</li>
                                <li>Wifi Connectivity</li>
                                <li>CCTV Coverage</li>
                                <li>Video Intercom Facility</li>
                                <li>Fibre Connectivity</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- / panel 2 -->
            </div> <!-- / panel-group -->
             
        </div> <!-- /col-md-4 -->

    </div> <!--/ .row -->
        </div>   
	</div>
    <!-- //projects -->

  

	<!-- projects -->
	<div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">GALLERY</h3>
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
                        <a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/tiara/images/p1.jpg">
                            <div class="stack twisted">
                                <img  src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/p1.jpg" alt=" " class="img-responsive" />
                            </div>
							
						</a>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/tiara/images/p2.jpg">
                        <div class="stack twisted">    
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/p2.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
                    </div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/tiara/images/p3.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/p3.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/tiara/images/p4.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/p4.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>
                <div class="col-md-4 col-sm-4 col-xs-4 gal-sec">
					<div class="gallery-grid1">
						<a title="Vajram Tiara" href="<?php echo S3_URL?>/site/realestate-assets/tiara/images/p5.jpg">
                        <div class="stack twisted">
                        <img  src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/p5.jpg" alt=" " class="img-responsive" />
							
							</div>
						</a>
					</div>
				</div>

			</div>
		</div>
	</div>
    <!-- //projects -->      
 	<!-- projects -->
	 <div class="gallery" id="projects">
		<div class="container">
			<h3 class="title">Plan</h3>
			<div class="agile_gallery_grids w3-agile demo">
				<div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/tiara/images/masterplan.jpg">
							<img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/masterplan.jpg" alt=" " class="img-responsive" />					
						</a>
					</div>			
				</div>
                <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/tiara/images/typical_plan_tiara.jpg">
							<img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/typical_plan_tiara.jpg" alt=" " class="img-responsive" />					
						</a>
					</div>			
				</div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/tiara/images/type-a.jpg">
							<img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/type-a.jpg" alt=" " class="img-responsive" />					
						</a>
					</div>			
				</div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/tiara/images/type-b.jpg">
							<img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/type-b.jpg" alt=" " class="img-responsive" />					
						</a>
					</div>			
				</div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/tiara/images/type-b1.jpg">
							<img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/type-b1.jpg" alt=" " class="img-responsive" />					
						</a>
					</div>			
				</div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
					<div class="gallery-grid1">
						<a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/tiara/images/type-b2.jpg">
							<img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/type-b2.jpg" alt=" " class="img-responsive" />					
						</a>
					</div>			
				</div>

			</div>
		</div>
	</div>
	<!-- //projects -->	


    	<!-- contact -->
	<div class="address" id="contact">
		<div class="container">
			<h3 class="title">Contact Us</h3>
			<div class="address-row">
                 <div class="col-md-12 col-xs-12  wow agile fadeInLeft animated" data-wow-delay=".5s">
					<div class="address-info wow fadeInDown animated" data-wow-delay=".5s">
						<h4>Map</h4>
						<img src="<?php echo S3_URL?>/site/realestate-assets/tiara/images/map-img.jpg" />
					</div>
				</div>
				<div class="col-md-12 col-xs-12 address-right">
					<div class="address-info wow fadeInRight animated" data-wow-delay=".5s">
						<h4>Address (Corporate Office)</h4>
						<p>Sy No. 61,Shri Vijayaraja Estate,<br> Chokkanahalli Thanisandra Main Road,<br> Yelahanka Hobli, Bengaluru,<br> Karnataka-560064</p>
					</div>
					
				
				</div>
				<div class="col-md-12 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
					<div class="address-grid">
						<!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
						<div id="location">
                       
                            <iframe style="border:0;width: 100%;height: 300px;"  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3885.3911152457167!2d77.56719311418902!3d13.137711590746981!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae18af6c6a2d55%3A0xe103fe1ebc7c62d7!2sVajram+Tiara!5e0!3m2!1sen!2sin!4v1538111542127"  allowfullscreen></iframe>
                        </div>
					</div>
				</div>
			
			</div>
		</div>
	</div>
	<!--//contact-->
    <footer>
		<div class="copy-right-grids">
			<p class="footer-gd">© 2018 vajramgroup. All rights reserved.| Maintained by Adcanopus</p>
		</div>
	</footer>    
 <div id="pi">
       <?php
	   $vr = $this->session->userdata('user_id');
	   $ut = $this->session->userdata('utm_source');
	   if( isset($vr) &&  $vr > 0 && $ut != ""){
		   echo $this->lead_check->set_pixel($vr, VAJRAM);
		   
	   }
	   ?>
      </div> 

	<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
	<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
	<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
	<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
	<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
	
	
	<script src="<?php echo S3_URL?>/site/realestate-assets/tiara/js/vendor.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
	<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>  
    <script src="<?php echo S3_URL?>/site/realestate-assets/tiara/js/main.js"></script>
	
	 <script src="<?php echo S3_URL?>/site/scripts/vajram.js"></script>
   </body>
</html>