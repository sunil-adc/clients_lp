<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Marathahalli-Sarjapur Outer Ring Road, bang opposite RMZ Ecospace in Bellandur, is Sterling Ascentia</title>
    <meta name="description" content="Find luxury flats in Bellandur at unbeatable prices. Visit Sterling Ascentia for 3bhk apartments in Bellandur from 90 lakhs onwards with top luxury amenities.">
    <meta name="keywords" content="Sterling Ascentia, Flats in Outer ring road, Apartments in Outer ring road , apartments in bellandur,  flats in bellandur, 3bhk on outer ring road">
    <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/favicon.png" type="image/x-icon" />


    <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/css/vendor.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
    <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/css/main.css">
    <!-- fonts -->
    <link href="//fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
        rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
</head>

<!-- DO NOT MODIFY -->
<!-- Quora Pixel Code (JS Helper) -->
<script>
!function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
qp('init', '47c1ea0be1b44f6ea0a23037b1df3e30');
qp('track', 'ViewContent');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/47c1ea0be1b44f6ea0a23037b1df3e30/pixel?tag=ViewContent&noscript=1"/></noscript>
<!-- End of Quora Pixel Code -->

<script>qp('track', 'GenerateLead');</script>


<body>

    <?php $all_array = all_arrays(); ?>

    <!-- banner -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="header-banner header-banner-desktop">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/desktop-banner.jpg" />
                </div>
                <div class="header-banner header-banner-mobile">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/mobile-banner.jpg" />
                </div>
                <div class="">
                    <div class="carousel-caption">
                        <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="prestige_jsfrm('<?php echo SITE_URL?>realestate/prestige/frm_submit','prestige-lake-ridge-adcanopus','1')">

                            <div class="">
                                <div class="col">
                                    <div class="form-logo">
                                        <a href="#">
                                            <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/logo-blue.png" />
                                        </a>
                                    </div>
                                    <h4>Thank you for expressing interest on our Properties
                            Our expert will get in touch with you shortly.</h4>   

                                </div>
                             
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal -->
    </div>
    <!--//banner -->

    <!-- about -->
    <div class="about" id="about">
        <div class="container">
            <div class="col-md-6 about-left">
                <div class="col-xs-12 aboutimg-w3l aboutimg-w3l2">
                    <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/ab5.jpg" alt="image" style="object-position: center;" />
                </div>
                <!-- <div class="col-xs-6 aboutimg-w3l">
					<img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/ab3.jpg" alt="image" />
				
				</div> -->
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-6 about_right">
                <h3>LIFE IS</h3>
                <h3 class="bold">JUST AROUND THE CORNER</h3>
                <p>
                Situated amidst the hub of life on the Marathahalli-Sarjapur Outer Ring Road, bang opposite RMZ Ecospace in Bellandur, is Sterling Ascentia. Home to 392 luxurious 3-BHK apartments, Sterling Ascentia is unlike any of the other properties in Bangalore and is thoughtfully located to help urban Bangaloreans get a life. Here, everything - from offices to malls and restaurants - is just around the corner, so you don’t have to spend your life getting stuck in things you can’t control - like traffic. Instead, you actually get a life.
                </p>
            </div>
            <div class="clearfix"> </div>
            <p>Site Address: #13, Bellandur, Sarjapura-Marathahalli Outer Ring Road, Bangalore - 560 103. Landmark: Opp. Ecospace</p>
            <p class="highlight-text">3 BHK APARTMENTS 392 UNITS |6 ACRES |1591 SFT - 2015 SFT *. ( 1 sq.mtr.=10.764 sq.ft. )</p>
            <p class="text-right">RERA REG. NO. PRM/KA/RERA/1251/446/PR/171026/000563</p>
        </div>
    </div>
    <!-- //about -->
     <!-- popular -->
     <div class="popular-w3" id="popular">
        <div class="container">
            <!-- <h3 class="title">Location Video</h3> -->
            <div class="popular-grids">
                <div class="">

                    <div class="popular-text">
                        <div class="detail-bottom">

                            <div class="rwd-media">
                            <iframe src="https://www.youtube.com/embed/QVLOU2y8AIc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                           
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div>
    <!-- //popular -->
    <div class="gallery highlight" id="projects">
        <div class="container">
            <h3 class="title">Highlights</h3>

            <div class="row">
                <div class="col-md-12">
                    <div class="desktop-only">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/price_pc.png" alt=" "
                                    class="img-responsive" />    
                    </div>
                    <div class="mobile-only">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/price_mobile.png" alt=" "
                                    class="img-responsive" />    
                    </div>
                </div> <!-- /col-md-4 -->

            </div>
            <!--/ .row -->
        </div>
    </div>

    <!-- projects -->
    <div class="gallery popular-w3" id="projects">
        <div class="container">
            <h3 class="title">Ammenities</h3>
            <div class="agile_gallery_grids w3-agile amenities-block demo">
				<div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/swimming.png" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>SWIMMING POOL</h4>
							</div>
					</div> 

				</div>
				<div class="gal-sec">
					<div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/Jogging.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>JOGGING TRACK</h4>
                        </div>
                    </div>            
				</div>
				<div class="gal-sec">
			
                    <div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/TT.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>TABLE TENNIS</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/study.png" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>LIBRARY</h4>
							</div>
					</div> 

				</div>
				<div class="gal-sec">
					<div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/grand-club.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>COMMUNITY HALL</h4>
                        </div>
                    </div>            
				</div>
				<div class="gal-sec">
			
                    <div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/fitness-station.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>GYM</h4>
                        </div>
					</div>                  
                </div>
                <div class="gal-sec">
					<div class="gallery-grid1">
                        <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/kids-play-zone.png" alt=" " class="img-responsive" />
							<div class="p-mask">
								<h4>PLAY AREA</h4>
							</div>
					</div> 

				</div>
				<div class="gal-sec">
					<div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/squash-court.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>TENNIS COURT</h4>
                        </div>
                    </div>            
				</div>
				<div class="gal-sec">
			
                    <div class="gallery-grid1">
                         <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/basketball.png" alt=" " class="img-responsive" />
                        <div class="p-mask">
                            <h4>BASKETBALL COURT</h4>
                        </div>
					</div>                  
                </div>
               

			</div>
        </div>
    </div>
    <!-- //projects -->

    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">GALLERY</h3>
            <div class="agile_gallery_grids w3-agile demo">

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Sterling Ascentia" href="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/gallery1.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/gallery1.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Sterling Ascentia" href="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/gallery2.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/gallery2.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Sterling Ascentia" href="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/gallery3.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/gallery3.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Sterling Ascentia" href="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/gallery4.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/gallery4.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Sterling Ascentia" href="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/gallery5.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/gallery5.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Sterling Ascentia" href="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/gallery6.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/gallery6.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Sterling Ascentia" href="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/gallery7.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/gallery7.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Sterling Ascentia" href="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/gallery8.jpg">
                            <div class="stack twisted">
                                <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/gallery8.jpg" alt=" "
                                    class="img-responsive" />
                            </div>
                        </a>
                    </div>
                </div>



            </div>
        </div>
    </div>
    <!-- //projects -->
    <!-- projects -->
    <div class="gallery" id="projects">
        <div class="container">
            <h3 class="title">Master Plan</h3>
            <div class="agile_gallery_grids w3-agile demo">
                <div class="col-md-12 col-sm-12 col-xs-12 gal-sec">
                    <div class="gallery-grid1">
                        <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/masterplan.jpg">
                            <img style="height: auto;" src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/masterplan.jpg"
                                alt=" " class="img-responsive" />
                        </a>
                    </div>
                </div>
           




            </div>
        </div>
    </div>
    <!-- //projects -->


    <!-- contact -->
    <div class="address" id="contact">
        <div class="container">
            <h3 class="title">Contact Us</h3>
            <div class="address-row">
            <div class="col-md-12 col-xs-12  wow agile fadeInLeft animated" data-wow-delay=".5s">
                    <div class="address-info wow fadeInDown animated gallery-grid1" data-wow-delay=".5s">
                        <!-- <h4>Map</h4> -->
                        <a title="Project Plan" href="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/map-img.jpg">
                            <img src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/images/map-img.jpg" />
                        </a>
                    </div>
                </div>
                <div class="col-md-12 col-xs-12 address-left wow agile fadeInLeft animated" data-wow-delay=".5s">
                    <div class="address-grid">
                        <!-- <h4 class="wow fadeIndown animated" data-wow-delay=".5s">Find in Map</h4> -->
                        <div id="location">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d31109.363588746157!2d77.681633!3d12.92889!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa881da3b46c89c55!2sSterling+Ascentia!5e0!3m2!1sen!2sin!4v1552297975079"  style="border:0;width: 100%;height: 350px;" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
    <!--//contact-->
    <footer>
     
        <div class="copy-right-grids">
            <p class="footer-gd">© 2019 Sterling Ascentia. All rights reserved.</p>
        </div>
    </footer>
     <div id="pi">
       <?php
       $vr = $this->session->userdata('user_id');
       $ut = $this->session->userdata('utm_source');
       if( isset($vr) &&  $vr > 0 && $ut != ""){
           echo $this->lead_check->set_pixel($vr, REALESTATE_USER);
           
       }
       ?>
      </div> 




    <script src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/js/vendor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/sterlingascentia/js/main.js"></script>
    
</body>

</html>