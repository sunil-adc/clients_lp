<!doctype html>
<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<link rel="icon" href="https://www.mantri.in/images/favicon.ico"  sizes="16x16">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Residential Apartment Projects Bengaluru from Mantri Developers</title>
<link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/mantri/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/mantri/css/style.css">
<script src="<?php echo S3_URL?>/site/realestate-assets/mantri/js/jquery.min.js"></script>
<script src="<?php echo S3_URL?>/site/realestate-assets/mantri/js/bootstrap.min.js"></script>
</head>
<body>
<div class="section">
  <div class="container-fluid">
    <div class="row bgred">
      <div class="col-md-12">
        <div id="myCarousel" class="carousel slide"> 
          <!-- Indicators -->
          <ul class="carousel-indicators">
            <li class="item1 active"></li>
            <li class="item2"></li>
            <li class="item3"></li>
          </ul>
          
          <!-- The slideshow -->
          <div class="carousel-inner carousel-inner-2">
            <div class="carousel-item active">
              <div class="carousal-project-detail"> <span class="p_name">
                <h5>MANTRI</h5>
                <h1>ENERGIA</h1>
                <span class="p_flats">3 BHK</span> </span> <span class="p_offer">Price : ₹92 LACS* ONWARDS</span> <span class="p_cote">Your future-ready home in the biggest project inside the biggest Tech Park, in the biggest investment hotspot of North Bangalore</span> </div>
              <img src="<?php echo S3_URL?>/site/realestate-assets/mantri/images/energia_banner.jpg" alt="New york" style="width:100%;"> </div>
			  
            <div class="carousel-item">
              <div class="carousal-project-detail"> <span class="p_name">
                <h5>MANTRI</h5>
                <h1>LITHOS</h1>
                <span class="p_flats">2 & 3 BHK</span> </span> <span class="p_offer">OFFER PRICE : ₹1.21 Cr* ONWARDS</span> <span class="p_cote">Ready to occupy apartments with a unique elevation and located amongst the futuristic infrastructure of Manyata Techpark</span> </div>
              <img src="<?php echo S3_URL?>/site/realestate-assets/mantri/images/lithos_banner.jpg" alt="New york" style="width:100%;"> </div>
			  
            <div class="carousel-item"> 
              <div class="carousal-project-detail"> <span class="p_name">
                <h5>MANTRI</h5>
                <h1>Serenity</h1>
                <span class="p_flats">2 , 3 BHK </span> </span> <span class="p_offer">Price : ₹73 LACS* ONWARDS</span> <span class="p_cote">A futuristic township, with large open spaces and an array of amenities, located just on the highway. </span> </div>
				<img src="<?php echo S3_URL?>/site/realestate-assets/mantri/images/Serenity_banner.jpg" alt="New york" style="width:100%;">
            </div>
			  
          </div>
          <!-- Left and right controls --> 
          <a class="carousel-control-prev" href="#myCarousel"> <span class="carousel-control-prev-icon"></span> </a> <a class="carousel-control-next" href="#myCarousel"> <span class="carousel-control-next-icon"></span> </a> </div>
      </div>
    </div>
  </div>
</div>
<section class="wrapper style1">
  <div class="inner">
    <article class="feature left"> <span class="image"><img src="<?php echo S3_URL?>/site/realestate-assets/mantri/images/energia.jpg" alt="" /></span>
      <div class="content">     
      <img src="<?php echo S3_URL?>/site/realestate-assets/mantri/images/energia_logo.png" alt="" style="float: right; top: 0; width: 30%" />
       <span style="text-transform: uppercase; font-size: 18px;">Mantri</span>
        <h2>Energia</h2>
        <p><strong>Configurations : </strong> 3 BHK</p>
        <p><strong>Location : </strong> Inside Manyata Tech Park</p>
        <p><strong>Budget : </strong> Starting @ ₹92 Lacs*</p>
        <ul>
          <li>Solarium - the clubhouse: 30,000 sq. Ft.  Grand clubhouse – the centre of life.</li>
          <li>Tree of life – power generating energy tree </li>
          <li>Tunnel form – a new age construction system </li>
          <li>Rainforest type garden </li>
          <li>Live work pod</li>
        </ul>
        <div class="rera_numebr"> RERA REGISTRATION NOS.<br>
          ENERGIA - PRM/KA/RERA/1251/309/PR/171014/000439 </div>
        <div class="col-10">
          <div class="row">
            <div><br>
              <a  class="button btn-default-2 anchorLink" onClick = "select_project('mantri_manyata_energia');">Enquire Now</a> </div>
          </div>
        </div>
      </div>
    </article>
    <article class="feature right"> <span class="image"> <img src="<?php echo S3_URL?>/site/realestate-assets/mantri/images/lithos.jpg" alt="" /></span>
     
      <div class="content">
      <img src="<?php echo S3_URL?>/site/realestate-assets/mantri/images/lithos_logo.png" alt="" style="float: right; top: 0; width: 30%" />
       <span style="text-transform: uppercase; font-size: 18px;">Mantri</span>
        <h2>Lithos</h2>
        <p><strong>Configurations : </strong> 2 &amp; 3 BHK</p>
        <p><strong>Location : </strong> Inside Manyata Tech Park</p>
        <p><strong>Budget : </strong> Starting @ ₹1.21 Cr*</p>
        <ul>
          <li>Unique architecture inspired by natural rock formations </li>
          <li>Green building features </li>
          <li>Undulating roofs cape with terrace penthouses.</li>
          <li>Distinctive angular form of the building compliments the name lithos, meaning rocky edges.</li>
          <li>Naturally ventilated & well lit designer apartments with contemporary looks, designed by international architects.</li>
          <li>One of its kind of high end Clubhouse which is literally submerged with green/landscaped roof.</li>
        </ul>
        <div class="rera_numebr"> RERA REGISTRATION NOS. <br>
          LITHOS - PRM/KA/RERA/1251/309/PR/171201/000444 </div>
        <div class="col-10">
          <div class="row">
            <div><br>
              <a   class="button btn-default-2 anchorLink" onClick = "select_project('mantri_manyata_lithos');">Enquire Now</a> </div>
          </div>
        </div>
      </div>
    </article>
  </div>
</section>
<section class="wrapper style1">
  <div class="inner">
    <article class="feature left"> <span class="image"><img src="<?php echo S3_URL?>/site/realestate-assets/mantri/images/Serenity.jpg" alt="" /></span>
      <div class="content"> 
       <img src="<?php echo S3_URL?>/site/realestate-assets/mantri/images/Serenity_logo.png" alt="" style="float: right; top: 0; width: 30%" />
       <span style="text-transform: uppercase; font-size: 18px;">Mantri</span>
        <h2>Serenity</h2>
        <p><strong>Configurations : </strong> 2 , 3 BHK</p>
        <p><strong>Location : </strong>Kanakapura Main Road</p>
        <p><strong>Budget : </strong> Starting @ ₹73 Lacs*</p>
        <ul>
          <li> 70000 Sq. Ft. of Clubhouse - The Lotus </li>
          <li> Indoor temperature controlled Swimming pool with Jacuzzi </li>
          <li> Aqua Gym </li>
          <li> Concierge Services </li>
          <li> Telemedicine </li>
          <li> Close Proximity to Metro station & Mantri Arena Mall </li>
        </ul>
        <div class="rera_numebr"> RERA REGISTRATION NOS. <br>
          Block 1 - PR/KN/170731/000494, 
          Block 3 - PR/KN/170731/000500, 
          Block 4 - PR/KN/170731/000502, 
          Block 5 - PR/KN/170731/000504,</div>
        <div class="col-10">
          <div class="row">
            <div><br>
              <a   class="button btn-default-2 anchorLink" onClick = "select_project('mantri_Serenity');">Enquire Now</a> </div>
          </div>
        </div>
      </div>
    </article>
  </div>
</section>
<!--
   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="text-left" id="myModalLabel">Enquire Now</h4>
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>                    
        </div>
        <div class="modal-body">
            <form method="post" action="javascript:void(0)" onsubmit="mantri_jsfrm('<?php echo SITE_URL?>realestate/mantri/submit_frm','2','mantri')" id="frm" name="frm">
              
              <div class="form-group">
                <input type="text" class="form-control" id="name2" name="name" placeholder="Your Name" onkeyup="chck_valid('name', 'Please enter correct name')">
                <span class="help-block" id="name_err2"></span>
              </div>
              <div class="form-group">
                <input type="email" class="form-control" id="email2" name="email" placeholder="Your email" onkeyup="chck_valid('email', 'Please enter correct email')">
                <span class="help-block" id="email_err2" ></span>
              </div>
              <div class="form-group">
                <input type="text" class="form-control only_numeric" id="phone2" name="phone" pattern="\d*" maxlength="10" placeholder="Your Telephone" onkeyup="chck_valid('phone', 'Please enter correct phone number')">
                <span class="help-block" id="phone_err2"> </span>
              </div>
              <div class="form-group">
                <select class="form-control" id="lp_name2" name="lp_name" onchange="chck_valid('city', 'Please enter correct city')">
                  <option value="" disabled="" selected="">Select Project</option>
                  <option value="mantri_manyata_energia">Mantri Manyata Energia</option>
                  <option value="mantri_manyata_lithos">Mantri Manyata Lithos</option> 
                  <option value="mantri_Serenity">Mantri Serenity</option>
                </select>
                <span class="help-block" id="lp_name_err2"> </span>
              </div>
              <input type="submit" class="btn btn-default" id="frm-sbmtbtn2" value="Submit">
            </form>
        </div> 
      </div>
    </div>
  </div>
 -->

<section class="wrapper" style="background:#b12537">
  <div class="container text-center" id="sendquery">
    <div class="col-md-6 footer_form">
      <div class="form"> <img src="<?php echo S3_URL?>/site/realestate-assets/mantri/images/mantri_logo.png" class="rounded responsive-img">
        <form method="post" action="javascript:void(0)" onsubmit="mantri_jsfrm('<?php echo SITE_URL?>realestate/mantri/submit_frm','1','mantri')" id="frm" name="frm">
          <h2  class="heading-h2">Enquire Now</h2>
          <div class="form-group">
            <input type="text" class="form-control" id="name1" name="name" placeholder="Your Name" onkeyup="chck_valid('name', 'Please enter correct name')">
            <span class="help-block" id="name_err1"></span> </div>
          <div class="form-group">
            <input type="email" class="form-control" id="email1" name="email" placeholder="Your email" onkeyup="chck_valid('email', 'Please enter correct email')">
            <span class="help-block" id="email_err1" ></span> </div>
          <div class="form-group">
            <input type="text" class="form-control only_numeric" id="phone1" name="phone" pattern="\d*" maxlength="10" placeholder="Your Telephone" onkeyup="chck_valid('phone', 'Please enter correct phone number')">
            <span class="help-block" id="phone_err1"> </span> </div>
          <div class="form-group">
            <select class="form-control" id="lp_name1" name="lp_name" onchange="chck_valid('city', 'Please enter correct city')">
              <option value="" disabled="" selected="">Select Project</option>
              <option value="Mantri Energia">Mantri Energia</option>
              <option value="Mantri Lithos">Mantri Lithos</option>
              <option value="Mantri Serenity">Mantri Serenity</option>
            </select>
            <span class="help-block" id="lp_name_err1"> </span> </div>
          <input type="submit" class="btn btn-default" id="frm-sbmtbtn1" value="Submit">
          <br>
          <br>
        </form>
      </div>
    </div>
  </div>
</section>
<footer class="footer">
  <div class="section">
    <div class="container-fluid space"> </div>
  </div>
  <div class="container text-center">
    <div class="large-12 medium-12 small-12">
      <p style="color: #999">© 2019 Mantri Developers. All Rights Reserved.</p>
    </div>
  </div>
  <div class="section">
    <div class="container-fluid space"> </div>
  </div>
</footer>
<div class="floating-form visiable" id="contact_form">
  <div class="contact-opener"> Enquire Now </div>
  <form method="post" action="javascript:void(0)" id="feedbackForm"  class="feedbackForm" onsubmit="mantri_jsfrm('<?php echo SITE_URL?>realestate/mantri/submit_frm','2','mantri')" id="frm" name="frm">
  <div class="col text-center">
    <div class="form-logo"> <a href="#"><img src="<?php echo S3_URL?>/site/realestate-assets/mantri/images/logo.jpg" 
				/></a> </div>
  </div>
  <div class="col">
    <div class="groups">
      <div class="form-group">
        <input type="text" class="form-control" id="name2" name="name" placeholder="Your Name" onkeyup="chck_valid('name', 'Please enter correct name')">
        <span class="help-block" id="name_err2"></span> </div>
      <div class="form-group">
        <input type="email" class="form-control" id="email2" name="email" placeholder="Your email" onkeyup="chck_valid('email', 'Please enter correct email')">
        <span class="help-block" id="email_err2" ></span> </div>
      <div class="form-group">
        <input type="text" class="form-control only_numeric" id="phone2" name="phone" pattern="\d*" maxlength="10" placeholder="Your Telephone" onkeyup="chck_valid('phone', 'Please enter correct phone number')">
        <span class="help-block" id="phone_err2"> </span> </div>
      <div class="form-group">
        <select class="form-control" id="lp_name2" name="lp_name" onchange="chck_valid('city', 'Please enter correct city')">
          <option value="" disabled="" selected="">Select Project</option>
          <option value="Mantri Energia">Mantri Energia</option>
          <option value="Mantri Lithos">Mantri Lithos</option>
          <option value="Mantri Serenity">Mantri Serenity</option>
        </select>
        <span class="help-block" id="lp_name_err2"> </span> </div>
      <input type="submit" class="btn btn-default" id="frm-sbmtbtn2" value="Submit">
      <br>
      <br>
    </div>
  </div>
  </form>
</div>
<input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
<input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
<script src="<?php echo S3_URL?>/site/scripts/mantri.js"></script> 
<script>
$(document).ready(function(){
  // Activate Carousel
  $("#myCarousel").carousel();
    
  // Enable Carousel Indicators
  $(".item1").click(function(){
    $("#myCarousel").carousel(0);
  });
  $(".item2").click(function(){
    $("#myCarousel").carousel(1);
  });
  $(".item3").click(function(){
    $("#myCarousel").carousel(2);
  });
    
  // Enable Carousel Controls
  $(".carousel-control-prev").click(function(){
    $("#myCarousel").carousel("prev");
  });
  $(".carousel-control-next").click(function(){
    $("#myCarousel").carousel("next");
  });
});

	
/* auto popup js */
setTimeout(function() {
    $('#myModal').modal();
}, 2000);
   
/* slide smooth */
$('a').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top
    }, 500);
    return false;
});
</script> 
<script src="<?php echo S3_URL?>/site/realestate-assets/mantri/js/index.js"></script>
</body>
</html>
