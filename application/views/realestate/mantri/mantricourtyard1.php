<!DOCTYPE html>

<html lang="en">
   <head>
      <meta charset="utf-8"> 
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <title> Mantri Courtyard </title>
      <link rel="shortcut icon" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/favicon.ico" type="image/x-icon" />


	  <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/css/vendor.css">
	  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
      <link rel="stylesheet" href="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/css/main.css">    
   </head>
   <body> 

    <?php $all_array = all_arrays(); ?>
	<!-- banner -->
	<div id="home" class="w3ls-banner cd-section">
		<div class="banner-info">
			<!-- header -->
			<div class="header-w3layouts">
				<div class="container">
				
				</div>
			</div>
			<!-- //header --> 
			<!-- banner-text -->
			<div class="container banner-w3ltext"> 
                
			<div>
			<?php $all_array = all_arrays(); ?>
            <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="mantri_jsfrm('<?php echo SITE_URL?>realestate/mantricourtyard/submit_frm','1')">
            <div class="form-logo">
						<a href="#">
                             <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/logo.png" />
						</a> 
                    </div> 
                    <h4>See Green in Every Aspect of Life Now Pre-Launching Mantri Courtyard Phase 4</h4>
            <div class="group">
                <div class="form-group">
                <i class="fa fa-user" aria-hidden="true"></i>
                    <input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid_puravankara('name', 'Please enter correct name')" data-attr="Please enter correct name">
                    <span class="help-block" id="name_err1"></span>
                </div>
               
            </div>  
            <div class="group">
			<div class="form-group">
            <i class="fa fa-envelope-o" aria-hidden="true"></i>
              <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid_puravankara('email', 'Please enter correct email')" data-attr="Please enter correct email">
              <span class="help-block" id="email_err1" ></span>
            </div>
            <div class="form-group">
            <i class="fa fa-phone" aria-hidden="true"></i>	
			<input type="hidden" class="hiddenCountry" name="CountryCode" id="CountryCode1" value="91">
              <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*" maxlength="10" placeholder="Mobile Number" onkeyup="chck_valid_puravankara('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
              <span class="help-block" id="phone_err1"> </span>
            </div>
            </div>       
           
            <!--<input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
			<input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
			<input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">-->
            <div class="submitbtncontainer">
              <input type="submit" id="frm-sbmtbtn1" value="Submit" name="submit">
            </div>
          </form>
               	
                </div>   
			</div>
			<!-- //banner-text -->
		
		</div>
	</div>
    <!-- //banner --> 
    	<!-- about -->
	<div id="about" class="about cd-section">  
		<div class="container">  
			<div class="col-md-6 about-w3lleft"> 
				<h3 class="w3stitle"><span>About Project</span></h3>
                <P>Mantri Courtyard</P> 
				<h4>REDISCOVER THE JOY OF LIVING</h4>
				<p>
                Introducing Mantri Courtyard, Ready to move in Row villas with a private green space that fills joy in your life. Get back to a home with a private outdoor space, a space that fills you up with happiness, a space where you can recharge, refresh and rebalance yourself. Find that space at Mantri Courtyard and rediscover the joy of living.
                </p>
			</div> 
			<div class="col-md-6 about-w3lright">
            <img class="img-responsive"  src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/main-4.jpg" />
			</div>
			<div class="clearfix"> </div> 
		</div>
    </div>

    <!-- //about -->
	<!-- blog -->
	<div id="blog" class="blog cd-section">
		<div class="container"> 
			<div class="agileits-hdng">
				<h3 class="w3stitle"><span>OVERVIEW</span></h3>
			</div>
			<div class="blog-agileinfo">
				<div class="col-md-7 blog-w3grid-img">
					<div class="wthree-blogimg">  
						<img style="height: 280px;" src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/main-1.jpg" class="img-responsive" alt=""/>
					</div>  
				</div>
				<div class="col-md-5 blog-w3grid-text"> 
					<h4>Project Specification</h4>
					
					<p><ul>
                                <li>3BHK, 4BHK Row villas starting from Rs 1.35 Crore onwards.</li>
                                <li>Every home is well equipped with uber-chic interiors and ultra-modern facilities.</li>
                                <li>Colossal clubhouse with array of amenities that leave you spoilt for choice.</li>
                            </ul></p>
				</div> 
				<div class="clearfix"> </div>
			</div> 

			<div class="blog-agileinfo blog-agileinfo-mdl">
				<div class="col-md-7 blog-w3grid-img blog-img-rght">
					<div class="wthree-blogimg">  
						<img  style="height: 450px;" src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/main-2.jpg" class="img-responsive" alt=""/>
					</div>  
				</div>
				<div class="col-md-5 blog-w3grid-text"> 
					<h4>Project Highlights</h4>
					
					<p>

                    <ul>
                                <li>Mantri Courtyard Phase 4 is just 0.8 kms off Kanakpura Main Road, it is very 
    close to the upcoming Anjanapura Metro Station.</li>
                                <li>With Nice Road junction being just 1.2 Kms from Mantri Courtyard Phase 4 it 
    gives good connectivity to locations like Bannergatta Road & Electronic City</li>
                                <li>
                                Mantri Courtyard, 3 & 4 BHK Row Villas with private green space that fill joy in 
    your life. 
                                </li>
                                <li>Successfully Completed Phase 1, Phase 2 & Phase 3 with 80+ Families already 
    living in.</li>
                                <li>Ready to use 17000 Sq. ft clubhouse.</li>
                                <li>Row Villas designed with parking in the Ground Floor, thus eliminating vehicular 
    movement in Front of the homes.</li>
                                <li>2 parking slots with each row villa 
</li>
                            </ul>
                    </p>
				</div> 
				<div class="clearfix"> </div>
			</div> 
			<div class="blog-agileinfo blog-agileinfo-mdl">
            <div class="col-md-7 blog-w3grid-img ">
					<div class="wthree-blogimg">  
						<img  style="height: 300px;" src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/15.jpg" class="img-responsive" alt=""/>
					</div>  
				</div>
				<div class="col-md-5 blog-w3grid-text"> 
					<h4>Project  Clubhouse</h4>
					
					<p>

                    <ul>
                                <li>A WELL EQUIPPED GYM</li>
                                <li>SWIMMING POOL</li>
                                <li>SPACIOUS BANQUET HALL
                                </li>
                                <li>21 SEAT MINI - THEATRE</li>
                                <li>CONVENIENCE STORE</li>
                                <li>SQUASH COURT</li>
                                <li>BILLIARDS</li>
                                <li>TABLE TENNIS</li>
                                <li>INDOOR GAMES</li>
                            </ul>
                    </p>
				</div> 
             
				<div class="clearfix"> </div>
			</div> 
	

		</div>
	</div>
    <!-- //blog -->    

<!-- amenities -->
<div id="amenities" class="services cd-section">
		<div class="container"> 
			<div class="agileits-hdng">
				<h3 class="w3stitle"><span>AMENITIES</span></h3> 
			</div>
			<div class="services-w3ls-row">
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/gym.png" class="img-icon" alt=""/>
				<h5>Gym</h5>
					
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/aerobics.png" class="img-icon" alt=""/>
					<h5>Aerobics Room</h5>
					
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/spa.png" class="img-icon" alt=""/>
					<h5>Steam Room</h5>				
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/yoga.png" class="img-icon" alt=""/>
					<h5>Meditation Room</h5>
				</div>
                <div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/jacuzzi.png" class="img-icon" alt=""/>
				<h5>Jaccuzi</h5>
					
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/restaurant.png" class="img-icon" alt=""/>
					<h5>Restaurant</h5>
					
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/cards.png" class="img-icon" alt=""/>
					<h5>Game Room</h5>				
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/multipurpose-hall.png" class="img-icon" alt=""/>
					<h5>Party Hall</h5>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/swimming.png" class="img-icon" alt=""/>
				<h5>Swimming Pool </h5>
					
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/badminton.png" class="img-icon" alt=""/>
					<h5>Badminton Court</h5>
					
				</div>
			


				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/telescope.png" class="img-icon" alt=""/>
					<h5>TV room/ Mini Theatre</h5>				
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 services-grid agileits-w3layouts">
                <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/sewage.png" class="img-icon" alt=""/>
					<h5>Health Room</h5>
                </div>
            
		
		

				<div class="clearfix"> </div>
			</div>  
		</div>
	</div>
    <!-- //amenities -->
    
 	<!-- info -->
     <div id="info" class="blog cd-section">
		<div class="container"> 
			<div class="agileits-hdng">
				<h3 class="w3stitle"><span>NEAR BY</span></h3> 
			</div>
			<div class="blog-agileinfo">
				<div class="col-md-7 blog-w3grid-img">
					<div class="wthree-blogimg">  
						<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/about-1.jpg" class="img-responsive" alt=""/>
                    </div>  
				</div>
				<div class="col-md-5 "> 
                <div class="detail-section-holder"><ul id="flat_count"><li>
                                            <div class="bhk-status-inner">
                                                <h5>Location USPs:</h5>
                                                <p>Just 0.8 kms off Kanakpura Main Road</p>  
                                                <p>Kumaran's Public School, Indus Business Academy,
                                                 The Valley School, Delhi Public School, all within 
                                                 close vicinity  
                                                 </p> 
                                                 <p>Located just 3kms from upcoming mall by Mantri & Krishna Leela park</p>   
                                                 <p>1 km from the Kanakpura Nice Junction</p>
                                                 <p>Electronic City and Bannergatta Road well connected through Nice Road</p>
                                            </div>
                                        </li>
   
                                   
                                     </ul>
                                    </div>
				</div> 
				<div class="clearfix"> </div>
			</div> 
		</div>
	</div>
    <!-- //blog -->   
    	<!-- info -->
        <div id="location" class="blog cd-section">
		<div class="container"> 
			<div class="agileits-hdng">
				<h3 class="w3stitle"><span>Gallery</span></h3> 
			</div>
			<div class="col-md-4 blog-agileinfo">
			<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/g1.jpg" class="img-responsive" alt=""/>
			
				<div class="clearfix"> </div>
            </div> 
            <div class="col-md-4 blog-agileinfo">
			<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/g2.jpg" class="img-responsive" alt=""/>
			
				<div class="clearfix"> </div>
			</div> 
            <div class="col-md-4 blog-agileinfo">
			<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/g4.jpg" class="img-responsive" alt=""/>
			
				<div class="clearfix"> </div>
            </div> 
            <div class="col-md-4 blog-agileinfo">
			<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/g3.jpg" class="img-responsive" alt=""/>
			
				<div class="clearfix"> </div>
			</div>
            <div class="col-md-4 blog-agileinfo">
			<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/g5.jpg" class="img-responsive" alt=""/>
			
				<div class="clearfix"> </div>
            </div> 
            <div class="col-md-4 blog-agileinfo">
			<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/g6.jpg" class="img-responsive" alt=""/>
			
				<div class="clearfix"> </div>
			</div>
            <div class="col-md-4 blog-agileinfo">
			<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/g7.jpg" class="img-responsive" alt=""/>
			
				<div class="clearfix"> </div>
            </div> 
            <div class="col-md-4 blog-agileinfo">
			<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/g8.jpg" class="img-responsive" alt=""/>
			
				<div class="clearfix"> </div>
			</div>
            <div class="col-md-4 blog-agileinfo">
			<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/g9.jpg" class="img-responsive" alt=""/>
			
				<div class="clearfix"> </div>
            </div> 
            <div class="col-md-4 blog-agileinfo">
			<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/g10.jpg" class="img-responsive" alt=""/>
			
				<div class="clearfix"> </div>
			</div>
            <div class="col-md-4 blog-agileinfo">
			<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/g11.jpg" class="img-responsive" alt=""/>
			
				<div class="clearfix"> </div>
            </div> 
            <div class="col-md-4 blog-agileinfo">
			<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/g12.jpg" class="img-responsive" alt=""/>
			
				<div class="clearfix"> </div>
			</div>
            <div class="col-md-4 blog-agileinfo">
			<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/g13.jpg" class="img-responsive" alt=""/>
			
				<div class="clearfix"> </div>
            </div> 
            <div class="col-md-4 blog-agileinfo">
			<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/g14.jpg" class="img-responsive" alt=""/>
			
				<div class="clearfix"> </div>
			</div>
            <div class="col-md-4 blog-agileinfo">
			<img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/g15.jpg" class="img-responsive" alt=""/>
			
				<div class="clearfix"> </div>
            </div> 
           
		</div>
	</div>
    <div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3889.646930793398!2d77.52319851392099!3d12.866065990925144!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae4399ee352555%3A0x4d5eff12a1e7c00f!2sMantri+Courtyard!5e0!3m2!1sen!2sin!4v1535096746289" width="100" height="450" frameborder="0" style="width:100%;" allowfullscreen></iframe>
    </div>

    <!-- //blog -->   
    <div class="copyw3-agile">
		<div class="container"> 
			<p>© 2018 MANTRI. All Rights Reserved</p>
		</div>
	</div>
	    <!-- contact form start -->
	<div class="floating-form visiable" id="contact_form">
	<div class="contact-opener">Enquire Now</div>
	<form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="mantri_jsfrm('<?php echo SITE_URL?>realestate/mantricourtyard/submit_frm','2')">
            <div class="form-logo">
						<a href="#">
                             <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/logo.png" />
						</a> 
                    </div> 
                    <h4>See Green in Every Aspect of Life Now Pre-Launching Mantri Courtyard Phase 4</h4>
            <div class="group">
                <div class="form-group">
                <i class="fa fa-user" aria-hidden="true"></i>
                    <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid_puravankara('name', 'Please enter correct name')" data-attr="Please enter correct name">
                    <span class="help-block" id="name_err2"></span>
                </div>
       
            </div>  
            <div class="group">
            <div class="form-group">
            <i class="fa fa-envelope-o" aria-hidden="true"></i>
              <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid_puravankara('email', 'Please enter correct email')" data-attr="Please enter correct email">
              <span class="help-block" id="email_err2" ></span>
            </div>
            <div class="form-group">
            <i class="fa fa-phone" aria-hidden="true"></i>	
			<input type="hidden" class="hiddenCountry" name="CountryCode" id="CountryCode2" value="91">
              <input type="text" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*" maxlength="10" placeholder="Mobile Number" onkeyup="chck_valid_puravankara('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
              <span class="help-block" id="phone_err2"> </span>
            </div>
            </div>      
           
            <div class="submitbtncontainer">
              <input type="submit" id="frm-sbmtbtn2" value="Submit" name="submit">
            </div>
          </form>
		  <div>
    <div class="popup-enquiry-form mfp-hide" id="popupForm">
	<form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="mantri_jsfrm('<?php echo SITE_URL?>realestate/mantricourtyard/submit_frm','3')">
            <div class="form-logo">
						<a href="#">
                             <img src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/images/logo.png" />
						</a> 
                    </div> 
                    <h4>See Green in Every Aspect of Life Now Pre-Launching Mantri Courtyard Phase 4</h4>
            <div class="group">
                <div class="form-group">
                <i class="fa fa-user" aria-hidden="true"></i>
                    <input type="text" class="form-control" id="name3" name="name" placeholder="Name" onkeyup="chck_valid_puravankara('name', 'Please enter correct name')" data-attr="Please enter correct name">
                    <span class="help-block" id="name_err3"></span>
                </div>
				<div class="form-group">
            <i class="fa fa-envelope-o" aria-hidden="true"></i>
              <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid_puravankara('email', 'Please enter correct email')" data-attr="Please enter correct email">
              <span class="help-block" id="email_err2" ></span>
            </div>
			<div class="form-group">
            <i class="fa fa-phone" aria-hidden="true"></i>	
			<input type="hidden" class="hiddenCountry" name="CountryCode" id="CountryCode3" value="91">
              <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*" maxlength="10" placeholder="Mobile Number" onkeyup="chck_valid_puravankara('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
              <span class="help-block" id="phone_err3"> </span>
            </div>
            </div>       
           
            
            <div class="submitbtncontainer">
              <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
            </div>
		  </form>
		</div> 
         
	 <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
	 <input type="hidden" id="utm_source1" name="utm_source1" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
	 <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
	 <input type="hidden" id="utm_source2" name="utm_source2" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
	 <input type="hidden" id="utm_source3" name="utm_source3" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
	<script src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/js/vendor.js"></script>  
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>  
	<script src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/js/intlTelInput.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo S3_URL?>/site/realestate-assets/mantri-courtyard/js/main.js"></script>
	  <script src="<?php echo S3_URL?>/site/scripts/default.js"></script>
	  <script src="<?php echo S3_URL?>/site/scripts/realesate.js"></script>
   </body>
</html>