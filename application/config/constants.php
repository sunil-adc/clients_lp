<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Calcutta');
/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


//for live
/*define("SITE_URL",     "http://".$_SERVER['HTTP_HOST'] . "/app/" . basename(dirname(dirname(dirname(__FILE__)))) . "/");
define("FULL_SITE_URL",     "http://".$_SERVER['HTTP_HOST'] . "/app/" . basename(dirname(dirname(dirname(__FILE__)))) . "/index.php");*/

// for local
define("SITE_NAME", "Adcanopus");
if($_SERVER['HTTP_HOST'] == "panelss.in" || $_SERVER['HTTP_HOST'] == "localhost"  || $_SERVER['HTTP_HOST'] == "panelss.in" || $_SERVER['HTTP_HOST'] == "10.0.0.34" || $_SERVER['HTTP_HOST'] == "10.0.0.18" ){
   define("SITE_URL",     "http://".$_SERVER['HTTP_HOST'] . "/Client_LP/index.php/");
   define("S3",     	  "http://".$_SERVER['HTTP_HOST'] . "/Client_LP/");
   define("FULL_SITE_URL",     "http://".$_SERVER['HTTP_HOST'] . "/Client_LP/index.php");
   define("S3_URL", 			S3."cdn");
}else{


   $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
   $domainName = $_SERVER['HTTP_HOST'].'/';
   define("SITE_URL",          $protocol.$_SERVER['HTTP_HOST'] . "/");
   define("FULL_SITE_URL",     $protocol.$_SERVER['HTTP_HOST'] . "/index.php");
   define("S3_URL",            SITE_URL."cdn");

   //define("SITE_URL",     "http://".$_SERVER['HTTP_HOST'] . "/");
   //define("FULL_SITE_URL",     "http://".$_SERVER['HTTP_HOST'] . "/index.php");
   //define("S3_URL",          SITE_URL."cdn");
   
}
define("CAMPAIGN_LOGO_FOLDER", "images/logo/");
define("BANK_BANNERS", "cdn/site/images/bank_banners/");
define("CMS_FOLDER", "cms/");
define("FULL_CMS_URL", FULL_SITE_URL."/cms");
define("SUPPORT_EMAIL", 'info@adcanopus.com');
define("ADMIN_LOGOUT_CONTROLLER", "logout_user/logout");

//TABLE CONSTANT

define("ADMIN_USER",     "admin_user");
define("ADMIN_ROLE",     "tbl_admin_role");
define("ADMIN_USERS",    "tbl_admin_user");
define("USER_DATA",  	 "coreg_user");
define("OFFERS",         "tbl_offers");
define("CITY",           "tbl_cities");
define("SOURCE_PIXEL",   "tbl_utm_source");
define("MEDIUM_PIXEL",   "tbl_utm_medium");
define("ITOKYO",   		 "tbl_itokyo");
define("ICITY",          "tbl_cities_itokyo");
define("MEDLIFE_USER",   "tbl_medlife_user");
define("RELIGARE_USER",  "tbl_religare_user");
define("SHELTERX_USER",  "tbl_sheltrex");
define("POLICY_HEALTH",  "tbl_policyx_health");
define("POLICY_LIFE",    "tbl_policyx_life");
define("PP_USER",        "tbl_pp_cc_user");

define("REWARDS",   	    "tbl_rewards");
define("BANKLIST",   	 "tbl_bank");
define("CC_CARD",   	    "tbl_cc_offers");
define("BANKLIST_CACHE", "banklistcache12");
define("CARD_CACHE",     "cardcache12");
define("REWARD_CACHE",   "rewardcache12");
define("PURAVANKARA",    "tbl_puravankara");
define("SRIRAMA",        "tbl_srirama");
define("MANTRI",         "tbl_mantri");
define("HIRANANDANI",    "tbl_hiranandani");
define("VAJRAM",        "tbl_vajram");          
define("LSBF",          "tbl_lsbf_users");
define("OZONE",         "tbl_ozone");
define("NMIMS",         "tbl_nmims");
define("WINMORE",       "tbl_winmore");
define("NUTRATIMES",    "tbl_nutratimes");
define("NUTRATIMES_QUIZ", "tbl_nutratimes_quiz");
define("GRATORAMA",     "tbl_gratorama");
define("AFFILIATE_PIXEL","tbl_utm_pub");
define("LEAD_LOGS",      "tbl_leads_logs");
define("OXYGENCLUB",     "tbl_oxygenclub");
define("VASWANIGROUP",    "tbl_vaswanigroup");
define("WOLKSWAGEN",    "tbl_wolkswagen");
define("SMARTOWNER",    "tbl_smartowner");
define("PRESTIGE",    "tbl_prestige");
define('REALESTATE_USER', 'tbl_realestate_user');
define('INSURANCE_USER', 'tbl_insurance');
define('UNIVERSALPRIME_USER', 'tbl_universalprime');
define('PRELANDER_USER',   'tbl_prelander_user');
define('CHAIPOINT',        'tbl_chaipoint');
define('MFINE_USER',       'tbl_mfine_user');
define('SOBHA',            'tbl_sobha');
define('LENSKART_USER',    'tbl_lenskart_user');


//CACHE CONSTANT
define("CITY_CACHE", "ciycacyhe423");  
define("ICITY_CACHE", "citycacyhe");


define("BANK_IMG_FOLDER", "cdn/site/bank_img/");
define("CARD_IMAGE",      "cdn/site/card_img/");
define('IMAGE_UPLOAD_MAX_SIZE', 2*1024*1024);


//LIST OF STORED KEYS IN MEMCACHE END

define("MEMCACHE_KEY", "74madisyqpad3453"); 

if(empty($_SERVER['HTTP_USER_AGENT'])) {
    
   define('IS_MOBILE', (bool)preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet'.
               '|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]'.
               '|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', 'windows'));    
      
}else{
      
   define('IS_MOBILE', (bool)preg_match('#\b(ip(hone|od|ad)|android|opera m(ob|in)i|windows (phone|ce)|blackberry|tablet'.
               '|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp|laystation portable)|nokia|fennec|htc[\-_]'.
               '|mobile|up\.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\b#i', $_SERVER['HTTP_USER_AGENT'] ));
}


define("ALLOWED_IP", "111.93.187.26, 111.93.187.254, 117.192.223.217, 1.22.234.86, 202.191.245.14, 218.108.97.54, 117.192.197.68, 117.192.192.243, 117.192.217.156, 117.216.158.251, 117.192.213.200, 117.221.29.244, 117.216.158.172, 117.192.206.139, 117.192.215.160, 182.74.201.118, 120.138.125.238, ::1");

/* End of file constants.php */
/* Location: ./application/config/constants.php */
