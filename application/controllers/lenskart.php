<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class lenskart extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model(array("cache_model", "email_templates", "lead_check"));
		$this->db->cache_off();
		
		
	}
   
	public function index(){				
	   
	   
	   $this->load->view("lenskart");

    }
   
   
    public function lenskart_frm(){
   		
		$all_array = all_arrays(); 
		
	    if( is_array($_POST) && count($_POST) > 0 ){
		   foreach ($_POST as $k=>$v){
			   $this->form_validation->set_rules($k, $k , 'trim|required');
		   }
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		   }else{
			    $res = $this->db->query("select id, phone from ".LENSKART_USER." where phone ='".$_POST['phone']."'");
			       
			    if($res->num_rows() > 0){
			      
				  echo "done";
				  exit;
			   
			    }else{
				   
				
				$val_otp=mt_rand(1000, 9999);   
				
			    $data = array(
							 'phone'        => $_POST['phone'],
							 'city'         => $_POST['city'],
							 'otp'          => $val_otp,
							 'utm_source'   => $_POST['utm_source'],
							 'utm_campaign' => $_POST['utm_campaign'],
							 'date_created' => date( 'Y-m-d H:i:s' ) 					   
							);
							
			    $result = $this->db->insert(LENSKART_USER, $data);	    
			    $insert_id = $this->db->insert_id($result);
				$this->session->set_userdata('user_id', $insert_id);
				$this->session->set_userdata('utm_source', trim($_POST['utm_source']));
				$this->session->set_userdata('phone', trim($_POST['phone']));
				
				if($insert_id != "" ){


					if(_sendsms($_POST['phone'],$val_otp." is your verification code.", "LNSKRT")){
						
						echo "done";

					}	
						
					if($_POST['utm_campaign'] != "2480_" && $_POST['utm_campaign'] != "1481_" ){
					
						//API 
						if(strtolower($_POST['city']) != "others"){ 

							$curl = curl_init();

							curl_setopt_array($curl, array(
							  CURLOPT_PORT => "9000",
							  CURLOPT_URL => "http://centrix.lenskart.com:9000//apps/addlead.php?mobile=".$_POST['phone']."&camp_name=HEC_Lead_Gen&dialer_flag=1&Source=Adcanopus_OTP&Service=HEC@Price=130&city=".$_POST['city'],
							  CURLOPT_RETURNTRANSFER => true,
							  CURLOPT_ENCODING => "",
							  CURLOPT_MAXREDIRS => 10,
							  CURLOPT_TIMEOUT => 30,
							  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							  CURLOPT_CUSTOMREQUEST => "POST",
							  CURLOPT_HTTPHEADER => array(
							    "cache-control: no-cache",
							  ),
							));

							$response = curl_exec($curl);
							$err = curl_error($curl);

							curl_close($curl);

							if ($err) {
							  //echo "cURL Error #:" . $err;
							} else {
							  //echo $response;
							}
						}
			    
					}
				}
			    }
			    //echo "done";
			    exit;	 
	        }
		  echo "done";exit;	
      }else{
	   
	   echo "empty";  exit; 
      }

      exit;
    }
	
	
	public function lnskrt_otpverification(){
		$all_array = all_arrays(); 
		
	    if( is_array($_POST) && count($_POST) > 0 ){
		   foreach ($_POST as $k=>$v){
			   $this->form_validation->set_rules($k, $k , 'trim|required');
		   }
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		   }else{
			  
			   $res = $this->db->query("select id, city, phone, utm_campaign from ".LENSKART_USER." where phone ='".$_POST['otp_phone']."' and otp= '".$_POST['otp']."'");
			    if( $res->num_rows() > 0){
					$result1= $res->result_array();
					
			        $data_update = array(
							 'otp_optin'    => 1, 					   
							);
					$this->db->where('id',$result1[0]['id']);
					$result = $this->db->update(LENSKART_USER, $data_update);

					$this->session->set_userdata('user_id', $result1[0]['id']);
					
					$n     = $result1[0]['city'];
					$p     = $result1[0]['phone'];

					if($result1[0]['utm_campaign'] == "2480_" || $result1[0]['utm_campaign'] == "1481_" ){
					
						if(strtolower($result1[0]['city']) != "others"){

							$curl = curl_init();

							curl_setopt_array($curl, array(
							  CURLOPT_PORT => "9000",
							  CURLOPT_URL => "http://centrix.lenskart.com:9000//apps/addlead.php?mobile=".$_POST['otp_phone']."&camp_name=HEC_Lead_Gen&dialer_flag=1&Source=Adcanopus_OTP&Service=HEC@Price=130&city=".$result1[0]['city'],
							  CURLOPT_RETURNTRANSFER => true,
							  CURLOPT_ENCODING => "",
							  CURLOPT_MAXREDIRS => 10,
							  CURLOPT_TIMEOUT => 30,
							  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							  CURLOPT_CUSTOMREQUEST => "POST",
							  CURLOPT_HTTPHEADER => array(
							    "cache-control: no-cache",
							  ),
							));

							$response = curl_exec($curl);
							$err = curl_error($curl);

							curl_close($curl);

							if ($err) {
							  //echo "cURL Error #:" . $err;
							} else {
							  //echo $response;
							}
						}
					}


					echo "done";
				    exit;
			   
			    }else{
					$res1 = $this->db->query("select id from ".LENSKART_USER." where phone ='".$_POST['otp_phone']."'");
					if($res1->num_rows() > 0){
						$result1= $res1->result_array();
						echo "resend";
						echo ','.$result1[0]['id'];
						
					exit;
					}				
				}
			}
		
      }else{
	   
	  	 echo "empty";   
      }

      exit;
    }
	
    public function lnkkrt_reset(){
		
	    if( is_array($_POST) && count($_POST) > 0 ){
		  if($_POST['otp_phone']!=''){
		  	 
		  	 $res = $this->db->query("select id, phone from ".LENSKART_USER." where phone ='".$_POST['otp_phone']."'");
				if($res->num_rows() > 0){
					$result1= $res->result_array();
					
					$uid = $result1[0]['id'];
					
				    $otp_val = mt_rand(1000, 9999);
				    $data_up = array('otp'    => $otp_val, );

					$this->db->where('id',$uid);
				    $result = $this->db->update(LENSKART_USER, $data_up);			
			
					if(_sendsms($result1[0]['phone'],$otp_val." is your verification code.", "LNSKRT")){
						echo "done";
					}else{
						echo 'error';
					}
				exit;
				}else{
					echo "error";
				}
			   
			}
		  
		}

      exit;
    }
  
	
    
    public function success(){
		
		$vr = $this->session->userdata('user_id');
	    if( isset($vr) &&  $vr > 0 ){
			
			$this->load->view("lenskart-success");
		}else{
			redirect(SITE_URL.'lenskart');
		}
			
			
		
	}

   

}
?>