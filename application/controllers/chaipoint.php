
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class chaipoint extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
	
		$this->db->cache_off();
		$this->load->model(array("cache_model", "email_templates","lead_check"));
         $this->load->helper(array("general_helper"));
		
	}
	
    public function index(){	
		
	    $this->load->view("chai-point");
	}    
        
     
    public function success(){    
        
        
        $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
            
            $this->load->view("chai-point-success");
        } else {
            redirect(SITE_URL.'chai-point');
        }
        
    }



    public function frm_submit()
    {
        $all_array = all_arrays();
       
        if (isset($_POST) && count($_POST) > 0) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('phone', 'Phone No.', 'trim|required');
            $this->form_validation->set_rules('city', 'City.', 'trim|required');
            
            if ($this->form_validation->run() == FALSE) {
                
                echo "validation_error";
                
                
            } else {
                
                $result = $this->general->is_duplicate(CHAIPOINT ,"id, email","email ='" . $_POST['email'] . "'");
                if ($result == true) {
                    
                    echo "done";
                    exit;
                    
                } else {
                    $data   = array(
                                'name'     => $_POST['name'],
                                'phone'    => $_POST['phone'],
                                'email'    => $_POST['email'],
                                'city'     => $_POST['city'],
                                'company'  => $_POST['company'],
                                'employee' => $_POST['employee'],
                                'city'     => $_POST['city'],
                                'utm_source' => $_POST['utm_source1'],
                                'utm_sub'    => $_POST['utm_source2'],
                                'utm_medium' => $_POST['utm_source3'],
                                'ip_address' => $_SERVER['REMOTE_ADDR'],
                                'utm_campaign' => $_POST['campaign'],
                                'date_created' => date('Y-m-d H:i:s')
                    );


                    $insert_id = $this->general->add_user(CHAIPOINT, $data);

                    $this->session->set_userdata('user_id', $insert_id);
                    $this->session->set_userdata('utm_source', trim($_POST['utm_source1']));
                    $this->session->set_userdata('phone', trim($_POST['phone']));
                    
                }
                echo "done";
                exit;
            }
            
            
        } else {
            
            echo "empty";
            exit;
        }
        
        
    }


    
}