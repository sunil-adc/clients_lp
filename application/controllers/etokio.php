<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class etokio extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model("cache_model");
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), $this->input->get('bnr'), $this->input->get('country'),  $this->input->get('zoneid'),  $this->input->get('mobile_track'), $this->input->get('ClickID'));
		
	}
   
	public function index(){				
	   
	   $data['city_arr'] = $this->cache_model->get_cache(ICITY_CACHE, "id,city,status", ICITY, " city ", "");	   
	   
	   $this->load->view("itokyo", $data);

    }
   
   
    public function itokyofrm(){
   		
	    if( is_array($_POST) && count($_POST) > 0 ){
		   foreach ($_POST as $k=>$v){
			   $this->form_validation->set_rules($k, $k , 'trim|required');
		   }
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		   }else{
			    $res = $this->db->query("select id, phone from ".ITOKYO." where phone ='".$_POST['phone']."'");
			       
			    if($res->num_rows() > 0){
			      
			   
			    }else{
				
				$data = array(	
					'name'      => $_POST['name'],
					'phone'     => $_POST['phone'],
					'plan'      => $_POST['plan'],
					'city'      => $_POST['city'],
					'income'    => $_POST['income'],
					'age'       => $_POST['age'],
					'utm_medium'=> $_POST['utm_medium'],
					'utm_source'=> $_POST['utm_source'],
					'date_created' => date( 'Y-m-d H:i:s' ) 
					);
					 
					$result = $this->db->insert(ITOKYO, $data);
					$insert_id = $this->db->insert_id($result);
					$this->session->set_userdata('etokio_id', $insert_id);
				
				}
					$ch = curl_init(); 
					
					curl_setopt($ch, CURLOPT_URL, "http://bidcan.com/edelweisstokio/tokyo_api.php"); 
					curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
					curl_setopt($ch,CURLOPT_HEADER, false); 
					curl_setopt($ch, CURLOPT_POST, count($data));
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 			
					
					$output=curl_exec($ch);

					// close curl resource to free up system resources 
					curl_close($ch);  
						
					//echo $output;exit;
					
					echo "done";
					exit;
				
			}
				 
	    
      }else{
	   
	   echo "empty";   
      }

      exit;
   }
   
   
   public function success(){
		
		$vr = $this->session->userdata('etokio_id');
	    if( isset($vr) &&  $vr > 0 ){
			
			$this->load->view("itokyo-success");
		}else{
			redirect(SITE_URL.'etokio');
		}	
		
	}
   
   

}
?>