<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class gratorama extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model(array("cache_model","lead_check"));
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), $this->input->get('bnr'), $this->input->get('country'),  $this->input->get('zoneid'),  $this->input->get('mobile_track'), $this->input->get('ClickID'));
		
	}
   
	public function index(){				
	   
	   $this->load->view("gratorama");

    }
   
   public function success(){
	   $this->load->view("gratorama-success");
   }
   
   public function submit_frm(){
   
	   if( isset($_POST) && count($_POST) > 0 ){
			$this->form_validation->set_rules('name', 'Name' , 'trim|required');
			$this->form_validation->set_rules('email', 'Email' , 'trim|required');
			$this->form_validation->set_rules('phone', 'Phone No.' , 'trim|required');
		    if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		    }else{
				
				$res = $this->db->query("select id, email from ".GRATORAMA." where email ='".$_POST['email']."'");
			       
			    if($res->num_rows() > 0){
			      
				  echo "done";
				  exit;
			   
			    }else{
					$data = array(
							 'name'          => $_POST['name'],
							 'phone'         => $_POST['phone'],
							 'email'         => $_POST['email'],
							 'city'          => $_POST['city'],
							 'utm_source'    => $_POST['utm_source1'],
							 'utm_sub'       => $_POST['utm_source2'],
							 'utm_medium'    => $_POST['utm_source3'],
							 'ip_address'    => $_SERVER['REMOTE_ADDR'],
							 'utm_campaign'  => $_POST['campaign'],
							 'date_created'  => date( 'Y-m-d H:i:s' ) 					   
							);
				$result = $this->db->insert(GRATORAMA, $data);	    
			    $insert_id = $this->db->insert_id($result);
				$this->session->set_userdata('user_id', $insert_id);
				$this->session->set_userdata('utm_source', trim($_POST['utm_source1']));
				echo "done";
					exit;
				}
				
		    }
	   }else{
		   
		   echo "empty";   exit;
       } 
	   
	   
   }
    
   
   

}
?>