<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class offercharge_kasplo extends CI_Controller
{
   public $global_country;
   public function __construct(){
        
		parent::__construct();
		$this->db->cache_off();
		$this->load->library("PHPExcel");
		$this->db = $this->load->database('knowastro', TRUE);
		//if(!is_user_login($this->session->userdata('uid'))){
			//redirect(FULL_CMS_URL."/".ADMIN_LOGOUT_CONTROLLER);
		//}else{
			//echo "not allowed";
			//die();
		//}
	}

   
    public function index($seckey, $interval=""){
 
		$yesterday_dt     = "2016-04-24";//date('Y-m-d', strtotime(date('Y-m-d') .' -1 day'));

		if($interval ==1){

			$from_dt     = $yesterday_dt." 00:00:01" ;

			$to_dt       = $yesterday_dt." 11:59:59" ;
		
		}else if ($interval == 2){

			$from_dt     = $yesterday_dt." 12:01:01" ;

			$to_dt       = $yesterday_dt." 23:59:59" ;
		}else{

			echo "Not authozized";
			die();
		}

		
		$sel_query ="SELECT 
					   	  first_name, last_name, gender, mobile, salary, birthdate, city,
						  ip_address, DATE_FORMAT(dateadded, '%d-%m-%Y') as date_created  
					 FROM
					  	 sc_users 
					 WHERE 
						 mobile != '' and  
						 date_added BETWEEN '".$from_dt."' AND '".$to_dt."'";				 
		
		echo $sel_query;
		die();

		$query = $this->db->query($sel_query);
		
		if ($query->num_rows() > 0) {
			


				$fileName = "offercharge_".strtotime($from_dt);
				$object = new PHPExcel();
				$object->setActiveSheetIndex(0);
				$table_columns = array("first_name", "last_name", "mobile", "salary", "dob", "city", 
					  "ip", "lead_date", "source", "category_ids");

			    $column = 0;
			    $excel_row = 2;
				foreach($table_columns as $field)
				{
				   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
				   $column++;
				}

				
				foreach ($query->result() as $value) {
					$know_data[] = $value;
				}
				
				if(is_array($know_data)){
				   foreach($know_data as $v){
						
						$date =date('Y-m-d', strtotime($v->date_created));

					    $object->getActiveSheet()->setCellValueByColumnAndRow(1,  $excel_row, $v->first_name);
					    $object->getActiveSheet()->setCellValueByColumnAndRow(2,  $excel_row, $v->last_name);
					    $object->getActiveSheet()->setCellValueByColumnAndRow(4,  $excel_row, $v->mobile);
					    $object->getActiveSheet()->setCellValueByColumnAndRow(10,  $excel_row, $v->salary);
					    $object->getActiveSheet()->setCellValueByColumnAndRow(11,  $excel_row, $v->dob);
						$object->getActiveSheet()->setCellValueByColumnAndRow(12,  $excel_row, $v->city);
						$object->getActiveSheet()->setCellValueByColumnAndRow(14,  $excel_row, $v->ip_address);
						$object->getActiveSheet()->setCellValueByColumnAndRow(15,  $excel_row, $dateadded);
						$object->getActiveSheet()->setCellValueByColumnAndRow(16,  $excel_row, "1");
						$object->getActiveSheet()->setCellValueByColumnAndRow(17,  $excel_row, "1");
					
						$excel_row++;
					}

					$object->getActiveSheet()->setTitle($fileName);
					$objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		            $objWriter->save(getcwd().'/cdn/site/kasplo-data/offercharge/'.$fileName . '.xls');

		            //$this->curl_call($fileName);
				}
				

		}else{
			echo "no data found";	
		}
   }
   
   
    public function curl_call($fileName){
		

		$ch = curl_init();

		$data = array('source' => 'offercharge', 'file' => '@cdn/site/kasplo-data/'.$fileName.'.xls');

		curl_setopt($ch, CURLOPT_URL, 'http://kasplo.com/pull-data-api/upload_script.php');

		curl_setopt($ch, CURLOPT_POST, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		$res = curl_exec($ch); 
		echo $res;


	 }


   
   
}

?>