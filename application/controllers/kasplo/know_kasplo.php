<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class know_kasplo extends CI_Controller
{
   public $global_country;
   public function __construct(){
        
		parent::__construct();
		$this->db->cache_off();
		$this->load->library("PHPExcel");
		$this->db = $this->load->database('knowastro', TRUE);
		//if(!is_user_login($this->session->userdata('uid'))){
			//redirect(FULL_CMS_URL."/".ADMIN_LOGOUT_CONTROLLER);
		//}else{
			//echo "not allowed";
			//die();
		//}
	}

   
    public function index($seckey, $interval=""){
 
	    $education = array(
							"1" => "10th Std", "2" => "12th Std", "3" => "BA", "4" => "BBA", "5" => "BCA", "6" => "BCom", "7" => "BE/BTech", "8" => "BSc", "9" => "MA",
							"10" => "MBA", "11" => "MCA", "12" => "MCom", "13" => "MSc", "14" => "ME/MTech", "15" => "Phd", "16" => "Other UG", "17" => "Other PG",
							"18" => "Others"	
							);

		$profession = array(
                            "1"=> "Designer", "2"=> "Employee", "3"=> "Enterpreneur/Businessman", "4"=> "Executive", "5"=> "Factory Worker", "6"=> "Graphic Designer",
                             "7"=> "Homemaker", "8"=> "IT Expert/Developer", "9"=> "Journalist", "10"=> "Medical Professional", "11"=> "Retailer", "12"=> "Retired",
                             "13"=> "Self-Employed", "14"=> "Student", "15"=> "Teacher", "16"=> "Technician", "17"=> "Tradesman", "18"=> "Unemployed", "19"=> "Others"
                            );						

		$yesterday_dt     = date('Y-m-d', strtotime(date('Y-m-d') .' -1 day'));

		if($interval ==1){

			$from_dt     = $yesterday_dt." 00:00:01" ;

			$to_dt       = $yesterday_dt." 11:59:59" ;
		
		}else if ($interval == 2){

			$from_dt     = $yesterday_dt." 12:01:01" ;

			$to_dt       = $yesterday_dt." 23:59:59" ;
		}else{

			echo "Not authozized";
			die();
		}

		
		$sel_query ="SELECT 
					   	 id,  gender,  name,  surname,  email,  checking,
					   	 phonenumber,
						 is_optin,  user_type,  fb_id,  gmail_id,  optin_date,  birth_place,  profession,  education,
						 country,  state,  city,  dob,  fire_status,  net,  pub,  utm_medium,  unsub_daily,  unsub_special,
						 unsub_promo,  os,  device,  browser,  status,  ip,  DATE_FORMAT(date_created, '%d-%m-%Y') as date_created  
					 FROM
					  	 user_register 
					 WHERE 
						 phonenumber != '' and  
						 country = 101 and
						 date_created BETWEEN '".$from_dt."' AND '".$to_dt."'";				 
		
		
		$query = $this->db->query($sel_query);
		
		if ($query->num_rows() > 0) {
			


			//GET STATES
			$state_query = $this->db->query("SELECT id, name, country_id FROM tbl_states WHERE country_id =101");
			if ($state_query->num_rows() > 0) {
				foreach ($state_query->result() as $v) {
					$state_arr[$v->id] = $v->name;
				}
			}
			
			
			//GET CITIES
			$city_query = $this->db->query("SELECT id, name FROM tbl_cities");
			if ($city_query->num_rows() > 0) {
				foreach ($city_query->result() as $c) {
					$city_arr[$c->id] = $c->name;
				}
			}
			

				$fileName = "knstro_".strtotime($from_dt);
				$object = new PHPExcel();
				$object->setActiveSheetIndex(0);
				$table_columns = array("gender", "first_name", "last_name", "email_id", "mobile_no", "profession", "qualification_1", "country",
				 "state", "city", "dob", "operating_system", "device", "browser", "ip", "lead_date", "source", "category_ids");

			    $column = 0;
			    $excel_row = 2;
				foreach($table_columns as $field)
				{
				   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
				   $column++;
				}

				
				foreach ($query->result() as $value) {
					$know_data[] = $value;
				}
				
				if(is_array($know_data)){
				   foreach($know_data as $v){
						
						$date =date('Y-m-d', strtotime($v->date_created));

					    $object->getActiveSheet()->setCellValueByColumnAndRow(0,  $excel_row, ($v->gender == 1 ? 'Male'  : 'Female'));
					    $object->getActiveSheet()->setCellValueByColumnAndRow(1,  $excel_row, $v->name);
					    $object->getActiveSheet()->setCellValueByColumnAndRow(2,  $excel_row, $v->surname);
					    $object->getActiveSheet()->setCellValueByColumnAndRow(3,  $excel_row, $v->email);
					    $object->getActiveSheet()->setCellValueByColumnAndRow(4,  $excel_row, $v->phonenumber);
					    $object->getActiveSheet()->setCellValueByColumnAndRow(5,  $excel_row, ($v->profession != "" && $v->profession != 0  && $v->profession < 20 ? $profession[$v->profession] : ''));
					    $object->getActiveSheet()->setCellValueByColumnAndRow(6,  $excel_row, ($v->education != "" && $v->education != 0 && $v->education < 19 ? $education[$v->education] : ''));
					    $object->getActiveSheet()->setCellValueByColumnAndRow(7,  $excel_row, 'INDIA');
					    $object->getActiveSheet()->setCellValueByColumnAndRow(8,  $excel_row, ($v->state != "" && $v->state !=0 ? $city_arr[$v->state] : ''));
					    $object->getActiveSheet()->setCellValueByColumnAndRow(9,  $excel_row, ($v->city != "" && $v->city != 0 ? $city_arr[$v->city] : ''));
					    $object->getActiveSheet()->setCellValueByColumnAndRow(10,  $excel_row, $v->dob);
					    $object->getActiveSheet()->setCellValueByColumnAndRow(11,  $excel_row, $v->os);
						$object->getActiveSheet()->setCellValueByColumnAndRow(12,  $excel_row, $v->device);
						$object->getActiveSheet()->setCellValueByColumnAndRow(13,  $excel_row, $v->browser);
						$object->getActiveSheet()->setCellValueByColumnAndRow(14,  $excel_row, $v->ip);
						$object->getActiveSheet()->setCellValueByColumnAndRow(15,  $excel_row, $date);
						$object->getActiveSheet()->setCellValueByColumnAndRow(16,  $excel_row, "1");
						$object->getActiveSheet()->setCellValueByColumnAndRow(17,  $excel_row, "1");
					
						$excel_row++;
					}

					$object->getActiveSheet()->setTitle($fileName);
					$objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel5');
		            $objWriter->save(getcwd().'/cdn/site/kasplo-data/'.$fileName . '.xls');

		            $this->curl_call($fileName);
				}
				

		}else{
			echo "no data found";	
		}
   }
   
   
    public function curl_call($fileName){
		

		$ch = curl_init();

		$data = array('source' => 'knowastro', 'file' => '@cdn/site/kasplo-data/'.$fileName.'.xls');

		curl_setopt($ch, CURLOPT_URL, 'http://kasplo.com/pull-data-api/upload_script.php');

		curl_setopt($ch, CURLOPT_POST, 1);

		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

		$res = curl_exec($ch); 
		echo $res;


	 }


   
   
}

?>