<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class life extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model(array("cache_model", "email_templates", "lead_check"));
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), $this->input->get('bnr'), $this->input->get('country'),  $this->input->get('zoneid'),  $this->input->get('mobile_track'), $this->input->get('ClickID'));
		
	}
   
	public function index(){				
	  
	   $data['city_arr'] = $this->cache_model->get_cache(CITY_CACHE, "id,city,status", CITY, " city ", "");	  
	   $this->load->view("life", $data);

    }
   
   
    public function lifefrm(){
   		//print_r($_POST);exit;
		$all_array = all_arrays(); 
		
	    if( is_array($_POST) && count($_POST) > 0 ){
		   foreach ($_POST as $k=>$v){
			   $this->form_validation->set_rules($k, $k , 'trim|required');
		   }
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		   }else{
			  
				
				$res = $this->db->query("select id, email from ".POLICY_LIFE." where email ='".$_POST['email']."'");
			       
			    if($res->num_rows() > 0){
			      
					echo "done";
					exit;
			   
			    }else{
					$u_dob = date("Y-m-d", strtotime($_POST['dob'])) ;
					
					$data = array(
							 'name'         => $_POST['name'],
							 'phone'        => $_POST['phone'],
							 'email'        => $_POST['email'],
							 'city'         => $_POST['city'],
							 'gender'       => $_POST['gender'],
							 'dob'          => date("Y-m-d", strtotime($_POST['dob'])),
							 'otp'          => '',
							 'utm_source' 	=> $_POST['utm_source'],
							 'utm_campaign' => $_POST['utm_campaign'],
							 'utm_medium'   => $_POST['utm_medium'],
							 'date_created' => date( 'Y-m-d H:i:s' ) 					   
							);
										
					$result = $this->db->insert(POLICY_LIFE, $data);	    
					$insert_id = $this->db->insert_id($result);
					$this->session->set_userdata('user_id', $insert_id);
					$this->session->set_userdata('utm_source', trim($_POST['utm_source']));
					$this->session->set_userdata('utm_medium', trim($_POST['utm_medium']));
					
					
					
					//$url ='http://uat.policyx.com/api/getAdcanopusTermLead.php?Name='.$_POST["name"].'&Phone='.$_POST["phone"].'&Email='.$_POST["email"].'&DOB='.$_POST["dob"].'&City='.$_POST["phone"].'&Gender='.$_POST["gender"];		
					
					$url ='https://www.policyx.com/api/getAdcanopusTermLead.php?Name='.$_POST["name"].'&Phone='.$_POST["phone"].'&Email='.$_POST["email"].'&DOB='.$u_dob.'&City='.$_POST["city"].'&Gender='.$_POST["gender"];		
					
					$curl = curl_init($url);
					curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($curl, CURLOPT_HEADER, 0);
					//curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
					curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
					$json_response = curl_exec($curl);
					
					curl_close($curl);
					
					
					if(isset($json_response)){
					
						
						echo "done";
						exit;
					}else{
						
						echo "not";
						exit;
					}
				
			    echo "done";
				exit;
			}
		   }
		echo "done";exit;
      }else{
	   
	   echo "empty";   
      }

      exit;
    }
	
	
    
    public function success(){
		
		$vr = $this->session->userdata('user_id');
	    if( isset($vr) &&  $vr > 0 ){
			
			$this->load->view("life-success");
		}else{
			redirect(SITE_URL.'life');
		}
			
			
	}

	

}
?>