<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class oxygen_resort extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model(array("cache_model","lead_check", "email_templates"));
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), $this->input->get('bnr'), $this->input->get('country'),  $this->input->get('zoneid'),  $this->input->get('mobile_track'), $this->input->get('ClickID'));
		
	}
   
	public function index(){				
	   
	   	$data['city_arr'] = $this->cache_model->get_cache(CITY_CACHE, "id,city,status", CITY, " city ", "");


	   $this->load->view("oxygen-resort", $data);

    }
   
   public function success(){
	   $this->load->view("oxygen-resort-success");
   }
   
   public function submit_frm(){
	   if( isset($_POST) && count($_POST) > 0 ){
			$this->form_validation->set_rules('name', 'Name' , 'trim|required');
			$this->form_validation->set_rules('email', 'Email' , 'trim|required');
			$this->form_validation->set_rules('phone', 'Phone No.' , 'trim|required');
		    if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		    }else{
				
				$res = $this->db->query("select id, email from ".OXYGENCLUB." where email ='".$_POST['email']."'");
			       
			    if($res->num_rows() > 0){
			      
				  echo "done";
				  exit;
			   
			    }else{

			    	$val_otp=mt_rand(1000, 9999); 
					$data = array(
							 'name'           => $_POST['name'],
							 'phone'          => $_POST['phone'],
							 'email'          => $_POST['email'],
							 'age'            => $_POST['age'],
							 'location'       => $_POST['location'],
							 'last_location'  => $_POST['lastlocation'],
							 'otp'            => $val_otp,
							 'utm_source'     => $_POST['utm_source1'],
							 'utm_sub'        => $_POST['utm_source3'],
							 'utm_medium'     => $_POST['utm_source2'],
							 'utm_term'       => $_POST['utm_term'],
							 'utm_content'    => $_POST['utm_content'],
							 'ip_address'     => $_SERVER['REMOTE_ADDR'],
							 'utm_campaign'   => $_POST['campaign'],
							 'date_created'   => date( 'Y-m-d H:i:s' ) 					   
							);
				$result = $this->db->insert(OXYGENCLUB, $data);	    
			    $insert_id = $this->db->insert_id($result);
			   
				$this->session->set_userdata('user_id', $insert_id);
				$this->session->set_userdata('phone', $_POST['phone']);
				$this->session->set_userdata('utm_source', trim($_POST['utm_source1']));

				if(_sendsms($_POST['phone'],$val_otp." is your verification code.", "OXRSRT")){

					//SEND THROUGH API
					if($_POST['age'] > 29 && strtolower($_POST['location']) != "others"){

						$post_arr = array(
											"api_key"=> "5d71e4df1aebfc0911364cdf",
						                    "first_name" => $_POST['name'],
						                    "age" => $_POST['age'],
						                    "city" => $_POST['location'],
						                    "email" => $_POST['email'],
						                    "mobile" => $_POST['phone'],
						                    "vaccation_in_year" => "2",
						                    "utm_campaign" => $_POST['campaign'],
						                    "utm_medium"   => $_POST['utm_source2'],
						                    "utm_term"     => $_POST['utm_term'],
						                    "utm_content"  => $_POST['utm_content'],
						                    "verification_status" => "0"
										);

						$curl = curl_init();

						curl_setopt_array($curl, array(
						  CURLOPT_URL => "https://o2.cluboxygen.net/api/createleadvendor",
						  CURLOPT_RETURNTRANSFER => true,
						  CURLOPT_ENCODING => "",
						  CURLOPT_MAXREDIRS => 10,
						  CURLOPT_TIMEOUT => 30,
						  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						  CURLOPT_CUSTOMREQUEST => "POST",
						  CURLOPT_POSTFIELDS => json_encode($post_arr),
						  CURLOPT_HTTPHEADER => array(
						    "cache-control: no-cache",
						    "content-type: application/json",
						  ),
						));

						$response = curl_exec($curl);
						$err = curl_error($curl);

						curl_close($curl);

						if ($err) {
						  //echo "cURL Error #:" . $err; 
						} else {
						  //echo $response;
						}
					}

						echo "done";
						
					}else{
						echo 'error';
					}
					exit;
				}
				
		    }
	   }else{
		   
		   echo "empty";   exit;
       } 
	   
	   
   }
    
   public function otp_verify(){

   $valid_city_arr=array('Kerala','Mumbai','Pune','Ahmedabad', 'Rajkot', 'Indore');

   if( isset($_POST) && count($_POST) > 0 ){
       $this->form_validation->set_rules('otp', 'otp' , 'trim|required');
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		   }else{
		   	$email = $_POST['email'];
		   	$otp   = $_POST['otp'];
            
		   	 $res = $this->db->query("select id, email, name, phone, age, location, last_location, utm_source, utm_campaign from ".OXYGENCLUB." where otp= '".$otp."' and email= '".$email."' ");
			    
			    if( $res->num_rows() > 0){
					$result1= $res->result_array();
					
			        $data_update = array('otp_optinstatus'    => 1, );
					$this->db->where('id',$result1[0]['id']);
					$this->db->update(OXYGENCLUB, $data_update);
					

					$n     = $result1[0]['name'];;
					$p     = $result1[0]['phone'];
					$e     = $result1[0]['email'];
					$age   = $result1[0]['age'];
					$location 	   = $result1[0]['location'];
					$lastlocation  = $result1[0]['last_location'];
					$source   = $result1[0]['utm_source'];
					$campaign = $result1[0]['utm_campaign'];
					

					echo"done";
					exit;
				}else{
					echo"resend";
					exit;
				}
		   }

   	}else{
   		echo "empty";   exit;
   	}
   }


   public function otp_resend(){
  
   	  if( isset($_POST) && count($_POST) > 0 ){
       if($_POST['email']!=''){
			  $otp_val = mt_rand(1000, 9999);
			  $otp_data = array(
							 'otp'          => $otp_val,					   
							);
				$this->db->where('email',$_POST['email']);
			    $result = $this->db->update(OXYGENCLUB, $otp_data);
				
				$data = $this->db->query("select phone,otp from ".OXYGENCLUB." where email ='".$_POST['email']."' ");
				
				$res_data= $data->result_array();

				if(_sendsms($res_data[0]['phone'],$otp_val." is your verification code.")){
				   echo "done";
				}else{
					echo 'error';
				}
				exit;
			   
			}
      }
  }
   

}
?>