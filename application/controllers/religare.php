<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Religare extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->library('XML2Array');
		$this->load->model(array("cache_model", "email_templates","lead_check"));
		$this->db->cache_off();
		
		
	}
   
	public function index(){				
	   $data['city_arr'] = $this->cache_model->get_cache(CITY_CACHE, "id,city,status", CITY, " city ", "");	   
	   
	   $this->load->view("religare", $data);

    }

   
    public function health(){
    	$data['city_arr'] = $this->cache_model->get_cache(CITY_CACHE, "id,city,status", CITY, " city ", "");	 
    	 $this->load->view("religare-health",$data);
    }
    public function health_insurance(){
    	$data['city_arr'] = $this->cache_model->get_cache(CITY_CACHE, "id,city,status", CITY, " city ", "");	 
    	 $this->load->view("religare-health-insurance",$data);
    }
    public function health_success(){
    	
    	 $vr = $this->session->userdata('religare_id');
	    if( isset($vr) &&  $vr > 0 ){
			
			 $this->load->view("religare-health-success");
		}else{
			redirect(SITE_URL.'religare/health');
		}
    }
   	public function healthinsurance_success(){
    	
    	 $vr = $this->session->userdata('religare_id');
	    if( isset($vr) &&  $vr > 0 ){
			
			 $this->load->view("religare-healthinsurance-success");
		}else{
			redirect(SITE_URL.'religare/health');
		}
    }

    public function religarefrm(){

   		date_default_timezone_set('Asia/Calcutta');

		$all_array = all_arrays(); 


	    if( is_array($_POST) && count($_POST) > 0 ){
		    $this->form_validation->set_rules('name', 'FirstName' , 'trim|required');
			$this->form_validation->set_rules('email', 'Email' , 'trim|required');
			$this->form_validation->set_rules('phone', 'Phone No.' , 'trim|required');
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		   }else{
		   	//print_r($_POST);exit;
		   	    $ctype = $_POST['type'];
		   	    if($ctype != "trading"){
		   	    	$type = $_POST['type'];
		   	    }else{
		   	    	$type ="trading";
		   	    }
		   	    $res = $this->db->query("select id, email from ".RELIGARE_USER." where phone ='".$_POST['phone']."' and type = '".$type."'");
			       
			    if($res->num_rows() > 0){
			      
				  echo "done";
				  exit;
			   
			    }else{

			    	$val_otp=mt_rand(1000, 9999);
					$user_name = preg_replace('/\s+/', '', $_POST['name']);
					$user_name = str_replace(' ', '', $user_name );
					if($type != "trading"){

						if($_POST['form_type'] == 1){
							$form_type = "Right-form";
						}else{
							$form_type = "Popup-form";
						}
						$data = array(
									 'name'         => $user_name,
									 'phone'        => $_POST['phone'],
									 'email'        => $_POST['email'],
									 'city'         => $_POST['city'],
									 'otp'          => $val_otp,
									 'familymember_count' => $_POST['family_member'],
									 'children_count' =>$_POST['children'],
									 'elder_age'    => $_POST['elder_member'],
									 'form_type'    => $form_type,
									 'type'         => $_POST['type'],
									 'utm_medium'   => $_POST['utm_medium'],
									 'utm_campaign' => $_POST['utm_campaign'],
									 'utm_source'   => $_POST['utm_source'],
									 'date_created' => date( 'Y-m-d H:i:s' ) 					   
									);

					}else{
					    $data = array(
									 'name'         => $_POST['name'],
									 'phone'        => $_POST['phone'],
									 'email'        => $_POST['email'],
									 'city'         => $_POST['city'],
									 'otp'          => '',
									 'type'         =>"trading",
									 'utm_medium'   => $_POST['utm_medium'],
									 'utm_campaign' => $_POST['utm_campaign'],
									 'utm_source'   => $_POST['utm_source'],
									 'date_created' => date( 'Y-m-d H:i:s' ) 					   
									);
					}
					
					$result = $this->db->insert(RELIGARE_USER, $data);	    
				    $insert_id = $this->db->insert_id($result);
					$this->session->set_userdata('religare_id', $insert_id);
					$this->session->set_userdata('utm_source', trim($_POST['utm_source']));
					$this->session->set_userdata('utm_medium', trim($_POST['utm_medium']));
					$this->session->set_userdata('name', trim($_POST['name']));
					$this->session->set_userdata('email', trim($_POST['email']));
					$this->session->set_userdata('city', trim($_POST['city']));
					$this->session->set_userdata('phone', trim($_POST['phone']));
					
					if($insert_id != "" && $type=="trading"){
						echo '<img src="https://secure.religareonline.com/TradingAccount/Weblead.aspx?phonenumber="'.$_POST['phone'].'"&Name="'.$_POST['name'].'"&email="'.$_POST['email'].'"&city="'.$_POST['city'].'"&utm_source=Adcanopus">';
				    
				    }elseif($insert_id != "" && $type=="health"){
						
						if($_POST['utm_campaign'] == '2924_'){

							if(_sendsms($_POST['phone'],$val_otp." is your verification code.", "RELIGR")){
								//echo "done";
							}
						}
                        
						$api_url = "https://cordysprod.religarehealthinsurance.com/LEADCREATIONREST/RHIS/CreateLead/subject=Optimise_emailer&mobilephone=".$_POST['phone']."&rhi_plan=Care&rhi_product=Health&rhi_leadstage=Cr-Quotation&rhi_agentid=%2020030031&firstname=".$user_name."&emailaddress1=".$_POST['email']."&utmsource=WebAdCanopus&location=".$_POST['city']."&Valid=&medium=%20API&utm_campaign=%20May_Care_AC&utm_content=%20AC_June_19";
						
						$this->db->query("update ".RELIGARE_USER." set api_request = '".$api_url."' where id =".$insert_id); 
						$curl = curl_init();

						  curl_setopt_array($curl, array(
						  CURLOPT_URL => $api_url,
						  CURLOPT_RETURNTRANSFER => true,
						  CURLOPT_ENCODING => "",
						  CURLOPT_MAXREDIRS => 10,
						  CURLOPT_TIMEOUT => 30,
						  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						  CURLOPT_SSL_VERIFYHOST => false,
						  CURLOPT_SSL_VERIFYPEER => false,
						  CURLOPT_CUSTOMREQUEST => "GET",
						  CURLOPT_HTTPHEADER => array(
						    "authorization: Basic U3ltYmlvc3lzVXNlcjpQYXNzd29yZC0x",
						    "cache-control: no-cache",
						    "content-type: application/xml",
						  ),
						));
						$response = curl_exec($curl);
						$err = curl_error($curl);
						if ($err) {
						  $this->db->query("update ".RELIGARE_USER." set api_response = '".$err."' where id =".$insert_id);
						} else {
						  $this->db->query("update ".RELIGARE_USER." set api_response = '".$response."' where id =".$insert_id);
						}
						curl_close($curl); 
						$array_res = XML2Array::createArray($response);
						$html_response=$array_res['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns1:StockController.createLeadResponse']['return']['CreateleadSoapOut']['CreateleadResponse']['CreateleadResult'];
						$result_data = XML2Array::createArray($html_response);
						if( $result_data['Response']){
						    $status = $result_data['Response']['StatusMsg'];
						   if($status=='Success'){
						       $lead_id = $result_data['Response']['LeadId'];
								if($lead_id != ""){
			                        $this->general->update_lead(RELIGARE_USER,$insert_id,$lead_id);
			                    }
			                   echo "done"; 
						       exit;   
						    }else{
						       echo 'failed';
						       exit;

						    }
						}
						


                    }elseif($insert_id != "" && trim($type) == "whizkidzz"){ 
                    	

						$curl = curl_init();

						curl_setopt_array($curl, array(
						  //CURLOPT_URL => "http://202.189.231.22/ConVoxCCS/leadapi.php?mobile=".$_POST['phone']."&process=RELIGARE&info_1=".$user_name."&info_2=NUll&info_3=0000000000&info_4=0000000000&info_5=".$_POST['city']."&info_6=".$_POST['email']."&info_7=Null&info_8=Null&info_9=Null&info_10=Null&info_11=Null&info_12=0&info_13=NULL&info_14=NULL&info_15=NULL&info_16=Null&info_17=null&info_18=2018",

						  CURLOPT_URL => "http://49.248.95.154/ConVoxCCS/leadapi.php?mobile=".$_POST['phone']."&process=RELIGARE&info_1=".$user_name."&info_2=NUll&info_3=0000000000&info_4=0000000000&info_5=".$_POST['city']."&info_6=".$_POST['email']."&info_7=Null&info_8=Null&info_9=Null&info_10=Null&info_11=Null&info_12=0&info_13=NULL&info_14=NULL&info_15=".$_POST['utm_source']."&info_16=".$_POST['utm_campaign']."&info_17=".$_POST['utm_medium']."&info_18=2019",

						  CURLOPT_RETURNTRANSFER => true,
						  CURLOPT_ENCODING => "",
						  CURLOPT_MAXREDIRS => 10,
						  CURLOPT_TIMEOUT => 30,
						  CURLOPT_SSL_VERIFYHOST => false,
						  CURLOPT_SSL_VERIFYPEER => false,
						  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						  CURLOPT_CUSTOMREQUEST => "GET",
						  CURLOPT_HTTPHEADER => array(
						    "cache-control: no-cache",
						  ),
						));

						$response = curl_exec($curl);
						$err = curl_error($curl);

						curl_close($curl);

						if ($err) {
						  echo "cURL Error #:" . $err;
						} else {
						  echo $response;
						}
						exit;
				    }
			    }
			}	   
				
      }else{
	   
	   echo "empty";   
      }

      exit;
    }
	
	
	public function success(){
		
		$vr = $this->session->userdata('religare_id');
	    if( isset($vr) &&  $vr > 0 ){
			
			$this->load->view("religare-success");
		}else{
			redirect(SITE_URL.'religare');
		}
			
			
		
	}


	public function religare_otpverification(){
		$all_array = all_arrays(); 
		
	    if( is_array($_POST) && count($_POST) > 0 ){
		   foreach ($_POST as $k=>$v){
			   $this->form_validation->set_rules($k, $k , 'trim|required');
		   }
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		   }else{
			  
			   $res = $this->db->query("select id, city, phone from ".RELIGARE_USER." where phone ='".$_POST['otp_phone']."' and otp= '".$_POST['otp']."'");
			    if( $res->num_rows() > 0){
					$result1= $res->result_array();
					
			        $data_update = array(
							 'otp_optin'    => 1, 					   
							);
					$this->db->where('id',$result1[0]['id']);
					$result = $this->db->update(RELIGARE_USER, $data_update);

					$this->session->set_userdata('user_id', $result1[0]['id']);
					
					echo "done";
				    exit;
			   
			    }else{
					$res1 = $this->db->query("select id from ".RELIGARE_USER." where phone ='".$_POST['otp_phone']."'");
					if($res1->num_rows() > 0){
						
						echo "resend";
						
					exit;
					}				
				}
			}
		
      }else{
	   
	  	 echo "empty";   
      }

      exit;
    }
	
    public function religare_reset(){
		
	    if( is_array($_POST) && count($_POST) > 0 ){
		  if($_POST['otp_phone']!=''){
		  	 
		  	 $res = $this->db->query("select id, phone from ".RELIGARE_USER." where phone ='".$_POST['otp_phone']."'");
				if($res->num_rows() > 0){
					$result1= $res->result_array();
					
					$uid = $result1[0]['id'];
					
				    $otp_val = mt_rand(1000, 9999);
				    $data_up = array('otp'    => $otp_val, );

					$this->db->where('id',$uid);
				    $result = $this->db->update(RELIGARE_USER, $data_up);			
			
					if(_sendsms($result1[0]['phone'],$otp_val." is your verification code.", "RELIGR")){
						//echo "done";
					}
					echo "done"; 

				exit;
				}else{
					echo "error";
				}
			   
			}
		  
		}

      exit;
    }

   

}
?>