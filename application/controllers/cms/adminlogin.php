<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class adminlogin extends CI_Controller
{
   public function __construct(){
        parent::__construct();
        $this->load->model(array(CMS_FOLDER.'log_model', CMS_FOLDER.'common_model'));
		$this->db->cache_off();
   }

   public function index(){ 
         if(($this->session->userdata('user_name')!="")){
              redirect(CMS_FOLDER.'dashboard');
         }else{
              $data['title']= 'Home';
              $this->load->view(CMS_FOLDER."header1");
              $this->load->view(CMS_FOLDER."login_view", $data);
         }
   }
  
	public function login(){
         // field name, error message, validation rules
          $this->form_validation->set_rules('user_name', 'user name', 'trim|required');
          $this->form_validation->set_rules('pass', 'Password', 'trim|required|min_length[4]|max_length[32]');
          
		  if($this->form_validation->run() == FALSE){
			   // echo "validation fails";
			   redirect(CMS_FOLDER.'adminlogin');
          
		  }else{
			    
			   $user_name= $this->input->post('user_name');
               $password = $this->input->post('pass');
			   
			   //MODEL REQUEST
               $result   = $this->log_model->login($user_name,$password);
			   
			   if($result) {
				  redirect(CMS_FOLDER.'dashboard');
				}else {   
				  $this->session->set_flashdata('message', 'invalid login credentials');
				  redirect(CMS_FOLDER.'adminlogin/index');
		       }
          }
    }
	
	
	
}
?>