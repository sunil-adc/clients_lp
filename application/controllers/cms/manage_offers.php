<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class manage_offers extends CI_Controller
{
   public function __construct()
   {
        parent::__construct();
		$this->load->model(array( CMS_FOLDER.'common_model', CMS_FOLDER.'/db_function'));
		$this->db->cache_off();
		$this->global_country =  ($this->session->userdata('cms_country') != "" ) ? $this->session->userdata('cms_country') : '';
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
		
   }

   public function index($perpage = 10, $offset = 0){
	   
	   $perpage = 10;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, NULL, 0);
   }		
   
    public function page($perpage = 10, $btn_search = NULL, $search_field = NULL, $search_txt = NULL, $status=NULL, $offset = 0){
       
	    $menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= __CLASS__;
		
		// PAGE DETAILS
		$page_details['cur_controller'] = $cur_controller;
		$page_details['menu']           = $menu;
		
	   // DECLARE NULL VARIABLES 
	    $tablename    = OFFERS;
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($search_field == NULL && $search_txt == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$data['search_field'] 	= "";
		$data['search_txt'] 	= "";
		$data['status']         = "";
		
	   $cur_controller 	= strtolower(__CLASS__);
	   $query_string = NULL;
	   $full_path = FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
	   
	    if( (trim($this->input->post("btn_search")) != "" || $btn_search != "") || 
			((trim($this->input->post("search_field")) != "" || $search_field != "") && 
			(trim($this->input->post("search_txt")) != "" || $search_txt != "")) || 
			(trim($this->input->post("status")) != "" || $status != "")) {
			
			// STORE SEARCH VALUE IN DATA ARRAY
			$data['btn_search']		= ($btn_search != "") 	? $btn_search 	: $this->input->post("btn_search");
			$data['search_field'] 	= ($search_field != "") ? $search_field : $this->input->post("search_field");
			$data['search_txt']		= ($search_txt != "") 	? $search_txt 	: $this->input->post("search_txt");
			$data['status']		    = ($status != "") 	    ? $status 	    : $this->input->post("status");
			 
			if($data['search_txt'] !=	"" &&  $data['search_field']!= ""  && $data['search_txt'] != '0'){
			    
				$query_string = $perpage."/".$data['btn_search']."/".$data['search_field']."/".$data['search_txt']."/".$data['status'];
				$whr = $data['search_field']." = '".$data['search_txt']."' AND status=".$data['status'];
			      
			}else if($data['status'] != "") {
			
				$query_string = $perpage."/".$data['btn_search']."/".$data['search_field']."/0/".$data['status'];
				$whr = "status =".$data['status'];
			}
			// CHECK FOR EXISTING CLAUSE
			if($where == "") {
				$where = " where ".$whr;
			} else {
				$where .= " and ".$whr;
			}
		} else {
			$data['status'] = '1';
			$query_string = $perpage."/";
			$where = " where status =".$data['status'];
		}
	   
	   
	    $order_by = " order by date_created desc";
	   
		$sel_query="SELECT id, tag_line, offers_name, campaign_name, offers_url, bank_logo, offer_image, status, date_created FROM ".$tablename.$where.$order_by;
		
	   if($this->uri->segment(9) == "") {
			$config['uri_segment'] 	= 5;
		} else {
			$config['uri_segment'] 	= 9;
		}
		
	   // MODIFY FULL PATH
		$full_path .= $query_string;
		
		$config['total_rows'] 		= $this->db_function->count_record($sel_query, false);
		$config['per_page'] 		= $perpage;
		$config['base_url'] 		= $full_path;
		$choice 					= $config['total_rows'] / $config["per_page"];
		$config['num_links'] 		= 2;
		$config['full_tag_open'] 	= '<div id="paging" style="float:right; "><ul style="clear:left;">';
		$config['full_tag_close'] 	= '</ul></div>';
		$config['anchor_class'] 	= 'class="btn" ';
		$config['first_tag_open'] 	= '<li class="num_off">';
		$config['first_tag_close'] 	= '</li>';
		$config['last_tag_open'] 	= '<li class="num_off">';
		$config['last_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="num_on">';
		$config['cur_tag_close'] 	= '</li>';
		$config['num_tag_open'] 	= '<li class="num_off">';
    	$config['num_tag_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="num_off">';
    	$config['prev_tag_close'] 	= '</li>';
		$config['next_tag_open'] 	= '<li class="num_off">';
    	$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= 'PREVIOUS';
    	$config['next_link'] 		= 'NEXT';
		$config['use_page_numbers'] = FALSE;
		// PAGINATION PARAMETER VALUES END 				
		
		// PAGINATION PARAMETER INITIALIZE 
		$this->pagination->initialize($config);
		
		// SQL QUERY WITH OFFSET AND PERPAGE LIMIT
		$sql = $sel_query." limit ".$offset.", ".$perpage;
	
        $data["details"] = $this->db_function->get_data($sql);
		
		// GET THE DATA FROM PAGINATION
		$data["today_count"] = $config['total_rows'];
		
        $data["links"] = $this->pagination->create_links();
		
	    $this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $page_details);
	    $this->load->view(CMS_FOLDER."manage-offers" ,  $data);
		$this->load->view(CMS_FOLDER.'footer'); 
   

   }   
   
   public function add_form(){
	    
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= __CLASS__;
		
		// PAGE DETAILS
		$page_details['cur_controller'] = $cur_controller;
		$page_details['menu']           = $menu;
		
	   $this->load->view(CMS_FOLDER."header");
	   $this->load->view(CMS_FOLDER."sidebar", $page_details);
	   $this->load->view(CMS_FOLDER."add-offers" );
	   $this->load->view(CMS_FOLDER.'footer'); 
   }
   
   public function add(){
	   
       $this->form_validation->set_rules('tag_line', 'Tag line', 'trim|required');
	   $this->form_validation->set_rules('offers_name', 'offer name', 'required');
	   $this->form_validation->set_rules('campaign_name', 'Campaign name', 'required');	   
	   $this->form_validation->set_rules('offers_url', 'Offers url', 'required');
	   
	   if($this->form_validation->run() == FALSE){
		  $this->session->set_flashdata('error', "Fields marked * are mandatory"); 
		  redirect(SITE_URL."index.php/cms/manage_offers/add_form");
       
	   }else{
		    if($this->db_function->get_data("SELECT offers_name, offers_url FROM ".OFFERS." WHERE offers_url = '".trim($this->input->post('offers_url ')."'"))) {
		   
			   $this->session->set_flashdata('error', 'offer Alredy existed, ');              
			   redirect(SITE_URL."index.php/cms/manage_offers/add_form"); 
			   
		    }else{
				
			   $bank_logo = $_FILES['bank_logo']['name'];
			   $offer_image = $_FILES['offer_image']['name'];
				   
			   $setting_details=array();
			   $setting_details['tag_line']                = trim($this->input->post('tag_line'));
			   $setting_details['offers_name']             = trim($this->input->post('offers_name'));
			   $setting_details['campaign_name']           = trim($this->input->post('campaign_name'));
			   $setting_details['offers_url']              = trim($this->input->post('offers_url'));
			   $setting_details['bank_logo']               = $bank_logo;
			   $setting_details['offer_image']             = $offer_image;
			   $setting_details['date_created']            = date('Y-m-d H:i:s');			   
			   
			   $this->db->insert(OFFERS,$setting_details);
			   
			   move_uploaded_file($_FILES['bank_logo']['tmp_name'], BANK_BANNERS.$bank_logo);
			   move_uploaded_file($_FILES['offer_image']['tmp_name'], BANK_BANNERS.$offer_image);
			   
			   $this->session->set_flashdata('success', 'Offer by the name '.$setting_details['offers_name'].' added');              
			   redirect(SITE_URL."index.php/cms/manage_offers/add_form"); 
		   }
	   }
   }
   
   public function edit_form($id=''){
	    $menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= __CLASS__;
		
		// PAGE DETAILS
		$page_details['cur_controller'] = $cur_controller;
		$page_details['menu']           = $menu;
		
	   $tablename = OFFERS;
	   if($id !=""){
		   $query=$this->db->query("SELECT
										id, tag_line, offers_name, offers_url, 
										bank_logo, offer_image, campaign_name, 
										status, date_created 
									FROM
									 	".$tablename."
									WHERE 
										id= ".$id);
		   
		   if ( $query->num_rows() > 0) {
				foreach ($query->result() as $p) {
				  
				  $data['setting'][] = $p;
				  $this->load->view(CMS_FOLDER."header");
				  $this->load->view(CMS_FOLDER."sidebar", $page_details);	    
				  $this->load->view(CMS_FOLDER."edit-offers" , $data);
				  $this->load->view(CMS_FOLDER.'footer'); 
				}
			}else{
		         redirect(CMS_FOLDER.'manage_offers');
	        }
	   }
   }
   
   
   public function edit(){
	   
	   $id=$this->input->post('id');
	   if( $id){
           
		   $this->form_validation->set_rules('tag_line', 'Tag line', 'trim|required|xss_clean');
		   $this->form_validation->set_rules('offers_name', 'offer name', 'required');
		   $this->form_validation->set_rules('campaign_name', 'Campaign name', 'required');
		   $this->form_validation->set_rules('offers_url', 'Offers url', 'required');
	   
		   if($this->form_validation->run() == FALSE){
			  $this->session->set_flashdata('message', validation_errors());
			  redirect(CMS_FOLDER.'manage_offers/edit_form/'.$id);
		   
		   }else{
			   
			   $this->db->where("id",$_POST['id']);
			   
			   $bank_logo = $_FILES['bank_logo']['name'];
			   $offer_image = $_FILES['offer_image']['name'];
				   
			   $setting_details=array();
			   $setting_details['tag_line']             = trim($this->input->post('tag_line'));
			   $setting_details['offers_name']          = trim($this->input->post('offers_name'));
			   $setting_details['campaign_name']        = trim($this->input->post('campaign_name'));
			   $setting_details['offers_url']           = trim($this->input->post('offers_url'));
			   $setting_details['status']               = trim($this->input->post('status'));
			   $setting_details['date_created']         = date('Y-m-d H:i:s');			   
			   			   
			   if( $_FILES['bank_logo']['name']){
				   $setting_details['bank_logo']        = $bank_logo;
				   move_uploaded_file($_FILES['bank_logo']['tmp_name'], BANK_BANNERS.$bank_logo);
			   }
			   
			   if( $_FILES['offer_image']['name']){
				   $setting_details['offer_image']      = $offer_image;
				   move_uploaded_file($_FILES['offer_image']['tmp_name'], BANK_BANNERS.$offer_image);
			   }
			   
			   $this->db->update(OFFERS,$setting_details);
			   
			   $this->session->set_flashdata('success', 'Succesfully updated', 'Succesfully updated');
		       
			   redirect(CMS_FOLDER.'manage_offers');  
	      }
	   }else{   
	       
		   $this->session->set_flashdata('error', 'Error Ocurred');
		   redirect(CMS_FOLDER.'manage_offers');
	   }
   }
   
   
}

?>