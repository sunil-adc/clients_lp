<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class manage_banklist extends CI_Controller
{
   public function __construct()
   {
        parent::__construct();
		global $page_details;
		$this->load->model(array( CMS_FOLDER.'common_model', CMS_FOLDER.'/db_function'));
		$this->db->cache_off();
		$this->tablename	= BANKLIST;	
		$this->page_details['cur_controller'] = strtolower(__CLASS__);
		$this->page_details['menu']           = $this->common_model->Menu_Array();;
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
		
   }

   public function index($perpage = 10, $offset = 0){
		
		$perpage = 1000;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, NULL, 0);
   }		
   
    public function page($perpage = 1000, $btn_search = NULL, $search_field = NULL, $search_txt = NULL, $status=NULL, $offset = 0){
       
	   if($search_field == NULL && $search_txt == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		// DECLARE NULL VARIABLES 
		$cur_controller 		= strtolower(__CLASS__);
		$query_string       	= NULL;
		$full_path 				= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';	   
	    $query_string 			= NULL;
		$where = $whr 			= NULL;
		$tablename				= BANKLIST;
		$data['search_field'] 	= "";
		$data['search_txt'] 	= "";
		$data['status']         = "";
		$data['page_title']	 	= "Bank list";
		$data['add_page_title']	= "Add Bank";
		$data['add_edit_page'] 	= $cur_controller."/add_edit_form";
		$data['tablename']	 	= $tablename;
		$data['primary_field']	= "id";
		

	    if( (trim($this->input->post("btn_search")) != "" || $btn_search != "") || 
			((trim($this->input->post("search_field")) != "" || $search_field != "") && 
			(trim($this->input->post("search_txt")) != "" || $search_txt != "")) || 
			(trim($this->input->post("status")) != "" || $status != "")) {
			
			// STORE SEARCH VALUE IN DATA ARRAY
			$data['btn_search']		= ($btn_search != "") 	? $btn_search 	: $this->input->post("btn_search");
			$data['search_field'] 	= ($search_field != "") ? $search_field : $this->input->post("search_field");
			$data['search_txt']		= ($search_txt != "") 	? $search_txt 	: $this->input->post("search_txt");
			$data['status']		    = ($status != "") 	    ? $status 	    : $this->input->post("status");
			 
			if($data['search_txt'] !=	"" &&  $data['search_field']!= ""  && $data['search_txt'] != '0'){
			    
				$query_string = $perpage."/".$data['btn_search']."/".$data['search_field']."/".$data['search_txt']."/".$data['status'];
				$whr = $data['search_field']." = '".$data['search_txt']."' AND status=".$data['status'];
			      
			}else if($data['status'] != "") {
			
				$query_string = $perpage."/".$data['btn_search']."/".$data['search_field']."/0/".$data['status'];
				$whr = "status =".$data['status'];
			}
			// CHECK FOR EXISTING CLAUSE
			if($where == "") {
				$where = " where ".$whr;
			} else {
				$where .= " and ".$whr;
			}
		} else {
			//$data['status'] = '1';
			$query_string = $perpage."/";
			//$where = " where status =".$data['status'];
		}
	   
	    $order_by = " order by date_created desc";
	   
		$sel_query="SELECT id, bank_name, bank_logo, status, date_created, date_updated FROM ".$tablename.$order_by;
		
	   if($this->uri->segment(9) == "") {
			$config['uri_segment'] 	= 5;
		} else {
			$config['uri_segment'] 	= 9;
		}
		
	   // MODIFY FULL PATH
		$full_path .= $query_string;
		
		$config['total_rows'] 		= $this->db_function->count_record($sel_query, false);
		$config['per_page'] 		= $perpage;
		$config['base_url'] 		= $full_path;
		$choice 					= $config['total_rows'] / $config["per_page"];
		$config['num_links'] 		= 2;
		$config['full_tag_open'] 	= '<div id="paging" style="float:right; "><ul style="clear:left;">';
		$config['full_tag_close'] 	= '</ul></div>';
		$config['anchor_class'] 	= 'class="btn" ';
		$config['first_tag_open'] 	= '<li class="num_off">';
		$config['first_tag_close'] 	= '</li>';
		$config['last_tag_open'] 	= '<li class="num_off">';
		$config['last_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="num_on">';
		$config['cur_tag_close'] 	= '</li>';
		$config['num_tag_open'] 	= '<li class="num_off">';
    	$config['num_tag_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="num_off">';
    	$config['prev_tag_close'] 	= '</li>';
		$config['next_tag_open'] 	= '<li class="num_off">';
    	$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= 'PREVIOUS';
    	$config['next_link'] 		= 'NEXT';
		$config['use_page_numbers'] = FALSE;
		// PAGINATION PARAMETER VALUES END 				
		
		// PAGINATION PARAMETER INITIALIZE 
		$this->pagination->initialize($config);
		
		// SQL QUERY WITH OFFSET AND PERPAGE LIMIT
		$sql = $sel_query." limit ".$offset.", ".$perpage;
	
        $data["details"] = $this->db_function->get_data($sql);
		
		// GET THE DATA FROM PAGINATION
		$data["today_count"] = $config['total_rows'];
		
        $data["links"] = $this->pagination->create_links();
		
	    $this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
	    $this->load->view(CMS_FOLDER."manage-banklist" ,  $data);
		$this->load->view(CMS_FOLDER.'footer'); 

   }   
   
   public function add_edit_form($mode = "add", $id = NULL){
	   
		// DETAILS WE WANT TO SEND IN VIEW
		$data['page_name']	 	 	= "Bank list";
		$data['manage_page_title']	= "Manage ".$data['page_name'];
		$data['page_title']	 		= ucfirst($mode)." ".$data['page_name'];
		$data['manage_page'] 		= $this->page_details['cur_controller'];
		$data['add_page'] 			= $this->page_details['cur_controller']."/add_edit_form";
		$data['tablename']	 		= $this->tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$this->page_details['cur_controller']."/add_edit_action";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] = NULL;
		$data['result_data'][$data['primary_field']]	    = NULL;
		$data['result_data']['client_type'] 				= NULL;
		$data['result_data']['bank_name'] 					= NULL;
		$data['result_data']['bank_url']					= NULL;
		$data['result_data']['bank_logo']				    = NULL;
		$data['result_data']['tag_line']					= NULL;
		$data['result_data']['status']					    = NULL;
		$data['result_data']['date_created']				= NULL;
		$data['result_data']['date_updated']				= NULL;
		
		
		if ($mode == 'edit' && is_numeric($id)) {
			$val = $this->db_function->get_single_row($this->tablename, '	id, bank_name, 
																	bank_logo,		status, 
																	date_created, 		date_updated', 
																	$data['primary_field'].'='.$id);
			$data['result_data'] = $val;	
		}
				
		$this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
		$this->load->view(CMS_FOLDER."add-banklist", $data);
		$this->load->view(CMS_FOLDER.'footer'); 
   }
   
   public function add_edit_action(){
		
		$this->form_validation->set_rules('bank_name', 'bank name', 'required');
		$this->form_validation->set_rules('status',    'status',  	'required');
		
		if( ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				$bank_id = (isset($_POST['id']) && is_numeric($_POST['id'])) ? $_POST['id'] : NULL;				
				$_POST['mode'] = strtolower($_POST['mode']);
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['id'])) { 		
					// SET THE WHERE CLAUSE
					$this->db->where("id",$_POST['id']);
					
					// SET THE DATA
					$data = array(
									'bank_name'         => $_POST['bank_name'],
									'status'            => $_POST['status'],
									'date_updated'      => date('Y-m-d H:i:s')
								);
					
					// UPDATE QUERY
					$this->db->update($this->tablename,$data);
					$this->session->set_flashdata('success', 'Updated successfully');
					
				} else if ( trim ($_POST['mode']) == 'add') { 			
					
					// SET THE DATA FOR INSERTION
					$data = array(
									'bank_name'         => $_POST['bank_name'],
									'status'            => $_POST['status'],
									'date_created'      => date('Y-m-d H:i:s')
								);
						
                    // INSERT QUERY
					$this->db->insert($this->tablename,$data);
					$bank_id = $this->db->insert_id();
					$this->session->set_flashdata('success', 'Added successfully');
				}
				
				
				if (isset ($_FILES['bank_image']['name'])) {
					
					$image_ext = $_FILES['bank_image']['name'];
					if($image_ext != false) {
						if($_FILES['bank_image']['size'] > IMAGE_UPLOAD_MAX_SIZE) {
							// IF MORE THAN GENERATE FLASH MESSAGE
							$this->session->set_flashdata('error', 'File size must be less than or equals to '.
																	(IMAGE_UPLOAD_MAX_SIZE/1024/1024).' MB');
						} else {
							// CHECK IMAGE WE ARE TRYING TO UPLOAD THAT DIRECTORY IS AVAILABLE
							if (is_dir("../".BANK_IMG_FOLDER) == false) {
								
								// DELETE THE CURRENT IMAGE FILE FROM HDD
								$bank_image = $this->db_function->get_single_value($this->tablename, 'bank_logo', "id=".$bank_id, false, false);
								
								// FINALLY UPLOAD THE FILE AFTER CHECKING OF EVERYTHING 
								$new_img_name = get_random_chracter('12','3').".".$image_ext;
								
								move_uploaded_file($_FILES['bank_image']['tmp_name'],"./".BANK_IMG_FOLDER.$new_img_name);
								
								// DELETE IMAGE FROM HDD
								unlink("./".BANK_IMG_FOLDER.$bank_image);
								
								$bank_image = NULL;
								$bank_image['bank_logo'] = $new_img_name;
								
								// SET THE WHERE CLAUSE
								$this->db->where("id", $bank_id);
								$this->db->update($this->tablename, $bank_image);
							}
						}
					}
				}
				
				
				//PAGE REDIRECTION
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['id'])) {
					redirect(CMS_FOLDER.$_POST['add_page']."/edit/".$_POST['id']);
				} else if ( trim ($_POST['mode']) == 'add') {
					redirect(CMS_FOLDER.$_POST['add_page']."/add/");
				}
				
				
				
			} else {
				
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect(CMS_FOLDER.$_POST['add_page']);
				
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again');
			redirect(CMS_FOLDER.$_POST['add_page']);	
		}
	}

   
   
}

?>