<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class download extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model(array("cache_model", "email_templates"));
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), $this->input->get('bnr'), $this->input->get('country'),  $this->input->get('zoneid'),  $this->input->get('mobile_track'), $this->input->get('ClickID'));
		
	}
   
	public function index(){				
	   
	   echo "Access Denied";
	   die();

    }
   
   
	public function client($cl){
		
		$all_array = all_arrays(); 
		
		$from_date = $_POST['from_date'];
		$to_date   = $_POST['to_date'];
		
		if($to_date != "" && $from_date != ""){
			
			$to_excel = "";
			$to_excel .= "<table border='1'>";
			$to_excel .= "<tr>";
			$to_excel .= "<th>No</th>";
			$to_excel .= "<th>Name</th>";
			$to_excel .= "<th>Phone</th>";
			$to_excel .= "<th>Email</th>";
			$to_excel .= "<th>City</th>";
			$to_excel .= "<th>Utm Source</th>";
			$to_excel .= "<th>Utm Sub</th>";
			$to_excel .= "<th>Appointment date</th>";
			$to_excel .= "<th>Time Slot</th>";
			$to_excel .= "<th>Address</th>";
			$to_excel .= "<th>Test</th>";
			$to_excel .= "<th>Date</th>";
			$to_excel .= "</tr>";
			
			$sel_query =$this->db->query("Select 
							 id, name, phone, email, city, clients, 
							 test, utm_source, publisher, appointment_date,
							 time_slot, address,
							 status, date_created 
						 From 
							 ".USER_DATA." 
						 Where 
							clients = ".$cl." AND 
							DATE_FORMAT(date_created, '%Y-%m-%d') between '".$from_date."' and '".$to_date."' 
						 ORDER BY  
							 date_created desc");
			
				$srl = 1;
				foreach ($sel_query->result() as $val_user){
				
					$to_excel .= "<td>".($srl)."</td>";
					$to_excel .= "<td>".($val_user->name)."</td>";
					$to_excel .= "<td>".($val_user->phone)."</td>";
					$to_excel .= "<td>".($val_user->email)."</td>";
					$to_excel .= "<td>".($val_user->city)."</td>";
					$to_excel .= "<td>".($val_user->utm_source)."</td>";
					$to_excel .= "<td>".($val_user->publisher)."</td>";
					$to_excel .= "<td>".($val_user->appointment_date)."</td>";
					if($cl == 2){
						$to_excel .= "<td>".($val_user->time_slot != 0 ? $all_array['SRL_TIME'][$val_user->time_slot] : '')."</td>";
					}else if($cl == 3){
						$to_excel .= "<td>".($val_user->time_slot != 0 ? $all_array['JUBINATION_SLOT'][$val_user->time_slot] : '')."</td>";
					}
					$to_excel .= "<td>".($val_user->address)."</td>";
					//$to_excel .= "<td>".($val_user->test != 0 ? $all_array['SRL_TEST'][$val_user->test][0] : '')."</td>";
					$to_excel .= "<td>".($val_user->test)."</td>";
					$to_excel .= "<td>".($val_user->date_created)."</td>";
					$to_excel .= "</tr>";
				
					$srl++;
				}
				
				header("Content-Type:  application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=srl_excel-".time().".xls");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				echo $to_excel;		
		}
		
	}
	
	public function medlife(){
		
		$all_array = all_arrays(); 
		
		$from_date = $_POST['from_date'];
		$to_date   = $_POST['to_date'];
		
		if($to_date != "" && $from_date != ""){
			
			$to_excel = "";
			$to_excel .= "<table border='1'>";
			$to_excel .= "<tr>";
			$to_excel .= "<th>No</th>";
			$to_excel .= "<th>Name</th>";
			$to_excel .= "<th>Phone</th>";
			$to_excel .= "<th>City</th>";

			if($this->session->userdata('admin_role_id') != 9){
							
				$to_excel .= "<th>Email</th>";
				$to_excel .= "<th>Optin</th>";
				$to_excel .= "<th>Utm Source</th>";
				$to_excel .= "<th>Utm medium</th>";
				
			}
			$to_excel .= "<th>Plan</th>";
			$to_excel .= "<th>Date</th>";
			$to_excel .= "</tr>";
			
			$sel_query =$this->db->query("Select 
											 id, name, phone, city,
											 email, plan, otp_optin, 
											 utm_source,utm_campaign, date_created 
										 From 
											 ".MEDLIFE_USER."
										where
											DATE_FORMAT(date_created, '%Y-%m-%d') between '".$from_date."' and '".$to_date."' 
										ORDER BY  
											 date_created desc ");
				
				$srl = 1;
				foreach ($sel_query->result() as $val_user){

					//$date = date('Y-m-d', strtotime($val_user->date_created));
					
					$to_excel .= "<td>".($srl)."</td>";
					$to_excel .= "<td>".($val_user->name)."</td>";
					$to_excel .= "<td>".($val_user->phone)."</td>";
					$to_excel .= "<td>".($val_user->city)."</td>"; 
					
					if($this->session->userdata('admin_role_id') != 9){
			
						$to_excel .= "<td>".($val_user->email)."</td>";
						$to_excel .= "<td>".($val_user->otp_optin)."</td>";
						$to_excel .= "<td>".($val_user->utm_source)."</td>";
						$to_excel .= "<td>".($val_user->utm_campaign)."</td>";
						
					}

					$to_excel .= "<td>".($val_user->plan != '' && $val_user->plan != '' ? $all_array['MEDLIFE_TEST'][$val_user->plan] : '' )."</td>";
					$to_excel .= "<td>".($val_user->date_created)."</td>";
					$to_excel .= "</tr>";
				
					$srl++;
				}
				
				header("Content-Type:  application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=medlife_excel-".time().".xls");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				echo $to_excel;		
		}
	}
	
	public function sheltrex(){
		
		$all_array = all_arrays(); 
		
		$from_date = $_POST['from_date'];
		$to_date   = $_POST['to_date'];
		
		if($to_date != "" && $from_date != ""){
			
			$to_excel = "";
			$to_excel .= "<table border='1'>";
			$to_excel .= "<tr>";
			$to_excel .= "<th>No</th>";
			$to_excel .= "<th>Name</th>";
			$to_excel .= "<th>Phone</th>";
			$to_excel .= "<th>Email</th>";
			$to_excel .= "<th>Utm Source</th>";
			$to_excel .= "<th>Camp1</th>";
			$to_excel .= "<th>Camp2</th>";
			$to_excel .= "<th>Utm mobile1</th>";
			$to_excel .= "<th>Utm category</th>";
			$to_excel .= "<th>Date</th>";
			$to_excel .= "</tr>";
			
			$sel_query =$this->db->query("Select 
											  id, name, phone, email,city,utm_source,utm_camp1,utm_camp2,utm_mobile1,utm_category,
											date_created  
										 From 
											 ".SHELTERX_USER."
										where
											DATE_FORMAT(date_created, '%Y-%m-%d') between '".$from_date."' and '".$to_date."' 
										ORDER BY  
											 date_created desc ");
			
				$srl = 1;
				foreach ($sel_query->result() as $val_user){
				
					$to_excel .= "<td>".($srl)."</td>";
					$to_excel .= "<td>".($val_user->name)."</td>";
					$to_excel .= "<td>".($val_user->phone)."</td>";
					$to_excel .= "<td>".($val_user->email)."</td>";
					$to_excel .= "<td>".($val_user->utm_source)."</td>";
					$to_excel .= "<td>".($val_user->utm_camp1)."</td>";
					$to_excel .= "<td>".($val_user->utm_camp2)."</td>";
					$to_excel .= "<td>".($val_user->utm_mobile1)."</td>";
					$to_excel .= "<td>".($val_user->utm_category)."</td>";
					$to_excel .= "<td>".($val_user->date_created)."</td>";
					$to_excel .= "</tr>";
				
					$srl++;
				}
				
				header("Content-Type:  application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=Sheltrex_excel-".time().".xls");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				echo $to_excel;		
		}
	}
	
	
		public function sheltrex1(){
		
		$all_array = all_arrays(); 
		
		$from_date = $_POST['from_date'];
		$to_date   = $_POST['to_date'];
		
		if($to_date != "" && $from_date != ""){
			
			$to_excel = "";
			$to_excel .= "<table border='1'>";
			$to_excel .= "<tr>";
			$to_excel .= "<th>No</th>";
			$to_excel .= "<th>Name</th>";
			$to_excel .= "<th>Phone</th>";
			$to_excel .= "<th>Email</th>";
			$to_excel .= "<th>Utm Source</th>";
			$to_excel .= "<th>Camp1</th>";
			$to_excel .= "<th>Camp2</th>";
			$to_excel .= "<th>Utm mobile1</th>";
			$to_excel .= "<th>Utm category</th>";
			$to_excel .= "<th>Date</th>";
			$to_excel .= "</tr>";
			
			$sel_query =$this->db->query("Select 
											  id, name, phone, email,city,utm_source,utm_camp1,utm_camp2,utm_mobile1,utm_category,
											date_created  
										 From 
											 ".SHELTERX_USER."
										where
											DATE_FORMAT(date_created, '%Y-%m-%d') between '".$from_date."' and '".$to_date."' 
										ORDER BY  
											 date_created desc ");
			
				$srl = 1;
				foreach ($sel_query->result() as $val_user){
				
					$to_excel .= "<td>".($srl)."</td>";
					$to_excel .= "<td>".($val_user->name)."</td>";
					$to_excel .= "<td>".($val_user->phone)."</td>";
					$to_excel .= "<td>".($val_user->email)."</td>";
					$to_excel .= "<td>".($val_user->utm_source)."</td>";
					$to_excel .= "<td>".($val_user->utm_camp1)."</td>";
					$to_excel .= "<td>".($val_user->utm_camp2)."</td>";
					$to_excel .= "<td>".($val_user->utm_mobile1)."</td>";
					$to_excel .= "<td>".($val_user->utm_category)."</td>";
					$to_excel .= "<td>".($val_user->date_created)."</td>";
					$to_excel .= "</tr>";
				
					$srl++;
				}
				
				header("Content-Type:  application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=Sheltrex_excel-".time().".xls");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				echo $to_excel;		
		}
	}
	
	public function policyx_health(){
		
		$all_array = all_arrays(); 
		
		$from_date = $_POST['from_date'];
		$to_date   = $_POST['to_date'];
		
		if($to_date != "" && $from_date != ""){
			
			$to_excel = "";
			$to_excel .= "<table border='1'>";
			$to_excel .= "<tr>";
			$to_excel .= "<th>No</th>";
			$to_excel .= "<th>Name</th>";
			$to_excel .= "<th>Phone</th>";
			$to_excel .= "<th>Email</th>";
			$to_excel .= "<th>DOB</th>";
			$to_excel .= "<th>City</th>";
			$to_excel .= "<th>Utm Source</th>";
			$to_excel .= "<th>Utm Medium</th>";
			$to_excel .= "<th>Utm Campaign</th>";
			$to_excel .= "<th>Date</th>";
			$to_excel .= "</tr>";
			
			$sel_query =$this->db->query("Select 
											   id, name, phone, email, city, dob, otp, otp_optin, utm_source, utm_campaign, utm_medium, date_created  
										 From 
											 ".POLICY_HEALTH."
										where
											DATE_FORMAT(date_created, '%Y-%m-%d') between '".$from_date."' and '".$to_date."' 
										ORDER BY  
											 date_created desc ");
			
				$srl = 1;
				foreach ($sel_query->result() as $val_user){
				
					$to_excel .= "<td>".($srl)."</td>";
					$to_excel .= "<td>".($val_user->name)."</td>";
					$to_excel .= "<td>".($val_user->phone)."</td>";
					$to_excel .= "<td>".($val_user->email)."</td>";
					$to_excel .= "<td>".($val_user->dob)."</td>";
					$to_excel .= "<td>".($val_user->city)."</td>";
					$to_excel .= "<td>".($val_user->utm_source)."</td>";
					$to_excel .= "<td>".($val_user->utm_medium)."</td>";
					$to_excel .= "<td>".($val_user->utm_campaign)."</td>";
					$to_excel .= "<td>".($val_user->date_created)."</td>";
					$to_excel .= "</tr>";
				
					$srl++;
				}
				
				header("Content-Type:  application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=Policyx_health_excel-".time().".xls");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				echo $to_excel;		
		}
	}
	
	public function policyx_life(){
		
		$all_array = all_arrays(); 
		
		$from_date = $_POST['from_date'];
		$to_date   = $_POST['to_date'];
		
		if($to_date != "" && $from_date != ""){
			
			$to_excel = "";
			$to_excel .= "<table border='1'>";
			$to_excel .= "<tr>";
			$to_excel .= "<th>No</th>";
			$to_excel .= "<th>Name</th>";
			$to_excel .= "<th>Phone</th>";
			$to_excel .= "<th>Email</th>";
			$to_excel .= "<th>DOB</th>";
			$to_excel .= "<th>Gender</th>";
			$to_excel .= "<th>City</th>";
			$to_excel .= "<th>Utm Source</th>";
			$to_excel .= "<th>Utm Medium</th>";
			$to_excel .= "<th>Utm Campaign</th>";
			$to_excel .= "<th>Date</th>";
			$to_excel .= "</tr>";
			
			$sel_query =$this->db->query("Select 
											   id, name, phone, email, city,gender, dob, otp, otp_optin, utm_source, utm_campaign, utm_medium, date_created  
										 From 
											 ".POLICY_LIFE."
										where
											DATE_FORMAT(date_created, '%Y-%m-%d') between '".$from_date."' and '".$to_date."' 
										ORDER BY  
											 date_created desc ");
			
				$srl = 1;
				foreach ($sel_query->result() as $val_user){
				
					$to_excel .= "<td>".($srl)."</td>";
					$to_excel .= "<td>".($val_user->name)."</td>";
					$to_excel .= "<td>".($val_user->phone)."</td>";
					$to_excel .= "<td>".($val_user->email)."</td>";
					$to_excel .= "<td>".($val_user->dob)."</td>";
					$to_excel .= "<td>".($val_user->gender)."</td>";
					$to_excel .= "<td>".($val_user->city)."</td>";
					$to_excel .= "<td>".($val_user->utm_source)."</td>";
					$to_excel .= "<td>".($val_user->utm_medium)."</td>";
					$to_excel .= "<td>".($val_user->utm_campaign)."</td>";
					$to_excel .= "<td>".($val_user->date_created)."</td>";
					$to_excel .= "</tr>";
				
					$srl++;
				}
				
				header("Content-Type:  application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=Policyx_health_excel-".time().".xls");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				echo $to_excel;		
		}
	}
	
	
	
	public function religare()
    {
         
        $all_array = all_arrays();
        
        $from_date = $_POST['from_date'];
        $to_date   = $_POST['to_date'];
         if ($_POST['utm_source'] != "") {
            $utm_source = "utm_source = '" . $_POST['utm_source'] . "' and ";
        } else {
            $utm_source = "";
        }
        if ($_POST['type'] != "") {
            $type = "type = '" . $_POST['type'] . "' and ";
        } else {
            $type = "";
        }
        
        if ($to_date != "" && $from_date != "") {
            
            $to_excel = "";
            $to_excel .= "<table border='1'>";
            $to_excel .= "<tr>";
            $to_excel .= "<th>No</th>";
            $to_excel .= "<th>Name</th>";
            $to_excel .= "<th>Phone</th>";
            $to_excel .= "<th>Email</th>";
            $to_excel .= "<th>City</th>";
            $to_excel .= "<th>Type</th>";
            $to_excel .= "<th>Utm Source</th>";
            $to_excel .= "<th>Utm Medium</th>";
            $to_excel .= "<th>Utm Campaign</th>";
            $to_excel .= "<th>Date</th>";
            $to_excel .= "<th>Lead Id</th>";
            $to_excel .= "<th>Request Log</th>";
            $to_excel .= "<th>Response Log</th>";
            $to_excel .= "</tr>";
            
            $sel_query = $this->db->query("Select 
                                               id, name, phone, email, city, utm_source, utm_campaign, utm_medium, date_created,
                                               api_request, api_response, type, lead_id
                                         From 
                                             " . RELIGARE_USER . "
                                        where  ". $utm_source . $type . " 
                                            DATE_FORMAT(date_created, '%Y-%m-%d') between '" . $from_date . "' and '" . $to_date . "' 
                                        ORDER BY  
                                             date_created desc ");
            
            $srl = 1;
            foreach ($sel_query->result() as $val_user) {
                
                $to_excel .= "<td>" . ($srl) . "</td>";
                $to_excel .= "<td>" . ($val_user->name) . "</td>";
                $to_excel .= "<td>" . ($val_user->phone) . "</td>";
                $to_excel .= "<td>" . ($val_user->email) . "</td>";
                $to_excel .= "<td>" . ($val_user->city) . "</td>";
                $to_excel .= "<td>" . ($val_user->type) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_source) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_medium) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_campaign) . "</td>";
                $to_excel .= "<td>" . ($val_user->date_created) . "</td>";
                $to_excel .= "<td>" . ($val_user->lead_id) . "</td>";
                $to_excel .= "<td>" . ($val_user->api_request) . "</td>";
                $to_excel .= "<td>" . ($val_user->api_response) . "</td>";
                $to_excel .= "</tr>";
                
                $srl++;
            }
            
            header("Content-Type:  application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=religare_excel-" . time() . ".xls");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            echo $to_excel;
        }
    }
    
	
	
	public function purvankara(){
		
		 $all_array = all_arrays(); 
		
		$from_date = $_POST['from_date'];
		$to_date   = $_POST['to_date'];
		
		
		if($_POST['utm_source'] != ""){
			$utm_source = "utm_source = '" .$_POST['utm_source'] ."' and ";
	    }else{
			$utm_source = "";
		}
		if($_POST['lp_name'] != ""){
			$lp_name = "lp_name = '" .$_POST['lp_name'] ."' and ";
	    }else{
			$lp_name = "";
		}
		
		//echo $from_date;exit;
		if($to_date != "" && $from_date != ""){
			
			$to_excel = "";
			$to_excel .= "<table border='1'>";
			$to_excel .= "<tr>";
			$to_excel .= "<th>No</th>";
			$to_excel .= "<th>Name</th>";
			$to_excel .= "<th>Country Code</th>";
			$to_excel .= "<th>Phone</th>";
			$to_excel .= "<th>Email</th>";
			$to_excel .= "<th>MObile Optin</th>";
			$to_excel .= "<th>Utm Source1</th>";
			$to_excel .= "<th>Utm Medium</th>";
			$to_excel .= "<th>Utm Sub</th>";
			$to_excel .= "<th>Utm Campaign</th>";
			$to_excel .= "<th>Lp Name</th>";
			$to_excel .= "<th>Date</th>";
			$to_excel .= "<th>user IP</th>";
			$to_excel .= "<th>Purvankara Lead id</th>";
			$to_excel .= "</tr>";
			
			$sel_query =$this->db->query("Select 
											  id, name, country, phone, email, otp_optinstatus ,city,
											  utm_source, utm_source2, utm_source3, lp_name, ip_address,
											  utm_campaign, date_created, lead_id  
										 From 
											 ".PURAVANKARA."
										where ".$utm_source . $lp_name ." 
											DATE_FORMAT(date_created, '%Y-%m-%d') between '".$from_date."' and '".$to_date."' 
										ORDER BY  
											 date_created desc ");
			
				$srl = 1;
				foreach ($sel_query->result() as $val_user){
				
					$to_excel .= "<td>".($srl)."</td>";
					$to_excel .= "<td>".($val_user->name)."</td>";
					$to_excel .= "<td>".($val_user->country)."</td>";
					$to_excel .= "<td>".($val_user->phone)."</td>";
					$to_excel .= "<td>".($val_user->email)."</td>";
					$to_excel .= "<td>".($val_user->otp_optinstatus == 1 ? 'Optin' : 'Non-Optin')."</td>";
					$to_excel .= "<td>".($val_user->utm_source)."</td>";
					$to_excel .= "<td>".($val_user->utm_source2)."</td>";
					$to_excel .= "<td>".($val_user->utm_source3)."</td>";
					$to_excel .= "<td>".($val_user->utm_campaign)."</td>";
					$to_excel .= "<td>".($val_user->lp_name)."</td>";
					$to_excel .= "<td>".($val_user->date_created)."</td>";
					$to_excel .= "<td>".($val_user->ip_address)."</td>";
					$to_excel .= "<td>".($val_user->lead_id)."</td>";
					$to_excel .= "</tr>";
				
					$srl++;
				}
				
				header("Content-Type:  application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=purvankara_excel-".time().".xls");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				echo $to_excel;	
		} 
	}


	
	
	public function sriram_download(){
		
		$all_array = all_arrays(); 
		
		$from_date = $_POST['from_date'];
		$to_date   = $_POST['to_date'];
		
		if($to_date != "" && $from_date != ""){
			
			$to_excel = "";
			$to_excel .= "<table border='1'>";
			$to_excel .= "<tr>";
			$to_excel .= "<th>No</th>";
			$to_excel .= "<th>Name</th>";
			$to_excel .= "<th>Phone</th>";
			$to_excel .= "<th>Email</th>";
			$to_excel .= "<th>Utm Source</th>";
			$to_excel .= "<th>Utm Medium</th>";
			$to_excel .= "<th>Utm Campaign</th>";
			$to_excel .= "<th>Date</th>";
			$to_excel .= "</tr>";
			
			$sel_query =$this->db->query("Select 
											   id, name, phone, email, utm_source, utm_campaign, utm_medium, date_created  
										 From 
											 ".SRIRAMA."
										where
											DATE_FORMAT(date_created, '%Y-%m-%d') between '".$from_date."' and '".$to_date."' 
										ORDER BY  
											 date_created desc ");
			
				$srl = 1;
				foreach ($sel_query->result() as $val_user){
				
					$to_excel .= "<td>".($srl)."</td>";
					$to_excel .= "<td>".($val_user->name)."</td>";
					$to_excel .= "<td>".($val_user->phone)."</td>";
					$to_excel .= "<td>".($val_user->email)."</td>";
					$to_excel .= "<td>".($val_user->utm_source)."</td>";
					$to_excel .= "<td>".($val_user->utm_medium)."</td>";
					$to_excel .= "<td>".($val_user->utm_campaign)."</td>";
					$to_excel .= "<td>".($val_user->date_created)."</td>";
					$to_excel .= "</tr>";
				
					$srl++;
				}
				
				header("Content-Type:  application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=sriram_excel-".time().".xls");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				echo $to_excel;		
		}
	}



	      public function hiranandani(){
		
		 $all_array = all_arrays(); 
		
		$from_date = $_POST['from_date'];
		$to_date   = $_POST['to_date'];
		
		
		if($_POST['utm_source'] != ""){
			$utm_source = "utm_source = '" .$_POST['utm_source'] ."' and ";
	    }else{
			$utm_source = "";
		}
		
		
		//echo $from_date;exit;
		if($to_date != "" && $from_date != ""){
			
			$to_excel = "";
			$to_excel .= "<table border='1'>";
			$to_excel .= "<tr>";
			$to_excel .= "<th>No</th>";
			$to_excel .= "<th>Name</th>";
			$to_excel .= "<th>Phone</th>";
			$to_excel .= "<th>Email</th>";
			$to_excel .= "<th>Utm Source</th>";
			$to_excel .= "<th>Utm Medium</th>";
			$to_excel .= "<th>Utm Sub</th>";
			$to_excel .= "<th>Utm Campaign</th>";
			
			$to_excel .= "<th>Date</th>";
			$to_excel .= "</tr>";
			
			$sel_query =$this->db->query("Select 
											  id, name, phone, email,utm_source,utm_medium,utm_sub,utm_campaign,											date_created  
										 From 
											 ".HIRANANDANI."
										where ".$utm_source." 
											DATE_FORMAT(date_created, '%Y-%m-%d') between '".$from_date."' and '".$to_date."' 
										ORDER BY  
											 date_created desc ");
			
				$srl = 1;
				foreach ($sel_query->result() as $val_user){
				
					$to_excel .= "<td>".($srl)."</td>";
					$to_excel .= "<td>".($val_user->name)."</td>";
					$to_excel .= "<td>".($val_user->phone)."</td>";
					$to_excel .= "<td>".($val_user->email)."</td>";
					$to_excel .= "<td>".($val_user->utm_source)."</td>";
					$to_excel .= "<td>".($val_user->utm_medium)."</td>";
					$to_excel .= "<td>".($val_user->utm_sub)."</td>";
					$to_excel .= "<td>".($val_user->utm_campaign)."</td>";
					
					$to_excel .= "<td>".($val_user->date_created)."</td>";
					$to_excel .= "</tr>";
				
					$srl++;
				}
				
				header("Content-Type:  application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=hiranandani_excel-".time().".xls");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				echo $to_excel;	
		} 
	}



	public function nmims(){  
         $all_array = all_arrays(); 
         
        $from_date = $_POST['from_date'];
        $to_date   = $_POST['to_date'];
         
         
        if($_POST['utm_source'] != ""){
            $utm_source = "utm_source = '" .$_POST['utm_source'] ."' and ";
        }else{
            $utm_source = "";
        }
        if($_POST['course'] != ""){
            $course = "course = '" .$_POST['course'] ."' and ";
        }else{
            $course = "";
        }
         
        //echo $from_date;exit;
        if($to_date != "" && $from_date != ""){
             
            $to_excel = "";
            $to_excel .= "<table border='1'>";
            $to_excel .= "<tr>";
            $to_excel .= "<th>No</th>";
            $to_excel .= "<th>Name</th>";
            $to_excel .= "<th>City</th>";
            $to_excel .= "<th>Phone</th>";
            $to_excel .= "<th>Email</th>";
            $to_excel .= "<th>Utm Source</th>";
            $to_excel .= "<th>Utm Medium</th>";
            $to_excel .= "<th>Utm Sub</th>";
            $to_excel .= "<th>Utm Campaign</th>";
            $to_excel .= "<th>course Name</th>";
            $to_excel .= "<th>API Response ID</th>";
            $to_excel .= "<th>Date</th>";
            $to_excel .= "</tr>";
             
            $sel_query =$this->db->query("Select 
                                              id, name, city,phone, email,utm_source,utm_sub,utm_medium,course,utm_campaign,
                                              lead_id, date_created  
                                         From 
                                             ".NMIMS."
                                        where ".$utm_source . $course ."
                                            DATE_FORMAT(date_created, '%Y-%m-%d') between '".$from_date."' and '".$to_date."'
                                        ORDER BY  
                                             date_created desc ");
             
                $srl = 1;
                $all_array = all_arrays();
                foreach ($sel_query->result() as $val_user){
                 
                    $to_excel .= "<td>".($srl)."</td>";
                    $to_excel .= "<td>".($val_user->name)."</td>";
                    foreach($all_array['NMIMS_CITY'] as $k=>$v){
                        if($val_user->city == $k){
                           $to_excel .= "<td>".($v)."</td>";
                        }
                    }
                    $to_excel .= "<td>".($val_user->phone)."</td>";
                    $to_excel .= "<td>".($val_user->email)."</td>";
                    $to_excel .= "<td>".($val_user->utm_source)."</td>";
                    $to_excel .= "<td>".($val_user->utm_sub)."</td>";
                    $to_excel .= "<td>".($val_user->utm_medium)."</td>";
                    $to_excel .= "<td>".($val_user->utm_campaign)."</td>";
                    $to_excel .= "<td>".($val_user->course)."</td>";
                    $to_excel .= "<td>".($val_user->lead_id)."</td>";
                    $to_excel .= "<td>".($val_user->date_created)."</td>";
                    $to_excel .= "</tr>";
                 
                    $srl++;
                }
                 
                header("Content-Type:  application/vnd.ms-excel");
                header("Content-Disposition: attachment; filename=nmims_excel-".time().".xls");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                echo $to_excel; 
        } 
    }

	public function winmore(){
         
         $all_array = all_arrays(); 
         
        $from_date = $_POST['from_date'];
        $to_date   = $_POST['to_date'];
         
         
        if($_POST['utm_source'] != ""){
            $utm_source = "utm_source = '" .$_POST['utm_source'] ."' and ";
        }else{
            $utm_source = "";
        }
         
         
        //echo $from_date;exit;
        if($to_date != "" && $from_date != ""){
             
            $to_excel = "";
            $to_excel .= "<table border='1'>";
            $to_excel .= "<tr>";
            $to_excel .= "<th>No</th>";
            $to_excel .= "<th>Name</th>";
            $to_excel .= "<th>Phone</th>";
            $to_excel .= "<th>Email</th>";
            $to_excel .= "<th>Campus</th>";
            $to_excel .= "<th>Grade</th>";
            $to_excel .= "<th>Utm Source</th>";
            $to_excel .= "<th>Utm Medium</th>";
            $to_excel .= "<th>Utm Sub</th>";
            $to_excel .= "<th>Utm Campaign</th>";
             
            $to_excel .= "<th>Date</th>";
            $to_excel .= "</tr>";
             
            $sel_query =$this->db->query("Select 
                                              id, name,phone, email,campus,grade,utm_source,utm_sub,utm_medium,utm_campaign,                                            date_created  
                                         From 
                                             ".WINMORE."
                                        where ".$utm_source."
                                            DATE_FORMAT(date_created, '%Y-%m-%d') between '".$from_date."' and '".$to_date."'
                                        ORDER BY  
                                             date_created desc ");
             
                $srl = 1;
                $all_array = all_arrays();
                foreach ($sel_query->result() as $val_user){
                 
                    $to_excel .= "<td>".($srl)."</td>";
                    $to_excel .= "<td>".($val_user->name)."</td>";
                     
                    $to_excel .= "<td>".($val_user->phone)."</td>";
                    $to_excel .= "<td>".($val_user->email)."</td>";
                    $to_excel .= "<td>".($val_user->campus)."</td>";
                    if($val_user->grade == 13){
                           $to_excel .= "<td>Playhome</td>";
                    }else{
                        foreach($all_array['WINMORE_GRADE'] as $k=>$v){
                                if($val_user->grade == $k){
                               $to_excel .= "<td>".($v)."</td>";
                                }
                        }
                    }
                    $to_excel .= "<td>".($val_user->utm_source)."</td>";
                    $to_excel .= "<td>".($val_user->utm_sub)."</td>";
                    $to_excel .= "<td>".($val_user->utm_medium)."</td>";
                    $to_excel .= "<td>".($val_user->utm_campaign)."</td>";
                     
                    $to_excel .= "<td>".($val_user->date_created)."</td>";
                    $to_excel .= "</tr>";
                 
                    $srl++;
                }
                 
                header("Content-Type:  application/vnd.ms-excel");
                header("Content-Disposition: attachment; filename=winmore_excel-".time().".xls");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                echo $to_excel; 
        } 
    }



    public function nutratimes(){
         
         $all_array = all_arrays(); 
         
        $from_date = $_POST['from_date'];
        $to_date   = $_POST['to_date'];
         
         
        if($_POST['utm_source'] != ""){
            $utm_source = "utm_source = '" .$_POST['utm_source'] ."' and ";
        }else{
            $utm_source = "";
        }
         
         
        //echo $from_date;exit;
        if($to_date != "" && $from_date != ""){
             
            $to_excel = "";
            $to_excel .= "<table border='1'>";
            $to_excel .= "<tr>";
            $to_excel .= "<th>No</th>";
            $to_excel .= "<th>Name</th>";
            $to_excel .= "<th>Phone</th>";
            $to_excel .= "<th>Email</th>";
            $to_excel .= "<th>City</th>";
            $to_excel .= "<th>Utm Source</th>";
            $to_excel .= "<th>Utm Medium</th>";
            $to_excel .= "<th>Utm Sub</th>";
            $to_excel .= "<th>Utm Campaign</th>";
             
            $to_excel .= "<th>Date</th>";
            $to_excel .= "</tr>";
             
            $sel_query =$this->db->query("Select 
                                              id, name,phone, email, city,
                                              utm_source, utm_sub, utm_medium, 
                                              utm_campaign, date_created  
                                         From 
                                             ".NUTRATIMES."
                                        where ".$utm_source."
                                            DATE_FORMAT(date_created, '%Y-%m-%d') between '".$from_date."' and '".$to_date."'
                                        ORDER BY  
                                             date_created desc ");
             
                $srl = 1;
                $all_array = all_arrays();
                foreach ($sel_query->result() as $val_user){
                 
                    $to_excel .= "<td>".($srl)."</td>";
                    $to_excel .= "<td>".($val_user->name)."</td>";
                     
                    $to_excel .= "<td>".($val_user->phone)."</td>";
                    $to_excel .= "<td>".($val_user->email)."</td>";
                    $to_excel .= "<td>".($val_user->city)."</td>";
                    $to_excel .= "<td>".($val_user->utm_source)."</td>";
                    $to_excel .= "<td>".($val_user->utm_sub)."</td>";
                    $to_excel .= "<td>".($val_user->utm_medium)."</td>";
                    $to_excel .= "<td>".($val_user->utm_campaign)."</td>";
                     
                    $to_excel .= "<td>".($val_user->date_created)."</td>";
                    $to_excel .= "</tr>";
                 
                    $srl++;
                }
                 
                header("Content-Type:  application/vnd.ms-excel");
                header("Content-Disposition: attachment; filename=nutralp_excel-".time().".xls");
                header("Expires: 0");
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                echo $to_excel; 
        } 
    }



    public function oxygenclub(){
		
		 $all_array = all_arrays(); 
		
		$from_date = $_POST['from_date'];
		$to_date   = $_POST['to_date'];
		
		
		if($_POST['utm_source'] != ""){
			$utm_source = "utm_source = '" .$_POST['utm_source'] ."' and ";
	    }else{
			$utm_source = "";
		}
		
		
		//echo $from_date;exit;
		if($to_date != "" && $from_date != ""){
			
			$to_excel = "";
			$to_excel .= "<table border='1'>";
			$to_excel .= "<tr>";
			$to_excel .= "<th>No</th>";
			$to_excel .= "<th>Name</th>";
			$to_excel .= "<th>Phone</th>";
			$to_excel .= "<th>Email</th>";
			$to_excel .= "<th>Age</th>";
			$to_excel .= "<th>Location</th>";
			$to_excel .= "<th>last Location</th>";
			$to_excel .= "<th>Utm Source</th>";
			$to_excel .= "<th>Utm Medium</th>";
			$to_excel .= "<th>Utm Sub</th>";
			$to_excel .= "<th>Utm Campaign</th>";
			$to_excel .= "<th>Utm Term</th>";
			$to_excel .= "<th>Utm Content</th>";
			$to_excel .= "<th>Date</th>";
			$to_excel .= "</tr>";
			
			$sel_query =$this->db->query("Select 
											  id, name, phone, email, age, location, last_location,utm_source,
											  utm_medium, utm_sub, utm_campaign, utm_term, utm_content, date_created  
										 From 
											 ".OXYGENCLUB."
										 where ".$utm_source." 
											DATE_FORMAT(date_created, '%Y-%m-%d') between '".$from_date."' and '".$to_date."' 
										 ORDER BY  
											date_created desc ");
			
				$srl = 1;
				foreach ($sel_query->result() as $val_user){
				
					$to_excel .= "<td>".($srl)."</td>";
					$to_excel .= "<td>".($val_user->name)."</td>";
					$to_excel .= "<td>".($val_user->phone)."</td>";
					$to_excel .= "<td>".($val_user->email)."</td>";
					$to_excel .= "<td>".($val_user->age)."</td>";
					$to_excel .= "<td>".($val_user->location)."</td>";
					$to_excel .= "<td>".($val_user->last_location)."</td>";
					$to_excel .= "<td>".($val_user->utm_source)."</td>";
					$to_excel .= "<td>".($val_user->utm_medium)."</td>";
					$to_excel .= "<td>".($val_user->utm_sub)."</td>";
					$to_excel .= "<td>".($val_user->utm_campaign)."</td>";
					$to_excel .= "<td>".($val_user->utm_term)."</td>";
					$to_excel .= "<td>".($val_user->utm_content)."</td>";
					
					$to_excel .= "<td>".($val_user->date_created)."</td>";
					$to_excel .= "</tr>";
				
					$srl++;
				}
				
				header("Content-Type:  application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=oxygenclub_excel-".time().".xls");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				echo $to_excel;	
		} 
	}



	//vaswani

  public function vaswani(){
		
		 $all_array = all_arrays(); 
		
		$from_date = $_POST['from_date'];
		$to_date   = $_POST['to_date'];
		
		
		if($_POST['utm_source'] != ""){
			$utm_source = "utm_source = '" .$_POST['utm_source'] ."' and ";
	    }else{
			$utm_source = "";
		}
		
		
		//echo $from_date;exit;
		if($to_date != "" && $from_date != ""){
			
			$to_excel = "";
			$to_excel .= "<table border='1'>";
			$to_excel .= "<tr>";
			$to_excel .= "<th>No</th>";
			$to_excel .= "<th>Name</th>";
			$to_excel .= "<th>Phone</th>";
			$to_excel .= "<th>Email</th>";
			$to_excel .= "<th>Utm Source</th>";
			$to_excel .= "<th>Utm Campaign</th>";
			$to_excel .= "<th>Date</th>";
			$to_excel .= "</tr>";
			
			$sel_query =$this->db->query("Select 
											  id, name, phone, email,utm_source, utm_campaign, date_created  
										 From 
											 ".VASWANIGROUP."
										 where ".$utm_source." 
											DATE_FORMAT(date_created, '%Y-%m-%d') between '".$from_date."' and '".$to_date."' 
										 ORDER BY  
											date_created desc ");
			
				$srl = 1;
				foreach ($sel_query->result() as $val_user){
				
					$to_excel .= "<td>".($srl)."</td>";
					$to_excel .= "<td>".($val_user->name)."</td>";
					$to_excel .= "<td>".($val_user->phone)."</td>";
					$to_excel .= "<td>".($val_user->email)."</td>";
					$to_excel .= "<td>".($val_user->utm_source)."</td>";
					$to_excel .= "<td>".($val_user->utm_campaign)."</td>";
					
					$to_excel .= "<td>".($val_user->date_created)."</td>";
					$to_excel .= "</tr>";
				
					$srl++;
				}
				
				header("Content-Type:  application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=oxygencvaswani_excel-".time().".xls");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				echo $to_excel;	
		} 
	}

	public function realestate()
    {
        
        $all_array = all_arrays();
        
        $from_date = $_POST['from_date'];
        $to_date   = $_POST['to_date'];
        
        
        if ($_POST['utm_source'] != "") {
            $utm_source = "utm_source = '" . $_POST['utm_source'] . "' and ";
        } else {
            $utm_source = "";
        }
        if ($_POST['lp_name'] != "") {
            $lp_name = "client_name = '" . $_POST['lp_name'] . "' and ";
        } else {
            $lp_name = "";
        }
        
        //echo $from_date;exit;
        if ($to_date != "" && $from_date != "") {
            
            $to_excel = "";
            $to_excel .= "<table border='1'>";
            $to_excel .= "<tr>";
            $to_excel .= "<th>No</th>";
            $to_excel .= "<th>Name</th>";
            $to_excel .= "<th>Country Code</th>";
            $to_excel .= "<th>Phone</th>";
            $to_excel .= "<th>Email</th>";
            $to_excel .= "<th>Utm Source</th>";
            $to_excel .= "<th>Utm Medium</th>";
            $to_excel .= "<th>Utm Sub</th>";
            $to_excel .= "<th>Utm Campaign</th>";
            $to_excel .= "<th>Lp Name</th>";
            $to_excel .= "<th>Date</th>";
            $to_excel .= "<th>IP</th>";
            $to_excel .= "<th>Lead id</th>";
            $to_excel .= "</tr>";
            
            $sel_query = $this->db->query("Select 
                                              id, name, country, phone, email,utm_source,utm_sub,utm_medium,
                                              client_name,utm_campaign, date_created, ip_address, lead_id  
                                         From 
                                             " . REALESTATE_USER . "
                                        where " . $utm_source . $lp_name . " 
                                            DATE_FORMAT(date_created, '%Y-%m-%d') between '" . $from_date . "' and '" . $to_date . "' 
                                        ORDER BY  
                                             date_created desc ");
            
            $srl = 1;
            foreach ($sel_query->result() as $val_user) {
                
                $to_excel .= "<td>" . ($srl) . "</td>";
                $to_excel .= "<td>" . ($val_user->name) . "</td>";
                $to_excel .= "<td>" . ($val_user->country) . "</td>";
                $to_excel .= "<td>" . ($val_user->phone) . "</td>";
                $to_excel .= "<td>" . ($val_user->email) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_source) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_medium) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_sub) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_campaign) . "</td>";
                $to_excel .= "<td>" . ($all_array['REALESTATE_CLIENTS'][$val_user->client_name]) . "</td>";
                $to_excel .= "<td>" . ($val_user->date_created) . "</td>";
                $to_excel .= "<td>" . ($val_user->ip_address) . "</td>";
                $to_excel .= "<td>" . ($val_user->lead_id) . "</td>";
                
                $to_excel .= "</tr>";
                
                $srl++;
            }
            
            header("Content-Type:  application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=realestate_excel-" . time() . ".xls");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            echo $to_excel;
        }
    }



    public function insurance()
    {
        
        $all_array = all_arrays();
        
        $from_date = $_POST['from_date'];
        $to_date   = $_POST['to_date'];
        
        
        if ($_POST['utm_source'] != "") {
            $utm_source = "utm_source = '" . $_POST['utm_source'] . "' and ";
        } else {
            $utm_source = "";
        }
        if ($_POST['lp_name'] != "") {
            $lp_name = "client_name = '" . $_POST['lp_name'] . "' and ";
        } else {
            $lp_name = "";
        }
        
        //echo $from_date;exit;
        if ($to_date != "" && $from_date != "") {
            
            $to_excel = "";
            $to_excel .= "<table border='1'>";
            $to_excel .= "<tr>";
            $to_excel .= "<th>No</th>";
            $to_excel .= "<th>Name</th>";
            $to_excel .= "<th>Country Code</th>";
            $to_excel .= "<th>Phone</th>";
            $to_excel .= "<th>Email</th>";
            //$to_excel .= "<th>City</th>";
            $to_excel .= "<th>Utm Source</th>";
            $to_excel .= "<th>Utm Medium</th>";
            $to_excel .= "<th>Utm Sub</th>";
            $to_excel .= "<th>Utm Campaign</th>";
            $to_excel .= "<th>Lp Name</th>";
            $to_excel .= "<th>Lead id</th>";
            $to_excel .= "<th>Date</th>";
            $to_excel .= "<th>Ip Address</th>";
            $to_excel .= "</tr>";
            
            $sel_query = $this->db->query("Select 
                                              id, name, country, phone, email, city, utm_source, utm_sub, utm_medium, client_name, lead_id,
                                              utm_campaign, date_created, ip_address  
                                         From 
                                             " . INSURANCE_USER . "
                                        where " . $utm_source . $lp_name . " 
                                            DATE_FORMAT(date_created, '%Y-%m-%d') between '" . $from_date . "' and '" . $to_date . "' 
                                        ORDER BY  
                                             date_created desc ");
            
            $srl = 1;
            foreach ($sel_query->result() as $val_user) {
                
                $to_excel .= "<td>" . ($srl) . "</td>";
                $to_excel .= "<td>" . ($val_user->name) . "</td>";
                $to_excel .= "<td>" . ($val_user->country) . "</td>";
                $to_excel .= "<td>" . ($val_user->phone) . "</td>";
                $to_excel .= "<td>" . ($val_user->email) . "</td>";
                //$to_excel .= "<td>" . ($all_array['POLICYMAFNIFER_CITIES'][$val_user->city]) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_source) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_medium) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_sub) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_campaign) . "</td>";
                $to_excel .= "<td>" . ($all_array['INSURANCE_CLIENTS'][$val_user->client_name]) . "</td>";
                $to_excel .= "<td>" . ($val_user->lead_id) . "</td>";
                $to_excel .= "<td>" . ($val_user->date_created) . "</td>";
                $to_excel .= "<td>" . ($val_user->ip_address) . "</td>";
                $to_excel .= "</tr>";
                
                $srl++;
            }
            
            header("Content-Type:  application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=insurance_excel-" . time() . ".xls");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            echo $to_excel;
        }
    }



    public function mantri()
    {
        
        $all_array = all_arrays();
        $lp_name   = "";
        $from_date = $_POST['from_date'];
        $to_date   = $_POST['to_date'];
        
        
        if ($_POST['utm_source'] != "") {
            $utm_source = "utm_source = '" . $_POST['utm_source'] . "' and ";
        } else {
            $utm_source = "";
        }

        /*if ($_POST['lp_name'] != "") {
            $lp_name = "lp_name = '" . $_POST['lp_name'] . "' and ";
        } else {
            $lp_name = "";
        }*/
        
        //echo $from_date;exit;
        if ($to_date != "" && $from_date != "") {
           
            $to_excel = "";
            $to_excel .= "<table border='1'>";
            $to_excel .= "<tr>";
            $to_excel .= "<th>No</th>";
            $to_excel .= "<th>Name</th>";
            $to_excel .= "<th>Phone</th>";
            $to_excel .= "<th>Email</th>";
            $to_excel .= "<th>Utm Source</th>";
            $to_excel .= "<th>Utm Medium</th>";
            $to_excel .= "<th>Utm Sub</th>";
            $to_excel .= "<th>Utm Campaign</th>";
            $to_excel .= "<th>Project Name</th>";
            $to_excel .= "<th>Date</th>";
            $to_excel .= "</tr>";
            
            $sel_query = $this->db->query("Select 
                                              id, name, country, phone, email,utm_source, utm_sub, utm_medium,
                                              lp_name, utm_campaign, date_created  
                                         From 
                                             " . MANTRI . "
                                        where " . $utm_source . $lp_name . " 
                                            DATE_FORMAT(date_created, '%Y-%m-%d') between '" . $from_date . "' and '" . $to_date . "' 
                                        ORDER BY  
                                             date_created desc ");
            
            $srl = 1;
            foreach ($sel_query->result() as $val_user) {
                
                
                $to_excel .= "<td>" . ($srl) . "</td>";
                $to_excel .= "<td>" . ($val_user->name) . "</td>";
                $to_excel .= "<td>" . ($val_user->phone) . "</td>";
                $to_excel .= "<td>" . ($val_user->email) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_source) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_medium) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_sub) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_campaign) . "</td>";
                $to_excel .= "<td>" . ($val_user->lp_name) . "</td>";
                $to_excel .= "<td>" . ($val_user->date_created) . "</td>";
                $to_excel .= "</tr>";
                
                $srl++;
            }
            
            header("Content-Type:  application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=realestate_excel-" . time() . ".xls");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            echo $to_excel;
        }
    }





    public function ozone()
    {
        
        $all_array = all_arrays();
        
        $from_date = $_POST['from_date'];
        $to_date   = $_POST['to_date'];
        
        $utm_source = $lp_name = "";

        
        //echo $from_date;exit;
        if ($to_date != "" && $from_date != "") {
           
            $to_excel = "";
            $to_excel .= "<table border='1'>";
            $to_excel .= "<tr>";
            $to_excel .= "<th>No</th>";
            $to_excel .= "<th>Name</th>";
            $to_excel .= "<th>Phone</th>";
            $to_excel .= "<th>Email</th>";
            $to_excel .= "<th>Utm Source</th>";
            $to_excel .= "<th>Utm Sub</th>";
            $to_excel .= "<th>Utm Campaign</th>";
            $to_excel .= "<th>Project Name</th>";
            $to_excel .= "<th>Date</th>";
            $to_excel .= "</tr>";
            
            $sel_query = $this->db->query("Select 
                                              id, name, country, phone, email,utm_source,utm_sub,lp_name,utm_campaign,                                           
                                              date_created  
                                         From 
                                             " . OZONE . "
                                        where " . $utm_source . $lp_name . " 
                                            DATE_FORMAT(date_created, '%Y-%m-%d') between '" . $from_date . "' and '" . $to_date . "' 
                                        ORDER BY  
                                             date_created desc ");
            
            $srl = 1;
            foreach ($sel_query->result() as $val_user) {
                 $date =strtotime ($val_user->date_created);
                 $month=date("F", $date);
                 $year=date("Y", $date);
                
                $to_excel .= "<td>" . ($srl) . "</td>";
                $to_excel .= "<td>" . ($val_user->name) . "</td>";
                $to_excel .= "<td>" . ($val_user->phone) . "</td>";
                $to_excel .= "<td>" . ($val_user->email) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_source) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_sub) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_campaign) . "</td>";
                $to_excel .= "<td>" . ($val_user->lp_name) . "</td>";
                $to_excel .= "<td>" . ($val_user->date_created) . "</td>";
                $to_excel .= "</tr>";
                
                $srl++;
            }
            
            header("Content-Type:  application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=ozone-" . time() . ".xls");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            echo $to_excel;
        }
    }


    public function chaipoint()
    {
        
        $all_array = all_arrays();
        
        $from_date = $_POST['from_date'];
        $to_date   = $_POST['to_date'];
        
        $utm_source = $lp_name = "";

        
        //echo $from_date;exit;
        if ($to_date != "" && $from_date != "") {
           
            $to_excel = "";
            $to_excel .= "<table border='1'>";
            $to_excel .= "<tr>";
            $to_excel .= "<th>No</th>";
            $to_excel .= "<th>Name</th>";
            $to_excel .= "<th>Phone</th>";
            $to_excel .= "<th>Email</th>";
            $to_excel .= "<th>city</th>";
            $to_excel .= "<th>company</th>";
            $to_excel .= "<th>Employee</th>";
            $to_excel .= "<th>Utm Source</th>";
            $to_excel .= "<th>Utm Sub</th>";
            $to_excel .= "<th>Utm Campaign</th>";
            $to_excel .= "<th>Date</th>";
            $to_excel .= "</tr>";
            
            $sel_query = $this->db->query("Select 
                                              id, name, phone, email, city, company, employee,
                                              utm_source,utm_sub,utm_campaign, date_created  
                                         From 
                                             " . CHAIPOINT . "
                                        where " . $utm_source . $lp_name . " 
                                            DATE_FORMAT(date_created, '%Y-%m-%d') between '" . $from_date . "' and '" . $to_date . "' 
                                        ORDER BY  
                                             date_created desc ");
            
            $srl = 1;
            foreach ($sel_query->result() as $val_user) {
                 $date =strtotime ($val_user->date_created);
                 $month=date("F", $date);
                 $year=date("Y", $date);
                
                $to_excel .= "<td>" . ($srl) . "</td>";
                $to_excel .= "<td>" . ($val_user->name) . "</td>";
                $to_excel .= "<td>" . ($val_user->phone) . "</td>";
                $to_excel .= "<td>" . ($val_user->email) . "</td>";
                $to_excel .= "<td>" . ($val_user->city) . "</td>";
                $to_excel .= "<td>" . ($val_user->company) . "</td>";
                $to_excel .= "<td>" . ($val_user->employee) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_source) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_sub) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_campaign) . "</td>";
                $to_excel .= "<td>" . ($val_user->date_created) . "</td>";
                $to_excel .= "</tr>";
                
                $srl++;
            }
            
            header("Content-Type:  application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=chaipoint-" . time() . ".xls");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            echo $to_excel;
        }
    }


    public function lenskart()
    {
        
        $all_array = all_arrays();
        
        $from_date = $_POST['from_date'];
        $to_date   = $_POST['to_date'];
        
        $utm_source = $lp_name = "";

        
        //echo $from_date;exit;
        if ($to_date != "" && $from_date != "") {
           
            $to_excel = "";
            $to_excel .= "<table border='1'>";
            $to_excel .= "<tr>";
            $to_excel .= "<th>No</th>";
            $to_excel .= "<th>Phone</th>";
            $to_excel .= "<th>city</th>";
            $to_excel .= "<th>is_optin</th>";
            $to_excel .= "<th>Utm Source</th>";
            $to_excel .= "<th>Utm Campaign</th>";
            $to_excel .= "<th>Date</th>";
            $to_excel .= "</tr>";
            
            $sel_query = $this->db->query("Select 
                                              id, phone, city, otp_optin, utm_source,utm_campaign,                                           
                                              date_created  
                                         From 
                                             " . LENSKART_USER . "
                                        where " . $utm_source . $lp_name . " 
                                            DATE_FORMAT(date_created, '%Y-%m-%d') between '" . $from_date . "' and '" . $to_date . "' 
                                        ORDER BY  
                                             date_created desc ");
            
            $srl = 1;
            foreach ($sel_query->result() as $val_user) {
                 $date =strtotime ($val_user->date_created);
                 $month=date("F", $date);
                 $year=date("Y", $date);
                
                $to_excel .= "<td>" . ($srl) . "</td>";
                $to_excel .= "<td>" . ($val_user->phone) . "</td>";
                $to_excel .= "<td>" . ($val_user->city) . "</td>";
                $to_excel .= "<td>" . ($val_user->otp_optin == 1 ? 'Yes': 'No') . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_source) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_campaign) . "</td>";
                $to_excel .= "<td>" . ($val_user->date_created) . "</td>";
                $to_excel .= "</tr>";
                
                $srl++;
            }
            
            header("Content-Type:  application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=lenskart-" . time() . ".xls");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            echo $to_excel;
        }
    }


    public function sobha()
    {
        
        $all_array = all_arrays();
        
        $from_date = $_POST['from_date'];
        $to_date   = $_POST['to_date'];
        
        $utm_source = $lp_name = "";

        
        //echo $from_date;exit;
        if ($to_date != "" && $from_date != "") {
           
            $to_excel = "";
            $to_excel .= "<table border='1'>";
            $to_excel .= "<tr>";
            $to_excel .= "<th>No</th>";
            $to_excel .= "<th>Name</th>";
            $to_excel .= "<th>Phone</th>";
            $to_excel .= "<th>Email</th>";
            $to_excel .= "<th>LP</th>";
            $to_excel .= "<th>Utm Source</th>";
            $to_excel .= "<th>Utm Campaign</th>";
            $to_excel .= "<th>Date</th>";
            $to_excel .= "</tr>";
            
            $sel_query = $this->db->query("Select 
                                              id, name, phone, email, utm_source,utm_campaign,                                            
                                              lp_name, date_created  
                                         From 
                                             " . SOBHA . "
                                        where " . $utm_source . $lp_name . " 
                                            DATE_FORMAT(date_created, '%Y-%m-%d') between '" . $from_date . "' and '" . $to_date . "' 
                                        ORDER BY  
                                             date_created desc ");
            
            $srl = 1;
            foreach ($sel_query->result() as $val_user) {
                 $date =strtotime ($val_user->date_created);
                 $month=date("F", $date);
                 $year=date("Y", $date);
                
                $to_excel .= "<td>" . ($srl) . "</td>";
                $to_excel .= "<td>" . ($val_user->name) . "</td>";
                $to_excel .= "<td>" . ($val_user->phone) . "</td>";
                $to_excel .= "<td>" . ($val_user->email) . "</td>";
                $to_excel .= "<td>" . ($val_user->lp_name) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_source) . "</td>";
                $to_excel .= "<td>" . ($val_user->utm_campaign) . "</td>";
                $to_excel .= "<td>" . ($val_user->date_created) . "</td>";
                $to_excel .= "</tr>";
                
                $srl++;
            }
            
            header("Content-Type:  application/vnd.ms-excel");
            header("Content-Disposition: attachment; filename=sobha-" . time() . ".xls");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            echo $to_excel;
        }
    }


    public function medlife_publisher(){
		
		$all_array = all_arrays(); 
		
		$from_date = $_POST['from_date'];
		$to_date   = $_POST['to_date'];
		
		if($to_date != "" && $from_date != ""){
			
			$to_excel = "";
			$to_excel .= "<table border='1'>";
			$to_excel .= "<tr>";
			$to_excel .= "<th>No</th>";
			$to_excel .= "<th>Name</th>";
			$to_excel .= "<th>Phone</th>";
			//$to_excel .= "<th>City</th>";
			$to_excel .= "<th>Plan</th>";
			$to_excel .= "<th>Date</th>";
			$to_excel .= "</tr>";
			
			$sel_query =$this->db->query("Select 
											 id, name, phone, city,
											 email, plan, otp_optin, 
											 utm_source,utm_campaign, date_created 
										 From 
											 ".MEDLIFE_USER."
										where
											DATE_FORMAT(date_created, '%Y-%m-%d') between '".$from_date."' and '".$to_date."' 
											AND otp_optin = 1 and utm_aff = 3160
										ORDER BY  
											 date_created desc ");
				
				$srl = 1;
				foreach ($sel_query->result() as $val_user){

					//$date = date('Y-m-d', strtotime($val_user->date_created));
					
					$to_excel .= "<td>".($srl)."</td>";
					$to_excel .= "<td>".($val_user->name)."</td>";
					$to_excel .= "<td>".(substr($val_user->phone, 0, -5) . '*****')."</td>";
					//$to_excel .= "<td>".($val_user->city)."</td>"; 
					$to_excel .= "<td>".($val_user->plan != '' && $val_user->plan != '' ? $all_array['MEDLIFE_TEST'][$val_user->plan] : '' )."</td>";
					$to_excel .= "<td>".($val_user->date_created)."</td>";
					$to_excel .= "</tr>";
				
					$srl++;
				}
				
				header("Content-Type:  application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=medlife_excel-".time().".xls");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				echo $to_excel;		
		}
	}


	public function mfine(){
		
		$all_array = all_arrays(); 
		
		$from_date = $_POST['from_date'];
		$to_date   = $_POST['to_date'];
		
		if($to_date != "" && $from_date != ""){
			
			$to_excel = "";
			$to_excel .= "<table border='1'>";
			$to_excel .= "<tr>";
			$to_excel .= "<th>No</th>";
			$to_excel .= "<th>Name</th>";
			$to_excel .= "<th>Phone</th>";
			//$to_excel .= "<th>City</th>";
			$to_excel .= "<th>Date</th>";
			$to_excel .= "<th>Utm Source</th>";
			$to_excel .= "<th>Utm Campaign</th>";
			$to_excel .= "</tr>";
			
			$sel_query =$this->db->query("Select 
											 id, name, phone,
											 email, otp_optin, 
											 utm_source,utm_campaign, date_created 
										 From 
											 ".MFINE_USER."
										where
											DATE_FORMAT(date_created, '%Y-%m-%d') between '".$from_date."' and '".$to_date."' 
										ORDER BY  
											 date_created desc ");
				
				$srl = 1;
				foreach ($sel_query->result() as $val_user){

					//$date = date('Y-m-d', strtotime($val_user->date_created));
					
					$to_excel .= "<td>".($srl)."</td>";
					$to_excel .= "<td>".($val_user->name)."</td>";
					$to_excel .= "<td>".($val_user->phone)."</td>";
					//$to_excel .= "<td>".($val_user->city)."</td>"; 
					$to_excel .= "<td>".($val_user->date_created)."</td>";
					$to_excel .= "<td>".($val_user->utm_source)."</td>";
					$to_excel .= "<td>".($val_user->utm_campaign)."</td>";
					$to_excel .= "</tr>";
				
					$srl++;
				}
				
				header("Content-Type:  application/vnd.ms-excel");
				header("Content-Disposition: attachment; filename=mfine_excel-".time().".xls");
				header("Expires: 0");
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				echo $to_excel;		
		}
	}


}
?>