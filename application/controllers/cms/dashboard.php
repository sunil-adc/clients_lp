<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class dashboard extends CI_Controller
{
   public function __construct()
   {
        parent::__construct();
		$this->db->cache_off();
		//$this->output->enable_profiler(TRUE);
		$this->load->model(array(CMS_FOLDER.'common_model', CMS_FOLDER.'db_function'));
        
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
		
   }

   public function index(){
		 
		$menu = $this->common_model->Menu_Array();
		$cur_controller 	= __CLASS__;
		
		// PAGE DETAILS
		$page_details['cur_controller'] = $cur_controller;
		$page_details['menu']           = $menu;
		
		$data['dt_type']                = NULL;
		$data['total_rejected_count']   = "";
		$data['total_qualified_count']  = "";
		$data['total_lead_count']       = "";
		$data['total_jubi_count']       = "";
		$data['total_vcare_count']		= "";
		$data['date_v']                 = "";
		$data['total_religare_health_count']= "";
		$data['total_nutra_count']		= "";
		$data['from_date']              = NULL;
		$data['to_date']                = NULL;
		$currentMonth                   = date('Y-m');
		$data['user_url']               = "";
		$tot_med_pubwise 				= ""; 
		$data['tot_med_pubwise'] 		= "";
		
		
		//$sql_etokyo = "Select id, name, phone, email, city, age,income,plan, date_created From ".ITOKYO." where 1=1";

		$sql_chaipoint = "Select id From ".CHAIPOINT." where 1=1";
		
		$sql_medlife = "Select id, name, phone, email,plan,otp_optin, date_created FROM ".MEDLIFE_USER." where 1=1"; 
		
		$sql_religare = "Select id, name, phone, email, otp_optin, date_created FROM ".RELIGARE_USER." where 1=1"; 
		
		$sql_mfine = "Select id FROM ".MFINE_USER." where 1=1"; 

		$sql_lenskart = "Select id FROM ".LENSKART_USER." where 1=1"; 

		$sql_purva    = "Select id date_created FROM ".PURAVANKARA." where 1=1"; 
		
		$sql_religare = "Select id date_created FROM ".RELIGARE_USER." where 1=1";

		$sql_nmims = "Select id date_created FROM ".NMIMS." where 1=1";

		$sql_insurance = "Select id date_created FROM ".INSURANCE_USER." where 1=1";

		$sql_mfine = "Select id date_created FROM ".MFINE_USER." where 1=1";

	

		if( $this->session->userdata('admin_role_id') == 5 ){
			
			$sql = " SELECT 
							 id, name, phone, email, city, 
							 date_created 
					   FROM 
							tbl_adcan_email where 1=1";
							 
		}else{
			$sql = " SELECT 
							 id, name, phone, email, city, 
							 status, date_created 
					   FROM 
							".USER_DATA." 
					   WHERE 
							 status = 1 "; 
		}
		
		if( is_array($_POST) && count($_POST) > 0){
		    
			$data['dt_type']                = $_POST['date_type'] != "" ? $_POST['date_type'] : 0;
			$data['from_date']              = $_POST['from_date'] != "" ? $_POST['from_date'] : "0";
		    $data['to_date']                = $_POST['to_date'] != "" ? $_POST['to_date'] : "0";
			
			if( $_POST['date_type'] != ""){
				
				if($_POST['date_type'] == 1){
					$date_val =  "CURDATE()";
					$sql_d = " AND DATE_FORMAT(date_created, '%Y-%m-%d') = ".$date_val;				 
	
				}else if($_POST['date_type'] == 2){
					$date_val =  "CURDATE() - INTERVAL 1 DAY" ;
					$sql_d = " AND DATE_FORMAT(date_created, '%Y-%m-%d') = ".$date_val;				 
	
				}else if($_POST['date_type'] == 3){
					$date_val =  "CURDATE() - INTERVAL 7 DAY";
					$sql_d = " AND DATE_FORMAT(date_created, '%Y-%m-%d') >= ".$date_val;				 
	
				}else if($_POST['date_type'] == 4){
					$date_val =  date('Y-m');
					$sql_d = " AND DATE_FORMAT(date_created, '%Y-%m') = '".$date_val."'";
	
				}else if($_POST['date_type'] == 5){
					//$sql_d =  " AND MONTH(date_created) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)" ;
					
					$sql_d =  " AND YEAR(date_created) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH) AND MONTH(date_created) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH) ";
				
				}else if($_POST['date_type'] == 6){
					$date_val =  "date_format(now() - interval 6 month,'%Y-%m')";
					$sql_d = " AND DATE_FORMAT(date_created, '%Y-%m') >= ".$date_val;
	
				}else if($_POST['date_type'] == 7){
					$date_val =  "";
					
				}else if($_POST['date_type'] == 8){
					$sql_d = " AND DATE_FORMAT(date_created, '%Y-%m-%d') between '".$data['from_date']."' and '".$data['to_date']."'";
				
				}				 
				
				$sql = $sql." ".$sql_d;
				
				//$data['user_url'] = FULL_CMS_URL."/user_details/page/10/GO"."/".$data['dt_type']."/0/0";
			
			}
		}else{
			$data['dt_type']   = 1;
			$data['from_date'] = $data['to_date']=  date('Y-m-d');
			$sql_d = " AND DATE_FORMAT(date_created, '%Y-%m-%d') = '".date('Y-m-d')."'";
			$sql = $sql." ".$sql_d;
		}
		
		if( $this->session->userdata('admin_role_id') == 6 ){
			
			$vcare_query =$this->db->query($sql);
			
			$data['total_vcare_count'] = $vcare_query ->num_rows();
		
		}else if( $this->session->userdata('admin_role_id') == 5 ){
			
			$email_leads_query =$this->db->query($sql);
			
			$data['email_lead_count'] = $email_leads_query ->num_rows();
		
		}else if( $this->session->userdata('admin_role_id') == 9 ){
			
			//MEDLIFE CLIENT

			if($this->session->userdata('uid') == 11){

				$tot_med_pubwise =$this->db->query($sql_medlife." ".$sql_d." and otp_optin = 1 and utm_aff = 3160" );

				$data['tot_med_pubwise'] = $tot_med_pubwise->num_rows();	
			}else{

				$tot_med_lead =$this->db->query($sql_medlife." ".$sql_d );
				
				$data['total_med_count'] = $tot_med_lead->num_rows();	
			}
			
		}else{		
						
			//MEDLIFE COUNT START

			$tot_med_lead =$this->db->query($sql_medlife." ".$sql_d );

			$data['total_med_count'] = $tot_med_lead->num_rows();	

			$tot_med_small_form_optin =$this->db->query($sql_medlife." ".$sql_d." and form_type = 1 and otp_optin = 1" );
			
			$data['small_form_optin'] = $tot_med_small_form_optin->num_rows();	
			
			$tot_med_small_form_nonoptin =$this->db->query($sql_medlife." ".$sql_d." and form_type = 1 and otp_optin != 1" );
			
			$data['small_form_nonoptin'] = $tot_med_small_form_nonoptin->num_rows();	
			
			$tot_med_big_form_optin =$this->db->query($sql_medlife." ".$sql_d." and form_type in (0, 2) and otp_optin = 1" );
			
			$data['big_form_optin'] = $tot_med_big_form_optin->num_rows();	
			
			$tot_med_big_form_nonoptin =$this->db->query($sql_medlife." ".$sql_d." and form_type in (0, 2) and otp_optin != 1" );
			
			$data['big_form_nonoptin'] = $tot_med_big_form_nonoptin->num_rows();


			//MEDLIFE COUNT END

			$tot_chaipoint_lead =$this->db->query($sql_chaipoint." ".$sql_d );
			
			$data['total_chaipoint_count'] = $tot_chaipoint_lead->num_rows();	

			$tot_purvankara_lead = $this->db->query($sql_purva." ".$sql_d );
			
			$data['total_purva_count'] = $tot_purvankara_lead->num_rows();	

			$tot_lenskart_lead =$this->db->query($sql_lenskart." ".$sql_d );
			
			$data['total_lenskart_count'] = $tot_lenskart_lead->num_rows();			

			$tot_religare_health =$this->db->query($sql_religare." ".$sql_d." and  type='health'");
			
			$data['total_religare_health_count'] = $tot_religare_health->num_rows();	

			$tot_nmims_lead =$this->db->query($sql_nmims." ".$sql_d);
			
			$data['total_nmims_count'] = $tot_nmims_lead->num_rows();

			$tot_maxlife_lead =$this->db->query($sql_insurance." ".$sql_d." and client_name=2");
			
			$data['total_maxlife_count'] = $tot_maxlife_lead->num_rows();

			$tot_mfine_lead =$this->db->query($sql_mfine ." ".$sql_d);
			
			$data['total_mfine_count'] = $tot_mfine_lead->num_rows();


			
		}
		
		$this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $page_details);
		$this->load->view(CMS_FOLDER.'dashboard', $data); 
		$this->load->view(CMS_FOLDER.'footer'); 
		
   }
}
?>