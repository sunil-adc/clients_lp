<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_details_nmims  extends CI_Controller
{
   public $global_country;
   public function __construct(){
        
		parent::__construct();
		$this->load->library('memcached');
		$this->load->model(array(CMS_FOLDER.'common_model', CMS_FOLDER.'db_function', 'cache_model'));
		$this->db->cache_off();
		$this->global_country =  ($this->session->userdata('cms_country') != "" ) ? $this->session->userdata('cms_country') : '';
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
		
   }

   
   public function index($perpage = 10, $offset = 0){
	   
	   $perpage = 10;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, NULL, NULL,NULL, 0);
   }		
   
    public function page($perpage = 10, $btn_search = NULL, $date_type = NULL, $from_date = NULL, $to_date = NULL, $utm_source = NULL, $course=NULL,$offset = 0){
	   	
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= __CLASS__;
		
		// PAGE DETAILS
		$page_details['cur_controller'] = $cur_controller;
		$page_details['menu']           = $menu;
		   
	   // DECLARE NULL VARIABLES 
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($date_type == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$data['date_type'] 	    = "";
		$data['from_date'] 	    = "0";
		$data['to_date'] 	    = "0";
		$data['utm_source']     = "";
		$data['clients']  		= "";				
		$data['search_field'] 	= "";
		$data['search_txt'] 	= "";
		$data["details"]        = "";
		$data["today_count"]    = "";
		$data["links"]          = "";
		$data['course']        = "";
		$tablename              = NMIMS;
		$data['tablename']      = $tablename;
		
	   $cur_controller 	= strtolower(__CLASS__);
	   $query_string = NULL;
	   $full_path = FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
	   
	   $data['source_data'] = $this->db->query("select id, utm_source from ".SOURCE_PIXEL." where status = '1' order by utm_source asc");	
	   
	   if(  (trim($this->input->post("btn_search")) != "" || $btn_search != "") && 
			(trim($this->input->post("date_type")) != ""  || $date_type != "") ) {
				
				// STORE SEARCH VALUE IN DATA ARRAY
				$data['btn_search']		= ($btn_search != "") 	? $btn_search 	: $this->input->post("btn_search");
				$data['date_type']		= ($date_type != "") 	? $date_type 	: $this->input->post("date_type");
				$data['utm_source']		= ($utm_source!= "") 	? $utm_source	: $this->input->post("utm_source");		
				//$data['clients']		= ($clients!= "") 		? $clients		: $this->input->post("clients");				
				$data['from_date']	    = ($from_date!= "")     ? $from_date    : $this->input->post("from_date");				
				$data['to_date']	    = ($to_date!= "")       ? $to_date      : $this->input->post("to_date");
				$data['course']		= ($course!= "") 	? $course	: $this->input->post("course");					
					
				$date_val = date_dropdown($data['date_type'], $data['from_date'], $data['to_date']);
				
				if(($data['course'] != "" && $data['course'] != "0") && ($data['utm_source'] != "" && $data['utm_source'] != "0" )){
				   
				   // PREPARE QUERY STRING
				   $query_string = $perpage."/".$data['btn_search']."/".$data['date_type']."/".$data['from_date']."/".$data['to_date']."/".$data['utm_source']."/".$data['course']."/";
				   // CREATE THE WHERE CLAUSE
				   $whr = $date_val." AND cource = '".$data['course']."' AND utm_source = '".$data['utm_source']."'";
				   
				}else if($data['course'] != "" && $data['course'] != "0"  && ($data['utm_source'] == "" || $data['utm_source'] == "0")){
				   
				   // PREPARE QUERY STRING
				   $query_string = $perpage."/".$data['btn_search']."/".$data['date_type']."/".$data['from_date']."/".$data['to_date']."/0/".$data['course']."/";
				   // CREATE THE WHERE CLAUSE
				   $whr = $date_val." AND course = '".$data['course']."'";
				   
				}else if($data['utm_source'] != "" && $data['utm_source'] != "0" && ($data['course'] == "" || $data['course'] == "0")){
				   
				   // PREPARE QUERY STRING
				   $query_string = $perpage."/".$data['btn_search']."/".$data['date_type']."/".$data['from_date']."/".$data['to_date']."/".$data['utm_source']."/0/";
				   // CREATE THE WHERE CLAUSE
				   $whr = $date_val." AND utm_source = '".$data['utm_source']."'";
				   
				}else{
				   // PREPARE QUERY STRING
				   $query_string = $perpage."/".$data['btn_search']."/".$data['date_type']."/".$data['from_date']."/".$data['to_date']."/0/0/";
				   
				   // CREATE THE WHERE CLAUSE
				   $whr = $date_val;
				}
				
				// CHECK FOR EXISTING CLAUSE	
				if($where == "") {
					$where = " where ".$whr;
				} else {
					$where .= " and ".$whr;
				}
			}else{
			   $data['from_date'] = $data['to_date']=  date('Y-m-d');
			   $data['date_type'] = 1;
			   $data['btn_search'] = "GO";
			   $date_val = date_dropdown($data['date_type'], $data['from_date'], $data['to_date']);
			   $query_string = $perpage."/".$data['btn_search']."/".$data['date_type']."/".$data['from_date']."/".$data['to_date']."/0/0/";
			   $where = " where ". $date_val;
			   //$query_string = $perpage."/";	
			}
	   	  
		
		 
		 //All active otp_optin
		 	   
		 	
		  	
		 $sel_query ="Select 
		                 id, name,phone,email,country,city,course,
		                 utm_source, utm_campaign, utm_sub,
		                 utm_medium, date_created
				     From 
					     ".$tablename.$where." 
					 ORDER BY  
					     date_created desc";
					 
       
		if($this->uri->segment(11) == "") {
			$config['uri_segment'] 	= 5;
		} else {
			$config['uri_segment'] 	= 11;
		}

	    // MODIFY FULL PATH
		$full_path .= $query_string;
		$config['total_rows'] 		= $this->db_function->count_record($sel_query, false);
		$config['per_page'] 		= $perpage;
		$config['base_url'] 		= $full_path;
		$choice 					= $config['total_rows'] / $config["per_page"];
		$config['num_links'] 		= 2;
		$config['full_tag_open'] 	= '<div id="paging" style="float:right; "><ul style="clear:left;">';
		$config['full_tag_close'] 	= '</ul></div>';
		$config['anchor_class'] 	= 'class="btn" ';
		$config['first_tag_open'] 	= '<li class="num_off">';
		$config['first_tag_close'] 	= '</li>';
		$config['last_tag_open'] 	= '<li class="num_off">';
		$config['last_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="num_on">';
		$config['cur_tag_close'] 	= '</li>';
		$config['num_tag_open'] 	= '<li class="num_off">';
    	$config['num_tag_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="num_off">';
    	$config['prev_tag_close'] 	= '</li>';
		$config['next_tag_open'] 	= '<li class="num_off">';
    	$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= 'PREVIOUS';
    	$config['next_link'] 		= 'NEXT';
		$config['use_page_numbers'] = FALSE;
		// PAGINATION PARAMETER VALUES END 				
		
		// PAGINATION PARAMETER INITIALIZE 
		$this->pagination->initialize($config);
		
		// SQL QUERY WITH OFFSET AND PERPAGE LIMIT
		$sql = $sel_query." limit ".$offset.", ".$perpage;

        $data["details"] = $this->db_function->get_data($sql);
		
		// GET THE DATA FROM PAGINATION
		$data["today_count"] = $config['total_rows'];
		
        $data["links"] = $this->pagination->create_links();
		
		$this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $page_details);
	    $this->load->view(CMS_FOLDER."users-details-nmims" ,  $data);
		$this->load->view(CMS_FOLDER.'footer'); 
	
   }


    
    public function offline_api_lead(){
     	/*
    	$from_date = "2019-05-28";
    	$to_date   = "2019-05-28";


    	$sel_query = $this->db->query("Select 
					                 id, name,phone,email,country,city,course,
					                 utm_source, utm_campaign, utm_sub,
					                 utm_medium, date_created
							     From 
								     ".NMIMS." 
								 WHERE 
								 	phone in (9654479753, 9268777319, 9560717449, 7459104452, 9711015595, 8439722339, 7066640146, 9958685726, 9711400609, 9816663444, 9870265173, 9811190418, 7042694524, 8299584536, 9012176899, 8954753113, 7678308234, 9999155545, 9510038212, 9920900613, 9538770899, 7021767835, 9935109271, 9004903121, 8160989262, 8126966555, 7988617223, 9711533532, 7028204001, 9205429935, 9555896730, 8961413266, 8586891523, 8001859778, 7365818281, 9987238648, 9699708977, 8291820502, 9619141205, 9911322838, 7549069954, 9999144423, 9910566616, 7095418820, 9810630604, 8758555489, 9924666976, 9334719430, 8239444438, 9718314729, 8800871727, 9167461968, 9007954412, 9831956332, 9769601577, 9833946105)");  
					    

    	
		if($sel_query->num_rows($sel_query)){
			foreach ($sel_query->result() as $val ) {
				
				$all_array = all_arrays();
				
				foreach($all_array['NMIMS_CITY'] as $k=>$v){
					if($val->city == $k){
						$city = $v;
					}
				}
				if($val->course == "Post Graduate Diploma - 2 yr "){
					$course = "Post Graduate Diploma Programs";
				}elseif($val->course ="Diploma - 1 yr"){
					$course = "Diploma Programs";
				}elseif($val->course="Certificate - 6 months"){
					$course = "Certificate Programs";
				}elseif($val->course ="Professional Programs - 1 year"){
					$course = "Professional Programs";
				}elseif($val->course = "master_programs"){
					$course = "Master Programs";
				}elseif($val->course = "digital_marketing"){
					$course = "Digital Marketing";
				}else{
					$course = "Executive Programs";
				}
				
				
				
				$data1 = array(
						 'Name'               => $val->name,
						 'ContactNo'          => $val->phone,
						 'EmailID'            => $val->email,
						 'CurrentLocation'    => $city ,
						 'CourseIntrestedIn'  => $course,
						 'Agency'    			=> "adcanopus1",
						 'AgencyPassword'       => "Adcanopus30@2019",
						 'HighestQualification' => " ",
						 'AdmissionYear'        => " " 					   
						);

				$api_url ="https://ngasce.secure.force.com/services/apexrest/leadservice";
				
				$ch = curl_init();

				curl_setopt($ch, CURLOPT_URL,$api_url);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
				curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data1));
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$server_output = curl_exec ($ch);

				echo $server_output;

				if(curl_errno($ch)){
                    echo 'Request Error:' . curl_error($ch);
                }

        	}
		}        */
	
	
   } 
   
   
   
   
}

?>