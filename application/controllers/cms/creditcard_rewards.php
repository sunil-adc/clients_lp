<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class creditcard_rewards extends CI_Controller
{
   public function __construct()
   {
        parent::__construct();
		global $page_details;
		global $product_arr;
		$this->load->model(array( CMS_FOLDER.'common_model', CMS_FOLDER.'/db_function'));
		$this->db->cache_off();
		$this->tablename					  = REWARDS;	
		$this->page_details['cur_controller'] = strtolower(__CLASS__);
		$this->page_details['menu']           = $this->common_model->Menu_Array();;
		
		//MEMCACHE FOR PRODUCT
		//$get_product = $this->cache_model->get_cache_withstatus(PRODUCT_CACHE, prod_const, PRODUCT, "", ""); 
		
		/*if(is_array($get_product) && count($get_product)){
		  $this->product_arr = $get_product;
		}*/
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
		
   }

   public function index($prod_id = NULL){
		
		// DECLARE NULL VARIABLES 
		$cur_controller 		= strtolower(__CLASS__);
		$query_string       	= NULL;
		$full_path 				= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';	   
	    $query_string 			= NULL;
		$where = $whr 			= NULL;
		$tablename				= $this->tablename;
		$data['get_product']	= "";//$this->product_arr;
		$data['prod_id'] 	 	= $prod_id;
		$data['status']         = "";
		$data['page_title']	 	= "Credit card Rewards";
		$data['add_page_title']	= "Add Credit card Rewards";
		$data['add_edit_page'] 	= $cur_controller."/add_edit_form";
		$data['tablename']	 	= $tablename;
		$data['primary_field']	= "id";
		$data['form_submit'] 	= FULL_CMS_URL."/".$this->page_details['cur_controller']."/add_edit_action";
		
	    $order_by = " order by date_created desc";
	    $where 	  = " where prod_id = ".$prod_id; 
		$sel_query="SELECT id, prod_id, rewards, status, date_created, date_updated FROM ".$tablename.$where.$order_by;
		
		$data["details"] = $this->db_function->get_data($sel_query);
			   
	    $this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
	    $this->load->view(CMS_FOLDER."manage-creditcard-rewards" ,  $data);
		$this->load->view(CMS_FOLDER.'footer'); 

   }   
   
   public function add_edit_form($mode = "add", $id = NULL){
	   
		// DETAILS WE WANT TO SEND IN VIEW
		$data['page_name']	 	 	= "Credit card rewards";
		$data['manage_page_title']	= "Manage ".$data['page_name'];
		$data['page_title']	 		= ucfirst($mode)." ".$data['page_name'];
		$data['manage_page'] 		= $this->page_details['cur_controller'];
		$data['add_page'] 			= $this->page_details['cur_controller']."/add_edit_form";
		$data['tablename']	 		= $this->tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$this->page_details['cur_controller']."/add_edit_action";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] = NULL;
		$data['result_data'][$data['primary_field']]	    = NULL;
		$data['result_data']['prod_id'] 					= NULL;
		$data['result_data']['rewards'] 					= NULL;
		$data['result_data']['status']					    = NULL;
		$data['result_data']['date_created']				= NULL;
		$data['result_data']['date_updated']				= NULL;
		
		
		if ($mode == 'edit' && is_numeric($id)) {
			$val = $this->db_function->get_single_row($this->tablename, 'id, prod_id, rewards, 
																		 status, 	date_created, 
																		 date_updated', 
																		 $data['primary_field'].'='.$id);
			$data['result_data'] = $val;	
		}
				
		$this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $this->page_details);
		$this->load->view(CMS_FOLDER."add-creditcard-rewards", $data);
		$this->load->view(CMS_FOLDER.'footer'); 
   }
   
   public function add_edit_action(){
		
		$this->form_validation->set_rules('prod_id', 'prod id', 'required');
		$this->form_validation->set_rules('rewards', 'rewards', 'required');
		
		if( ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				$_POST['mode'] = strtolower($_POST['mode']);
				
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['id'])) { 		
					// SET THE WHERE CLAUSE
					$this->db->where("id",$_POST['id']);
					
					// SET THE DATA
					$data = array(
									'prod_id'          => $_POST['prod_id'],
									'rewards'          => $_POST['rewards'],
									'status'           => $_POST['status'],
									'date_updated'     => date('Y-m-d H:i:s')
								);
								
					
					// UPDATE QUERY
					$this->db->update($this->tablename,$data);
					$this->session->set_flashdata('success', 'Updated successfully');
					redirect(CMS_FOLDER.$this->page_details['cur_controller']."/index/".$_POST['prod_id']);
					
				} else if ( trim ($_POST['mode']) == 'add') { 			
					
					// SET THE DATA FOR INSERTION
					$data = array(
									'prod_id'          => $_POST['prod_id'],
									'rewards'          => $_POST['rewards'],
									'status'           => $_POST['status'],
									'date_created'     => date('Y-m-d H:i:s')
								);
						
                    // INSERT QUERY
					$this->db->insert($this->tablename,$data);
					$insert_id = $this->db->insert_id();
					$this->session->set_flashdata('success', 'Added successfully');
					redirect(CMS_FOLDER.$this->page_details['cur_controller']."/index/".$_POST['prod_id']);
				}
				
			} else {
				
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect(CMS_FOLDER.$_POST['add_page']);
				
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again');
			redirect(CMS_FOLDER.$_POST['add_page']);	
		}
	}

   
   
}

?>