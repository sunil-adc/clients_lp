<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class medlife_report extends CI_Controller
{
   public $global_country;
   public function __construct(){
        
		parent::__construct();
		$this->load->library('memcached');
		$this->load->model(array(CMS_FOLDER.'common_model', CMS_FOLDER.'db_function', 'cache_model'));
		$this->db->cache_off();
		//$this->output->enable_profiler(TRUE);
		$this->global_country =  ($this->session->userdata('cms_country') != "" ) ? $this->session->userdata('cms_country') : '';
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
		
   }

   
    public function index($perpage = 10, $offset = 0){
	   
	   $perpage = 10;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, NULL, NULL, NULL, 0);
    }		
   
    public function page($perpage = 10, $btn_search = NULL, $date_type = NULL, $from_date = NULL, $to_date = NULL, $utm_source = NULL, $utm_aff = NULL, $offset = 0){
	   	
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= __CLASS__;
		
		// PAGE DETAILS
		$page_details['cur_controller'] = $cur_controller;
		$page_details['menu']           = $menu;
		   
	   // DECLARE NULL VARIABLES 
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($date_type == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$data['date_type'] 	    = "";
		$data['from_date'] 	    = "0";
		$data['to_date'] 	    = "0";
		$data['utm_source'] 	    = "";
		$data['utm_aff']  		= "";				
		
		$tablename              = MEDLIFE_USER;
		$data['tablename']      = $tablename;
		
	   $cur_controller 	= strtolower(__CLASS__);
	   $query_string = NULL;
	   $full_path = FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
	   
	   if(  (trim($this->input->post("btn_search")) != "" || $btn_search != "") && 
			(trim($this->input->post("date_type")) != ""  || $date_type != "") || 
			((trim($this->input->post("utm_source")) != ""      || $utm_source != ""))) {
				
				// STORE SEARCH VALUE IN DATA ARRAY
				$data['btn_search']		= ($btn_search != "") 	? $btn_search 	: $this->input->post("btn_search");
				$data['date_type']		= ($date_type != "") 	? $date_type 	: $this->input->post("date_type");
				$data['utm_source']		= ($utm_source != "") 	    ? $utm_source 	    : $this->input->post("utm_source");
				$data['utm_aff']		= ($utm_aff!= "") 		? $utm_aff		: $this->input->post("utm_aff");				
				$data['from_date']	    = ($from_date!= "")     ? $from_date    : $this->input->post("from_date");				
				$data['to_date']	    = ($to_date!= "")       ? $to_date      : $this->input->post("to_date");				
				
				$date_val = date_dropdown($data['date_type'], $data['from_date'], $data['to_date']);

				if($data['utm_source'] != "" && $data['utm_source'] != '0' && $data['utm_aff'] != "" && $data['utm_aff'] != '0'){
				   
				   // CREATE THE WHERE CLAUSE
				   $whr = $date_val." AND utm_source = '".$data['utm_source']."' and utm_aff = '".$data['utm_aff']."'";
				   
				}else if($data['utm_source'] != ""){
				   
				  
				   
				   // CREATE THE WHERE CLAUSE
				   $whr = $date_val." AND utm_source = '".$data['utm_source']."'";
				   
				}else if($data['utm_aff'] != "" && $data['utm_aff'] != '0'){
				   
				 
				   
				   // CREATE THE WHERE CLAUSE
				   $whr = $date_val." and utm_aff = '".$data['utm_aff']."'";
				   
				}else{
				  // CREATE THE WHERE CLAUSE
				   $whr = $date_val;
				}
				
				// CHECK FOR EXISTING CLAUSE	
				if($where == "") {
					$where = " where ".$whr;
				} else {
					$where .= " and ".$whr;
				}
			}else{

			   $data['from_date'] = $data['to_date']=  date('Y-m-d');
			   $data['date_type'] = 1;
			   $data['btn_search'] = "GO";
			   $date_val = date_dropdown($data['date_type'], $data['from_date'], $data['to_date']);
			   $data['utm_source'] ="adc";
			   $where = " where ". $date_val." and utm_source = 'adc'";
			   //$query_string = $perpage."/";	
			}
        

		
	   	$sql_total_count= $this->db->query("select id as totalcount  from tbl_medlife_user ".$where.""); 
        if($sql_total_count->num_rows()> 0){
         	
         		$data['total_count'] = $sql_total_count->num_rows;
         	
         }else{
         	$data['total_count'] = 0;
         }
		 $sql_total_fired= $this->db->query("select id as firedcount from tbl_medlife_user ".$where." and fire_status = 1"); 
         if($sql_total_fired->num_rows()> 0){
         	
         		$data['total_fired'] = $sql_total_fired->num_rows;
         	
         }else{
         	$data['total_fired'] = 0;
         }
         $sql_total_optin= $this->db->query("select id as firedcount from tbl_medlife_user ".$where." and otp_optin = 1"); 
         if($sql_total_optin->num_rows()> 0){
         	
         		$data['total_optin'] = $sql_total_optin->num_rows;
         	
         }else{
         	$data['total_optin'] = 0;
         }

         $sql_total_sent= $this->db->query("select id as firedcount from tbl_medlife_user ".$where." and sent_status in (2,3)"); 
         if($sql_total_sent->num_rows()> 0){
         	
         		$data['total_sent'] = $sql_total_sent->num_rows;
         	
         }else{
         	$data['total_sent'] = 0;
         }

          $sql_total_optinsent= $this->db->query("select id as firedcount from tbl_medlife_user ".$where." and otp_optin = 1 and sent_status = 3"); 
         if($sql_total_optinsent->num_rows()> 0){
         	
         		$data['total_sent_optin'] = $sql_total_optinsent->num_rows;
         	
         }else{
         	$data['total_sent_optin'] = 0;
         }

          $sql_total_smallform= $this->db->query("select id as firedcount from tbl_medlife_user ".$where." and form_type = 1"); 
         if($sql_total_smallform->num_rows()> 0){
         	
         		$data['total_smallform'] = $sql_total_smallform->num_rows;
         	
         }else{
         	$data['total_smallform'] = 0;
         }

          $sql_total_bigform= $this->db->query("select id as firedcount from tbl_medlife_user ".$where." and form_type = 2"); 
         if($sql_total_bigform->num_rows()> 0){
         	
         		$data['total_bigform'] = $sql_total_bigform->num_rows;
         	
         }else{
         	$data['total_bigform'] = 0;
         }



		$data['source_data'] = $this->db->query("select id, utm_source from ".SOURCE_PIXEL." where status = '1' order by utm_source asc");
		$utm_source_id = "";
		foreach ($data['source_data']->result() as $row) {
			if($data['utm_source'] == $row->utm_source ){
				
         		$utm_source_id = " and utm_source =".$row->id;
			
         	}
         }
        
		$data['aff_data'] = $this->db->query("select id, utm_aff from ".AFFILIATE_PIXEL." where status = '1' ".$utm_source_id." order by utm_aff asc");
       
		$this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $page_details);
	    $this->load->view(CMS_FOLDER."medlife-report" ,  $data);
		$this->load->view(CMS_FOLDER.'footer'); 
	
   }
   
   
   
   
}

?>