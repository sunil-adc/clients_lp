<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class winmore_email_cron extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->library("excel");
		$this->load->model(array("cache_model", "email_templates"));
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), $this->input->get('bnr'), $this->input->get('country'),  $this->input->get('zoneid'),  $this->input->get('mobile_track'), $this->input->get('ClickID'));
		
	}
   
	public function index(){				
	   
	   echo "Access Denied";
	   die();

    }
   
   
	
	
	public function winmore_email(){
		
		$all_array = all_arrays();
        $array ="";		
		$today_date = date('Y-m-d',strtotime("-1 days"));

		$dir=S3_URL."/downloads/";
		
		
		$sel_query =$this->db->query("Select 
										  id, name, phone, email, country, grade, 
										  utm_source, utm_campaign, date_created 
										    
									 From 
										 ".WINMORE."
									where
										DATE_FORMAT(date_created, '%Y-%m-%d') between '".$today_date."' and '".$today_date."' 
									ORDER BY  
										 date_created desc ");
		
		if($sel_query->num_rows() > 0 ){
		
			$fileName = "winmore_".$today_date;
			$object = new PHPExcel();
			$object->setActiveSheetIndex(0);
			$table_columns = array("Name", "Phone", "Email", "Grade", "Utm Source", "Utm Campaign", "Date");
		    $column = 0;
			foreach($table_columns as $field)
			{
			   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
			   $column++;
			}
	        $excel_row = 2;
			
			foreach($sel_query->result() as $row)
			{
			   $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->name);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->phone);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->email);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $all_array['WINMORE_GRADE'][$row->grade]);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->utm_source);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->utm_campaign);
			   $date = date("Y-m-d", strtotime($row->date_created));
			   $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $date);
			   $excel_row++;
			}
				$object->getActiveSheet()->setTitle($fileName);
				$objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel5');
	            $objWriter->save(getcwd().'/cdn/downloads/'.$fileName . '.xls');
			    //$email_arr = array('sahana@adcanopus.com','atul@adcanopus.com');
				$to1 = "preethu@adcanopus.com";
				$to2 = "Shrikathiresan@viscouscubes.com";
				$to3 = "Rajesh.tak@viscouscubes.com"; 
				$to4 = "Dhanraj.v@puravankara.com";
				$to5 = "info@winmoreacademy.com";
				
			
				$this->email_templates->email_winmore("atul@adcanopus.com", $today_date, $to1, $to2, $to3, $to4, $to5);
				
			}else{
				echo "No Record Found";
			}
		 
	}
	
	

}
?>