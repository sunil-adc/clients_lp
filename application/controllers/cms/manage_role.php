<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class manage_role extends CI_Controller
{
   public function __construct()
   {
        parent::__construct();
		$this->load->model(array( CMS_FOLDER.'common_model', CMS_FOLDER.'/db_function'));
		$this->db->cache_off();
		$this->global_country =  ($this->session->userdata('cms_country') != "" ) ? $this->session->userdata('cms_country') : '';
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
		
   }

   public function index($perpage = 30, $offset = 0){
	   
	   $perpage = 30;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, 0);
   }		
   
    public function page($perpage = 30, $btn_search = NULL, $search_field = NULL, $search_txt = NULL, $offset = 0){
       
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= __CLASS__;
		
		// PAGE DETAILS
		$page_details['cur_controller'] = $cur_controller;
		$page_details['menu']           = $menu;
		
		
	   // DECLARE NULL VARIABLES 
	    $tablename    = ADMIN_ROLE;
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($search_field == NULL && $search_txt == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$data['search_field'] 	= "";
		$data['search_txt'] 	= "";
		
	   $cur_controller 	= strtolower(__CLASS__);
	   $query_string = NULL;
	   $full_path = FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
	   
	    if( (trim($this->input->post("btn_search")) != "" || $btn_search != "") || 
			((trim($this->input->post("search_field")) != "" || $search_field != "") && 
			(trim($this->input->post("search_txt")) != "" || $search_txt != ""))) {
			
			// STORE SEARCH VALUE IN DATA ARRAY
			$data['btn_search']		= ($btn_search != "") 	? $btn_search 	: trim($this->input->post("btn_search"));
			$data['search_field'] 	= ($search_field != "") ? $search_field : trim($this->input->post("search_field"));
			$data['search_txt']		= ($search_txt != "") 	? $search_txt 	: trim($this->input->post("search_txt"));
			 
			$query_string = $perpage."/".$data['btn_search']."/".$data['search_field']."/".$data['search_txt'];
			$whr = $data['search_field']." like '%".$data['search_txt']."%'";
			
			// CHECK FOR EXISTING CLAUSE
			if($where == "") {
				$where = " where ".$whr;
			} else {
				$where .= " and ".$whr;
			}
		} else {
			
			$query_string = $perpage;
			
		}
	   
	    
	    $order_by = " order by date_created desc";
	   
		$sel_query="SELECT id, role_name, role_details, status, date_created, date_updated FROM ".$tablename.$where.$order_by;
		
	   if($this->uri->segment(9) == "") {
			$config['uri_segment'] 	= 5;
		} else {
			$config['uri_segment'] 	= 9;
		}
		
	   // MODIFY FULL PATH
		$full_path .= $query_string;
		
		$config['total_rows'] 		= $this->db_function->count_record($sel_query, false);
		$config['per_page'] 		= $perpage;
		$config['base_url'] 		= $full_path;
		$choice 					= $config['total_rows'] / $config["per_page"];
		$config['num_links'] 		= 2;
		$config['full_tag_open'] 	= '<div id="paging" style="float:right; "><ul style="clear:left;">';
		$config['full_tag_close'] 	= '</ul></div>';
		$config['anchor_class'] 	= 'class="btn" ';
		$config['first_tag_open'] 	= '<li class="num_off">';
		$config['first_tag_close'] 	= '</li>';
		$config['last_tag_open'] 	= '<li class="num_off">';
		$config['last_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="num_on">';
		$config['cur_tag_close'] 	= '</li>';
		$config['num_tag_open'] 	= '<li class="num_off">';
    	$config['num_tag_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="num_off">';
    	$config['prev_tag_close'] 	= '</li>';
		$config['next_tag_open'] 	= '<li class="num_off">';
    	$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= 'PREVIOUS';
    	$config['next_link'] 		= 'NEXT';
		$config['use_page_numbers'] = FALSE;
		// PAGINATION PARAMETER VALUES END 				
		
		// PAGINATION PARAMETER INITIALIZE 
		$this->pagination->initialize($config);
		
		// SQL QUERY WITH OFFSET AND PERPAGE LIMIT
		$sql = $sel_query." limit ".$offset.", ".$perpage;
	
        $data["details"] = $this->db_function->get_data($sql);
		
		// GET THE DATA FROM PAGINATION
		$data["today_count"] = $config['total_rows'];
		
        $data["links"] = $this->pagination->create_links();
		
	    $this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $page_details);
	    $this->load->view(CMS_FOLDER."manage-role" ,  $data);
		$this->load->view(CMS_FOLDER.'footer'); 
   

   }   
   
   public function add_form(){
	   
	    $menu = $this->common_model->Menu_Array();
		$data['menu_list'] = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= __CLASS__;
		
		// PAGE DETAILS
		$page_details['cur_controller'] = $cur_controller;
		$page_details['menu']           = $menu;
		
		
	   $this->load->view(CMS_FOLDER."header");
	   $this->load->view(CMS_FOLDER."sidebar", $page_details);
	   $this->load->view(CMS_FOLDER."add-role" , $data);
	   $this->load->view(CMS_FOLDER.'footer'); 
   }
   
   public function add(){
	   
       $this->form_validation->set_rules('role_name', 'Role Name', 'trim|required');
	   $this->form_validation->set_rules('role_details', 'Role Details', 'required');
	   
	   if($this->form_validation->run() == FALSE){
		  $this->session->set_flashdata('error', "Fields marked * are mandatory"); 
		  redirect(SITE_URL."index.php/cms/manage_role/add_form");
       
	   }else{
		    if($this->db_function->get_data("SELECT role_name, role_details FROM ".ADMIN_ROLE." WHERE role_name = '".trim($this->input->post('role_name ')."'"))) {
		   
			   $this->session->set_flashdata('error', 'Role Alredy existed, ');              
			   redirect(SITE_URL."index.php/cms/manage_role/add_form"); 
			   
		    }else{
				
				 
			   $setting_details=array();
			   $setting_details['role_name']               = trim($this->input->post('role_name'));
			   $setting_details['role_details']            = trim(serialize($this->input->post('role_details')));;
			   $setting_details['date_created']            = date('Y-m-d H:i:s');			   
			   
			   $this->db->insert(ADMIN_ROLE,$setting_details);
			  
			   $this->session->set_flashdata('success', 'Role by the name '.$setting_details['role_name'].' added');              
			   redirect(SITE_URL."index.php/cms/manage_role/add_form"); 
		   }
	   }
   }
   
   public function edit_form($id=''){
	    $menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= __CLASS__;
		
		// PAGE DETAILS
		$page_details['cur_controller'] = $cur_controller;
		$page_details['menu']           = $menu;
		
	   $tablename = ADMIN_ROLE;
	   
	   if($id !=""){
		   $query=$this->db->query("SELECT
										id, role_name, role_details, status, date_created, date_updated
									FROM
									 	".$tablename."
									WHERE 
										id= ".$id);
		   
		   if ( $query->num_rows() > 0) {
				foreach ($query->result() as $p) {
				  
				  $data['result_data'] = $p;
				  $this->load->view(CMS_FOLDER."header");
				  $this->load->view(CMS_FOLDER."sidebar", $page_details);	    
				  $this->load->view(CMS_FOLDER."edit-role" , $data);
				  $this->load->view(CMS_FOLDER.'footer'); 
				}
			}else{
		         redirect(CMS_FOLDER.'manage_role');
	        }
	   }
   }
   
   
   public function edit(){
	   
	   $id=$this->input->post('id');
	   if( $id){
           
	       $this->form_validation->set_rules('role_name', 'Role Name', 'trim|required');
	       $this->form_validation->set_rules('role_details', 'Role Details', 'required');
	   
		   if($this->form_validation->run() == FALSE){
			  $this->session->set_flashdata('message', validation_errors());
			  redirect(CMS_FOLDER.'manage_role/edit_form/'.$id);
		   
		   }else{
			   
			   $this->db->where("id",$_POST['id']);

			   $setting_details=array();
			   $setting_details['role_name']             = trim($this->input->post('role_name'));
			   $setting_details['role_details']          = trim(serialize($this->input->post('role_details')));
			   $setting_details['status']                = trim($this->input->post('status'));
			   $setting_details['date_updated']          = date('Y-m-d H:i:s');			   
			   
			   
			   $this->db->update(ADMIN_ROLE,$setting_details);
			   
			   $this->session->set_flashdata('success', 'Succesfully updated', 'Succesfully updated');
		       
			   redirect(CMS_FOLDER.'manage_role');  
	      }
	   }else{   
	       
		   $this->session->set_flashdata('error', 'Error Ocurred');
		   redirect(CMS_FOLDER.'manage_role');
	   }
   }
   
   
}


?>