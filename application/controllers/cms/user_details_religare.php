<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_details_religare extends CI_Controller
{
   public $global_country;
   public function __construct(){
        
		parent::__construct();
		$this->load->library('memcached');
		$this->load->model(array(CMS_FOLDER.'common_model', CMS_FOLDER.'db_function', 'cache_model'));
		$this->db->cache_off();
		$this->global_country =  ($this->session->userdata('cms_country') != "" ) ? $this->session->userdata('cms_country') : '';
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
		
   }

   
   public function index($perpage = 10, $offset = 0){
	   
	   $perpage = 10;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);
   }		
   
    public function page($perpage = 10, $btn_search = NULL, $date_type = NULL, $from_date = NULL, $to_date = NULL, $utm_source = NULL, $utm_campaign = NULL, $type = NULL, $offset = 0){
	   	
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= __CLASS__;
		
		// PAGE DETAILS
		$page_details['cur_controller'] = $cur_controller;
		$page_details['menu']           = $menu;
		   
	   // DECLARE NULL VARIABLES 
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($date_type == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		$data['source_data'] = $this->db->query("select id, utm_source from ".SOURCE_PIXEL." where status = '1' order by utm_source asc");	
		$data['date_type'] 	    = "";
		$data['from_date'] 	    = "0";
		$data['to_date'] 	    = "0";
		$data['otp_optin'] 	    = "";
		$data['clients']  		= "";				
		$data['search_field'] 	= "";
		$data['search_txt'] 	= "";
		$data["details"]        = "";
		$data["today_count"]    = "";
		$data["links"]          = "";
		$tablename              = RELIGARE_USER;
		$data['tablename']      = $tablename;
		
	   $cur_controller 	= strtolower(__CLASS__);
	   $query_string = NULL;
	   $full_path = FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
	   
	   if(  (trim($this->input->post("btn_search")) != "" || $btn_search != "") && 
			(trim($this->input->post("date_type")) != ""  || $date_type != "") ) {
				
				// STORE SEARCH VALUE IN DATA ARRAY
				$data['btn_search']		= ($btn_search != "") 	? $btn_search 	: $this->input->post("btn_search");
				$data['date_type']		= ($date_type != "") 	? $date_type 	: $this->input->post("date_type");
				$data['utm_source']		= ($utm_source != "") 	? $utm_source 	: $this->input->post("utm_source");
				$data['utm_campaign']	= ($utm_campaign != "") ? $utm_campaign : $this->input->post("utm_campaign");
				$data['type']			= ($type!= "") 			? $type			: $this->input->post("type");				
				$data['from_date']	    = ($from_date!= "")     ? $from_date    : $this->input->post("from_date");				
				$data['to_date']	    = ($to_date!= "")       ? $to_date      : $this->input->post("to_date");				
				
				$date_val = date_dropdown($data['date_type'], $data['from_date'], $data['to_date']);
				
				if($data['utm_source'] != "" && $data['utm_source'] != '0' && $data['type'] != "" && $data['type'] != '0'){
				   
				   // PREPARE QUERY STRING
				   $query_string = $perpage."/".$data['btn_search']."/".$data['date_type']."/".$data['from_date']."/".$data['to_date']."/".$data['utm_source']."/0/".$data['type'];
				   // CREATE THE WHERE CLAUSE
				   $whr = $date_val." AND utm_source = '".$data['utm_source']."' and type = '".$data['type']."'";
				   
				}else if($data['utm_source'] != "" && $data['utm_source'] != "0"){
				   
				   // PREPARE QUERY STRING
				   $query_string = $perpage."/".$data['btn_search']."/".$data['date_type']."/".$data['from_date']."/".$data['to_date']."/".$data['utm_source']."/0/0/";
				   // CREATE THE WHERE CLAUSE
				   $whr = $date_val." AND utm_source = '".$data['utm_source']."'";
				   
				}else if($data['type'] != "" && $data['type'] != '0'){
				   
				   // PREPARE QUERY STRING
				   $query_string = $perpage."/".$data['btn_search']."/".$data['date_type']."/".$data['from_date']."/".$data['to_date']."/0/0/".$data['type']."/";
				   // CREATE THE WHERE type
				   $whr = $date_val." and type = '".$data['type']."'";
				   
				}else if($data['utm_campaign'] != "" && $data['utm_campaign'] != '0'){
				   
				   // PREPARE QUERY STRING
				   $query_string = $perpage."/".$data['btn_search']."/".$data['date_type']."/".$data['from_date']."/".$data['to_date']."/0/".$data['utm_campaign']."/0/";
				   // CREATE THE WHERE type
				   $whr = $date_val." AND utm_campaign like '%".$data['utm_campaign']."%'";
				   
				}else{
				   // PREPARE QUERY STRING
				   $query_string = $perpage."/".$data['btn_search']."/".$data['date_type']."/".$data['from_date']."/".$data['to_date']."/0/0";
				   
				   // CREATE THE WHERE CLAUSE
				   $whr = $date_val;
				}
				
				// CHECK FOR EXISTING CLAUSE	
				if($where == "") {
					$where = " where ".$whr;
				} else {
					$where .= " and ".$whr;
				}
			}else{
			   $data['from_date'] = $data['to_date']=  date('Y-m-d');
			   $data['date_type'] = 1;
			   $data['btn_search'] = "GO";
			   $date_val = date_dropdown($data['date_type'], $data['from_date'], $data['to_date']);
			   $query_string = $perpage."/".$data['btn_search']."/".$data['date_type']."/".$data['from_date']."/".$data['to_date']."/0/0/0";
			   $where = " where ". $date_val;
			   //$query_string = $perpage."/";	
			}
	   	  
		
		 
		 //All active otp_optin
		 	   
		 	
		  	
		 $sel_query ="Select 
		                 id, name, phone, email,city,utm_campaign,type,utm_source,
					      date_created 
				     From 
					     ".$tablename.$where." 
					 ORDER BY  
					     id desc";
						 
       
		if($this->uri->segment(12) == "") {
			$config['uri_segment'] 	= 5;
		} else {
			$config['uri_segment'] 	= 12;
		}

	    // MODIFY FULL PATH
		$full_path .= $query_string;
		
		$config['total_rows'] 		= $this->db_function->count_record($sel_query, false);
		$config['per_page'] 		= $perpage;
		$config['base_url'] 		= $full_path;
		$choice 					= $config['total_rows'] / $config["per_page"];
		$config['num_links'] 		= 2;
		$config['full_tag_open'] 	= '<div id="paging" style="float:right; "><ul style="clear:left;">';
		$config['full_tag_close'] 	= '</ul></div>';
		$config['anchor_class'] 	= 'class="btn" ';
		$config['first_tag_open'] 	= '<li class="num_off">';
		$config['first_tag_close'] 	= '</li>';
		$config['last_tag_open'] 	= '<li class="num_off">';
		$config['last_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="num_on">';
		$config['cur_tag_close'] 	= '</li>';
		$config['num_tag_open'] 	= '<li class="num_off">';
    	$config['num_tag_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="num_off">';
    	$config['prev_tag_close'] 	= '</li>';
		$config['next_tag_open'] 	= '<li class="num_off">';
    	$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= 'PREVIOUS';
    	$config['next_link'] 		= 'NEXT';
		$config['use_page_numbers'] = FALSE;
		// PAGINATION PARAMETER VALUES END 				
		
		// PAGINATION PARAMETER INITIALIZE 
		$this->pagination->initialize($config);
		
		// SQL QUERY WITH OFFSET AND PERPAGE LIMIT
		$sql = $sel_query." limit ".$offset.", ".$perpage;

        $data["details"] = $this->db_function->get_data($sql);
		
		// GET THE DATA FROM PAGINATION
		$data["today_count"] = $config['total_rows'];
		
        $data["links"] = $this->pagination->create_links();
		
		$this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $page_details);
	    $this->load->view(CMS_FOLDER."users-details-religare" ,  $data);
		$this->load->view(CMS_FOLDER.'footer'); 
	
   }
   
   
   
    public function offline_api_lead(){
    	/*

		$this->load->library('XML2Array');

    	$sel_query = $this->db->query("Select 
			                 id, name, phone, email, city, utm_campaign,type,utm_source,
							date_created 
					     From 
						     ".RELIGARE_USER." where 
						     type = 'health' and  lead_id = '' and date_format(date_created , '%Y-%m-%d') between '2019-09-06' and '2019-09-06' ");   

    	
    	echo $sel_query->num_rows($sel_query);

		if($sel_query->num_rows($sel_query)){
			foreach ($sel_query->result() as $val ) {
				
				$user_name  = $val->name;
				$user_phone = $val->phone;
				$user_city  = $val->city;
				$user_email = $val->email;
				$insert_id  = $val->id;


				$user_name = preg_replace('/\s+/', '', $user_name);

				$user_name = str_replace(' ', '', $user_name );

				$api_url = "https://cordysprod.religarehealthinsurance.com/LEADCREATIONREST/RHIS/CreateLead/subject=Optimise_emailer&mobilephone=".$user_phone."&rhi_plan=Care&rhi_product=Health&rhi_leadstage=Cr-Quotation&rhi_agentid=%2020030031&firstname=".$user_name."&emailaddress1=".$user_email."&utmsource=Web%20AdCanopus&location=".$user_city."&Valid=&medium=%20API&utm_campaign=%20May_Care_AC&utm_content=%20AC_June_19";

						$this->db->query("update ".RELIGARE_USER." set api_request = '".$api_url."' where id =".$insert_id); 
					    
						$curl = curl_init();

						  curl_setopt_array($curl, array(
						  CURLOPT_URL => $api_url,
						  CURLOPT_RETURNTRANSFER => true,
						  CURLOPT_ENCODING => "",
						  CURLOPT_MAXREDIRS => 10,
						  CURLOPT_TIMEOUT => 30,
						  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						  CURLOPT_SSL_VERIFYHOST => false,
						  CURLOPT_SSL_VERIFYPEER => false,
						  CURLOPT_CUSTOMREQUEST => "GET",
						  CURLOPT_HTTPHEADER => array(
						    "authorization: Basic U3ltYmlvc3lzVXNlcjpQYXNzd29yZC0x",
						    "cache-control: no-cache",
						    "content-type: application/xml",
						  ),
						));


						$response = curl_exec($curl);
						
						$err = curl_error($curl);
						
						if ($err) {
						  $this->db->query("update ".RELIGARE_USER." set api_response = '".$err."' where id =".$insert_id);
						} else {
						  $this->db->query("update ".RELIGARE_USER." set api_response = '".$response."' where id =".$insert_id);
						}

							


						curl_close($curl); 

						$array_res = XML2Array::createArray($response);

						$html_response=$array_res['SOAP-ENV:Envelope']['SOAP-ENV:Body']['ns1:StockController.createLeadResponse']['return']['CreateleadSoapOut']['CreateleadResponse']['CreateleadResult'];

						$result_data = XML2Array::createArray($html_response);

						if( $result_data['Response']){
						    $status = $result_data['Response']['StatusMsg'];
						    
						   if($status=='Success'){
						       $lead_id = $result_data['Response']['LeadId'];

						       print_r($lead_id) ;
						       
						        if($lead_id != ""){
			                        //$this->general->update_lead(RELIGARE_USER,$insert_id,$lead_id);
			                        $this->db->query("update ".RELIGARE_USER." set lead_id = '".$lead_id."' where id =".$insert_id);
			                    }

			                   echo "done<br/>"; 
						         
						    }else{
						       echo 'failed';
						       

						    }
						}

			
			}
		}

	*/

    }

}

?>