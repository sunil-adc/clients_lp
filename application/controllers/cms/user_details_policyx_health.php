<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_details_policyx_health  extends CI_Controller
{
   public $global_country;
   public function __construct(){
        
		parent::__construct();
		$this->load->library('memcached');
		$this->load->model(array(CMS_FOLDER.'common_model', CMS_FOLDER.'db_function', 'cache_model'));
		$this->db->cache_off();
		$this->global_country =  ($this->session->userdata('cms_country') != "" ) ? $this->session->userdata('cms_country') : '';
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);  
		
   }

   
   public function index($perpage = 10, $offset = 0){
	   
	   $perpage = 10;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, NULL, NULL, NULL, 0);
   }		
   
    public function page($perpage = 10, $btn_search = NULL, $date_type = NULL, $from_date = NULL, $to_date = NULL, $otp_optin = NULL, $clients = NULL, $offset = 0){
	   	
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= __CLASS__;
		
		// PAGE DETAILS
		$page_details['cur_controller'] = $cur_controller;
		$page_details['menu']           = $menu;
		   
	   // DECLARE NULL VARIABLES 
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($date_type == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$data['date_type'] 	    = "";
		$data['from_date'] 	    = "0";
		$data['to_date'] 	    = "0";
		$data['otp_optin'] 	    = "";
		$data['clients']  		= "";				
		$data['search_field'] 	= "";
		$data['search_txt'] 	= "";
		$data["details"]        = "";
		$data["today_count"]    = "";
		$data["links"]          = "";
		$tablename              = POLICY_HEALTH;
		$data['tablename']      = $tablename;
		
	   $cur_controller 	= strtolower(__CLASS__);
	   $query_string = NULL;
	   $full_path = FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
	   
	   if(  (trim($this->input->post("btn_search")) != "" || $btn_search != "") && 
			(trim($this->input->post("date_type")) != ""  || $date_type != "") ) {
				
				// STORE SEARCH VALUE IN DATA ARRAY
				$data['btn_search']		= ($btn_search != "") 	? $btn_search 	: $this->input->post("btn_search");
				$data['date_type']		= ($date_type != "") 	? $date_type 	: $this->input->post("date_type");
				//$data['otp_optin']		= ($otp_optin != "") 	    ? $otp_optin 	    : $this->input->post("otp_optin");
				//$data['clients']		= ($clients!= "") 		? $clients		: $this->input->post("clients");				
				$data['from_date']	    = ($from_date!= "")     ? $from_date    : $this->input->post("from_date");				
				$data['to_date']	    = ($to_date!= "")       ? $to_date      : $this->input->post("to_date");				
				
				$date_val = date_dropdown($data['date_type'], $data['from_date'], $data['to_date']);
				
				if($data['otp_optin'] != "" && $data['otp_optin'] != '0' && $data['clients'] != "" && $data['clients'] != '0'){
				   
				   // PREPARE QUERY STRING
				   $query_string = $perpage."/".$data['btn_search']."/".$data['date_type']."/".$data['from_date']."/".$data['to_date']."/".$data['otp_optin']."/".$data['clients'];
				   // CREATE THE WHERE CLAUSE
				   $whr = $date_val." AND otp_optin = '".$data['otp_optin']."' and plan = '".$data['clients']."'";
				   
				}else if($data['otp_optin'] != ""){
				   
				   // PREPARE QUERY STRING
				   $query_string = $perpage."/".$data['btn_search']."/".$data['date_type']."/".$data['from_date']."/".$data['to_date']."/".$data['otp_optin']."/0/";
				   // CREATE THE WHERE CLAUSE
				   $whr = $date_val." AND otp_optin = '".$data['otp_optin']."'";
				   
				}else if($data['clients'] != "" && $data['clients'] != '0'){
				   
				   // PREPARE QUERY STRING
				   $query_string = $perpage."/".$data['btn_search']."/".$data['date_type']."/".$data['from_date']."/".$data['to_date']."/0/".$data['clients']."/";
				   // CREATE THE WHERE CLAUSE
				   $whr = $date_val." and plan = ".$data['clients'];
				   
				}else{
				   // PREPARE QUERY STRING
				   $query_string = $perpage."/".$data['btn_search']."/".$data['date_type']."/".$data['from_date']."/".$data['to_date']."/0/0";
				   
				   // CREATE THE WHERE CLAUSE
				   $whr = $date_val;
				}
				
				// CHECK FOR EXISTING CLAUSE	
				if($where == "") {
					$where = " where ".$whr;
				} else {
					$where .= " and ".$whr;
				}
			}else{
			   $data['from_date'] = $data['to_date']=  date('Y-m-d');
			   $data['date_type'] = 1;
			   $data['btn_search'] = "GO";
			   $date_val = date_dropdown($data['date_type'], $data['from_date'], $data['to_date']);
			   $query_string = $perpage."/".$data['btn_search']."/".$data['date_type']."/".$data['from_date']."/".$data['to_date']."/0/0";
			   $where = " where ". $date_val;
			   //$query_string = $perpage."/";	
			}
	   	  
		
		 
		 //All active otp_optin
		 	   
		 	
		  	
		 $sel_query ="Select 
		                 id, name, phone, email, city, dob, otp, otp_optin, utm_source, utm_campaign, utm_medium, date_created 
				     From 
					     ".$tablename.$where." 
					 ORDER BY  
					     date_created desc";
						 
       
		if($this->uri->segment(11) == "") {
			$config['uri_segment'] 	= 5;
		} else {
			$config['uri_segment'] 	= 11;
		}

	    // MODIFY FULL PATH
		$full_path .= $query_string;
		
		$config['total_rows'] 		= $this->db_function->count_record($sel_query, false);
		$config['per_page'] 		= $perpage;
		$config['base_url'] 		= $full_path;
		$choice 					= $config['total_rows'] / $config["per_page"];
		$config['num_links'] 		= 2;
		$config['full_tag_open'] 	= '<div id="paging" style="float:right; "><ul style="clear:left;">';
		$config['full_tag_close'] 	= '</ul></div>';
		$config['anchor_class'] 	= 'class="btn" ';
		$config['first_tag_open'] 	= '<li class="num_off">';
		$config['first_tag_close'] 	= '</li>';
		$config['last_tag_open'] 	= '<li class="num_off">';
		$config['last_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="num_on">';
		$config['cur_tag_close'] 	= '</li>';
		$config['num_tag_open'] 	= '<li class="num_off">';
    	$config['num_tag_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="num_off">';
    	$config['prev_tag_close'] 	= '</li>';
		$config['next_tag_open'] 	= '<li class="num_off">';
    	$config['next_tag_close'] 	= '</li>';
		$config['prev_link'] 		= 'PREVIOUS';
    	$config['next_link'] 		= 'NEXT';
		$config['use_page_numbers'] = FALSE;
		// PAGINATION PARAMETER VALUES END 				
		
		// PAGINATION PARAMETER INITIALIZE 
		$this->pagination->initialize($config);
		
		// SQL QUERY WITH OFFSET AND PERPAGE LIMIT
		$sql = $sel_query." limit ".$offset.", ".$perpage;

        $data["details"] = $this->db_function->get_data($sql);
		
		// GET THE DATA FROM PAGINATION
		$data["today_count"] = $config['total_rows'];
		
        $data["links"] = $this->pagination->create_links();
		
		$this->load->view(CMS_FOLDER."header");
		$this->load->view(CMS_FOLDER."sidebar", $page_details);
	    $this->load->view(CMS_FOLDER."users-details-policyx-health" ,  $data);
		$this->load->view(CMS_FOLDER.'footer'); 
	
   }
   
   
   
   
}

?>