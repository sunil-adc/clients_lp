<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class lgcl_email_cron  extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->library("excel");
		$this->load->model(array("cache_model", "email_templates"));
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), $this->input->get('bnr'), $this->input->get('country'),  $this->input->get('zoneid'),  $this->input->get('mobile_track'), $this->input->get('ClickID'));
		
	}
   
	public function index(){				
	   
	   echo "Access Denied";
	   die();

    }
   
   
	
	
	public function lgcl_email(){
		
		
		$all_array = all_arrays();
        $array ="";		
		$today_date = date('Y-m-d',strtotime("-1 days"));
		$dir=S3_URL."/lgcl_downloads/";
		
		
		
		if($today_date != ""){
			$sel_query =$this->db->query("Select id, name, phone, email,client_name,utm_source,utm_medium,utm_sub,utm_campaign,
										 date_created From ".REALESTATE_USER." where client_name in (5,6,7) and 
										 DATE_FORMAT(date_created, '%Y-%m-%d') between '".$today_date."' and '".$today_date."' 
										ORDER BY date_created desc ");
		foreach ($sel_query->result() as $val_user){
				$array[]=$val_user;
			}
		}
		//print_r($array);exit;
		$fileName = "lgcl_".$today_date;
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$table_columns = array("Name", "Phone", "Email", "LP Name ","Utm Source","Utm Medium","Utm Sub","Utm Campaign","Date");
	    $column = 0;
		foreach($table_columns as $field)
		{
		   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
		   $column++;
		}
        $excel_row = 2;
        $all_array   = all_arrays();
        
		foreach($sel_query->result() as $row)
		{
		  
		   $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->name);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->phone);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->email);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $all_array['REALESTATE_CLIENTS'][$row->client_name]);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->utm_source);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->utm_medium);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->utm_sub);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->utm_campaign);
		   $date = date("Y-m-d", strtotime($row->date_created));
		   $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $date);
		   $excel_row++;
		}
			$object->getActiveSheet()->setTitle($fileName);
			$objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel5');
            $objWriter->save(getcwd().'/cdn/downloads/'.$fileName . '.xls');
		    
		    $to1 = "sales@lgcl.in";
			$to2 = "gunaseelan@adcanopus.com";
			$to3 = ""; 
			$to4 = "";
			$to5 = "";
			$to6 = "";
					
			$this->email_templates->email_lgcl("sooraj@adcanopus.com", $fileName, $to1, $to2, $to3, $to4, $to5, $to6);
			
	}
	
	
	

}
?>