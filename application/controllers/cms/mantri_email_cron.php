<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mantri_email_cron  extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->library("excel");
		$this->load->model(array("cache_model", "email_templates"));
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), $this->input->get('bnr'), $this->input->get('country'),  $this->input->get('zoneid'),  $this->input->get('mobile_track'), $this->input->get('ClickID'));
		
	}
   
	public function index(){				
	   
	   echo "Access Denied";
	   die();

    }
   
   
	
	
	public function mantri_email(){
		
		
		$all_array = all_arrays();
        $array ="";		
		
		//$today_date = date('Y-m-d');
		
		//$last_date = date('Y-m-d',strtotime("-1 days"));
		$today_date = date('Y-m-d',strtotime("-1 days"));
		

		$dir=S3_URL."/mantri_downloads/";
		/*
		if($dt == 1){
			$default_date =  " date_created > '".$last_date ." 17:00:00' and date_created < '".date('Y-m-d')." 10:00:00'  ";
			$today_date   = date('Y-m-d')."-10";

        }else if ($dt == 2) {
        	$default_date =  " date_created > '".date('Y-m-d')." 10:00:00' and date_created < '".date('Y-m-d')." 12:00:00'  ";
        	$today_date   = date('Y-m-d')."-12";

        }elseif ($dt == 3) {
        	$default_date =  " date_created > '".date('Y-m-d')." 12:00:00' and date_created < '".date('Y-m-d')." 15:00:00'  ";
        	$today_date   = date('Y-m-d')."-15";

        }elseif ($dt == 4) {
        	$default_date =  " date_created > '".date('Y-m-d')." 15:00:00' and date_created < '".date('Y-m-d')." 17:00:00'  ";
        	$today_date   = date('Y-m-d')."-17";
        }*/

		if($today_date != ""){
			$sel_query =$this->db->query("Select 
											  id, name, phone,email,utm_source,utm_sub,utm_medium,utm_campaign, 
											  lp_name, date_created  
										 From 
											 ".MANTRI."
										where
											DATE_FORMAT(date_created, '%Y-%m-%d') between '".$today_date."' and '".$today_date."'  
										ORDER BY  
											 date_created desc ");
			
			
			
			foreach ($sel_query->result() as $val_user){
				$array[]=$val_user;
			}
		}

		$fileName = "mantri_".$today_date;
		$object = new PHPExcel();
		$object->setActiveSheetIndex(0);
		$table_columns = array("Name", "Phone", "Email", "Utm Source","Utm Sub","Utm Medium","Utm campaign","Date");
	    $column = 0;
		foreach($table_columns as $field)
		{
		   $object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
		   $column++;
		}
        $excel_row = 2;
		foreach($sel_query->result() as $row)
		{
		   $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $row->name);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->phone);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->email);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->utm_source);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->utm_sub);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->utm_medium);
		   $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->utm_campaign);
		   $date = date("Y-m-d", strtotime($row->date_created));
		   $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $date);
		   $excel_row++;
		}
			$object->getActiveSheet()->setTitle($fileName);
			$objWriter = PHPExcel_IOFactory::createWriter($object, 'Excel5');
            $objWriter->save(getcwd().'/cdn/mantri_downloads/'.$fileName . '.xls');
		    
			$to1 = "gunaseelan@adcanopus.com";
			$to2 = "chengappa.ks@mantri.in";
			$to3 = "sooraj@adcanopus.com"; 
			$to4 = "atul@adcanopus.com"; 
			$to5 = "";
			$to6 = "";
			$lpname = "Mantri Energia";

			$this->email_templates->email_mantri("preethu@adcanopus.com", $today_date, $lpname, $to1, $to2, $to3, $to4, $to5, $to6);
			
		 
	}
	
	

}
?>