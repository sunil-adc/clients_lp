<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class change_status extends CI_Controller
{
   public $global_country;
   public function __construct(){
        
		parent::__construct();
		$this->load->model(array(CMS_FOLDER.'common_model', CMS_FOLDER.'db_function'));
		$this->db->cache_off();
		$this->global_country =  ($this->session->userdata('cms_country') != "" ) ? $this->session->userdata('cms_country') : '';
		
		
   }


	public function index($tablename = "", $field = "", $value = "", $status_column = "", $divid = "") {
		
		// CHECK REQUIRED FIELED IS NOT BLANK.
		if( trim ($tablename) != "" && trim ($field) != "" &&  
			trim ($value) != "" && trim ($status_column) != "" && trim ($divid) != "") { 
			
			// GET THE VALUE AND CHANGE IT
			// TURN OFF CACHE FOR ONE QUERY
			$this->db->cache_off();
			$get_val = abs($this->db_function->get_single_value($tablename, $status_column, $field."=".$value));
			
			$is_status = (($get_val == 1) ? '0' : '1') ; 
			
			// UPDATE STATUS COLUMN			
			$data = array(
               $status_column 	=> 	$is_status
			   //'verified_by'	=>	($is_verified > 0) ? $this->session->userdata('uid') : 0
            );
			
			
			$this->db->where(array ($field => $value));
			$this->db->update($tablename, $data); 
			
			
			// SEND THE RESPONCE IN CALLBACK
			
			?><a href="javascript:change_verifiy('<?php echo FULL_CMS_URL?>','<?php echo $tablename;?>','<?php echo $field;?>','<?php echo $value;?>','<?php echo $status_column?>','<?php echo $divid?>');"><?php echo ($is_status == '1') ? "<span class='color_red'>active</span>" : "<span class='color_green'>Inactive</span>"; ?></a><?php
			
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			
		} else {
			// ERROR OCCURE SO SEND REFRESH CALLBACK
			echo "refresh";
			
			exit;
			
		}		
		
	}

   
   
}

?>