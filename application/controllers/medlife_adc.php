<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class medlife_adc extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model(array("cache_model", "email_templates", "lead_check"));
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), $this->input->get('bnr'), $this->input->get('country'),  $this->input->get('zoneid'),  $this->input->get('mobile_track'), $this->input->get('ClickID'));
		
	}
   
	public function index(){				
	   
	   $data['city_arr'] = $this->cache_model->get_cache(CITY_CACHE, "id,city,status", CITY, " city ", "");	   
	   
	   $this->load->view("medlife-adc", $data);

    }
   
   
    public function medlifefrm(){
   		
   		date_default_timezone_set('Asia/Calcutta');

		$all_array = all_arrays(); 
		
	    if( is_array($_POST) && count($_POST) > 0 ){
		   foreach ($_POST as $k=>$v){
			   $this->form_validation->set_rules($k, $k , 'trim|required');
		   }
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		   }else{
			    $res = $this->db->query("select id, email from ".MEDLIFE_USER." where email ='".$_POST['email']."'");
			       
			    if($res->num_rows() > 0){
			      
				  echo "done";
				  exit;
			   
			    }else{
				   
				
				$val_otp=mt_rand(1000, 9999);   
				
			    $data = array(
							 'name'         => $_POST['name'],
							 'phone'        => $_POST['phone'],
							 'email'        => $_POST['email'],
							 'plan'         => $_POST['medlife_test'],
							 'otp'          => $val_otp,
							 'utm_medium'   => $_POST['utm_medium'],
							 'utm_campaign' => $_POST['utm_campaign'],
							 'utm_source'   => $_POST['utm_source'],
							 'date_created' => date( 'Y-m-d H:i:s' ) 					   
							);
							
			    $result = $this->db->insert(MEDLIFE_USER, $data);	    
			    $insert_id = $this->db->insert_id($result);
				$this->session->set_userdata('medlife_id', $insert_id);
				$this->session->set_userdata('utm_source', trim($_POST['utm_source']));
				$this->session->set_userdata('utm_medium', trim($_POST['utm_medium']));
				
				
				if($insert_id != "" ){
					
					$n     = $_POST['name'];
					$p     = $_POST['phone'];
					$e     = $_POST['email'];
					$test  = $_POST['medlife_test'];
					
					$add_sub = "";
					$um = $this->session->userdata('utm_source');
					if( isset($um) ){
						if(strtolower($um) == "adcanopusbnglr"){
							$add_sub = "Bangalore ";
						}
					}
					
					
					$to			  = "labs@medlife.com";
					$to1		  = "";
					//$to1		  = "goutam.narayan@medlife.com"; 					
					$from_email   = "support@adcanopus.com";
					$subject      = $add_sub."Medlife Lead - Adcanopus".$insert_id;
					$message	  = '<table width="500px" cellspacing="0" border="1" cellpadding="8" bordercolor="#6C9" style="border:1px solid #6C9;font-family:Verdana, Geneva, sans-serif;font-size:14px">
								<tr style="background: #6C9;color: #FFF;">
								<td colspan="2">Medlife APPOINTMENT </td></tr>
								<tr><td>Name</td><td>'.$n.'</td></tr>
								<tr><td>Phone</td><td>'.$p.'</td></tr>
								<tr><td>Email</td><td>'.$e.' </td></tr>
								<tr><td>Test</td><td>'.$all_array['MEDLIFE_TEST'][$test].' </td></tr>
								</table>';
					
					$this->email_templates->mlkt_mail_send($from_email, $from_name, $to, $subject, $message, $to1); 
					
				}

				
				
				
				if(_sendsms($_POST['phone'],$val_otp." is your verification code.", "MDLIFE")){
					echo "done";
				}else{
					echo 'error';
				}
				
				
			    echo "done";
			}
				 
	      }
      }else{
	   
	   echo "empty";   
      }

      exit;
    }
	
	
	
	public function medlife_otpverification(){
		$all_array = all_arrays(); 
		
	    if( is_array($_POST) && count($_POST) > 0 ){
		   foreach ($_POST as $k=>$v){
			   $this->form_validation->set_rules($k, $k , 'trim|required');
		   }
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		   }else{
			  
			   $res = $this->db->query("select id, name, phone, email, plan, appointment_date, appointment_time  from ".MEDLIFE_USER." where phone ='".$_POST['otp_phone']."' and otp= '".$_POST['otp']."' and email= '".$_POST['otp_email']."' ");
			    if( $res->num_rows() > 0){
					$result1= $res->result_array();
					
			        $data_update = array(
							 'otp_optin'    => 1, 					   
							);
					$this->db->where('id',$result1[0]['id']);
					$result = $this->db->update(MEDLIFE_USER, $data_update);

					$n     = $result1[0]['name'];
					$p     = $result1[0]['phone'];
					$e     = $result1[0]['email'];
					$test  = $result1[0]['plan'];
					$date  = $result1[0]['appointment_date'];
					$time  = $result1[0]['appointment_time'];

					
					/*
					$to			  = "labs@medlife.com";
					$to1		  = "goutam.narayan@medlife.com"; 					
					$from_email   = "support@adcanopus.com";
					$subject      = $add_sub."Medlife Lead - Adcanopus".$insert_id;
					$message	  = '<table width="500px" cellspacing="0" border="1" cellpadding="8" bordercolor="#6C9" style="border:1px solid #6C9;font-family:Verdana, Geneva, sans-serif;font-size:14px">
								<tr style="background: #6C9;color: #FFF;">
								<td colspan="2">Medlife APPOINTMENT </td></tr>
								<tr><td>Name</td><td>'.$n.'</td></tr>
								<tr><td>Phone</td><td>'.$p.'</td></tr>
								<tr><td>Email</td><td>'.$e.' </td></tr>
								<tr><td>Test</td><td>'.$all_array['MEDLIFE_TEST'][$test].' </td></tr>
								</table>';
					
					$this->email_templates->mlkt_mail_send($from_email, $from_name, $to, $subject, $message, $to1); 
					*/
				    echo "done";
				    exit;
			   
			    }else{
					 $res1 = $this->db->query("select id from ".MEDLIFE_USER." where phone ='".$_POST['otp_phone']."' and email= '".$_POST['otp_email']."' ");
					if($res1->num_rows() > 0){
						$result1= $res1->result_array();
					echo "resend";
					echo ','.$result1[0]['id'];
					
					exit;
					}				
				}
			}
		
      }else{
	   
	   echo "empty";   
      }

      exit;
    }
	
    public function medlife_reset(){
		
	    if( is_array($_POST) && count($_POST) > 0 ){
		  if($_POST['id']!=''){
			  $otp_val = mt_rand(1000, 9999);
			  $data_up = array(
							 'otp'          => $otp_val,					   
							);
				$this->db->where('id',$_POST['id']);
			    $result = $this->db->update(MEDLIFE_USER, $data_up);
				
				$data = $this->db->query("select phone from ".MEDLIFE_USER." where id ='".$_POST['id']."' ");
				$res_data= $data->result_array();
				if(_sendsms($res_data[0]['phone'],$otp_val." is your verification code.", "MDLIFE")){
				echo "done";
				}else{
					echo 'error';
				}
				exit;
			   
			}
		  
		}

      exit;
    }
   
    
    public function success(){
		
		$vr = $this->session->userdata('medlife_id');
	    if( isset($vr) &&  $vr > 0 ){
			
			$this->load->view("medlife-success");
		}else{
			redirect(SITE_URL.'medlife');
		}
			
			
		
	}
	
	
	public function bng(){				
	   
	   $this->load->view("medlife-bng");

    }
	
   

}
?>