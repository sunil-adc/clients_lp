<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class volkswagen extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model(array("cache_model", "email_templates", "lead_check"));
		$this->db->cache_off();
		
	}
   
	public function index(){				
	   
	   $this->load->view("wolkswagen");

    }
  
   public function volkswagen_success(){
	  $user_id = $this->session->userdata('user_id');
	 
	    if(isset($user_id) &&  $user_id > 0 ){
			
		 	$this->load->view("wolkswagen-success");
		 }else{
			 	
			redirect(SITE_URL.'wolkswagen');
		}	
	    
   }
   
    public function submit_frm(){
	  
	   if( isset($_POST) && count($_POST) > 0 ){
			$this->form_validation->set_rules('name', 'Name' , 'trim|required');
			$this->form_validation->set_rules('email', 'Email' , 'trim|required');
			$this->form_validation->set_rules('phone', 'Phone No.' , 'trim|required');
		    if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		    }else{
				
				$res = $this->db->query("select id,email from ".WOLKSWAGEN." where email ='".$_POST['email']."'");
			       
			    if($res->num_rows() > 0){
			      
				  echo "already";
				  exit;
			   
			    }else{
					$data = array(
							 'name'          => $_POST['name'],
							 'phone'         => $_POST['phone'],
							 'email'         => $_POST['email'],
							 'dealer'        => $_POST['dealer'],
							 'car'           => $_POST['car'],
							 'utm_source'    => $_POST['utm_source1'],
							 'utm_sub'       => $_POST['utm_source2'],
							 'utm_medium'    => $_POST['utm_source3'],
							 'utm_campaign'  => $_POST['campaign'],
							 'date_created'  => date( 'Y-m-d H:i:s' ) 					   
							);
				$result = $this->db->insert(WOLKSWAGEN, $data);	    
			    $insert_id = $this->db->insert_id($result);
				$this->session->set_userdata('user_id', $insert_id);
				$this->session->set_userdata('utm_source', trim($_POST['utm_source1']));
				echo "done";
					exit;
				}
				
		    }
	   }else{
		   
		   echo "empty";   exit;
       } 
	   
	   
   }

}
?>