<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class jubination extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model("cache_model");
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), $this->input->get('bnr'), $this->input->get('country'),  $this->input->get('zoneid'),  $this->input->get('mobile_track'), $this->input->get('ClickID'));
		
	}
   
	public function index(){				
	   
	   $data['city_arr'] = $this->cache_model->get_cache(CITY_CACHE, "id,city,status", CITY, " city ", "");	   
	   
	   $this->load->view("jubination", $data);

    }
   
   
    public function jubi_firstfrm(){
   		$all_arrays =  all_arrays(); 
	    
		if( is_array($_POST) && count($_POST) > 0 ){
		   foreach ($_POST as $k=>$v){
			   $this->form_validation->set_rules($k, $k , 'trim|required');
		   }
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		   }else{
			    $res = $this->db->query("select id, email from ".USER_DATA." where email ='".$_POST['email']."'");
			       
			    if($res->num_rows() > 0){
			      
				  echo "already";
				  exit;
			   
			    }else{
				   
				  				   
			    $data = array(
							 'name'         => $_POST['name'],
							 'phone'        => $_POST['phone'],
							 'email'        => $_POST['email'],
							 'city'         => $_POST['city'],
							 'test'         => $_POST['pkg'],
							 'utm_source'   => $_POST['ut'],
							 'publisher'    => $_POST['pb'],
							 'clients'      => 3,
							 'date_created' => date( 'Y-m-d H:i:s' ) 					   
							);
							
			    $result = $this->db->insert(USER_DATA, $data);	    
			    $insert_id = $this->db->insert_id($result);
				
				if($insert_id != "" ){
					
					$pk_id = $_POST['pkg'];
					
					$this->session->set_userdata('jb_uid', $insert_id);
					
				//JUBINATION FIRST API
					
					$url ="http://188.166.253.79/save_enquiry";

					$form_data =array(
						   	
						  'form_data[0][campaign_id]'          => $all_arrays['JUBINATION_ID'][$pk_id], 
						   'form_data[0][full_name]'           => $_POST['name'],
						   'form_data[0][email_id]'            => $_POST['email'],
						   'form_data[0][contact_no]'          => $_POST['phone'],
						   'form_data[0][city]'                => $_POST['city'],
						   'form_data[0][ip]'                  => $_SERVER['REMOTE_ADDR'],
						   'form_data[0][source]'              => 'adcan',
						   'form_data[0][pid]'                 => '1221',
						   'form_data[0][step_2]'              => 'no',
						   'form_data[0][step_2_created_at]'   => date('Y-m-d H:i:s'),
						   'form_data[0][step_2_inform_at]'    => date('Y-m-d H:i:s',strtotime('+3 minutes', strtotime(date('Y-m-d H:i:s'))))
								
					);
					
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
					curl_setopt($ch, CURLOPT_URL,$url);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS,$form_data);  //Post Fields
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

					$server_output = curl_exec ($ch);
					$ret =json_decode($server_output);
					$_SESSION['jubi_id'] = $ret->saved_id;//
								
					echo "done";
					exit;
				}
				
			    echo "done";
			}
				 
	      }
      }else{
	   
	   echo "empty";   
      }

      exit;
   }
   
   
   
   public function jubi_secfrm(){
   		
	    if( is_array($_POST) && count($_POST) > 0 ){
		   foreach ($_POST as $k=>$v){
			   $this->form_validation->set_rules($k, $k , 'trim|required');
		   }
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		   }else{
			    $res = $this->db->query("select id, email from ".USER_DATA." where id =".$this->session->userdata('jb_uid'));
			       
			    if($res->num_rows() > 0){
			      
				    $data = array(
								 'appointment_date' => $_POST['j_date'],
								 'time_slot'        => $_POST['j_time'],
								 'address'        	=> $_POST['j_addr'],
								 'pincode'        	=> $_POST['j_pin'],
								);
								
					
					$this->db->where(id, $this->session->userdata('jb_uid')); 
					$result = $this->db->update(USER_DATA, $data);	    
					
					
				//JUBINATION SECOND API
				
					$url_sec ="http://188.166.253.79/save_customer_enquiry/".$_SESSION['jubi_id'];//session id from first request

					$fields =array(
						'form_data[0][id]'                   => $_SESSION['jubi_id'],//required
						'form_data[0][address]'              => $_POST['j_addr'],
						'form_data[0][pin_code]'             => $_POST['j_pin'],
						'form_data[0][appointment_date]'     => $_POST['j_date'],
					);

					$ch_s = curl_init();
					curl_setopt($ch_s, CURLOPT_URL,$url_sec);
					curl_setopt($ch_s, CURLOPT_POST, 1);
					curl_setopt($ch_s, CURLOPT_POSTFIELDS,$fields);  //Post Fields
					curl_setopt($ch_s, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch_s, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
							
					$server_output = curl_exec ($ch_s);
					$ret =json_decode($server_output);
					curl_close ($ch_s);
					
					
					echo "done";
					exit;
				
			    }
				 
	      }
      }else{
	   
	   echo "empty";   
      }

      exit;
   }
   
   
	public function ttt(){
		/*
		$url ="http://188.166.253.79/save_enquiry";

					$form_data =array(

						  'form_data[0][campaign_id]'          => "1",
						   'form_data[0][full_name]'           => "adcan test",
						   'form_data[0][email_id]'            => "adcan@gmail.com",
						   'form_data[0][contact_no]'          => "8880186255",
						   'form_data[0][city]'                => "bangalore",
						   'form_data[0][ip]'                  => "192.168.1.1",
						   'form_data[0][source]'              => 'adcan',
						   'form_data[0][pid]'                 => '1221',
						   'form_data[0][step_2]'              => 'no',
						   'form_data[0][step_2_created_at]'   => date('Y-m-d H:i:s'),
						   'form_data[0][step_2_inform_at]'    => date('Y-m-d H:i:s',strtotime('+3 minutes', strtotime(date('Y-m-d H:i:s'))))
								
					);
					
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
					curl_setopt($ch, CURLOPT_URL,$url);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS,$form_data);  //Post Fields
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

					$server_output = curl_exec ($ch);
					echo $server_output;
					print_r($server_output);
					$ret =json_decode($server_output);
					$_SESSION['jubi_id'] = $ret->saved_id;//
					*/
	   
	}
   

}
?>