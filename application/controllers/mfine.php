<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mfine extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model(array("cache_model", "email_templates", "lead_check"));
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), $this->input->get('bnr'), $this->input->get('country'),  $this->input->get('zoneid'),  $this->input->get('mobile_track'), $this->input->get('ClickID'));
		
	}
   
	public function index(){				
	   	   
	   $this->load->view("mfine");

    }
   
   
    public function mfinefrm(){

   		date_default_timezone_set('Asia/Calcutta');

		$all_array = all_arrays(); 
		
	    if( is_array($_POST) && count($_POST) > 0 ){
		   foreach ($_POST as $k=>$v){
			   $this->form_validation->set_rules($k, $k , 'trim|required'); 
		   }
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";
			
		   }else{
			    $res = $this->db->query("select id, email from ".MFINE_USER." where phone ='".$_POST['phone']."'"); 
			       
			    if($res->num_rows() > 0){
			      
				  echo "done";
				  exit;
			   
			    }else{
				   
				
				$val_otp=mt_rand(1000, 9999);   
				
			    $data = array(
							 'name'         => $_POST['name'],
							 'phone'        => $_POST['phone'],
							 'email'        => $_POST['email'],
							 'plan'         => $_POST['mfine_test'],
							 'otp'          => $val_otp,
							 'utm_medium'   => $_POST['utm_medium'],
							 'utm_campaign' => $_POST['utm_campaign'],
							 'utm_source'   => $_POST['utm_source'],
							 'date_created' => date( 'Y-m-d H:i:s' ),
							);
							
			    $result = $this->db->insert(MFINE_USER, $data);	    
			    $insert_id = $this->db->insert_id($result);
				$this->session->set_userdata('mfine_id', $insert_id);
				$this->session->set_userdata('utm_source', trim($_POST['utm_source']));
				$this->session->set_userdata('utm_medium', trim($_POST['utm_medium']));
				$this->session->set_userdata('phone', trim($_POST['phone']));
				
				if($insert_id != "" ){
					
					$n     = $_POST['name'];
					$p     = $_POST['phone'];
					$e     = $_POST['email'];
					$test  = $_POST['mfine_test'];
					
					$data_val = array("name" => $n, "phone" => $p, "email" => $e, "plan" => $test, "utm_source" => $_POST['utm_source'], "utm_campaign" => $_POST['utm_campaign']);

					$curl = curl_init();

					curl_setopt_array($curl, array(
					  CURLOPT_URL => "https://hooks.zapier.com/hooks/catch/4741001/o3g0zgo/",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_SSL_VERIFYHOST => false,
					  CURLOPT_SSL_VERIFYPEER => false,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "POST",
					  CURLOPT_POSTFIELDS => json_encode($data_val),
					  CURLOPT_HTTPHEADER => array(
					    "cache-control: no-cache",
					    "content-type: application/json",
					    
					  ),
					));

					$response = curl_exec($curl);
					$err = curl_error($curl);

					curl_close($curl);

					if ($err) {
					  //echo "cURL Error #:" . $err;
					} else {
						
					  $json_arr = json_decode($response);

					    if(is_object($json_arr) ){  
						    if($json_arr->status == "success"){
				            	$lead_id = $json_arr->request_id;
				            	$this->general->update_lead(MFINE_USER,$insert_id,$lead_id);
				            }
				        }
					}

					if(_sendsms($_POST['phone'],$val_otp." is your verification code.", "IMFINE")){
						echo "done";
					}else{
						echo 'error';
					}
					
				}else{
					echo "done";	
				}
				
			    
			}
				 
	      }
      }else{
	   
	   echo "empty";   
      }

      exit;
    }


    public function mfine_otpverification(){
		$all_array = all_arrays(); 
		
	    if( is_array($_POST) && count($_POST) > 0 ){
		   foreach ($_POST as $k=>$v){
			   $this->form_validation->set_rules($k, $k , 'trim|required');
		   }
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		   }else{
			  
			   $res = $this->db->query("select id, name, phone, email, plan from ".MFINE_USER." where phone ='".$_POST['otp_phone']."' and otp= '".$_POST['otp']."' and email= '".$_POST['otp_email']."' ");
			    if( $res->num_rows() > 0){
					$result1= $res->result_array();
					
			        $data_update = array(
							 'otp_optin'    => 1, 					   
							);
					$this->db->where('id',$result1[0]['id']);
					$result = $this->db->update(MFINE_USER, $data_update);

					$this->session->set_userdata('mfine_id', $result1[0]['id']);
					
					$n     = $result1[0]['name'];
					$p     = $result1[0]['phone'];
					$e     = $result1[0]['email'];
					$test  = $result1[0]['plan'];
					
					echo "done";
				    exit;
			   
			    }else{
					 $res1 = $this->db->query("select id from ".MFINE_USER." where phone ='".$_POST['otp_phone']."' and email= '".$_POST['otp_email']."' ");
					if($res1->num_rows() > 0){
						$result1= $res1->result_array();
						echo "resend";
						echo ','.$result1[0]['id'];
						
					exit;
					}				
				}
			}
		
      }else{
	   
	  	 echo "empty";   
      }

      exit;
    }
	
    public function mfine_reset(){
		
	    if( is_array($_POST) && count($_POST) > 0 ){
		  if($_POST['id']!=''){
			  $otp_val = mt_rand(1000, 9999);
			  $data_up = array('otp'    => $otp_val, );

				$this->db->where('id',$_POST['id']);
			    $result = $this->db->update(MFINE_USER, $data_up);
				
				$data = $this->db->query("select phone from ".MFINE_USER." where id ='".$_POST['id']."' ");
				$res_data= $data->result_array();
				
				if(_sendsms($res_data[0]['phone'],$otp_val." is your verification code.", "IMFINE")){
					echo "done";
				}else{
					echo 'error';
				}
				exit;
			   
			}
		  
		}

      exit;
    }
  
	
    
    public function success(){
		
		$vr = $this->session->userdata('mfine_id');
	    if( isset($vr) &&  $vr > 0 ){
			
			$this->load->view("mfine-success");
		}else{
			redirect(SITE_URL.'mfine');
		}
			
			
		
	}
	
	
	
	
   

}
?>