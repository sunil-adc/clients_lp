<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class drbatra extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model(array("cache_model", "email_templates"));
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), $this->input->get('bnr'), $this->input->get('country'),  $this->input->get('zoneid'),  $this->input->get('mobile_track'), $this->input->get('ClickID'));
		
	}
   
	public function index(){				
	   
	   $data['city_arr'] = $this->cache_model->get_cache(CITY_CACHE, "id,city,status", CITY, " city ", "");	   
	   
	   $this->load->view("dr-batra", $data);

    }
   
   
    public function drbapical(){
   
	   if( is_array($_POST) && count($_POST) > 0 ){
		   foreach ($_POST as $k=>$v){
			   $this->form_validation->set_rules($k, $k , 'trim|required');
		   }
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		   }else{
			    $res = $this->db->query("select id, email from ".USER_DATA." where email ='".$_POST['email']."'");
			       
			    if($res->num_rows() > 0){
			      
				  echo "already";
			   
			    }else{
				   
				  				   
			    $data = array(
							 'name'         => $_POST['name'],
							 'phone'        => $_POST['phone'],
							 'email'        => $_POST['email'],
							 'city'         => $_POST['city'],
							 'utm_source'   => $_POST['ut'],
							 'publisher'    => $_POST['pb'],
							 'clients'      => 1,
							 'date_created' => date( 'Y-m-d H:i:s' ) 					   
							);
							
			    $result = $this->db->insert(USER_DATA, $data);	    
			    $insert_id = $this->db->insert_id($result);
				
				if($insert_id != "" && $sr != "d_sr"){
					
					$n  = $_POST['name'];
					$p  = $_POST['phone'];
					$e  = $_POST['email'];
					$c  = $_POST['city'];
					$pb = $_POST['pb'];
					$sr = $_POST['source'];
					
					echo "<img src = 'http://www.drbatras.com/campaigns/Lead_Trigger/default.aspx?
					name=".$n."&mobile=".$p."&email=".$e."&Ailment=HAIR&city=".$c."&publisher=".$pb."&Source=".$sr."' width='1px'    height='1px'>";
					
					exit;
				}
				
			    echo "done";
			}
				 
	      }
      }else{
	   
	   echo "empty";   
      }

      exit;
   }
   
  /*
  public function test_mail(){

	  	if($this->email_templates->mlkt_mail_send("info@adcanopus.com", "adcanopus", "atul@adcanopus.com", "Test mail", "test mail", "") ){
	  		echo "sent mail";

	  	}else{
			echo "Not sent";  		
	  	}
  } */

}
?>