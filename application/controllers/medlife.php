<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class medlife extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		date_default_timezone_set('Asia/Calcutta');
		$this->load->helper("general");
		$this->load->model(array("cache_model", "email_templates", "lead_check"));
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('utm_source'), $this->input->get('utm_aff'), $this->input->get('utm_campaign'), $this->input->get('bnr'), $this->input->get('utm_medium'),  $this->input->get('zoneid'),  $this->input->get('mobile_track'), $this->input->get('clkid')); 

		
	}
   
	public function index(){				


	    //$data['city_arr'] = $this->cache_model->get_cache(CITY_CACHE, "id,city,status", CITY, " city ", "");	   

		if(isset($_GET['utm_campaign'])) {

			$_GET['utm_aff'] = strtolower($_GET['utm_campaign']) ; 
		}

	    $data = "";
	    if(isset($_GET['utm_aff']) != "" ){
	   		$aff_arr  = $this->lead_check->_check_utm_aff_form($_GET['utm_source'], $_GET['utm_aff'], "form");

	   		if(is_object($aff_arr) && count($aff_arr)){
	   			$data['otp_verify']	= $aff_arr->opt_verify;
	   			$data['form_type']	= $aff_arr->form_type;
	   			$data['fire_type']  = $aff_arr->fire_type;
	   			$data['a_id']       = $aff_arr->id;
	   			$data['s_id']       = $aff_arr->utm_source;

	   		}else if(isset($_GET['utm_source']) != ""){
	   			$source_arr = $this->lead_check->_check_utm_source_form($_GET['utm_source'], "form");
			
				if(is_object($source_arr) && count($source_arr)){
		   			$data['otp_verify']	= $source_arr->opt_verify;
		   			$data['form_type']	= $source_arr->form_type;
		   			$data['fire_type']  = $source_arr->fire_type;
		   			$data['s_id']       = $source_arr->id;
		   			//$data['a_id']       = 0;
		   			$data['a_id']       = "no";
		   		}else{

			   		$data['otp_verify']	= 2;
			   		$data['form_type']	= 2;
			   		$data['fire_type']  = 0;
			   		$data['s_id']       = 0;
			   		$data['a_id']       = 0;
			   		$data['a_id']       = "no";	 

		   		}	
	   		}

	   }else if(isset($_GET['utm_source']) != ""){
	   		$source_arr = $this->lead_check->_check_utm_source_form($_GET['utm_source'], "form");
			
			if(is_object($source_arr) && count($source_arr)){
	   			$data['otp_verify']	= $source_arr->opt_verify;
	   			$data['form_type']	= $source_arr->form_type;
	   			$data['fire_type']  = $source_arr->fire_type;
	   			$data['s_id']       = $source_arr->id;
	   			//$data['a_id']       = 0;
	   			$data['a_id']       = "no";
	   		}	   		
	   		
	   }else{
	   		$data['otp_verify']	= 2;
	   		$data['form_type']	= 2;
	   		$data['fire_type']  = 0;
	   		$data['s_id']       = 0;
	   		//$data['a_id']       = 0;
	   		$data['a_id']       = "no";
	   }

	
	    $utm_banner = $header_class = "";
	    
	    if(isset($_GET['utm_banner'])){ 
	          if($_GET['utm_banner'] == "bnr4") {
	              $utm_banner = "bnr4";
	              
	          }
	    }

	   if($utm_banner == "bnr4"){
	   	
	   		$this->load->view("medlife/medlife-new", $data);	
	   }else{
	   	    $this->load->view("medlife/medlife", $data);
	   } 
	   

    }
   
   
    public function medlifefrm(){
   		
   		$all_array = all_arrays(); 
		
		$city_arr  = array("delhi" , "bangalore", "mumbai", "kolkatta", "chennai", "Ahmedabad", "hyderabad", "jaipure", "pune", "lucknow", "kochi", "kanpur", "indore", "mysore" );		

	    if( is_array($_POST) && count($_POST) > 0 ){
		    foreach ($_POST as $k=>$v){
			   $this->form_validation->set_rules($k, $k , 'trim|required');
		   }
	   		

		  // if($this->form_validation->run() == FALSE){
			  
			//   echo "validation_error";
			   

		   //}else{
			    $res = $this->db->query("select id, otp, email from ".MEDLIFE_USER." where phone ='".$_POST['phone']."'");
			       
			    if($res->num_rows() > 0){
			    	$data_med_records = $res->result_array();
					
					$val_otp = $data_med_records[0]['otp'];
			      	$this->session->set_userdata('medlife_id', $data_med_records[0]['id']);

				  	if(_sendsms($_POST['phone'],$val_otp." is your verification code.", "MDLIFE")){
						echo "done";
					}else{
						echo 'error';
					}
				 	exit;
			   
			    }else{
				   
				
				$val_otp=mt_rand(1000, 9999);   
				
				$utm_aff = "";
				/*if($this->session->userdata('utm_aff') != ""){
					$utm_aff = $this->session->userdata('utm_aff');
				}*/

				if(strtolower($_POST['city']) == "others") {
					$other_city_val = array_rand($city_arr,1);
					$city_val = "others- ".$city_arr[$other_city_val];
					$c_val    = $city_arr[$other_city_val];
				}else{
					$city_val = $_POST['city'];
					$c_val    = $city_val;
				}

			    $data = array(
							 'name'         => $_POST['name'],
							 'phone'        => $_POST['phone'],
							 'email'        => $_POST['email'],
							 'plan'         => $_POST['medlife_test'],
							 'city'         => $city_val,
							 'otp'          => $val_otp,
							 //'utm_aff'   	=> $_POST['utm_aff'],
							 'utm_aff'   	=> $_POST['utm_aff'],
							 'utm_medium'   => $_POST['utm_medium'],
							 'utm_campaign' => $_POST['utm_campaign'],
							 'utm_source'   => $_POST['utm_source'],
							 'date_created' => date( 'Y-m-d H:i:s' ),
							 
							);

			    if($_POST['form_type'] == 2){
			    	$date  = $_POST['date'];
					$time  = $_POST['time'];

			    	$data['appointment_date'] =  $date;
					$data['appointment_time'] =  $time;

			    }
			    if($_POST['fire_type'] !=0 ){
						if($_POST['a_id'] == "no"){
							$val= $this->lead_check->leads_track($_POST['fire_type'],$_POST['s_id'],0,"medlife");
						}else{
							$val= $this->lead_check->leads_track($_POST['fire_type'],$_POST['s_id'],$_POST['a_id'],"medlife");
						}	
						if($val ){

							$val=1;	
						}else{
							$val=2;		
						}
					
				}else{
					$val=2;
				}
				$data['fire_status'] =  $val;
			
			    $result    = $this->db->insert(MEDLIFE_USER, $data);	    
			    $insert_id = $this->db->insert_id($result);


				$this->session->set_userdata('medlife_id', $insert_id);

				$this->session->set_userdata('utm_source', trim($_POST['utm_source']));
				$this->session->set_userdata('utm_medium', trim($_POST['utm_medium']));
				$this->session->set_userdata('utm_aff',    trim($_POST['utm_aff']));
				$this->session->set_userdata('phone',      trim($_POST['phone']));
				
				
				if($insert_id != "" ){
					
					$n     = str_replace(' ', '', $_POST['name']);
					$p     = $_POST['phone'];
					$c     = $c_val;
					$e     = $_POST['email'];
					$test  = $_POST['medlife_test'];
					$utm_source  = $_POST['utm_source'];
					$utm_campaign= $_POST['utm_campaign'];
					$utm_medium  = $_POST['utm_medium'];
					$utm_aff     = $_POST['utm_aff'];
					$test_name   = str_replace(' ', '', $all_array['MEDLIFE_TEST'][$test]);
					$referer     = SITE_URL."medlife?utm_source=CPL&utm_campaign=".$utm_campaign."&utm_medium=Adcanopus&utm_term=".$utm_medium;

					$add_sub = "";
					$um = $this->session->userdata('utm_source');
					if( isset($um) ){
						if(strtolower($um) == "adcanopusbnglr"){
							$add_sub = "Bangalore ";
						}
					}
					

					
					if($_POST['otp_verify'] == 2 ) {
						
						$form_type = ($_POST['form_type'] == 1 ? "Four field Form" : "Six field Form"); 

						if($_POST['form_type'] == 2){
							$message_addon = '<tr><td>Appointment Date</td><td>'.$date.' </td></tr><tr><td>Appointment Time</td><td>'.$time.' </td></tr>';	
						}else{
							$message_addon = "";
						}

						$to			  = "labs@medlife.com";
						//$to1		  = "goutam.narayan@medlife.com"; 
						$to1		  = "atul@adcanopus.com"; 
	                   	$to1		  = "";

	                   	$from_name    = "Adcanopus support";				
						$from_email   = "support@adcanopus.com";
						$subject      = $add_sub."Medlife Lead ".$form_type ."- Adcanopus".$insert_id;
						
						$message	  = '<table width="500px" cellspacing="0" border="1" cellpadding="8" bordercolor="#6C9" style="border:1px solid #6C9;font-family:Verdana, Geneva, sans-serif;font-size:14px">
									<tr style="background: #6C9;color: #FFF;">
									<td colspan="2">Medlife APPOINTMENT </td></tr>
									<tr><td>Name</td><td>'.$n.'</td></tr>
									<tr><td>Phone</td><td>'.$p.'</td></tr>
									<tr><td>Email</td><td>'.$e.' </td></tr>
									<tr><td>Email</td><td>'.$referer.' </td></tr>
									<tr><td>Test</td><td>'.$all_array['MEDLIFE_TEST'][$test].' </td></tr>
									'.$message_addon.'
									</table>';

						
						//$this->email_templates->mlkt_mail_send($from_email, $from_name, $to, $subject, $message, $to1); 
						

						$curl = curl_init();

							curl_setopt_array($curl, array(
							  CURLOPT_URL => "https://labs.medlife.com/api/search/tests/leads/messages?phone=".$p."&name=".$n."&testName=".$test_name."&email=".$e."&address=ecity&age=&gender=&referer=".urlencode($referer)."&offer_cue=123&utm_source=CPL&utm_campaign=".$utm_campaign."&utm_medium=Adcanopus&utm_term=".$utm_medium."&utm_content=eeee&gclid=aaaaaaa",
							  CURLOPT_RETURNTRANSFER => true,
							  CURLOPT_ENCODING => "",
							  CURLOPT_MAXREDIRS => 10,
							  CURLOPT_TIMEOUT => 30,
							  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							  CURLOPT_CUSTOMREQUEST => "POST",
							  CURLOPT_HTTPHEADER => array(
							    "cache-control: no-cache",
							    "content-type: application/x-www-form-urlencoded"
							  ),
							));

							$response = curl_exec($curl);
							$err = curl_error($curl);

							$response_decode = json_decode($response);

							curl_close($curl);

							if ($err) {
							  //echo "cURL Error #:" . $err;
							} else {
							  if($response_decode->success == 1 ){

									$lead_id = $response_decode->data->leadId;
								}else{
									$lead_id = "";
								}
							}



						
						//SENT TO CLIENT
						
						if($insert_id != 0  && $insert_id != ""){
						
							$sent_data =array(
							 	'form_type'         => $_POST['form_type'],
							 	'sent_status'       => "2",
							 	'lead_id'			=>	$lead_id
							);
		                        
	                        $this->db->where('id', $insert_id);
	                        $this->db->update(MEDLIFE_USER, $sent_data);
	                    }else{


							$sent_data =array(
							 	'form_type'         => $_POST['form_type'],
							 	'sent_status'       => "1",
							);
	                    	$this->db->where('id', $insert_id);
	                        $this->db->update(MEDLIFE_USER, $sent_data);
	                    }

					}else{

						$sent_data =array(
						 	'form_type'         => $_POST['form_type'],
						 	'sent_status'       => "1",
						);
                    	$this->db->where('id', $insert_id);
                        $this->db->update(MEDLIFE_USER, $sent_data);

					}
				}
				
				
				
				
				if(_sendsms($_POST['phone'],$val_otp." is your verification code.", "MDLIFE")){
					echo "done";
				}else{
					echo 'error';
				}
				
				
			    //echo "done";
			}
				 
	      //}
      }else{
	   
	   echo "empty";   
      }

      exit;
    }
	
	
	
	public function medlife_otpverification(){
		$all_array = all_arrays(); 
		$city_arr = array("delhi" , "bangalore", "mumbai", "kolkatta", "chennai", "Ahmedabad", "hyderabad", "jaipure", "pune", "lucknow", "kochi", "kanpur", "indore", "mysore" );
		
	    if( is_array($_POST) && count($_POST) > 0 ){
		   foreach ($_POST as $k=>$v){
			   $this->form_validation->set_rules($k, $k , 'trim|required');
		   }
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";
			
		   }else{
			  
			   $res = $this->db->query("select id, name, phone, email, plan, city, appointment_date, appointment_time, utm_source, utm_campaign, utm_medium, utm_aff from ".MEDLIFE_USER." where phone ='".$_POST['otp_phone']."' and otp= '".$_POST['otp']."' and email= '".$_POST['otp_email']."' ");
			    if( $res->num_rows() > 0){
					$result1= $res->result_array();
					
			        $data_update = array(
							 'otp_optin'    => 1,
							 'email_token'  => $result1[0]['id']."med" 					   
							);
					$this->db->where('id',$result1[0]['id']);
					$result = $this->db->update(MEDLIFE_USER, $data_update);

					$this->session->set_userdata('medlife_id', $result1[0]['id']);
					
					$c = str_replace('others-', ' ', $result1[0]['city']);

					$n     = str_replace(' ', '', $result1[0]['name']);
					$p     = $result1[0]['phone'];
					$e     = $result1[0]['email'];
					//$c     = $result1[0]['city'];
					$test  = $result1[0]['plan'];
					$date  = $result1[0]['appointment_date'];
					$time  = $result1[0]['appointment_time'];
					$utm_source  = $result1[0]['utm_source'];
					$utm_campaign= $result1[0]['utm_campaign'];
					$utm_medium  = $result1[0]['utm_medium'];
					$utm_aff     = $result1[0]['utm_aff'];
					$test_name	 = str_replace(' ', '', $all_array['MEDLIFE_TEST'][$test]);
					$referer     = SITE_URL."medlife?utm_source=CPL&utm_campaign=".$utm_campaign."&utm_medium=Adcanopus&utm_term=".$utm_medium; 	

					
					if($_POST['otp_verify'] == 1){
						
						$form_type = ($_POST['form_type'] == 1 ? "Four field Form" : "Six field Form"); 

						if($_POST['form_type'] == 2){
							$message_addon = '<tr><td>Appointment Date</td><td>'.$date.' </td></tr><tr><td>Appointment Time</td><td>'.$time.' </td></tr>';	
						}else{
							$message_addon = "";
						}

						$to			  = "labs@medlife.com";
						$to1		  = ""; 
						//$to1		  = "atul@adcanopus.com"; 
	                   	
	                   	$from_name    = "Adcanopus support";				
						$from_email   = "support@adcanopus.com";
						$subject      = $add_sub."Medlife Lead ".$form_type." - Adcanopus".$insert_id;
						$message	  = '<table width="500px" cellspacing="0" border="1" cellpadding="8" bordercolor="#6C9" style="border:1px solid #6C9;font-family:Verdana, Geneva, sans-serif;font-size:14px">
									<tr style="background: #6C9;color: #FFF;">
									<td colspan="2">Medlife APPOINTMENT </td></tr>
									<tr><td>Name</td><td>'.$n.'</td></tr>
									<tr><td>Phone</td><td>'.$p.'</td></tr>
									<tr><td>Email</td><td>'.$e.' </td></tr>
									<tr><td>Test</td><td>'.$all_array['MEDLIFE_TEST'][$test].' </td></tr>
									'.$message_addon.'
									</table>';
						
							
						//$this->email_templates->mlkt_mail_send($from_email, $from_name, $to, $subject, $message, $to1); 


						$curl = curl_init();

							curl_setopt_array($curl, array(
							  CURLOPT_URL => "https://labs.medlife.com/api/search/tests/leads/messages?phone=".$p."&name=".$n."&testName=".$test_name."&email=".$e."&address=ecity&age=&gender=&referer=".$referer."&offer_cue=123&utm_source=CPL&utm_campaign=".$utm_campaign."&utm_medium=Adcanopus&utm_term=".$utm_medium."&utm_content=eeee&gclid=aaaaaaa",
							  CURLOPT_RETURNTRANSFER => true,
							  CURLOPT_ENCODING => "",
							  CURLOPT_MAXREDIRS => 10,
							  CURLOPT_TIMEOUT => 30,
							  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							  CURLOPT_CUSTOMREQUEST => "POST",
							  CURLOPT_HTTPHEADER => array(
							    "cache-control: no-cache",
							    "content-type: application/x-www-form-urlencoded"
							  ),
							));

							$response = curl_exec($curl);
							$err = curl_error($curl);

							$response_decode = json_decode($response);

							curl_close($curl);

							if ($err) {
							  //echo "cURL Error #:" . $err;
							} else {
							  if($response_decode->success == 1 ){

									$lead_id = $response_decode->data->leadId;
								}else{
									$lead_id = "";
								}
							}


						

						if($result1[0]['id'] != ""){

							$sent_data = array(
							 	'form_type'         => $_POST['form_type'],
							 	'sent_status'       => 3,
							 	'lead_id'			=> $lead_id
							);
		                        
	                        $this->db->where('id', $result1[0]['id']);
	                        $this->db->update(MEDLIFE_USER, $sent_data);
	                    	
						}else{
							$sent_data = array(
							 	'form_type'         => $_POST['form_type'],
							 	'sent_status'       => 1,
							);
		                        
	                        $this->db->where('id', $result1[0]['id']);
	                        $this->db->update(MEDLIFE_USER, $sent_data);
	                    	
						}
						

					}
					
					//SENDING OPTIN LINK IN EMAIL
					$email_explode = explode('@', $result1[0]['email']);

					$valid_domain  = array('gmail.com','googlemail.com','hotmail.com','live.com','yahoo.com','ymail.com','rediffmail.com','aol.com','outlook.com','msn.com','yahoo.in','yahoo.co.in','mailkoot.com','adcanopus.com','seajintech.com'); 

					if($email_explode[1] != ""){

						if (in_array($email_explode[1], $valid_domain)){

							$this->email_token($result1[0]['id']);
						}
					}

					echo "done";
				    exit;
			   
			    }else{
					 $res1 = $this->db->query("select id from ".MEDLIFE_USER." where phone ='".$_POST['otp_phone']."' and email= '".$_POST['otp_email']."' ");
					if($res1->num_rows() > 0){
						$result1= $res1->result_array();
						echo "resend";
						echo ','.$result1[0]['id'];
						
					exit;
					}				
				}
			}
		
      }else{
	   
	   echo "empty";   
      }

      exit;
    }
	
    public function medlife_reset(){
		
	    if( is_array($_POST) && count($_POST) > 0 ){
		  if($_POST['id']!=''){
			  $otp_val = mt_rand(1000, 9999);
			  $data_up = array(
							 'otp'          => $otp_val,					   
							);
				$this->db->where('id',$_POST['id']);
			    $result = $this->db->update(MEDLIFE_USER, $data_up);
				
				$data = $this->db->query("select phone from ".MEDLIFE_USER." where id ='".$_POST['id']."' ");
				$res_data= $data->result_array();
				if(_sendsms($res_data[0]['phone'],$otp_val." is your verification code.", "MDLIFE")){
				echo "done";
				}else{
					echo 'error';
				}
				exit;
			   
			}
		  
		}

      exit;
    }
   
    
    public function success(){
		
		$vr = $this->session->userdata('medlife_id');
	    if( isset($vr) &&  $vr > 0 ){
			
			$this->load->view("medlife/medlife-success");
		}else{
			redirect(SITE_URL.'medlife');
		}
			
			
		
	}
	
	
	public function bng(){				
	   
	   $this->load->view("medlife-bng");

    }


    public function email_token($u_id){

    	//$u_id = $this->session->userdata('medlife_id');

	    $res = $this->db->query("select id, name, email, email_token from ".MEDLIFE_USER." where id ='".$u_id."'");
	    if( $res->num_rows() > 0){
			$result1= $res->result_array();
					
				$u_name      = $result1[0]['name'];
				$u_email     = $result1[0]['email'];
				$u_token     = $result1[0]['email_token'];

		    	
	    	if($this->email_templates->medlife_registeration_mail($u_email, $u_name, $u_token)) {
	    		echo "send";
	    	}else{
	    		echo "not send ";
	    	}

	    }
    
    }


    public function confirmation($email_token){

    	if ($email_token != "") {
            $user_login = $this->db->query("select id, email_optin  from " . MEDLIFE_USER . " where email_token = '" . $email_token . "'");
            
            if ($user_login->num_rows() > 0) {
                $user_data = $user_login->result();
                
                if ($user_data[0]->email_optin != 1) {
                    
                    $this->db->where("email_token", $email_token);
                    $user_verify = array(
                        'email_optin' => 1,
                    );
                    
                    $this->db->update(MEDLIFE_USER, $user_verify);

                    $this->session->set_userdata('email_optin', '1');

            		$this->success();	        
                }
                
            }
            
        }else{
        	echo "Token Expired";
        }



    }


    public function test_api(){

			$curl = curl_init();

			curl_setopt_array($curl, array(
			  CURLOPT_URL => "https://labs.medlife.com/api/search/tests/leads/messages?phone=8880186251&name=priyanka&testName=CBC&email=priyanka.agasti@medlife.com&address=ecity&age=29&gender=female&referer=abc.com&referer=localhost&offer_cue=123&utm_source=aaaaa&utm_campaign=bbba&utm_medium=cccca&utm_term=dddd&utm_content=eeee&gclid=aaaaaaa",
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "POST",
			  CURLOPT_HTTPHEADER => array(
			    "cache-control: no-cache",
			    "content-type: application/x-www-form-urlencoded",
			    "postman-token: a525335b-bdce-79d4-5f65-c4a61a7a3678"
			  ),
			));

			$response = curl_exec($curl);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
			  echo "cURL Error #:" . $err;
			} else {
			  //echo $response;
			}

			$response_decode = json_decode($response);

			if($response_decode->success == 1 )

				echo $response_decode->data->leadId;

			echo "<pre>";
			print_r($response_decode);


    }


    
}
?> 