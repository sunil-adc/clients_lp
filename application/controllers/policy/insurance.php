<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class insurance extends CI_Controller
{	
    
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general_helper","api_helper");
		$this->load->model(array("cache_model", "email_templates","lead_check"));
		$this->db->cache_off();
		
	}
   
	public function index(){				
	  echo"dfsdg";exit;
	   header("location:javascript://history.go(-1)");

    }

    public function policy_magnifier(){				
	   
	    $this->load->view("policy/policy-magnifier");

    }
   public function maxlife(){				
	   
	    $this->load->view("policy/maxlife");

    }
    public function maxlife_success(){				
	   
	    $this->load->view("policy/maxlife-success");

    }
    public function maxbupa(){				
	   
	    $this->load->view("policy/maxbupa");

    }
    public function maxbupa_success(){				
	   
	    $this->load->view("policy/maxbupa-success");

    }

    public function policy_magnifier_success(){
   		
	  
	    $id = $this->session->userdata('user_id');
        if (isset($id) && $id > 0) {
              $this->load->view("policy/policy-magnifier-success");
        } else {
            redirect(SITE_URL.'policy/insurance/policy_magnifier');
        }
   }

  public function getInsured(){        
     
      $this->load->view("policy/getinsured");

  }
  
   public function getInsured_success(){       
     
      $this->load->view("policy/getinsured-success");

  }
   
   public function submit_frm()
    { 
        if (isset($_POST) && count($_POST) > 0) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('phone', 'Phone No.', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                
                echo "validation_error";
                ;
                
            } else {
                $all_array = all_arrays();
                $key = array_search($_POST['lp'], $all_array['INSURANCE_CLIENTS']);
               
                $lp_name = $all_array['INSURANCE_CLIENTS'][$key];
                $result = $this->general->is_duplicate(INSURANCE_USER ,"id,email","email ='" . $_POST['email'] . "' and client_name=".$key);
                if ($result == true) {
                    echo "done";
                    exit;
                    
                } else {
                    if($lp_name=="policy_magnifier"){
                        $city=$_POST['city'];
                        $dob="";
                        $age="";
                    }elseif($lp_name=="maxlife"){
                        $dob='';
                        $city="";
                        $age=$_POST['dob'];
                    }elseif($lp_name=="maxbupa"){
                        $dob='';
                        $city="";
                        $age="";
                    }elseif($lp_name=="getinsured"){
                        $dob=$_POST['dob'];
                        $city=$_POST['city'];
                        $age="";
                    }else{
                        $dob="";
                        $city="";
                        $age="";
                    }
                    $data      = array(
                        'name' => $_POST['name'],
                        'phone' => $_POST['phone'],
                        'email' => $_POST['email'],
                        'country' => $_POST['country'],
                        'city' => $city,
                        'dob' => $dob,
                        'age' => $age,
                        'client_name'=>$key,
                        'utm_source' => $_POST['utm_source'],
                        'utm_sub' => $_POST['utm_sub'],
                        'utm_medium' => $_POST['utm_medium'],
                        'ip_address' => $_SERVER['REMOTE_ADDR'],
                        'utm_campaign' => $_POST['campaign'],
                        'date_created' => date('Y-m-d H:i:s')
                    );

                    if(isset($_POST['campaign'])){

                      $camp_id = $_POST['campaign'];
                    }else{
                      $camp_id = "adcanopus";
                    }
                    
                    $insert_id = $this->general->add_user(INSURANCE_USER,$data);
                    
                    $date =date("d/m/Y");
                    
                    if($insert_id != "" && $lp_name == "maxlife"){
                        //$api_url = 'https://api.svgcolumbus.com/service/api.php'; // as mentioned on left under HTTP Request Heading
                          $curl = curl_init();
                          curl_setopt_array($curl, array(
                          CURLOPT_URL => "https://api.svgcolumbus.com/service/api.php",
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_SSL_VERIFYPEER =>false,
                          CURLOPT_ENCODING => "",
                          CURLOPT_MAXREDIRS => 10,
                          CURLOPT_TIMEOUT => 30,
                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                          CURLOPT_CUSTOMREQUEST => "POST",
                          //CURLOPT_POSTFIELDS => '{"user":"adcanopus_api", "pass":"gU5HAqhzt2","camp":273,"ref_id":"'.$insert_id.'","name":"'.$_POST['name'].'","email":"'.$_POST['email'].'","mobile_number":"'.$_POST['phone'].'","date_of_birth":"01/01/1988","city":"", "sub_id1": "'.$camp_id.'", ip":"'.$_SERVER['REMOTE_ADDR'].'"}',

                          CURLOPT_POSTFIELDS => '{"user":"adcanopus_api", "pass":"gU5HAqhzt2","camp":273,"ref_id":"'.$insert_id.'","name":"'.$_POST['name'].'","email":"'.$_POST['email'].'","mobile_number":"'.$_POST['phone'].'","date_of_birth":"15/09/1992","city":"NA","ip":"'.$_SERVER['REMOTE_ADDR'].'"}',

                          CURLOPT_HTTPHEADER => array(
                            "cache-control: no-cache",
                            "content-type: application/json",
                          ),
                        ));
                        $response = curl_exec($curl);
                        $err = curl_error($curl);
                        //print_r($response ."-----".$err);exit;
                        curl_close($curl);
                        $result = $response;//'{"Status":true,"Message":"Lead Added Successfully","Data":{"LeadId":6153093}}';
                        $res_array = json_decode($result);
                        $lead_id   = $res_array->Data->LeadId;
                       
                       
                    }elseif($insert_id != "" && ($lp_name == "maxbupa")){

                         $curl = curl_init();
                          curl_setopt_array($curl, array(
                          CURLOPT_URL => "https://api.svgcolumbus.com/service/api.php",
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_SSL_VERIFYPEER =>false,
                          CURLOPT_ENCODING => "",
                          CURLOPT_MAXREDIRS => 10,
                          CURLOPT_TIMEOUT => 30,
                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                          CURLOPT_CUSTOMREQUEST => "POST",
                          CURLOPT_POSTFIELDS => '{"user":"adcanopus_api", "pass":"gU5HAqhzt2","camp":297,"ref_id":"'.$insert_id.'","name":"'.$_POST['name'].'","email":"'.$_POST['email'].'","mobile_number":"'.$_POST['phone'].'","date_of_birth":"","city":"notavailable", "sub_id1": "'.$camp_id.'", "ip":"'.$_SERVER['REMOTE_ADDR'].'"}',
                          CURLOPT_HTTPHEADER => array(
                            "cache-control: no-cache",
                            "content-type: application/json",
                          ),
                        ));  
                        $response = curl_exec($curl);
                        $err = curl_error($curl);
                        //print_r($response ."-----".$err);exit;
                        curl_close($curl);
                        $result = $response;//'{"Status":true,"Message":"Lead Added Successfully","Data":{"LeadId":6153093}}';
                        $res_array = json_decode($result);
                        $lead_id   = $res_array->Data->LeadId;
                        
                    }elseif($insert_id != "" && ($lp_name == "getinsured")){

                         $curl = curl_init();
                          curl_setopt_array($curl, array(
                          CURLOPT_URL => "https://api.svgcolumbus.com/service/maxlife/pushAPI.php",
                          CURLOPT_RETURNTRANSFER => true,
                          CURLOPT_SSL_VERIFYPEER =>false,
                          CURLOPT_ENCODING => "",
                          CURLOPT_MAXREDIRS => 10,
                          CURLOPT_TIMEOUT => 30,
                          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                          CURLOPT_CUSTOMREQUEST => "POST",
                          CURLOPT_POSTFIELDS => '{"user":"gig_maxlife", "pass":"5j5Sq6WxUx", "camp":273, "ref_id":"'.$insert_id.'","name":"'.$_POST['name'].'","email":"'.$_POST['email'].'","mobile_number":"'.$_POST['phone'].'","date_of_birth":"","city":"notavailable", "sub_id1": "'.$camp_id.'", "ip":"'.$_SERVER['REMOTE_ADDR'].'",
                           "utm_source" : "46106_giglife" ,  "utm_medium" : "46106" }',  
                          CURLOPT_HTTPHEADER => array(
                            "cache-control: no-cache",
                            "content-type: application/json",
                          ),
                        ));  
                        $response = curl_exec($curl);
                        $err = curl_error($curl);
                        //print_r($response ."-----".$err);exit;
                        curl_close($curl);
                        $result = $response;//'{"Status":true,"Message":"Lead Added Successfully","Data":{"LeadId":6153093}}';
                        $res_array = json_decode($result);
                        $lead_id   = $res_array->Data->LeadId;
                        
                    }else{
                        $lead_id   ="";
                    }

                    
                    $this->session->set_userdata('user_id', $insert_id);
                    $this->session->set_userdata('utm_source', trim($_POST['utm_source']));
                    $this->session->set_userdata('phone', trim($_POST['phone']));
                    if($lead_id != ""){
                        $res = $this->general->update_lead(INSURANCE_USER,$insert_id,$lead_id);
                    }
                                        
                }
                echo "done";
                exit;
            }
            
            
        } else {
            
            echo "empty";
            exit;
        }
        
        
    }
   
   
  

}
?> 