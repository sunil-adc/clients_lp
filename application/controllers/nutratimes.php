<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class nutratimes extends CI_Controller
{
    
    
    public function __construct()
    {
        
        parent::__construct();
        $this->load->helper("general");
        $this->load->model(array(
            "cache_model",
            "email_templates",
            "lead_check",
            "cms/db_function"
        ));
        $this->db->cache_off();
        $this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), $this->input->get('bnr'), $this->input->get('country'), $this->input->get('zoneid'), $this->input->get('mobile_track'), $this->input->get('ClickID'));
        
    }
    
    public function index()
    {
        
         $this->load->view("nutratimes");
        
    }


    public function airmask()
    {
        
         $this->load->view("nutratimes/mask");
        
    }
    
    public function airmask_success()
    {
        
         $this->load->view("nutratimes/mask-success");
        
    }

    
    
    
    public function success()
    {
        $user_id = $this->session->userdata('user_id');
        
        if (isset($user_id) && $user_id > 0) {
            
            $this->load->view("nutratimes-success");
        } else {
            
            redirect(SITE_URL . 'nutratimes');
        }
        
    }
    
    public function submit_frm()
    {
        
        if (isset($_POST) && count($_POST) > 0) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('phone', 'Phone No.', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                
                echo "validation_error";
                
                
            } else {
                
                $res = $this->db->query("select id, email from tbl_nutratimes_airmask where phone ='" . $_POST['phone'] . "'");
                
                if ($res->num_rows() > 0) {
                    
                    echo "already";
                    exit;
                    
                } else {
                    $data      = array(
                        'name' => $_POST['name'],
                        'phone' => $_POST['phone'],
                        'email' => $_POST['email'],
                        'city'    => $_POST['city'],
                        'product' => $_POST['product'],
                        'utm_source' => $_POST['utm_source1'],
                        'utm_sub' => $_POST['utm_source2'],
                        'utm_medium' => $_POST['utm_source3'],
                        'ip_address' => $_SERVER['REMOTE_ADDR'],
                        'utm_campaign' => $_POST['campaign'],
                        'date_created' => date('Y-m-d H:i:s')
                    );
                    $result    = $this->db->insert('tbl_nutratimes_airmask', $data);
                    $insert_id = $this->db->insert_id($result);
                    $this->session->set_userdata('user_id', $insert_id);
                    $this->session->set_userdata('utm_source', trim($_POST['utm_source1']));
                    
                    echo "done";
                    exit;
                    
                    
                }
                
            }
        } else {
            
            echo "empty";
            exit;
        }
        
        
    }
    
    public function quiz()
    {
        
        //$data['form']='show';
        $this->load->view("nutratimes-quiz");
    }
    
    public function submitfrm()
    {
        
        //print_r($_POST);exit;
        if (isset($_POST) && count($_POST) > 0) {
            $res = $this->db->query("select id, email from " . NUTRATIMES_QUIZ . " where email ='" . $_POST['userEmail'] . "'");
            
            if ($res->num_rows() > 0) {
                
                // $data1['res'] ='already';
                
                //$this->load->view("nutratimes-orderresult",$data1);
                //echo 'already';exit;
                $res_data    = $res->result_array();
                $val_otp     = mt_rand(1000, 9999);
                $data_update = array(
                    'name' => $_POST['userName'],
                    'phone' => $_POST['userPhone'],
                    'email' => $_POST['userEmail'],
                    'age' => $_POST['ageGroup'],
                    'gender' => $_POST['gender'],
                    'height' => $_POST['height'],
                    'weight' => $_POST['weight'],
                    'state' => $_POST['state'],
                    'otp' => $val_otp,
                    'utm_source' => $_POST['utm_source'],
                    'utm_sub' => $_POST['utm_sub'],
                    'utm_medium' => $_POST['utm_medium'],
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'utm_campaign' => $_POST['utm_campaign'],
                    'date_created' => date('Y-m-d H:i:s'),
                    'is_otp_verified' => 0
                );
                $this->db->where('id', $res_data[0]['id']);
                $this->db->update(NUTRATIMES_QUIZ, $data_update);
                $this->session->set_userdata('user_id', $res_data[0]['id']);
                $this->session->set_userdata('phone', $_POST['userPhone']);
                $this->session->set_userdata('utm_source', trim($_POST['utm_source']));
                if(_sendsms($_POST['userPhone'],$val_otp." is your verification code.", "NTIMES")){
                echo "done";
                }else{
                echo 'error';
                }
                //echo "done";
                exit;
                
            } else {
                $val_otp   = mt_rand(1000, 9999);
                $data      = array(
                    'name' => $_POST['userName'],
                    'phone' => $_POST['userPhone'],
                    'email' => $_POST['userEmail'],
                    'age' => $_POST['ageGroup'],
                    'gender' => $_POST['gender'],
                    'height' => $_POST['height'],
                    'weight' => $_POST['weight'],
                    'state' => $_POST['state'],
                    'otp' => $val_otp,
                    'utm_source' => $_POST['utm_source'],
                    'utm_sub' => $_POST['utm_sub'],
                    'utm_medium' => $_POST['utm_medium'],
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'utm_campaign' => $_POST['utm_campaign'],
                    'date_created' => date('Y-m-d H:i:s')
                );
                $result    = $this->db->insert(NUTRATIMES_QUIZ, $data);
                $insert_id = $this->db->insert_id($result);
                $this->session->set_userdata('user_id', $insert_id);
                $this->session->set_userdata('phone', $_POST['userPhone']);
                $this->session->set_userdata('utm_source', trim($_POST['utm_source']));
                if(_sendsms($_POST['userPhone'],$val_otp." is your verification code.", "NTIMES")){
                echo "done";
                }else{
                echo 'error';
                }
                
               // echo "done";
                exit;
            }
            
        } else {
            
            $this->load->view("nutratimes-quiz");
        }
        
    }
    
    
    
    
    public function otp_verify()
    {
        $id = $this->session->userdata('user_id');
        
        $otp = $_POST['otp'];
        //echo "select id,phone from ".NUTRATIMES_OTPLOGS." where otp= '".$otp."' and id= ".$id."";exit;
        if (isset($otp) && $otp != "") {
            
            
            
            $res = $this->db->query("select id,phone from " . NUTRATIMES_QUIZ . " where otp= '" . $otp . "' and id= " . $id);
            if ($res->num_rows() > 0) {
                $result = $res->result_array();
                
                $data_update = array(
                    'is_otp_verified' => 1
                );
                $this->db->where('id', $id);
                $this->db->update(NUTRATIMES_QUIZ, $data_update);
                echo "done";
            } else {
                echo "resend";
            }
            
            
        } else {
            echo "error";
        }
        exit;
        
        
        
    }
    
    
    public function otp_resend()
    {
        
        if ($this->session->userdata('user_id') != "") {
            $otp_val  = mt_rand(1000, 9999);
            $otp_data = array(
                'otp' => $otp_val
            );
            $this->db->where('id', $this->session->userdata('user_id'));
            $result   = $this->db->update(NUTRATIMES_QUIZ, $otp_data);
            $data     = $this->db->query("select phone,otp from " . NUTRATIMES_QUIZ . " where id =" . $this->session->userdata('user_id'));
            $res_data = $data->result_array();
            if(_sendsms($res_data[0]['phone'],$otp_val." is your verification code.", "NTIMES")){
            echo "done";
            }else{
            echo 'error';
            }
            //echo "done";
            
            
        } else {
            echo 'error';
        }
        exit;
    }
    
    public function bmi_calculate()
    {
        if ($this->session->userdata('user_id') != "") {
            $query = $this->db->query("select name,height,weight from " . NUTRATIMES_QUIZ . " where id =" . $this->session->userdata('user_id'));
            if ($query->num_rows > 0) {
                foreach ($query->result() as $key) {
                    $data1['name'] = $key->name;
                    $height        = $key->height;
                    $weight        = $key->weight;
                }
            }
            
            $BMIScore          = $weight / ($height / 100 * $height / 100);
            $val               = round($BMIScore, 2);
            $data1['BMIScore'] = $val;
            $BMIStatus         = "";
            $data1['CalcType'] = "BMI";
            
            if ($BMIScore < 18.5) {
                
                $data1['BMIStatus'] = "Under Weight";
            }
            
            if ($BMIScore >= 18.5 && $BMIScore <= 22) {
                
                $data1['BMIStatus'] = "Normal Weight";
            }
            
            if ($BMIScore >= 23 && $BMIScore <= 25) {
                
                $data1['BMIStatus'] = "Slightly  Weight";
            }
            if ($BMIScore >= 25.0 && $BMIScore <= 30) {
                
                $data1['BMIStatus'] = "Over Weight";
            }
            
            if ($BMIScore > 30.0) {
                
                $data1['BMIStatus'] = "Obese";
            }
            $this->load->view("nutratimes-result", $data1);
         } else {
            redirect(SITE_URL . 'nutratimes/quiz');
        }
    }
    
    
    public function order($id = "")
    {
        //echo $id;exit;
        $ids = "";
        if ($id == 0 || $id > 3) {
            $ids = 1;
        } else {
            $ids = $id;
        }
        $all_array   = all_arrays();
        $product_arr = $all_array['NUTRTATIMES_PRODUCT'][$ids];
        if (!empty($product_arr)) {
            $data['product_detail'] = $all_array['NUTRTATIMES_PRODUCT'][$ids];
        } else {
            $data['product_detail'] = $all_array['NUTRTATIMES_PRODUCT'][1];
        }
        
        $query = $this->db->query("select id,name,email,phone,state from " . NUTRATIMES_QUIZ . " where id =" . $this->session->userdata('user_id'));
        if ($query->num_rows > 0) {
            foreach ($query->result() as $key) {
                $data['user_details'][] = $key;
            }
        }
        //print_r($data);exit;
        $this->load->view("nutratimes-cart", $data);
        //$this->load->view("nutratimes-orderresult");
    }
    
    public function order_result()
    {
        $this->load->view("nutratimes-orderresult");
    }
    
    public function confirm_order()
    {
        $id = $this->session->userdata['user_id'];
        $_POST['net'] = $this->session->set_userdata('utm_source');
        
        if (isset($_POST) && !empty($_POST)) {
            $product_id = "";
            if ($_POST['product_id'] == 1) {
                $product_id = 1;
                $quantity   = "1 Bottle";
            } elseif ($_POST['product_id'] == 2) {
                $product_id = 2;
                $quantity   = "2 Bottle";
            } else {
                $product_id = 3;
                $quantity   = "6 Bottle";
            }
            $data = array(
                'name' => $_POST['name'],
                'phone' => $_POST['phone'],
                'email' => $_POST['email'],
                'address' => $_POST['address'],
                'pincode' => $_POST['pincode'],
                'city' => $_POST['city'],
                'state' => $_POST['state'],
                'product_id' => $product_id,
                'confirm_order' => 1
                
                
            );
            
            
            $this->db->where('id', $id);
            $this->db->update(NUTRATIMES_QUIZ, $data);

            echo "done";
            $_POST['prod'] = $quantity;
            $this->create_nutra_order($_POST);

        } else {
            
            echo "empty";
        }
        exit;
    }
    
    public function loader()
    {
        $this->load->view("nutratimes-result");
    }



    public function create_nutra_order($POST) {
         
        $all_array   = all_arrays();
         
        $this->otherdb = $this->load->database('nutra', TRUE); 

        date_default_timezone_set('Asia/Calcutta');

        /**************************************************************************************/
        /******************CREATE NEW USER PROFILE IF MOBILE NUMBER IS NOT EXIST***************/
        /**************************************************************************************/
        if($this->general->is_duplicate_add('tbl_users', "mobile", $POST['phone'], true, false)) {
          $password   =   get_random_chracter(8, 15, true, true, true);
          $arr_post = $POST;
            // SET THE DATA FOR INSERTION
            $data = array(
                            'passwd'        => md5($password),
                            'name'          => $POST['name'],
                            'mobile'        => $POST['phone'],
                            'email'         => $POST['email'],
                            'address'       => $POST['address'],
                            'city'          => $POST['city'],
                            'state'         => $POST['state'],
                            'pincode'       => $POST['pincode'],
                            'datecreated'   => date('Y-m-d H:i:s')
                        );
            // INSERT QUERY
            $this->db->insert('tbl_users',$data);
            $user_id = $this->otherdb->insert_id();
            
        } else {
            $user_id = $this->db_function->get_single_value('tbl_users', 'user_id', "mobile='".$POST['phone']."'", false, false);
        }
        
        /**************************************************************************************/
        /**********************************CREATE NEW ORDER************************************/
        /**************************************************************************************/
        
        $POST['net'] = ($POST['net'] != "" ? $POST['net'] : "");

        $data = array(
                        'user'      => $user_id,
                        'payment_status' => 1,
                        'qualified_date' => date('Y-m-d'),
                        'name'      => $POST['name'],
                        'mobile'    => $POST['phone'],
                        'email'     => $POST['email'],
                        'caller_status' => '9',
                        'address'   => $POST['address'],
                        'city'      => $POST['city'],
                        //'area'      => $_POST['area'],
                        'state'     => $POST['state'],
                        'pincode'   => $POST['pincode'],
                        'payment_status' => 0,
                        'cms_order' => 1,
                        'net'       => $POST['net'],
                        'dt_c'      => date('Y-m-d H:i:s'),
                        'payment_mode' => 2,
                        'status'    => 1,
                        'comments'  => $POST['prod']
                    );
        // INSERT QUERY
        $this->db->insert('tbl_orders',$data);
        $order_id = $this->otherdb->insert_id();
        
        return true;
        
    }

    public function product()
    {
        
        //$data['form']='show';
        $this->load->view("nutratimesnew");
    }

    public function formsubmit(){
        if (isset($_POST) && count($_POST) > 0) {
            if($_POST['product'] == "product1"){
                $product_id = 3;
                $quantity   = "6 Bottle";
            }elseif($_POST['product'] == "product2"){
                $product_id = 4;
                $quantity   = "4 Bottle";
            }elseif($_POST['product'] == "product3"){
                $product_id = 1;
                $quantity   = "1 Bottle";
            }else{
                $product_id = 1;
                $quantity   = "1 Bottle";
            }
            $res = $this->db->query("select id, email from " . NUTRATIMES_QUIZ . " where email ='" . $_POST['email'] . "'");
            $val_otp     = mt_rand(1000, 9999);
            if ($res->num_rows() > 0) {
                $resdata    = $res->result_array();
                $data_update = array(
                    'name' => $_POST['firstname'],
                    'lastname' => $_POST['lastname'],
                    'pincode' => $_POST['zipcode'],
                    'address' => $_POST['address'],
                    'phone' => $_POST['phone'],
                    'email' => $_POST['email'],
                    'state' => $_POST['state'],
                    'city' => $_POST['city'],
                    'product_id' => $product_id,
                    'otp' => $val_otp,
                    'utm_source' => $_POST['utm_source'],
                    'utm_sub' => $_POST['utm_sub'],
                    'utm_medium' => $_POST['utm_medium'],
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'utm_campaign' => $_POST['utm_campaign'],
                    'date_created' => date('Y-m-d H:i:s'),
                    
                );
                $this->db->where('id', $resdata[0]['id']);
                $this->db->update(NUTRATIMES_QUIZ, $data_update);
                $this->session->set_userdata('user_id', $resdata[0]['id']);
                $this->session->set_userdata('phone', $_POST['phone']);
                $this->session->set_userdata('utm_source', trim($_POST['utm_source']));
                
                $_POST['pincode'] = $_POST['zipcode'];
                $_POST['name'] =  $_POST['firstname'];    
                $_POST['net']  = $_POST['utm_source'];
                $_POST['prod'] = $quantity;
                $this->create_nutra_order($_POST);    

                if(_sendsms($_POST['phone'],$val_otp." is your verification code.", "NTIMES")){
                echo "done";
                }else{
                echo 'error';
                }
                
            }else{
                $data_insert = array(
                    'name' => $_POST['firstname'],
                    'lastname' => $_POST['lastname'],
                    'pincode' => $_POST['zipcode'],
                    'address' => $_POST['address'],
                    'phone' => $_POST['phone'],
                    'email' => $_POST['email'],
                    'state' => $_POST['state'],
                    'city' => $_POST['city'],
                    'product_id' =>$product_id ,
                    'otp' => $val_otp,
                    'utm_source' => $_POST['utm_source'],
                    'utm_sub' => $_POST['utm_sub'],
                    'utm_medium' => $_POST['utm_medium'],
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'utm_campaign' => $_POST['utm_campaign'],
                    'date_created' => date('Y-m-d H:i:s'),
                    
                );
                $result    = $this->db->insert(NUTRATIMES_QUIZ, $data_insert);
                $insert_id = $this->db->insert_id($result);
                $this->session->set_userdata('user_id', $insert_id);
                $this->session->set_userdata('phone', $_POST['phone']);
                $this->session->set_userdata('utm_source', trim($_POST['utm_source']));

                $_POST['pincode'] = $_POST['zipcode'];
                $_POST['name'] =  $_POST['firstname'];    
                $_POST['net']  = $_POST['utm_source'];
                $_POST['prod'] = $quantity;
                $this->create_nutra_order($_POST);   

                if(_sendsms($_POST['phone'],$val_otp." is your verification code.", "NTIMES")){
                echo "done";
                }else{
                echo 'error';
                }
                
            }
            exit;

        }else{
            redirect(SITE_URL."nutratimes/product");
           
        }
    }
    
    public function nutrasuccess(){
       $id=$this->session->userdata('user_id');
       if($id != ""){
            $query = $this->db->query("select id,name,lastname,email,phone,state,city,pincode,address,product_id from " . NUTRATIMES_QUIZ . " where id =" . $this->session->userdata('user_id'));
            if ($query->num_rows > 0) {
                foreach ($query->result() as $key) {
                    $data['user_details'][] = $key;
                    $pid=$key->product_id;
                }
            }
            $all_array   = all_arrays();
            $product_arr = $all_array['NUTRTATIMES_PRODUCT'][$pid];
            if (!empty($product_arr)) {
                $data['product_detail'] = $all_array['NUTRTATIMES_PRODUCT'][$pid];
            } else {
                $data['product_detail'] = $all_array['NUTRTATIMES_PRODUCT'][1];
            }
        
        
            $this->load->view("nutratimesnewsuccess",$data);

       }else{
            redirect(SITE_URL.'nutratimes/product');
       }
       
    }
}
?>