<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class brigade_api extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper(array("general"));
		
		$this->load->model(array("cache_model", "email_templates","lead_check","general"));
		$this->db->cache_off();
		
		
	}
   
	


    public function scre(){
    	
    	 $d=  file_get_contents("php://input");
		 $e =  json_decode($d);

		 if($e != ""){
		 foreach ($e as $k=>$v){
		 	 $data[$k]=$v; 
		 }
		 
		 if(isset($data['phone']) && isset($data['lp_name'])) { 

				 $res = $this->db->query("select id, phone from ".REALESTATE_USER." where phone ='".$data['phone']."' and client_name ='".$data['lp_name']."'");

			    if($res->num_rows() > 0){
					 			         
					 $response_arr['response_code'] = 202;
			         $response_arr['status']        = true;
			         $response_arr['message']       = "DuplicateEntry" ;
				 
			   
			    }else{
			    	 
			    	 $insert_data = array(
									 'name'          => $data['name'],
									 'phone'         => $data['phone'],
									 'email'         => $data['email'],
									 'country'       => $data['country'],
									 'utm_source'    => isset($data['utm_source']) ? $data['utm_source'] : '' ,
									 'utm_medium'    => isset($data['utm_medium']) ? $data['utm_medium'] : '' ,
									 'utm_sub'       => isset($data['utm_sub']) ?  $data['utm_sub'] : '',
									 'utm_campaign'  => isset($data['utm_campaign']) ? $data['utm_campaign'] : '',
									 'client_name'   => $data['lp_name'],
									 'ip_address'    => $_SERVER['REMOTE_ADDR'],
									 'date_created'  => date( 'Y-m-d H:i:s' ) 
									 );		
									
				     $result = $this->db->insert(REALESTATE_USER, $insert_data);
				     $insert_id = $this->db->insert_id($result);
				     $response_arr['response_code'] = 200;
		             $response_arr['status']        = true;
		             $response_arr['message']       = "Inserted successfully";
		             $response_arr['lead_id']       = $insert_id;


		             $curl = curl_init();

					curl_setopt_array($curl, array(
					  CURLOPT_URL => "https://my342370.crm.ondemand.com/sap/bc/srt/scs/sap/querycustomerin1?sap-vhost=my342370.crm.ondemand.com",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_SSL_VERIFYPEER => false,
					  CURLOPT_CUSTOMREQUEST => "POST",
					  CURLOPT_POSTFIELDS => "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:glob=\"http://sap.com/xi/SAPGlobal20/Global\" xmlns:a15=\"http://sap.com/xi/AP/CustomerExtension/BYD/A1531\">\r\n\t\t   <soapenv:Header/>\r\n\t\t   <soapenv:Body>\r\n\t\t      <glob:CustomerByCommunicationDataQuery_sync>\r\n\t\t         <CustomerSelectionByCommunicationData>\r\n\t\t            <SelectionByNormalisedTelephoneNumber>\r\n\t\t               <InclusionExclusionCode>I</InclusionExclusionCode>\r\n\t\t               <IntervalBoundaryTypeCode>1</IntervalBoundaryTypeCode>\r\n\t\t               <LowerBoundaryNormalisedTelephoneNumber>".$data['phone']."</LowerBoundaryNormalisedTelephoneNumber>\r\n\t\t            </SelectionByNormalisedTelephoneNumber>\r\n\t\t            <SelectionByEmailURI>\r\n\t\t               <InclusionExclusionCode>I</InclusionExclusionCode>\r\n\t\t               <IntervalBoundaryTypeCode>1</IntervalBoundaryTypeCode>\r\n\t\t               <LowerBoundaryEmailURI>".$data['email']."</LowerBoundaryEmailURI>\r\n\t\t            </SelectionByEmailURI>\r\n\t\t         </CustomerSelectionByCommunicationData>\r\n\t\t      </glob:CustomerByCommunicationDataQuery_sync>\r\n\t\t   </soapenv:Body>\r\n\t\t</soapenv:Envelope>",
					  
					  CURLOPT_HTTPHEADER => array(
					    "authorization: Basic X0M0Q19CUjpCcmlnYWRlMTIz",
					    "cache-control: no-cache",
					    "content-type: text/xml"
					  ),
					));

					$response = curl_exec($curl);
					$err = curl_error($curl);

					//echo $response;

					$xml = simplexml_load_string($response);
					$xml->registerXPathNamespace('soap-env', 'http://schemas.xmlsoap.org/soap/envelope/');
					$xml->registerXPathNamespace('n', 'http://sap.com/xi/SAPGlobal20/Global');
					$nodes = $xml->xpath('//soap-env:Envelope/soap-env:Body/n:CustomerByCommunicationDataResponse_sync/ProcessingConditions/ReturnedQueryHitsNumberValue');
					$prospect = $xml->xpath('//soap-env:Envelope/soap-env:Body/n:CustomerByCommunicationDataResponse_sync');
					$role = "Customer";
					if($nodes[0] > 0){
						foreach ($prospect[0]->Customer as $customer) {
							
							$p_internal_id = $customer->InternalID;
							$role = $customer->RoleDescription;
							break;
						}
					}
					
					//echo "Step1:CustomerNo:".$nodes[0].'<br>';




					if($nodes[0] == 0 || $role!="Prospect"){
			

						$curl = curl_init();

						curl_setopt_array($curl, array(
						  CURLOPT_URL => "https://my342370.crm.ondemand.com/sap/bc/srt/scs/sap/managecustomerin1",
						  CURLOPT_RETURNTRANSFER => true,
						  CURLOPT_ENCODING => "",
						  CURLOPT_MAXREDIRS => 10,
						  CURLOPT_TIMEOUT => 30,
						  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						  CURLOPT_CUSTOMREQUEST => "POST",
						  CURLOPT_POSTFIELDS => "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:glob=\"http://sap.com/xi/SAPGlobal20/Global\" xmlns:a15=\"http://sap.com/xi/AP/CustomerExtension/BYD/A1531\" xmlns:glob1=\"http://sap.com/xi/AP/Globalization\">\r\n   <soapenv:Header/>\r\n   <soapenv:Body>\r\n      <glob:CustomerBundleMaintainRequest_sync_V1>\r\n         <BasicMessageHeader/>\r\n         <!--1 or more repetitions:-->\r\n         <Customer actionCode=\"01\">\r\n            <!--Optional:-->\r\n            <CategoryCode>2</CategoryCode>\r\n            <!--Optional:-->\r\n            <ProspectIndicator>true</ProspectIndicator>\r\n             <!--Optional:-->\r\n            <LifeCycleStatusCode>2</LifeCycleStatusCode>\r\n            <!--Optional:-->\r\n            <Organisation>\r\n               <!--Optional:-->\r\n               <FirstLineName>?</FirstLineName>\r\n            </Organisation>\r\n            <AddressInformation actionCode=\"01\" addressUsageListCompleteTransmissionIndicator=\"true\">\r\n               <!--Optional:-->\r\n               <Address actionCode=\"01\" telephoneListCompleteTransmissionIndicator=\"true\">\r\n                  <!--Optional:-->\r\n                  <Email>\r\n                     <!--Optional:-->\r\n                     <URI>".$data['email']."</URI>\r\n                  </Email>\r\n                  <!--Optional:-->\r\n                  <PostalAddress>\r\n                     <!--Optional:-->\r\n                     <CountryCode>In</CountryCode>\r\n                     <!--Optional:-->\r\n                     <RegionCode>10</RegionCode>\r\n                     <!--Optional:-->\r\n                     <CityName>city</CityName>\r\n                  </PostalAddress>\r\n                  <Telephone>\r\n                     <FormattedNumberDescription>".$data['phone']."</FormattedNumberDescription>\r\n                     <!--Optional:-->\r\n                     <MobilePhoneNumberIndicator>true</MobilePhoneNumberIndicator>\r\n                  </Telephone>\r\n\r\n               </Address>\r\n            </AddressInformation>\r\n            <!--Optional:-->\r\n            <a15:LastName>".$data['name']."</a15:LastName>\r\n            <!--Optional:-->\r\n            <a15:FirstName>".$data['name']."</a15:FirstName>\r\n         </Customer>\r\n      </glob:CustomerBundleMaintainRequest_sync_V1>\r\n   </soapenv:Body>\r\n</soapenv:Envelope>",

						  CURLOPT_HTTPHEADER => array(
						    "authorization: Basic X0M0Q19CUjpCcmlnYWRlMTIz",
						    "cache-control: no-cache",
						    "content-type: text/xml",
						    "postman-token: 388f244e-4bcd-0538-f7b9-12102b71da47"
						  ),
						));

							$response2 = curl_exec($curl);
							$err = curl_error($curl);

							curl_close($curl);

							$xml2 = simplexml_load_string($response2);
							$xml2->registerXPathNamespace('soap-env', 'http://schemas.xmlsoap.org/soap/envelope/');
							$xml2->registerXPathNamespace('n', 'http://sap.com/xi/SAPGlobal20/Global');
							
							$nodes2 = $xml2->xpath('//soap-env:Envelope/soap-env:Body/n:CustomerBundleMaintainConfirmation_sync_V1');
							$internal_id ="";
							if(isset($nodes2[0]->Customer)){

								//print_r($nodes2[0]->Customer);
								foreach ($nodes2[0]->Customer as $value) {

									$internal_id = $value->InternalID;
									break;	
								}
							}else{
								//echo "No proper inputs";
							}
							

							//echo "Step2:AccountInternalID:".$internal_id;

							if(isset($internal_id) && $internal_id!=""){

								$this->call_step3($internal_id, $data['name'], $data['utm_campaign'], $insert_id);

								
							}else{
								//echo "step2:internal id doesnt exist";
							}
								
					}elseif($role == "Prospect" && isset($p_internal_id)){
						 
						$this->call_step3($p_internal_id, $data['name'], $data['utm_campaign'], $insert_id);
						

					}else{
						//echo "Invalid input or response";
					}



				
				
		    }
		}else{

			 $response_arr['response_code'] = 201;
		     $response_arr['status']        = false;
		     $response_arr['message']       = "No Data Please check2";

		} 
	
	}else{
		 $response_arr['response_code'] = 201;
	     $response_arr['status']        = false;
	     $response_arr['message']       = "No Data Please check1";
	}

	echo json_encode($response_arr);
    exit;
	}
	



    
    public function call_step3($internal_id, $name, $utm_campaign, $insert_id){
	    
	    if($insert_id != ""){
        	$this->general->update_lead(REALESTATE_USER,$insert_id, $internal_id);
    	}

	    $curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://my342370.crm.ondemand.com/sap/bc/srt/scs/sap/managemarketingleadin",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:glob=\"http://sap.com/xi/SAPGlobal20/Global\" xmlns:a15=\"http://sap.com/xi/AP/CustomerExtension/BYD/A1531\" xmlns:y34=\"http://0004487409-one-off.sap.com/Y34E1TR4Y_\">\r\n   <soapenv:Header/>\r\n   <soapenv:Body>\r\n      <glob:MarketingLeadBundleMaintainRequest_sync>\r\n         <BasicMessageHeader/>\r\n         <!--1 or more repetitions:-->\r\n         <MarketingLead actionCode=\"01\" itemListCompleteTransmissionIndicator=\"true\">\r\n            <!--Optional:-->\r\n            <Name>".$name." – Juniper</Name>\r\n            <OriginTypeCode>Z05</OriginTypeCode>\r\n\t    <GroupCode>Z001</GroupCode>\r\n\t\r\n            <!--Optional:-->\r\n            <StatusCode>01</StatusCode>\r\n            <!--Optional:-->\r\n            <CampaignPredecessorReferenceID>7316</CampaignPredecessorReferenceID>\r\n            <!--Optional:-->\r\n            <EmployeeInternalID>12231</EmployeeInternalID>\r\n            <!--Optional:-->\r\n            <SalesEmployeeResponsibleInternalID>12231</SalesEmployeeResponsibleInternalID>\r\n            <!--Optional:-->\r\n            <Note>Test Note</Note>\r\n            <!--Optional:-->\r\n            <UseExistingAccountContactIndicator>true</UseExistingAccountContactIndicator>\r\n            <ProspectParty>\r\n               <!--Optional:-->\r\n               <AccountInternalID>".$internal_id."</AccountInternalID>\r\n            </ProspectParty>\r\n            <a15:WebsiteName>www.brigadeorchards.com/apartments/</a15:WebsiteName>\r\n            <!--Optional:-->\r\n            <a15:UTMCampaign>ORC_DIG_ADC_OND_19_20</a15:UTMCampaign>\r\n\t    <a15:Secondaryleadsource>Digital</a15:Secondaryleadsource>\r\n\r\n            <!--Optional:-->\r\n            <a15:ModeofEnquiry>Z07</a15:ModeofEnquiry>\r\n            <!--Optional:-->\r\n            <a15:TypeofApartment>Z11</a15:TypeofApartment>\r\n\t    <a15:ExpectedSiteVisitDate></a15:ExpectedSiteVisitDate>\r\n            <!--Optional:-->\r\n            <a15:RRWebsite>01</a15:RRWebsite>\r\n            <!--Optional:-->\r\n            <a15:Budget1>Z09</a15:Budget1>\r\n            <!--Optional:-->\r\n            <a15:Location>02</a15:Location>\r\n            <!--Optional:-->\r\n            <y34:ProjctCd1>Juniper</y34:ProjctCd1>\r\n         </MarketingLead>\r\n      </glob:MarketingLeadBundleMaintainRequest_sync>\r\n   </soapenv:Body>\r\n</soapenv:Envelope>",
		  CURLOPT_HTTPHEADER => array(
		    "authorization: Basic X0M0Q19CUjpCcmlnYWRlMTIz",
		    "cache-control: no-cache",
		    "content-type: text/xml",
		    "postman-token: 4d9f7122-9260-02ed-e294-4e266a8dc319"
		  ),
		));

		$response3 = curl_exec($curl);
		$err = curl_error($curl);

		$xml3 = simplexml_load_string($response3);
		$xml3->registerXPathNamespace('soap-env', 'http://schemas.xmlsoap.org/soap/envelope/');
		$xml3->registerXPathNamespace('n', 'http://sap.com/xi/SAPGlobal20/Global');
		$nodes3 = $xml3->xpath('//soap-env:Envelope/soap-env:Body/n:MarketingLeadBundleMaintainConfirmation_sync/MarketingLead/ID');
		if(isset($nodes3)){
			//echo "Step3:MarketingLeadID:".$nodes3[0];
			//return $nodes3[0];
 		}else{
 			//return false;
			//echo "Step3:No MarketingLead for Prospect";
		}
		
   	}
   	}		