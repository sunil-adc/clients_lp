<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sobha_api extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper(array("general"));
		
		$this->load->model(array("cache_model", "email_templates","lead_check","general"));
		$this->db->cache_off();
		
		
	}
   
	


    public function scre(){
    	
    	 $d=  file_get_contents("php://input");
		 $e =  json_decode($d);

		 if($e != ""){
		 foreach ($e as $k=>$v){
		 	 $data[$k]=$v; 
		 }
		 
		 if(isset($data['phone']) && isset($data['lp_name'])) { 

				 $res = $this->db->query("select id, phone from ".SOBHA." where phone ='".$data['phone']."' and lp_name ='".$data['lp_name']."'");

			    if($res->num_rows() > 0){
			         
					 $response_arr['response_code'] = 202;
			         $response_arr['status']        = true;
			         $response_arr['message']       = "DuplicateEntry";
				 
			   
			    }else{
			    	 
			    	 $insert_data = array(
									 'name'          => $data['name'],
									 'phone'         => $data['phone'],
									 'email'         => $data['email'],
									 'country'       => $data['country'],
									 'apartment_type'=> $data['apartment_type'], 
									 'utm_source'    => $data['utm_source'],
									 'utm_sub'       => $data['utm_sub'],
									 'utm_campaign'  => $data['utm_campaign'],
									 'lp_name'       => $data['lp_name'],
									 'ip_address'    => $_SERVER['REMOTE_ADDR'],
									 'date_created'  => date( 'Y-m-d H:i:s' ) 
									 );		
									
				     $result = $this->db->insert(SOBHA, $insert_data);
				     $insert_id = $this->db->insert_id($result);
				     $response_arr['response_code'] = 200;
		             $response_arr['status']        = true;
		             $response_arr['message']       = "Inserted successfully";
		             $response_arr['lead_id']       = $insert_id;
				
				
		    }
		}else{

			 $response_arr['response_code'] = 201;
		     $response_arr['status']        = false;
		     $response_arr['message']       = "No Data Please check2";

		} 
	
	}else{
		 $response_arr['response_code'] = 201;
	     $response_arr['status']        = false;
	     $response_arr['message']       = "No Data Please check1";
	}

	echo json_encode($response_arr);
    exit;
}
	
}
?>