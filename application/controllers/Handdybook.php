<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Handdybook extends CI_Controller {

	//Loading index page
	public function index()
	{
		$this->load->view('handdy_home');
	}
	//Loading instantsavebills page
	public function instantsavebills()
	{	
		$this->load->view('instantsavebills');
	}

	//GET THE IP ADDRESS OF THE USER
	function getVisIpAddr() { 
      
	    if (!empty($_SERVER['HTTP_CLIENT_IP'])) { 
	        return $_SERVER['HTTP_CLIENT_IP']; 
	    } 
	    else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) { 
	        return $_SERVER['HTTP_X_FORWARDED_FOR']; 
	    } 
	    else { 
	        return $_SERVER['REMOTE_ADDR']; 
	    } 
	} 

	//singapore phone number validation 
	function singapore_phone_check($number){
		$pattern = "/[6|8|9]\d{7}|\+65[6|8|9]\d{7}|\+65\s[6|8|9]\d{7}/";

		$match = preg_match($pattern,$number);

		if ($match != false) {
			$this->db->select('phone');
			$this->db->from('tbl_handdybook');
			$this->db->where('phone',$number);
			$res = $this->db->get()->result();
			if($res == ''){
				return TRUE;
			}else{
				$this->form_validation->set_message('singapore_phone_check', 'The %s number already registerd');
            	return FALSE;
			}
		    
		} else {
		    $this->form_validation->set_message('singapore_phone_check', 'The %s number must be a valid Singapore phone number');
            return FALSE;
		}
	} 
	//australia phone number validation 
	function australia_phone_check($number){
		$pattern = "/^(?:\+?61|0)4 ?(?:(?:[01] ?[0-9]|2 ?[0-57-9]|3 ?[1-9]|4 ?[7-9]|5 ?[018]) ?[0-9]|3 ?0 ?[0-5])(?: ?[0-9]){5}$/";

		$match = preg_match($pattern,$number);

		if ($match != false) {
		   	$this->db->select('phone');
			$this->db->from('tbl_handdybook');
			$this->db->where('phone',$number);
			$res = $this->db->get()->result();
			if($res == ''){
				return TRUE;
			}else{
				$this->form_validation->set_message('australia_phone_check', 'The %s number already registerd');
            	return FALSE;
			}
		} else {
		   $this->form_validation->set_message('australia_phone_check', 'The %s number must be a valid Australia phone number');
            return FALSE;
		}
	}
	//UK phone number validation 
    function uk_phone_check($number)
    {
    	$pattern = "/^(\+44\s?7\d{3}|\(?07\d{3}\)?)\s?\d{3}\s?\d{3}$/";

		$match = preg_match($pattern,$number);

		if ($match != false) {
		   	$this->db->select('phone');
			$this->db->from('tbl_handdybook');
			$this->db->where('phone',$number);
			$res = $this->db->get()->result();
			if($res == ''){
				return TRUE;
			}else{
				$this->form_validation->set_message('uk_phone_check', 'The %s number already registerd');
            	return FALSE;
			}
		} else {
		   $this->form_validation->set_message('uk_phone_check', 'The %s number must be a valid UK phone number');
            return FALSE;
		}
    } 

    //GET THE CURRENT DATETIME BASED ON THE CURRENT TIMEZONE
    function get_requester_timezone_details(){
		$ci_instance = & get_instance();

		$ip_address = $ci_instance->input->ip_address();


		//$requester_details = json_decode(file_get_contents('http://ip-api.com/json/'.$ip_address));
		if($_SERVER['SERVER_NAME'] == 'localhost'){
			return "Asia/Kolkata";
		}
		
		$requester_details = json_decode(file_get_contents('http://pro.ip-api.com/json/'.$ip_address.'?key=uYshoG77tqvqlJm'));
		if($requester_details->status == "success"){
			if(isset($requester_details->timezone)){
				return $requester_details->timezone;
			}
		}
		else{
			return "Asia/Kolkata";
		}
	}
	//CALLING THAT DATETIME FUNCTION HERE
	function get_current_datetime_based_on_user_timezone(){
		$usersTimezone = $this->get_requester_timezone_details();
		$date = new DateTime('now', new DateTimeZone($usersTimezone));
		return $today_date = $date->format('Y-m-d');
	}
	function common_phone_check($number){
		$pattern = "^(0/91)?[7-9][0-9]{9}^";
		$match = preg_match($pattern,$number);

		if ($match != false) {
		    $this->db->select('phone');
			$this->db->from('tbl_handdybook');
			$this->db->where('phone',$number);
			$res = $this->db->get()->result();
			if($res == ''){
				return TRUE;
			}else{
				$this->form_validation->set_message('common_phone_check', 'The %s number already registerd');
            	return FALSE;
			}
		} else {
		   $this->form_validation->set_message('common_phone_check', 'The %s must be a valid phone number');
            return FALSE;
		}
	}
	function uk_pincode_check($number){
		$pattern = "/^[A-Z]{1,2}[0-9][A-Z0-9]? ?[0-9][A-Z]{2}$/";
		$match = preg_match($pattern,$number);

		if ($match != false) {
		    return TRUE;
		} else {
		   $this->form_validation->set_message('uk_pincode_check', 'The %s must be a valid pincode');
            return FALSE;
		}
	} 
	function singapore_pincode_check($number){
		$pattern = "/^[1-9][0-9]{5}$/";
		$match = preg_match($pattern,$number);

		if ($match != false) {
		    return TRUE;
		} else {
		   $this->form_validation->set_message('singapore_pincode_check', 'The %s must be a valid pincode');
            return FALSE;
		}
	}
	function australia_pincode_check($number){
		$pattern = "/^(?:(?:[2-8]\d|9[0-7]|0?[28]|0?9(?=09))(?:\d{2}))$/";
		$match = preg_match($pattern,$number);

		if ($match != false) {
		    return TRUE;
		} else {
		   $this->form_validation->set_message('australia_pincode_check', 'The %s must be a valid pincode');
            return FALSE;
		}
	}

	public function check_email_is_unique($email){
		$this->db->select('email');
		$this->db->from('tbl_handdybook');
		$this->db->where('email',$email);
		$res = $this->db->get()->result();
		if($res == ''){
			return TRUE;
		}else{
			$this->form_validation->set_message('check_email_is_unique', 'The %s Id already registerd');
        	return FALSE;
		}
	}

	//SUBMITTING THE JOIN FORM WITH VALIDATION
	public function submit_join_form(){
		date_default_timezone_set('UTC');
		$vis_ip = $this->getVisIPAddr(); 
		$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip)); 
		$country_name = $ipdat->geoplugin_countryName;
		//$country_name = 'singapore';
		$this->form_validation->set_rules('name','Name','trim|required|alpha');
		$this->form_validation->set_rules('email','Email','trim|required|valid_email|callback_check_email_is_unique');
		if(strtolower($country_name) == 'uk'){
			$this->form_validation->set_rules('contact_no','Phone Number','trim|required||callback_uk_phone_check');
			$this->form_validation->set_rules('pincode','Pincode','trim|required|callback_uk_pincode_check');

		}else if(strtolower($country_name) == 'singapore'){
			$this->form_validation->set_rules('contact_no','Phone Number','trim|required|callback_singapore_phone_check');
			$this->form_validation->set_rules('pincode','Pincode','trim|required|callback_singapore_pincode_check');

		}else if(strtolower($country_name) == 'australia'){
			$this->form_validation->set_rules('contact_no','Phone Number','trim|required|callback_australia_phone_check');
			$this->form_validation->set_rules('pincode','Pincode','trim|required|callback_australia_pincode_check');

		}else{
			$this->form_validation->set_rules('contact_no','Phone Number','trim|required|callback_common_phone_check');
			$this->form_validation->set_rules('pincode','Pincode','trim|required|exact_length[6]');

		}
		if($this->form_validation->run() == false){
			if(validate_fields() == FALSE){
				return;
			}	
		}
		if(isset($_POST['utm_source'])){
			$data['utm_source'] = $this->input->post('utm_source');
		}
		if(isset($_POST['utm_medium'])){
			$data['utm_medium'] = $this->input->post('utm_medium');
		}
		if(isset($_POST['utm_sub'])){
			$data['utm_sub'] = $this->input->post('utm_sub');
		}
		if(isset($_POST['utm_campaign'])){
			$data['utm_campaign'] = $this->input->post('utm_campaign');
		}

		$data['name'] = $this->input->post('name');
		$data['email'] = $this->input->post('email');
		$data['phone'] = $this->input->post('contact_no');
		$data['country'] = $country_name;
		$data['pincode'] = $this->input->post('pincode');
		$data['ip_address'] = $vis_ip; //ip address
		$data['date_created'] = $this->get_current_datetime_based_on_user_timezone();
		
		$this->db->insert('tbl_handdybook',$data);
		$last_id = $this->db->insert_id();
		$this->session->set_userdata('user_id', $last_id);
		setResponse(true);
	}

	//LOADING THE QUESTIONS PAGE
	public function questions(){
		$this->load->view("questions");
	}
	//SUBMITTING THE QUESTION FORM WITH VALIDATION
	public function submit_question_form(){
		$id = $this->session->userdata('user_id');
		$this->form_validation->set_rules('value_q1','answer','trim|required|alpha_numeric_spaces');
		$this->form_validation->set_rules('value_q2','answer','trim|required|alpha_numeric_spaces');
		$this->form_validation->set_rules('value_q3','answer','trim|required|alpha_numeric_spaces');
		$this->form_validation->set_rules('value_q4','answer','trim|required|alpha_numeric_spaces');
		$this->form_validation->set_rules('value_q5','answer','trim|required|alpha_numeric_spaces');
		if($this->form_validation->run() == false){
			if(validate_fields() == FALSE){
				return;
			}	
		} 
		$update_condition['id'] = $id;
		$data['question_1'] =  $this->input->post('value_q1');
		$data['question_2'] =  $this->input->post('value_q2');
		$data['question_3'] =  $this->input->post('value_q3');
		$data['question_4'] =  $this->input->post('value_q4');
		$data['question_5'] =  $this->input->post('value_q5');

		if(isset($_POST['utm_source'])){
			$data['utm_source'] = $this->input->post('utm_source');
		}
		if(isset($_POST['utm_medium'])){
			$data['utm_medium'] = $this->input->post('utm_medium');
		}
		if(isset($_POST['utm_sub'])){
			$data['utm_sub'] = $this->input->post('utm_sub');
		}
		if(isset($_POST['utm_campaign'])){
			$data['utm_campaign'] = $this->input->post('utm_campaign');
		}
		$this->db->update('tbl_handdybook',$data,$update_condition);
		return true;
	}
	//LOADING THE THANK YOU PAGE
	public function thank_you(){
		$this->load->view('handdybook-thank-you');
	}
}
