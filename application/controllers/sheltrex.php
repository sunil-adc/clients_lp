<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sheltrex extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model(array("cache_model", "email_templates", "lead_check"));
		$this->db->cache_off();
		
		
	}
   
	public function index(){				
	   
	   $data['city_arr'] = $this->cache_model->get_cache(CITY_CACHE, "id,city,status", CITY, " city ", "");	   
	   
	   $this->load->view("sheltrex", $data);

    }
   
   
    public function sheltrex_frm(){
   		
		$all_array = all_arrays(); 
		
	    if( is_array($_POST) && count($_POST) > 0 ){
		   foreach ($_POST as $k=>$v){
			   $this->form_validation->set_rules($k, $k , 'trim|required');
		   }
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		   }else{
			    $res = $this->db->query("select id, email from ".SHELTERX_USER." where email ='".$_POST['email']."'");
			       
			    if($res->num_rows() > 0){
			      
				  echo "done";
				  exit;
			   
			    }else{
				   
				
				$val_otp=mt_rand(1000, 9999);   
				
			    $data = array(
							 'name'         => $_POST['name'],
							 'phone'        => $_POST['phone'],
							 'email'        => $_POST['email'],
							// 'city'         => $_POST['city'],
							 'otp'          => '',
							 'utm_category' => $_POST['utm_category'],
							 'utm_camp1'    => $_POST['utm_camp1'],
							 'utm_camp2'    => $_POST['utm_camp2'],
							 'utm_source'   => $_POST['utm_leadsource'],
							 'utm_mobile1'  => $_POST['utm_mobile1'],
							 'date_created' => date( 'Y-m-d H:i:s' ) 					   
							);
							
			    $result = $this->db->insert(SHELTERX_USER, $data);	    
			    $insert_id = $this->db->insert_id($result);
				$this->session->set_userdata('user_id', $insert_id);
				$this->session->set_userdata('utm_source', trim($_POST['utm_leadsource']));
				$this->session->set_userdata('utm_medium', trim($_POST['utm_category']));
				$this->session->set_userdata('name', trim($_POST['name']));
				$this->session->set_userdata('email', trim($_POST['email']));
				//$this->session->set_userdata('city', trim($_POST['city']));
				$this->session->set_userdata('phone', trim($_POST['phone']));
				
				if($insert_id != "" ){
					/*
					$accessKey = 'u$r83ae320a3d01c591e923619e80e0f688';
					$secretKey = 'f211c19a51ac84e0947671be4c87083c68987dee';
					$api_url_base = 'https://api.leadsquared.com/v2/LeadManagement.svc';
					$url ='https://api.leadsquared.com/v2/LeadManagement.svc/Lead.Capture?accessKey=u$r83ae320a3d01c591e923619e80e0f688&secretKey=f211c19a51ac84e0947671be4c87083c68987dee&Leadsource='.$_POST['utm_leadsource'].'&Category='.$_POST['utm_category'].'&Camp1='.$_POST['utm_camp1'].'&Camp2='.$_POST['utm_camp2'].'&Mobile1='.$_POST['utm_mobile1'];	

					$FirstName = $_POST['email'];
					$phone = $_POST['phone'];
					$email = $_POST['email'];
					$data_string = '[
						{
							"Attribute": "EmailAddress",
							"Value": "'.$email.'"
						},
						{
							"Attribute": "FirstName",
							"Value": "'.$FirstName.'"
						},
						{
							"Attribute": "Phone",
							"Value": "'.$phone.'"
						}
					]';
					try
					{
						$curl = curl_init($url);
						curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($curl, CURLOPT_HEADER, 0);
						curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
						curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
						curl_setopt($curl, CURLOPT_HTTPHEADER, array(
																	'Content-Type:application/json',
																	'Content-Length:'.strlen($data_string)
																	));
						$json_response = curl_exec($curl);
						curl_close($curl);
						
					} catch (Exception $ex) { 
						curl_close($curl);
					}
					
					
					if(isset($json_response)){
					
					$decode_result=json_decode($json_response,true);	
					$edit_data = array(
							'lead_id'=>	$decode_result['Message']['Id']			   
							);
					$this->db->where("id",$insert_id);
					$this->db->update(SHELTERX_USER,$edit_data);
					
					}*/
			    
				}
			}
			 echo "done";exit;	 
	      }
		  echo "done";exit;	
      }else{
	   
	   echo "empty";  exit; 
      }

      exit;
    }
	
	
	public function success(){
	
		$vr = $this->session->userdata('user_id');
	    if( isset($vr) &&  $vr > 0 ){
			
			$this->load->view("sheltrex-success");
		}else{
			redirect(SITE_URL.'Sheltrex');
		}
	
	}

   

}
?>