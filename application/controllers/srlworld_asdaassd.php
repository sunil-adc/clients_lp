<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class srlworld extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model(array("cache_model", "email_templates", "lead_check"));
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), $this->input->get('bnr'), $this->input->get('country'),  $this->input->get('zoneid'),  $this->input->get('mobile_track'), $this->input->get('ClickID'));
		
	}
   
	public function index(){				
	   
	   $data['city_arr'] = $this->cache_model->get_cache(CITY_CACHE, "id,city,status", CITY, " city ", "");	   
	   
	   $this->load->view("srlworld", $data);

    }
   
   
    public function srlfrm(){
   		$all_array = all_arrays(); 
	    
		if( is_array($_POST) && count($_POST) > 0 ){
		   foreach ($_POST as $k=>$v){
			   $this->form_validation->set_rules($k, $k , 'trim|required');
		   }
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		   }else{
			    $res = $this->db->query("select id, email from ".USER_DATA." where email ='".$_POST['email']."'");
			       
			    if($res->num_rows() > 0){
			      
				  echo "already";
			   
			    }else{
				   
				  				   
			    $data = array(
							 'name'         => $_POST['name'],
							 'phone'        => $_POST['phone'],
							 'email'        => $_POST['email'],
							 'city'         => $_POST['city'],
							 'test'			=> $_POST['s_test'],
							 //'address'		=> $_POST['addr'],
							 //'appointment_date'=>$_POST['s_d'],
							 //'time_slot'	=> $_POST['s_time'],
							 'utm_source'   => $_POST['ut'],
							 'publisher'    => $_POST['pb'],
							 'clients'      => 2,
							 'date_created' => date( 'Y-m-d H:i:s' ) 					   
							);
							
			    $result = $this->db->insert(USER_DATA, $data);	    
			    $insert_id = $this->db->insert_id($result);
				$this->session->set_userdata('srl_id', $insert_id);
				$this->session->set_userdata('utm_source', $_POST['ut']);
				
				if($insert_id != "" ){
					
					$n     = $_POST['name'];
					$p     = $_POST['phone'];
					$e     = $_POST['email'];
					$c     = $_POST['city'];
					//$d     = $_POST['s_d'];
					//$time  = $_POST['s_time'];
					$test  = $_POST['s_test'];
					//$addr  = $_POST['addr'];
					
					
					$to			  = "connect@srl.in";
					$to1		  = "arzoo.mann@srl.in";					
					$from_name    = "Adcanopus"; 
					$from_email   = "support@adcanopus.com";
					$subject      = "Home Collection Lead - Adcanopus";
					/*$message	  = '<table width="500px" cellspacing="0" border="1" cellpadding="8" bordercolor="#6C9" style="border:1px solid #6C9;font-family:Verdana, Geneva, sans-serif;font-size:14px">
								<tr style="background: #6C9;color: #FFF;">
								<td colspan="2">SRL APPOINTMENT </td></tr>
								<tr><td>Name</td><td>'.$n.'</td></tr>
								<tr><td>Phone</td><td>'.$p.'</td></tr>
								<tr><td>Email</td><td>'.$e.' </td></tr>
								<tr><td>City</td><td>'.$c.' </td></tr>
								<tr><td>Date</td><td>'.$d.' </td></tr>
								<tr><td>Utm source</td><td>'.$_POST['ut'].' </td></tr>
								<tr><td>Utm Campaign</td><td>'.$_POST['pb'].' </td></tr>
								<tr><td>Time</td><td>'.$all_array['SRL_TIME'][$time].' </td></tr>
								<tr><td>Test</td><td>'.$_POST['s_test'].' </td></tr>
								</table>';*/
								
					$message	  = '<table width="500px" cellspacing="0" border="1" cellpadding="8" bordercolor="#6C9" style="border:1px solid #6C9;font-family:Verdana, Geneva, sans-serif;font-size:14px">
								<tr style="background: #6C9;color: #FFF;">
								<td colspan="2">SRL APPOINTMENT </td></tr>
								<tr><td>Name</td><td>'.$n.'</td></tr>
								<tr><td>Phone</td><td>'.$p.'</td></tr>
								<tr><td>Email</td><td>'.$e.' </td></tr>
								<tr><td>City</td><td>'.$c.' </td></tr>
								<tr><td>Utm source</td><td>'.$_POST['ut'].' </td></tr>
								<tr><td>Utm Campaign</td><td>'.$_POST['pb'].' </td></tr>
								<tr><td>Test</td><td>'.$_POST['s_test'].' </td></tr>
								</table>';			
					
					$this->email_templates->mlkt_mail_send($from_email, $from_name, $to, $subject, $message, $to1);
					
					echo "done";
					exit;
				}
				
			    echo "done";
			}
				 
	      }
      }else{
	   
	   echo "empty";   
      }

      exit;
   }
   
    
	public function success(){
		
		$vr = $this->session->userdata('srl_id');
	   
		if( isset($vr) &&  $vr > 0 ){
			
			$this->load->view("srlworld-success");
		}else{
			redirect(SITE_URL.'srlworld');
		}	
		
	}

}
?>