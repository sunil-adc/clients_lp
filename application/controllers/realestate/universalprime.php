
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class universalprime extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
	
		$this->db->cache_off();
		$this->load->model(array("cache_model", "email_templates","lead_check"));
         $this->load->helper(array("general_helper","realestate_helper"));
		
	}
	public function index(){	
		
		header("location:javascript://history.go(-1)");
		//$this->load->view("realestate/sterlingascentia/sterlingascentia");
	}
    public function creek_edge_dubai(){    
        
        
        $this->load->view("realestate/universalprime/creek-edge-dubai");
    }
    public function creek_edge_dubai_success(){    
        
        
        $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
            $this->load->view("realestate/universalprime/creek-edge-dubai-success");
        } else {
            redirect(SITE_URL.'realestate/universalprime/creek_edge_dubai');
        }
        
    }
    public function lg_20_upr(){    
        
        
        $this->load->view("realestate/universalprime/lg-20-upr");
    }
    public function lg_20_upr_success(){    
        
        
        $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
            
            $this->load->view("realestate/universalprime/lg-20-upr-success");
       } else {
            redirect(SITE_URL.'realestate/universalprime/lg_20_upr');
        }
        
    }

    public function lg_18_emaar(){    
        
        
        $this->load->view("realestate/universalprime/lg-18-emaar");
    }
    public function lg_18_emaar_success(){    
        
        
        $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
            
            $this->load->view("realestate/universalprime/lg-18-emaar-success");
        } else {
             redirect(SITE_URL.'realestate/universalprime/lg_18_emaar');
        }
        
    }

     public function mohammed_bin_rashid_city(){    
        
        
        $this->load->view("realestate/universalprime/mohammed-bin-rashid-city");
    }
    public function mohammed_bin_rashid_city_success(){    
        
        
        $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
            
            $this->load->view("realestate/universalprime/mohammed-bin-rashid-city-success");
        } else {
             redirect(SITE_URL.'realestate/universalprime/mohammed_bin_rashid_city');
        }
        
    }

    public function wilton_park_ellington(){    
        
        
        $this->load->view("realestate/universalprime/wilton-park-ellington");
    }
    public function wilton_park_ellington_success(){    
        
        
        $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
            
            $this->load->view("realestate/universalprime/wilton-park-ellington-success");
        } else {
             redirect(SITE_URL.'realestate/universalprime/wilton-park_ellington');
        }
        
    }

    // Regina Towers Controller Start

    public function reginatowers(){
        
        $this->load->view("realestate/universalprime/reginatowers");
    }
    public function reginatowers_success(){

       // print_r($this->session->all_userdata());exit;
        $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
             $this->load->view("realestate/universalprime/reginatowers-success");
        } else {
            redirect(SITE_URL.'realestate/universalprime/reginatowers');
        }
        
       
    }
    
    public function frm_submit()
    {
       
        if (isset($_POST) && count($_POST) > 0) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('phone', 'Phone No.', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                
                echo "validation_error";
                ;
                
            } else {
                $all_array = all_arrays();
                $key = array_search($_POST['lp'], $all_array['UNIVERSALPRIME_CLIENTS']);
               
                $lp_name = $all_array['UNIVERSALPRIME_CLIENTS'][$key];
                $result = $this->general->is_duplicate(UNIVERSALPRIME_USER ,"id,email","email ='" . $_POST['email'] . "' and project_name = ".$key);
                if ($result == true) {
                    
                    echo "done";
                    exit;
                    
                } else {
                    $data      = array(
                        'name' => $_POST['name'],
                        'phone' => $_POST['phone'],
                        'email' => $_POST['email'],
                        'country' => $_POST['country'],
                        'project_name'=>$key,
                        'utm_source' => $_POST['utm_source1'],
                        'utm_sub' => $_POST['utm_source2'],
                        'utm_medium' => $_POST['utm_source3'],
                        'ip_address' => $_SERVER['REMOTE_ADDR'],
                        'utm_campaign' => $_POST['campaign'],
                        'date_created' => date('Y-m-d H:i:s')
                    );

                    $insert_id = $this->general->add_user(UNIVERSALPRIME_USER,$data);
                    $this->session->set_userdata('user_id', $insert_id);
                    $this->session->set_userdata('utm_source', trim($_POST['utm_source1']));
                    $this->session->set_userdata('phone', trim($_POST['phone']));
                    $this->session->set_userdata('email', trim($_POST['email']));
                    $this->session->set_userdata('name', trim($_POST['name']));
                    $this->session->set_userdata('utm_source', trim($_POST['utm_source1']));
                    $this->session->set_userdata('utm_campaign', trim($_POST['campaign']));
                    
                }
                echo "done";
                exit;
            }
            
            
        } else {
            
            echo "empty";
            exit;
        }
        
        
    }
    

    
	
}
?>