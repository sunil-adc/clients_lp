<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mantricourtyard extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model(array("cache_model", "email_templates","lead_check"));
		$this->db->cache_off();
	}
   
	public function index(){				
	   
	   $this->load->view("realestate/mantricourtyard");

    }
   
   public function mantricourtyard_success(){
	   $user_id = $this->session->userdata('user_id');
	    if( isset($user_id) &&  $user_id > 0 ){
			
			$this->load->view("realestate/mantricourtyard-success");
		}else{
			redirect(SITE_URL.'realestate/mantricourtyard');
		}	
	   
   }
   
   public function submit_frm(){
	   if( isset($_POST) && count($_POST) > 0 ){
			$this->form_validation->set_rules('name', 'Name' , 'trim|required');
			$this->form_validation->set_rules('email', 'Email' , 'trim|required');
			$this->form_validation->set_rules('phone', 'Phone No.' , 'trim|required');
		    if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		    }else{
				
				$res = $this->db->query("select id, email from ".MANTRI." where email ='".$_POST['email']."'");
			       
			    if($res->num_rows() > 0){
			      
				  echo "done";
				  exit;
			   
			    }else{
					$data = array(
							 'name'          => $_POST['name'],
							 'phone'         => $_POST['phone'],
							 'email'         => $_POST['email'],
							 'country'       => $_POST['country'],
							 'utm_source'    => $_POST['utm_source1'],
							 'utm_sub'       => $_POST['utm_source2'],
							 'utm_medium'        => $_POST['utm_source3'],
							// 'lp_name'       => $_POST['lp_name'],
							 'ip_address'    => $_SERVER['REMOTE_ADDR'],
							 'utm_campaign'  => $_POST['campaign'],
							 'date_created' => date( 'Y-m-d H:i:s' ) 					   
							);
				$result = $this->db->insert(MANTRI, $data);	    
			    $insert_id = $this->db->insert_id($result);
			    $this->session->set_userdata('phone', trim($_POST['phone']));
			    
			    if($insert_id != "" ){

					$curl = curl_init();

					  curl_setopt_array($curl, array(
					  CURLOPT_URL => "https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8",
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 30,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "POST",
					  CURLOPT_POSTFIELDS => "oid=00D280000013QIk&retURL=google.com&last_name=".$_POST['name']."&email=".$_POST['email']."&mobile=".$_POST['phone']."&00N28000006DTZm=Mantri%20Courtyard%20Parcel-4&description=interested%20in%20property&lead_source=Digital&Campaign_ID=7010I000000XgWn&00N28000006DTKu=web&00N28000006DTx4=http%3A%2F%2Famazepromos.com%2Frealestate%2Fmantricourtyard&00N2800000IYsy1=".$_POST['utm_source1']."&00N2800000FWXaD=Adcanopus",
					  CURLOPT_HTTPHEADER => array(
					    "cache-control: no-cache",
					    "content-type: application/x-www-form-urlencoded",
					  ),
					));

					$response = curl_exec($curl);
					$err = curl_error($curl);

					curl_close($curl);

				}
				$this->session->set_userdata('user_id', $insert_id);
				$this->session->set_userdata('utm_source', trim($_POST['utm_source1']));
				echo "done";
					exit;
				}
				
		    }
	   }else{
		   
		   echo "empty";   exit;
       } 
	   
	   
   }

}
?>