<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class prestige extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
	
		$this->db->cache_off();
		$this->load->model(array("cache_model", "email_templates","lead_check"));
		
	}
	public function index(){	
		redirect("https://www.prestigegroup.com");
    }
   
	
	public function lake_ridge(){
		
		$this->load->view("realestate/prestige/lake-ridge");
	}
	public function lake_ridge_success(){
		
		$this->load->view("realestate/prestige/lake-ridge-success");
	}

	public function misty_waters(){
		
		$this->load->view("realestate/prestige/misty-waters");
	}
	public function misty_waters_success(){
		
		$this->load->view("realestate/prestige/misty-waters-success");
	}
	
	public function frm_submit()
    {
       
        if (isset($_POST) && count($_POST) > 0) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('phone', 'Phone No.', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                
                echo "validation_error";
                ;
                
            } else {
                
                $res = $this->db->query("select id,email from " . PRESTIGE . " where email ='" . $_POST['email'] . "' and lp_name = '".$_POST['lp']."'");

                if ($res->num_rows() > 0) {
                    
                    echo "done";
                    exit;
                    
                } else {
                    $data      = array(
                        'name' => $_POST['name'],
                        'phone' => $_POST['phone'],
                        'email' => $_POST['email'],
                        'country' => $_POST['country'],
                        'lp_name' => $_POST['lp'],
                        'utm_source' => $_POST['utm_source1'],
                        'utm_sub' => $_POST['utm_source2'],
                        'utm_medium' => $_POST['utm_source3'],
                        'ip_address' => $_SERVER['REMOTE_ADDR'],
                        'utm_campaign' => $_POST['campaign'],
                        'date_created' => date('Y-m-d H:i:s')
                    );
                    $result    = $this->db->insert(PRESTIGE, $data);
                    $insert_id = $this->db->insert_id($result);
                    $this->session->set_userdata('user_id', $insert_id);
                    $this->session->set_userdata('utm_source', trim($_POST['utm_source1']));
                    $this->session->set_userdata('phone', trim($_POST['phone']));

                if($insert_id != ""){

                    $api_url = 'https://lead.anarock.com/api/v0/adcanopus/sync-lead'; // as mentioned on left under HTTP Request Heading

                    $key = '711032e021af85fb'; 
                    $current_time = time();
                    $message = (string)$current_time;

                    // to lowercase hexits
                    $hash = hash_hmac('sha256', $message, $key);

                
                    $country_code = "IN"; //strtoupper($_POST['country_name']); // GB
                    
                    $phone = $_POST['phone']; // 7123456789
                    
                    $source_id = $_POST['utm_source1']; //Affiliate source id
                    
                    $sub_source_id = $_POST['campaign'] ;

                    $campaign_id   = $_POST['lp']; // String to Attribute the lead to specific project. contact ANAROCK team for this.
                     
                    $postFields  = "";
                    $postFields .= "&email=".$_POST['email']."&name=".$_POST['name']; // refer to query parameters section on left section
                    $postFields .= "&current_time=".$current_time;
                    $postFields .= "&purpose=buy";
                    $postFields .= "&source=".$source_id;
                    $postFields .= "&sub_source=".$sub_source_id;
                    
                    $postFields .= "&phone=".$phone; // Phone number without country code
                    $postFields .= "&country_code=".$country_code; // Standard ISO3166-1 alpha-2 code for a country.
                    $postFields .= "&hash=".$hash; // its mandatory to create hash using same timestamp as sent in current_time parameter
                    $postFields .= "&campaign_id=".$campaign_id;
                  
                    $ch = curl_init();

                    curl_setopt($ch, CURLOPT_URL,$api_url);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS,
                                $postFields);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                    $server_output = curl_exec ($ch);
                    if(curl_errno($ch)){
                        echo 'Request Error:' . curl_error($ch);
                     }
                     print_r($server_output);
                     exit;

                }
                    
                }
                echo "done";
                exit;
            }
            
            
        } else {
            
            echo "empty";
            exit;
        }
        
        
    }
}
?>