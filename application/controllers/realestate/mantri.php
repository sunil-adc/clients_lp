<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mantri extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model("lead_check");
		$this->db->cache_off();
	}
   
	public function index(){				
	   //header("location:javascript://history.go(-1)");
	   $this->load->view("realestate/mantri/mantri");
    }
    //mantri_success
    public function mantri_success(){
	   //$user_id = $this->session->userdata('user_id');
	    //if( isset($user_id) &&  $user_id > 0 ){
			
			$this->load->view("realestate/mantri/mantri-success");
		//}else{
			//redirect(SITE_URL.'realestate/mantri/mantri');
		//}	
	   
   }
   public function mantricourtyard()
   {
   		 $this->load->view("realestate/mantri/mantricourtyard");

   }
   public function mantricourtyard_success(){
	   $user_id = $this->session->userdata('user_id');
	    if( isset($user_id) &&  $user_id > 0 ){
			
			$this->load->view("realestate/mantri/mantricourtyard-success");
		}else{
			redirect(SITE_URL.'realestate/mantri/mantricourtyard');
		}	
	   
   }
  

	
   public function submit_frm(){
	   if( isset($_POST) && count($_POST) > 0 ){
			$this->form_validation->set_rules('name', 'Name' , 'trim|required');
			$this->form_validation->set_rules('email', 'Email' , 'trim|required');
			$this->form_validation->set_rules('phone', 'Phone No.' , 'trim|required');
		    if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		    }else{
				
				$res = $this->db->query("select id, email from ".MANTRI." where email ='".$_POST['email']."' and lp_name ='".$_POST['lp_name']."'");
			       
			    if($res->num_rows() > 0){
			      
				  echo "done";
				  exit;
			   
			    }else{
			    	$country=isset($_POST['country'])?$_POST['country']:"91";
					$data = array(
							 'name'          => $_POST['name'],
							 'phone'         => $_POST['phone'],
							 'email'         => $_POST['email'],
							 'country'       => $country,
							 'utm_source'    => $_POST['utm_source1'],
							 'utm_sub'       => $_POST['utm_source3'],
							 'utm_medium'    => $_POST['utm_source2'],
							 'lp_name'       => $_POST['lp_name'],
							 'ip_address'    => $_SERVER['REMOTE_ADDR'],
							 'utm_campaign'  => $_POST['campaign'],
							 'date_created'  => date( 'Y-m-d H:i:s' ) 					   
							);
				$result = $this->db->insert(MANTRI, $data);	    
			    $insert_id = $this->db->insert_id($result);


					if($insert_id != "" ){

						$lead_arr = array(
									    "Name"  => $_POST['name'],
									    "EMail" => $_POST['email'],
									    "Phone" => $_POST['phone'],
									    "Country" => $country,
									    "City" => "",
									    "Referral" => "Adcanopus",
									    "LandingPage" => "http://www.amazepromos.com/realestate/mantri",
									    "IP" => $_SERVER['REMOTE_ADDR'],
									    "CreatedTime" => date( 'Y-m-d H:i:s' ),
									    "Project" => $_POST['lp_name'],
									    "ChatTranscript" => "NA",
									    "Title" => "NA",
									    "Utm_Source" => $_POST['utm_source1'],
									    "Utm_Medium" => $_POST['utm_source2'],
									    "Utm_Content" => $_POST['utm_source3'],
									    "Utm_Term" => "",
									    "Utm_Campaign" => $_POST['campaign'],
									    "CallData" => "NA",
									    "VisitorCallStatus" => "NA",
									    "SalesPersonStatus" => "NA",
									    "BHK" => "NA",
									    "Budget" => "NA",
									    "Remarks1" => "Buy",
									    "Remarks2" => "NA",
									    "CallConnectStatus" => "NA",
									    "InComingSource" => "Adcanopus-Campaign",
									    "CampaignType" => "",
									    "CampaignName" => ""
									
									);

						$lead_json = json_encode($lead_arr);
						
						$curl = curl_init();

						curl_setopt_array($curl, array(
						  CURLOPT_PORT => "8101",
						  CURLOPT_URL => "https://quadraleads.in:8101/api/qleads",
						  CURLOPT_RETURNTRANSFER => true,
						  CURLOPT_MAXREDIRS => 10,
						  CURLOPT_TIMEOUT => 30,
						  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						  CURLOPT_CUSTOMREQUEST => "POST",
						  CURLOPT_POSTFIELDS => $lead_json,
						  CURLOPT_SSL_VERIFYHOST => false,
					      CURLOPT_SSL_VERIFYPEER => false,
						  CURLOPT_HTTPHEADER => array(
						    "Cache-Control: no-cache",
						    "Content-Type: application/json",
						    
						  ),
						));

						$response = curl_exec($curl);
						$err = curl_error($curl);

						curl_close($curl);

						if ($err) {
						  //echo $err;
						} else {
						  //echo $response;
						}
  

					}
					
					$this->session->set_userdata('user_id', $insert_id);
	        		$this->session->set_userdata('phone', trim($_POST['phone']));
					$this->session->set_userdata('utm_source', trim($_POST['utm_source1']));
					
					echo "done";
					exit;
				}
				
		    }
	   }else{
		   
		   echo "empty";   exit;
       } 
	   
	   
   }

}
?>