<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class smartowner extends CI_Controller
{
    public $country = "";
    public function __construct()
    {
        
        parent::__construct();
        
        $this->db->cache_off();
        $this->load->model(array(
            "lead_check"
        ));
        
    }
    public function index()
    {
        $this->load->view("realestate/smartowner/smartowner");
        
    }
    
    
    public function smartowner_success()
    {
        
        $this->load->view("realestate/smartowner/smartowner-success");
    }
    
    public function submit_frm()
    {
        
        if (isset($_POST) && count($_POST) > 0) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('phone', 'Phone No.', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                
                echo "validation_error";
                ;
                
            } else {
                
                $res = $this->db->query("select id,email from " . SMARTOWNER . " where email ='" . $_POST['email'] . "'");
                
                if ($res->num_rows() > 0) {
                    
                    echo "done";
                    exit;
                    
                } else {
                    $data      = array(
                        'name' => $_POST['name'],
                        'phone' => $_POST['phone'],
                        'email' => $_POST['email'],
                        'country' => $_POST['country'],
                        'utm_source' => $_POST['utm_source1'],
                        'utm_sub' => $_POST['utm_source2'],
                        'utm_medium' => $_POST['utm_source3'],
                        'ip_address' => $_SERVER['REMOTE_ADDR'],
                        'utm_campaign' => $_POST['campaign'],
                        'date_created' => date('Y-m-d H:i:s')
                    );
                    $result    = $this->db->insert(SMARTOWNER, $data);
                    $insert_id = $this->db->insert_id($result);
                    $this->session->set_userdata('user_id', $insert_id);
                    $this->session->set_userdata('utm_source', trim($_POST['utm_source1']));
                    $this->session->set_userdata('phone', trim($_POST['phone']));
                    
                }
                echo "done";
                exit;
            }
            
            
        } else {
            
            echo "empty";
            exit;
        }
        
        
    }
}