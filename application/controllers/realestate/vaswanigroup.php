<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class vaswanigroup extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper(array("general"));
		
		$this->load->model(array("cache_model", "email_templates","lead_check"));
		$this->db->cache_off();
		
		
	}
   
	public function index(){				
	   
	   $data['city_arr'] = $this->cache_model->get_cache(CITY_CACHE, "id,city,status", CITY, " city ", "");	   
	   
	  // $this->load->view("sheltrex", $data);

    }
	public function walnut_creek(){
		$data['city_arr'] = $this->cache_model->get_cache(CITY_CACHE, "id,city,status", CITY, " city ", "");
		$this->load->view("realestate/vaswanigroup/walnut_creek",$data);
	}
	
	
	public function success_walnutcreek(){
		
		$vr = $this->session->userdata('user_id');
	    if( isset($vr) &&  $vr > 0 ){
			
			$this->load->view("realestate/vaswanigroup/walnut_creek-success");
		}else{
			redirect(SITE_URL.'realestate/vaswanigroup/walnut_creek');
		}
	   //$this->load->view("realestate/purvaamaiti");
	}


public function frm_submit(){
		
		if( isset($_POST) && count($_POST) > 0 ){
			
			$this->form_validation->set_rules('name', 'Name' , 'trim|required');
			$this->form_validation->set_rules('email', 'Email' , 'trim|required');
			$this->form_validation->set_rules('phone', 'Phone No.' , 'trim|required');
			if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";exit;
			
		   }else{
			  
			   $query = $this->db->query("select id, email from ".VASWANIGROUP." where email ='".$_POST['email']."' and lp_name ='".$_POST['lp_name']."'");
			    if($query->num_rows() > 0){
					
					echo "done";exit;
					
				}else{
					
					$data = array(
							 'name'          => $_POST['name'],
							 'phone'         => $_POST['phone'],
							 'email'         => $_POST['email'],
							 'country'       => $_POST['country'],
							 'utm_source'    => $_POST['utm_source1'],
							 'utm_source2'   => $_POST['utm_source2'],
							 'utm_source3'   => $_POST['utm_source3'],
							 'utm_campaign'  => $_POST['campaign'],
							 'lp_name'       => $_POST['lp_name'],
							 'ip_address'    => $_SERVER['REMOTE_ADDR'],
							 'date_created'  => date( 'Y-m-d H:i:s' ) 					   
							);
							
					$result = $this->db->insert(VASWANIGROUP, $data);	    
					$insert_id = $this->db->insert_id($result);
					$this->session->set_userdata('user_id', $insert_id);
					$this->session->set_userdata('utm_source', trim($_POST['utm_source1']));
					$this->session->set_userdata('phone', trim($_POST['phone']));
				

					
					
				}echo "done";exit;
		   }
		}else{
			
			 echo "empty";exit;
			 
	    }
		
		exit;
	}
}


?>