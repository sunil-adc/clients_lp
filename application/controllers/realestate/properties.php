
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class properties extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
	
		$this->db->cache_off();
		$this->load->model(array("cache_model", "email_templates","lead_check"));
         $this->load->helper(array("general_helper","realestate_helper"));
		
	}
	public function index(){	
		
		header("location:javascript://history.go(-1)");
		//$this->load->view("realestate/sterlingascentia/sterlingascentia");
	}
    public function sterlingascentia(){    
        
        
        $this->load->view("realestate/sterlingascentia/sterlingascentia");
    }
    public function sterlingascentia_success(){    
        
        
        $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
            
            $this->load->view("realestate/sterlingascentia/sterlingascentia-success");
        } else {
            redirect(SITE_URL.'realestate/properties/sterlingascentia');
        }
        
    }

    public function gcorptheicon(){    
        
      
        $this->load->view("realestate/gcorptheicon/gcorptheicon");
    }
    public function gcorptheicon_success(){    
        
        $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
            
            $this->load->view("realestate/gcorptheicon/gcorptheicon-success");
        } else {
            redirect(SITE_URL.'realestate/properties/gcorptheicon');
        }
        
    }

     public function galleriaresidences(){    
        
        $this->load->view("realestate/galleriaresidences/galleriaresidences");
    }
    public function galleriaresidences_success(){    
        
       $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
            
            $this->load->view("realestate/galleriaresidences/galleriaresidences-success");
        } else {
            redirect(SITE_URL.'realestate/properties/galleriaresidences');
        }
        
    }


   public function mittalelanza(){
      $this->load->view("realestate/mittalelanza/mittalelanza");
   }


    public function mittalelanza_success(){

       $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
            
            $this->load->view("realestate/mittalelanza/mittalelanza-success");
        } else {
            redirect(SITE_URL.'realestate/properties/mittalelanza');
        }
    }

    
    public function lgcl_newlife(){
        
        $this->load->view("realestate/lgcl/new-life");
    }
    public function lgcl_newlife_success(){
        
          $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
            $this->load->view("realestate/lgcl/new-life-success");
        } else {
            redirect(SITE_URL.'realestate/properties/lgcl_newlife');
        }
        
    }

    public function lgcl_pueblo(){
        
        $this->load->view("realestate/lgcl/pueblo");
    }
    public function lgcl_pueblo_success(){
        $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
              $this->load->view("realestate/lgcl/pueblo-success");
        } else {
            redirect(SITE_URL.'realestate/properties/lgcl_pueblo');
        }
      
    }

    public function lgcl_highstreet(){
        
        $this->load->view("realestate/lgcl/high-street");
    }
    public function lgcl_highstreet_success(){

       // print_r($this->session->all_userdata());exit;
        $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
             $this->load->view("realestate/lgcl/high-street-success");
        } else {
            redirect(SITE_URL.'realestate/properties/lgcl_highstreet');
        }
        
       
    }

    public function maya_indradhanush(){
        
        $this->load->view("realestate/indradhanush/maya-indradhanush");
    }
    public function maya_indradhanush_success(){

       // print_r($this->session->all_userdata());exit;
        $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
             $this->load->view("realestate/indradhanush/maya-indradhanush-success");
        } else {
            redirect(SITE_URL.'realestate/properties/maya_indradhanush');
        }
        
       
    }
     public function tata(){
        
        $this->load->view("realestate/tata/tatavaluehomes");
    }
    public function tata_success(){

        $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
             $this->load->view("realestate/tata/tatavaluehomes-success");
        } else {
            redirect(SITE_URL.'realestate/properties/tata');
        }
        
    }


    public function tatapromont(){
        
        $this->load->view("realestate/tata/tatahomespromont");
    }
    public function tatapromont_success(){

        $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
             $this->load->view("realestate/tata/tatahomespromont-success");
        } else {
            redirect(SITE_URL.'realestate/properties/tatapromont');
        }
    }


    public function brigade(){
        $this->load->view("realestate/brigade/brigade");
    }

    public function brigade_success(){
        $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) { 
            
        $this->load->view("realestate/brigade/brigade-success");
        } else {
            redirect(SITE_URL.'realestate/properties/brigade');
        }
    }
   

    public function frm_submit()
    {
       
        if (isset($_POST) && count($_POST) > 0) {
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required');
            $this->form_validation->set_rules('phone', 'Phone No.', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                
                echo "validation_error";
                ;
                
            } else {
                $all_array = all_arrays();
                
                $key = array_search($_POST['lp'], $all_array['REALESTATE_CLIENTS']);
                
                $lp_name = $all_array['REALESTATE_CLIENTS'][$key];
                $result = $this->general->is_duplicate(REALESTATE_USER ,"id,email","email ='" . $_POST['email'] . "' and client_name=".$key);
                if ($result == true) {
                    
                    echo "done";
                    exit;
                    
                } else {
                    $data      = array(
                        'name' => $_POST['name'],
                        'phone' => $_POST['phone'],
                        'email' => $_POST['email'],
                        'country' => $_POST['country'],
                        'client_name'=>$key,
                        'utm_source' => $_POST['utm_source1'],
                        'utm_sub' => $_POST['utm_source2'],
                        'utm_medium' => $_POST['utm_source3'],
                        'ip_address' => $_SERVER['REMOTE_ADDR'],
                        'utm_campaign' => $_POST['campaign'],
                        'date_created' => date('Y-m-d H:i:s')
                    );

                    $insert_id = $this->general->add_user(REALESTATE_USER,$data);

                   if($lp_name == "sterlingascentia"){
                       $result    =  api_sterlingascentia($data);
                       $res_array = json_decode($result);
                       $lead_id   = $res_array->response->lead_id;
                    }

                    elseif ($lp_name == "gcorptheicon") {
                        $result    = api_gcorptheicon($data);
                        $res_array = json_decode($result);
                        $lead_id   = $res_array->response->lead_id;
                    }
                    elseif($lp_name == "mittalelanza"){
                        $result    = api_mittalelanza($data);
                        $res_array = json_decode($result);
                        $lead_id   = $res_array->response->lead_id;
                    }
                    elseif($lp_name == "maya_indradhanush"){
                        $result    = api_maya_indradhanush($data);
                        $res_array = json_decode($result);
                        $lead_id   = $res_array->response->lead_id;
                    }
                    elseif($lp_name == "tata"){
                        $result    = api_tata($data);
                        $res_array = json_decode($result);
                        $lead_id   = $res_array->response->lead_id;
                    }
                    elseif($lp_name == "tatapromont"){
                        $result    = api_tatapromont($data);
                        $res_array = json_decode($result);
                        $lead_id   = $res_array->response->lead_id;
                    }

                                     
                    $this->session->set_userdata('user_id', $insert_id);
                    $this->session->set_userdata('utm_source', trim($_POST['utm_source1']));
                    $this->session->set_userdata('phone', trim($_POST['phone']));
                    if($lead_id != ""){
                        $res = $this->general->update_lead(REALESTATE_USER,$insert_id,$lead_id);
                    }
                    
                }
                echo "done";
                exit;
            }
            
            
        } else {
            
            echo "empty";
            exit;
        }
        
        
    }

    

    

    
	
}
?>