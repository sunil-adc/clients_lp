<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class hiranandani extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper(array("general","realestate_helper"));
		$this->load->model(array("cache_model", "email_templates","lead_check"));
		$this->db->cache_off();
	}
   
	public function index(){				
	   
	  redirect("http://www.houseofhiranandanianchorage.com/lms.php?cid=7017F0000017zkEQAQ#form_container");
    }
    
   			
    public function anchorage(){
        $this->load->view("realestate/anchorage");
    }
   
    public function anchorage_success(){
	    
	    $user_id = $this->session->userdata('user_id');
	 
	    if(isset($user_id) &&  $user_id > 0 ){
			
		 	$this->load->view("realestate/anchorage-success");
		 }else{
			 	
			redirect(SITE_URL.'realestate/hiranandani/anchorage');
		}	
	    
   }

   public function evita(){
    	
    	$this->load->view("realestate/evita");
   }
   
    public function evita_success(){
	   
	    $user_id = $this->session->userdata('user_id');
	 
	    if(isset($user_id) &&  $user_id > 0 ){
			
		 	$this->load->view("realestate/evita-success");
		
		 }else{
			 	
			redirect(SITE_URL.'realestate/hiranandani/evita');
		}	  	
		 	
	    
    }
   
	

    public function submit_frm(){
	   if( isset($_POST) && count($_POST) > 0 ){
			$this->form_validation->set_rules('name', 'Name' , 'trim|required');
			$this->form_validation->set_rules('email', 'Email' , 'trim|required');
			$this->form_validation->set_rules('phone', 'Phone No.' , 'trim|required');
		    if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		    }else{
				
				$res = $this->db->query("select id,email from ".HIRANANDANI." where email ='".$_POST['email']."'");
			       
			    if($res->num_rows() > 0){
			      
				  echo "done";
				  exit;
			   
			    }else{
					    
					    $res = $this->db->query("select id,email from ".HIRANANDANI." where email ='".$_POST['email']."' and lp_name = '".$_POST['lp_name']."'");
			       
					    if($res->num_rows() > 0){
					      
						  echo "done";
						  exit;
					   
					    }else{
							$data = array(
									 'name'          => $_POST['name'],
									 'phone'         => $_POST['phone'],
									 'email'         => $_POST['email'],
									 'country'       => $_POST['country'],
									 'utm_source'    => $_POST['utm_source1'],
									 'utm_sub'       => $_POST['utm_source2'],
									 'utm_medium'    => $_POST['utm_source3'],
									 'lp_name'       => $_POST['lp_name'],
									 'ip_address'    => $_SERVER['REMOTE_ADDR'],
									 'utm_campaign'  => $_POST['campaign'],
									 'date_created' => date( 'Y-m-d H:i:s' ) 					   
									);

						$result = $this->db->insert(HIRANANDANI, $data);	    
					    $insert_id = $this->db->insert_id($result);
						$this->session->set_userdata('user_id', $insert_id);
						$this->session->set_userdata('utm_source', trim($_POST['utm_source1']));
					}	
				
				$cid = $_POST['cid'];
				
				if($insert_id != ""){
					   
					if($cid == 'hoh-bannerghatta-adcanopus'){	
					   
					    $result_hiran    =  api_hoh_hiranandani($data);
                        $res_hiran 	     =  json_decode($result_hiran);
                        $res_lead_id     =  $res_hiran->response->lead_id;

                        if($res_lead_id != ""){
	                       
	                       $this->general->update_lead(HIRANANDANI,$insert_id,$res_lead_id);
	                    }
						
					   
						}else{

						$curl = curl_init();
							   
						curl_setopt_array($curl, array(
						CURLOPT_URL => "https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8",
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 30,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "POST",
						CURLOPT_POSTFIELDS => "oid=00D7F0000010u7h&retURL=http%3A%2F%2Famazepromos.com%2Frealestate%2Fhiranandani%2Fanchorage&Campaign_ID=".$cid."&debug=1&debugEmail=atul%40adcanopus.com&last_name=".$_POST['name']."&email=".$_POST['email']."&mobile=".$_POST['phone']."&country_code=IN&utm_source=Adcanopus&utm_campaign=Adcanopus_email&utm_medium=ad&PageURL=http%3A%2F%2Fwww.houseofhiranandanianchorage.com%2Flms.php%3Fcid=".$cid,
						CURLOPT_HTTPHEADER => array(						
							"cache-control: no-cache",
							"content-type: application/x-www-form-urlencoded",
						
							),
						));
	 
						$response = curl_exec($curl);
						$err = curl_error($curl);

						curl_close($curl);
					}
 

				}

				echo "done";
				exit;
				}
				
		    }
	   }else{
		   
		   echo "empty";   exit;
       } 
	   
	   
   }

}
?>