<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ozone extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
	
		$this->db->cache_off();
		$this->load->model(array("cache_model", "email_templates","lead_check"));
		
	}
	public function index(){	
		
	  
    }
   
	public function ozone_urbana(){	
		$data=array();	
		$this->load->view("realestate/ozone/ozone-urbana",$data);
	  
    }
	public function ozone_urbana_success(){
		$data=array();
		$this->load->view("realestate/ozone/ozone-urbana_success",$data);
	}
	
	public function submit_frm(){
	 
	   if( isset($_POST) && count($_POST) > 0 ){
			$this->form_validation->set_rules('name', 'Name' , 'trim|required');
			$this->form_validation->set_rules('email', 'Email' , 'trim|required');
			$this->form_validation->set_rules('phone', 'Phone No.' , 'trim|required');
		    if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		    }else{
				
				$res = $this->db->query("select id,email from ".OZONE." where email ='".$_POST['email']."'");
			       
			    if($res->num_rows() > 0){
			      
				  echo "done";
				  exit;
			   
			    }else{
					$data = array(
							 'name'          => $_POST['name'],
							 'phone'         => $_POST['phone'],
							 'email'         => $_POST['email'],
							 'country'       => $_POST['country'],
							 'utm_source'    => $_POST['utm_source1'],
							 'utm_sub'       => $_POST['utm_source2'],
							 'utm_medium'        => $_POST['utm_source3'],
							'lp_name'       => $_POST['lp_name'],
							 'ip_address'    => $_SERVER['REMOTE_ADDR'],
							 'utm_campaign'  => $_POST['campaign'],
							 'date_created' => date( 'Y-m-d H:i:s' ) 					   
							);
					$result = $this->db->insert(OZONE, $data);	    
				    $insert_id = $this->db->insert_id($result);
					$this->session->set_userdata('user_id', $insert_id);
					$this->session->set_userdata('utm_source', trim($_POST['utm_source1']));
					$this->session->set_userdata('phone', trim($_POST['phone']));
					if($insert_id != ""){
				    $api_url = 'https://lead.anarock.com/api/v0/adcanopus/sync-lead'; // as mentioned on left under HTTP Request Heading

				    $key = '711032e021af85fb'; 
					$current_time = time();
					$message = (string)$current_time;

					// to lowercase hexits
					$hash = hash_hmac('sha256', $message, $key);


					// you can use libraries like https://github.com/brick/phonenumber to format numbers before sending

					
					//$country_code = $number->getRegionCode(); // GB
					//$phone = $number->getNationalNumber(); // 7123456789
                    //$number = PhoneNumber::parse('+447123456789'); // phone number of the lead with +
                    $country_code = strtoupper($_POST['country_name']); // GB
					$phone = $_POST['phone']; // 7123456789
                    // shows that lead is generated from google
					$source_id = 12; //Affiliate source id
					// shows that lead is generated from google GDN.
					$sub_source_id = $_POST['campaign'] ;
					$campaign_id   = "adcanopus-ozone-cpql"; // String to Attribute the lead to specific project. contact ANAROCK team for this.
 					 
					$postFields  = "";
					$postFields .= "&email=".$_POST['email']."&name=".$_POST['name']; // refer to query parameters section on left section
					$postFields .= "&current_time=".$current_time;
					$postFields .= "&purpose=buy";
					$postFields .= "&source_id=".$source_id;
					$postFields .= "&sub_source_id=".$sub_source_id;
					
					$postFields .= "&phone=".$phone; // Phone number without country code
					$postFields .= "&country_code=".$country_code; // Standard ISO3166-1 alpha-2 code for a country.
					$postFields .= "&hash=".$hash; // its mandatory to create hash using same timestamp as sent in current_time parameter
					$postFields .= "&campaign_id=".$campaign_id;
                  
                   $ch = curl_init();

					curl_setopt($ch, CURLOPT_URL,$api_url);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS,
					            $postFields);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

					$server_output = curl_exec ($ch);
					if(curl_errno($ch)){
                        echo 'Request Error:' . curl_error($ch);
                     }
					 print_r($server_output);exit;
				}
			}
				echo "done";
					exit;
				}
				
		    
	   }else{
		   
		   echo "empty";   exit;
       } 
	   
	   
   }

   	

}

?>