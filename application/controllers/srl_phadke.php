<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class srl_phadke extends CI_Controller
{	
    public $country = "";
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		$this->load->model("cache_model");
		$this->db->cache_off();
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), $this->input->get('bnr'), $this->input->get('country'),  $this->input->get('zoneid'),  $this->input->get('mobile_track'), $this->input->get('ClickID'));
		
	}
   
	public function index(){				
	   
	   $data['city_arr'] = $this->cache_model->get_cache(CITY_CACHE, "id,city,status", CITY, " city ", "");	   
	   
	   $this->load->view("srl-phadke", $data);

    }
   
   
    public function srlfrm(){
   		
	    if( is_array($_POST) && count($_POST) > 0 ){
		   foreach ($_POST as $k=>$v){
			   $this->form_validation->set_rules($k, $k , 'trim|required');
		   }
	   
		   if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		   }else{
			    $res = $this->db->query("select id, email from ".USER_DATA." where email ='".$_POST['email']."'");
			       
			    if($res->num_rows() > 0){
			      
				  echo "already";
			   
			    }else{
				   
				  				   
			    $data = array(
							 'name'         => $_POST['name'],
							 'phone'        => $_POST['phone'],
							 'email'        => $_POST['email'],
							 'city'         => $_POST['city'],
							 'clients'      => 2,
							 'date_created' => date( 'Y-m-d H:i:s' ) 					   
							);
							
			    //$result = $this->db->insert(USER_DATA, $data);	    
			    $insert_id = 4;//$this->db->insert_id($result);
				
				if($insert_id != "" && $sr != "d_sr"){
					
					$n     = $_POST['name'];
					$p     = $_POST['phone'];
					$e     = $_POST['email'];
					$c     = $_POST['city'];
					$d     = $_POST['s_d'];
					$time  = $_POST['s_time'];
					$test  = $_POST['s_test'];
					$addr  = $_POST['addr'];
					
					echo "done";
					exit;
				}
				
			    echo "done";
			}
				 
	      }
      }else{
	   
	   echo "empty";   
      }

      exit;
   }
   
   

}
?>