<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class nmims extends CI_Controller
{	
    
    public function __construct(){
	   
        parent::__construct();
		$this->load->helper("general");
		//$this->load->model("cache_model");
		$this->load->model(array("cache_model", "email_templates","lead_check"));
		$this->db->cache_off();
		
	}
   
	public function index(){				
	   
	   
	   $this->load->view("nmims");

    }

   
   
    public function nmims_success(){
   		
	   
	    $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) {
            
          $this->load->view("nmims-success");
      } else {
            redirect(SITE_URL.'nmims');
        }
   }

   public function executivemba(){
    	$this->load->view("nmims-mba");
    }

    public function executivemba_success(){
   		
	   
	    $vr = $this->session->userdata('user_id');
        if (isset($vr) && $vr > 0) { 
            
          $this->load->view("nmims-mba-success");
        } else {
            redirect(SITE_URL.'nmims/executivemba');
        }
   }


   public function digitalMarketing(){
		$this->load->view("nmims-marketing");
	}

	public function digitalMarketing_success(){
		
	
		$vr = $this->session->userdata('user_id');
		if (isset($vr) && $vr > 0) { 
			
		$this->load->view("nmims-marketing-success");
		} else {
			redirect(SITE_URL.'nmims/digitalMarketing');
		}
	}

   
   public function submit_frm(){
	   

	   $all_array = all_arrays();
	   if( isset($_POST) && count($_POST) > 0 ){
			$this->form_validation->set_rules('name', 'FirstName' , 'trim|required');
			
			$this->form_validation->set_rules('email', 'Email' , 'trim|required');
			$this->form_validation->set_rules('phone', 'Phone No.' , 'trim|required');
		    if($this->form_validation->run() == FALSE){
			  
			   echo "validation_error";;
			
		    }else{
				
				$res = $this->db->query("select id, email from ".NMIMS." where phone ='".$_POST['phone']."'");
			       
			    if($res->num_rows() > 0){
			      
				  echo "done";
				  exit;
			   
			    }else{
					$data = array(
							 'name'         => $_POST['name'],
							
							 'phone'         => $_POST['phone'],
							 'email'         => $_POST['email'],
							 'country'       => $_POST['country'],
							 'city'          => $_POST['city'],
							 'course'        => $_POST['course'],
							 'utm_source'    => $_POST['utm_source1'],
							 'utm_sub'       => $_POST['utm_source2'],
							 'utm_medium'    => $_POST['utm_source3'],
							 'ip_address'    => $_SERVER['REMOTE_ADDR'],
							 'utm_campaign'  => $_POST['campaign'],
							 'date_created'  => date( 'Y-m-d H:i:s' ) 					   
							);
				
				if($_POST['course'] == "master_programs"){
				
					$data['course'] =  "Master Programs";
					$data['center'] =   $_POST['center'];
					$city           =   $_POST['city'];
				}else if($_POST['course'] == "digital_marketing"){
						
						$data['course'] = "Digital Marketing";
						$city           =  $_POST['city'];

				}else{

					foreach($all_array['NMIMS_CITY'] as $k=>$v){
						if($_POST['city'] == $k){
							$city = $v;
						}
					}
				}

				$result = $this->db->insert(NMIMS, $data);	 
				
			    $insert_id = $this->db->insert_id($result);
				$this->session->set_userdata('user_id', $insert_id);
				$this->session->set_userdata('utm_source', trim($_POST['utm_source1']));
				$this->session->set_userdata('phone', trim($_POST['phone']));
				if($insert_id !=""){
					
					if($_POST['course'] == "Post Graduate Diploma - 2 yr "){
						$course = "Post Graduate Diploma Programs";
					}elseif($_POST['course'] =="Diploma - 1 yr"){
						$course = "Diploma Programs";
					}elseif($_POST['course'] == "Certificate - 6 months"){
						$course = "Certificate Programs";
					}elseif($_POST['course'] == "Professional Programs - 1 year"){
						$course = "Professional Programs";
					}elseif($_POST['course'] == "master_programs"){
						$course = "Master Programs";
					}else{
						$course = "Executive Programs";
					}
					
					
					
					$data1 = array(
							 'Name'         => $_POST['name'],
							 'ContactNo'         => $_POST['phone'],
							 'EmailID'         => $_POST['email'],
							 'CurrentLocation'          => $city ,
							 'CourseIntrestedIn'        => $course,
							 'Agency'    => "adcanopus1",
							 'AgencyPassword'       => "Adcanopus30@2019",
							 'HighestQualification'    => " ",
							 'AdmissionYear'  => " " 					   
							);
					$api_url ="https://ngasce.secure.force.com/services/apexrest/leadservice";
					

					$ch = curl_init();

					curl_setopt($ch, CURLOPT_URL,$api_url);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
					curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data1));
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$server_output = curl_exec ($ch);



					if(curl_errno($ch)){
                        echo 'Request Error:' . curl_error($ch);
                    }else{

                    	$r = json_decode($server_output);

						if(is_object($r) && count($r) > 0) {
							
							if(strtolower($r->result) == "success" ){
								
								if($r->LeadId != ""){
			                        $this->general->update_lead(NMIMS, $insert_id, $r->LeadId);
			                        

			                    }
								

							}
						}
                    }
					
				}
				echo "done";
					exit;
				}
				
		    }
	   }else{
		   
		   echo "empty";   exit;
       } 
	   
	   
   }



   public function gettest(){


   	$data1 = array(
							 'Name'         => "atultest",
							 'ContactNo'         => '991640087000',
							 'EmailID'         => 'atultest12@gmail.com',
							 'CurrentLocation'          => 'bangalore' ,
							 'CourseIntrestedIn'        => 'Certificate Programs',
							 'Agency'    => "adcanopus1",
							 'AgencyPassword'       => "Adcanopus30@2019",
							 'HighestQualification'    => " ",
							 'AdmissionYear'  => " " 					   
							);
					$api_url ="https://ngasce.secure.force.com/services/apexrest/leadservice";
					
					$ch = curl_init();

					curl_setopt($ch, CURLOPT_URL,$api_url);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
					curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data1));
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$server_output = curl_exec ($ch);

					$r = json_decode($server_output);

					if(is_object($r) && count($r) > 0) {
						
						if(strtolower($r->result) == "success" ){
							
							echo $r->LeadId;

						}
					}

					if(curl_errno($ch)){
                        echo 'Request Error:' . curl_error($ch);
                    }
   }
   
  

}
?>