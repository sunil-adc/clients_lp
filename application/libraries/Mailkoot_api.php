<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mailkoot_api{
		
	public function __construct() {
	}
	
	public function mailkoot_param($get_param, $listkey, $listid) {
		$user_id   = "";
		$method    = "POST";
		$all_array = all_arrays();
		
		if(is_object($get_param) ){		
			$params = array(
							'mailing_list_id' => $listid,
							//'mailing_list_id' => 139,
							'email'           => $get_param->email,
							'status'          => 'active',
							'subscribe_time'  => $get_param->date_created,
							'custom_fields'   => array(
									'name'      	=> $get_param->name,
									'mobile' 		=> $get_param->phone,
							)
						);
						
			$result = $this->greenarrow_studio_create_subscriber($params, $listkey, $user_id, $method);
		}else{
		    
			$result = "feilds null";
		}
		return $result;
		
	}
	
	
	public function greenarrow_studio_create_subscriber($params, $listkey, $user_id, $method) {
		
		$all_arr = all_arrays();
		
		// Gather parameters needed to communicate with GA Studio.
		$listID   = $params['mailing_list_id'];
		$user_id  = ($user_id != 0 || $user_id != "" ? "/".$user_id : "" );
		$url      = 'https://tracecampaigns.com/ga/api/v2/mailing_lists/'.$listID.'/subscribers';
		
		//$url      = 'https://app.tracecampaigns.com/ga/api/v2/mailing_lists/'.$listID.'/subscribers';
		$ch       = curl_init($url);
		
		$campaign = array('subscriber' => $params);
		$json     = json_encode($campaign);
		
		// Set up cURL to communicate this message.
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		//curl_setopt($ch, CURLOPT_USERPWD, GAS_API_KEY);
		curl_setopt($ch, CURLOPT_USERPWD, $listkey);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($json),
		));
		
		// Execute the command, retrieve HTTP errors.
		$response_raw = curl_exec($ch);
		$error_test   = curl_error($ch);
		$err          = curl_errno($ch);
		
		// Don't leave the connection hanging.
		curl_close($ch);
		
		// First, check if there was an HTTP error.
		if ($err != 0) {
		$rv = "ERROR: cURL - $err $error_test\n";
		return $rv;
		}
		
		// Decode Studio's response JSON.
		$result = json_decode($response_raw);
		
		// Return an appropriate response.
		if (isset($result->success)) {
		if ($result->success == false) {
		  $return_value = "Error:";
		
		  if (isset($result->error_message)) {
			$return_value .= " " . $result->error_message;
		  }
		} else if ($result->success == true) {
		  $return_value = "OK";
		} else {
		  $return_value = "Error: unknown status";
		}
		} else {
		$return_value = "Error: unknown response from GAS Station: $response_raw";
		}
		
		return $return_value;
		
	}
	

}