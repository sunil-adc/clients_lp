<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class memcached_live {
		
	private $m;
	public function __construct() {
	}
	
	public function mem_connect() {
		if (version_compare(PHP_VERSION, '5.4.0') < 0) {
			$this->m = new Memcached;
			$this->m->connect('cachecluster1.4wcvbl.cfg.apse1.cache.amazonaws.com', 11211);
		} else {
			$this->m = new Memcached;
			$this->m->connect('cachecluster1.4wcvbl.cfg.apse1.cache.amazonaws.com', 11211);
		}
		
		//$this->m = memcache_connect(MEMCACHE_CLUSTER_NAME, 11211);
		
	}
	
	public function set_cache ($key, $value, $expire_time = 604800, $compress = true) {
		if (isset($key) && $key != '') {
			
			$this->mem_connect();
			$key = md5(MEMCACHE_KEY).'_'.$key;
			
			return $this->m->set($key, $value, (($compress == true) ? MEMCACHE_COMPRESSED : ''), $expire_time); 
		
		} else {
		
			return 'Key must be alpha numeric empty key is not allowed';
		
		}
	}
	
	public function get_stats() { 
	    $this->mem_connect();
		return $this->m->getExtendedStats();
	}
	
	public function get_version() { 
	    $this->mem_connect();
		return $this->m->getVersion();
	}
	
	public function _flush_cache() { 
	    $this->mem_connect();
		return $this->m->flush();
	}

	public function get_cache($key = '') {
		$this->mem_connect();
		$key = md5(MEMCACHE_KEY).'_'.$key;
			
		return (isset($key) && $key != '') ? $this->m->get($key) : false;	
	}
	
	public function del_cache($key = '') {
		
		if (isset($key) && $key != '') {
			$this->mem_connect();
			$key = md5(MEMCACHE_KEY).'_'.$key;
			
			$this->m->delete($key);
		}
	}
}