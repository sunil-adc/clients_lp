
function chck_valid(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#"+ele+"_err").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#"+ele+"_err").html(""); 
  }
	
}


function properties_jsfrm(u,lp,i){
 
  var page ="";
  var name          = $("#name"+i).val();
  var phone         = $("#phone"+i).val();
  var email         = $("#email"+i).val();
  var country       = $("#CountryCode"+i).val();
  var utm_source   = $("#utm_source").val();
  var utm_campaign	= $("#utm_campaign").val();
  var utm_medium	= $("#utm_medium").val();
  
  var utm_sub	= $("#utm_sub").val();
 
   if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
	if(phone.length != '10'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	} 
	
	
	
	
 	var r_ = new Array( "name","email","phone");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
		
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&country="+country+"&utm_source="+utm_source+"&utm_campaign="+utm_campaign+"&utm_medium="+utm_medium+"&utm_sub=" +utm_sub+"&lp_name=11",
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				  
				if(e == "202"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				    window.location.href = "https://www.brigadeorchards.com/apartments/"+lp+"-success.php";
				   
				}
			}
		}
  
      ); 
}
