
 <!DOCTYPE html>
     <?php
        define("S3_URL", "https://www.brigadeorchards.com/apartments/cdn/site/brigade-assets/");
        define("SITE_URL","https://www.brigadeorchards.com/apartments/");
     ?>

 <html lang="en">
 <head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title> Brigade North Home Fest | Aug 30th to Sept 1st at Brigade Orchards, Devanahalli </title>      
 <link rel="shortcut icon" href="./cdn/site/brigade-assets/images/fav.png" type="image/x-icon" />
 
 
 <link rel="stylesheet" href="./cdn/site/brigade-assets/css/vendor.css">
 <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css">
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
 <link rel="stylesheet" href="./cdn/site/brigade-assets/css/main.css">    
 <!-- fonts -->
 <link href="https://fonts.googleapis.com/css?family=Great+Vibes&amp;subset=latin-ext" rel="stylesheet">
 <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i&amp;subset=latin-ext,vietnamese"
 rel="stylesheet">
 <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
 
 <!-- DO NOT MODIFY -->
 <!-- Quora Pixel Code (JS Helper) -->
 <script>
 !function(q,e,v,n,t,s){if(q.qp) return; n=q.qp=function(){n.qp?n.qp.apply(n,arguments):n.queue.push(arguments);}; n.queue=[];t=document.createElement(e);t.async=!0;t.src=v; s=document.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t,s);}(window, 'script', 'https://a.quora.com/qevents.js');
 qp('init', '0250ea612ac748c6b5516e1e84caa786');
 qp('track', 'ViewContent');
 </script>
 <noscript><img height="1" width="1" style="display:none" src="https://q.quora.com/_/ad/0250ea612ac748c6b5516e1e84caa786/pixel?tag=ViewContent&noscript=1"/></noscript>
 <!-- End of Quora Pixel Code --> 
 <script>qp('track', 'GenerateLead');</script>
 
 
    </head>
    <body> 
 <!-- 	
    <header class="">
          <div class="logo">
             <a href="JavaScript:Void(0);" target=""><img src="./cdn/site/brigade-assets/images/brigade.png"></a>
          </div>
          <div class="clearB"></div>
       </header> -->
       
       
     <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="4000">
        <ul class="carousel-indicators">
            <li class="slide1 active"></li>
            <li class="slide2"></li>
            <li class="slide3"></li>
            <li class="slide4"></li>
            <li class="slide5"></li>
            <li class="slide6"></li>
        </ul>
         <div class="carousel-inner" role="listbox">
             <div class="item item1 active">
                 <div class="">
                     <div class="carousel-caption">
 
                     </div>
                 </div>
             </div>
             <div class="item item2">
                <div class="">
                    <div class="carousel-caption">

                    </div>
                </div>
             </div>

             <div class="item item3">
                <div class="">
                    <div class="carousel-caption">

                    </div>
                </div>
            </div>

            <div class="item item4">
            <div class="">
                <div class="carousel-caption">

                </div>
            </div>
            </div>

            <div class="item item5">
            <div class="">
                <div class="carousel-caption">

                </div>
            </div>
            </div>

            <div class="item item6">
            <div class="">
                <div class="carousel-caption">

                </div>
            </div>
            </div>

             <a class="carousel-control-prev" href="#myCarousel"> <span class="carousel-control-prev-icon"></span> </a>
             <a class="carousel-control-next" href="#myCarousel"> <span class="carousel-control-next-icon"></span> </a>
         </div>
 
     </div>
     <!--//banner -->    
     
     <section>
         <div class="PDIDM-section" id="Program" style="padding: 60px 0px;">
                <div class="container text-center wow fadeInDown animated" data-wow-delay=".6s" style="visibility: visible; animation-name: fadeInDown;">
                        <h2 class="crown-t">A Multifaceted and Inclusive Township<br> in North Bangalore<br><br></h2>
                        

                        <div class="col-md-12">

                            <div class="col-lg-4 col-md-4">
                                <ul class="crown-ul">
                                    <li>2 &amp; 3 BHK Apartments in 135 Acre Smart Township with Fully
                                        functional Sports Arena &amp; Signature Club Resort
                                    </li>
                                   
                                </ul>
                            </div>
                            
                            <div class="col-lg-4 col-md-4">
                                <ul class="crown-ul">
                                    <li>Just 10 Mins from Airport, Close Proximity to Yelahanka &amp;
                                        Manyata  Tech Park, Quick Access to Hebbal &amp; Whitefield
                                    </li>
                                </ul>
                            </div>

                            <div class="col-lg-4 col-md-4">
                                <ul class="crown-ul">
                                    <li>Ready to Move In Apartments Starting at 57 Lakhs* All Inclusive,
                                        No GST, No Floor Rise
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h4 class="stamp">*Stamp Duty &amp; Registration Charges Extra</h4>
                        </div>

                    </div>
         </div>
     </section>

     <section id="projectConf" class="sppb-section   resort-discount wow">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="sppb-col-sm-12">
                        
                        <div class="section-title sppb-text-center">
                            <h2 class="sppb-title-heading myt delay-10s animated wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown; color: #fff; text-align: center; font-size: 35px;">
                                Project Configuration</h2>
                            <div class="underline2" style="margin-bottom: 5px;">&nbsp;</div>
                        </div>
                        
                        <div class="delay-10s animated wow fadeInDown animated" style="visibility: visible; animation-name: fadeInLeft;">
                            <table class="table spe-table" style="text-align: center; margin: auto; width: 100%;color: #fff;border-color: #fff;" border="0" cellspacing="0" cellpadding="0">
                                <thead>
                                    <tr>
                                        <th class="her" style="background: #90948f; text-align: center;">
                                            Configuration
                                        </th>
                                        <th class="her" style="background: #717171; text-align: center;">SBA
                                            Range
                                            (Sq.Ft)
                                        </th>
                                        <th class="her" style="background: #90948f; text-align: center;">Price
                                        </th>
                                    </tr>
                                </thead>
                                <tbody style="color: #5c5c5c;">
                                    <tr style="background: #ededed;">
                                        <td scope="row" class="her1">2 BHK</td>
                                        <td class="her1">1080 - 1390</td>
                                        <td class="her1 cursor click_here"><a href="javascript:void(0)" class="price-click"> Click
                                                Here</a></td>
                                    </tr>
                                    <tr style="background-color: #fff;">
                                        <td scope="row" class="her1">3 BHK</td>
                                        <td class="her1">1290 - 3100</td>
                                        <td class="her1 cursor click_here"><a href="javascript:void(0)" class="price-click"> Click
                                                Here</a></td>
                                    </tr>
                                    <tr style="background: #ededed;">
                                        <td scope="row" class="her1">Villas</td>
                                        <td class="her1">4900 - 4920</td>
                                        <td class="her1 cursor click_here"><a href="javascript:void(0)" class="price-click"> Click
                                                Here</a></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

     <section id="award" class="sppb-section   rooms-suits">
        <div style="width: 85%; margin: 0px auto;">


            <div class="row awarded">
                <div>
                    <div class="sppb-section-title sppb-text-center" style="margin-top: 5.5em;">
                        <h5 class="nearby-head">Awarded One Of India’s Top 3 Smart
                            Townships*</h5>
                        <div class="underline2" style="margin-bottom: 5px;">&nbsp;</div>
                    </div>

                    <div class="col-md-3 awardImages">
                        <img src="./cdn/site/brigade-assets/images/sports-arena.jpg">
                        <p>World-class Sports Arena</p>
                    </div>
                    <div class="col-md-3 awardImages">
                        <img src="./cdn/site/brigade-assets/images/school.jpg">
                        <p>School at Brigade Orchards</p>
                    </div>
                    <div class="col-md-3 awardImages">
                        <img src="./cdn/site/brigade-assets/images/office.jpg">
                        <p>Office &amp; Retail</p>
                    </div>
                    <div class="col-md-3 awardImages">
                        <img src="./cdn/site/brigade-assets/images/arts-village.jpg">
                        <p>Arts Village</p>
                    </div>
                </div>
            </div>
            <br><br><br> 


        </div>
    </section>

    <section id="aminites">
     
        <div class="sppb-col-sm-12 amenities-icon overlay" style="color: #fff;text-align: center; padding: 110px 21px;">
            <div class="sppb-section-title sppb-text-center">
                <h2 class="sppb-title-heading myt delay-09s animated wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown; ">
                    Amenities</h2>
                <div class="underline2">&nbsp;</div>
            </div>
            <!--<div class="sppb-empty-space  clearfix" style="height: 50px;"></div>-->

            <div class="row" style="margin-bottom: 45px;">
                <h3 class="ami-head margin20Zero">World Class Sports Arena</h3>
                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/pool.png">
                        <p class="animated fadeInUp wow">Swimming Pool - 6 Lane
                        </p>
                    </div>
                </div>

                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/bathroom.png">
                        <p class="animated fadeInUp wow">Indoor Heated Pool</p>
                    </div>
                </div>


                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/cricket-2.png">
                        <p class="animated fadeInUp wow">Cricket Ground</p>
                    </div>
                </div>

                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/tennis-racket.png">
                        <p class="animated fadeInUp wow">Lawn Tennis Courts</p>
                    </div>
                </div>

                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/basketball.png">
                        <p class="animated fadeInUp wow">Basket Ball Court</p>
                    </div>
                </div>

                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/volleyball.png">
                        <p class="animated fadeInUp wow">Volley Ball Court</p>
                    </div>
                </div>

                <div class="col-md-2 col-xs-6 marginTopBottom15 col-md-offset-2">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/table-tennis.png">
                        <p class="animated fadeInUp wow">Table Tennis Room</p>
                    </div>
                </div>

                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/football.png">
                        <p class="animated fadeInUp wow">Football Ground</p>
                    </div>
                </div>

                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/billiard-ball.png">
                        <p class="animated fadeInUp wow">Billiards/ Snooker Room
                        </p>
                    </div>
                </div>

                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/meditation.png">
                        <p class="animated fadeInUp wow">Yoga Terrace</p>
                    </div>
                </div>
            </div>


            <div class="row" style="margin-bottom: 45px;">
                <h3 class="ami-head margin20Zero">Lifestyle Features</h3>
                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/cplay.png">
                        <p class="animated fadeInUp wow">Kids Play Areas</p>
                    </div>
                </div>

                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/amphitheatre.png">
                        <p class="animated fadeInUp wow">Amphitheatre</p>
                    </div>
                </div>

                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/bicycle-1.png">
                        <p class="animated fadeInUp wow">Cycling Track Across
                            Township</p>
                    </div>
                </div>

                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/rock.png">
                        <p class="animated fadeInUp wow">Rock Park</p>
                    </div>
                </div>

                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/shop.png">
                        <p class="animated fadeInUp wow">Convenience Store</p>
                    </div>
                </div>


            </div>


            <div class="row">
                <h3 class="ami-head margin20Zero">Signature Club Resort</h3>
                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/conference.png">
                        <p class="animated fadeInUp wow">Conference Hall</p>
                    </div>
                </div>

                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/dish.png">
                        <p class="animated fadeInUp wow">Multi Cuisine
                            Restaurant</p>
                    </div>
                </div>

                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/dumbbell.png">
                        <p class="animated fadeInUp wow">State-of-the-art
                            Gymnasium</p>
                    </div>
                </div>

                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/spa.png">
                        <p class="animated fadeInUp wow">Spa For Relaxation</p>
                    </div>
                </div>

                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/banquet.png">
                        <p class="animated fadeInUp wow">Banquet Space For Up To
                            2000 Guests</p>
                    </div>
                </div>

                <div class="col-md-2 col-xs-6 marginTopBottom15">
                    <div class="i-holder">
                        <img class="animated fadeInUp wow" src="./cdn/site/brigade-assets/images/barbershop.png">
                        <p class="animated fadeInUp wow">Salon For Men &amp; Women
                        </p>
                    </div>
                </div>


            </div>


        </div>
    </section>
     
 
     <section>
         <div class="programme_structure_section" id="Specialisations">
             <div class="container">					
                 <div class="specialisation_block">
                     <h2 class="heading-01 ">Gallery</h2>
                     <div class="programme_section">
                         <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                             <ul class="resp-tabs-list">
                                 <li class="resp-tab-item1 resp-tab-active" aria-controls="tab_item-0" role="tab">General</li>
                                 <li class="resp-tab-item2" aria-controls="tab_item-1" role="tab">2 BHK Floor Plan</li>
                                 <li class="resp-tab-item3" aria-controls="tab_item-2" role="tab">3 BHK Floor Plan</li>
                                 
                             </ul>
                             <div class="resp-tabs-container">
                                 <h2 class="resp-accordion resp-accordion1 resp-tab-active" role="tab" aria-controls="tab_item-0"><span class="resp-arrow"></span>Core Specialisation</h2><div class="resp-tab-content resp-tab-content1 resp-tab-content-active mobileTab" aria-labelledby="tab_item-0">
                                     <div class="sem01-T">
                                     <div class="latest-post sppb-col-sm-6 col-md-4">
                                         <div class="prop-div">
                                             <a class="move">
                                                 <div class="prop-img-div generalImgContainer">
                                                     <img src="./cdn/site/brigade-assets/images/1.jpg" class="full-width zoomimg containerImageHeigth">
                                                     
                                                 </div>
                                             </a>
                                             
                                         </div>
                                     </div>
                                     </div>
 
                                     <div class="sem01-B">
                                     <div class="latest-post sppb-col-sm-6 col-md-4">
                                         <div class="prop-div">
                                             <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                 <div class="prop-img-div generalImgContainer">
                                                     <img src="./cdn/site/brigade-assets/images/2.jpg" class="full-width zoomimg containerImageHeigth">
                                                     
                                                    
                                                 </div>
                                             </a>
                                             
                                         </div>
                                     </div>
                                     </div>
                                     <!-- <i class="clearB"></i> -->
 
                                     <div class="sem01-T">
                                     <div class="latest-post sppb-col-sm-6 col-md-4">
                                         <div class="prop-div">
                                             <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                 <div class="prop-img-div generalImgContainer">
                                                     <img src="./cdn/site/brigade-assets/images/3.jpg" class="full-width zoomimg containerImageHeigth">
                                                     
                                                 </div>
                                             </a>
                                             
                                         </div>
                                     </div>
                                 </div>
 
 
                                <div class="sem01-B">
                                     <div class="latest-post sppb-col-sm-6 col-md-4">
                                         <div class="prop-div">
                                             <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                 <div class="prop-img-div generalImgContainer">
                                                     <img src="./cdn/site/brigade-assets/images/4.jpg" class="full-width zoomimg containerImageHeigth">
                                                     
                                                 </div>
                                             </a>
                                             
                                         </div>
                                     </div>
                                     </div>
 
 
                                     <div class="sem01-T">
                                     <div class="latest-post sppb-col-sm-6 col-md-4">
                                         <div class="prop-div">
                                             <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                 <div class="prop-img-div generalImgContainer">
                                                     <img src="./cdn/site/brigade-assets/images/5.jpg" class="full-width zoomimg containerImageHeigth" >
                                                     
                                                 </div>
                                             </a>
                                             
                                         </div>
                                     </div>
                                 </div>
 
 
                                <div class="sem01-B">
                                     <div class="latest-post sppb-col-sm-6 col-md-4">
                                         <div class="prop-div">
                                             <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                 <div class="prop-img-div generalImgContainer">
                                                     <img src="./cdn/site/brigade-assets/images/6.jpg" class="full-width zoomimg containerImageHeigth" >
                                                     
                                                 </div>
                                             </a>
                                             
                                         </div>
                                     </div>
                                     </div>
 
 
                                     <div class="sem01-T">
                                     <div class="latest-post sppb-col-sm-6 col-md-4">
                                         <div class="prop-div">
                                             <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                 <div class="prop-img-div generalImgContainer">
                                                     <img src="./cdn/site/brigade-assets/images/7.jpg" class="full-width zoomimg containerImageHeigth">
                                                     
                                                 </div>
                                             </a>
                                             
                                         </div>
                                     </div>
                                     </div>
 
                                 </div>
                                 <h2 class="resp-accordion resp-accordion2" role="tab" aria-controls="tab_item-2"><span class="resp-arrow"></span>Second Specialisations</h2>
                                 <div class="resp-tab-content resp-tab-content2" aria-labelledby="tab_item-2">
                                 <div class="sem01-T">
                                     <div class="latest-post sppb-col-sm-6 col-md-4">
                                         <div class="prop-div">
                                             <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                 <div class="prop-img-div">
                                                     <img src="./cdn/site/brigade-assets/images/floorplan1.jpg" class="full-width zoomimg containerImageHeigth" >
                                                     
                                                     
                                                 </div>
                                             </a>
                                             
                                         </div>
                                     </div>
                                     </div>
 
                                     <div class="sem01-B">
                                     <div class="latest-post sppb-col-sm-6 col-md-4">
                                         <div class="prop-div">
                                             <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                 <div class="prop-img-div">
                                                     <img src="./cdn/site/brigade-assets/images/floorplan2.jpg" class="full-width zoomimg containerImageHeigth" >
                                                     
                                                     
                                                 </div>
                                             </a>
                                             
                                         </div>
                                     </div>
                                     <div class="latest-post sppb-col-sm-6 col-md-4">
                                         <div class="prop-div">
                                             <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                 <div class="prop-img-div">
                                                     <img src="./cdn/site/brigade-assets/images/floorplan3.jpg" class="full-width zoomimg containerImageHeigth" >
                                                     
                                                     
                                                 </div>
                                             </a>
                                             
                                         </div>
                                     </div>
                                     <div class="latest-post sppb-col-sm-6 col-md-4">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="./cdn/site/brigade-assets/images/floorplan4.jpg" class="full-width zoomimg containerImageHeigth" >
                                                    
                                                    
                                                </div>
                                            </a>
                                            
                                        </div>
                                    </div>
                                    <div class="latest-post sppb-col-sm-6 col-md-4">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="./cdn/site/brigade-assets/images/floorplan5.jpg" class="full-width zoomimg containerImageHeigth" >
                                                    
                                                    
                                                </div>
                                            </a>
                                            
                                        </div>
                                    </div>
                                    <div class="latest-post sppb-col-sm-6 col-md-4">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="./cdn/site/brigade-assets/images/floorplan6.jpg" class="full-width zoomimg containerImageHeigth" >
                                                    
                                                    
                                                </div>
                                            </a>
                                            
                                        </div>
                                    </div>
                                    
                                     </div>
 
                                     
                                     
                                 </div>
 
                                 
 
                                 <h2 class="resp-accordion resp-accordion3" role="tab" aria-controls="tab_item-3"><span class="resp-arrow"></span>Third Specialisations</h2>
                                 <div class="resp-tab-content resp-tab-content3" aria-labelledby="tab_item-3">
                                 
 
                                 <div class="sem01-T">
                                     <div class="latest-post sppb-col-sm-6 col-md-4">
                                         <div class="prop-div">
                                             <a class="move">
                                                 <div class="prop-img-div">
                                                     <img src="./cdn/site/brigade-assets/images/third1.jpg" class="full-width zoomimg containerImageHeigth" >
                                                     
                                                     
                                                 </div>
                                             </a>
                                             
                                         </div>
                                     </div>
                                     </div>
 
                                     <div class="sem01-B">
                                     <div class="latest-post sppb-col-sm-6 col-md-4">
                                         <div class="prop-div">
                                             <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                 <div class="prop-img-div">
                                                     <img src="./cdn/site/brigade-assets/images/third2.jpg" class="full-width zoomimg containerImageHeigth">
                                                     
                                                     
                                                 </div>
                                             </a>
                                             
                                         </div>
                                     </div>
                                     </div>
                                     <!-- <i class="clearB"></i>                                 -->
 
 
                                
 
 
                                     <div class="sem01-T">
                                     <div class="latest-post sppb-col-sm-6 col-md-4">
                                         <div class="prop-div">
                                             <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                 <div class="prop-img-div">
                                                     <img src="./cdn/site/brigade-assets/images/third3.jpg" class="full-width zoomimg containerImageHeigth" >
                                                     
                                                 </div>
                                             </a>
                                             
                                         </div>
                                     </div>
                                 </div>
 
 
                                <div class="sem01-B">
                                     <div class="latest-post sppb-col-sm-6 col-md-4">
                                         <div class="prop-div">
                                             <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                 <div class="prop-img-div">
                                                     <img src="./cdn/site/brigade-assets/images/third4.jpg" class="full-width zoomimg containerImageHeigth" >
                                                     
                                                 </div>
                                             </a>
                                             
                                         </div>
                                     </div>
                                     </div>
 
 
                                     <div class="sem01-T">
                                     <div class="latest-post sppb-col-sm-6 col-md-4">
                                         <div class="prop-div">
                                             <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                 <div class="prop-img-div">
                                                     <img src="./cdn/site/brigade-assets/images/third5.jpg" class="full-width zoomimg containerImageHeigth" >
                                                     
                                                     
                                                 </div>
                                             </a>
                                             
                                         </div>
                                     </div>
                                     <div class="latest-post sppb-col-sm-6 col-md-4">
                                        <div class="prop-div">
                                            <a class="move" onclick="pricePopProjectname('Orchards Apts');">
                                                <div class="prop-img-div">
                                                    <img src="./cdn/site/brigade-assets/images/third6.jpg" class="full-width zoomimg containerImageHeigth" >
                                                    
                                                    
                                                </div>
                                            </a>
                                            
                                        </div>
                                    </div>
                                     </div>
 
                                     
                                     <i class="clearB"></i>
                                 </div>
 
                                 
                                 
 
                                 
                                 
                             </div>
                         </div>
                         <i class="clearB"></i>
                     </div>
                 </div><!--Specialization-->
                 
             </div>				
         </div><!--Programme Structure-->
     </section>

     <section class="mapSection">
        <h2 class="crown-t" style="text-align: center; margin: 1em 0; font-size: 35px;">Location</h2>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3883.812779875313!2d77.7220913148248!3d13.237062990682132!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1d6a607bee43%3A0x41105d60db4a0c74!2sBrigade+Orchards!5e0!3m2!1sen!2sin!4v1521109322967" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen=""></iframe>

     </section>

     


 
     <footer class="footerSec">
 
         <div class="copy-right-grids">
             <div class="container">
                 <h2>About Brigade Orchards</h2>
                 <p class="footer-gd">
                        Brigade Orchards is a 135-acre smart township, ten minutes from the Bangalore International Airport. A multifaceted and inclusive township, it offers you a richer life. Within the complex are residences, school, Proposed Hospital, Signature club resort, World class sports arena, Shopping & leisure and Offices. A sports arena, indoor games and workout facilities at the Signature Club Resort, Jogging tracks and winding trails give you enough options to stay fit. While retail spaces take care of your everyday needs.
                 </p>

                 <br>
                  <p class="footer-gd">
                        Brigade Orchards will revolutionize the urban lifestyle by integrating more than just the ordinary into a single enclave. This is sure to change the way Bangaloreans live today.
                  </p>
             </div>
         </div>
 
     </footer> 
     <footer id="sp-footer" style="background: #eaeaea;padding: 18px 0;color: #000;" class="wow">
        <div class="container delay-09s animated wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
            <div class="row">
                <div id="" class="col-sm-4 col-md-4">
                    <div class="sp-column">
                        <div class="text-center">
                            

                            RERA - PRM/KA/RERA/1250/303/PR/170916/000462 
                        </div>
                    </div>
                </div>
                <div id="" class="col-sm-4 col-md-4">
                    <div class="sp-column">
                        <span class="sp-copyright">Copyright © 2019. all rights reserved.</span>

                    </div>
                </div>
                <div id="" class="col-sm-4 col-md-4">
                    <div class="sp-column">
                        <div class="sp-copyright"> 
                            <a href="http://www.brigadeorchards.com/terms" target="_blank" style="color: blue;">
                                Terms &amp; Conditions.</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>   
 
     <div class="floating-form visiable" id="contact_form">
     <div class="contact-opener">Enquire Now</div>
         <form role="form" id="feedbackForm" class="second_form feedbackForm" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>brigade_api.php','brigade','2')">
                    
                     <div class="">       
                     <div class="col">
                            <div class="form-logo">
                                 <a href="#">
                                     <img src="./cdn/site/brigade-assets/images/brigade.png" />
                                 </a> 
                             </div> 
                             <h4>Building Positive Experience</h4>                           
                             </div> 
                         <div class="col">
                         <div class="form-group">
                             <input type="text" class="form-control" id="name2" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                             <span class="help-block" id="name_err2"></span>
                         </div>
                         
                     <div class="form-group">
                         <input type="email" class="form-control" id="email2" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                         <span class="help-block" id="email_err2" ></span>
                     </div>
                     
                     <div class="form-group">
                         <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode2">
                         <input type="text" class="form-control only_numeric phone" id="phone2" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                         <span class="help-block" id="phone_err2"> </span>
                         
                     </div>
                     
                     <div class="form-group radio-form">
                         <div class="custom-radio">
                             <p>
                                 <input type="checkbox" id="agree3" name="agree3" value="agree"  required checked disabled>
                                 <label for="agree3">I authorize Brigade representative to contact me via phone and/or email. This will override registry on DND/NDNC.</label>
                             </p>
                         </div>                        
                     </div>
 
                   
                                          
                     <div class="submitbtncontainer">
                         <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                     </div>
 
                     <p class="disclaimer">All fields are mandatory. T&C and Privacy Policy applies.</p>
 
                 </div>
             </div>
 
         </form>	
     </div>
 
     <div class="popup-enquiry-form mfp-hide" id="popupForm">
         <form role="form" id="feedbackForm" class="third_form feedbackForm" action="JavaScript:void(0)" onsubmit="properties_jsfrm('<?php echo SITE_URL?>brigade_api.php','brigade','3')">                   
                     <div class="">       
                     <div class="col">
                            <div class="form-logo">
                                 <a href="#">
                                     <img src="./cdn/site/brigade-assets/images/brigade.png" />
                                 </a> 
                             </div> 
                             <h4>Building Positive Experience</h4>                           
                             </div> 
                         <div class="col">   
                         <div class="form-group">
                             <input type="text" class="form-control" id="name3" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                             <span class="help-block" id="name_err3"></span>
                         </div>
               
                     <div class="form-group">
                         <input type="email" class="form-control" id="email3" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                         <span class="help-block" id="email_err3" ></span>
                     </div>
                     
                     <div class="form-group">
                         <input type="hidden" class="hiddenCountry" name="CountryCode" value="91" id="CountryCode3">
                         <input type="text" class="form-control only_numeric phone" id="phone3" name="phone" pattern="\d*"  placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                         <span class="help-block" id="phone_err3"> </span>
                        
                     </div>
                     
                     <div class="form-group radio-form">
                         <div class="custom-radio">
                             <p>
                                 <input type="checkbox" id="agree3" name="agree3" value="agree"  required checked disabled>
                                 <label for="agree3">I authorize Brigade representative to contact me via phone and/or email. This will override registry on DND/NDNC.</label>
                             </p>
                         </div>    
                     
                         <span class="help-block" id="cource_err3"> </span>                           
                     </div>
                                           
                     <div class="submitbtncontainer">
                         <input type="submit" id="frm-sbmtbtn3" value="Submit" name="submit">
                     </div>
 
                     <p class="disclaimer">All fields are mandatory. T&C and Privacy Policy applies.</p>
 
                 </div>
             </div>
             
         </form>
     </div>
                                     
 
 
 
     <input type="hidden" name="siteurl" id="siteurl" value="<?php echo SITE_URL?>" />
     <input type="hidden" id="utm_source" name="utm_source" value="<?php echo (isset($_REQUEST['utm_source']) != "" ? $_REQUEST['utm_source'] : " "); ?>">
     <input type="hidden" id="utm_campaign" name="utm_campaign" value="<?php echo (isset($_REQUEST['utm_campaign']) != "" ? $_REQUEST['utm_campaign'] : " "); ?>">
     <input type="hidden" id="utm_medium" name="utm_medium" value="<?php echo (isset($_REQUEST['utm_medium']) != "" ? $_REQUEST['utm_medium'] : " "); ?>">
     <input type="hidden" id="utm_sub" name="utm_sub" value="<?php echo (isset($_REQUEST['utm_sub']) != "" ? $_REQUEST['utm_sub'] : " "); ?>">
     
     <script src="./cdn/site/brigade-assets/js/vendor.js"></script> 
     <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
     <script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script> 
 
     <script src="./cdn/site/brigade-assets/js/main.js"></script>
     
     <script src="./cdn/site/scripts/properties.js"></script>
 
 
 <script>

    $(document).ready(function(){
        $("#myCarousel").carousel();

        $(".slide1").click(function(){
            $("#myCarousel").carousel(0);
        });
        $(".slide2").click(function(){
            $("#myCarousel").carousel(1);
        });
        $(".slide3").click(function(){
            $("#myCarousel").carousel(2);
        });
        $(".slide4").click(function(){
            $("#myCarousel").carousel(3);
        });
        $(".slide5").click(function(){
            $("#myCarousel").carousel(4);
        });
        $(".slide6").click(function(){
            $("#myCarousel").carousel(5);
        });
    });
 
     $(".resp-tab-item1").click(function(){
         $(this).addClass("resp-tab-active");
         $(".resp-tab-item2").removeClass("resp-tab-active");
         $(".resp-tab-item3").removeClass("resp-tab-active");
         $(".resp-tab-item4").removeClass("resp-tab-active");
         $(".resp-tab-item5").removeClass("resp-tab-active");
         
         $(".resp-tab-content1").addClass("resp-tab-content-active");
         $(".resp-tab-content1").show();
 
         $(".resp-tab-content2").removeClass("resp-tab-content-active");
         $(".resp-tab-content3").removeClass("resp-tab-content-active");
         $(".resp-tab-content4").removeClass("resp-tab-content-active");
         $(".resp-tab-content5").removeClass("resp-tab-content-active");
 
         $(".resp-tab-content2").hide();
         $(".resp-tab-content3").hide();
         $(".resp-tab-content4").hide();
         $(".resp-tab-content5").hide();
 
     });
 
 
     $(".resp-tab-item2").click(function(){
         $(this).addClass("resp-tab-active");
         $(".resp-tab-item1").removeClass("resp-tab-active");
         $(".resp-tab-item3").removeClass("resp-tab-active");
         $(".resp-tab-item4").removeClass("resp-tab-active");
         $(".resp-tab-item5").removeClass("resp-tab-active");
         
         $(".resp-tab-content2").addClass("resp-tab-content-active");
         $(".resp-tab-content2").show();
 
         $(".resp-tab-content1").removeClass("resp-tab-content-active");
         $(".resp-tab-content3").removeClass("resp-tab-content-active");
         $(".resp-tab-content4").removeClass("resp-tab-content-active");
         $(".resp-tab-content5").removeClass("resp-tab-content-active");
 
         $(".resp-tab-content1").hide();
         $(".resp-tab-content3").hide();
         $(".resp-tab-content4").hide();
         $(".resp-tab-content5").hide();
         
     });
 
     
     $(".resp-tab-item3").click(function(){
         $(this).addClass("resp-tab-active");
         $(".resp-tab-item2").removeClass("resp-tab-active");
         $(".resp-tab-item1").removeClass("resp-tab-active");
         $(".resp-tab-item4").removeClass("resp-tab-active");
         $(".resp-tab-item5").removeClass("resp-tab-active");
         
         $(".resp-tab-content3").addClass("resp-tab-content-active");
         $(".resp-tab-content3").show();
 
         $(".resp-tab-content2").removeClass("resp-tab-content-active");
         $(".resp-tab-content1").removeClass("resp-tab-content-active");
         $(".resp-tab-content4").removeClass("resp-tab-content-active");
         $(".resp-tab-content5").removeClass("resp-tab-content-active");
 
         $(".resp-tab-content2").hide();
         $(".resp-tab-content1").hide();
         $(".resp-tab-content4").hide();
         $(".resp-tab-content5").hide();
     });
     
     $(".resp-tab-item4").click(function(){
         $(this).addClass("resp-tab-active");
         $(".resp-tab-item2").removeClass("resp-tab-active");
         $(".resp-tab-item3").removeClass("resp-tab-active");
         $(".resp-tab-item1").removeClass("resp-tab-active");
         $(".resp-tab-item5").removeClass("resp-tab-active");
         
         $(".resp-tab-content4").addClass("resp-tab-content-active");
         $(".resp-tab-content4").show();
 
         $(".resp-tab-content2").removeClass("resp-tab-content-active");
         $(".resp-tab-content3").removeClass("resp-tab-content-active");
         $(".resp-tab-content1").removeClass("resp-tab-content-active");
         $(".resp-tab-content5").removeClass("resp-tab-content-active");
 
         $(".resp-tab-content2").hide();
         $(".resp-tab-content3").hide();
         $(".resp-tab-content1").hide();
         $(".resp-tab-content5").hide();
     });
     
     $(".resp-tab-item5").click(function(){
         $(this).addClass("resp-tab-active");
         $(".resp-tab-item2").removeClass("resp-tab-active");
         $(".resp-tab-item3").removeClass("resp-tab-active");
         $(".resp-tab-item4").removeClass("resp-tab-active");
         $(".resp-tab-item1").removeClass("resp-tab-active");
         
         $(".resp-tab-content5").addClass("resp-tab-content-active");
         $(".resp-tab-content5").show();
 
         $(".resp-tab-content2").removeClass("resp-tab-content-active");
         $(".resp-tab-content3").removeClass("resp-tab-content-active");
         $(".resp-tab-content4").removeClass("resp-tab-content-active");
         $(".resp-tab-content1").removeClass("resp-tab-content-active");
 
         $(".resp-tab-content2").hide();
         $(".resp-tab-content3").hide();
         $(".resp-tab-content4").hide();
         $(".resp-tab-content1").hide();
     });
 
     
 
 
 </script>
 
 <script>
     function pricePopProjectname(project) {
         var projname = project;
         $('.pricepopupTit').html(projname);
         $('#enqproject').val(projname);
         $('#price-pop').modal('show');
     }
   
 
 
 $(document).ready(function(){
 
   $(".aboutLink1").click(function(){
     $(".viewAboutDiv1").removeClass("displayNone");
   });
 
   $(".aboutLink2").click(function(){
     $(".viewAboutDiv2").toggleClass("displayNone");
   });
 
   $(".aboutLink3").click(function(){
     $(".viewAboutDiv3").toggleClass("displayNone");
   });
 
   $(".aboutLink4").click(function(){
     $(".viewAboutDiv4").toggleClass("displayNone");
   });
 
   $(".aboutLink5").click(function(){
     $(".viewAboutDiv5").toggleClass("displayNone");
   });
 
   $(".aboutLink6").click(function(){
     $(".viewAboutDiv6").toggleClass("displayNone");
   });
 
   $(".aboutLink7").click(function(){
     $(".viewAboutDiv7").toggleClass("displayNone");
   });
 
   $(".aboutLink8").click(function(){
     $(".viewAboutDiv8").toggleClass("displayNone");
   });
 
   $(".aboutLink9").click(function(){
     $(".viewAboutDiv9").toggleClass("displayNone");
   });
 
   $(".aboutLink10").click(function(){
     $(".viewAboutDiv10").toggleClass("displayNone");
   });
 
   $(".aboutLink11").click(function(){
     $(".viewAboutDiv11").toggleClass("displayNone");
   });
 
   $(".aboutLink12").click(function(){
     $(".viewAboutDiv12").toggleClass("displayNone");
   });
 
   $(".aboutLink13").click(function(){
     $(".viewAboutDiv13").toggleClass("displayNone");
   });
 
   $(".aboutLink14").click(function(){
     $(".viewAboutDiv14").toggleClass("displayNone");
   });
 
   $(".aboutLink15").click(function(){
     $(".viewAboutDiv15").toggleClass("displayNone");
   });
 
   $(".aboutLink1").click(function(){
     $(".viewAboutDiv1").toggleClass("displayNone");
   });
 
 
 });
 
 </script>
 
 </body>
 </html>