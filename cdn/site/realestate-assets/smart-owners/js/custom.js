$(document).ready(function () {
    addEventListener("load", function () {
        setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
        window.scrollTo(0, 1);
    }
    $('.testimonials-logos').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 4
            }
        }, {
            breakpoint: 520,
            settings: {
                slidesToShow: 3
            }
        }]
    });
    $(".phone").intlTelInput({
        utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/js/utils.js",
        separateDialCode: "true",
        initialCountry: "IN",

    });
    $(".phone").on("countrychange", function (e, countryData) {
        $(".hiddenCountry").val(countryData['dialCode']);
    });
});


$(document).ready(function () {

    var mfp = $.magnificPopup.instance;
    if ($('#popupForm').length) {
        /*setTimeout(function () {
            if (!mfp.isOpen) {
                $.magnificPopup.open({
                    items: {
                        src: '#popupForm'
                    },
                    type: 'inline'
                });
            }
        }, 15000);*/
        $('.open-popupform').magnificPopup({
            type: 'inline',
            items: {
                src: '#popupForm'
            }
        });

        if($("#utm_content").val() != null && $("#utm_content").val() != ' ' && $("#utm_content").val().length != 0 && $("#utm_content").val() != 'in') {
             $("#div_show").hide();

            //alert($("#utm_content").val());
        /*setTimeout(function () {
            if ($('.disclaimer-popup-block').length) {
                $.magnificPopup.open({
                    type: 'inline',
                    preloader: false,
                    modal: true,
                    items: {
                        src: '#disclaimer-modal'
                    }
                });
            }
        }, 0);
        $(function () {
            $(document).on('click', '.popup-modal-dismiss', function (e) {
                e.preventDefault();
                $.magnificPopup.close();
            });
        });*/

       }else{
        $("#div_hide").hide();
       }
    }

});
$(document).ready(function () {
    var _floatbox = $("#enquire_form"), _floatbox_opener = $(".enquire-opener");
    _floatbox.css("right", "0px"); //initial contact form position
    _floatbox.css("display", "none");
    //Contact form Opener button
    _floatbox_opener.click(function () {
        if (_floatbox.hasClass('visibleform')) {
            _floatbox.animate({ "right": "-300px" }, { duration: 300 }).removeClass('visibleform');
        } else {
            _floatbox.animate({ "right": "0px" }, { duration: 300 }).addClass('visibleform');
        }
    });
    _floatbox.animate({ "top": "30px" }, { easing: "linear" }, { duration: 500 });
});
var $window = $(window);
var nav = $('#enquire_form');
$window.scroll(function () {
    if ($window.scrollTop() <= 500) {
        nav.css("display", "none");
    }
    else {
        nav.css("display", "block");
    }
});