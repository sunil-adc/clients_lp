jQuery(document).ready(function($){
	$('.country').select2();
	var	scrolling = false;
	var contentSections = $('.cd-section'),
		verticalNavigation = $('.cd-vertical-nav'),
		navigationItems = verticalNavigation.find('a'),
		navTrigger = $('.cd-nav-trigger'),
		scrollArrow = $('.cd-scroll-down');

	$(window).on('scroll', checkScroll);

	//smooth scroll to the selected section
	verticalNavigation.on('click', 'a', function(event){
        event.preventDefault();
        smoothScroll($(this.hash));
        verticalNavigation.removeClass('open');
    });

    //smooth scroll to the second section
    scrollArrow.on('click', function(event){
    	event.preventDefault();
        smoothScroll($(this.hash));
    });

	// open navigation if user clicks the .cd-nav-trigger - small devices only
    navTrigger.on('click', function(event){
    	event.preventDefault();
    	verticalNavigation.toggleClass('open');
    });

	function checkScroll() {
		if( !scrolling ) {
			scrolling = true;
			(!window.requestAnimationFrame) ? setTimeout(updateSections, 300) : window.requestAnimationFrame(updateSections);
		}
	}

	function updateSections() {
		var halfWindowHeight = $(window).height()/2,
			scrollTop = $(window).scrollTop();
		contentSections.each(function(){
			var section = $(this),
				sectionId = section.attr('id'),
				navigationItem = navigationItems.filter('[href^="#'+ sectionId +'"]');
			( (section.offset().top - halfWindowHeight < scrollTop ) && ( section.offset().top + section.height() - halfWindowHeight > scrollTop) )
				? navigationItem.addClass('active')
				: navigationItem.removeClass('active');
		});
		scrolling = false;
	}

	function smoothScroll(target) {
        $('body,html').animate(
        	{'scrollTop':target.offset().top},
        	300
        );
	}
	$(function() {  
		$('.wthree-btn')
		  .on('mouseenter', function(e) {
				  var parentOffset = $(this).offset(),
				  relX = e.pageX - parentOffset.left,
				  relY = e.pageY - parentOffset.top;
				  $(this).find('span').css({top:relY, left:relX})
		  })
		  .on('mouseout', function(e) {
				  var parentOffset = $(this).offset(),
				  relX = e.pageX - parentOffset.left,
				  relY = e.pageY - parentOffset.top;
			  $(this).find('span').css({top:relY, left:relX})
		  });
		// $('[href=#]').click(function(){return false});
	  });
	  jQuery(function($) {
		$(".swipebox").swipebox();
	});
	$(function() {
		//Initialize filterizr with default options
		// $('.filtr-container').filterizr();
	});
	$(".phone").intlTelInput({
		utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/js/utils.js",
		separateDialCode: "true",
		initialCountry: "IN",
	});
	$(".phone").on("countrychange", function (e, countryData) {
		$(".hiddenCountry").val(countryData['dialCode']);
	});
});
jQuery(document).ready(function($) {
	$(".scroll").click(function(event){		
		event.preventDefault();

$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
	});
});

$(document).ready(function () {
	setTimeout(function() {
	 if ($('.popup-enquiry-form').length) {
	   $.magnificPopup.open({
		items: {
			src: '#popupForm' 
		},
		type: 'inline'
		  });
	   }
	 }, 15000);
	});



	$(document).ready(function () {

		var  _floatbox = $("#contact_form"), _floatbox_opener = $(".contact-opener");
		_floatbox.css("right", "0px"); //initial contact form position
		_floatbox.css("display", "none");
		//Contact form Opener button
		_floatbox_opener.click(function () {
			if (_floatbox.hasClass('visiable')) {
				_floatbox.animate({ "right": "-300px" }, { duration: 300 }).removeClass('visiable');		
			} else {
				_floatbox.animate({ "right": "0px" }, { duration: 300 }).addClass('visiable');			
			}
		});
		_floatbox.animate({ "top": "30px" }, { easing: "linear" }, { duration: 500 });
	});
	var $window = $(window);
	var nav = $('#contact_form');
	$window.scroll(function(){
		if ($window.scrollTop() <= 500) {
		   nav.css("display", "none");
		}
		else {
			nav.css("display", "block");
		}
	});

	$('.image-popup-fit-width').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		image: {
			verticalFit: false
		}
	});