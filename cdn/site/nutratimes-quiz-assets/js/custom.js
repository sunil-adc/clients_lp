function desktopStep() {
  $(".quiz-block-list-desktop .step").each(function (e) {
    if (e != 0)
      $(this).hide();
  });
  $(".btn-next").click(function () {
    if ($(".quiz-block-list-desktop .step:visible").next().length != 0)
      $(".quiz-block-list-desktop .step:visible").next().show().prev().hide();
    else {
      $(".quiz-block-list-desktop .step:visible").hide();
      $(".quiz-block-list-desktop .step:first").show();
    }
    return false;
  });
  $(".btn-prev").click(function () {

    if ($(".quiz-block-list-desktop .step:visible").prev().length != 0)

      $(".quiz-block-list-desktop .step:visible").prev().show().next().hide();

    else {
      $(".quiz-block-list-desktop .step:visible").hide();
      $(".quiz-block-list-desktop .step:last").show();
    }
    $('.step').removeClass('step-incomplete');
    return false;
  });
}
function mobileStep() {
  $(".quiz-block-list-mobile-view .step").each(function (e) {
    if (e != 0)
      $(this).hide();
  });
  $(".btn-next-mob").click(function () {
    if ($(".quiz-block-list-mobile-view .step:visible").next().length != 0)
      $(".quiz-block-list-mobile-view .step:visible").next().show().prev().hide();
    else {
      $(".quiz-block-list-mobile-view .step:visible").hide();
      $(".quiz-block-list-mobile-view .step:first").show();
    }
    return false;
  });
  $(".btn-prev-mob").click(function () {
    if ($(".quiz-block-list-mobile-view .step:visible").prev().length != 0)
      $(".quiz-block-list-mobile-view .step:visible").prev().show().next().hide();
    else {
      $(".quiz-block-list-mobile-view .step:visible").hide();
      $(".quiz-block-list-mobile-view .step:last").show();
    }
    $('.step').removeClass('step-incomplete');
    return false;
  });

}
function genderSelect(form) {

  $('#' + form).find('input[name=ageGroup]').change(function () {
    if ($(".quiz-block-list-desktop .step:visible").next().length != 0)
      $(".quiz-block-list-desktop .step:visible").next().show().prev().hide();
    else {
      $(".quiz-block-list-desktop .step:visible").hide();
      $(".quiz-block-list-desktop .step:first").show();
    }

    if ($(".quiz-block-list-mobile-view .step:visible").next().length != 0)
      $(".quiz-block-list-mobile-view .step:visible").next().show().prev().hide();
    else {
      $(".quiz-block-list-mobile-view .step:visible").hide();
      $(".quiz-block-list-mobile-view .step:first").show();
    }
  });
  $('#' + form).find('input[name=gender]').change(function () {

    if ($(".quiz-block-list-desktop .step:visible").next().length != 0)
      $(".quiz-block-list-desktop .step:visible").next().show().prev().hide();
    else {
      $(".quiz-block-list-desktop .step:visible").hide();
      $(".quiz-block-list-desktop .step:first").show();
    }
    if ($(".quiz-block-list-mobile-view .step:visible").next().length != 0)
      $(".quiz-block-list-mobile-view .step:visible").next().show().prev().hide();
    else {
      $(".quiz-block-list-mobile-view .step:visible").hide();
      $(".quiz-block-list-mobile-view .step:first").show();
    }
    var value = $('input[name=gender]:checked').val();
    var unvalue = $('input[name=gender]:not(:checked)').val();
    $('#' + form).find('.based-on-gender').removeClass(unvalue);
    $('#' + form).find('.based-on-gender').addClass(value);
    $('#' + form).find('.based-on-gender').removeClass(unvalue);

  });
}
function heightRange() {

  var $heightrangeslider = $('.heightRange');
  var $heightValue = $('.heightValue');
  var $mobileheightrangeslider = $('.mobileheightRange');
  var $mobileheightValue = $('.mobileheightValue');

  $heightrangeslider.rangeslider({ polyfill: false }).on('input', function () {
    $heightValue[0].value = this.value;
  });
  $heightValue.on('input', function () {
    $heightrangeslider.val(this.value).change();
  });

  $mobileheightrangeslider.rangeslider({ polyfill: false }).on('input', function () {
    $mobileheightValue[0].value = this.value;
  });

  $mobileheightValue.on('input', function () {
    $mobileheightrangeslider.val(this.value).change();
  });


}
function weightRange() {

  var $weightrangeslider = $('.weightRange');
  var $weightValue = $('.weightValue');
  var $mobileweightrangeslider = $('.mobileweightRange');
  var $mobileweightValue = $('.mobileweightValue');

  $weightrangeslider.rangeslider({ polyfill: false }).on('input', function () {
    $weightValue[0].value = this.value;
  });
  $weightValue.on('input', function () {
    $weightrangeslider.val(this.value).change();
  });

  $mobileweightrangeslider.rangeslider({ polyfill: false }).on('input', function () {
    $mobileweightValue[0].value = this.value;
  });

  $mobileweightValue.on('input', function () {
    $mobileweightrangeslider.val(this.value).change();
  });

}
function Step1Validation() {
  $(".step-1 .btn-next").click(function () {
    if ($('#desktopForm').find('input[name="ageGroup"]:checked').length) {
      $('.step-1').addClass('step-complete');
      return true;
    }
    else {
      $('.step-1').addClass('step-incomplete');
      $(".quiz-block-list-desktop .step:visible").hide();
      $(".quiz-block-list-desktop .step:first").show();
      return false;
    }
  });

  $(".step-1 .btn-next-mob").click(function () {
    if ($('#mobileForm').find('input[name="ageGroup"]:checked').length) {
      $('.step-1').addClass('step-complete');
      return true;

    }
    else {
      $('.step-1').addClass('step-incomplete');
      $(".quiz-block-list-mobile-view .step:visible").hide();
      $(".quiz-block-list-mobile-view .step:first").show();
      return false;

    }
  });
}
function Step2Validation() {
  $(".step-2 .btn-next").click(function () {
    if ($('#desktopForm').find('input[name="gender"]:checked').length) {
      $('.step-2').addClass('step-complete');
      return true;
    }
    else {
      $('.step-2').addClass('step-incomplete');
      $(".quiz-block-list-desktop .step:visible").hide();
      $(".quiz-block-list-desktop .step-2").show();
      return false;
    }
  });
  $('.step-2 .btn-next-mob').click(function () {
    if ($('#mobileForm').find('input[name="gender"]:checked').length) {
      $('.step-2').addClass('step-complete');
      return true;
    }
    else {
      $('.step-2').addClass('step-incomplete');
      $(".quiz-block-list-mobile-view .step:visible").hide();
      $(".quiz-block-list-mobile-view .step-2").show();
      return false;
    }
  });
}

function FormValidation(form) {
 

  var site_url = $('#' + form).find('input[name="siteurl"]').val();

  jQuery.validator.addMethod("phoneIN", function (phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, "");
    return this.optional(element) || phone_number.length == 10 && phone_number.match(/^[6-9]{1}[0-9]{9}$/);
  }, "Please enter valid 10 digit mobile number");

  jQuery.validator.addMethod("lettersonly", function (value, element) {
    return this.optional(element) || /^[a-zA-Z]+$/i.test(value);
  }, "Please enter valid name");


  $('#' + form).validate({ // initialize the plugin
    rules: {
      userName: {
        required: true,
        minlength: 3,
        lettersonly: true
      },
      userEmail: {
        required: true,
        email: true
      },
      userPhone: {
        required: true,
        number: true,
        phoneIN: true
      },
      otp: {
        required: true,
        minlength: 4,
        maxlength: 4,
        number: true,

      },
      heightValue: {
        required: true
      },
      weightValue: {
        required: true
      },
      gender: {
        required: true
      },
      ageGroup: {
        required: true
      },
      state: {
        required: true
      },
      activitiest: {
        required: true
      },
      loseweight: {
        required: true
      }
    },
    submitHandler: function () {
      if ($('#' + form).length) {
        userDetails(form);
      }
    }

  });

}
function userDetails(form) {
  var site_url = $('#' + form).find('input[name="siteurl"]').val();

  var userDetails = new Object();
  userDetails.userName = $('#' + form).find('input[name="userName"]').val();
  userDetails.userEmail = $('#' + form).find('input[name="userEmail"]').val();
  userDetails.userPhone = $('#' + form).find('input[name="userPhone"]').val();
  userDetails.ageGroup = $('#' + form).find('[name="ageGroup"]:checked').val();
  userDetails.gender = $('#' + form).find('[name="gender"]:checked').val();
  userDetails.height = $('#' + form).find('input[name="heightValue"]').val();
  userDetails.weight = $('#' + form).find('input[name="weightValue"]').val();
  userDetails.state = $('#' + form).find('select[name="state"]').val();
 // userDetails.loseweight = $('#' + form).find('select[name="loseweight"]').val();
  //userDetails.activitiest = $('#' + form).find('select[name="activitiest"]').val();
  userDetails.loseweight = $('#' + form).find('select[name="loseweight"]').val();
  userDetails.utm_source = $('#' + form).find('input[name="utm_source"]').val();
  userDetails.utm_medium = $('#' + form).find('input[name="utm_medium"]').val();
  userDetails.utm_sub = $('#' + form).find('input[name="utm_sub"]').val();
  userDetails.utm_campaign = $('#' + form).find('[name="utm_campaign"]').val();
  userDetails.otp = $('#' + form).find('[name="otp"]').val();
   //alert(userDetails.otp);
  if(userDetails.otp == ""){
    $.ajax({
    url: site_url + "nutratimes/submitfrm",
    type: "post",
    data: userDetails,
    beforeSend: function () {

    },
    success: function (e) {

       if(e == "done"){
       
        $('#' + form).find('.otp-box').addClass("active-text");

       }else{
            window.location.href = site_url+"nutratimes/quiz";
       }
    
          
    }
  });
  }else{
    $.ajax({
    url: site_url + "nutratimes/otp_verify",
    type: "post",
    data: userDetails,
    beforeSend: function () {

    },
    success: function (e) {
       if(e == "done"){
         $('#' + form).find(".otp_error").html("");
        window.location.href = site_url+"nutratimes/bmi_calculate";

       }else{
           $('#' + form).find(".otp_error").html("Incorrect OTP");
       }
    
          
    }
  });
  }
  



}



function calculateBmi(user) {
  var name = user.name;
  var height = user.heightValue;
  var weight = user.weightValue;
  var BMIScore = weight / (height / 100 * height / 100);
  var BMIScore = BMIScore.toFixed(2);
  var BMIStatus = "";
  var CalcType = "BMI";

  if (BMIScore < 18.5) {
    BMIStatus = "Thin";
  }
  if (BMIScore > 18.5 && BMIScore < 25) {
    BMIStatus = "Healthy";
  }
  if (BMIScore > 25) {
    BMIStatus = "Overweight";
  }

  var userResult = new Object();
  userResult.name = name;
  userResult.CalculationType = CalcType;
  userResult.BMIScore = BMIScore;
  userResult.BMIStatus = BMIStatus;
  sessionStorage.setItem('userResult', JSON.stringify(userResult));

}
function caluculateCalories(user) {
  var name = user.name;
  var height = user.heightValue;
  var weight = user.weightValue;
  var age = user.age;
  var gender = user.gender;
  if (gender == "female") {
    CaloriesValue = ((10 * weight) + (6.25 * height) - (5 * age) - 161);
  } else {
    CaloriesValue = ((10 * weight) + (6.25 * height) - (5 * age) + 5);
  };
  var userResult = new Object();
  userResult.name = name;
  userResult.CaloriesValue = CaloriesValue.toFixed();
  sessionStorage.setItem('userResult', JSON.stringify(userResult));
}
function result() {
  var score = $('#user_score').attr('data-bmi');


  var g = new JustGage({
    id: "gauge",
    value: score,
    min: 10,
    max: 40,
    color: "#000",
    gaugeWidthScale: 0.8,
    customSectors: [{
      color: "#EBE733",
      lo: 0,
      hi: 18.4
    },
    {
      color: "#A8E467",
      lo: 18.5,
      hi: 24.9
    },
    {
      color: "#ff9900",
      lo: 25,
      hi: 29.9
    },
    {
      color: "#EB392C",
      lo: 30,
      hi: 34.9
    },
    {
      color: "#B91E00",
      lo: 35,
      hi: 39.9
    },
    {
      color: "#B91E00",
      lo: 40,
      hi: 100
    }
    ],
    counter: true
  });
 

  if (score >= 1 && score <= 18.5) {
    $(".low-weight").addClass('active');
    
  }
  else if (score >= 18.5 && score <= 22) {
    $(".normal-weight").addClass('active');
    
  }
  else if (score >= 23 && score <= 25) {
    $(".over-weight").addClass('active');
   
  }
  else if (score >= 25.0 && score <= 30) {
    $(".class-one").addClass('active');
    
  }
  else if (score >= 30.0 && score <= 1000) {
    $(".class-two").addClass('active');
    
  }

}
function tabindexFix() {
  $("input[tabindex], textarea[tabindex]").each(function () {
      $(this).on("keypress", function (e) {
          if (e.keyCode === 13)
          {
              var nextElement = $('[tabindex="' + (this.tabIndex + 1) + '"]');
              if (nextElement.length) {
                  $('[tabindex="' + (this.tabIndex + 1) + '"]').focus();
                  e.preventDefault();
              } else
                  $('[tabindex="1"]').focus();
          }
      });
  });
}

$(document).ready(function () {
  $("#desktop_form").show();
  $("#loader").hide();
  $("#result").hide();
  $("#old").hide();
  desktopStep();
  mobileStep();
  heightRange();
  weightRange();
 
  Step1Validation();
  Step2Validation();
  genderSelect('desktopForm');
  genderSelect('mobileForm');
  FormValidation('desktopForm');
  FormValidation('mobileForm');
  FormValidation('quiz2');
  FormValidation('quiz3');
  tabindexFix();
  result();
  $('.btn-order').click(function () {
    $(this).addClass("active");
  });
  $(function () {
    $('#desktop_form').on('keydown', 'input', function (e) {
      if (e.which == 10 || e.which == 13) {
        $(this).next('input').focus();
        event.preventDefault();
      }
    });
  });
  $(function () {
    $('#desktop_form').each(function () {
      $(this).find('#phone_enter').keypress(function (e) {
        // Enter pressed?
        if (e.which == 10 || e.which == 13) {
          $(".btn-submit-d-form").click();
        }
      });
    });
  });
});



function otpverify(u,type){

  var otp = $("#otp"+type).val();
  
  if($.trim(otp) == "" || otp.length != '4'){
     $("#otp"+type).css("border-color","rgb(232, 46, 33)").focus();
     $("#otp_err"+type).html("Please Correct enter OTP");
    return false;  
  }else{
     $("#otp"+type).css("border-color","rgb(218, 218, 218)");
     $("#otp_err"+type).html(""); 
  }
  if(typeof $("#siteurl"+type).val()!="undefined"){
      var o=$("#siteurl"+type).val();
  }

  $.ajax({
      url:u,
      type:"post",
      data: "&otp="+otp,
      beforeSend:function(){
        
      },
      success:function(e){
       
        if(e == "done"){
         window.location.href = o+"nutratimes/bmi_calculate";
        }else if(e == "resend"){
          $("#otp"+type).val("");
          $("#otp_err"+type).html("Incorrect OTP");
           
        }
      }
    }) 

}

 function otpresend(u,type){
 //alert(u);return false
  $.ajax({
      url:u,
      beforeSend:function(){
        
      },
      success:function(e){
        //alert(e);return false;
        if(e == "done"){
           $(".otp").val("");
           //alert("otp resent successfully");
           $(".otp_sent").html("OTP Resent Successfully");
            
        }else{
         $(".otp_sent").html("Something went wrong");
        }
      }
    }
  
      ) 
 }

function orderfrm(u){
  
    
      var name          = $("#name").val();
      var phone         = $("#phone").val();
      var email         = $("#email").val();
      var address       = $("#address").val();
      var pincode       = $("#pincode").val();
      var city          = $("#city").val();
      var state         = $("#state").val();
    
      if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
         $("#name").css("border-color","rgb(232, 46, 33)").focus();
         $("#name_err").html("Please enter correct name");
         return false;  
      }else{
         $("#name").css("border-color","rgb(218, 218, 218)");
         $("#name_err").html(""); 
      }
    
      if(phone.length != '10'){
         $("#phone").css("border-color","rgb(232, 46, 33)").focus();
         $("#phone_err").html("Please enter valid 10 digit mobile number");
         return false;
      }else{
         $("#phone").css("border-color","rgb(218, 218, 218)");
         $("#phone_err").html(""); 
      }
      
      if($("#phone").val().length > 0){
         if(!$("#phone").val().match(/^[6-9]{1}[0-9]{9}$/)){
          $("#phone").css("border-color","rgb(218, 218, 218)").focus();
          $("#phone_err").html("Please enter correct mobile number");
          //$("#phone").css({"border":"1px solid #C50736"}).focus();
        return false; 
        }else{
          $("#phone").css("border-color","rgb(218, 218, 218)");
          $("#phone_err").html(""); 
        }  
      } 
    
      if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
         $("#email").css("border-color","rgb(232, 46, 33)").focus();
         $("#email_err").html("Please enter correct Email");
         return false;
      }else{
         $("#email").css("border-color","rgb(218, 218, 218)").focus();
         $("#email_err").html(""); 
      }
    
      if($.trim(address) == "" ){
         $("#address").css("border-color","rgb(232, 46, 33)").focus();
         $("#address_err").html("Please enter correct address");
         return false;  
      }else{
         $("#address").css("border-color","rgb(218, 218, 218)");
         $("#address_err").html(""); 
      }
    
      if($("pincode").val() != ""){
          if(!$("#pincode").val().match(/^[0-9]{6}$/)){
          $("#pincode").css("border-color","rgb(218, 218, 218)").focus();
          $("#pincode_err").html("Please enter pincode");
          //$("#phone").css({"border":"1px solid #C50736"}).focus();
          return false; 
        }else{
          $("#pincode").css("border-color","rgb(218, 218, 218)");
          $("#pincode_err").html(""); 
        } 
      }
      

      if($.trim(city) == "" || !city.match(/^[a-zA-Z\s]+$/)){
         $("#city").css("border-color","rgb(232, 46, 33)").focus();
         $("#city_err").html("Please enter correct city");
         return false;  
      }else{
         $("#city").css("border-color","rgb(218, 218, 218)");
         $("#city_err").html(""); 
      } 
      
      if($.trim(state) == "" ){
         $("#state").css("border-color","rgb(232, 46, 33)").focus();
         $("#state_err").html("Please select state");
         return false;  
      }else{
         $("#state").css("border-color","rgb(218, 218, 218)");
         $("#state_err").html(""); 
      } 
      
      if(typeof $("#siteurl").val()!="undefined"){
          var o=$("#siteurl").val()
      }

      var r_ = new Array( "name", "phone", "email","address","pincode","city","state");
        
      for( var j=0; j < r_.length; j++ ){

        if($.trim($("#"+r_[j]).val()) == "" || $("#"+r_[j]).val() == 0 || $("#"+r_[j]).val() == "undefined"){
           $("#"+r_[j]+"_err").html($("#"+r_[j]).attr("data-attr"));
           $("#"+r_[j]).focus();
          
          // return false;  
        }else{
           $("#"+r_[j]+"_err").html("");  
        }
      
      }
      
      
      if(typeof $("#siteurl").val()!="undefined"){
          var o=$("#siteurl").val()
      }
        var product_id=$("#product_id").val()
     
      
      $.ajax({
          url:u,
          type:"post",
          data: "&name="+name+"&phone="+phone+"&email="+email+"&address="+address+"&pincode="+pincode+"&city="+city+"&state="+state+"&product_id="+product_id,
          beforeSend:function(){
           
          },
          success:function(e){
            
            if(e == "done"){
              
               window.location.href = o+"nutratimes/order_result";
            }else{
              alert("something went wrong")
               window.location.href = o+"nutratimes/quiz";
               
            }
          }
        }
      
      )   


 }