$( document ).ready(function() {

  

  var widthOne = $(".widthOne");
  var widthTwo = $(".widthTwo");

  if(window.innerWidth > 600){

    widthOne.addClass("firstContainer");
    widthTwo.addClass("secondContainer");

  }else{

    widthOne.removeClass("firstContainer");
    widthTwo.removeClass("secondContainer");
    
  }
});



function lenskart_form(){

    var siteurl        = $("#su").val(); 
    var su             = siteurl; 
	var mobile         = $("#mobile").val();
	var city           = $("#city").val();
	var utm_source	   = $("#utm_source").val();
	var utm_medium	   = $("#utm_medium").val();
	var utm_campaign   = $("#utm_campaign").val();
	var u              = su+"lenskart/lenskart_frm";

  	if($.trim($("#city").val()) == "" || $("#city").val() == 0 || $("#city").val() == "undefined"){
	   $("#lenskart_err").html("Please select city");
	   $("#city").css("border-color","rgb(175, 63, 63)").focus();
	   $("#city").focus();
	  
	   return false;	
	}else{
	   $("#lenskart_err").html("");	
	   $("#city").css("border-color","#ccc");
	}

  
    
	if(mobile.length != '10'){
		$("#mobile").css("border-color","rgb(175, 63, 63)").focus();
		$("#lenskart_err").html("Please enter 10 digit mobile number");
		 return false;
	}else{
		$("#mobile").css("border-color","#ccc");
		$("#lenskart_err").html(""); 
	}
	
	if($("#mobile").val().length > 0){
		if(!$("#mobile").val().match(/^[6-9]{1}[0-9]{9}$/)){
		   $("#mobile").css("border-color","rgb(175, 63, 63)").focus();
		   $("#lenskart_err").html("Please enter correct mobile number");
		  return false; 
		}else{
		   $("#mobile").css("border-color","#ccc");
		   $("#lenskart_err").html(""); 
		}  
	} 
    
    $("#otp_mobile").val(mobile);
    $("#otp_mobile_res").val(mobile);

    $.ajax({
			url:u,
			type:"post",
			data: "&phone="+mobile+"&city="+city+"&utm_medium="+utm_medium+"&utm_source="+utm_source+"&utm_campaign="+utm_campaign,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn").val("Submit");
				   
				if(e == "already"){
				   $("#lenskart_err").html("Already registered we will contact you");	
				}else{
				    $('#lenskart_form').trigger('reset'); 
				   	$("#otp_section").show();
				   	$("#lenskart_form").hide();				   

				}
			}
		}
  
    )	  

}


function lenskart_otp(){
	var siteurl       = $("#su").val();
	var su            = siteurl; 
	var otp           = $.trim($("#otp").val());
	var otp_mobile    = $("#otp_mobile").val();
	var u             = su+"lenskart/lnskrt_otpverification";

	if(otp.length != 4 ){
	   $("#otp").css("border-color","rgb(232, 46, 33)").focus();
	   $("#otp_err").html("Please enter OTP");
	   return false;  
	}else{
		 $("#otp").css("border-color","rgb(218, 218, 218)");
		 $("#otp_err").html(""); 
	}
	
	$.ajax({
			url:u,
			type:"post",
			data: "&otp="+otp+"&otp_phone="+otp_mobile,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#otpfrm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				var e = $.trim(e);
				var arr=e.split(',');
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#otpfrm-sbmtbtn").val("Submit");
				   
				if(arr[0] == "resend"){
				   $("#otp_res_err").html("Incorrect OTP");
				   $("#res_id").val(arr[1]);
				   
				}else{
				   window.location.href = su+"lenskart/success";
				   
				}
			}
		})	 
	
}


function lenskart_otp_resend(u){
	
	var otp_mobile    = $("#otp_mobile_res").val();
	
	if(otp_mobile){
		$.ajax({
				url:u,
				type:"post",
				data: "&otp_phone="+otp_mobile,
				beforeSend:function(){
					$("#spinner_loader").show();
					$("#res-sbmtbtn").val("Resending ..");
					$(':input[type="submit"]').prop('disabled', true);
				},
				success:function(e){
					
					$(':input[type="submit"]').prop('disabled', false);
					$("#res-sbmtbtn").val("Click Here");
					 
					if(e == "done"){
					   $("#otp").val("");
					   $("#otp_res_err").html("Incorrect OTP");
					   
					}
					
				}
			})
	}else{
		alert("ss");
	}
	
}
