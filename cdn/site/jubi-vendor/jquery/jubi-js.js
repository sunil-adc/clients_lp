function sliding_modal(show_id, hide_id){   
	
	$("#"+show_id).show();
	//$("#"+show_id).slideUp().show();
	$("#"+hide_id).slideUp("normal", function() { $(this).hide(); } );
}


$(document).ready(function(e) {    
	$(document).on("keydown", ".only_numeric", function(e) {
		46 == e.keyCode || 8 == e.keyCode || 9 == e.keyCode || 27 == e.keyCode || 13 == e.keyCode || 65 == e.keyCode && e.ctrlKey === !0 
		|| e.keyCode >= 35 && e.keyCode <= 39 || (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) 
		&& e.preventDefault()
	})

	
$(function () {
   $('.non_numeric').keydown(function (e) {
	 
	  if (e.ctrlKey ) {
		  e.preventDefault();
	  }else {
		  var key = e.keyCode;
		  if (!((key == 9) || (key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
			 e.preventDefault();
		  }
	  }
   });
});

});


function jubi_jsfrm(u){
  
  var name          = $("#name").val();
  var email         = $("#email").val();
  var phone         = $("#phone").val();
  var pkg           = $("#pkg" ).val();
  var city          = $("#city").val();
  var pb          	= $("#pub").val();
  var ut            = $("#utm").val();
 // var s_date        = $("#get_date").val();
 // var s_time        = $("#srl_time").val();
 // var s_test        = $("#srl_test").val();
 // var addr          = $("#address").val();
  
    
  if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name").css("border-color","rgb(232, 46, 33)").focus();
	 $("#frm_error").html("Please enter correct name").css("background", "#ffffff;");
	 return false;  
  }else{
	 $("#name").css("border-color","rgb(3, 164, 75)");
	 $("#frm_error").html(""); 
  }
  if(phone.length != '10'){
	 $("#phone").css("border-color","rgb(232, 46, 33)").focus();
	 $("#frm_error").html("Please enter 10 digit mobile number").css("background", "#ffffff");
	 return false;
  }else{
	 $("#phone").css("border-color","rgb(3, 164, 75)");
	 $("#frm_error").html(""); 
  }
  
  if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email").css("border-color","rgb(232, 46, 33)").focus();
	 $("#frm_error").html("Please enter correct Email").css("background", "#ffffff");
	 return false;
  }else{
	 $("#email").css("border-color","rgb(3, 164, 75)");
	 $("#frm_error").html(""); 
  }
  
  
  if(pkg == '' || pkg.trim()=='' || pkg==0 ){
	 $("#pkg").css("border-color","rgb(232, 46, 33)").focus();
	 $("#frm_error").html("Please Select Package").css("background", "#ffffff;");
	 return false;
	}else{
	 $("#frm_error").html("");
	}
  if(city == '' || city.trim()=='' ){
	 $("#pkg").css("border-color","rgb(232, 46, 33)").focus();
	 $("#frm_error").html("Please Enter City").css("background", "#ffffff");
	 return false;
	}else{
	 $("#frm_error").html("");
	}	

 	var r_ = new Array("name", "email", "phone", "city", "pkg");
  	for( var i=0; i < r_.length; i++ ){
		if($.trim($("#"+r_[i]).val()) == "" || $("#"+r_[i]).val() == 0 || $("#"+r_[i]).val() == "undefined"){
		   $("#frm_error").html($("#"+r_[i]).attr("data-attr")).css("background", "#ffffff");
		   $("#"+r_[i]).focus();
		   return false;	
		}else{
		   $("#"+r_[i]).css("border-color","rgb(3, 164, 75)");
		   $("#frm_error").html("").css("background", "");
		}
	}


  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&pkg="+pkg+"&ut="+ut+"&pb="+pb,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn1").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn1").val("Submit");
				   
				if(e == "already"){
				   $("#frm_error").html("Already registered we will contact you").css("background", "#ffffff");	
				}else{
				   
				   //sliding_modal('sec_frm', 'first_frm' );
				   $("#first_frm").hide();
				   $("#sec_frm").show().css('display', 'flex');;
				   //$("#div_pix").html(e);
				   
				   //$("#user_info").html("Some error occured try again later");	
				}
			}
		}
  
      )	  
}


function jubi_secfrmjs(u){
    
	var j_date        = $("#jubi_date").val();
	var j_time        = $("#jubi_time").val();
	var j_addr        = $("#address").val();
	var j_pin         = $("#jubi_pincode").val();
	
 	var k_ = new Array("jubi_date", "jubi_time", "address", "jubi_pincode");
  	for( var i=0; i < k_.length; i++ ){
		if($.trim($("#"+k_[i]).val()) == "" || $("#"+k_[i]).val() == 0 || $("#"+k_[i]).val() == "undefined"){
		   $("#frm_error").html($("#"+k_[i]).attr("data-attr")).css("background", "#ffffff");
		   $("#"+k_[i]).css("border-color","rgb(232, 46, 33)").focus();
		   return false;	
		}else{
		   $("#"+k_[i]).css("border-color","rgb(3, 164, 75)");
		   $("#frm_error").html("").css("background", "");	
		}
	}

  $.ajax({
			url:u,
			type:"post",
			data: "&j_date="+j_date+"&j_time="+j_time+"&j_addr="+j_addr+"&j_pin="+j_pin,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn2").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn2").val("Submit");
				   
				if(e == "already"){
				   $("#frm_error").html("Already registered we will contact you").css("background", "#ffffff");	
				}else{
				   
				   //sliding_modal( 'success_msg', 'frm_full');
				   $("#frm_full").hide();
				   $("#success_msg").show();
				   //$("#div_pix").html(e);
				   
				   //$("#user_info").html("Some error occured try again later");	
				}
			}
		}
  
      )	  
}




function chck_valid(ele, msg){
	
	if($.trim($("#"+ele).val()) == ""  ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#"+ele+"_err").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#"+ele+"_err").html(""); 
  }
	
}


function make_active(show_id){
	 
	$("li").removeClass("active");
	$("#"+show_id).addClass("active");
	 
}

function slide_focus(s){
    
	$("#pkg" ).val(s);
	$("li").removeClass("active");
	$("#l"+s).addClass("active");
	//$("#tab"+s).show();
	var ln = ($('.tabname').offset().top - 20 )
	$("html, body").animate({ scrollTop: ln }, 1000);
}