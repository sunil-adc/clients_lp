$(document).ready(function () {
	setTimeout(function () {
		if ($('.popup-enquiry-form').length) {
			$.magnificPopup.open({
				items: {
					src: '#popupForm'
				},
				type: 'inline'
			});
		}
	}, 15000);
	$('.slick-slider').slick({
		dots: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true
	});
	$(function () {
		$('.gallery-grid1 a').Chocolat();
	});
});
$(document).ready(function () {

	var  _floatbox = $("#contact_form"), _floatbox_opener = $(".contact-opener");
	_floatbox.css("right", "0px"); //initial contact form position
	_floatbox.css("display", "none");
	//Contact form Opener button
	_floatbox_opener.click(function () {
		if (_floatbox.hasClass('visiable')) {
			_floatbox.animate({ "right": "-300px" }, { duration: 300 }).removeClass('visiable');		
		} else {
			_floatbox.animate({ "right": "0px" }, { duration: 300 }).addClass('visiable');			
		}
	});
	_floatbox.animate({ "top": "30px" }, { easing: "linear" }, { duration: 500 });
});
var $window = $(window);
var nav = $('#contact_form');
$window.scroll(function(){
    if ($window.scrollTop() <= 500) {
       nav.css("display", "none");
    }
    else {
		nav.css("display", "block");
    }
});
