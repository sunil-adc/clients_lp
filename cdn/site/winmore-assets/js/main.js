$(document).ready(function () {

	toggle_nav_container();
	gotoByScroll();
	$(".phone").intlTelInput({
		utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/js/utils.js",
		separateDialCode: "true",
		initialCountry: "IN",
	});
	$(function () {
		$('.gallery-grid1 a').Chocolat();
	});
	$(".phone").on("countrychange", function (e, countryData) {
		$(".hiddenCountry").val(countryData['dialCode']);
	});
	$().UItoTop({
		easingType: 'easeOutQuart'
	});
	$(".scroll").click(function (event) {
		event.preventDefault();

		$('html,body').animate({
			scrollTop: $(this.hash).offset().top
		}, 1000);
	});
	$("#owl-demo").owlCarousel({
		items: 1,
		lazyLoad: true,
		autoPlay: false,
		navigation: true,
		navigationText: true,
		pagination: true,
	});
	$('.cm-overlay').cmOverlay();
	$('.time_to_call').select2();

		 

});


function gradechange(i){
	
	var campus =$("#campus"+i).val();
	if (campus != "whitefield") {
		$("#grade"+i+" option").eq(1).before($("<option></option>").val("13").text("Playschool"));
		$('#grade'+i+' option[value="8"]').remove();
		$('#grade'+i+' option[value="9"]').remove();
	}
	else{
		$('#grade'+i+' option[value="13"]').remove();
	}
}

var toggle_nav_container = function () {



	var $toggleButton = $('#toggle_m_nav');
	$navContainer = $('#m_nav_container');
	$menuButton = $('#m_nav_menu')
	$menuButtonBars = $('.m_nav_ham');
	$wrapper = $('#wrapper');

	// toggle the container on click of button (can be remapped to $menuButton)

	$toggleButton.on("click", function () {

		// declare a local variable for the window width
		var $viewportWidth = $(window).width();

		// if statement to determine whether the nav container is already toggled or not

		if ($navContainer.is(':hidden')) {
			$wrapper.removeClass('closed_wrapper');
			$wrapper.addClass("open_wrapper");
			$navContainer.slideDown(200).addClass('container_open').css("z-index", "2");
			// $(window).scrollTop(0);
			$menuButtonBars.removeClass('button_closed');
			$menuButtonBars.addClass('button_open');
			$("#m_ham_1").addClass("m_nav_ham_1_open");
			$("#m_ham_2").addClass("m_nav_ham_2_open");
			$("#m_ham_3").addClass("m_nav_ham_3_open");

		}
		else {
			$navContainer.css("z-index", "0").removeClass('container_open').slideUp(200)
			$menuButtonBars.removeClass('button_open')
			$menuButtonBars.addClass('button_closed')
			$wrapper.removeClass('open_wrapper')
			$wrapper.addClass("closed_wrapper")
			$("#m_ham_1").removeClass("m_nav_ham_1_open");
			$("#m_ham_2").removeClass("m_nav_ham_2_open");
			$("#m_ham_3").removeClass("m_nav_ham_3_open");

		}
	});



}


// Function that takes the href value of links in the navbar and then scrolls 
//the div on the page whose ID matches said value. This only works if you use 
//a consistent naming scheme for the navbar anchors and div IDs

var gotoByScroll = function () {

	$(".m_nav_item a").on("click", function (e) {



		$('html,body').animate({
			scrollTop: $($(this).attr("href")).offset().top - 50
		}, "slow");

	});




}





$(document).ready(function () {
	setTimeout(function () {
		if ($('.popup-enquiry-form').length) {
			$.magnificPopup.open({
				items: {
					src: '#popupForm'
				},
				type: 'inline'
			});
		}
	}, 15000);
});



$(document).ready(function () {

	var  _floatbox = $("#contact_form"), _floatbox_opener = $(".contact-opener");
	_floatbox.css("right", "0px"); //initial contact form position
	_floatbox.css("display", "none");
	//Contact form Opener button
	_floatbox_opener.click(function () {
		if (_floatbox.hasClass('visiable')) {
			_floatbox.animate({ "right": "-300px" }, { duration: 300 }).removeClass('visiable');		
		} else {
			_floatbox.animate({ "right": "0px" }, { duration: 300 }).addClass('visiable');			
		}
	});
	_floatbox.animate({ "top": "30px" }, { easing: "linear" }, { duration: 500 });
});
var $window = $(window);
var nav = $('#contact_form');
$window.scroll(function(){
    if ($window.scrollTop() <= 500) {
       nav.css("display", "none");
    }
    else {
		nav.css("display", "block");
    }
});