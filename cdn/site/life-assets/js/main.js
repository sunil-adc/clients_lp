
$(document).ready(function () {
    $('.jarallax').jarallax({
        speed: 0.5,
        imgWidth: 1366,
        imgHeight: 768
    })
    /*
        var defaults = {
        containerID: 'toTop', // fading element id
        containerHoverID: 'toTopHover', // fading element hover id
        scrollSpeed: 1200,
        easingType: 'linear' 
        };
    */
   $("#testimonial-slider").owlCarousel({
    items:1,
    itemsDesktop:[1000,1],
    itemsDesktopSmall:[979,1],
    itemsTablet:[768,1],
    pagination:true,
    navigation:false,
    navigationText:["",""],
    slideSpeed:1000,
    singleItem:true,
    autoPlay:true
});
    $().UItoTop({ easingType: 'easeOutQuart' });
    $(".scroll").click(function (event) {
        event.preventDefault();
        $('html,body').animate({ scrollTop: $(this.hash).offset().top }, 1000);
    });
    
    $('#datetimepicker').datetimepicker({
        format: 'DD/MM/YYYY'
         });
});