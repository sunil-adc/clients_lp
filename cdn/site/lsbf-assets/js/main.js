$(document).ready(function () {
	$(function () {
		$('.gallery-grid1 a').Chocolat();
	});
	$('.skillbar').skillBars({
		from: 0,
		speed: 4000,
		interval: 100,
		decimals: 0,
	});
	$(".scroll").click(function (event) {
		event.preventDefault();

		$('html,body').animate({
			scrollTop: $(this.hash).offset().top
		}, 1000);
	});
	$().UItoTop({
		easingType: 'easeOutQuart'
	});
	$(".phone").intlTelInput({
		utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/js/utils.js",
		separateDialCode: "true",
		initialCountry: "IN",

	});
	$(".phone").on("countrychange", function (e, countryData) {
		$(".hiddenCountry").val(countryData['dialCode']);
	});
	$('.time_to_call').select2();
});
$(window).load(function () {
	$('.flexslider').flexslider({
		animation: "slide",
		start: function (slider) {
			$('body').removeClass('loading');
		}
	});
});
addEventListener("load", function () {
	setTimeout(hideURLbar, 0);
}, false);

function hideURLbar() {
	window.scrollTo(0, 1);
}

$(document).ready(function () {
	setTimeout(function () {
		if ($('.popup-enquiry-form').length) {
			$.magnificPopup.open({
				items: {
					src: '#popupForm'
				},
				type: 'inline'
			});
		}
	}, 15000);
});



$(document).ready(function () {

	var _floatbox = $("#contact_form"), _floatbox_opener = $(".contact-opener");
	_floatbox.css("right", "0px"); //initial contact form position
	_floatbox.css("display", "none");
	//Contact form Opener button
	_floatbox_opener.click(function () {
		if (_floatbox.hasClass('visiable')) {
			_floatbox.animate({ "right": "-300px" }, { duration: 300 }).removeClass('visiable');
		} else {
			_floatbox.animate({ "right": "0px" }, { duration: 300 }).addClass('visiable');
		}
	});
	_floatbox.animate({ "top": "30px" }, { easing: "linear" }, { duration: 500 });
});
var $window = $(window);
var nav = $('#contact_form');
$window.scroll(function () {
	if ($window.scrollTop() <= 500) {
		nav.css("display", "none");
	}
	else {
		nav.css("display", "block");
	}
});

$(document).ready(function () {
	var data = [
		{	
			"id": 1,
			"course_name": "Diploma in Data Analytics - Full Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The main aim of the Diploma in Data Analytics is to give students the knowledge and practical insight into the workings of the business analytics industry. In particular, students will get an insight into how big data is managed and useful information extracted to better make informed decisions.",
			"course_career_path": "Data Analytics"
		},
		{
			"id": 2,
			"course_name": "Diploma in Data Analytics - Part Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The main aim of the Diploma in Data Analytics is to give students the knowledge and practical insight into the workings of the business analytics industry. In particular, students will get an insight into how big data is managed and useful information extracted to better make informed decisions.",
			"course_career_path": "Data Analytics"
		},
		{
			"id": 3,
			"course_name": "Foundation Diploma in Business Studies - Full Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Foundation Diploma in Business Studies has been developed by London School of Business and Finance as an initial qualification for students who have completed their O-Level and/or high school education. Graduates of the course will be able to demonstrate a sound knowledge of basic business concepts, and will be eligibile to progress into the Diploma programme.",
			"course_career_path": "Business"
		},
		{
			"id": 4,
			"course_name": "Foundation Diploma in Business Studies - Part Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Foundation Diploma in Business Studies has been developed by London School of Business and Finance as an initial qualification for students who have completed their O-Level and/or high school education. Graduates of the course will be able to demonstrate a sound knowledge of basic business concepts, and will be eligibile to progress into the Diploma programme.",
			"course_career_path": "Business"
		},
		{
			"id": 5,
			"course_name": "Diploma in Business Studies - Full Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "Graduates of the course will be able to demonstrate a sound knowledge of the basic underlying academic concepts and principles associated with the broader discipline of Management. The credits gained by achieving the Diploma in Business Studies would also enable progression to a suitable Advanced Diploma in Business Studies, and the skills learned would equip students for entry to a career in industry.",
			"course_career_path": "Business"
		},
		{
			"id": 6,
			"course_name": "Diploma in Business Studies - Part Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "Graduates of the course will be able to demonstrate a sound knowledge of the basic underlying academic concepts and principles associated with the broader discipline of Management. The credits gained by achieving the Diploma in Business Studies would also enable progression to a suitable Advanced Diploma in Business Studies, and the skills learned would equip students for entry to a career in industry.",
			"course_career_path": "Business"
		},
		{
			"id": 7,
			"course_name": "Advanced Diploma in Business Studies - Full Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Advanced Diploma in Business Studies aims to provide students with the ability to apply underlying concepts and principles of business and management",
			"course_career_path": "Business"
		},
		{
			"id": 8,
			"course_name": "Advanced Diploma in Business Studies - Part Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Advanced Diploma in Business Studies aims to provide students with the ability to apply underlying concepts and principles of business and management",
			"course_career_path": "Business"
		},
		{
			"id": 9,
			"course_name": "Higher Diploma in Business Studies",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Higher Diploma in Business Studies aims to provide students with the ability to analyse strategic issues apply underlying concepts and principles of business and management.",
			"course_career_path": "Business"
		},
		{
			"id": 10,
			"course_name": "Diploma in Banking & Finance - Full Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Diploma in Banking and Finance aims to provide students with the relevant knowledge and understanding of banking and finance as it relates to the wider business context, and to develop students’ competence and practical skills in preparation for a career in the banking and finance industry.",
			"course_career_path": "Banking, Finance, Business"
		},
		{
			"id": 11,
			"course_name": "Diploma in Banking & Finance - Part Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Diploma in Banking and Finance aims to provide students with the relevant knowledge and understanding of banking and finance as it relates to the wider business context, and to develop students’ competence and practical skills in preparation for a career in the banking and finance industry.",
			"course_career_path": "Banking, Finance, Business"
		},
		{
			"id": 12,
			"course_name": "Diploma in Law - Full Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The main aim of the Diploma in Law is to give students the legal knowledge and practical insight into the workings of the legal industry. In particular, students will get an insight into the workings of law firms and the role that they can play in law firms across Singapore",
			"course_career_path": "Law"
		},
		{
			"id": 13,
			"course_name": "Diploma in Law - Part Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The main aim of the Diploma in Law is to give students the legal knowledge and practical insight into the workings of the legal industry. In particular, students will get an insight into the workings of law firms and the role that they can play in law firms across Singapore",
			"course_career_path": "Law"
		},
		{
			"id": 14,
			"course_name": "Diploma in Logistics & Supply Chain Management - Full Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Diploma in Logistics & Supply Chain Management aims to provide students with an understanding of the core concepts, theories and techniques in the Logistics & Supply Chain Management field. Graduates of the course will be able to demonstrate competence and high degree of effectiveness within the industry by providing a wide range of relevant knowledge and skills to complement their experience in their scope of work through a 6 months industrial attachment programme.",
			"course_career_path": "Logistics"
		},
		{
			"id": 15,
			"course_name": "Diploma in Logistics & Supply Chain Management - Part Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Diploma in Logistics & Supply Chain Management aims to provide students with an understanding of the core concepts, theories and techniques in the Logistics & Supply Chain Management field. Graduates of the course will be able to demonstrate competence and high degree of effectiveness within the industry by providing a wide range of relevant knowledge and skills to complement their experience in their scope of work through a 6 months industrial attachment programme.",
			"course_career_path": "Logistics"
		},
		{
			"id": 16,
			"course_name": "Advanced Diploma in Logistics & Supply Chain Management - Full Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Advanced Diploma in Logistics & Supply Chain Management aims to provide students with an understanding of the core concepts, theories and techniques in the Logistics & Supply Chain Management field.<br><br>Graduates of the course will be able to demonstrate competence and high degree of effectiveness within the industry by providing a wide range of relevant knowledge and skills to complement their experience in their scope of work through a 6 months industrial attachment programme.",
			"course_career_path": "Logistics"
		},
		{
			"id": 17,
			"course_name": "Advanced Diploma in Logistics & Supply Chain Management - Part Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Advanced Diploma in Logistics & Supply Chain Management aims to provide students with an understanding of the core concepts, theories and techniques in the Logistics & Supply Chain Management field.<br><br>Graduates of the course will be able to demonstrate competence and high degree of effectiveness within the industry by providing a wide range of relevant knowledge and skills to complement their experience in their scope of work through a 6 months industrial attachment programme.",
			"course_career_path": "Logistics"
		},
		{
			"id": 18,
			"course_name": "Higher Diploma in Logistics & Supply Chain Management - Full time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Higher Diploma in Logistics & Supply Chain Management aims to provide students with an understanding of the core concepts, theories and techniques in the Logistics & Supply Chain Management field.<br><br>Graduates of the course will be able to demonstrate competence and high degree of effectiveness within the industry by providing a wide range of relevant knowledge and skills.",
			"course_career_path": "Logistics"
		},
		{
			"id": 19,
			"course_name": "Diploma in Accounting & Finance - Full Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Diploma in Accounting and Finance aims to develop students’ competence and practical skills in accounting and to provide students with the relevant knowledge and understanding of accounting and finance as it relates to the wider business context.",
			"course_career_path": "Accounting, Finance"
		},
		{
			"id": 20,
			"course_name": "Diploma in Accounting & Finance - Part Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Diploma in Accounting and Finance aims to develop students’ competence and practical skills in accounting and to provide students with the relevant knowledge and understanding of accounting and finance as it relates to the wider business context.",
			"course_career_path": "Accounting, Finance"
		},
		{
			"id": 21,
			"course_name": "Advanced Diploma in Accounting & Finance - Full Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Advanced Diploma in Accounting and Finance aims to provide students with the relevant knowledge and understanding of accounting and finance as it relates to the wider business context that will enable them to follow a career in all areas of accounting.",
			"course_career_path": "Accounting, Finance"
		},
		{
			"id": 22,
			"course_name": "Advanced Diploma in Accounting & Finance - Part Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Advanced Diploma in Accounting and Finance aims to provide students with the relevant knowledge and understanding of accounting and finance as it relates to the wider business context that will enable them to follow a career in all areas of accounting.",
			"course_career_path": "Accounting, Finance"
		},
		{
			"id": 23,
			"course_name": "Higher Diploma in Accounting & Finance",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "Graduates of the course will be able to show an understanding of accounting and finance principles and practices in the context of their chosen specialisation, and demonstrate proficiency in quantitative methods and computing techniques and know how to use these techniques and methods effectively across a range of problems",
			"course_career_path": "Accounting, Finance"
		},
		{
			"id": 24,
			"course_name": "Diploma in International Hospitality Management - Full time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Diploma in International Hospitality Management aims to provide students with the skills to identify and understand the techniques and theories in the hospitality environment. Students will also attend a 6 months industrial attachment to develop the professional skills that is required in the industry.",
			"course_career_path": "Hospitality"
		},
		{
			"id": 25,
			"course_name": "Diploma in International Hospitality Management - Part time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Diploma in International Hospitality Management aims to provide students with the skills to identify and understand the techniques and theories in the hospitality environment. Students will also attend a 6 months industrial attachment to develop the professional skills that is required in the industry.",
			"course_career_path": "Hospitality"
		},
		{
			"id": 26,
			"course_name": "Diploma in Applied Hospitality Skills - Full time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Diploma in Applied Hospitality Management aims to provide students with the skills to identify and understand the techniques and theories in the hospitality environment.<br><br>Students will also attend a 6 months industrial attachment to develop the professional skills that is required in the industry.",
			"course_career_path": "Hospitality"
		},
		{
			"id": 27,
			"course_name": "Diploma in Hospitality and Tourism Management - Part time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Diploma in Hospitality and Tourism Management aims to provide students with the skills to identify and understand the techniques and theories in the hospitality environment.",
			"course_career_path": "Hospitality"
		},
		{
			"id": 28,
			"course_name": "Advanced Diploma in Hospitality and Tourism Management - Full time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Advanced Diploma in Hospitality and Tourism Management aims to provide students with the necessary skills, knowledge to be immediately effective within the hospitality and tourism industry. Students will also attend a 6 months industrial attachment to develop the professional skills that is required in the industry.",
			"course_career_path": "Hospitality"
		},
		{
			"id": 29,
			"course_name": "Advanced Diploma in Hospitality and Tourism Management - Part time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Advanced Diploma in Hospitality and Tourism Management aims to provide students with the necessary skills, knowledge to be immediately effective within the hospitality and tourism industry. Students will also attend a 6 months industrial attachment to develop the professional skills that is required in the industry.",
			"course_career_path": "Hospitality"
		},
		{
			"id": 30,
			"course_name": "Higher Diploma in Hospitality and Tourism Management - Full time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Higher Diploma in Hospitality and Tourism Management aims to focus on the latest development in the hospitality industry. It will help the students to identify and understand the techniques and theory implied in the hospitality environment through theory lessons and practical experience. Students are given emphasis on developing professional hotel competencies such as effective communication skills, good customer service skills, leadership, team work and other relevant professionalism topics related to the industry. Students will also attend a 6 months industrial attachment to develop the professional skills that is required in the industry.",
			"course_career_path": "Hospitality"
		},
		{
			"id": 31,
			"course_name": "Bachelor of Science (Honours) Banking Practice and Management",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The BSc Banking Practice and Management degree is designed to cater for a variety of different requirements including those starting out in a management career in financial services, particularly in retail and commercial banking. It provides essential core knowledge of the industry, the external environment in which it operates and the interaction between the two.",
			"course_career_path": "Banking, Finance"
		},
		{
			"id": 32,
			"course_name": "Bachelor of Business (Management & Innovation) (Top-Up)",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Bachelor of Business (Management & Innovation) has been developed by Victoria University, Australia in conjunction with LSBF, Singapore. It provides an opportunity for current Higher and Advanced Diploma graduates in Business Studies to further their studies with Victoria University at LSBF Singapore.",
			"course_career_path": "Management"
		},
		{
			"id": 33,
			"course_name": "Bachelor of Business (Accounting) (Top-Up)",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Bachelor of Business (Accounting) has been developed by Victoria University, Australia in conjunction with LSBF, Singapore. It provides an opportunity for current Advanced and Higher Diploma graduates in Accounting & Finance of LSBF to further their studies with Victoria University at LSBF Singapore.<br><br>VU's Accounting major within BBNS is accredited by CPA Australia, one of the world's largest accounting bodies, and Chartered Accountants Australia and New Zealand (CA ANZ), a worldwide network of finance leaders.",
			"course_career_path": "Accounting"
		},
		{
			"id": 34,
			"course_name": "Bachelor of Business (Financial Risk Management) (Top-Up)",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Bachelor of Business (Financial Risk Management) has been developed by Victoria University, Australia in conjunction with LSBF, Singapore. It provides an opportunity for current Advanced and Higher Diploma graduates in Accounting & Finance to further their studies with Victoria University at LSBF Singapore.",
			"course_career_path": "Finance"
		},
		{
			"id": 35,
			"course_name": "Higher Diploma in Logistics & Supply Chain Management - Part time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Higher Diploma in Logistics & Supply Chain Management aims to provide students with an understanding of the core concepts, theories and techniques in the Logistics & Supply Chain Management field.<br><br>Graduates of the course will be able to demonstrate competence and high degree of effectiveness within the industry by providing a wide range of relevant knowledge and skills.",
			"course_career_path": "Logistics"
		},
		{
			"id": 36,
			"course_name": "Bachelor of Business - Supply Chain & Logistics Management (Top-Up) - Full time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Bachelor of Business - Supply Chain & Logistics Management (Top-up) has been developed by Victoria University, Australia in conjunction with LSBF, Singapore.<br><br>It provides an opportunity for current Higher and Advanced Diploma graduates in Logistics and Supply Chain Management to further their studies with Victoria University at LSBF Singapore.",
			"course_career_path": "Logistics"
		},
		{
			"id": 37,
			"course_name": "Bachelor of Business - Supply Chain & Logistics Management (Top-Up) - Part time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "The Bachelor of Business (Supply Chain & Logistics Management (Top-up)) has been developed by Victoria University, Australia in conjunction with LSBF, Singapore. It provides an opportunity for current Higher and Advanced Diploma graduates in Logistics and Supply Chain Management to further their studies with Victoria University at LSBF Singapore.",
			"course_career_path": "Logistics"
		},
		{
			"id": 38,
			"course_name": "Bachelor of Arts (Hons) Accounting and Finance - Full-Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "UoG's BA (Hons) in Accounting and Finance equips you with financial and management accounting skills, enabling you to understand how they affect planning, decision-making, and budget control.",
			"course_career_path": "Accounting & Finance"
		},
		{
			"id": 39,
			"course_name": "Bachelor of Arts (Hons) Accounting and Finance - Part-Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "UoG's BA (Hons) in Accounting and Finance equips you with financial and management accounting skills, enabling you to understand how they affect planning, decision-making, and budget control.",
			"course_career_path": "Accounting & Finance"
		},
		{
			"id": 40,
			"course_name": "Advanced Diploma in Data Analytics - Full Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "This course is designed to guide participants to use data science tools to perform data analytics and data science techniques on a variety of data types and cyber-based fields.",
			"course_career_path": "Data Analytics"
		},
		{
			"id": 41,
			"course_name": "Advanced Diploma in Data Analytics - Part Time",
			"course_type": "Undergraduate",
			"course_location": "Singapore",
			"course_description": "This course is designed to guide participants to use data science tools to perform data analytics and data science techniques on a variety of data types and cyber-based fields.",
			"course_career_path": "Data Analytics"
		}
	]

	var htmlText = '';

	for (var key in data) {
		htmlText += ' <div class="panel panel-default">';
			htmlText += ' <span class="side-tab" data-target="#tab1" data-toggle="tab" role="tab" aria-expanded="false">';
				htmlText += ' <div class="panel-heading collapsed" role="tab" id="headingOne" data-toggle="collapse" data-parent="#accordion" href="#tab' + data[key].id + '" aria-expanded="true" aria-controls="collapseOne">';
					htmlText += ' <h4 class="panel-title">' + data[key].course_name + '</h4>';
					htmlText += ' <h4>' + data[key].course_type + '</h4>';
					htmlText += ' <h4>' + data[key].course_location + '</h4>';
					htmlText += ' <span class="btn"><span class="more">View More</span><span class="less">View Less</span></span>'
				htmlText += ' </div>';
			htmlText += ' </span>';
			htmlText += ' <div id="tab' + data[key].id + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne"><div class="panel-body"><div class="details">';
				htmlText += '<h6>Course Details</h6>';
				htmlText += '<p>' + data[key].course_description + '</p>';
				htmlText += '<h6>Career Path</h6>';
				htmlText += '<p>' + data[key].course_career_path + '</p>';
			htmlText += '</div></div></div>';
		htmlText += '</div>';

	}

	$('.course-list-accoedian').append(htmlText);
});
