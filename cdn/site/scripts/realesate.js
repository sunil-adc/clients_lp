function puravankara_jsfrm_purvaamaiti(u,s,i){
 
 
  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var city          = '';
  var time_to_call  = '';
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
   var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
     if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please select country");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
	if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	

	
 	var r_ = new Array( "name", "phone", "email","CountryCode");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara/success_"+s;
				   
				}
			}
		}
  
      )	    
}

function puravankara_jsfrm_freedom(u,s,i){
   
  var name          = $("#name"+i).val();
  var phone         = $("#phone"+i).val();
  var email         = $("#email"+i).val();
  var time_to_call  = $("#time_to_call"+i).val();
  var country       = $("#CountryCode"+i).val();
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var city          = '';
  var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
	
    if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please Select Country Code");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
	
	
		if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	
	
	if($.trim(time_to_call) == "" || time_to_call == 0 ){
		$("#time_to_call"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#time_to_call_err"+i).html("Please Select Preferred Time To Call");
	 return false;
	}else{
		$("#time_to_call"+i).css("border-color","rgb(218, 218, 218)");
		$("#time_to_call_err"+i).html(""); 
	}
	
 	var r_ = new Array( "name", "phone", "email","time_to_call");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 // alert(r_[i]);
		  // return false;	
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign=" +utm_campaign,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				    /*$("#otp_email"+i).val(email);
				    $("#form_div"+i).hide();
				   	$("#otp_section"+i).show(); 
				   	if(i != 3){
				   		$("body").addClass("otp_enabled");
				   	}*/
				   window.location.href = o+"realestate/puravankara/success_"+s;
				   
				}
			}
		}
  
      )	  
}

function puravankara_jsfrm_skydale(u,s,i){
  
	  var name          = $("#name"+i).val();
	  var country       = $("#CountryCode"+i).val();
	  var phone         = $("#phone"+i).val();
	  var email         = $("#email"+i).val();
	  var time_to_call  = '';
	  var utm_source1   = $("#utm_source1").val();
	  var utm_source2	= $("#utm_source2").val();
	  var utm_source3	= $("#utm_source3").val();
	  var city          = '';
	  var utm_campaign	= $("#utm_campaign").val();
	 // var utm_camp2	     = $("#utm_camp2").val();
	 // var utm_mobile1	 = $("#utm_mobile1").val();
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
		 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#name_err"+i).html("Please enter correct name");
		 return false;  
		}else{
			 $("#name"+i).css("border-color","rgb(218, 218, 218)");
			 $("#name_err"+i).html(""); 
		}
		
		
		
		if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
		 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#email_err"+i).html("Please enter correct Email");
		 return false;
		}else{
			 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
			 $("#email_err"+i).html(""); 
		}
	 
	    
			if(phone.length < '6' || phone.length > '13'){
			 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
			 $("#phone_err"+i).html("Please enter correct phone number");
			 return false;
		}else{
			 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
			 $("#phone_err"+i).html(""); 
		}
	  
	
	 	var r_ = new Array( "name", "phone", "email");
	  	for( var j=0; j < r_.length; j++ ){
			if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
			   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
			   $("#"+r_[j]+i).focus();
			 // alert(r_[i]);
			  // return false;	
			}else{
			   $("#"+r_[j]+"_err"+i).html("");	
			}
		}
		
		$("#otp_phone").val(phone);
		$("#otp_email").val(email);
		
		if(typeof $("#siteurl").val()!="undefined"){
				var o=$("#siteurl").val()
			}
		
	  $.ajax({
				url:u,
				type:"post",
				data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
				beforeSend:function(){
					$("#spinner_loader").show();
					$("#frm-sbmtbtn"+i).val("Processing..");
					$(':input[type="submit"]').prop('disabled', true);
				},
				success:function(e){
					//alert(e);return false;
					$(':input[type="submit"]').prop('disabled', false);
					$("#frm-sbmtbtn"+i).val("Submit");
					   
					if(e == "already"){
					   $("#email_err").html("Already registered we will contact you");	
					}else{
					   
					   
					   window.location.href = o+"realestate/puravankara/success_"+s;
					   
					}
				}
			}
	  
	      )	  
}

function puravankara_jsfrm_bluemont(u,s,i){
   
  var name          = $("#name"+i).val();
  var phone         = $("#phone"+i).val();
  var email         = $("#email"+i).val();
  var city          = '';
  var country       = $("#country"+i).val();
  var time_to_call  = '';
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
	
	if($.trim(country) == "" || country == 0 ){
		$("#country"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#country_err"+i).html("Please select country");
	 return false;
	}else{
		$("#country"+i).css("border-color","rgb(218, 218, 218)");
		$("#country_err"+i).html(""); 
	}
 
		if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	
	
	
 	var r_ = new Array( "name", "phone", "email","country");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 // alert(r_[i]);
		  // return false;	
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara/success_"+s;
				   
				}
			}
		}
  
      )	  
}

function puravankara_jsfrm_adoradegoa(u,s,i){
 
  var name          = $("#name"+i).val();
  var phone         = $("#phone"+i).val();
  var email         = $("#email"+i).val();
  var time_to_call  = '';
  var country       = $("#CountryCode"+i).val();
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var city          = '';
  var utm_campaign	= $("#utm_campaign").val();
  
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

 
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
	
	if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please Select Country Code");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
	
		if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  
	
	 
	if($.trim(time_to_call) == "" || time_to_call == 0 ){
		$("#time_to_call"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#time_to_call_err"+i).html("Please Select Preferred Time To Call");
	 return false;
	}else{
		$("#time_to_call"+i).css("border-color","rgb(218, 218, 218)");
		$("#time_to_call_err"+i).html(""); 
	}*/
	
	
	
 	var r_ = new Array( "name", "phone", "email");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 // alert(r_[i]);
		  // return false;	
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign=" +utm_campaign,
			beforeSend:function(){
				//alert(i);
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
			
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				   window.location.href = o+"realestate/puravankara/success_"+s;
				   
				}
			}
		}
  
      )	  
}



function chck_valid_puravankara(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#"+ele+"_err").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#"+ele+"_err").html(""); 
  }
	
}


function puravankara_jsfrm_providentsunworth(u,s,i){
 
  var name          = $("#name"+i).val();
  var phone         = $("#phone"+i).val();
  var email         = $("#email"+i).val();
  var time_to_call  = $("#time_to_call"+i).val();
  var country       = $("#CountryCode"+i).val();
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var city          = '';
 var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
	if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please Select Country Code");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
		if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	
	 
	if($.trim(time_to_call) == "" || time_to_call == 0 ){
		$("#time_to_call"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#time_to_call_err"+i).html("Please Select Preferred Time To Call");
	 return false;
	}else{
		$("#time_to_call"+i).css("border-color","rgb(218, 218, 218)");
		$("#time_to_call_err"+i).html(""); 
	}
	
	
	
 	var r_ = new Array( "name", "phone", "email","time_to_call");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 // alert(r_[i]);
		  // return false;	
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
	}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err").html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara/success_"+s;
				   
				}
			}
		}
  
      )	   
}


function puravankara_jsfrm_welworth_city(u,s,i){
  
  var name          = $("#name"+i).val();
  var phone         = $("#phone"+i).val();
  var email         = $("#email"+i).val();
  var time_to_call  = $("#time_to_call"+i).val();
  var country       = $("#CountryCode"+i).val();
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var city          = '';
  var utm_campaign	= $("#utm_campaign").val();
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
	
		if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	
	 
	if($.trim(time_to_call) == "" || time_to_call == 0 ){
		$("#time_to_call"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#time_to_call_err"+i).html("Please Select Preferred Time To Call");
	 return false;
	}else{
		$("#time_to_call"+i).css("border-color","rgb(218, 218, 218)");
		$("#time_to_call_err"+i).html(""); 
	}
	
	
	
 	var r_ = new Array( "name", "phone", "email","time_to_call");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 // alert(r_[i]);
		  // return false;	
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
	}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err").html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara/success_"+s;
				   
				}
			}
		}
  
      )	   
}


 
function puravankara_jsfrm_raysofdawn(u,s,i){
  var name           = $("#name"+i).val();
  var phone         = $("#phone"+i).val();
  var email         = $("#email"+i).val();
  var time_to_call  = $("#time_to_call"+i).val();
  var country       = $("#CountryCode"+i).val();
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var city          = '';
  var utm_campaign	= $("#utm_campaign").val();
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
	
		if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	
	 
	if($.trim(time_to_call) == "" || time_to_call == 0 ){
		$("#time_to_call"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#time_to_call_err"+i).html("Please Select Preferred Time To Call");
	 return false;
	}else{
		$("#time_to_call"+i).css("border-color","rgb(218, 218, 218)");
		$("#time_to_call_err"+i).html(""); 
	}
	
	
	
 	var r_ = new Array( "name", "phone", "email","time_to_call");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 // alert(r_[i]);
		  // return false;	
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
	}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err").html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara_provident/"+s+"_success";
				   
				}
			}
		}
  
      )	   
   
}

function puravankara_jsfrm_palmbeach(u,s,i){

  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var city          = '';
  var time_to_call  = '';
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
   var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
     if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please select country");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
	if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	
	
 	var r_ = new Array( "name", "phone", "email","CountryCode");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				    
				    /*
				    $("#otp_email"+i).val(email);
				    $("#form_div"+i).hide();
				   	$("#otp_section"+i).show(); 
				   	if(i != 3){
				   		$("body").addClass("otp_enabled");
				   	}*/
				    window.location.href = o+"realestate/puravankara_provident/"+s+"_success";
				   
				}
			}
		}
  
      )	    
   
}


function puravankara_jsfrm_silversands(u,s,i){

  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var city          = '';
  var time_to_call  = '';
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
   var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
     if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please select country");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
		if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	
	
 	var r_ = new Array( "name", "phone", "email","CountryCode");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara_provident/"+s+"_success";
				   
				}
			}
		}
  
      )	    
   
}

//parksquare
function puravankara_jsfrm_parksquare(u,s,i){

  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var city          = '';
  var time_to_call  = '';
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
 var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
     if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please select country");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
		if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	
	
 	var r_ = new Array( "name", "phone", "email","CountryCode");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				    /*$("#otp_email"+i).val(email);
				    $("#form_div"+i).hide();
				   	$("#otp_section"+i).show();
				   		if(i != 3){
				   		$("body").addClass("otp_enabled");
				   	}*/
				    window.location.href = o+"realestate/puravankara_provident/"+s+"_success";
				   
				}
			}
		}
  
      )	    
   
}


function puravankara_jsfrm_westend(u,s,i){
//alert(i);return false;
  var name          = $("#name"+i).val();
  var phone         = $("#phone"+i).val();
  var email         = $("#email"+i).val();
  var city          = '';
  var time_to_call  = '';
  var country       = $("#CountryCode"+i).val();;
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
	
    if($.trim(country) == "" || country == 0 ){
		
		$("#country"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#country_err"+i).html("Please select country");
	 return false;
	}else{
		$("#country"+i).css("border-color","rgb(218, 218, 218)");
		$("#country_err"+i).html(""); 
	}
		if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	} 
	 */
	
 	var r_ = new Array( "name", "phone", "email","CountryCode");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara_provident/"+s+"_success";
				   
				}
			}
		}
  
      )	    
   
}

//
function puravankara_jsfrm_swanlake(u,s,i){
//alert(i);return false;
  var name          = $("#name"+i).val();
  var phone         = $("#phone"+i).val();
  var email         = $("#email"+i).val();
  var city          = '';
  var time_to_call  = '';
  var country       = $("#CountryCode"+i).val();
  //var recaptcha     = $("#RecaptchaField"+i).val();
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();


    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
	
	if($.trim(country) == "" || country == 0 ){
		
		$("#country"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#CountryCode_err"+i).html("Please select country");
	 return false;
	}else{
		$("#country"+i).css("border-color","rgb(218, 218, 218)");
		$("#CountryCode_err"+i).html(""); 
	}
	
	
	if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}

	
	
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	
	
 	var r_ = new Array( "name", "phone", "email","CountryCode");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara_provident/"+s+"_success";
				   
				}
			}
		}
  
      )	    
   
}


//purvawindermere
function puravankara_jsfrm_purvawindermere(u,s,i){
  //alert(s);return false;
  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var city          = '';
  var time_to_call  = '';
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
     if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please select country");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
		if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	
	
 	var r_ = new Array( "name", "phone", "email","CountryCode");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara_provident/"+s+"_success";
				   
				}
			}
		}
  
      )	    
   
}


function puravankara_jsfrm_grandbay(u,s,i){
  //alert(s);return false;
  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var city          = '';
  var time_to_call  = '';
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
     if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please select country");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
		if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	
	
 	var r_ = new Array( "name", "phone", "email","CountryCode");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara_provident/"+s+"_success";
				   
				}
			}
		}
  
      )	    
   
}

//oceana
function puravankara_jsfrm_oceana(u,s,i){
	

  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var city          = '';
  var time_to_call  = '';
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
 var utm_campaign	= $("#utm_campaign").val(); 
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
     if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please select country");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
	if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	
	
 	var r_ = new Array( "name", "phone", "email","CountryCode");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara_provident/"+s+"_success";
				   
				}
			}
		}
  
      )	    
   
}

//sunflower
function puravankara_jsfrm_sunflower(u,s,i){
 // alert(i+s);return false;
  var name          = $("#name"+i).val();
  var phone         = $("#phone"+i).val();
  var email         = $("#email"+i).val();
  var city          = '';
  var time_to_call  = '';
  var country       = $("#country"+i).val();;
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
 var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	if($.trim(country) == "" || country == 0 ){
		
		$("#country"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#country_err"+i).html("Please select country");
	 return false;
	}else{
		$("#country"+i).css("border-color","rgb(218, 218, 218)");
		$("#country_err"+i).html(""); 
	}
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
	if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	
	
 	var r_ = new Array( "name", "phone", "email","country");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara_provident/"+s+"_success";
				   
				}
			}
		}
  
      )	    
   
}



function puravankara_jsfrm_northerndestiny(u,s,i){
//alert(u+s+i);return false;
  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var city          = '';
  var time_to_call  = '';
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
   var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
     if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please select country");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
	if(phone.length < '6' || phone.length > '13'){
		
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	
	
 	var r_ = new Array( "name", "phone", "email","CountryCode");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara_provident/"+s+"_success";
				   
				}
			}
		}
  
      )	    
   
}




function puravankara_jsfrm_vriksha(u,s,i){
  //alert(u+s+i);return false;
  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var city          = '';
  var time_to_call  = '';
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
   var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
     if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please select country");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
	if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	
	
 	var r_ = new Array( "name", "phone", "email","CountryCode");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara_provident/"+s+"_success";
				   
				}
			}
		}
  
      )	    
}


function puravankara_jsfrm_neora(u,s,i){
  //alert(u+s+i);return false;
  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var city          = '';
  var time_to_call  = '';
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
   var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
     if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please select country");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
	if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	
	
 	var r_ = new Array( "name", "phone", "email","CountryCode");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara_provident/"+s+"_success";
				   
				}
			}
		}
  
      )	    
}

function puravankara_jsfrm_watabid(u,s,i){
 // alert(u+s+i);return false;
  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var city          = '';
  var time_to_call  = '';
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
   var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
    
	if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	/* if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}  */
	
	
 	var r_ = new Array( "name", "phone", "email");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]+i).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara/success_"+s;
				   
				}
			}
		}
  
      )	    
}

function puravankara_jsfrm_highland(u,s,i){
 //alert(u+s+i);return false;
  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var city          = '';
  var time_to_call  = '';
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
    
	if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
		
 	var r_ = new Array( "name", "phone", "email");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]+i).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara/success_"+s;
				   
				}
			}
		}
  
      )	    
}


function puravankara_jsfrm_balinese(u,s,i){
 //alert(u+s+i);return false;
  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var city          = '';
  var time_to_call  = '';
   var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
    
	if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
		
 	var r_ = new Array( "name", "phone", "email");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]+i).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				    /*$("#otp_email"+i).val(email);
				    $("#form_div"+i).hide();
				   	$("#otp_section"+i).show();
				   		if(i != 3){
				   		$("body").addClass("otp_enabled");
				   	}*/
				  window.location.href = o+"realestate/puravankara_provident/"+s+"_success";
				   
				}
			}
		}
  
      )	    
}


function puravankara_jsfrm_toogoodhomes(u,s,i){
	

  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var city          = '';
  var time_to_call  = '';
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
 var utm_campaign	= $("#utm_campaign").val(); 
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
     if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please select country");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
	if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	
 	var r_ = new Array( "name", "phone", "email","CountryCode");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"realestate/puravankara_provident/"+s+"_success";
				   
				}
			}
		}
  
      )	    
   
}



function puravankara_jsfrm_greenpark(u,s,i){
	

  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var city          = '';
  var time_to_call  = '';
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var utm_campaign	= $("#utm_campaign").val(); 
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
     if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please select country");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
	if(phone.length < '6' || phone.length > '13'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	
 	var r_ = new Array( "name", "phone", "email","CountryCode");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				   
				    /*$("#otp_email"+i).val(email);
				    $("#form_div"+i).hide();
				   	$("#otp_section"+i).show();
				   	if(i != 3){
				   		$("body").addClass("otp_enabled");
				   	}*/
				   window.location.href = o+"realestate/puravankara_provident/"+s+"_success";
				   
				}
			}
		}
  
      )	    
   
}


function purvankara_otpverify(u,lp,i)
{
    //alert(u+lp+i);return false;
	var otp          = $("#otp"+i).val();
	var otp_email    = $("#otp_email"+i).val();
    
	if($.trim(otp) == ""){
		 $("#otp"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#otp_err"+i).html("Please Correct enter OTP");
	 	return false;  
	}else{
		 $("#otp"+i).css("border-color","rgb(218, 218, 218)");
		 $("#otp_err"+i).html(""); 
	}
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
	}
	$.ajax({
			url:u,
			type:"post",
			data: "&email="+otp_email+"&otp="+otp+"&lp="+lp,
			beforeSend:function(){
				//$("#spinner_loader").show();
				$("#sbmt"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;

				$(':input[type="submit"]').prop('disabled', false);
				$("sbmt"+i).val("Submit");
				   
				if(e == "done"){
					if(lp == 'freedom'){
						window.location.href = o+"realestate/puravankara/success_"+lp;
					}else{
					  window.location.href = o+"realestate/puravankara_provident/"+lp+"_success"
					}
				
				}else if(e == "resend"){
					$("#otp"+i).val("");
				    $("#otp_err"+i).html("Incorrect OTP");
				    $("#otp_email"+i).val(otp_email);
				    $("#form_div"+i).hide();
				   	$("#otp_section"+i).show();
				  
				   
				}
			}
		}
  
      )	 
}

function otp_resend(u,lp,i){

	var otp_email    = $("#otp_email"+i).val();
   
	$.ajax({
			url:u,
			type:"post",
			data: "&email="+otp_email+"&lp="+lp,
			beforeSend:function(){
				
			},
			success:function(e){
				//alert(e);return false;
				if(e == "done"){
				 $("#otp"+i).val("");
				    $("#otp_err"+i).html("OTP Resent Successfully");
				    $("#otp_email"+i).val(otp_email);
				    $("#form_div"+i).hide();
				   	$("#otp_section"+i).show();
				}else{
				   alert("something went wrong");
				   //window.location.href = o+"realestate/puravankara_provident/"+lp
				}
			}
		} 
      )	
} 


///purvankara dev-kajal

function puravankara_jsfrm_somerset(u,s,i){
  
	  var name          = $("#name"+i).val();
	  var country       = $("#CountryCode"+i).val();
	  var phone         = $("#phone"+i).val();
	  var email         = $("#email"+i).val();
	  var time_to_call  = '';
	  var utm_source1   = $("#utm_source1").val();
	  var utm_source2	= $("#utm_source2").val();
	  var utm_source3	= $("#utm_source3").val();
	  var city          = '';
	  var utm_campaign	= $("#utm_campaign").val();
	 // var utm_camp2	     = $("#utm_camp2").val();
	 // var utm_mobile1	 = $("#utm_mobile1").val();
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
		 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#name_err"+i).html("Please enter correct name");
		 return false;  
		}else{
			 $("#name"+i).css("border-color","rgb(218, 218, 218)");
			 $("#name_err"+i).html(""); 
		}
		
		
		
		if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
		 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#email_err"+i).html("Please enter correct Email");
		 return false;
		}else{
			 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
			 $("#email_err"+i).html(""); 
		}
	 
	    
			if(phone.length < '6' || phone.length > '13'){
			 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
			 $("#phone_err"+i).html("Please enter correct phone number");
			 return false;
		}else{
			 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
			 $("#phone_err"+i).html(""); 
		}
	  
	
	 	var r_ = new Array( "name", "phone", "email");
	  	for( var j=0; j < r_.length; j++ ){
			if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
			   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
			   $("#"+r_[j]+i).focus();
			 // alert(r_[i]);
			  // return false;	
			}else{
			   $("#"+r_[j]+"_err"+i).html("");	
			}
		}
		
		$("#otp_phone").val(phone);
		$("#otp_email").val(email);
		
		if(typeof $("#siteurl").val()!="undefined"){
				var o=$("#siteurl").val()
			}
		
	  $.ajax({
				url:u,
				type:"post",
				data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
				beforeSend:function(){
					$("#spinner_loader").show();
					$("#frm-sbmtbtn"+i).val("Processing..");
					$(':input[type="submit"]').prop('disabled', true);
				},
				success:function(e){
					//alert(e);return false;
					$(':input[type="submit"]').prop('disabled', false);
					$("#frm-sbmtbtn"+i).val("Submit");
					   
					if(e == "already"){
					   $("#email_err").html("Already registered we will contact you");	
					}else{
					   
					   
					   window.location.href = o+"realestate/puravankara/success_"+s;
					   
					}
				}
			}
	  
	      )	  
}


function puravankara_jsfrm_zenium(u,s,i){
  
	  var name          = $("#name"+i).val();
	  var country       = $("#CountryCode"+i).val();
	  var phone         = $("#phone"+i).val();
	  var email         = $("#email"+i).val();
	  var time_to_call  = '';
	  var utm_source1   = $("#utm_source1").val();
	  var utm_source2	= $("#utm_source2").val();
	  var utm_source3	= $("#utm_source3").val();
	  var city          = '';
	  var utm_campaign	= $("#utm_campaign").val();
	 // var utm_camp2	     = $("#utm_camp2").val();
	 // var utm_mobile1	 = $("#utm_mobile1").val();
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
		 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#name_err"+i).html("Please enter correct name");
		 return false;  
		}else{
			 $("#name"+i).css("border-color","rgb(218, 218, 218)");
			 $("#name_err"+i).html(""); 
		}
		
		
		
		if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
		 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#email_err"+i).html("Please enter correct Email");
		 return false;
		}else{
			 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
			 $("#email_err"+i).html(""); 
		}
	 
	    
			if(phone.length < '6' || phone.length > '13'){
			 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
			 $("#phone_err"+i).html("Please enter correct phone number");
			 return false;
		}else{
			 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
			 $("#phone_err"+i).html(""); 
		}
	  
	
	 	var r_ = new Array( "name", "phone", "email");
	  	for( var j=0; j < r_.length; j++ ){
			if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
			   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
			   $("#"+r_[j]+i).focus();
			 // alert(r_[i]);
			  // return false;	
			}else{
			   $("#"+r_[j]+"_err"+i).html("");	
			}
		}
		
		$("#otp_phone").val(phone);
		$("#otp_email").val(email);
		
		if(typeof $("#siteurl").val()!="undefined"){
				var o=$("#siteurl").val()
			}
		
	  $.ajax({
				url:u,
				type:"post",
				data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
				beforeSend:function(){
					$("#spinner_loader").show();
					$("#frm-sbmtbtn"+i).val("Processing..");
					$(':input[type="submit"]').prop('disabled', true);
				},
				success:function(e){
					//alert(e);return false;
					$(':input[type="submit"]').prop('disabled', false);
					$("#frm-sbmtbtn"+i).val("Submit");
					   
					if(e == "already"){
					   $("#email_err").html("Already registered we will contact you");	
					}else{
					   
					   
					   window.location.href = o+"realestate/puravankara/success_"+s;
					   
					}
				}
			}
	  
	      )	  
}

function puravankara_jsfrm_smilingwillows(u,s,i){
  
	  var name          = $("#name"+i).val();
	  var country       = $("#CountryCode"+i).val();
	  var phone         = $("#phone"+i).val();
	  var email         = $("#email"+i).val();
	  var time_to_call  = '';
	  var utm_source1   = $("#utm_source1").val();
	  var utm_source2	= $("#utm_source2").val();
	  var utm_source3	= $("#utm_source3").val();
	  var city          = '';
	  var utm_campaign	= $("#utm_campaign").val();
	 // var utm_camp2	     = $("#utm_camp2").val();
	 // var utm_mobile1	 = $("#utm_mobile1").val();
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
		 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#name_err"+i).html("Please enter correct name");
		 return false;  
		}else{
			 $("#name"+i).css("border-color","rgb(218, 218, 218)");
			 $("#name_err"+i).html(""); 
		}
		
		
		
		if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
		 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#email_err"+i).html("Please enter correct Email");
		 return false;
		}else{
			 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
			 $("#email_err"+i).html(""); 
		}
	 
	    
			if(phone.length < '6' || phone.length > '13'){
			 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
			 $("#phone_err"+i).html("Please enter correct phone number");
			 return false;
		}else{
			 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
			 $("#phone_err"+i).html(""); 
		}
	  
	
	 	var r_ = new Array( "name", "phone", "email");
	  	for( var j=0; j < r_.length; j++ ){
			if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
			   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
			   $("#"+r_[j]+i).focus();
			 // alert(r_[i]);
			  // return false;	
			}else{
			   $("#"+r_[j]+"_err"+i).html("");	
			}
		}
		
		$("#otp_phone").val(phone);
		$("#otp_email").val(email);
		
		if(typeof $("#siteurl").val()!="undefined"){
				var o=$("#siteurl").val()
			}
		
	  $.ajax({
				url:u,
				type:"post",
				data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
				beforeSend:function(){
					$("#spinner_loader").show();
					$("#frm-sbmtbtn"+i).val("Processing..");
					$(':input[type="submit"]').prop('disabled', true);
				},
				success:function(e){
					//alert(e);return false;
					$(':input[type="submit"]').prop('disabled', false);
					$("#frm-sbmtbtn"+i).val("Submit");
					   
					if(e == "already"){
					   $("#email_err").html("Already registered we will contact you");	
					}else{
					   
					   
					   window.location.href = o+"realestate/puravankara/success_"+s;
					   
					}
				}
			}
	  
	      )	  
}

function puravankara_jsfrm_marinaone(u,s,i){
  
	  var name          = $("#name"+i).val();
	  var country       = $("#CountryCode"+i).val();
	  var phone         = $("#phone"+i).val();
	  var email         = $("#email"+i).val();
	  var time_to_call  = '';
	  var utm_source1   = $("#utm_source1").val();
	  var utm_source2	= $("#utm_source2").val();
	  var utm_source3	= $("#utm_source3").val();
	  var city          = '';
	  var utm_campaign	= $("#utm_campaign").val();
	 // var utm_camp2	     = $("#utm_camp2").val();
	 // var utm_mobile1	 = $("#utm_mobile1").val();
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
		 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#name_err"+i).html("Please enter correct name");
		 return false;  
		}else{
			 $("#name"+i).css("border-color","rgb(218, 218, 218)");
			 $("#name_err"+i).html(""); 
		}
		
		
		
		if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
		 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#email_err"+i).html("Please enter correct Email");
		 return false;
		}else{
			 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
			 $("#email_err"+i).html(""); 
		}
	 
	    
			if(phone.length < '6' || phone.length > '13'){
			 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
			 $("#phone_err"+i).html("Please enter correct phone number");
			 return false;
		}else{
			 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
			 $("#phone_err"+i).html(""); 
		}
	  
	
	 	var r_ = new Array( "name", "phone", "email");
	  	for( var j=0; j < r_.length; j++ ){
			if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
			   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
			   $("#"+r_[j]+i).focus();
			 // alert(r_[i]);
			  // return false;	
			}else{
			   $("#"+r_[j]+"_err"+i).html("");	
			}
		}
		
		$("#otp_phone").val(phone);
		$("#otp_email").val(email);
		
		if(typeof $("#siteurl").val()!="undefined"){
				var o=$("#siteurl").val()
			}
		
	  $.ajax({
				url:u,
				type:"post",
				data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&time_to_call="+time_to_call+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
				beforeSend:function(){
					$("#spinner_loader").show();
					$("#frm-sbmtbtn"+i).val("Processing..");
					$(':input[type="submit"]').prop('disabled', true);
				},
				success:function(e){
					//alert(e);return false;
					$(':input[type="submit"]').prop('disabled', false);
					$("#frm-sbmtbtn"+i).val("Submit");
					   
					if(e == "already"){
					   $("#email_err").html("Already registered we will contact you");	
					}else{
					   
					   
					   window.location.href = o+"realestate/puravankara/success_"+s;
					   
					}
				}
			}
	  
	      )	  
}