
function chck_valid(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#"+ele+"_err").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#"+ele+"_err").html(""); 
  }
	
}


function gratorama_jsfrm(u){
 
  var name          = $("#name").val();
  var phone         = $("#phone").val();
  var email         = $("#email").val();
  var city          = $("#city").val();
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var utm_campaign	= $("#utm_campaign").val();
  if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name").css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err").html("Please enter correct name");
	 return false;  
	}else{
		 $("#name").css("border-color","rgb(218, 218, 218)");
		 $("#name_err").html(""); 
	}
  if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email").css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err").html("Please enter correct Email");
	 return false;
	}else{
		 $("#email").css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err").html(""); 
	}
 if(phone.length != '10'){
		 $("#phone").css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err").html("Please enter 10 digit mobile number");
		 return false;
	}else{
		 $("#phone").css("border-color","rgb(218, 218, 218)");
		 $("#phone_err").html(""); 
 }
  if($("#phone").val().length > 0){
	   if(!$("#phone").val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone").css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err").html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone").css("border-color","rgb(218, 218, 218)");
			$("#phone_err").html(""); 
		}  
  } 
    if($.trim(city) == "" || !city.match(/^[a-zA-Z\s]+$/)){
	 $("#city").css("border-color","rgb(232, 46, 33)").focus();
	 $("#city_err").html("Please enter correct city");
	 return false;  
	}else{
		 $("#city").css("border-color","rgb(218, 218, 218)");
		 $("#city_err").html(""); 
	}
  var r_ = new Array( "name", "phone", "email","city");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]).val()) == "" || $("#"+r_[j]).val() == 0 || $("#"+r_[j]).val() == "undefined"){
		   $("#"+r_[j]+"_err").html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]).focus();
		 // alert(r_[i]);
		  // return false;	
		}else{
		   $("#"+r_[j]+"_err").html("");	
		}
	}
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&campaign="+utm_campaign,
			beforeSend:function(){
				
				//$("#overlay_loading").show();
				$("#frm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);

			},
			success:function(e){
				
			   	
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn").val("Submit");
				   
				if(e == "already"){
				   //$("#email_err").html("Already registered please contact our customer service");
                     $("#form_show").hide();
                     $("#message_show").html("Already registered User");

				}else{
				    window.location.href = o+"gratorama/success";
				   
				}
			}
		}
  
      )	  
}

$(document).ready(function(e) {    
	$(document).on("keydown", ".only_numeric", function(e) {
		46 == e.keyCode || 8 == e.keyCode || 9 == e.keyCode || 27 == e.keyCode || 13 == e.keyCode || 65 == e.keyCode && e.ctrlKey === !0 
		|| e.keyCode >= 35 && e.keyCode <= 39 || (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) 
		&& e.preventDefault()
	})
});

