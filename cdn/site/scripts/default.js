$(document).ready(function(e) {    
	$(document).on("keydown", ".only_numeric", function(e) {
		46 == e.keyCode || 8 == e.keyCode || 9 == e.keyCode || 27 == e.keyCode || 13 == e.keyCode || 65 == e.keyCode && e.ctrlKey === !0 
		|| e.keyCode >= 35 && e.keyCode <= 39 || (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) 
		&& e.preventDefault()
	})

	
$(function () {
   $('.non_numeric').keydown(function (e) {
	 
	  if (e.ctrlKey ) {
		  e.preventDefault();
	  }else {
		  var key = e.keyCode;
		  if (!((key == 9) || (key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
			 e.preventDefault();
		  }
	  }
   });
});

});


function batra_jsfrm(u){
   
  var name          = $("#name").val();
  var email         = $("#email").val();
  var phone         = $("#phone").val();
  var city          = $("#city").val();
  var pb          	= $("#pub").val();
  var sr            = $("#source").val();
  var ut            = $("#utm").val();
    
  if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name").css("border-color","#ffe145").focus();
	 $("#user_info").html("Please enter correct name");
	 return false;  
  }else{
	 $("#name").css("border-color","#e6e6e6").focus();
	 $("#user_info").html(""); 
  }
  
  if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email").css("border-color","#ffe145").focus();
	 $("#user_info").html("Please enter correct Email");
	 return false;
  }else{
	 $("#email").css("border-color","#e6e6e6").focus();
	 $("#user_info").html(""); 
  }
  
  if(phone.length != '10'){
	 $("#phone").css("border-color","#ffe145").focus();
	 $("#user_info").html("Please enter 10 digit mobile number");
	 return false;
  }else{
	 $("#phone").css("border-color","#e6e6e6").focus();
	 $("#user_info").html(""); 
  }
  
  if($.trim(city) == "" || city == 0 ){
	 $("#city").css("border-color","#ffe145").focus(); 
	 $("#user_info").html("Please Select city");
	 return false;
  }else{
	 $("#city").css("border-color","#e6e6e6").focus();
	 $("#user_info").html(""); 
  }
  
  
  
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&ut="+ut+"&source="+sr+"&pb="+pb,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn").val("Submit");
				   
				if(e == "already"){
				   $("#user_info").html("Already registered we will contact you");	
				}else{
				   
				   $('#batra_frm').trigger('reset'); 
				   $("#spinner_loader").hide();
				   $("#b_div").show();
				   $("#success_div").show();
				   $("#div_pix").html(e);
				   
				   //$("#user_info").html("Some error occured try again later");	
				}
			}
		}
  
      )
	  
}


function onkeyup_valid(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" || city == 0 ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#user_info").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#user_info").html(""); 
  }
	
}


function onchange_valid(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" || city == 0 ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#user_info").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#user_info").html(""); 
  }
	
}

function change_test(test_id){
	
	if(test_id != ""){
		$("#srl_test").val(test_id);
		$("#srl_test").css("background-color", "#fbe185");
		$("#srl_test").focus();
	}
}


/*SRL JS*/



function srl_jsfrm(u, s_url){
   
  var name          = $("#name").val();
  var email         = $("#email").val();
  var phone         = $("#phone").val();
  var city          = $("#city").val();
  //var s_date        = $("#get_date").val();
  //var s_time        = $("#srl_time").val();
  var s_test        = $("#srl_test").val();
  //var addr          = $("#address").val();
  var pb          	= $("#pub").val();
  var ut            = $("#utm").val();
    
    
  if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name").css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err").html("Please enter correct name");
	 return false;  
  }else{
	 $("#name").css("border-color","#dadada");
	 $("#name_err").html(""); 
  }
  
  if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email").css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err").html("Please enter correct Email");
	 return false;
  }else{
	 $("#email").css("border-color","#dadada");
	 $("#email_err").html(""); 
  }
  
  if(phone.length != '10'){
	 $("#phone").css("border-color","rgb(232, 46, 33)").focus();
	 $("#phone_err").html("Please enter 10 digit mobile number");
	 return false;
  }else{
	 $("#phone").css("border-color","#dadada");
	 $("#phone_err").html(""); 
  }
  if($("#phone").val().length > 0){
	   if(!$("#phone").val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone").css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err").html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone").css("border-color","rgb(218, 218, 218)");
			$("#phone_err").html(""); 
		}  
	} 
 
 	//var r_ = new Array("name", "email", "phone", "city", "get_date", "srl_time", "srl_test", "address");
	//var r_ = new Array("name", "email", "phone", "city", "get_date", "srl_time", "srl_test");
	var r_ = new Array("name", "email", "phone", "city", "srl_test");
  	for( var i=0; i < r_.length; i++ ){
		if($.trim($("#"+r_[i]).val()) == "" || $("#"+r_[i]).val() == 0 || $("#"+r_[i]).val() == "undefined"){
		   $("#"+r_[i]+"_err").html($("#"+r_[i]).attr("data-attr"));
		   $("#"+r_[i]).css("border-color","#dadada").focus();
		   return false;	
		}else{
		   $("#"+r_[i]).css("border-color","#e6e6e6");
		   $("#"+r_[i]+"_err").html("");	
		}
	}


  $.ajax({
			url:u,
			type:"post",
			//data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&s_d="+s_date+"&s_time="+s_time+"&s_test="+s_test+"&ut="+ut+"&pb="+pb,
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&s_test="+s_test+"&ut="+ut+"&pb="+pb,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn").val("Submit");
				   
				if(e == "already"){
				   /*$('#feedbackForm').trigger('reset'); 
				   $("#spinner_loader").hide();
				   $("#b_div").show();
				   $("#f_div").hide();
				   $("#success_div").show()*/
				   window.location.href = s_url+"srlworld/success";
				}else{
				   
				  // window.location.href = "http://amazepromos.com/srlworld/success";
				   window.location.href = s_url+"srlworld/success";
				   /*$('#feedbackForm').trigger('reset'); 
				   $("#spinner_loader").hide();
				   $("#b_div").show();
				   $("#f_div").hide();
				   $("#success_div").show(); */
				   //$("#div_pix").html(e);
				   
				   //$("#user_info").html("Some error occured try again later");	
				}
			}
		}
  
      )	  
}


function chck_valid(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" || city == 0 ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#"+ele+"_err").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6");
	 $("#"+ele+"_err").html(""); 
  }
	
}



/*itokyo JS*/



function itokyo_jsfrm(u){
   
  var name          = $("#name").val();
  var phone         = $("#phone").val();
  var city          = $("#city").val();
  var age           = $("#age").val();
  var income        = $("#income").val();
  var plan          = $("#plan").val();
  var addr          = $("#address").val();
  var utm_source	= $("#utm_source").val();
  var utm_medium	= $("#utm_medium").val();
  
  if($.trim(plan) == "" || $.trim(plan) == "undefined" || $.trim(plan) == 0 ){
	 $("#plan").css("border-color","rgb(232, 46, 33)").focus();
	 $("#plan_err").html("Please select plan");
	 
	 return false;  
  }else{
	 $("#plan").css("border-color","rgb(218, 218, 218)");
	 $("#plan_err").html(""); 
  }  
    
  if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name").css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err").html("Please enter correct name");
	 
	 return false;  
  }else{
	 $("#name").css("border-color","rgb(218, 218, 218)");
	 $("#name_err").html(""); 
  }
 
	  if(phone.length != '10'){
		 $("#phone").css("border-color","rgb(218, 218, 218)").focus();
		 $("#phone_err").html("Please enter 10 digit mobile number");
		 return false;
	  }else{
		 $("#phone").css("border-color","rgb(218, 218, 218)");
		 $("#phone_err").html(""); 
	  }
  
	  if($.trim(city) == "" || $.trim(city) == "undefined" || $.trim(city) == 0 ){
		 $("#city").css("border-color","rgb(232, 46, 33)").focus();
		 $("#city_err").html("Please select plan");
		 return false;  
	  }else{
		 $("#city").css("border-color","rgb(218, 218, 218)");
		 $("#city_err").html(""); 
	  }  
	if($.trim(income) == "" || $.trim(income) == "undefined" ){
		 $("#income").css("border-color","rgb(232, 46, 33)").focus();
		 $("#income_err").html("Please enter income per month");
		 return false;  
	  }else{
		 $("#income").css("border-color","rgb(218, 218, 218)");
		 $("#income_err").html(""); 
	  } 
	if($.trim(age) == "" || $.trim(age) == "undefined" ){
		 $("#age").css("border-color","rgb(232, 46, 33)").focus();
		 $("#age_err").html("Please select age range");
		 return false;  
	  }else{
		 $("#age").css("border-color","rgb(218, 218, 218)");
		 $("#age_err").html(""); 
	  } 
 	var r_ = new Array("plan", "name", "phone", "city", "income", "age");
  	for( var i=0; i < r_.length; i++ ){
		if($.trim($("#"+r_[i]).val()) == "" || $("#"+r_[i]).val() == 0 || $("#"+r_[i]).val() == "undefined"){
		   $("#"+r_[i]+"_err").html($("#"+r_[i]).attr("data-attr"));
		   $("#"+r_[i]).focus();
		  
		   return false;	
		}else{
		   $("#"+r_[i]+"_err").html("");	
		}
	}
	

  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&plan="+plan+"&city="+city+"&income="+income+"&age="+age+"&utm_medium="+utm_medium+"&utm_source="+utm_source,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn").val("Submit");
				   
				if(e == "already"){
				   $("#email_err").html("Already registered we will contact you");	
				}else{
				   
				   window.location.href = "http://amazepromos.com/etokio/success";
				   
				   /*$('#feedbackForm').trigger('reset'); 
				   $("#spinner_loader").hide();
				   $("#b_div").show();
				   $("#f_div").hide();
				   $("#success_div").show(); */
				   //$("#div_pix").html(e);
				   
				   //$("#user_info").html("Some error occured try again later");	
				}
			}
		}
  
      )	  
}

function chck_valid_tokyo(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#"+ele+"_err").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#"+ele+"_err").html(""); 
  }
	
}


/* medlife JS*/



function medlife_adc_jsfrm(u, st_ur){
   
  var name          = $("#name").val();
  var phone         = $("#phone").val();
  var email          = $("#email").val();
  var medlife_test  = $("#medlife_test").val();
  var utm_source	= $("#utm_source").val();
  var utm_medium	= $("#utm_medium").val();
  var utm_campaign	= $("#utm_campaign").val();
  
  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name").css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err").html("Please enter correct name");
	 return false;  
	}else{
		 $("#name").css("border-color","rgb(218, 218, 218)");
		 $("#name_err").html(""); 
	}
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email").css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err").html("Please enter correct Email");
	 return false;
	}else{
		 $("#email").css("border-color","rgb(232, 46, 33)").focus();
		 $("#email_err").html(""); 
	}
 
	if(phone.length != '10'){
		 $("#phone").css("border-color","rgb(218, 218, 218)").focus();
		 $("#phone_err").html("Please enter 10 digit mobile number");
		 return false;
	}else{
		 $("#phone").css("border-color","rgb(218, 218, 218)");
		 $("#phone_err").html(""); 
	}
	
	if($("#phone").val().length > 0){
		if(!$("#phone").val().match(/^[6-9]{1}[0-9]{9}$/)){
		   $("#phone").css("border-color","rgb(218, 218, 218)").focus();
		   $("#phone_err").html("Please enter correct mobile number");
		  return false; 
		}else{
		   $("#phone").css("border-color","rgb(218, 218, 218)");
		   $("#phone_err").html(""); 
		}  
	} 

  
	if($.trim(medlife_test) == "" || $.trim(medlife_test) == "undefined" || $.trim(medlife_test) == 0 ){
	 $("#medlife_test").css("border-color","rgb(232, 46, 33)").focus();
	 $("#medlife_test_err").html("Please select plan");
	 
	 return false;  
	}else{
		 $("#medlife_test").css("border-color","rgb(218, 218, 218)");
		 $("#medlife_test_err").html(""); 
	}  
 	var r_ = new Array("medlife_test", "name", "phone", "email");
  	for( var i=0; i < r_.length; i++ ){
		if($.trim($("#"+r_[i]).val()) == "" || $("#"+r_[i]).val() == 0 || $("#"+r_[i]).val() == "undefined"){
		   $("#"+r_[i]+"_err").html($("#"+r_[i]).attr("data-attr"));
		   $("#"+r_[i]).focus();
		  
		   return false;	
		}else{
		   $("#"+r_[i]+"_err").html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&medlife_test="+medlife_test+"&email="+email+"&utm_medium="+utm_medium+"&utm_source="+utm_source+"&utm_campaign="+utm_campaign,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn").val("Submit");
				   
				if(e == "already"){
				   $("#email_err").html("Already registered we will contact you");	
				}else{
				   
				   $('#feedbackForm').trigger('reset'); 
				   $("#spinner_loader").hide();
				   $("#b_div").show();
				   $("#f_div").hide();
				   $("#success_div").show();
				   //$("#div_pix").html(e);
				   
				   //$("#user_info").html("Some error occured try again later");	
				}
			}
		}
  
      )	  
}


function medlife_jsfrm_old(u){
   
  var name          = $("#name").val();
  var phone         = $("#phone").val();
  var email          = $("#email").val();
  var medlife_test  = $("#medlife_test").val();
  var utm_source	= $("#utm_source").val();
  var utm_medium	= $("#utm_medium").val();
  var utm_campaign	= $("#utm_campaign").val();
  var date          = "";//$("#date").val();
  var time          = "";//$("#time").val();
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name").css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err").html("Please enter correct name");
	 return false;  
	}else{
		 $("#name").css("border-color","rgb(218, 218, 218)");
		 $("#name_err").html(""); 
	}
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email").css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err").html("Please enter correct Email");
	 return false;
	}else{
		 $("#email").css("border-color","rgb(218, 218, 218)");
		 $("#email_err").html(""); 
	}
 
	if(phone.length != '10'){
		 $("#phone").css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err").html("Please enter 10 digit mobile number");
		 return false;
	}else{
		 $("#phone").css("border-color","rgb(218, 218, 218)");
		 $("#phone_err").html(""); 
	}
	
	if($("#phone").val().length > 0){
	   if(!$("#phone").val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone").css("border-color","rgb(232, 46, 33)").focus();
			$("#phone_err").html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone").css("border-color","rgb(218, 218, 218)");
			$("#phone_err").html(""); 
		}  
	} 
  
	if($.trim(medlife_test) == "" || $.trim(medlife_test) == "undefined" || $.trim(medlife_test) == 0 ){
	 $("#medlife_test").css("border-color","rgb(232, 46, 33)").focus();
	 $("#medlife_test_err").html("Please select plan");
	 
	 return false;  
	}else{
		 $("#medlife_test").css("border-color","rgb(218, 218, 218)");
		 $("#medlife_test_err").html(""); 
	}  
	
	/*if(date == "" || date == "undefined" || date == 0 ){
	 $("#date").css("border-color","rgb(232, 46, 33)").focus();
	 $("#date_err").html("Please Select Appointment Date");
	 
	 return false;  
	}else{
		 $("#date").css("border-color","rgb(218, 218, 218)");
		 $("#date_err").html(""); 
	}  
	if(time == "" || time == "undefined" || time == 0 ){
	 $("#time").css("border-color","rgb(232, 46, 33)").focus();
	 $("#time_err").html("Please Select Time Slot");
	 
	 return false;  
	}else{
		 $("#time").css("border-color","rgb(218, 218, 218)");
		 $("#time_err").html(""); 
	}*/

 	//var r_ = new Array("medlife_test", "name", "phone", "email","date","time");
 	var r_ = new Array("medlife_test", "name", "phone", "email");
  	for( var i=0; i < r_.length; i++ ){
		if($.trim($("#"+r_[i]).val()) == "" || $("#"+r_[i]).val() == 0 || $("#"+r_[i]).val() == "undefined"){
		   $("#"+r_[i]+"_err").html($("#"+r_[i]).attr("data-attr"));
		   $("#"+r_[i]).focus();
		  
		   return false;	
		}else{
		   $("#"+r_[i]+"_err").html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&medlife_test="+medlife_test+"&email="+email+"&utm_medium="+utm_medium+"&utm_source="+utm_source+"&utm_campaign="+utm_campaign,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn").val("Submit");
				
				if(e == "already"){
				   $("#email_err").html("Already registered we will contact you");	
				}else{
				   
				   $('#feedbackForm').trigger('reset'); 
				   $("#spinner_loader").hide();
				   $("#b_div").show();
				   $("#f_div").hide();
				   $("#success_div").show();
				   //$("#div_pix").html(e);
				   
				   //$("#user_info").html("Some error occured try again later");	
				}
			}
		}
  
      )	  
}


function medlife_jsfrm(u){
   
  var name          = $("#name").val();
  var phone         = $("#phone").val();
  var email          = $("#email").val();
  var city          = $("#city").val();
  var medlife_test  = $("#medlife_test").val();
  var utm_source	= $("#utm_source").val();
  var utm_medium	= $("#utm_medium").val();
  var utm_campaign	= $("#utm_campaign").val();
  var utm_aff	    = $("#utm_aff").val();
  var form_type     = $("#form_type").val();
  var otp_verify	= $("#otp_verify").val(); 
  var fire_type     = $("#fire_type").val();
  var s_id	        = $("#s_id").val(); 
  var a_id			= $("#a_id").val(); 

	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name").css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err").html("Please enter correct name");
	 return false;  
	}else{
		 $("#name").css("border-color","rgb(218, 218, 218)");
		 $("#name_err").html(""); 
	}
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email").css("border-color","rgb(232, 46, 33)").focus();
	 	$("#email_err").html("Please enter correct Email");
	 	return false;
	}else{
		 $("#email").css("border-color","rgb(218, 218, 218)");
		 $("#email_err").html(""); 
	}
 
	if(phone.length != '10'){
		 $("#phone").css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err").html("Please enter 10 digit mobile number");
		 return false;
	}else{
		 $("#phone").css("border-color","rgb(218, 218, 218)");
		 $("#phone_err").html(""); 
	}
	
	if($("#phone").val().length > 0){
	   if(!$("#phone").val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone").css("border-color","rgb(232, 46, 33)").focus();
			$("#phone_err").html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone").css("border-color","rgb(218, 218, 218)");
			$("#phone_err").html(""); 
		}  
	} 
  
	if($.trim(medlife_test) == "" || $.trim(medlife_test) == "undefined" || $.trim(medlife_test) == 0 ){
		$("#medlife_test").css("border-color","rgb(232, 46, 33)").focus();
		$("#medlife_test_err").html("Please select plan");
	 
	 	return false;  
	}else{
		$("#medlife_test").css("border-color","rgb(218, 218, 218)");
		$("#medlife_test_err").html(""); 
	}

	if($.trim(city) == "" || $.trim(city) == "undefined" || $.trim(city) == 0 ){
		$("#city").css("border-color","rgb(232, 46, 33)").focus();
		$("#city_err").html("Please select city");
	 
	 	return false;  
	}else{
		$("#city").css("border-color","rgb(218, 218, 218)");
		$("#city_err").html(""); 
	}
	
	if(form_type == 2){	
		var date          = $("#date").val();
	  	var time          = $("#time").val();

		if(date == "" || date == "undefined" || date == 0 ){
			$("#date").css("border-color","rgb(232, 46, 33)").focus();
		 	$("#date_err").html("Please Select Appointment Date");
		 
		 	return false;  
		}else{
			 $("#date").css("border-color","rgb(218, 218, 218)");
			 $("#date_err").html(""); 
		}  
		if(time == "" || time == "undefined" || time == 0 ){
			$("#time").css("border-color","rgb(232, 46, 33)").focus();
			$("#time_err").html("Please Select Time Slot");
			 
			return false;  
		}else{
			$("#time").css("border-color","rgb(218, 218, 218)");
			$("#time_err").html(""); 
		}  

		var r_ = new Array("medlife_test", "name", "phone", "email","date","time");
		var datim = "&date="+date+"&time="+time+"&form_type="+form_type+"&otp_verify="+otp_verify+"&fire_type="+fire_type+"&s_id="+s_id+"&a_id="+a_id;
	}else{
		var r_ = new Array("medlife_test", "name", "phone", "email");
		var datim = "&form_type="+form_type+"&otp_verify="+otp_verify+"&fire_type="+fire_type+"&s_id="+s_id+"&a_id="+a_id;
	}

 	
  	for( var i=0; i < r_.length; i++ ){
		if($.trim($("#"+r_[i]).val()) == "" || $("#"+r_[i]).val() == 0 || $("#"+r_[i]).val() == "undefined"){
		   $("#"+r_[i]+"_err").html($("#"+r_[i]).attr("data-attr"));
		   $("#"+r_[i]).focus();
		  
		   return false;	
		}else{
		   $("#"+r_[i]+"_err").html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&medlife_test="+medlife_test+"&city="+city+"&email="+email+"&utm_medium="+utm_medium+"&utm_source="+utm_source+"&utm_campaign="+
			utm_campaign+"&utm_aff="+utm_aff+datim,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				var e = $.trim(e)
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn").val("Submit");
				   
				if(e == "already"){
					
				   	$("#email_err").html("Already registered we will contact you");	
				
			    }else{
				   
				   	$('#feedbackForm').trigger('reset'); 
				   	$("#spinner_loader").hide();
				   	$("#b_div").show();
				   	$("#f_div").hide();
				   	$("#success_div").show();
				   //$("#div_pix").html(e);
				   
				   //$("#user_info").html("Some error occured try again later");	
				}
			}
		}  
    )	  
}



function medlife_otp(u, st_ur){
	var otp          = $("#otp").val();
	var otp_phone    = $("#otp_phone").val();
    var otp_email    = $("#otp_email").val();
  	var form_type    = $("#form_type").val();
	var otp_verify   = $("#otp_verify").val(); 

	if($.trim(otp) == "" ){
	 $("#otp").css("border-color","rgb(232, 46, 33)").focus();
	 $("#otp_err").html("Please enter OTP");
	 return false;  
	}else{
		 $("#otp").css("border-color","rgb(218, 218, 218)");
		 $("#otp_err").html(""); 
	}
	
	$.ajax({
			url:u,
			type:"post",
			data: "&otp="+otp+"&otp_phone="+otp_phone+"&otp_email="+otp_email+"&form_type="+form_type+"&otp_verify="+otp_verify,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				var arr=e.split(',');
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn").val("Submit");
				   
				if(arr[0] == "resend"){
				   $("#otp_res_err").html("Incorrect OTP");
				   $("#res_id").val(arr[1]);
				   //$("#res_email").val(otp);
				   $('#feedbackForm').trigger('reset'); 
				   $("#spinner_loader").hide();
				   $("#b_div").show();
				   $("#f_div").hide();
				   $("#success_div").hide();
				   
				   $("#res_otp_div").show();
				   
				}
				else{
				   
				   window.location.href = st_ur+"success";
				   
				   /*$('#feedbackForm').trigger('reset'); 
				   $("#spinner_loader").hide();
				   $("#b_div").show();
				   $("#f_div").hide();
				   $("#success_div").hide();
				   
				   $("#success_verify").show();
				   */
				  
				}
			}
		})	 
	
}
function medlife_otp_resend(u){
	
	var id   = $("#res_id").val();
	$.ajax({
			url:u,
			type:"post",
			data: "&id="+id,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn").val("Submit");
				   
				if(e == "done"){
				   $("#otp").val("");
				   $("#otp_res_err").html("Incorrect OTP");
				   $('#otp').val(); 
				   $("#spinner_loader").hide();
				   $("#b_div").show();
				   $("#f_div").hide();
				   $("#success_div").show();
				   
				   $("#res_otp_div").hide();
				   
				}
				
			}
		})
	
}
function chck_valid_medlife(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#"+ele+"_err").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#"+ele+"_err").html(""); 
  }
	
}


/*religare JS*/



function religare_jsfrm(u, st_ur){
   
  var name          = $("#name").val();
  var phone         = $("#phone").val();
  var email         = $("#email").val();
  var city          = $("#city").val();
  var utm_source	= $("#utm_source").val();
  var utm_medium	= $("#utm_medium").val();
  var utm_campaign	= $("#utm_campaign").val();
  
  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name").css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err").html("Please enter correct name");
	 return false;  
	}else{
		 $("#name").css("border-color","rgb(218, 218, 218)");
		 $("#name_err").html(""); 
	}
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email").css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err").html("Please enter correct Email");
	 return false;
	}else{
		 $("#email").css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err").html(""); 
	}
 
	if(phone.length != '10'){
		 $("#phone").css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err").html("Please enter 10 digit mobile number");
		 return false;
	}else{
		 $("#phone").css("border-color","rgb(218, 218, 218)");
		 $("#phone_err").html(""); 
	}
  
	if($.trim(city) == "" || $.trim(city) == "undefined" ){
	 $("#city").css("border-color","rgb(232, 46, 33)").focus();
	 $("#city_err").html("Please enter city");
	 
	 return false;  
	}else{
		 $("#city_err").css("border-color","rgb(218, 218, 218)");
		 $("#city_err").html(""); 
	}  
 	var r_ = new Array( "name", "phone", "email","city");
  	for( var i=0; i < r_.length; i++ ){
		if($.trim($("#"+r_[i]).val()) == "" || $("#"+r_[i]).val() == 0 || $("#"+r_[i]).val() == "undefined"){
		   $("#"+r_[i]+"_err").html($("#"+r_[i]).attr("data-attr"));
		   $("#"+r_[i]).focus();
		   return false;	
		}else{
		   $("#"+r_[i]+"_err").html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&city="+city+"&email="+email+"&utm_medium="+utm_medium+"&utm_source="+utm_source+"&utm_campaign="+utm_campaign,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn").val("Submit");
				

				if(e == "already"){
				   $("#email_err").html("Already registered we will contact you");	
				}else{
				   
				   window.location.href = st_ur+"religare/success"; 
				   
				}
			}
		}
  
      )	  
}

function chck_valid_religare(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#"+ele+"_err").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#"+ele+"_err").html(""); 
  }
	
}



/*sheltrex  JS*/
 
 
 
function sheltrex_jsfrm(u,i){
   //alert(u+"-"+i);return false;
  var name          = $("#name"+i).val();
  var phone         = $("#phone"+i).val();
  var email         = $("#email"+i).val();
 // var city          = $("#city").val();
  var utm_leadsource = $("#utm_leadsource").val();
  var utm_category   = $("#utm_category").val();
  var utm_camp1      = $("#utm_camp1").val();
  var utm_camp2      = $("#utm_camp2").val();
  var utm_mobile1    = $("#utm_mobile1").val();
 
   
     
    if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
     $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
     $("#name_err"+i).html("Please enter correct name");
     return false;  
    }else{
         $("#name"+i).css("border-color","rgb(218, 218, 218)");
         $("#name_err"+i).html(""); 
    }
    if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
     $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
     $("#email_err"+i).html("Please enter correct Email");
     return false;
    }else{
         $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
         $("#email_err"+i).html(""); 
    }
  
    if(phone.length != '10'){
         $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
         $("#phone_err"+i).html("Please enter 10 digit mobile number");
         return false;
    }else{
         $("#phone"+i).css("border-color","rgb(218, 218, 218)");
         $("#phone_err"+i).html(""); 
    }
   
    if($("#phone"+i).val().length > 0){
       if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
            $("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
            $("#phone_err"+i).html("Please enter correct mobile number");
            //$("#phone").css({"border":"1px solid #C50736"}).focus();
        return false; 
        }else{
            $("#phone"+i).css("border-color","rgb(218, 218, 218)");
            $("#phone_err"+i).html(""); 
        }  
    } 
     
     
    var r_ = new Array( "name", "phone", "email");
    for( var j=0; j < r_.length; j++ ){
        if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
           $("#"+r_[j]+"_err"+i).html($("#"+r_[j]+i).attr("data-attr"));
           $("#"+r_[j]+i).focus();
          //alert(r_[i]);
           return false;    
        }else{
           $("#"+r_[j]+"_err"+i).html("");  
        }
    }
     
    $("#otp_phone").val(phone);
    $("#otp_email").val(email);
    //return false;
    if(typeof $("#siteurl").val()!="undefined"){
            var o=$("#siteurl").val()
        }
     
  $.ajax({
            url:u,
            type:"post",
            data: "&name="+name+"&phone="+phone+"&email="+email+"&utm_leadsource="+utm_leadsource+"&utm_category="+utm_category+"&utm_camp1="+utm_camp1+"&utm_camp2="+utm_camp2+"&utm_mobile1="+utm_mobile1,
            beforeSend:function(){
                $("#spinner_loader").show();
                $("#frm-sbmtbtn").val("Processing..");
                $(':input[type="submit"]').prop('disabled', true);
            },
            success:function(e){
                //alert(e);return false;
                $(':input[type="submit"]').prop('disabled', false);
                $("#frm-sbmtbtn").val("Submit");
                    
                if(e == "already"){
                   $("#email_err").html("Already registered we will contact you");  
                }else{
                    
                    
                   window.location.href = o+"sheltrex/success";
                    
                }
            }
        }
   
      )   
}
 
function chck_valid_sheltrex(ele, msg){
     
    if($.trim($("#"+ele).val()) == "" ){
     $("#"+ele).css("border-color","#ffe145").focus(); 
     $("#"+ele+"_err").html(msg);
     return false;
  }else{
     $("#"+ele).css("border-color","#e6e6e6").focus();
     $("#"+ele+"_err").html(""); 
  }
     
}



/*ploicyx health  JS*/



function ploicyx_health_jsfrm(u){
   
  var name          = $("#name").val();
  var phone         = $("#phone").val();
  var email         = $("#email").val();
  var city          = $("#city").val();
  var dob           = $("#datetimepicker").val();
  var utm_medium    = $("#utm_medium").val();
  var utm_source	= $("#utm_source").val();
  var utm_campaign	= $("#utm_campaign").val();
  
  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
		 $("#name").css("border-color","rgb(232, 46, 33)").focus();
		 $("#name_err").html("Please enter correct name");
		 return false;  
	}else{
		 $("#name").css("border-color","rgb(218, 218, 218)");
		 $("#name_err").html(""); 
	}
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
		 $("#email").css("border-color","rgb(232, 46, 33)").focus();
		 $("#email_err").html("Please enter correct Email");
		 return false;
	}else{
		 $("#email").css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err").html(""); 
	}
 
	if(phone.length != '10'){
		 $("#phone").css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err").html("Please enter 10 digit mobile number");
		 return false;
	}else{
		 $("#phone").css("border-color","rgb(218, 218, 218)");
		 $("#phone_err").html(""); 
	}
  
	if($("#phone").val().length > 0){
	   if(!$("#phone").val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone").css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err").html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone").css("border-color","rgb(218, 218, 218)");
			$("#phone_err").html(""); 
		}  
	} 
	
	if($.trim(dob) == "" ){
		 $("#dob").css("border-color","rgb(232, 46, 33)").focus();
		 $("#dob_err").html("Please select DOB");
		 return false;  
	}else{
		 $("#dob").css("border-color","rgb(218, 218, 218)");
		 $("#dob_err").html(""); 
	}
	
	if($.trim(city) == "" || city == 0 ){
		$("#city").css("border-color","rgb(218, 218, 218)").focus();
		$("#city_err").html("Please select city");
	 return false;
	}else{
		$("#city").css("border-color","rgb(218, 218, 218)");
		$("#city_err").html(""); 
	}
	
	
	
	
 	var r_ = new Array( "name", "email","phone","datetimepicker","city");
  	for( var i=0; i < r_.length; i++ ){
		if($.trim($("#"+r_[i]).val()) == "" || $("#"+r_[i]).val() == 0 || $("#"+r_[i]).val() == "undefined"){
		   $("#"+r_[i]+"_err").html($("#"+r_[i]).attr("data-attr"));
		   $("#"+r_[i]).focus();
		   return false;	
		}else{
		   $("#"+r_[i]+"_err").html("");	
		}
	}

	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&dob="+dob+"&utm_medium="+utm_medium+"&utm_source="+utm_source+"&utm_campaign="+utm_campaign,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn").val("Submit");
				   
				if(e == "already"){
				   $("#email_err").html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"health/success";
				   
				}
			}
		}
  
      )	  
}

function chck_valid_health(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#"+ele+"_err").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#"+ele+"_err").html(""); 
  }
	
}

/*ploicyx life  JS*/



function ploicyx_life_jsfrm(u){
   
  var name          = $("#name").val();
  var phone         = $("#phone").val();
  var email         = $("#email").val();
  var city          = $("#city").val();
  var dob           = $("#datetimepicker").val();
  var gender        = $("input[name='sex']:checked").val();
  var utm_medium    = $("#utm_medium").val();
  var utm_source	= $("#utm_source").val();
  var utm_campaign	= $("#utm_campaign").val();
  
  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
		 $("#name").css("border-color","rgb(232, 46, 33)").focus();
		 $("#name_err").html("Please enter correct name");
		 return false;  
	}else{
		 $("#name").css("border-color","rgb(218, 218, 218)");
		 $("#name_err").html(""); 
	}
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
		 $("#email").css("border-color","rgb(232, 46, 33)").focus();
		 $("#email_err").html("Please enter correct Email");
		 return false;
	}else{
		 $("#email").css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err").html(""); 
	}
 
	if(phone.length != '10'){
		 $("#phone").css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err").html("Please enter 10 digit mobile number");
		 return false;
	}else{
		 $("#phone").css("border-color","rgb(218, 218, 218)");
		 $("#phone_err").html(""); 
	}
  
	if($("#phone").val().length > 0){
	   if(!$("#phone").val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone").css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err").html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone").css("border-color","rgb(218, 218, 218)");
			$("#phone_err").html(""); 
		}  
	} 
	
	if($.trim(dob) == "" ){
		 $("#dob").css("border-color","rgb(232, 46, 33)").focus();
		 $("#dob_err").html("Please select DOB");
		 return false;  
	}else{
		 $("#dob").css("border-color","rgb(218, 218, 218)");
		 $("#dob_err").html(""); 
	}
	
	
	if($.trim(city) == "" || city == 0 ){
		$("#city").css("border-color","rgb(218, 218, 218)").focus();
		$("#city_err").html("Please select city");
	 return false;
	}else{
		$("#city").css("border-color","rgb(218, 218, 218)");
		$("#city_err").html(""); 
	}
	
	
	
	
 	var r_ = new Array( "name", "email","phone","datetimepicker","city");
  	for( var i=0; i < r_.length; i++ ){
		if($.trim($("#"+r_[i]).val()) == "" || $("#"+r_[i]).val() == 0 || $("#"+r_[i]).val() == "undefined"){
		   $("#"+r_[i]+"_err").html($("#"+r_[i]).attr("data-attr"));
		   $("#"+r_[i]).focus();
		  alert(r_[i]);
		   return false;	
		}else{
		   $("#"+r_[i]+"_err").html("");	
		}
	}

	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&dob="+dob+"&gender="+gender+"&utm_medium="+utm_medium+"&utm_source="+utm_source+"&utm_campaign="+utm_campaign,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn").val("Submit");
				   
				if(e == "already"){
				   $("#email_err").html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"life/success";
				   
				}
			}
		}
  
      )	  
}

function chck_valid_life(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#"+ele+"_err").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#"+ele+"_err").html(""); 
  }
	
}

function srirama_jsfrm(u,i){
  //alert("hi");
  var s ="";
  var name          = $("#name"+i).val();
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var email         = $("#email"+i).val();
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();
;
  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
 
    
 
	if(phone.length != '10'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter 10 digit mobile number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	} 
	
	
 	var r_ = new Array( "name", "phone", "email");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 // alert(r_[i]);
		  // return false;	
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&country="+country+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&lp_name="+s+"&campaign="+utm_campaign,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err").html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"codetakeiteasy/codetakeiteasy_success";
				   
				}
			}
		}
  
      )	  
}

	   

function religarehealth_jsfrm(u,type,i){
   
  var name          = $("#name"+i).val();
  var phone         = $("#phone"+i).val();
  var email         = $("#email"+i).val();
  var city          = $("#city"+i).val();
  var utm_source	= $("#utm_source").val();
  var utm_medium	= $("#utm_medium").val();
  var utm_campaign	= $("#utm_campaign").val();
  var family_member = $("#family_member"+i).val();
  var eldes_member  = $("#eldes_member"+i).val();
  var children      = $("#children"+i).val();
  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
 
	

	if($.trim(family_member) == "" ){
		$("#family_member"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#family_member_err"+i).html("Please Select Number of Members");
	 return false;
	}else{
		$("#family_member"+i).css("border-color","rgb(218, 218, 218)");
		$("#family_member_err"+i).html(""); 
	}  

	if($.trim(eldes_member) == "" || $.trim(eldes_member) == "undefined" ){
	 $("#eldes_member"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#eldes_member_err"+i).html("Please select eleder member age");
	 
	 return false;  
	}else{
		 $("#eldes_member_err"+i).css("border-color","rgb(218, 218, 218)");
		 $("#eldes_member_err"+i).html(""); 
	}  
	if(phone.length != '10'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter 10 digit mobile number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	if($.trim(city) == "" || $.trim(city) == "undefined" ){
	 $("#city"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#city_err"+i).html("Please enter city");
	 
	 return false;  
	}else{
		 $("#city_err"+i).css("border-color","rgb(218, 218, 218)");
		 $("#city_err"+i).html(""); 
	}  
	
 	var r_ = new Array( "name", "email","family_member","eldes_member","phone","city");
  	
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	
	//$("#otp_phone").val(phone);
	//$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
  
  
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&city="+city+"&email="+email+"&family_member="+family_member+"&children="+children+"&elder_member="+eldes_member+"&utm_medium="+utm_medium+"&utm_source="+utm_source+"&utm_campaign="+utm_campaign+"&type="+type+"&form_type="+i,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err").html("Already registered we will contact you");	
				}else{

				   if(type== "health"){
				   		 
				   		if(utm_campaign == "2924_"){
					   		 $("#feedbackForm"+i).hide();
					   		 $("#otpForm_div"+i).show();
					   		 $("#res_otp_div"+i).show();
					   		 $("#otp_phone"+i).val(phone);
					   		 $("#res_phone"+i).val(phone)
				   		}else{	
				   			 window.location.href = o+"religare/health_success";
				   		}

				   }else{
				   	  alert(type);
				   		 window.location.href = o+"religare/healthinsurance_success";
				   }
				  
				   
				}
			}
		}
  
      )	  
}


function religarehealth_otp(u, st_ur, i){
	var otp          = $.trim($("#otp"+i).val());
	var otp_phone    = $("#otp_phone"+i).val();

	if(otp.length != 4 ){
	   $("#otp"+i).css("border-color","rgb(232, 46, 33)").focus();
	   $("#otp_err"+i).html("Please enter OTP");
	   return false;  
	}else{
		 $("#otp"+i).css("border-color","rgb(218, 218, 218)");
		 $("#otp_err"+i).html(""); 
	}
	
	$.ajax({
			url:u,
			type:"post",
			data: "&otp="+otp+"&otp_phone="+otp_phone,
			beforeSend:function(){
				
				$("#otpfrm-sbmtbtn"+i).val("Verifying..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				var e = $.trim(e);
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#otpfrm-sbmtbtn"+i).val("Verify");
				   
				if(e == "resend"){
				   $("#otp_err"+i).html("Incorrect OTP. Please resend ");
				   
				}else{
				   window.location.href = st_ur+"religare/health_success";
				   
				}
			}
		})	 
	
}
function religarehealth_otp_resend(u,i){
	
	var otp_phone   = $("#res_phone"+i).val();
	
	if(otp_phone){
		$.ajax({
				url:u,
				type:"post",
				data: "&otp_phone="+otp_phone,
				beforeSend:function(){
					$("#res-sbmtbtn"+i).val("Sending..");
					$(':input[type="submit"]').prop('disabled', true);
				},
				success:function(e){
					$("#otp"+i).val("");
					$("#otp_err"+i).html("Otp Sent");
					
					$(':input[type="submit"]').prop('disabled', false);
					$("#res-sbmtbtn"+i).val("Resend");
					   
					if(e == "done"){
					   $("#otp"+i).val("");
					   $("#otp_err"+i).html("Otp Sent");
					}
					
				}
			})
	}
	
}



function mfine_jsfrm(u, st_ur){
   
  var name          = $("#name").val();
  var phone         = $("#phone").val();
  var email         = $("#email").val();
  var mfine_test    = $("#mfine_test").val();
  var utm_source	= $("#utm_source").val();
  var utm_medium	= $("#utm_medium").val();
  var utm_campaign	= $("#utm_campaign").val();
  
  
    
    if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	   $("#name").css("border-color","rgb(232, 46, 33)").focus();
	   $("#name_err").html("Please enter correct name");
	   return false;  
	}else{
	   $("#name").css("border-color","rgb(218, 218, 218)");
	   $("#name_err").html(""); 
	}
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	   $("#email").css("border-color","rgb(232, 46, 33)").focus();
	   $("#email_err").html("Please enter correct Email");
	   return false;
	}else{
	   $("#email").css("border-color","rgb(218, 218, 218)");
	   $("#email_err").html(""); 
	}
 
	if(phone.length != '10'){
		$("#phone").css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err").html("Please enter 10 digit mobile number");
		 return false;
	}else{
		$("#phone").css("border-color","rgb(218, 218, 218)");
		$("#phone_err").html(""); 
	}
	
	if($("#phone").val().length > 0){
		if(!$("#phone").val().match(/^[6-9]{1}[0-9]{9}$/)){
		   $("#phone").css("border-color","rgb(218, 218, 218)").focus();
		   $("#phone_err").html("Please enter correct mobile number");
		  return false; 
		}else{
		   $("#phone").css("border-color","rgb(218, 218, 218)");
		   $("#phone_err").html(""); 
		}  
	} 

  
	if($.trim(mfine_test) == "" || $.trim(mfine_test) == "undefined"  ){
	 $("#mfine_test").css("border-color","rgb(232, 46, 33)").focus();
	 $("#mfine_test_err").html("Please select plan");
	 
	 return false;  
	}else{
		 $("#mfine_test").css("border-color","rgb(218, 218, 218)");
		 $("#mfine_test_err").html(""); 
	}  
 	var r_ = new Array( "name", "email", "phone", "mfine_test");
  	for( var i=0; i < r_.length; i++ ){
		if($.trim($("#"+r_[i]).val()) == "" || $("#"+r_[i]).val() == 0 || $("#"+r_[i]).val() == "undefined"){
		   $("#"+r_[i]+"_err").html($("#"+r_[i]).attr("data-attr"));
		   $("#"+r_[i]).focus();
		  
		   return false;	
		}else{
		   $("#"+r_[i]+"_err").html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);

  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&mfine_test="+mfine_test+"&email="+email+"&utm_medium="+utm_medium+"&utm_source="+utm_source+"&utm_campaign="+utm_campaign,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn").val("Submit");
				   
				if(e == "already"){
				   $("#email_err").html("Already registered we will contact you");	
				}else{
				    $("#f_div").hide(); 
				    $('#feedbackForm').trigger('reset'); 
				   	$("#success_div").show();				   

				}
			}
		}
  
      )	  
}


function mfine_otp(u, st_ur){
	var otp          = $.trim($("#otp").val());
	var otp_phone    = $("#otp_phone").val();
    var otp_email    = $("#otp_email").val();

	if(otp.length != 4 ){
	   $("#otp").css("border-color","rgb(232, 46, 33)").focus();
	   $("#otp_err").html("Please enter OTP");
	   return false;  
	}else{
		 $("#otp").css("border-color","rgb(218, 218, 218)");
		 $("#otp_err").html(""); 
	}
	
	$.ajax({
			url:u,
			type:"post",
			data: "&otp="+otp+"&otp_phone="+otp_phone+"&otp_email="+otp_email,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				var e = $.trim(e);
				var arr=e.split(',');
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn").val("Submit");
				   
				if(arr[0] == "resend"){
				   $("#otp_res_err").html("Incorrect OTP");
				   $("#res_id").val(arr[1]);
				   //$("#res_email").val(otp);
				   $('#feedbackForm').trigger('reset'); 
				   $("#success_div").show();
				   $("#f_div").hide();
				   $("#res_otp_div").show();
				   
				}else{
				   window.location.href = st_ur+"mfine/success";
				   
				}
			}
		})	 
	
}
function mfine_otp_resend(u){
	
	var id   = $("#res_id").val();
	
	if(id){
		$.ajax({
				url:u,
				type:"post",
				data: "&id="+id,
				beforeSend:function(){
					$("#spinner_loader").show();
					$("#frm-sbmtbtn").val("Processing..");
					$(':input[type="submit"]').prop('disabled', true);
				},
				success:function(e){
					
					$(':input[type="submit"]').prop('disabled', false);
					$("#frm-sbmtbtn").val("Submit");
					   
					if(e == "done"){
					   $("#otp").val("");
					   $("#otp_res_err").html("Incorrect OTP");
					   $('#otp').val(); 
					   $("#spinner_loader").hide();
					   $("#b_div").show();
					   $("#f_div").hide();
					   $("#success_div").show();
					   
					   $("#res_otp_div").hide();
					   
					}
					
				}
			})
	}
	
}


