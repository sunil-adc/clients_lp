$(document).ready(function(e) {    
	$(document).on("keydown", ".only_numeric", function(e) {
		46 == e.keyCode || 8 == e.keyCode || 9 == e.keyCode || 27 == e.keyCode || 13 == e.keyCode || 65 == e.keyCode && e.ctrlKey === !0 
		|| e.keyCode >= 35 && e.keyCode <= 39 || (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) 
		&& e.preventDefault()
	})

	
$(function () {
   $('.non_numeric').keydown(function (e) {
	 
	  if (e.ctrlKey ) {
		  e.preventDefault();
	  }else {
		  var key = e.keyCode;
		  if (!((key == 9) || (key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
			 e.preventDefault();
		  }
	  }
   });
});

});


function wolkswagen_jsfrm(u){
   
  var name          = $("#name").val();
  var email         = $("#email").val();
  var phone         = $("#mobile").val();
  var dealer        = $("#dealer").val();
  var car          	= $("#car").val();
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
 var utm_campaign	= $("#utm_campaign").val(); 
    
  if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name").css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err").html("Please enter correct name");
	 return false;  
	}else{
		 $("#name").css("border-color","rgb(218, 218, 218)");
		 $("#name_err").html(""); 
	}
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email").css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err").html("Please enter correct Email");
	 return false;
	}else{
		 $("#email").css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err").html(""); 
	}
	
	
	
	if(phone.length < '6' || phone.length > '13'){
		 $("#mobile").css("border-color","rgb(232, 46, 33)").focus();
		 $("#mobile_err").html("Please enter correct mobile number");
		 return false;
	}else{
		 $("#mobile").css("border-color","rgb(218, 218, 218)");
		 $("#mobile_err").html(""); 
	}
  
	if($("#mobile").val().length > 0){
	   if(!$("#mobile").val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#mobile").css("border-color","rgb(218, 218, 218)").focus();
			$("#mobile_err").html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#mobile").css("border-color","rgb(218, 218, 218)");
			$("#mobile_err").html(""); 
		}  
	} 
	if($.trim(dealer) == "" || dealer == 0 ){
		$("#dealer").css("border-color","rgb(218, 218, 218)").focus();
		$("#dealer_err").html("Please Select Dealer");
	 return false;
	}else{
		$("#dealer").css("border-color","rgb(218, 218, 218)");
		$("#dealer_err").html(""); 
	}
	 
	if($.trim(car) == "" || car == 0 ){
		$("#car").css("border-color","rgb(218, 218, 218)").focus();
		$("#car_err").html("Please Select Your Car");
	 return false;
	}else{
		$("#car").css("border-color","rgb(218, 218, 218)");
		$("#car_err").html(""); 
	}
	
	
	
 	var r_ = new Array( "name", "phone", "email","dealer","car");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]).val()) == "" || $("#"+r_[j]).val() == 0 || $("#"+r_[j]).val() == "undefined"){
		   $("#"+r_[j]+"_err").html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]).focus();
		 // alert(r_[i]);
		  // return false;	
		}else{
		   $("#"+r_[j]+"_err").html("");	
		}
	}
	

	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data:  "&name="+name+"&phone="+phone+"&email="+email+"&dealer="+dealer+"&car="+car+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&campaign="+utm_campaign,
			beforeSend:function(){
				//alert(i);
				$("#spinner_loader").show();
				$("#frm-sbmtbtn").val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
			
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn").val("Submit");
				   
				if(e == "already"){
				   $("#email_err").html("Already registered we will contact you");	
				}else{
				   
				   window.location.href = o+"volkswagen/volkswagen_success";
				   
				}
			}
		}
  
      )	  
}


function onkeyup_valid(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" || city == 0 ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#user_info").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#user_info").html(""); 
  }
	
}


function onchange_valid(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" || city == 0 ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#user_info").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#user_info").html(""); 
  }
	
}