
function chck_valid(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#"+ele+"_err").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#"+ele+"_err").html(""); 
  }
	
}


function lsbf_jsfrm(u,i){



  var s ="";
  var fname          = $("#fname"+i).val();
  //alert(fname);return false;
  var lname          = $("#lname"+i).val();
  var country        = $("#CountryCode"+i).val();
  var phone          = $("#phone"+i).val();
  var email          = $("#email"+i).val();
  var cource         = $("#cource"+i).val();
  var utm_source1    = $("#utm_source1").val();
  var utm_source2	 = $("#utm_source2").val();
  var utm_source3	 = $("#utm_source3").val();
  var utm_campaign	 = $("#utm_campaign").val();
  

  
    
	if($.trim(fname) == "" || !fname.match(/^[a-zA-Z\s]+$/)){
		
	 $("#fname"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#fname_err"+i).html("Please enter correct first name");
	 return false;  
	}else{
		
		 $("#fname"+i).css("border-color","rgb(218, 218, 218)");
		 $("#fname_err"+i).html(""); 
	}
	if($.trim(lname) == "" || !lname.match(/^[a-zA-Z\s]+$/)){
	 $("#lname"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#lname_err"+i).html("Please enter correct last name");
	 return false;  
	}else{
		 $("#lname"+i).css("border-color","rgb(218, 218, 218)");
		 $("#lname_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
 
    
 
	if(phone.length != '10'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter 10 digit mobile number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	} 
	if($.trim(cource) == "" || cource == 0 ){
		$("#cource"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#cource_err"+i).html("Please Select Preferred Cource");
	 return false;
	}else{
		$("#cource"+i).css("border-color","rgb(218, 218, 218)");
		$("#cource"+i).html(""); 
	}
	
 	var r_ = new Array( "fname","lname", "phone", "email","cource");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 // alert(r_[i]);
		  // return false;	
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&fname="+fname+"&lname="+lname+"&phone="+phone+"&email="+email+"&country="+country+"&course="+cource+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&campaign="+utm_campaign,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				//alert(e);return false;
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err").html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"lsbf/success";
				   
				}
			}
		}
  
      )	  
}
