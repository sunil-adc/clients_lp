
$(function() {
		var dt = new Date();
		var startDt = dt.getDate() + '/' + ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + (dt.getFullYear() - 100);
		var endDt = dt.getDate() + '/' + ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + (dt.getFullYear() - 0);

		$( ".datepicker" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			showMonthAfterYear: true,
			yearRange: (dt.getFullYear() - 100) + ":" + (dt.getFullYear() ),
			inline: true
		});
		$('.content-box ul.content-box-tabs li a, .change_criteria').click(
			function() { 
				var _this = ".content-box ul.content-box-tabs li a";
				$(_this).parent().siblings().find("a").removeClass('current');
				$(_this).removeClass("default-tab");
				$(_this).removeClass("current");
				
				var currentTab = $(_this).attr('href');
				$(_this).addClass('current default-tab');
				$(currentTab).siblings().hide();
				$(currentTab).show();
				return false; 
			}
		);
		
	});
function chck_valid(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#"+ele+"_err").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#"+ele+"_err").html(""); 
  }
	
}


function insurance_jsfrm(u,lp,i){
 
   var name          = $("#name"+i).val();
   var phone         = $("#phone"+i).val();
   var email         = $("#email"+i).val();
   var dob           = $("#user_dob"+i).val();
   var country       = $("#CountryCode"+i).val();
   var city          = $("#city"+i).val();
   /*var date          = new Date($("#user_dob"+i).val()); 
   day = date.getDate();
   month = date.getMonth() + 1;
   year = date.getFullYear();
   dob=[day, month, year].join('/');
  */
   var utm_source    = $("#utm_source").val();
   var utm_medium	= $("#utm_medium").val();
   var utm_sub	    = $("#utm_sub").val();
   var utm_campaign	= $("#utm_campaign").val();
    
    
 
   if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	    return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
	if(phone.length != '10'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter correct phone number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	}
	if(lp =="policy_magnifier" || lp == "getinsured") {
		if($.trim(city) == "" || city == 0 ){
		$("#city"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#city_err"+i).html("Please Select Preferred City");
	    $("#city"+i).focus();
	    return false;
	}else{
		$("#city"+i).css("border-color","rgb(218, 218, 218)");
		$("#city"+i).html(""); 
	  }
	}
	if(lp =="maxlife"  || lp == "getinsured") {
		if($.trim(dob) == ""){
		$("#user_dob"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#user_dob_err"+i).html("Please Select age");
	 	$("#user_dob"+i).focus();
	 	return false;
	}else{
		$("#user_dob"+i).css("border-color","rgb(218, 218, 218)");
		$("#user_dob"+i).html(""); 
	  }
	}
	
	if(lp =="policy_magnifier") {
		var r_ = new Array( "name","email","phone","city");
	} if(lp =="getinsured") {
	 	var r_ = new Array( "name","email","phone","city","user_dob");
	}else{
		var r_ = new Array( "name","email","phone","user_dob");
	}
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
		if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val();
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&country="+country+"&city="+city+"&utm_source="+utm_source+"&utm_medium="+utm_medium+"&utm_sub="+utm_sub+"&campaign=" +utm_campaign+"&lp="+lp+"&dob="+dob,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err"+i).html("Already registered we will contact you");	
				}else{
				    window.location.href = o+"policy/insurance/"+lp+"_success";
				}
			}
		}
  
        )	  
}
