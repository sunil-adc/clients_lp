
function chck_valid(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#"+ele+"_err").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#"+ele+"_err").html(""); 
  }
	
}


function nmims_jsfrm(u,i){
//
  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  
  var city          = $("#city"+i).val();
  var cource  = $("#cource"+i).val();
 
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
   var utm_campaign	= $("#utm_campaign").val();
 // var utm_camp2	     = $("#utm_camp2").val();
 // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
     if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please select country");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
	
  
	if(phone.length != '10'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter 10 digit mobile number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	} 
	if($.trim(city) == "" || city == 0 ){
		$("#city"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#city_err"+i).html("Please Select Preferred City");
	 return false;
	}else{
		$("#city"+i).css("border-color","rgb(218, 218, 218)");
		$("#city"+i).html(""); 
	}
	/* if($.trim(cource) == "" || cource == 0 ){
		$("#cource"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#cource_err"+i).html("Please Select Preferred Cource");
	 return false;
	}else{
		$("#cource"+i).css("border-color","rgb(218, 218, 218)");
		$("#cource"+i).html(""); 
	}
	 */
	 if($('[name=cource'+i+']:checked').length > 0){
	}else{
	    $("#cource_err").html("Please Select Preferred Cource").css({"color":"#C50736"});
	    return false;	
	}
 	var r_ = new Array( "name", "phone", "email","CountryCode","city");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&country="+country+"&course="+$('input:radio[name=cource'+i+']:checked').val()+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&campaign="+utm_campaign,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err").html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"nmims/nmims_success";
				   
				}
			}
		}
  
      )	  
}



function nmims_jsfrm_executive_mba(u, i){

  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var city          = $("#city"+i).val();
  var center        = $("#center"+i).val();
  var cource  	    = $("#cource"+i).val(); 
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var utm_campaign	= $("#utm_campaign").val();
  // var utm_camp2	     = $("#utm_camp2").val();
  // var utm_mobile1	 = $("#utm_mobile1").val();

  
    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
     if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please select country");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
	
  
	if(phone.length != '10'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter 10 digit mobile number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	} 
	if($.trim(city) == "" || city == 0 ){
		$("#city"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#city_err"+i).html("Please Select Preferred City");
	 return false;
	}else{
		$("#city"+i).css("border-color","rgb(218, 218, 218)");
		$("#city"+i).html(""); 
	}

	if($.trim(center) == "" || center == 0 ){
		$("#center"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#center_err"+i).html("Please Select center");
	 return false;
	}else{
		$("#center"+i).css("border-color","rgb(218, 218, 218)");
		$("#center"+i).html(""); 
	}

 	
 	var r_ = new Array( "name", "phone", "email","CountryCode","city", "center");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
	}

  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&center="+center+"&country="+country+"&course=master_programs&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&campaign="+utm_campaign,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
			
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err").html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"nmims/executivemba_success";
				   
				}
			}
		}
  
      )	  
}



function nmims_jsfrm_digital(u, i){

  var name          = $("#name"+i).val();
  var email         = $("#email"+i).val();
  var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var city          = $("#city"+i).val();
  var center        = $("#center"+i).val();
  var cource  	    = $("#cource"+i).val(); 
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var utm_campaign	= $("#utm_campaign").val();
  // var utm_camp2	     = $("#utm_camp2").val();
  // var utm_mobile1	 = $("#utm_mobile1").val();

  
     
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
     if($.trim(country) == "" || country == 0 ){
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#phone_err"+i).html("Please select country");
	 return false;
	}else{
		$("#CountryCode"+i).css("border-color","rgb(218, 218, 218)");
		$("#phone_err"+i).html(""); 
	}
	
  
	if(phone.length != '10'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter 10 digit mobile number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	} 
	if($.trim(city) == "" || city == 0 ){
		$("#city"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#city_err"+i).html("Please Select Preferred City");
	 return false;
	}else{
		$("#city"+i).css("border-color","rgb(218, 218, 218)");
		$("#city"+i).html(""); 
	}

	if($.trim(center) == "" || center == 0 ){
		$("#center"+i).css("border-color","rgb(218, 218, 218)").focus();
		$("#center_err"+i).html("Please Select center");
	 return false;
	}else{
		$("#center"+i).css("border-color","rgb(218, 218, 218)");
		$("#center"+i).html(""); 
	}
	
 	var r_ = new Array( "name", "phone", "email","CountryCode","city", "center");
  	for( var j=0; j < r_.length; j++ ){
		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		 
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	$("#otp_phone").val(phone);
	$("#otp_email").val(email);
	
	if(typeof $("#siteurl").val()!="undefined"){
		var o=$("#siteurl").val()
	}

  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&city="+city+"&center="+center+"&country="+country+"&course=digital_marketting&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&campaign="+utm_campaign,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){

				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
				   
				if(e == "already"){
				   $("#email_err").html("Already registered we will contact you");	
				}else{
				   
				   
				   window.location.href = o+"nmims/digitalMarketing_success";
				   
				}
			}
		}
  
      )	  
}
