
function chck_valid(ele, msg){
	
	if($.trim($("#"+ele).val()) == "" ){
	 $("#"+ele).css("border-color","#ffe145").focus(); 
	 $("#"+ele+"_err").html(msg);
	 return false;
  }else{
	 $("#"+ele).css("border-color","#e6e6e6").focus();
	 $("#"+ele+"_err").html(""); 
  }
	
}


function oxygenresort_jsfrm(u,i){


  var s ="";
  var name          = $("#name"+i).val();
  //var country       = $("#CountryCode"+i).val();
  var phone         = $("#phone"+i).val();
  var email         = $("#email"+i).val();
  var age           = $("#age"+i).val();
  var location      = $("#location"+i).val();
  var lastlocation  = $("#lastlocation"+i).val();
  var utm_source1   = $("#utm_source1").val();
  var utm_source2	= $("#utm_source2").val();
  var utm_source3	= $("#utm_source3").val();
  var utm_campaign	= $("#utm_campaign").val();

  var utm_term	    = $("#utm_term").val();
  var utm_content	= $("#utm_content").val();


    
	if($.trim(name) == "" || !name.match(/^[a-zA-Z\s]+$/)){
	 $("#name"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#name_err"+i).html("Please enter correct name");
	 return false;  
	}else{
		 $("#name"+i).css("border-color","rgb(218, 218, 218)");
		 $("#name_err"+i).html(""); 
	}
	
	
	
	if(!email.match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
	 $("#email"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#email_err"+i).html("Please enter correct Email");
	 return false;
	}else{
		 $("#email"+i).css("border-color","rgb(218, 218, 218)").focus();
		 $("#email_err"+i).html(""); 
	}
 
    
 
	if(phone.length != '10'){
		 $("#phone"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#phone_err"+i).html("Please enter 10 digit mobile number");
		 return false;
	}else{
		 $("#phone"+i).css("border-color","rgb(218, 218, 218)");
		 $("#phone_err"+i).html(""); 
	}
  
	if($("#phone"+i).val().length > 0){
	   if(!$("#phone"+i).val().match(/^[6-9]{1}[0-9]{9}$/)){
			$("#phone"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#phone_err"+i).html("Please enter correct mobile number");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#phone"+i).css("border-color","rgb(218, 218, 218)");
			$("#phone_err"+i).html(""); 
		}  
	} 
	/*if(!$("#age"+i).val().match(/^[0-9]$/)){
			$("#age"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#age_err"+i).html("Please enter valid age");
			//$("#phone").css({"border":"1px solid #C50736"}).focus();
		return false; 
		}else{
			$("#age"+i).css("border-color","rgb(218, 218, 218)");
			$("#age_err"+i).html(""); 
		} */
	 if(isNaN($("#age"+i).val())|| $("#age"+i).val()>100){
			$("#age"+i).css("border-color","rgb(218, 218, 218)").focus();
			$("#age_err"+i).html("Please enter valid age");
			
			return false; 
		}else{
			$("#age"+i).css("border-color","rgb(218, 218, 218)");
			$("#age_err"+i).html(""); 
		}  
	
	
	if($.trim(location) == "" || location == 0){
	 $("#location"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#location_err"+i).html("Please select location");
	 return false;  
	}else{
		 $("#location"+i).css("border-color","rgb(218, 218, 218)");
		 $("#location_err"+i).html(""); 
	}
	if($.trim(lastlocation) == "" || lastlocation == 0){
	 $("#lastlocation"+i).css("border-color","rgb(232, 46, 33)").focus();
	 $("#lastlocation_err"+i).html("Please select last location");
	return false; 
	}else{
		 $("#lastlocation"+i).css("border-color","rgb(218, 218, 218)");
		 $("#lastlocation_err"+i).html(""); 
	}
 	var r_ = new Array( "name", "phone", "email","age","location","lastlocation");
  	for( var j=0; j < r_.length; j++ ){

		if($.trim($("#"+r_[j]+i).val()) == "" || $("#"+r_[j]+i).val() == 0 || $("#"+r_[j]+i).val() == "undefined"){
		   $("#"+r_[j]+"_err"+i).html($("#"+r_[j]).attr("data-attr"));
		   $("#"+r_[j]+i).focus();
		  
		  // return false;	
		}else{
		   $("#"+r_[j]+"_err"+i).html("");	
		}
	}
	
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
	
  $.ajax({
			url:u,
			type:"post",
			data: "&name="+name+"&phone="+phone+"&email="+email+"&age="+age+"&location="+location+"&lastlocation="+lastlocation+"&utm_source1="+utm_source1+"&utm_source2="+utm_source2+"&utm_source3="+utm_source3+"&campaign="+utm_campaign+"&utm_term="+utm_term+"&utm_content="+utm_content,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#frm-sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				$(':input[type="submit"]').prop('disabled', false);
				$("#frm-sbmtbtn"+i).val("Submit");
								   
				if(e == "already"){
				   alert("Already registered we will contact you");	
				   window.location.href = o+"oxygen_resort/success";
				}else{
				    $("#otp_email"+i).val(email);
				    $("#otp_phone"+i).val(phone);
				    $("#form_div"+i).hide();
				   	$("#otp_section"+i).show();
				   //window.location.href = o+"oxygen_resort/success";
				   
				}
			}
		}
  
      )	  
}

function oxygenresort_otpverify(u,i)
{
	var otp          = $("#otp"+i).val();
	var otp_email    = $("#otp_email"+i).val();
    var otp_phone    = $("#otp_phone"+i).val();

	if($.trim(otp) == ""){
		 $("#otp"+i).css("border-color","rgb(232, 46, 33)").focus();
		 $("#otp_err"+i).html("Please Correct enter OTP");
	 	return false;  
	}else{
		 $("#otp"+i).css("border-color","rgb(218, 218, 218)");
		 $("#otp_err"+i).html(""); 
	}
	if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
	}
	$.ajax({
			url:u,
			type:"post",
			data: "&email="+otp_email+"&otp="+otp,
			beforeSend:function(){
				$("#spinner_loader").show();
				$("#sbmtbtn"+i).val("Processing..");
				$(':input[type="submit"]').prop('disabled', true);
			},
			success:function(e){
				
				$(':input[type="submit"]').prop('disabled', false);
				$("sbmtbtn"+i).val("Submit");
				
				var e = $.trim(e);
   
				if(e == "done"){

				 window.location.href = o+"oxygen_resort/success";
				}else if(e == "resend"){
					$("#otp"+i).val("");
				    $("#otp_err"+i).html("Incorrect OTP");
				    $("#otp_email"+i).val(otp_email);
				    $("#otp_phone"+i).val(otp_phone);
				    $("#form_div"+i).hide();
				   	$("#otp_section"+i).show();
				   //window.location.href = o+"oxygen_resort/success";
				   
				}
			}
		}
  
      )	 
}


function otp_resend(u,i){

	var otp_email    = $("#otp_email"+i).val();
    var otp_phone    = $("#otp_phone"+i).val();
	$.ajax({
			url:u,
			type:"post",
			data: "&email="+otp_email+"&phone="+otp_phone,
			beforeSend:function(){
				
			},
			success:function(e){
				
				var e = $.trim(e);

				if(e == "done"){
				 $("#otp"+i).val("");
				    $("#otp_err"+i).html("OTP Resent Successfully");
				    $("#otp_email"+i).val(otp_email);
				    $("#otp_phone"+i).val(otp_phone);
				    $("#form_div"+i).hide();
				   	$("#otp_section"+i).show();
				}else{
				   alert("something went wrong");
				   window.location.href = o+"oxygen_resort";
				}
			}
		}
  
      )	 
}
