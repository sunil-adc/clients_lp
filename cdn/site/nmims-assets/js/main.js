$(document).ready(function () {
	$(function () {
		$('.gallery-grid1 a').Chocolat();
	});
	$('.skillbar').skillBars({
		from: 0,
		speed: 4000,
		interval: 100,
		decimals: 0,
	});
	$(".scroll").click(function (event) {
		event.preventDefault();

		$('html,body').animate({
			scrollTop: $(this.hash).offset().top
		}, 1000);
	});
	$().UItoTop({
		easingType: 'easeOutQuart'
	});
	$(".phone").intlTelInput({
		utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/8.4.6/js/utils.js",
		separateDialCode: "true",
		initialCountry: "IN",

	});
	$(".phone").on("countrychange", function (e, countryData) {
		$(".hiddenCountry").val(countryData['dialCode']);
	});
	$('.time_to_call').select2();
});
$(window).load(function () {
	$('.flexslider').flexslider({
		animation: "slide",
		start: function (slider) {
			$('body').removeClass('loading');
		}
	});
});
addEventListener("load", function () {
	setTimeout(hideURLbar, 0);
}, false);

function hideURLbar() {
	window.scrollTo(0, 1);
}

// $(document).ready(function () {
// 	setTimeout(function () {
// 		if ($('.popup-enquiry-form').length) {
// 			$.magnificPopup.open({
// 				items: {
// 					src: '#popupForm'
// 				},
// 				type: 'inline'
// 			});
// 		}
// 	}, 15000);
// 	$('.pricingTable-signup').magnificPopup({
// 		type: 'inline',
// 		items: {
// 			src: '#popupForm'
// 		}

// 	});
	
// });



$(document).ready(function () {

	var _floatbox = $("#contact_form"), _floatbox_opener = $(".contact-opener");
	_floatbox.css("right", "0px"); //initial contact form position
	_floatbox.css("display", "none");
	//Contact form Opener button
	_floatbox_opener.click(function () {
		if (_floatbox.hasClass('visiable')) {
			_floatbox.animate({ "right": "-300px" }, { duration: 300 }).removeClass('visiable');
		} else {
			_floatbox.animate({ "right": "0px" }, { duration: 300 }).addClass('visiable');
		}
	});
	_floatbox.animate({ "top": "30px" }, { easing: "linear" }, { duration: 500 });
});
var $window = $(window);
var nav = $('#contact_form');
$window.scroll(function () {
	if ($window.scrollTop() <= 500) {
		nav.css("display", "none");
	}
	else {
		nav.css("display", "block");
	}
});


$(document).ready(function(){
    $("#testimonial-slider").owlCarousel({
        items:2,
        itemsDesktop:[1000,2],
        itemsDesktopSmall:[990,1],
        itemsTablet:[768,1],
        itemsMobile:[650,1],
        pagination:true,
        navigation:false,
        autoPlay:false
    });
});


$(document).ready(function(){
    $("#hiring-slider").owlCarousel({
        items:1,
        itemsDesktop:[1199,1],
        itemsDesktopSmall:[979,1],
        itemsTablet:[768,1],
        pagination: false,
        navigation:true,
        navigationText:["",""],
        autoPlay:true
    });
});