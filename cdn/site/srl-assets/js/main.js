$(document).ready(function () {
    $("#owl-demo").owlCarousel({
        items: 1,
        lazyLoad: true,
        autoPlay: true,
        navigation: false,
        navigationText: false,
        pagination: true,
    });
    $('.selectSearch').select2();
    /* init Jarallax */
	// 	$('.jarallax').jarallax({
	// 		speed: 0.5,
	// 		imgWidth: 1366,
	// 		imgHeight: 768
    // })
    $('.timepicker').wickedpicker({ twentyFour: false });
    $(function () {
        $("#datepicker,#datepicker1,#datepicker2,#datepicker3, #get_date").datepicker();
    });
    $(".smoothScroll").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
          // Prevent default anchor click behavior
          event.preventDefault();
    
          // Store hash
          var hash = this.hash;
    
          // Using jQuery's animate() method to add smooth page scroll
          // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
          $('html, body').animate({
            scrollTop: $(hash).offset().top
          }, 800, function(){
       
            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
          });
        } // End if
      });

})
$(function () {
    // Slideshow 4
    $("#slider4").responsiveSlides({
        auto: true,
        pager: true,
        nav: false,
        speed: 500,
        namespace: "callbacks",
        before: function () {
            $('.events').append("<li>before event fired.</li>");
        },
        after: function () {
            $('.events').append("<li>after event fired.</li>");
        }
    });

});
$(function () {
    // Slideshow 4
    $("#slider3").responsiveSlides({
        auto: true,
        pager: false,
        nav: true,
        speed: 500,
        namespace: "callbacks",
        before: function () {
            $('.events').append("<li>before event fired.</li>");
        },
        after: function () {
            $('.events').append("<li>after event fired.</li>");
        }
    });

});
