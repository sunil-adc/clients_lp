$(document).ready(function(e) {  


    $('.products').click(function(event) {
         $(".products").each(function(i) {
             $(this).find('input[type="radio"]').attr('checked', false);
                                                                                                                                                                    
          });
         $(this).find('input[type="radio"]').attr('checked', false);
         var product = $(this).data('pid');
         var id =$(this).attr('id');
         if(product == 1){
              $("#p"+product).attr("checked", "checked");
              $("#prod").val(id);
              $("#myImage1").show();
              $("#myImage").show();
              $("#myImage2").hide();
              $("#myImage1h").hide();
              $("#myImageh").hide();
              $("#myImage2h").show();
              window.scrollTo({
                 top: $('#hr').offset().top,
                 behavior: 'smooth'
              })

         }else if(product == 2){
              $("#p"+product).attr("checked", "checked");
              $("#prod").val(id);
              $("#myImage1").hide();
              $("#myImage").show();
              $("#myImage2").show();
              $("#myImage1h").show();
              $("#myImageh").hide();
              $("#myImage2h").hide();
              window.scrollTo({
                 top: $('#hr').offset().top,
                 behavior: 'smooth'
              })

         }else if(product == 3){
              $("#p"+product).attr("checked", "checked");
              $("#prod").val(id);
              $("#myImage1").show();
              $("#myImage").hide();
              $("#myImage2").show();
              $("#myImage1h").hide();
              $("#myImageh").show();
              $("#myImage2h").hide();
              window.scrollTo({
                 top: $('#hr').offset().top,
                 behavior: 'smooth'
              })

         }
    });
    jQuery.validator.addMethod("lettersonly", function (value, element) {
    return this.optional(element) || /^[a-zA-Z]+$/i.test(value);
    }, "Please enter valid name");

    jQuery.validator.addMethod("phoneIN", function (phone_number, element) {
    phone_number = phone_number.replace(/\s+/g, "");
    return this.optional(element) || phone_number.length == 10 && phone_number.match(/^[6-9]{1}[0-9]{9}$/);
    }, "Please enter valid 10 digit mobile number");

    jQuery.validator.addMethod("pincode", function (zipcode, element) {
    zipcode = zipcode.replace(/\s+/g, "");
    return this.optional(element) || zipcode.length == 6 && zipcode.match(/^[1-9]{1}[0-9]{5}$/);
    }, "Please enter valid 6 digit pincode ");
    
     

    $('#detailsform').validate({
      rules: {
        first_name: {
          required: true,
          minlength: 3,
          lettersonly: true
        },
        last_name: {
          required: true,
          lettersonly: true
        },
        zip_code: {
        required: true,
        number: true,
        maxlength:6,
        minlength: 6,
        pincode:true
      },
      address_1: {
        required: true,
      },
      city:{
        required:true,
      },
      state:{
        required:true,
      },
      email:{
        required:true,
        email:true,
      },
      phone_number:{
         required: true,
         number: true,
         phoneIN: true
      },
      prod:{
        required:true,
      }
     
    },
    messages: {
      first_name: {
          required:"Please Enter First Name",
      },
      last_name: {
          required:"Please Enter Last Name",
      },
      zip_code: {
          required:"Please Enter Correct Pincode",
      },
      address_1: {
          required:"Please Enter Address",
      },
      city: {
          required:"Please Enter City",
      },
      state: {
          required:"Please Select State",
      },
      email: {
          required:"Please Enter Email Id",
      },
      phone_number: {
          required:"Please Enter Correct Phone Number",
      },
      prod:{
        required:"Please Select Product To Proceed"
      }
    },
    submitHandler: function () {
      if ($('#detailsform').length) {
      var site_url = $('#detailsform').find('input[name="siteurl"]').val();
      var userDetails = new Object();
      userDetails.firstname    = $('#detailsform').find('input[name="first_name"]').val();
      userDetails.lastname     = $('#detailsform').find('input[name="last_name"]').val();
      userDetails.zipcode      = $('#detailsform').find('input[name="zip_code"]').val();
      userDetails.address      = $('#detailsform').find('input[name="address_1"]').val();
      userDetails.city         = $('#detailsform').find('input[name="city"]').val();
      userDetails.state        = $('#detailsform').find('select[name="state"]').val();
      userDetails.phone        = $('#detailsform').find('input[name="phone_number"]').val();
      userDetails.email        = $('#detailsform').find('input[name="email"]').val();
      userDetails.product      = $('#detailsform').find('input[name="prod"]').val();
      userDetails.utm_source   = $('#detailsform').find('input[name="utm_source"]').val();
      userDetails.utm_medium   = $('#detailsform').find('input[name="utm_medium"]').val();
      userDetails.utm_sub      = $('#detailsform').find('input[name="utm_sub"]').val();
      userDetails.utm_campaign = $('#detailsform').find('[name="utm_campaign"]').val();
      userDetails.otp          = $('#detailsform').find('[name="otp"]').val();
      if(userDetails.otp == ""){
        $.ajax({
        url: site_url + "nutratimes/formsubmit",
        type: "post",
        data: userDetails,
        beforeSend: function () {

        },
        success: function (e) {
           
           if(e == "done"){
           
            // window.location.href = site_url+"nutratimes/nutrasuccess";

             $('#detailsform').find('.otp').addClass("activebox");
             $('#detailsform').find(".otp_error").html("OTP sent to your mobile.Please verify");

           }else{
               window.location.href = site_url+"nutratimes/product";
           }
        
              
        }
      });
    }else{
     $.ajax({
    url: site_url + "nutratimes/otp_verify",
    type: "post",
    data: userDetails,
    beforeSend: function () {

    },
    success: function (e) {
       if(e == "done"){
        $('#detailsform').find(".otp_error").html("");
        window.location.href = site_url+"nutratimes/nutrasuccess";

       }else{
           $('#detailsform').find(".otp_error").html("Incorrect OTP");
       }
    
          
    }
  });
    }
        //userDetails($('#detailsform'));
      }
    }
    });
    

});

function otpverify(u){

  var otp = $("#otp").val();
  
  if($.trim(otp) == "" || otp.length != '4'){
     $("#otp"+type).css("border-color","rgb(232, 46, 33)").focus();
     $("#otp_err"+type).html("Please Correct enter OTP");
    return false;  
  }else{
     $("#otp"+type).css("border-color","rgb(218, 218, 218)");
     $("#otp_err"+type).html(""); 
  }
  if(typeof $("#siteurl"+type).val()!="undefined"){
      var o=$("#siteurl"+type).val();
  }

  $.ajax({
      url:u,
      type:"post",
      data: "&otp="+otp,
      beforeSend:function(){
        
      },
      success:function(e){
       
        if(e == "done"){
         window.location.href = site_url+"nutratimes/nutrasuccess";
        }else if(e == "resend"){
          $("#otp").val("");
          $("#otp_err").html("Incorrect OTP");
           
        }
      }
    }) 

}

 function otpresend(u,type){
 //alert(u);return false
  $.ajax({
      url:u,
      beforeSend:function(){
        
      },
      success:function(e){
        //alert(e);return false;
        if(e == "done"){
           $(".otp").val("");
           //alert("otp resent successfully");
           $(".otp_sent").html("OTP Resent Successfully");
            
        }else{
         $(".otp_sent").html("Something went wrong");
        }
      }
    }
  
      ) 
 }
 